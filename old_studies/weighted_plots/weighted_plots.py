from ROOT import *
import operator
gROOT.Reset()
gStyle.SetOptStat(000)
from math import sqrt
from ROOT import gDirectory as gd



kGreen = kGreen+2

filename = {
    "13512010_Bs_Kmunu_":"DTT_13512010_Bs_Kmunu_DecProdCut_Dn_Py8_MC12___inclb_BDT____neutral_BDT____inclb_afc_BDT____SS_aftercut_BDT____charge_BDT____charge_neutral_BDT__.root",
    # "data11":"",
    # "data11SS":"",
    "data12":"_DTT_2012_Reco14Strip21r0p1a_Up_SEMILEPTONIC_trimmed___inclb_BDT____neutral_BDT____charge_BDT____charge_neutral_BDT_____SS_aftercut_BDT____inclb_afc_BDT__.root",
    # "data12SS":"",
    # inclb bkg
    # "10010032_OC2Kmu":"DTT_MC12_inclb_OC2Kmu_gencut_BKG_10010032_Dn___inclb_BDT____neutral_BDT____inclb_afc_BDT____SS_aftercut_BDT____charge_BDT____charge_neutral_BDT__.root",
    "10010037_OC2Kplusmu":"DTT_MC12_inclb_OC2Kplusmu_gencut_BKG_10010037_Dn___inclb_BDT____neutral_BDT____inclb_afc_BDT____SS_aftercut_BDT____charge_BDT____charge_neutral_BDT__.root",
    # main specific bkg (to be removed the shared with the inclb)
    "13512400_Bs_Kstmunu_Kpi0_":"DTT_13512400_Bs_Kstmunu_Kpi0_DecProdCut_Dn_Py8_MC12___inclb_BDT____neutral_BDT____inclb_afc_BDT____SS_aftercut_BDT____charge_BDT____charge_neutral_BDT__.root",
    "13512410_Bs_K2stmunu_Kpi0_":"DTT_13512410_Bs_K2stmunu_Kpi0_DecProdCut_Dn_Py8_MC12___inclb_BDT____neutral_BDT____inclb_afc_BDT____SS_aftercut_BDT____charge_BDT____charge_neutral_BDT__.root",
    "13512420_Bs_K1430munu_Kpi0_":"DTT_13512420_Bs_K1430munu_Kpi0_DecProdCut_Dn_Py8_MC12___inclb_BDT____neutral_BDT____inclb_afc_BDT____SS_aftercut_BDT____charge_BDT____charge_neutral_BDT__.root",
    "12143001_Bu_JpsiK_mm_":"DTT_12143001_Bu_JpsiK_mm_DecProdCut_Dn_Py8_MC12___inclb_BDT____neutral_BDT____inclb_afc_BDT____SS_aftercut_BDT____charge_BDT____charge_neutral_BDT__.root",
    "12143401_Bu_JpsiKst_mm_Kpi0_":"DTT_12143401_Bu_JpsiKst_mm_Kpi0_DecProdCut_Dn_Py8_MC12___inclb_BDT____neutral_BDT____inclb_afc_BDT____SS_aftercut_BDT____charge_BDT____charge_neutral_BDT__.root",
    # "15512013_Lb_pmunu_":"DTT_15512013_Lb_pmunu_DecProdCut_LCSR_Dn_Py8_MC12___inclb_BDT____neutral_BDT____inclb_afc_BDT____SS_aftercut_BDT____charge_BDT____charge_neutral_BDT__.root",
    # bkgs to merge
    "13144001_Bs_Jpsiphi_mm_":"DTT_13144001_Bs_Jpsiphi_mm_CPV_update2012_DecProdCut_Dn_Py8_MC12___inclb_BDT____neutral_BDT____inclb_afc_BDT____SS_aftercut_BDT____charge_BDT____charge_neutral_BDT__.root",
    "11144001_Bd_JpsiKst_mm_":"DTT_11144001_Bd_JpsiKst_mm_DecProdCut_Dn_Py8_MC12___inclb_BDT____neutral_BDT____inclb_afc_BDT____SS_aftercut_BDT____charge_BDT____charge_neutral_BDT__.root",
    # "13774002_DsMuNu_cocktail":"DTT_MC12_Bs2DsMuNu_13774002_Cocktail_BKG_Dn___inclb_BDT____neutral_BDT____inclb_afc_BDT____SS_aftercut_BDT____charge_BDT____charge_neutral_BDT__.root",
    # "11512011_Bd_pimunu_":"DTT_11512011_Bd_pimunu_DecProdCut_Dn_Py8_MC12___inclb_BDT____neutral_BDT____inclb_afc_BDT____SS_aftercut_BDT____charge_BDT____charge_neutral_BDT__.root",
    # "11512400_Bd_rhomunu_pipi0_":"DTT_11512400_Bd_rhomunu_pipi0_DecProdCut_Dn_Py8_MC12___inclb_BDT____neutral_BDT____inclb_afc_BDT____SS_aftercut_BDT____charge_BDT____charge_neutral_BDT__.root",
    # "11874004_Bd_Dstmunu_Kpi_":"DTT_11874004_Bd_Dstmunu_Kpi_cocktail_hqet_D0muInAcc_BRCorr1_Dn_Py8_MC12___inclb_BDT____neutral_BDT____inclb_afc_BDT____SS_aftercut_BDT____charge_BDT____charge_neutral_BDT__.root",
    # "11874042_Bd_Dmunu_K-pi+pi+_":"DTT_11874042_Bd_Dmunu_K-pi+pi+_cocktail_hqet_BRCorr1_DmuInAcc_Dn_Py8_MC12___inclb_BDT____neutral_BDT____inclb_afc_BDT____SS_aftercut_BDT____charge_BDT____charge_neutral_BDT__.root",
    # "12873002_Bu_D0munu_Kpi_":"DTT_12873002_Bu_D0munu_Kpi_cocktail_D0muInAcc_BRcorr1_Dn_Py8_MC12___inclb_BDT____neutral_BDT____inclb_afc_BDT____SS_aftercut_BDT____charge_BDT____charge_neutral_BDT__.root"
    }
data12OS_numbers_Up = [1000, 1001, 1002, 1003, 1004, 1005, 1006, 1007, 1008, 1009, 1010, 1011, 1012, 1013, 1014,1015, 1017, 1028, 1032, 694, 696, 697, 698, 702, 703, 706, 711, 714, 715, 716, 717, 718, 719, 720, 721,
722, 723, 724, 725, 727, 728, 729, 730, 731, 732, 733, 734, 735, 737, 738,739, 740, 741, 742, 743, 751, 778, 781, 806, 813, 846, 850, 855, 857, 860, 861, 862, 863, 864, 865,
866, 867, 868, 869, 870, 871, 872, 873, 874, 875, 876, 877, 878, 879, 880,881, 882, 883, 884, 885, 886, 887, 888, 889, 890, 891, 892, 893, 894, 895, 896, 897, 899, 900, 901,
902, 903, 904, 905, 906, 907, 909, 910, 911, 912, 913, 914, 915, 916, 917,918, 919, 920, 921, 922, 923, 924, 925, 927, 928, 929, 930, 931, 932, 934, 935, 937, 938, 939, 940,
941, 942, 943, 944, 946, 947, 949, 950, 951, 952, 953, 954, 955, 956, 957,958, 959, 960, 961, 962, 963, 964, 965, 966, 967, 969, 970, 971, 972, 973, 974, 975, 976, 977, 978,
979, 980, 981, 982, 983, 984, 985, 986, 987, 988, 989, 990, 991, 992, 993,994, 995, 996, 997, 998, 999 ]

data12OS_numbers_Down = [116, 117, 118, 119, 120, 121, 122, 123, 124, 125, 126, 127, 128, 129, 12,130, 131, 132, 133, 134, 136, 137, 138, 139, 13, 140, 141, 142, 143, 144, 145, 146, 147, 148, 14,
150, 151, 152, 153, 154, 155, 156, 157, 158, 159, 15, 160, 161, 162, 163,164, 165, 166, 167, 168, 16 , 16, 170, 171, 172, 173, 174, 175, 176, 177, 178, 179, 17, 180, 181,
182, 183, 184, 185, 186, 187, 188, 189, 18, 190, 191, 192, 193, 194, 195,196, 197, 198, 199, 19, 1, 200, 201, 202, 203, 204, 205, 206, 207, 208, 209, 20, 210, 211, 212,
213, 214, 215, 216, 217, 218, 219, 21, 220, 221, 222, 223, 224, 225, 226,227, 228, 229, 22, 230, 231, 232, 233, 234, 235, 236, 237, 238, 239, 23, 240, 241, 242, 243, 244,
245, 246, 247, 248, 249, 24, 250, 251, 252, 253, 254, 255, 256, 257, 258,259, 25, 260, 261, 262, 263, 264, 265, 266, 267, 268, 269, 26, 270, 271, 272, 273, 274, 275, 276,
277, 278, 279, 27, 280, 281, 282, 283, 284, 285, 286, 287, 288, 289, 28,290, 291, 292, 293, 294, 295, 296, 297, 298, 299, 29, 2, 300, 301, 302, 303, 304, 305, 306, 307,
308, 309, 310, 311, 312, 313, 314, 315, 316, 317, 318, 319, 31, 320, 321,322, 323, 324, 325, 326, 327, 328, 329, 32, 330, 331, 332, 333, 334, 336, 337, 338, 339, 33, 340,
341, 342, 343, 344, 345, 346, 347, 348, 349, 34, 350, 351, 352, 353, 354,355, 356, 357, 358, 359, 35, 360, 361, 362, 363, 364, 365, 36, 37, 38, 39, 3, 40, 41, 42,
43, 44, 45, 46, 47, 49, 4, 50, 51, 52, 53, 54, 55, 56, 57,58, 59, 5, 60, 61, 6 , 63, 64, 65, 66, 67, 68, 69, 6, 70, 71, 72, 73, 74, 75,
76, 77, 78, 79, 7, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89,8, 90, 91, 92, 93, 9 , 95, 96, 97, 98, 99, 9, 100, 101, 102, 103, 104, 105, 106, 107,
108, 109, 10, 110, 111, 112, 113, 114, 115]
# data12SS_numbers = []

tall = {}
for key in filename.keys():
    tall[key] = TChain("Bs2KmuNuTuple/DecayTree")

for key in filename.keys():
    if "data" not in key:
        pathtofile = "/home/borgheresi/Tuple_updated_allBDT/"
        tall[key].Add(pathtofile+filename[key])
        tall[key].Add(pathtofile+filename[key].replace("_Dn","_Up"))
    if "data12" in key:
        tall[key] = TChain("reducedTree")
        pathtofile = "/home/borgheresi/DATA_Trimmed_Tuple_allBDT/"
        for nup in data12OS_numbers_Up:
            tall[key].Add(pathtofile+str(nup)+filename[key])
        for ndn in data12OS_numbers_Down:
            tall[key].Add(pathtofile+str(ndn)+filename[key].replace("Up","Down"))

        
totlumy = 2.0 # in fb-1
weights = { # to 1 fb-1
    "13512010_Bs_Kmunu_":0.241866762641,
    # "data11":1.,
    # "data11SS":10.,
    "data12":1.,
    # "data12SS":10.,
    # inclb bkg
    # "10010032_OC2Kmu":784095.359311,
    "10010037_OC2Kplusmu":18, # 532324.13031,
    # main specific bkg (to be removed the shared with the inclb)
    "13512400_Bs_Kstmunu_Kpi0_":0.0971215585185,
    "13512410_Bs_K2stmunu_Kpi0_":0.0242154870765,
    "13512420_Bs_K1430munu_Kpi0_":0.0476334228613,
    "12143001_Bu_JpsiK_mm_":0.0744035388832,
    "12143401_Bu_JpsiKst_mm_Kpi0_":0.207021475836,
    "15512013_Lb_pmunu_":2.24556611432,
    # bkgs to merge
    "11144001_Bd_JpsiKst_mm_":0.0620351277637,
    "13144001_Bs_Jpsiphi_mm_":0.00142240156917,
    "13774002_DsMuNu_cocktail":0.126598308066,
    "11512011_Bd_pimunu_":0.654681869718,
    "11512400_Bd_rhomunu_pipi0_":1.21767934762,
    "11874004_Bd_Dstmunu_Kpi_":0.570864417175,
    "11874042_Bd_Dmunu_K-pi+pi+_":3.61564782099,
    "12873002_Bu_D0munu_Kpi_":0.755480915516,
    }
#old weights
# weights = { # to 1 fb-1
#     "13512010_Bs_Kmunu_": 28225.529457145,
#     "11144001_Bd_JpsiKst_mm_": 25688.744403291,
#     "12143001_Bu_JpsiK_mm_": 56512.904666428,
#     # "13144001_Bs_Jpsiphi_mm_": 6799.2199000674,
#     "12143401_Bu_JpsiKst_mm_Kpi0_": 52036.148582672,
#     "13512400_Bs_Kstmunu_Kpi0_": 14143.839233374,
#     "13512410_Bs_K2stmunu_Kpi0_": 2566.32023770062,
#     "13512420_Bs_K1430munu_Kpi0_": 5101.5356611896,
#     "15512013_Lb_pmunu_":597.1131346113327,
#     "DsMuNu_cocktail":19555.051942671937*(0.0555/0.0066), # *15*3.5
#     "data12": 1., # data   
#     "data12SS":10, # data SS prescaled x10
#     "data11": 1., # data   
#     "data11SS":10, # data SS prescaled x10
#     "10010032_inclb_OC2Kmu":,
#     "10010037_inclb_OC2Kplusmu":
#     # with BDT included
#     # "13512010_Bs_Kmunu_": 23194.6271491, # signal
#     # "11144001_Bd_JpsiKst_mm_": 1136.63468611, # JPsi
#     # "12143001_Bu_JpsiK_mm_": 7063.39875108, # JPsi
#     # "13144001_Bs_Jpsiphi_mm_": 119.258596671, # JPsi
#     # "13512400_Bs_Kstmunu_Kpi0_": 10349.5893084, # neutral
#     # "13512410_Bs_K2stmunu_Kpi0_": 1960.45489963, # neutral
#     # "13512420_Bs_K1430munu_Kpi0_": 3828.95964122, # neutral
#     }
for k in weights.keys():
    if "data1" not in k :
        weights[k] = weights[k]*totlumy

# trig = "(Bs_Hlt2TopoMu2BodyBBDTDecision_TOS || Bs_Hlt2SingleMuonDecision_TOS)"
# Qreq = "&& (Bs_Q2SOL1 != -1000)"
# kstar = "((kaon_m_MasshPi0_maxCL>832 && kaon_m_MasshPi0_maxCL<962) && (kaon_m_Pi0_M_maxCL>112 && kaon_m_Pi0_M_maxCL<158))"
# kstar1 = "((kaon_m_MasshPi0_maxCL>1340 && kaon_m_MasshPi0_maxCL<1520) && (kaon_m_Pi0_M_maxCL>112 && kaon_m_Pi0_M_maxCL<158))"
# vetoKstar = "&& !("+kstar+"||"+kstar1+")"
# b = "(sqrt(105.66*105.66+105.66*105.66+ 2.*sqrt(105.66*105.66 + kaon_m_PX*kaon_m_PX + kaon_m_PY*kaon_m_PY + kaon_m_PZ*kaon_m_PZ)*sqrt(105.66*105.66 + muon_p_PX*muon_p_PX + muon_p_PY*muon_p_PY + muon_p_PZ*muon_p_PZ)-2*(kaon_m_PX*muon_p_PX + kaon_m_PY*muon_p_PY + kaon_m_PZ*muon_p_PZ)) < 3130)"
# a = "(sqrt(105.66*105.66+105.66*105.66+ 2.*sqrt(105.66*105.66 + kaon_m_PX*kaon_m_PX + kaon_m_PY*kaon_m_PY + kaon_m_PZ*kaon_m_PZ)*sqrt(105.66*105.66 + muon_p_PX*muon_p_PX + muon_p_PY*muon_p_PY + muon_p_PZ*muon_p_PZ)-2*(kaon_m_PX*muon_p_PX + kaon_m_PY*muon_p_PY + kaon_m_PZ*muon_p_PZ)) > 3072)"
# veto_JpsimisID = "&& !(("+a+"&&"+b+")&& (kaon_m_isMuon==1))"
# visMass = "&& (Bs_M > 1900)"
# # BDTcut = "&& ((TMVA_JPsi_BDT > 0.01895) && (TMVA_SS_aftercut_BDT>-0.1455))"
# thecut = trig+Qreq+vetoKstar+veto_JpsimisID+visMass

dummy = TCanvas("dummy","dummy") 

variables = {
    # "TMVA_charge_BDT":"hcharge{0}(250,0.1,0.3)",
    # "TMVA_inclb_BDT":"hinclb{0}(250,-0.25,0.25)",
    # "TMVA_SS_aftercut_BDT":"hSS{0}(250,-0.1,0.25)",
    "Bs_MCORR":"hMCORR{0}(50, 2400, 5500)",
    # "Bs_M":"hM{0}(200, 1300, 5500)",
    # "Bs_Q2SOL1":"hQSOL1{0}(200, 0, 25000000)",
    # "Bs_Q2SOL2":"hQSOL2{0}(200, 0, 25000000)",
    # # "Bs_Q2SOL1/Bs_Q2SOL2":"hratio12{0}(200,0,3)",
    # # "Bs_Q2SOL2/Bs_Q2SOL1":"hratio21{0}(200,0,3)",
    # "kaon_m_PAIR_M":"hKh{0}(400, 600, 4000)",
    # "muon_p_PAIR_M":"hMuMu{0}(550, 100, 5000)"
    }
# listforLogY = ["kaon_m_PAIR_M","muon_p_PAIR_M"]
listforLogY = []

colorlist = {
    "13512010_Bs_Kmunu_":kBlack,
    # "data11":kBlack,
    # "data11SS":kBlack,
    "data12":kBlack,
    # "data12SS":kBlack,
    # inclb bkg
    # "10010032_OC2Kmu":kCyan,
    "10010037_OC2Kplusmu":kBlue-2,
    # main specific bkg (to be removed the shared with the inclb)    
    "13512400_Bs_Kstmunu_Kpi0_":kViolet,
    "13512410_Bs_K2stmunu_Kpi0_":kOrange,
    "13512420_Bs_K1430munu_Kpi0_":kYellow+2,
    "12143001_Bu_JpsiK_mm_":kBlue,
    "12143401_Bu_JpsiKst_mm_Kpi0_":kGreen+2,
    "15512013_Lb_pmunu_":kRed,
    # bkgs to merge
    "11144001_Bd_JpsiKst_mm_":kOrange+4,
    "13144001_Bs_Jpsiphi_mm_":kGray,
    # "13774002_DsMuNu_cocktail":kGray,
    "11512011_Bd_pimunu_":kRed,
    # "11512400_Bd_rhomunu_pipi0_":kGray,
    # "11874004_Bd_Dstmunu_Kpi_":kGray,
    # "11874042_Bd_Dmunu_K-pi+pi+_":kGray,
    # "12873002_Bu_D0munu_Kpi_":kGray
    }

fout = TFile("/home/borgheresi/weighted_plots/output_weighted_plotsNEW.root","recreate")
fout.cd()

#For the x axes
latvar = {}
for var in variables.keys():
    lat = TLatex(0.2,0.01,var)
    lat.SetNDC()
    lat.SetTextSize(0.06)
    latvar[var] = lat

canvasall = []

# non stack plots after some cuts
# cuts = {
#     "NoCuts":"(1.)",
#     "TMVA_JPsi_BDT": "(TMVA_JPsi_BDT > 0.01895)",
#     "TMVA_neutral_JpsiKst_BDT": "(TMVA_neutral_JpsiKst_BDT > -0.1706)",
#     "TMVA_neutral_JPsi_BDT": "(TMVA_neutral_JPsi_BDT > -0.1214)"
#     }
# first_plot = {
#     "NoCuts":"12143001_Bu_JpsiK_mm_",
#     "TMVA_JPsi_BDT":"13512010_Bs_Kmunu_",
#     "TMVA_neutral_JpsiKst_BDT":"12143001_Bu_JpsiK_mm_",
#     "TMVA_neutral_JPsi_BDT":"13512010_Bs_Kmunu_"
#     }
# for cutname,cut in zip(cuts.keys(), cuts.values()):
#     for var in variables.keys():
#         print "Printing variable:"+var+" for cut "+cutname
#         c = TCanvas("c"+var+cutname,"c"+var+cutname,1600,1000)
#         if(var in listforLogY): gPad.SetLogy()
#         tall[first_plot[cutname]].SetLineColor(colorlist[first_plot[cutname]])
#         tall[first_plot[cutname]].Draw(var+">>"+cutname+variables[var].format(first_plot[cutname]+var), "("+str(weights[first_plot[cutname]])+")*"+cut,"h")
#         c.Update()
#         latvar[var].Draw("same")
#         for mode in filename.keys():
#             if mode == first_plot[cutname]:
#                 continue
#             tall[mode].SetLineColor(colorlist[mode])
#             tall[mode].Draw(var+">>"+cutname+variables[var].format(mode+var), "("+str(weights[mode])+")*"+cut, "sameh"), weights[mode]
#             c.Update()
#         canvasall.append(c)
#         c.SaveAs(var.replace("/","")+"_"+cutname+".pdf")

tosavedifferent = ""
dic_forleg = {}
# book the histograms
stack_dict = {}
histo_saver = {}
not_in_stack = ["data11","data11SS","data12","data12SS"]
dummy.cd()
for var in variables.keys():    
    stack_dict[var] = THStack("hstack_"+var,"hstack_"+var)
    # histo_range = variables[var][variables[var].find("(")+1: variables[var].find(")")]
    for key in filename.keys():
        if key not in ["data11SS","data12SS"]:
            dic_forleg[key] =tall[key].Draw( var+">>h"+variables[var].format(var+key), "((TMVA_charge_BDT>0.107104007248) && (TMVA_SS_aftercut_BDT>-0.09541729826))"+"*"+str(weights[key]) )
            histo_saver[var+key] = gd.Get( "h"+variables[var].format(var+key)[0:variables[var].format(var+key).find("(")] )
            histo_saver[var+key].SetFillColor(colorlist[key])
        
# exec('histo_saver["'+var+key+'"] = TH1F("h'+var+key+'","h'+var+key+'",'+histo_range+")")
# if(key not in not_in_stack):
#     histo_saver[var+key].SetFillColor(colorlist[key])
# else:
#     histo_saver[var+key].SetLineColor(colorlist[key])

# # fill the histograms
# for key in filename.keys():
#     print "starting filling the file ", key
#     for i,evn in enumerate(tall[key]):
#         # # comment to run not in test mode
#         # if i == 1: print "you are running in test mode"
#         # if i > 500: break        
#         if not (evn.Bs_Q2SOL1 > 0): continue
#         if not ((evn.Bs_Hlt2TopoMu2BodyBBDTDecision_TOS == 1) | (evn.Bs_Hlt2SingleMuonDecision_TOS == 1)): continue
#         if not (evn.Bs_M > 1900.): continue
#         if not (evn.totCandidates<2): continue
#         Mpi0 = (evn.kaon_m_Pi0_M_maxCL>112) & (evn.kaon_m_Pi0_M_maxCL<158)
#         kstar = (evn.kaon_m_MasshPi0_maxCL>832) & (evn.kaon_m_MasshPi0_maxCL<962) & Mpi0
#         kstar1 = (evn.kaon_m_MasshPi0_maxCL>1340) & (evn.kaon_m_MasshPi0_maxCL<1520) & Mpi0 # for kstar2 is the same mass range
#         myKstar_veto = not (kstar | kstar1)
#         if not myKstar_veto: continue
#         mu_m = 105.66
#         b = (sqrt(mu_m*mu_m+mu_m*mu_m+ 2.*sqrt(mu_m*mu_m + evn.kaon_m_PX*evn.kaon_m_PX + evn.kaon_m_PY*evn.kaon_m_PY + evn.kaon_m_PZ*evn.kaon_m_PZ)*sqrt(mu_m*mu_m + evn.muon_p_PX*evn.muon_p_PX + evn.muon_p_PY*evn.muon_p_PY + evn.muon_p_PZ*evn.muon_p_PZ)-2*(evn.kaon_m_PX*evn.muon_p_PX + evn.kaon_m_PY*evn.muon_p_PY + evn.kaon_m_PZ*evn.muon_p_PZ)) < 3130)
#         a = (sqrt(mu_m*mu_m+mu_m*mu_m+ 2.*sqrt(mu_m*mu_m + evn.kaon_m_PX*evn.kaon_m_PX + evn.kaon_m_PY*evn.kaon_m_PY + evn.kaon_m_PZ*evn.kaon_m_PZ)*sqrt(mu_m*mu_m + evn.muon_p_PX*evn.muon_p_PX + evn.muon_p_PY*evn.muon_p_PY + evn.muon_p_PZ*evn.muon_p_PZ)-2*(evn.kaon_m_PX*evn.muon_p_PX + evn.kaon_m_PY*evn.muon_p_PY + evn.kaon_m_PZ*evn.muon_p_PZ)) > 3072)
#         myJpsimisID_veto = not (a & b & (evn.kaon_m_isMuon==1))
#         if not myJpsimisID_veto: continue
#         if not evn.TMVA_JPsi_BDT > 0.01895: continue
#         if not evn.TMVA_SS_aftercut_BDT>-0.1455: continue
#         for var in variables:
#             #exec('histo_saver["'+var+key+'"].Fill(evn.'+var+')')
#             histo_saver[var+key].Fill(getattr(evn,var))

weights_leg = weights.copy()
# Create a leg with dummy histos
cleg = TCanvas("cleg","cleg", 600, 600)
leg = TLegend(0.1,0.1,0.9,0.9)
leg.SetTextSize(0.035)
saveh = []
for key in filename.keys():
    if key is not "data12":
        if key is not "data12SS":
            weights_leg[key] = weights_leg[key] * float(dic_forleg[key])
            print key, dic_forleg[key] * weights_leg[key]

weight_sorted_leg = sorted(weights_leg.items(), key=operator.itemgetter(1), reverse = True)
for na,val in weight_sorted_leg:
    if na in filename.keys():
        nar = na[na.find("_")+1:]
        h = TH1F(nar,nar,100,0,1)
        h.SetLineColor(colorlist[na])
        h.SetLineWidth(4)
        h.SetMarkerColor(colorlist[na])
        h.SetMarkerSize(4)
        saveh.append(h)
        if "data1" in na:
            leg.AddEntry(h, nar+":  "+str(weights_leg[na]), "lep")
        elif "data1SS" in na:
            pass
        else:
            leg.AddEntry(h, nar+":  "+str(weights_leg[na]), "l")
leg.Draw()
cleg.SaveAs("leg_stack"+tosavedifferent+".pdf")

weight_sorted_leg = sorted(weights_leg.items(), key=operator.itemgetter(1), reverse = True)
for var in variables.keys():
    stack_dict[var].Add(histo_saver[var+"13512010_Bs_Kmunu_"])
for key,value in weight_sorted_leg:
    if(key not in not_in_stack):
        if key in filename.keys():
            if key is not "13512010_Bs_Kmunu_":
                for var in variables.keys():
                    stack_dict[var].Add(histo_saver[var+key])


# plot the histograms
for var in variables.keys():            
    c = TCanvas("cStack"+var,"cStack"+var)
    print "data12", histo_saver[var+"data12"].Draw("e")
    stack_dict[var].Draw("sameh")
    c.Update()
    c.SaveAs("StackPlot_"+var+tosavedifferent+".pdf")
    canvasall.append(c)


k = input("press a number and enter to exit")

# Write canvas and leg in output file
fout.cd()
for c in canvasall:
    c.Write()
cleg.Write()

fout.Write()
fout.Close()
print "outputfile ", fout.GetName(), " created which contain all Canvas"





































