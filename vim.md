# YouCompleteMe (YCM)

## what is YCM

Search for it on youtube ;)

It is autocompletion through clang for vim. I.e.

```c++
TFile* f;
f->
```

will cause a list of methods of TFile to pop up (including their arguments).

YCM also embeds compiler warnings (usage of uninitialised variable,…) in vim.

## install YCM

### operating systems which have YCM (might require root privileges)

Possibly your operating system comes with YCM. In which case I recommend [this
SO post instead of the official
documentation](http://stackoverflow.com/a/34032417).

What I did was

```shell
sudo apt-get install vim vim-youcompleteme
vam install youcompleteme
```

I skipped the configuration in `.vimrc` (just worked out of the box).

Also copying the `.ycm_extra_conf.py` file might not be needed because
[`.ycm_extra_conf.py`](utils/Trimmer/.ycm_extra_conf.py) is provided within the
project.

### Rogue installation

Haven't tried it yet, but the natural starting point should be the official
[installation guide](https://github.com/Valloric/YouCompleteMe#installation).
