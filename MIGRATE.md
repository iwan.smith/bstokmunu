# obtain authors

get all committers
http://stackoverflow.com/a/3972103
```
svn log --xml | grep author | sort -u | perl -pe 's/.*>(.*?)<.*/$1 = /' | tee authors.txt
```

clear their names
```
for v in `cat authors.txt | sed "s/=//" ; do phonebook --terse "displayname" --terse "email" $v | sed "s/;/ </" | sed "s/;$/>/" >> authors.txt; done
```

# check out

```
git svn clone -A authors.txt svn+ssh://svn.cern.ch/reps/lhcb/Urania/trunk/Phys/BsToKMuNu/ BsToKMuNu.git
cd BsToKMuNu.git
git filter-branch -f --index-filter 'git rm --cached --ignore-unmatch CMakeLists.txt' HEAD 
git filter-branch -f --index-filter 'git rm --cached --ignore-unmatch cmt' HEAD 
git filter-branch -f --index-filter 'git rm -r --cached --ignore-unmatch cmt' HEAD 
git filter-branch -f --index-filter 'git rm -r --cached --ignore-unmatch doc' HEAD 
git filter-branch -f --index-filter 'git rm -r --cached --ignore-unmatch src' HEAD 
git filter-branch -f --index-filter 'git rm -r --cached --ignore-unmatch -- scripts/Bs2Kmunu_TupleMaker_MC_NEW.py scripts/Bs2Kmunu_TupleMaker_MC.py scripts/Bs_TupleMaker_data.py scripts/Bs_TupleMaker_MC_both_plusRestripping.py scripts/Bs_TupleMaker_MC_control.py scripts/Bs_TupleMaker_MC_LooseStrp.py scripts/Bs_TupleMaker.py scripts/Bs_TupleMaker_tmp.py scripts/Control_TupleMaker_data.py scripts/gangaSubmit.py scripts/gangaSunbmit_Bs2KmuNu.py scripts/newGanga.py scripts/RunOnGanga.py' HEAD
git filter-branch --prune-empty -f
gitk &
git filter-branch -f --index-filter 'git rm -r --cached --ignore-unmatch -- scriptsBs2JpsiPhi_TupleMaker_MC.py' HEAD
git filter-branch -f --index-filter 'git rm -r --cached --ignore-unmatch -- scripts/Bs2JpsiPhi_TupleMaker_MC.py scripts/Bs2JpsiPhi_TupleMaker_data.py scripts/Bu2JpsiK_TupleMaker_MC.py scripts/Bu2JpsiK_TupleMaker_data.py ' HEAD
git filter-branch --prune-empty -f
git filter-branch -f --index-filter 'git rm -r --cached --ignore-unmatch -- scripts/Bs_TupleMaker_MC_both.py scripts/Bs_TupleMaker_MC_both.py' HEAD
git filter-branch --prune-empty -f
```

