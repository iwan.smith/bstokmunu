/*

This simple script is designed to fit the data using the following assumption:
Pdf = Signal(MC) + SS background (from SS stripping line in data) + OS background (from anti-isolation sample in data)   
 */

//#include "/home/khanji/roofitinclude.h"
#include <iomanip>
#include "Math/Vector4D.h"
#include "TFractionFitter.h"
#include "TH1F.h"
#include "THStack.h"
#include "TFile.h"
#include "TROOT.h"
#include "TCanvas.h"


using std::min;
using std::abs;

using namespace std;
using namespace RooFit;
using namespace ROOT::Math;

int Fitter()
{
  
  TFile *data_file = new TFile("./data_h.root") ;
  TH1F *data_h = (TH1F*)data_file ->Get("data");
  //data_h->SetLineColor(kBlack);
  
  TFile *data_SS_file = new TFile("./SS_h.root") ;
  TH1F *data_SS_h = (TH1F*) data_SS_file->Get("data_SS");
  data_SS_h->SetLineColor(kBlue+2);
  
  TFile *Signal_file = new TFile("./MC_h.root") ;
  TH1F *Signal_MC_h = (TH1F*) Signal_file->Get("MC_signal");
  Signal_MC_h->SetLineColor(kGreen+2);
  
  TFile *antiiso_file = new TFile("./data_antiiso.root") ;
  TH1F *data_antiiso_h = (TH1F*) antiiso_file->Get("data_antiiso");
  data_antiiso_h->SetLineColor(kRed);
  
  TCanvas *c_compare = new TCanvas("c_compare" , "c_compare");
  data_SS_h->DrawNormalized();
  data_antiiso_h->DrawNormalized("same");
  Signal_MC_h->DrawNormalized("same");
  data_h->DrawNormalized("same");
  
  TObjArray *mc = new TObjArray(2);
  mc->Add(Signal_MC_h);
  mc->Add(data_antiiso_h);
  mc->Add(data_SS_h);
  
  
  TFractionFitter* fit = new TFractionFitter(data_h, mc);
  //fit->Constrain(1, 0.0 , 1.0);  
  //fit->SetRangeX(10,100); 
  Int_t status = fit->Fit();
  std::cout << "fit status: " << status << std::endl;
  double FitPar[3], FitParErr[3];
  if (status != 0)  {
    cout << "Fit status is not 0 -> exiting\n\n";
  }
  else {  for (Int_t i = 0; i < 3 ; ++i) {
      fit->GetResult(i, FitPar[i], FitParErr[i]); }}
  
  // Make a stack :
  TH1F* mcSig_scaled = (TH1F*)Signal_MC_h ->Clone();
  mcSig_scaled->Scale(FitPar[0]* data_h->Integral()/Signal_MC_h->Integral());
  TH1F* data_antiiso_scaled = (TH1F*)data_antiiso_h ->Clone();
  data_antiiso_scaled->Scale(FitPar[1]* data_h->Integral()/data_antiiso_h->Integral());
  TH1F* data_SS_scaled = (TH1F*) data_SS_h->Clone();
  data_SS_scaled->Scale(FitPar[2]* data_h->Integral()/data_SS_h->Integral());
  
  /*
    THStack* stack_kmu_histos = new THStack("stack_kmu_histos", "");
  stack_kmu_histos->Add(mcSig_scaled);
  stack_kmu_histos->Add(data_antiiso_scaled);
  stack_kmu_histos->Add(data_SS_scaled);
  stack_kmu_histos->SetTitle("Kmunu Histos; Kmunu Histos ; events");
  */
  TCanvas *c_fit = new TCanvas("c_fit" , "c_fit");
  if (status == 0) { 
    TH1F* result = (TH1F*) fit->GetPlot();
    //stack_kmu_histos->Draw();
    data_h->Draw("Ep");
    mcSig_scaled->Draw("same"); 
    data_antiiso_scaled->Draw("same");
    data_SS_scaled->Draw("same");
    
    result->Draw("same");
  }
  
  cout<< "  ==================++++++++++++++++ "<<endl;
  cout<<  "Chi2 of the Fit is = " <<fit->GetChisquare() <<endl;
  cout<<  "NDOF of the Fit is = " <<fit->GetNDF()  <<endl;
  cout<<  "Chi2/NDOF of the Fit is = " <<fit->GetChisquare()/fit->GetNDF()  <<endl;
  cout<< "  ==================++++++++++++++++ "<<endl;
  return 0;
}




