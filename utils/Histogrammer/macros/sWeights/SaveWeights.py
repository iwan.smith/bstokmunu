


import argparse

parser = argparse.ArgumentParser(description='Fit and sWeight the B invariant mass distribution.')
parser.add_argument("--JpsiPhi",   action="store_true", help="Run over the JPsiPhi sample")
parser.add_argument("--JpsiK",     action="store_true", help="Run over the JPsiK sample")
parser.add_argument("--JpsiK_Iso",     action="store_true", help="Run over the KMuNu sample and reconstruct as JpsiK")
parser.add_argument("--DsMuNu",    action="store_true", help="Run over the JPsiK sample")
parser.add_argument("--SavePlots", action="store_true", help="Save plots as pdfs")

import sys


if len(sys.argv) == 1:
    parser.print_help()
    exit(1)

args = parser.parse_args()

if ( args.JpsiPhi + args.JpsiK + args.DsMuNu > 1 ):
    print "Please only select one of JpsiPhi and JpsiK"
    exit(1)

if ( args.JpsiPhi + args.JpsiK + args.DsMuNu > 1 ):
    print "You Must Select one of JpsiPhi and JpsiK"
    exit(1)



import ROOT
import ROOT.RooFit as RF
from ROOT import gROOT

gROOT.SetBatch(True)



if args.JpsiPhi:
    filename = "root://eoslhcb.cern.ch//eos/lhcb/wg/semileptonic/Bs2KmunuAna/Histogrammer/Output/Histograms.root"
    HistPath = "Nominal/JPsiPhi/Data/Trigger_TOS/h_B_MM"
    NameKey  = "JpsiPhi"

if args.JpsiK:
    filename = "root://eoslhcb.cern.ch//eos/lhcb/wg/semileptonic/Bs2KmunuAna/Histogrammer/Output/Histograms.root"
    HistPath = "Nominal/JPsiK/Data/Trigger_TOS/h_B_MM"
    NameKey  = "JpsiK"

if args.JpsiK_Iso:
    filename = "root://eoslhcb.cern.ch//eos/lhcb/wg/semileptonic/Bs2KmunuAna/Histogrammer/Output/Histograms.root"
    HistPath = "Nominal/KMuNu/Data/Is_Q2_NoBDT/h_Bu_MM"
    NameKey  = "JpsiK_Iso"

if args.DsMuNu:
    filename = "root://eoslhcb.cern.ch//eos/lhcb/wg/semileptonic/Bs2KmunuAna/Histogrammer/Output/Histograms.root"
    HistPath = "Nominal/DsMuNu/Data/PID_Tight/h_Ds_MM"
    NameKey  = "DsMuNu"



"""
Load Histogram for the fit
"""

f_Hist = ROOT.TFile.Open(filename)
Hist_B_MM = f_Hist.Get(HistPath)

print f_Hist, Hist_B_MM

"""
Define more variables for the job
"""
nbins  = Hist_B_MM.GetNbinsX()
xlow   = Hist_B_MM.GetBinLowEdge(1)
xhigh  = Hist_B_MM.GetBinLowEdge(nbins+1)

sigma  = Hist_B_MM.GetStdDev()

mu     = Hist_B_MM.GetMean()
mulow  = mu-sigma
muhigh = mu+sigma

nev    = Hist_B_MM.Integral()
nsig   = nev*0.95
nbkg   = nev*0.05

"""
Setup Workspace, Perform fit, plot results
"""

w = ROOT.RooWorkspace()
w.factory('Gaussian::g1(x[{xlow}, {xhigh}],mu[{mu}, {mulow}, {muhigh}],sigma [5  ,15])'.format( xlow=xlow, xhigh=xhigh, mu=mu, mulow=mulow, muhigh=muhigh) )
w.factory('Gaussian::g2(x           ,mu                               ,sigma2[10, 30])')
w.factory('SUM::g(f1[0.5,0,1]*g1,f2[0.5,0,1]*g2)')

w.factory('Exponential::e(x,tau[-.0045,-0.02, 0.02])')
w.factory('SUM::model(s[{nsig}, 0, {nev}]*g, b[{nbkg}, 0, {nev}]*e)'.format(nev=nev, nsig=nsig, nbkg=nbkg))

x = w.var('x')
pdf = w.pdf('model')
data = ROOT.RooDataHist("Hist_Data", "Data Histogram", ROOT.RooArgList(x), Hist_B_MM)

fitResult = pdf.fitTo(data,ROOT.RooFit.Save())




"""
Make some toy data and get the sWeight Results
"""

SampleData = pdf.generate(ROOT.RooArgSet(x)) # This is a quirk of RooFit
sData = ROOT.RooStats.SPlot("sData","An SPlot", SampleData, pdf, ROOT.RooArgList(w.var("s"),w.var("b")) )

nSample = SampleData.sumEntries()
"""
Fill a TProfile with the B mass and sWeights
"""


p1 = ROOT.TProfile("p1", "p1", 1000, xlow, xhigh)
p2 = ROOT.TProfile("p2", "p2", 1000, xlow, xhigh)


for ev in range(int(nSample)):
    entry = SampleData.get(ev)
    v_x = entry.getRealValue("x"),
    v_y = entry.getRealValue("s_sw")
    v_y2 = entry.getRealValue("b_sw")
    
    p1.Fill(v_x[0], v_y)
    p2.Fill(v_x[0], v_y2)
    
    
"""
Convert the TProfiles to a Histogram and Tidy up empty bins
"""
h1=p1.ProjectionX()
h2=p2.ProjectionX()

h1.SetTitle("Signal sWeights")
h2.SetTitle("Background sWeights")

h1.GetXaxis().SetTitle("B invariant mass")
h1.SetName("nsig_sw")
h2.SetName("nbkg_sw")

h1.SetMaximum( max(h1.GetMaximum(), h2.GetMaximum())*1.05 )
h1.SetMinimum( min(h1.GetMinimum(), h2.GetMinimum())*1.05 )

h1.Sumw2()
h2.Sumw2()

for h in [h1, h2]:
    if h.GetBinContent(1) == 0:
        BC = 0
        it = 1
        while BC == 0:
            BC = h.GetBinContent(1+it)
            it+=1
        h.SetBinContent(1, BC)
        h.SetBinError  (1, 0.0)

    if h.GetBinContent(1000) == 0:
        BC = 0
        it = 1
        while BC == 0:
            BC = h.GetBinContent(1000-it)
            it+=1
        h.SetBinContent(1000, BC)
        h.SetBinError  (1000, 0.0)
    
    for bin in range(2, 1000):
        if h.GetBinContent(bin) == 0:
            BinContent = h.GetBinContent(bin-1)
            for it in range (100):
                BC = h.GetBinContent(bin+1+it)
                if BC != 0:
                    BinContent += BC
                    break
            BinContent /= 2
            h.SetBinContent(bin, BinContent)
            h.SetBinError  (bin, 0.0)



f_out = ROOT.TFile.Open("sWeights_{Key}.root".format(Key=NameKey), "RECREATE")
h1.ClearUnderflowAndOverflow()
h1.Write()
h2.ClearUnderflowAndOverflow()
h2.Write()
f_out.Close()


if not args.SavePlots:
    exit(0)
    

"""
Draw And save the fit plot
"""
    
gROOT.ProcessLine(".x ../../.lhcbstyle.C")
ROOT.gStyle.SetOptStat(0)

frame = x.frame()
frame.SetTitle("Fit to B invariant mass")

data.plotOn(frame)

pdf.plotOn( frame, RF.Components("g"), RF.LineStyle(2), RF.LineColor(2))
pdf.plotOn( frame, RF.Components("e"), RF.LineStyle(2), RF.LineColor(3))
pdf.plotOn( frame )

hresid = frame.pullHist()
frame2 = x.frame()
frame2.SetTitle("Fit Residuals")
frame2.addPlotable(hresid,"P") 

c1 = ROOT.TCanvas("c1", "c1", 900, 900)
c1.Divide(1,2)
c1.SetLogy()
c1.cd(1).SetLogy()
frame.Draw()
c1.cd(2)
frame2.Draw()


c1.Print("output/Fit_{Key}.pdf".format(Key=NameKey))


c1.Clear()
c1.cd()
c1.SetLogy(False)

h1.Draw()

h2.SetLineColor(2)
h2.Draw("SAME")

c1.Print("output/sWeights_{Key}.pdf".format(Key=NameKey))

