import ROOT


HistFile = ROOT.TFile.Open("/eos/lhcb/wg/semileptonic/Bs2KmunuAna/Histogrammer/Output/Histograms.root")

c1 = ROOT.TCanvas("c1", "c1", 1600, 1600)


ROOT.gStyle.SetOptStat(0)

for mode in ["K", "Ds"]:
    
    c1.Print("GeneratorCuts_{0}.pdf[".format(mode))

    Particles = ["Kminus", "muplus"]
    Folder = "Nominal/{0}MuNu/".format(mode)
    
    if mode == "Ds":
        Particles += ["D_sminus", "Kplus", "piminus"]
    
    # Plot the Theta distributions of particles
    
    for P in Particles:
        
        print Folder + "MCDecay_NoGenCut/Nominal/{0}_theta".format(P)
        print Folder + "MCDecay_NoGenCut/GenCut/{0}_theta".format(P)
        print Folder + "MCDecay/Nominal/{0}_theta".format(P)
        
        h_Before = HistFile.Get(Folder + "MCDecay_NoGenCut/Nominal/{0}_theta".format(P))
        h_After  = HistFile.Get(Folder + "MCDecay_NoGenCut/GenCut/{0}_theta".format(P))
        h_MC     = HistFile.Get(Folder + "MCDecay/Nominal/{0}_theta".format(P))
      
        h_Before.Scale(1.0/h_After.Integral())
        h_After .Scale(1.0/h_After.Integral())
        h_MC    .Scale(1.0/h_MC.Integral() )

        h_Before.Sumw2(False)
        h_After .Sumw2(False)
        h_MC    .Sumw2(False)
        
        h_MC.SetLineColor(2)
        
        c1.Clear()
        
        h_MC.SetMaximum(h_Before.GetMaximum()*1.1)
        h_MC.Draw()
        h_After.Draw("SAME")
        h_Before.Draw("SAME")
        
        
        c1.Print("GeneratorCuts_{0}.pdf".format(mode))
        
        h_Before = HistFile.Get(Folder + "MCDecay_NoGenCut/Nominal/{0}_theta2".format(P))
        h_After  = HistFile.Get(Folder + "MCDecay_NoGenCut/GenCut/{0}_theta2".format(P))
        h_MC     = HistFile.Get(Folder + "MCDecay/Nominal/{0}_theta2".format(P))

        h_Before.Scale(1.0/h_After.Integral())
        h_After .Scale(1.0/h_After.Integral())       
        h_MC    .Scale(1.0/h_MC.Integral() )

        h_MC.Divide(h_After);

        h_MC.SetMaximum(1.2)
        h_MC.SetMinimum(0.8)
        
        h_MC.Fit(ROOT.TF1("LinearFunction", "[0] + [1]*x"))
        
        h_MC.Draw();
        c1.Print("GeneratorCuts_{0}.pdf".format(mode))
        
        
        
        
    # Plot the Q^2 efficiencies
    
    Q2Before = HistFile.Get(Folder + "MCDecay_NoGenCut/Nominal/True_Q2")
    Q2After  = HistFile.Get(Folder + "MCDecay_NoGenCut/GenCut/True_Q2")

    Q2After.Sumw2()
    Q2Before.Sumw2()
    
    Scaling = (99818.0 if mode == "Ds" else 99837.0) / 200000.0
    
    Q2After.Divide(Q2After, Q2Before, Scaling, 1, "B")
    
    c1.Clear()
    Q2After.SetTitle(";q^{2} [GeV^{2}];#varepsilon")
    Q2After.SetMinimum(0.15)
    Q2After.Draw();
    
    c1.Print("GeneratorCuts_{0}_qsq.pdf".format(mode))
    
    c1.Print("GeneratorCuts_{0}.pdf]".format(mode))

HistFile.Close()

HistFile = ROOT.TFile.Open("../Histograms.root")

for mode in ["K", "Ds"]:
    # Get the efficiencies:
    
    Folder = "Nominal/{0}MuNu/".format(mode)
    h_Before = HistFile.Get(Folder + "MCDecay_NoGenCut/Nominal/B_s0_theta")
    h_After = HistFile.Get(Folder + "MCDecay_NoGenCut/GenCut/B_s0_theta")
    
    allEv = h_Before.Integral()
    PassEv = h_After.Integral()
    
    #PassEv *= (99818.0 if mode == "Ds" else 99837.0) / 200000.0
    
    efficiency = PassEv/allEv
    
    import math
    sigma = math.sqrt(allEv*efficiency*(1-efficiency)) / allEv
    
    print "\n"
    print "Decay Mode: {0}".format(mode)
    print "Total Events: {0}".format(allEv)
    print "Passed Events: {0}".format(PassEv)
    print "Efficiency: {0} +/- {1}".format(efficiency*50, sigma*50)
    
    
    
    
