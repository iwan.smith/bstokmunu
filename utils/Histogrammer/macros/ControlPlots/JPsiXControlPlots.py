#!/usr/bin/env python
"""@package Control Plotting
This script will generate control plots

It will use the Histogrammer output taken from eos as default. 
"""



# Read the options from command line
import argparse

parser = argparse.ArgumentParser(description='Generate plots from out control samples.')

parser.add_argument("--Path", "-P", default="/eos/lhcb/wg/semileptonic/Bs2KmunuAna/Histogrammer/Output/Histograms.root", help="Specify output location of Histogrammer." )
parser.add_argument("--Mode", "-M", default="JPsiK", choices=["JPsiK" ,"JPsiPhi"], help="Mode to run over")

args = parser.parse_args()



from ROOT import gROOT
gROOT.SetBatch(True)
import ROOT, os, numpy as np, sys
gROOT.ProcessLine(".x "+os.environ["BSTOKMUNUROOT"]+"/include/official.C")

input = ROOT.TFile(args.Path)
Mode  = args.Mode


plotPrefix = "{Mode}_ControlPlots__".format(Mode=Mode)


# list of systematic uncertainties for consideration
systematics = (
   "Nominal",
#   "sWeight",
#   "sWeight_BG",
   "PID_K_m",
   "PID_Mu_m",
   "PID_Mu_p",
   "Tracking_K_m",
   "Tracking_Mu_p",
   "Tracking_Mu_m",
   "RW_nTracks",)

# list of (variable,xtitle) for plotting
variables = (
    ("h_B_ETA2","#it{B} #eta"),
    ("h_B_FD_OWNPV","#it{B} flight distance from PV [mm]"),
    ("h_B_IPCHI2_OWNPV","#it{B} #chi^{2}_{IP} w.r.t. PV"),
    ("h_B_IP_OWNPV","#it{B} IP w.r.t. PV [mm]"),
    ("h_B_MM","#it{B} mass [MeV]"),
    ("h_B_VCHI2_LOKI","#it{B} LoKi vertex fit #chi^{2}"),
    ("h_nTracks3","Track multiplicity"))

if Mode == "JPsiPhi":
    systematics += (
        "PID_K_p",
        "Tracking_K_p",  
    )
    variables += ()


inapplicableSystematics = []

# loop over variables
for (variable,xtitle) in variables:
    
    c = ROOT.TCanvas("c1", "c1", 1400, 940)
    
    histData    = input.Get("sWeight/{Mode}/Data/Nominal/{Var}".format(Mode=Mode, Var=variable))
    histNominal = input.Get("Default/{Mode}/Signal_MC/Nominal/{Var}".format(Mode=Mode, Var=variable))
    histNominal.Scale( histData.Integral()/histNominal.Integral() )
    # collection of MC histograms with different systematcs
    histosMC = {}

    # get the MC histograms for each systematic
    for systematic in systematics:
        path = "{Syst}/{Mode}/Signal_MC/Nominal/{Var}".format(Syst=systematic, Mode=Mode, Var=variable)
        histMC = input.Get(path)

        if not histMC: 
            if not systematic in inapplicableSystematics:
                inapplicableSystematics.append(systematic)
            continue
        if histMC.Integral <= 0.: continue
        histMC.Scale(histData.Integral()/histMC.Integral())
        ROOT.SetOwnership(histMC,False)
        histosMC[systematic] = histMC

    # make the error envelope histogram
    histBand = histNominal.Clone(variable+"BAND")
    for iBin in range(1,histBand.GetNbinsX()+1):
        bins = [ (hist.GetBinContent(iBin) - histosMC["Nominal"].GetBinContent(iBin))**2 for hist in histosMC.values() if hist.Integral() > 0. ]
        #histBand.SetBinContent( iBin,  np.mean(bins) )
        histBand.SetBinError(iBin,np.sqrt(np.sum(bins)))


    
    # now we have everything that we need to plot
    histData.SetMinimum(0)
    histData.Draw("E")
    histData.SetXTitle(xtitle)
    #histNominal = histosMC["Nominal"]
    histNominal.SetLineColor(ROOT.kAzure)
    histBand.SetFillColor(ROOT.kAzure+1)
    histBand.SetLineColor(ROOT.kAzure+1)
    histBand.SetMarkerSize(0)
    histBand.SetMarkerColor(ROOT.kAzure+1)
    histBand.Draw("SAME E2")
    histNominal.Draw("SAME HIST")
    histData.Draw("SAME E")
    c.Print("%s%s.png"%(plotPrefix,variable))
    c.Print("%s%s.pdf"%(plotPrefix,variable))


print 'inapplicableSystematics:',inapplicableSystematics

