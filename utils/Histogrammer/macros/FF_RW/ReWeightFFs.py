import ROOT
import numpy as np

f_FF_K      = ROOT.TFile.Open("../../../FormFactors/FF_K.root")
f_MC_KMuNu  = ROOT.TFile.Open("/eos/lhcb/wg/semileptonic/Bs2KmunuAna/Tuples/MCDecay_NoGenCut_Aug17/Bs_MCDecayTree_NoGenCut_13512010_Sim08av2.root")
f_FF_RW     = ROOT.TFile.Open("FF_RW_K.root", "RECREATE") 

T_KMuNu = f_MC_KMuNu.Get("MCDecayTreeTuple_K/MCDecayTree")

for FF in ["FF_Witzel", "FF_Bouchard", "FF_Rusov"]:
    g_FF = f_FF_K.Get(FF)
    
    h_nBin = g_FF.GetN()
    h_MinX = g_FF.GetXaxis().GetXmin()*1e6
    h_MaxX = g_FF.GetXaxis().GetXmax()*1e6
    
    
    h_FF_MC    = ROOT.TH1F("h_FF",       "h_FF", h_nBin, h_MinX, h_MaxX)
    h_FF_Model = ROOT.TH1F("h_FF_Model", "h_FF", h_nBin, h_MinX, h_MaxX)
    h_FF_Ratio = ROOT.TH1F("h_FF_Ratio", "h_FF", h_nBin, h_MinX, h_MaxX)
    
    #h_FF_MC.Sumw2()
    T_KMuNu.Draw("((B_s0_TRUEP_E - Kminus_TRUEP_E)**2  - (B_s0_TRUEP_X - Kminus_TRUEP_X)**2 - (B_s0_TRUEP_Y - Kminus_TRUEP_Y)**2 - (B_s0_TRUEP_Z - Kminus_TRUEP_Z)**2) >> h_FF")
    
    
    FFBins = np.frombuffer( g_FF.GetY(), count=h_nBin )
    
    for i, dGamma in enumerate(FFBins):
        h_FF_Model.SetBinContent(i+1, dGamma)
    
        
    h_FF_MC   .Rebin(10)
    h_FF_Model.Rebin(10)
    h_FF_Ratio.Rebin(10)
    
    h_FF_MC   .Scale(1.0/h_FF_MC   .Integral())
    h_FF_Model.Scale(1.0/h_FF_Model.Integral())
    
    h_FF_Ratio.Divide(h_FF_Model, h_FF_MC)
    
    
    for i in range(1, h_nBin+1):
        if ( h_FF_Ratio.GetBinContent(i) > 10):
                h_FF_Ratio.SetBinContent(i, 10)
        
    
    f_FF_RW.cd()
    h_FF_Model.SetName(FF + "_Model")
    h_FF_Ratio.SetName(FF + "_Ratio")
    
    h_FF_Model.Write()
    h_FF_Ratio.Write()
    
    
    c1 = ROOT.TCanvas("c1", "c1", 450, 900)
    c1.Divide(1,3)
    
    c1.cd(1)
    h_FF_MC    .Draw()
    
    c1.cd(2)
    h_FF_Model .Draw()
    
    c1.cd(3)
    h_FF_Ratio .Draw()
    
    c1.Print(FF+".pdf")
    

f_FF_RW.Close()
