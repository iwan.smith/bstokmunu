"""@package CalculateVub

Documentation for the calculation of Vub

This code takes the output of the histogrammer, will run the fits and return barnching fractions and Vub

Efficiencies from each function are returned as a dictionary:

The Ratio is KMuNu/DsMuNu

"""

class VubResult:
    
    Yield    = None
    Yield_Pr = None
    Yield_FF = None
    FF       = None
    
    
    Eff_Sel  = None
    Eff_Gen  = None
    
    Vub      = None
    
    K_BinMig = None
    
    
from VubCalculationLib import *
import ROOT
import os

def CalculateVub( 
    KMuNuFileName  = "KMuNuYields_High_Q2.txt",
    DsMuNuFileName = "DsMuNuYields.txt",
    
    Cut_K_Fit      = "%s_Q2",
    Cut_Ds_Fit     = "PID_Tight",

    Cut_K          = "High_Q2",
    Cut_Ds         = "PID_Tight",
    
    Cut_K_Q2       = "High_Q2",
    Cut_Ds_Q2      = "Nominal",
    
    FF_Model_K     = "Witzel",
    FF_Model_Ds    = "Bailey",
    
    Q2Cut_Signal   = (7, 25),
    Q2Cut_Norm     = (0, 14),
    
    Blinding       = False,
    
    filename       = "/eos/lhcb/wg/semileptonic/Bs2KmunuAna/Histogrammer/Output/Histograms.root" 
    ):
    ###############################################################################
    # Run the Fits
    
    OutDir = "$BSTOKMUNUROOT/utils/Histogrammer/macros/CalculateVub/Out_{}".format(Cut_K)

    os.system("mkdir {} 2>/dev/null".format(OutDir))
    
    print "Running signal fit..."
    # This command:    
    #  o) cd into Fit Directory
    #  o) If the fitter binary is newer than output text file:
    #    o) Then run the fit
    os.system("cd $BSTOKMUNUROOT/utils/Fitter_Control; "\
              "[ bin/Fitter_Signal_2D -nt {FitOutput} ] && " \
              "./bin/Fitter_Signal_2D --nToys=0 --Cut={Cut}"\
              " > {Output}/FitLogSignal.cout 2> {Output}/FitLogSignal.cerr"\
              .format(FitOutput=KMuNuFileName, Cut=Cut_K_Fit, Output=OutDir)
              )
    
    print "Running normalisation fit..."
    os.system("cd $BSTOKMUNUROOT/utils/Fitter_Control; "\
              "[ bin/Fitter_Control -nt {FitOutput} ] && " \
              "./bin/Fitter_Control --nToys=0 --Cut={Cut}"\
              " > {Output}/FitLogControl.cout 2> {Output}/FitLogControl.cerr"\
              .format(FitOutput=DsMuNuFileName, Cut=Cut_Ds_Fit, Output=OutDir))
    
    f_hist = None
    
    if filename[0:4] == "/eos":
        f_hist = ROOT.TFile.Open("root://eoslhcb.cern.ch/" + filename)
    
    if not f_hist:
        f_hist = ROOT.TFile.Open(filename)
    
    
    ###############################################################################
    
            
    
    Yields = GetFitYields(KMuNuFileName, DsMuNuFileName)
    print Yields
    
    BFs = GetBranchingFractions()
    print BFs
    
    
    FFs = GetFormFactors(Q2Cut_Signal, Q2Cut_Norm, FF_Model_K, FF_Model_Ds)
    print FFs
    
    EffGen = GetGeneratorEfficiency(f_hist, Q2Cut_Signal, Q2Cut_Norm )
    print EffGen
    
    EffSel = GetEfficiency(f_hist, Cut_K, Cut_Ds, Cut_K_Q2, Cut_Ds_Q2)
    print EffSel
    
    
    CorrTrack = GetRatio(f_hist, "RW_Kin", "Tracking_Comb", Cut_K, Cut_Ds, "Tracking Correction", BinomialError = False, Blinding = False )
    print CorrTrack
    
    CorrPID = GetRatio(f_hist, "RW_Kin", "PID_Comb", Cut_K, Cut_Ds, "PID Correction", BinomialError = False, Blinding = False )
    print CorrPID
    
    
    Corr_BinMigration = GetBinMigration("Default", *Q2Cut_Signal)
    print Corr_BinMigration
    
    TotEv = Yields / (BFs * EffGen * EffSel)  
    TotEv.name = "Yields at Production, i.e. Branching Fraction"
    print TotEv
    
    TotEvFF = Yields / (BFs * EffGen * EffSel * FFs)  
    TotEvFF.name = "Yields with Form Factors, i.e. Vub^2 / Vcb^2"
    print TotEvFF
    
    import math
    print ("\nVub/Vcb = {0}".format(math.sqrt(TotEvFF.Ratio[0])))
    
    Vub     = math.sqrt(TotEvFF.Ratio[0])                           
    Vub_err = 0.5 * TotEvFF.Ratio[1] / math.sqrt(TotEvFF.Ratio[0])


    Results = VubResult()
    
    Results.Yield    = Yields
    Results.Yield_Pr = TotEv 
    Results.Yield_FF = TotEvFF 
    Results.FF       = FFs
    Results.Eff_Gen  = EffGen
    Results.Eff_Sel  = EffSel
    Results.Vub      = (Vub, Vub_err)
    Results.K_BinMig = Corr_BinMigration.KMuNu
    return Results
    
    



