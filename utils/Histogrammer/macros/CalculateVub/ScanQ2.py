from GetCalculationLib import CalculateVub
import ROOT, os
ROOT.gROOT.ProcessLine(".x "+os.environ["BSTOKMUNUROOT"]+"/include/official.C")
ROOT.gROOT.SetBatch(True)

Results_High_Bouchard = []

for Q2Bin in ["02","04","06","08","10","12","14","16"]:
    
    R = CalculateVub( 
        KMuNuFileName  = "KMuNuYields_Q2_{}_High.txt".format(Q2Bin),
        DsMuNuFileName = "DsMuNuYields.txt",
        
        Cut_K_Fit      = "Q2_{}_%s".format(Q2Bin),
        Cut_Ds_Fit     = "PID_Tight",
    
        Cut_K          = "Q2_{}_High".format(Q2Bin),
        Cut_Ds         = "PID_Tight",
        
        Cut_K_Q2       = "Q2_{}_High".format(Q2Bin),# ToDo
        Cut_Ds_Q2      = "Nominal",
        
        FF_Model_K     = "Bouchard",
        FF_Model_Ds    = "Bailey",
        
        Q2Cut_Signal   = (int(Q2Bin), 25),
        Q2Cut_Norm     = (0, 14),
        
        Blinding       = False,
        
        filename       = "/eos/lhcb/wg/semileptonic/Bs2KmunuAna/Histogrammer/Output/Histograms.root" 
        #filename       = "../../Histograms.root" 
        )
    
    
    Results_High_Bouchard += [R]

Results_High_Witzel = []

for Q2Bin in ["02","04","06","08","10","12","14","16"]:
    
    R = CalculateVub( 
        KMuNuFileName  = "KMuNuYields_Q2_{}_High.txt".format(Q2Bin),
        DsMuNuFileName = "DsMuNuYields.txt",
        
        Cut_K_Fit      = "Q2_{}_%s".format(Q2Bin),
        Cut_Ds_Fit     = "PID_Tight",
    
        Cut_K          = "Q2_{}_High".format(Q2Bin),
        Cut_Ds         = "PID_Tight",
        
        Cut_K_Q2       = "Q2_{}_High".format(Q2Bin),# ToDo
        Cut_Ds_Q2      = "Nominal",
        
        FF_Model_K     = "Witzel",
        FF_Model_Ds    = "Bailey",
        
        Q2Cut_Signal   = (int(Q2Bin), 25),
        Q2Cut_Norm     = (0, 14),
        
        Blinding       = False,
        
        filename       = "/eos/lhcb/wg/semileptonic/Bs2KmunuAna/Histogrammer/Output/Histograms.root" 
        #filename       = "../../Histograms.root" 
        )
    
    Results_High_Witzel += [R]

"""
Results_High_WitzelBouchard = []

for Q2Bin in ["02","04","06","08","10","12","14","16"]:
    
    R = CalculateVub( 
        KMuNuFileName  = "KMuNuYields_Q2_{}_High.txt".format(Q2Bin),
        DsMuNuFileName = "DsMuNuYields.txt",
        
        Cut_K_Fit      = "Q2_{}_%s".format(Q2Bin),
        Cut_Ds_Fit     = "PID_Tight",
    
        Cut_K          = "Q2_{}_High".format(Q2Bin),
        Cut_Ds         = "PID_Tight",
        
        Cut_K_Q2       = "Q2_{}_High".format(Q2Bin),# ToDo
        Cut_Ds_Q2      = "Nominal",
        
        FF_Model_K     = "WitzelBouchard",
        FF_Model_Ds    = "Bailey",
        
        Q2Cut_Signal   = (int(Q2Bin), 25),
        Q2Cut_Norm     = (0, 14),
        
        Blinding       = False,
        
        filename       = "/eos/lhcb/wg/semileptonic/Bs2KmunuAna/Histogrammer/Output/Histograms.root" 
        )
    
    Results_High_WitzelBouchard += [R]
"""

Results_Low_Rusov = []

for Q2Bin in ["02","04","06","08","10","12","14","16"]:
    
    R = CalculateVub( 
        KMuNuFileName  = "KMuNuYields_Q2_{}_Low.txt".format(Q2Bin),
        DsMuNuFileName = "DsMuNuYields.txt",
        
        Cut_K_Fit      = "Q2_{}_%s".format(Q2Bin),
        Cut_Ds_Fit     = "PID_Tight",
    
        Cut_K          = "Q2_{}_Low".format(Q2Bin),
        Cut_Ds         = "PID_Tight",
        
        Cut_K_Q2       = "Q2_{}_Low".format(Q2Bin),# ToDo
        Cut_Ds_Q2      = "Nominal",
        
        FF_Model_K     = "Rusov",
        FF_Model_Ds    = "Bailey",
        
        Q2Cut_Signal   = (0, int(Q2Bin)),
        Q2Cut_Norm     = (0, 14),
        
        Blinding       = False,
        
        filename       = "/eos/lhcb/wg/semileptonic/Bs2KmunuAna/Histogrammer/Output/Histograms.root" 
        #filename       = "../../Histograms.root" 
        )
    
    Results_Low_Rusov += [R]


# Now make the arrays for plotting

from array import array
q2Cuts     = array('f', [2, 4, 6, 8, 10, 12, 14, 16])
q2Cuts_err = array('f', [0, 0, 0, 0,  0,  0,  0,  0])

import ROOT
c = ROOT.TCanvas("c1", "c1", 1600, 1600)

#####################################################################
#
#   Make Vub Plots 
#
#####################################################################



Vub_Bouchard     = array( 'f', [R.Vub[0] for R in Results_High_Bouchard])
Vub_Bouchard_err = array( 'f', [R.Vub[1] for R in Results_High_Bouchard])

Vub_Witzel     = array( 'f', [R.Vub[0] for R in Results_High_Witzel])
Vub_Witzel_err = array( 'f', [R.Vub[1] for R in Results_High_Witzel])

#Vub_WitzelBouchard     = array( 'f', [R.Vub[0] for R in Results_High_WitzelBouchard])
#Vub_WitzelBouchard_err = array( 'f', [R.Vub[1] for R in Results_High_WitzelBouchard])

Vub_Rusov     = array( 'f', [R.Vub[0] for R in Results_Low_Rusov])
Vub_Rusov_err = array( 'f', [R.Vub[1] for R in Results_Low_Rusov])



g_Vub_Bouchard = ROOT.TGraphErrors(8, q2Cuts, Vub_Bouchard, q2Cuts_err, Vub_Bouchard_err )
g_Vub_Witzel   = ROOT.TGraphErrors(8, q2Cuts, Vub_Witzel,   q2Cuts_err, Vub_Witzel_err   )
g_Vub_Rusov    = ROOT.TGraphErrors(8, q2Cuts, Vub_Rusov,    q2Cuts_err, Vub_Rusov_err   )

g_Vub_Bouchard.SetLineColor(1)
g_Vub_Bouchard.SetMarkerColor(1)

g_Vub_Witzel.SetLineColor(2)
g_Vub_Witzel.SetMarkerColor(2)

g_Vub_Rusov.SetLineColor(3)
g_Vub_Rusov.SetMarkerColor(3)


c.Clear()
g_VubScan = ROOT.TMultiGraph()


g_VubScan.Add(g_Vub_Rusov)
g_VubScan.Add(g_Vub_Bouchard)
g_VubScan.Add(g_Vub_Witzel)

g_VubScan.SetTitle(";q^{2} [GeV^{2}]")

leg = ROOT.TLegend(0.5, 0.8, 1.0, 1.0, "", "NDC")
leg.AddEntry(g_Vub_Rusov,    "Rusov Low Bin"    , "lep")
leg.AddEntry(g_Vub_Bouchard, "Bouchard High Bin", "lep")
leg.AddEntry(g_Vub_Witzel,   "Witzel   High Bin", "lep")

g_VubScan.Draw('A')
leg.Draw()
c.Print("VubScan.pdf")



#####################################################################
#
#   Make Yield Plots 
#
#####################################################################


Yield_High     = array( 'f', [R.Yield.KMuNu[0] for R in Results_High_Bouchard])
Yield_High_err = array( 'f', [R.Yield.KMuNu[1] for R in Results_High_Bouchard])

Yield_Low     = array( 'f', [R.Yield.KMuNu[0] for R in Results_Low_Rusov])
Yield_Low_err = array( 'f', [R.Yield.KMuNu[1] for R in Results_Low_Rusov])

from math import sqrt
Yield_Total     = array( 'f', [Y1 + Y2             for Y1, Y2 in zip(Yield_High,     Yield_Low)])
Yield_Total_err = array( 'f', [sqrt(Y1*Y1 + Y2*Y2) for Y1, Y2 in zip(Yield_High_err, Yield_Low_err)])

g_Yield_High  = ROOT.TGraphErrors(8, q2Cuts, Yield_High,  q2Cuts_err, Yield_High_err )
g_Yield_Low   = ROOT.TGraphErrors(8, q2Cuts, Yield_Low,   q2Cuts_err, Yield_Low_err  )
g_Yield_Total = ROOT.TGraphErrors(8, q2Cuts, Yield_Total, q2Cuts_err, Yield_Total_err)


g_Yield_High .SetLineColor(1)
g_Yield_Low  .SetLineColor(2)
g_Yield_Total.SetLineColor(3)

g_Yield_High .SetMarkerColor(1)
g_Yield_Low  .SetMarkerColor(2)
g_Yield_Total.SetMarkerColor(3)

mg_Yields = ROOT.TMultiGraph()

mg_Yields.Add( g_Yield_High  )
mg_Yields.Add( g_Yield_Low   )
mg_Yields.Add( g_Yield_Total )

mg_Yields.SetTitle(";q^{2} [GeV^{2}]")

leg = ROOT.TLegend(0.5, 0.4, 0.89, 0.6, "", "NDC")
leg.AddEntry(g_Yield_High,    "High q^{2} Bin", "lep")
leg.AddEntry(g_Yield_Low,     "Low  q^{2} Bin", "lep")
leg.AddEntry(g_Yield_Total,   "Combined"      , "lep")


c.Clear()
mg_Yields.Draw('A')
leg.Draw()
c.Print("Yields.pdf")


#####################################################################
#
#   Make Form Factor Plots 
#
#####################################################################


FF_Bouchard     = array( 'f', [R.FF.KMuNu[0] for R in Results_High_Bouchard])
FF_Bouchard_err = array( 'f', [R.FF.KMuNu[1] for R in Results_High_Bouchard])

FF_Witzel       = array( 'f', [R.FF.KMuNu[0] for R in Results_High_Witzel])
FF_Witzel_err   = array( 'f', [R.FF.KMuNu[1] for R in Results_High_Witzel])

FF_Rusov        = array( 'f', [R.FF.KMuNu[0] for R in Results_Low_Rusov])
FF_Rusov_err    = array( 'f', [R.FF.KMuNu[1] for R in Results_Low_Rusov])


g_FF_Bouchard = ROOT.TGraphErrors(8, q2Cuts, FF_Bouchard,  q2Cuts_err, FF_Bouchard_err )
g_FF_Witzel   = ROOT.TGraphErrors(8, q2Cuts, FF_Witzel,    q2Cuts_err, FF_Witzel_err   )
g_FF_Rusov    = ROOT.TGraphErrors(8, q2Cuts, FF_Rusov,     q2Cuts_err, FF_Rusov_err    )

g_FF_Bouchard.SetTitle(";q^{2} [GeV^{2}]")

g_FF_Bouchard.SetLineColor(1)
g_FF_Witzel  .SetLineColor(2)
g_FF_Rusov   .SetLineColor(3)

g_FF_Bouchard.SetMarkerColor(1)
g_FF_Witzel  .SetMarkerColor(2)
g_FF_Rusov   .SetMarkerColor(3)

mg_FF = ROOT.TMultiGraph()

mg_FF.Add( g_FF_Bouchard )
mg_FF.Add( g_FF_Witzel   )
mg_FF.Add( g_FF_Rusov    )

mg_FF.SetTitle(";q^{2} [GeV^{2}]")

leg = ROOT.TLegend(0.25, 0.6, 0.45, 1.0, "", "NDC")
leg.AddEntry(g_FF_Rusov,    "#int_{0}^{q^{2}}  Rusov"    , "lep")
leg.AddEntry(g_FF_Witzel,   "#int_{q^{2}}^{25} Witzel"  , "lep")
leg.AddEntry(g_FF_Bouchard, "#int_{q^{2}}^{25} Bouchard", "lep")


c.Clear()
mg_FF.Draw('A')
leg.Draw()
c.Print("FormFactorScan.pdf")



#####################################################################
#
#   Make Plots of selection Efficiency 
#
#####################################################################


Eff_High     = array( 'f', [R.Eff_Sel.KMuNu[0] for R in Results_High_Bouchard])
Eff_High_Err = array( 'f', [R.Eff_Sel.KMuNu[1] for R in Results_High_Bouchard])


Eff_Low     = array( 'f', [R.Eff_Sel.KMuNu[0] for R in Results_Low_Rusov])
Eff_Low_Err = array( 'f', [R.Eff_Sel.KMuNu[1] for R in Results_Low_Rusov])


g_Eff_High = ROOT.TGraphErrors(8, q2Cuts, Eff_High,  q2Cuts_err, Eff_High_Err )
g_Eff_Low  = ROOT.TGraphErrors(8, q2Cuts, Eff_Low,   q2Cuts_err, Eff_Low_Err  )


g_Eff_High.SetLineColor(1)
g_Eff_Low .SetLineColor(2)

g_Eff_High.SetMarkerColor(1)
g_Eff_Low .SetMarkerColor(2)

mg_EFF = ROOT.TMultiGraph()

mg_EFF.Add( g_Eff_High )
mg_EFF.Add( g_Eff_Low  )

mg_EFF.SetTitle(";q^{2} [GeV^{2}]")


leg = ROOT.TLegend(0.5, 0.5, 0.89, 0.7, "", "NDC")
leg.AddEntry(g_Eff_High,  "Efficiency in High q^{2} bin"    , "lep")
leg.AddEntry(g_Eff_Low,   "Efficiency in Low q^{2} bin"  , "lep")


c.Clear()
mg_EFF.Draw('A')
leg.Draw()
c.Print("EfficiencyPlot.pdf")

