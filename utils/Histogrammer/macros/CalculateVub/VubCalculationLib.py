

import ROOT

import KMuNuTools

from math import sqrt, pow
import numpy as np

# Efficiencies Class to manage efficiencies and error propagation


class Efficiency:
    
    def __init__(self, name = ""):
        self.name = name
    
        self.DsMuNu = (0, 0)
        self.KMuNu  = (0, 0)
        self.Ratio  = (0, 0)

    def __str__(self):
        Output = "Parameter: {0} \n".format(self.name)
        Output+= "    K Mu Nu: {0:>10.4g} +/- {1:<.4g} \n".format( self.KMuNu [0], self.KMuNu [1] )
        Output+= "   Ds Mu Nu: {0:>10.4g} +/- {1:<.4g} \n".format( self.DsMuNu[0], self.DsMuNu[1] )
        Output+= "      Ratio: {0:>10.4g} +/- {1:<.4g} \n".format( self.Ratio [0], self.Ratio [1] )

        return Output
    
    def __mul__(self, rhs):
        
        Out = Efficiency()
        
        KMuNu  = self.KMuNu [0] * rhs.KMuNu [0]
        DsMuNu = self.DsMuNu[0] * rhs.DsMuNu[0]
        Ratio  = self.Ratio [0] * rhs.Ratio [0]
        
        KMuNu__Err = KMuNu  * sqrt( pow(self.KMuNu [1]/self.KMuNu [0], 2) + pow(rhs.KMuNu [1]/rhs.KMuNu [0], 2) )
        DsMuNu_Err = DsMuNu * sqrt( pow(self.DsMuNu[1]/self.DsMuNu[0], 2) + pow(rhs.DsMuNu[1]/rhs.DsMuNu[0], 2) )
        Ratio__Err = Ratio  * sqrt( pow(self.Ratio [1]/self.Ratio [0], 2) + pow(rhs.Ratio [1]/rhs.Ratio [0], 2) )
        
        Out.KMuNu  = (KMuNu,  KMuNu__Err)
        Out.DsMuNu = (DsMuNu, DsMuNu_Err)
        Out.Ratio  = (Ratio,  Ratio__Err)
        
        return Out
    
    def __div__(self, rhs):
        Out = Efficiency()
        
        KMuNu  = self.KMuNu [0] / rhs.KMuNu [0]
        DsMuNu = self.DsMuNu[0] / rhs.DsMuNu[0]
        Ratio  = self.Ratio [0] / rhs.Ratio [0]
        
        KMuNu__Err = KMuNu  * sqrt( pow(self.KMuNu [1]/self.KMuNu [0], 2) + pow(rhs.KMuNu [1]/rhs.KMuNu [0], 2) )
        DsMuNu_Err = DsMuNu * sqrt( pow(self.DsMuNu[1]/self.DsMuNu[0], 2) + pow(rhs.DsMuNu[1]/rhs.DsMuNu[0], 2) )
        Ratio__Err = Ratio  * sqrt( pow(self.Ratio [1]/self.Ratio [0], 2) + pow(rhs.Ratio [1]/rhs.Ratio [0], 2) )

        Out.KMuNu  = (KMuNu,  KMuNu__Err)
        Out.DsMuNu = (DsMuNu, DsMuNu_Err)
        Out.Ratio  = (Ratio,  Ratio__Err)
        
        return Out

    

def GetFitYields(KMuNuFileName, DsMuNuFileName):
    Yields = Efficiency("Fit Yield")

    import os
    YieldsDir = os.environ["BSTOKMUNUROOT"] + "/utils/Fitter_Control/"

    with open(YieldsDir + DsMuNuFileName) as YieldsFile:
        Yields.DsMuNu = tuple(float(i) for i in YieldsFile.readline().split())
    
    with open(YieldsDir + KMuNuFileName) as YieldsFile:
        Yields.KMuNu = tuple(float(i) for i in YieldsFile.readline().split())
    

    eff_ratio = Yields.KMuNu[0] / Yields.DsMuNu[0]
    err_ratio = eff_ratio * sqrt( pow( Yields.KMuNu[1] / Yields.KMuNu[0], 2) + pow( Yields.DsMuNu[1] / Yields.DsMuNu[0], 2) )
    Yields.Ratio = (eff_ratio, err_ratio)
    
    return Yields


def GetBranchingFractions():
    BFs = Efficiency("Charm Branching Fraction")
    
    BFs.KMuNu  = (  1.0   , 0.0    )
    BFs.DsMuNu = (  0.0545, 0.0017 )
    BFs.Ratio  = ( 18.35  , 0.57   )
    
    return BFs

def GetFormFactors( Q2Cut_K, Q2Cut_Ds, Model_K = "Witzel", Model_Ds = "Bailey" ):

        
    FormFactors = Efficiency("Form Factors")
    
    if Model_K == "Witzel":
        from FormFactors.FormFactorModels import FF_Witzel
        FormFactors.KMuNu  = FF_Witzel(*Q2Cut_K)

    elif Model_K == "Bouchard":
        from FormFactors.FormFactorModels import FF_Bouchard
        FormFactors.KMuNu  = FF_Bouchard(*Q2Cut_K)

    elif Model_K == "WitzelBouchard":
        from FormFactors.FormFactorModels import FF_WitzelBouch
        FormFactors.KMuNu  = FF_WitzelBouch(*Q2Cut_K)
    
    elif Model_K == "Rusov":
        from FormFactors.FormFactorModels import FF_Rusov
        FormFactors.KMuNu  = FF_Rusov(*Q2Cut_K)

    else:
        print Model_K, "is not valid. Select one of:"
        print "\tWitzel", "Bouchard", "Rusov"
        exit(1)
        
    if Model_Ds == "Bailey":
        from FormFactors.FormFactorModels import FF_Bailey
        FormFactors.DsMuNu = FF_Bailey(*Q2Cut_Ds)
    else:
        print Model_Ds, "is not valid. Select one of:"
        print "\tBailey"
        exit(1)
        
    eff_ratio = FormFactors.KMuNu[0] / FormFactors.DsMuNu[0]
    err_ratio = eff_ratio * sqrt( pow( FormFactors.KMuNu[1] / FormFactors.KMuNu[0], 2) + pow( FormFactors.DsMuNu[1] / FormFactors.DsMuNu[0], 2) )
    
    FormFactors.Ratio = (eff_ratio, err_ratio)

    return FormFactors

def GetGeneratorEfficiency(f_hist,  Q2Cut_K, Q2Cut_Ds ):
    Eff_K  = KMuNuTools.GetCalculatedGeneratorEfficiency(f_hist, KMuNuTools.Decay.KMuNu,  Q2Cut_K [0] *1e6, Q2Cut_K [1] *1e6, -1,                -1                )
    Eff_Ds = KMuNuTools.GetCalculatedGeneratorEfficiency(f_hist, KMuNuTools.Decay.DsMuNu, Q2Cut_Ds[0] *1e6, Q2Cut_Ds[1] *1e6, -1,                -1                )
    Eff_R  = KMuNuTools.GetCalculatedGeneratorEfficiency(f_hist, KMuNuTools.Decay.Ratio,  Q2Cut_K [0] *1e6, Q2Cut_K [1] *1e6,  Q2Cut_Ds [0] *1e6, Q2Cut_Ds [1] *1e6)

    Eff_Gen = Efficiency("Generator Efficiency")
    Eff_Gen.KMuNu  = KMuNuTools.PairToTuple(Eff_K  )
    Eff_Gen.DsMuNu = KMuNuTools.PairToTuple(Eff_Ds )
    Eff_Gen.Ratio  = KMuNuTools.PairToTuple(Eff_R  )

    return Eff_Gen


def GetEfficiency(f_hist, Cut_K, Cut_Ds, Cut_K_Q2, Cut_Ds_Q2, Blinding = False):
    
    Path2_K  = "RW_Kin/KMuNu/MCDecay/{}/Systematic_Toys".format(Cut_K_Q2)
    Path1_K  = "Default/KMuNu/MC_13512010/{}/Systematic_Toys".format(Cut_K)

    Path2_Ds = "RW_Kin/DsMuNu/MCDecay/{}/Systematic_Toys".format(Cut_Ds_Q2)
    Path1_Ds = "Default/DsMuNu/13774000_Ds/{}/Systematic_Toys".format(Cut_Ds)

    
    Eff_K  = KMuNuTools.GetRatio(f_hist, Path1_K,  Path2_K,  False)
    Eff_Ds = KMuNuTools.GetRatio(f_hist, Path1_Ds, Path2_Ds, False)
    Eff_R  = KMuNuTools.GetRatio(f_hist, Path1_K,  Path2_K, Path1_Ds, Path2_Ds,  False)
    
    
    # These need to be scaled w.r.t. Q2 Cut
    
    
    
    Effs = Efficiency("Selection Efficiency {}".format("Blinded" if Blinding else ""))
                         
    Effs.KMuNu  = KMuNuTools.PairToTuple(Eff_K  )
    Effs.DsMuNu = KMuNuTools.PairToTuple(Eff_Ds )
    Effs.Ratio  = KMuNuTools.PairToTuple(Eff_R  )

    if Blinding:
        
        random.seed( Correction1 + Correction2 + Reco1 + Reco2 )
        
        BlindingFactor = random.random()+0.5
        Effs.KMuNu  = tuple(BlindingFactor * e for e in Effs.KMuNu)
        Effs.Ratio  = tuple(BlindingFactor * e for e in Effs.Ratio)

        BlindingFactor = random.random()+0.5
        Effs.DsMuNu = tuple(BlindingFactor * e for e in Effs.DsMuNu)
        Effs.Ratio  = tuple(e / BlindingFactor for e in Effs.Ratio)

    return Effs

import random
def GetRatio(f_hist, Correction1, Correction2, Cut_K, Cut_Ds, Name = "Ratio", BinomialError=False, Blinding=False):

    Effs = Efficiency(Name + (": Blinded" if Blinding else ""))
    
    eff_K = KMuNuTools.GetCalculatedCorrection( f_hist, "MC_13512010", Correction1, Correction2, Cut_K, KMuNuTools.Decay.KMuNu, BinomialError )
    Effs.KMuNu = KMuNuTools.PairToTuple(eff_K)

    eff_D = KMuNuTools.GetCalculatedCorrection( f_hist, "13774000_Ds", Correction1, Correction2, Cut_Ds, KMuNuTools.Decay.DsMuNu, BinomialError )
    Effs.DsMuNu = KMuNuTools.PairToTuple(eff_D)

    eff_R = KMuNuTools.GetCalculatedCorrection( f_hist, "MC_13512010", "13774000_Ds", Correction1, Correction2, Cut_K, Cut_Ds, BinomialError )
    Effs.Ratio = KMuNuTools.PairToTuple(eff_R)
    
    if Blinding:
        
        random.seed( Correction1 + Correction2 + Name + Cut_K + Cut_Ds )
        
        BlindingFactor = random.random()+0.5
        Effs.KMuNu  = tuple(BlindingFactor * e for e in Effs.KMuNu)
        Effs.Ratio  = tuple(BlindingFactor * e for e in Effs.Ratio)

        BlindingFactor = random.random()+0.5
        Effs.DsMuNu = tuple(BlindingFactor * e for e in Effs.DsMuNu)
        Effs.Ratio  = tuple(e / BlindingFactor for e in Effs.Ratio)

    return Effs


def GetBinMigration(Correction, Q2CutLow, Q2CutHigh):

    BinMigration = Efficiency("Bin Migration")
    
    Q2CutLow  *= 1e6
    Q2CutHigh *= 1e6
    f_Hist = ROOT.TFile.Open("../../Histograms.root")
    
    h_Q2 = f_Hist.Get("{}/KMuNu/MC_13512010/Is_Q2/h_Q2_True_Reco".format(Correction))
    
    
    N_True = h_Q2.Integral(int(1000*Q2CutLow/25e6), int(1000*Q2CutHigh/25e6), 0,                       1000 )
    N_Reco = h_Q2.Integral(0,                       1000,                     int(1000*Q2CutLow/25e6), int(1000*Q2CutHigh/25e6) )
    
    N_Ratio = N_True/N_Reco
    
    BinMigration.KMuNu  = (N_Ratio, 0.0)
    BinMigration.DsMuNu = (1.0,     0.0)
    BinMigration.Ratio  = (N_Ratio, 0.0)
    return BinMigration
    
    
    