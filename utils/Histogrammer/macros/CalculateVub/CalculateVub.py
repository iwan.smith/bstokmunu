"""@package CalculateVub

Documentation for the calculation of Vub

This code takes the output of the histogrammer, will run the fits and return barnching fractions and Vub

Efficiencies from each function are returned as a dictionary:

The Ratio is KMuNu/DsMuNu


"""
# Variables to Steer the calcualtion of the result


KMuNuFileName  = "KMuNuYields_High_Q2.txt"
DsMuNuFileName = "DsMuNuYields.txt"

Cut_K  = "High_Q2"
Cut_Ds = "PID_Tight"

Cut_K_Q2  = "High_Q2"
Cut_Ds_Q2 = "Nominal"

FF_Model_K  = "Witzel"
FF_Model_Ds = "Bailey"

Q2Cut_Signal = (7, 25)
Q2Cut_Norm   = (0, 14)

Blinding=False

filename = "/eos/lhcb/wg/semileptonic/Bs2KmunuAna/Histogrammer/Output/Histograms.root"



###############################################################################

import ROOT
f_hist = ROOT.TFile.Open("root://eoslhcb.cern.ch/" + filename)

if not f_hist:
    f_hist = ROOT.TFile.Open(filename)

from VubCalculationLib import *

###############################################################################

        

Yields = GetFitYields(KMuNuFileName, DsMuNuFileName)
print Yields

BFs = GetBranchingFractions()
print BFs


FFs = GetFormFactors(Q2Cut_Signal, Q2Cut_Norm, FF_Model_K, FF_Model_Ds)
print FFs

EffGen = GetGeneratorEfficiency(f_hist, Q2Cut_Signal, Q2Cut_Norm )
print EffGen

EffSel = GetEfficiency(f_hist, Cut_K, Cut_Ds, Cut_K_Q2, Cut_Ds_Q2)
print EffSel


CorrTrack = GetRatio(f_hist, "RW_Kin", "Tracking_Comb", Cut_K, Cut_Ds, "Tracking Correction", BinomialError = False, Blinding = False )
print CorrTrack

CorrPID = GetRatio(f_hist, "RW_Kin", "PID_Comb", Cut_K, Cut_Ds, "PID Correction", BinomialError = False, Blinding = False )
print CorrPID




TotEv = Yields / (BFs * EffGen * EffSel)  
TotEv.name = "Yields at Production, i.e. Branching Fraction"
print TotEv

TotEvFF = Yields / (BFs * EffGen * EffSel * FFs)  
TotEvFF.name = "Yields with Form Factors, i.e. Vub^2 / Vcb^2"
print TotEvFF

import math
print ("\nVub/Vcb = {0}".format(math.sqrt(TotEvFF.Ratio[0])))







