#!/bin/bash


mkdir FitInputPlots Q2_Migration 2>/dev/null

for SCRIPT in `ls *.py`
do
	echo "Running: " $SCRIPT
        python $SCRIPT
done



for SCRIPT in `ls *.C`
do
        echo "Running: " $SCRIPT
	root -l -q -b -x $SCRIPT+
done

