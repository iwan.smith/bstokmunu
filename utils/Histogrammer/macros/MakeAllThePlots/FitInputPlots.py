from ROOT import gROOT
gROOT.SetBatch(True)
import ROOT, os, numpy as np, sys
gROOT.ProcessLine(".x "+os.environ["BSTOKMUNUROOT"]+"/include/official.C")
import numpy as np


Histograms = ROOT.TFile.Open("/eos/lhcb/wg/semileptonic/Bs2KmunuAna/Histogrammer/Output/Histograms.root")


####################################################
#
# Plot the Fits to subtract the background
#
#
#
####################################################
PlotConfig={}
PlotConfig["Systematic"] = "Nominal"
PlotConfig["Cut"]        = "PID_Tight"
PlotConfig["Histogram"]  = "h_Bs_MCORR2"


c = ROOT.TCanvas("c", "c", 4000, 3200)
c.Divide(10,8)



DsMuNuPlots = [
    "13774000_Ds",
    "13774000_DsStar",
    "13774000_DsStar0",
    "13774000_DsStar1",
    "13774000_DsXtau",
    "13774002_Ds",
    "13774002_DsStar",
    "13774002_DsStar0",
    "13774002_DsStar1",
    "13774002_DsXtau",
    
    "MC_11876001",
    "MC_11995200",
    "MC_11995202",
    "MC_12875601",
    "MC_12995600",
    "MC_12995602",
    "MC_13873201",
    "MC_13996202",
    "MC_14175001",
    "MC_14175003",
    
    "SameSign",
    "FakeMu",
    "SameSign_FakeMu",
 ]

PlotConfig = {}
PlotConfig["Systematic"]     = "Default"
PlotConfig["SystematicElse"] = "Nominal"

PlotConfig["Cut"]            = "PID_Tight"
PlotConfig["Histogram"]      = "h_Bs_MCORR2"


c = ROOT.TCanvas("c1", "c1", 1600, 1600)

c.Print("FitInputPlots/DsMuNuInput.pdf[")

for plot in DsMuNuPlots:
    PlotConfig["Plot"] = plot
    
    print 'Nominal/DsMuNu/Data/%(Cut)s/%(Histogram)s_rmbg'%PlotConfig 
    print '%(Systematic)s/DsMuNu/13774000_Ds/%(Cut)s/%(Histogram)s'%PlotConfig 
    print '%(Systematic)s/DsMuNu/%(Plot)s/%(Cut)s/%(Histogram)s'%PlotConfig 
    print '%(SystematicElse)s/DsMuNu/%(Plot)s/%(Cut)s/%(Histogram)s'%PlotConfig 

    
    h_Data   = Histograms.Get( 'Nominal/DsMuNu/Data/%(Cut)s/%(Histogram)s_rmbg'%PlotConfig )
    h_Signal = Histograms.Get( '%(Systematic)s/DsMuNu/13774000_Ds/%(Cut)s/%(Histogram)s'%PlotConfig )
    h_Plot   = Histograms.Get( '%(Systematic)s/DsMuNu/%(Plot)s/%(Cut)s/%(Histogram)s'%PlotConfig )
    
    if not h_Plot:
        h_Plot   = Histograms.Get( '%(SystematicElse)s/DsMuNu/%(Plot)s/%(Cut)s/%(Histogram)s'%PlotConfig )
        
    
    print h_Data  
    print h_Signal
    print h_Plot  


    if h_Plot.Integral() < 100:
        continue
    
    h_Plot.Sumw2()
        
    #print '%(Systematic)s/DsMuNu/%(Plot)s/%(Cut)s/%(Histogram)s'%PlotConfig
    print h_Signal, h_Data, h_Plot
    
    h_Signal.SetLineColor(2)
    h_Signal.SetMarkerColor(2)
    h_Signal.SetMinimum(0)
    
    
    h_Data.SetLineColor(3)
    h_Data.SetMarkerColor(3)
    
    
    h_Signal.DrawNormalized();
    h_Plot  .DrawNormalized("SAME")
    h_Data  .DrawNormalized("SAME")
    
    
    leg = ROOT.TLegend(0.15, 0.7, 0.4, 0.9, "", "NDC")
    leg.AddEntry(h_Data,   "Data")
    leg.AddEntry(h_Signal, "Signal")
    leg.AddEntry(h_Plot, plot)
    leg.Draw()
    
    c.Print("FitInputPlots/DsMuNuInput.pdf")
c.Print("FitInputPlots/DsMuNuInput.pdf]")





KMuNuPlots = [
    "SameSign",
    "FakeK",
    "FakeMu",
    "FakeKMu",
    "SameSign_FakeK",
    "SameSign_FakeMu",
    "SameSign_FakeKMu",
    "MC_10010037",
    "MC_13512010",
    "MC_12143001",
    "MC_10010017",
    "MC_11144001",
    "MC_11512011",
    "MC_11512400",
    "MC_11574050",
    "MC_11574051",
    "MC_11676001",
    "MC_11676001_SS",
    "MC_11874004",
    "MC_11874004_SS",
    "MC_11874010",
    "MC_11874010_SS",
    "MC_11874042",
    "MC_11874042_SS",
    "MC_12143401",
    "MC_12513001",
    "MC_12573050",
    "MC_12573200",
    "MC_12573400",
    "MC_12573401",
    "MC_12873002",
    "MC_12873002_SS",
    "MC_13144001",
    "MC_13512400",
    "MC_13512410",
    "MC_13512420",
    "MC_13574040",
    "MC_13774000",
    "MC_13774002",
    "MC_13796000",
    "MC_15512013",
    "MC_15512014",
 ]

PlotConfig = {}
PlotConfig["Systematic"]     = "Default"
PlotConfig["SystematicElse"] = "Nominal"

PlotConfig["Cut"]            = "No_Q2"
PlotConfig["Histogram"]      = "h_Bs_MCORR2"



c.Print("FitInputPlots/KMuNuInput.pdf[")

for plot in KMuNuPlots:
    PlotConfig["Plot"] = plot
    
    h_Data   = Histograms.Get( 'Nominal/KMuNu/Data/%(Cut)s/%(Histogram)s'%PlotConfig )
    h_Signal = Histograms.Get( '%(Systematic)s/KMuNu/MC_13512010/%(Cut)s/%(Histogram)s'%PlotConfig )
    h_Plot   = Histograms.Get( '%(Systematic)s/KMuNu/%(Plot)s/%(Cut)s/%(Histogram)s'%PlotConfig )
    
    if not h_Plot:
        h_Plot   = Histograms.Get( '%(SystematicElse)s/KMuNu/%(Plot)s/%(Cut)s/%(Histogram)s'%PlotConfig )

    print '%(Systematic)s/KMuNu/%(Plot)s/%(Cut)s/%(Histogram)s'%PlotConfig
    print h_Signal, h_Data, h_Plot
        
    if h_Plot.Integral() < 100:
        continue
    
    h_Plot.Sumw2()
        
    
    h_Signal.SetLineColor(2)
    h_Signal.SetMarkerColor(2)
    h_Signal.SetMinimum(0)
    
    
    h_Data.SetLineColor(3)
    h_Data.SetMarkerColor(3)
    
    
    h_Signal.DrawNormalized();
    h_Plot  .DrawNormalized("SAME")
    h_Data  .DrawNormalized("SAME")
    
    
    leg = ROOT.TLegend(0.15, 0.7, 0.4, 0.9, "", "NDC")
    leg.AddEntry(h_Data,   "Data")
    leg.AddEntry(h_Signal, "Signal")
    leg.AddEntry(h_Plot, plot)
    leg.Draw()
    
    c.Print("FitInputPlots/KMuNuInput.pdf")
c.Print("FitInputPlots/KMuNuInput.pdf]")


