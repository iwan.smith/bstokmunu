from ROOT import gROOT
gROOT.SetBatch(True)
import ROOT, os, numpy as np, sys
gROOT.ProcessLine(".x "+os.environ["BSTOKMUNUROOT"]+"/include/official.C")
import numpy as np

def GetQ2Migration(Hist, Q2Low, Q2High):
    
    N_True = h_Q2.Integral(int(1000*Q2Low/25e6), int(1000*Q2High/25e6), 0,                    1000                  )
    N_Reco = h_Q2.Integral(0,                    1000,                  int(1000*Q2Low/25e6), int(1000*Q2High/25e6) )
    
    try:
        N_Ratio = N_True/N_Reco
    except:
        N_Ratio = 1.0
        
    return N_Ratio


f_Hists = ROOT.TFile.Open("/eos/lhcb/wg/semileptonic/Bs2KmunuAna/Histogrammer/Output/Histograms.root")

c = ROOT.TCanvas("c", "c", 1600, 1600)
for HName in ["", "_Witzel", "_Rusov", "_Bouchard"]:

    c.Clear()
    c.Print("Q2_Migration/Plots{}.pdf[".format(HName))

    print "Default{}/KMuNu/MC_13512010/Is_Q2/h_Q2_True_Reco".format(HName)
    
    h_Q2  = f_Hists.Get("Default{}/KMuNu/MC_13512010/Is_Q2/h_Q2_True_Reco".format(HName)).Clone()
    h_Q2.SetTitle(";q^{2}_{True};q^{2}_{Reco}")
    
    h_Q2.Rebin2D(10,10)
    
    h_Q2True = h_Q2.ProjectionX()
    h_Q2Reco = h_Q2.ProjectionY()
    
    h_Q2True.Rebin(2)
    h_Q2Reco.Rebin(2)

    h_Q2True.SetMinimum(0)
    h_Q2Reco.SetMinimum(0)


    h_Q2.Draw("COLZ")
    c.Print("Q2_Migration/Plots{}.pdf".format(HName))
    
    h_Q2True.SetTitle(";q^{2};")
    h_Q2True.Draw("")
    
    h_Q2Reco.SetMarkerColor(2)
    h_Q2Reco.SetLineColor(2)
    h_Q2Reco.Draw("SAME")
    
    TextBox = ROOT.TPaveText(0.75, 0.8, 0.9, 0.9, "NDC")
    TextBox.AddText("True")
    TextBox.AddText("Reco").SetTextColor(2)
    TextBox.SetFillColor(0)
    TextBox.SetLineWidth(0)
    TextBox.SetShadowColor(0)
    TextBox.Draw()


    c.Print("Q2_Migration/Plots{}.pdf".format(HName))
    
    


    ##
    #  Now calculate the ratio of ( nTrue > q2 ) / ( nReco > q2 )
    ##
    h_Q2  = f_Hists.Get("Default{}/KMuNu/MC_13512010/Is_Q2/h_Q2_True_Reco".format(HName))
    h_Q2.SetTitle(";q^{2}_{True};q^{2}_{Reco}")
    h_Q2.SetMarkerStyle(6)
    h_Q2.Draw("")
    
    ## Using q2 cut at 7e6
    Q2Cut = 7e6
    
    N_True = h_Q2.Integral(int(1000*Q2Cut/25e6), 1000, 0,                    1000)
    N_Reco = h_Q2.Integral(0,                    1000, int(1000*Q2Cut/25e6), 1000)
    
    N_Ratio = N_True/N_Reco

    print N_True, N_Reco, N_Ratio

    
    box_True = ROOT.TBox(Q2Cut, 0e6,   25e6, 25e6)
    box_Reco = ROOT.TBox(0e6,   Q2Cut, 25e6, 25e6)
    
    box_True.SetFillColorAlpha(2, 0.3)
    box_Reco.SetFillColorAlpha(3, 0.3)
    
    box_True.Draw()
    box_Reco.Draw()
    
    TextBox = ROOT.TPaveText(0.15, 0.7, 0.45, 0.9, "NDC")
    TextBox.AddText("n_True: {}".format(N_True))
    TextBox.AddText("n_Reco: {}".format(N_Reco))
    TextBox.AddText("True/Reco: {}".format(N_Ratio))
    TextBox.SetFillColor(0)
    TextBox.SetLineWidth(0)
    TextBox.SetShadowColor(0)

    TextBox.Draw()
    
    
    
    c.Print("Q2_Migration/Plots{}.pdf".format(HName))

    ##
    # Now perform a scan
    ##
    
    
    nBin = 100
    
    q2 = np.linspace(2, 22, nBin)
    
    Migration_High  = q2.copy()
    Migration_Low   = q2.copy()
    
    for i in range(nBin):
        Migration_High[i] = GetQ2Migration(h_Q2, q2[i]*1e6, 25e6     )
        Migration_Low [i] = GetQ2Migration(h_Q2, 0,         q2[i]*1e6)
    
    
    g_Migration_High = ROOT.TGraph(nBin, q2, Migration_High)
    g_Migration_Low  = ROOT.TGraph(nBin, q2, Migration_Low )
    
    g_Migration_High.SetLineColor(1)
    g_Migration_Low .SetLineColor(2)
    
    
    c.Clear()
    g_Migration_High.Draw()
    g_Migration_High.SetTitle(";q^{2};")
    g_Migration_High.SetMinimum(0.8)
    g_Migration_High.SetMaximum(1.2)
    
    mg_Migraion = ROOT.TMultiGraph()
    mg_Migraion.Add(g_Migration_High)
    mg_Migraion.Add(g_Migration_Low)
    
    mg_Migraion.Draw()
    c.Modified()
    
    TextBox = ROOT.TPaveText(0.8, 0.7, 0.9, 0.9, "NDC")
    TextBox.AddText("High Bin")
    TextBox.AddText("Low Bin").SetTextColor(2)
    TextBox.SetFillColor(0)
    TextBox.SetLineWidth(0)
    TextBox.SetShadowColor(0)

    TextBox.Draw()

    c.Print("Q2_Migration/Plots{}.pdf".format(HName))
    
    
    h_BsMCORR  = f_Hists.Get("Default{}/KMuNu/MC_13512010/No_Q2/h_Bs_MCORR2".format(HName)).Clone()

    h_BsMCORR.SetTitle(";m_{Corr} B_{s}^{0}")
    h_BsMCORR.DrawNormalized()
    
    c.Print("Q2_Migration/Plots{}.pdf".format(HName))
    
    
    c.Print("Q2_Migration/Plots{}.pdf]".format(HName))

    
    