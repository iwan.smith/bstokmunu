import ROOT
import numpy as np
from array import array

from sys import argv
ROOT.gROOT.SetBatch(True)

HistFile = ROOT.TFile.Open("/eos/lhcb/wg/semileptonic/Bs2KmunuAna/Histogrammer/Output/Histograms.root")
#HistFile = ROOT.TFile.Open("../../Histograms.root")

try:
    print argv[1], argv[2]
    Mode = argv[1]
    HistName = argv[2]
except:
    Mode = "JPsiK"
    HistName = "h_nTracks"
    
print "Nominal/{0}/MonteCarlo/Nominal/{1}".format(Mode, HistName)

try:
    h_MC   = HistFile.Get("Nominal/{0}/Signal_MC/Nominal/{1}".format(Mode, HistName))
    h_Data = HistFile.Get("sWeight/{0}/Data/Nominal/{1}".format(Mode, HistName))
    
    Binning = [h_MC.GetBinLowEdge(1)]
except:
    h_MC   = HistFile.Get("Nominal/{0}/13774000/Nominal/{1}".format(Mode, HistName))
    h_Data = HistFile.Get("sWeight/{0}/Data/Nominal/{1}".format(Mode, HistName))
    
    Binning = [h_MC.GetBinLowEdge(1)]

    
    
n_MC   = h_MC  .Integral()
n_Data = h_Data.Integral()

nBins = h_MC.GetNbinsX()

nNewBins = 15
print nBins
for bin in range(nBins):
    nev_MC   = h_MC  .Integral(1, bin+1)
    nev_Data = h_Data.Integral(1, bin+1)
    BinNumber = (nev_MC/n_MC + nev_Data/n_Data) * ( nNewBins+1 )/2
    
    if ( int( BinNumber ) > len(Binning)-1):
        Binning += [h_MC.GetBinLowEdge(bin+1)]
        
        
print Binning, len(Binning)

HistNew_MC   = ROOT.TH1F("h_nTracks_MC",   "Track Multiplicity", len(Binning)-1, array('d', Binning))
HistNew_Data = ROOT.TH1F("h_nTracks_Data", "Track Multiplicity", len(Binning)-1, array('d', Binning))

for bin in range(nBins):
    HistNew_MC  .Fill(h_MC  .GetBinLowEdge(bin+1), h_MC  .GetBinContent(bin+1))
    HistNew_Data.Fill(h_Data.GetBinLowEdge(bin+1), h_Data.GetBinContent(bin+1))
    
HistNew_MC  .Sumw2(False)
HistNew_Data.Sumw2(False)


HistNew_MC.SetLineColor(2)
HistNew_MC.SetMarkerColor(2)
    
c1 = ROOT.TCanvas("c1", "c1", 1600, 1600)

HistNew_MC  .Sumw2(True)
HistNew_Data.Sumw2(True)

HistNew_MC  .Scale( 1.0/HistNew_MC.Integral() )
HistNew_Data.Scale( 1.0/HistNew_Data.Integral() )

HistNew_Data.Draw()
HistNew_MC.Draw("SAME")

f_out = ROOT.TFile.Open("ReWeight_{0}_{1}.root".format(Mode, HistName), "RECREATE")
HistNew_Data.Write()
HistNew_MC.Write()

c1.Print("Hist_{0}_{1}.pdf".format(Mode, HistName))


HistNew_MC.Divide(HistNew_Data, HistNew_MC)

HistNew_MC.SetName("tracks_weight")
HistNew_MC.Write()

f_out.Close()
HistNew_MC.Draw()
c1.Print("Hist_{0}_{1}_rw.pdf".format(Mode, HistName))