import ROOT
import numpy as np
from array import array

from sys import argv
ROOT.gROOT.SetBatch(True)

HistFile = ROOT.TFile.Open("/eos/lhcb/wg/semileptonic/Bs2KmunuAna/Histogrammer/Output/Histograms.root")
#HistFile = ROOT.TFile.Open("../../Histograms.root")

try:
    print argv[1], argv[2]
    Mode = argv[1]
    HistName = argv[2]
except:
    Mode = "JPsiK"
    HistName = "h_nTracks"
    
print "Nominal/{0}/MonteCarlo/Nominal/{1}".format(Mode, HistName)

try:
    h_MC   = HistFile.Get("Nominal/{0}/Signal_MC/Nominal/{1}".format(Mode, HistName))
    h_Data = HistFile.Get("sWeight/{0}/Data/Nominal/{1}".format(Mode, HistName))
    
except:
    exit()
    

h_MC.Scale( 1.0/h_MC.Integral() )
h_Data.Scale( 1.0/h_Data.Integral() )


h_Data.Divide(h_MC)

f_out = ROOT.TFile.Open("ReWeight_{0}_{1}.root".format(Mode, HistName), "RECREATE")
h_Data.Write()
f_out.Close()