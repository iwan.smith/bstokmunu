\mainpage

Software written by Iwan Smith <iwan.smith@cern.ch>

# \f$B_{s} \rightarrow K^{-} \mu^{+} \nu_{\mu}\f$ NTuple Processing Code

# Requirements

## Generate histograms to be used in fits to the data:

<img src="../Histograms.png" alt="Fit to the Corrected mass distribution" style="width: 1000px;"/>

We have a large quantity of ROOT NTuples over several TB corresponding to different 
event types (Monte Carlo, data) and reconstructed into several decay modes:

  - \f$B_{s} \rightarrow K^{-} \mu^{+} \nu_{\mu}\f$
  - \f$B_{s} \rightarrow D_s^{-} \mu^{+} \nu_{\mu}\f$

In addition many backgrounds are simulated for each decay, each of which need to be processed 
with the exact same treatment applied.
 
### Summary of Requirements

  - Process many data sources simultaneously
    - Apply the exact same set of corrections/selections to each source
    - Categorise the output based on decay, selections, corrections and reconstruction 
  - Able to process large volumes of data safely
    - Should continue operation if fault is non critical, e.g. missing file
    - Should terminate if fault is critical, e.g. Missing lookup table
  - Produce an output dataset containing the processed variables, newly calculated 
    variables, corrections applied and list of cuts
  - Ability to calculate new variables on the fly if desired variable is missing
  - Able to lookup corrections to the data from variables, lookup tables etc.

# Output file format

A ROOT file will be generated containing Histograms after processing

```
Histograms.root
 |
 |--TrackingCorrection                        // Correction Factor applied
 |        |--Bs_KMuNu                         // Reconstructed decay
 |        |     |--Nominal                    // Selection applied
 |        |     |     |--Data_Sim             // DataType selected
 |        |     |     |       |--Bs_MCORR.TH1 // Final Histograms
 |        |     |     |       |--XX_XXX.TH1
 |        |     |     |
 |        |     |     |--Decay_12143001
 |        |     |     |       |--Bs_MCORR.TH1
 |        |     |     |       |      etc.
 |        |     |     |
 |        |     |     |--FakeMu // Use short names when there is no event type
 |        |     |             |--Bs_MCORR.TH1
 |        |     |             |      etc.
 |        |     |     
 |        |     | 
 |        |     |--Veto_Kstar
 |        |     |     |--Data_Sim             
 |        |     |     |       |--Bs_MCORR.TH1 
 |        |     |     |       |     etc.
 |        |     |     |
 |        |     |     |--Decay_12143001
 |        |     |     |       |--Bs_MCORR.TH1
 |        |     |     |       |      etc.
 |        |     |     |
 |        |     |     |--FakeMu // Use short names when there is no event type
 |        |     |     |       |--Bs_MCORR.TH1
 |        |    etc.
 |        |   
 |        |--Bs_DsMuNu
 |        |     |--Nominal
 |        |     |     |--Data_8TeV
 |        |     |     |       |--Bs_MCORR__Ds_MM.TH2 // For a 2D histogram separate the two variables by __
 |        |     |     |       |--Bs_PT__Ds_MM.TH2
 |        |     |     |       |      etc.

```
The path to the histogram will be:  

Histograms.root#/CorrectionName/ReconstructedAs/Selectionname/EventType/Variable.TH1  

# Clone and Build

The Histogrammer must be built as part of the analysis framework

Build using make:
```bash
git clone --recursive ssh://git@gitlab.cern.ch:7999/lhcb-slb/BsToKMuNu.git
cd BsToKMuNu/utils
source ./SetupEnv.sh

make -j8 -k

cd Histogrammer
./bin/MinimalExample

```


# Tutorial and Quickstart guide


A minimal example is given in src/app/MinimalExample.cxx <a href="MinimalExample_8cxx_source.html">Link 
To Source.</a>  <br>
This example shows the basics of processing data and Monte Carlo, with a few corrections 
applied to the Monte Carlo to correct for differences between Monte Carlo and data.

A full example used in the analysis is here src/app/Histogrammer_KMuNu.cxx <a href="Histogrammer__KMuNu_8cxx_source.html">Link 
To Source.</a>  <br>
This example has gone through a few iterations with multiple contributors adding elements, 
so isn't the neatest.


You will mostly be dealing with the Histogrammer class:
  - include/Histogrammer.h and src/lib/Histogrammer.cxx
    - This will allow you start processing data, save histograms, and create a smaller 
      dataset 
      
You will soon want to add corrections and weights.  
For this you will need the Systematics Classes:
  - include/Systematic.h and src/lib/Systematic.cxx
    - This will allow you to lookup corrections from a table, sPlot subtract the data etc.


