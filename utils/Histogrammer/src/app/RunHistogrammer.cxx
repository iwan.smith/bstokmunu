/*
 * Histogrammer.cxx
 *
 *  Created on: 15 May 2017
 *      Author: ismith
 */

#include <string>
#include <cstring>
#include <cstdlib>
#include <map>
#include <utility>
#include <fstream>
#include <iostream>
#include <cstdlib>
#include <time.h>

#include "TCanvas.h"
#include "TFile.h"
#include "TTree.h"
#include "TTreeFormula.h"
#include "TChain.h"
#include "TH1.h"
#include "TMath.h"
#include "TRandom.h"
#include "TLorentzVector.h"

#include "Histogrammer.h"
#include "HistogrammerFunctions.h"
#include "Histogrammer_AddOn.h"
//Test file to make sure we can do a simple loop and write a Histogram.

#include <boost/program_options.hpp>

using namespace boost;
namespace po = boost::program_options;
using namespace TMath;
using namespace std;



#include "KMuNu_Factory_Systematics.h"
#include "KMuNu_Factory_Run_JPsiK.h"
#include "KMuNu_Factory_Run_JPsiPhi.h"
#include "KMuNu_Factory_Run_DsMuNu.h"
#include "KMuNu_Factory_Run_KMuNu.h"
#include "KMuNu_Factory_Run_KMuNu_ScanQ2.h"
#include "KMuNu_Factory_Run_KMuNu_Detailed.h"

// From
// - https://gitlab.cern.ch/kgizdov/BsJpsiPhi/blob/master/analysis/src/lib/calculate_helicity_angles.cpp


void doBanner(){}

int main(int ac, char* av[]){



    /**********************************************************************
     *
     * Setup the options for boost program_options
     *
     *
     **********************************************************************/


    bool Run_KMuNu   = false;
    bool Run_DsMuNu  = false;
    bool Run_JPsiK   = false;
    bool Run_JPsiPhi = false;

    bool Skip_Data    = true;
    bool Skip_MCGen   = true;
    bool Skip_MCReco  = true;

    bool RunAddOns   = false;
    bool SaveTree    = false;

    bool SaveNorm    = false;

    bool RunFast     = false;
    bool RunSilent   = false;

    int PrintFrequency = 10000;
    int MaxFiles = -1;
    int MaxEv = -1;

    int nToys = 10;

    std::string OutputFile = "";
    try{
        po::options_description desc("Allowed options");
        desc.add_options()
            ("help,h", "produce help message")

            ("KMuNu",  po::bool_switch(&Run_KMuNu),   "Run the Bs -> K Mu Nu Histogrammer" )
            ("DsMuNu", po::bool_switch(&Run_DsMuNu),  "Run the Bs -> Ds Mu Nu Histogrammer" )
            ("JpsiK",  po::bool_switch(&Run_JPsiK),   "Run the B+ -> JPsi K Histogrammer" )
            ("JpsiPhi",po::bool_switch(&Run_JPsiPhi), "Run the Bs -> JPsi Phi Histogrammer" )

            ("NoData",  po::bool_switch(&Skip_Data),    "Do Not run over data")
            ("NoMCGen", po::bool_switch(&Skip_MCGen),   "Do Not run over generator level Monte Carlo")
            ("NoMCReco",po::bool_switch(&Skip_MCReco),  "Do Not run over reconstructed Monte Carlo")


            ("AddOns",   po::bool_switch(&RunAddOns), "After execution run the AddOns (if any)" )
            ("SaveTree", po::bool_switch(&SaveTree), "Save Tree of values. Effectively a trimmer" )
            ("SaveNorm", po::bool_switch(&SaveTree), "Save normalised clones of histograms. Doubles the number of histograms Saved" )

            ("Fast,F",   po::bool_switch(&RunFast), "Run the histogramer quickly. i.e. Don't save the unimportant stuff, and skip FakeSample etc." )
            ("Silent,S", po::bool_switch(&RunSilent), "Run the histogramer with a minimum of text output." )


            ("PrintFreq,P", po::value<int>(&PrintFrequency)->default_value(10000), "Print information for every n events")
            ("MaxFiles",    po::value<int>(&MaxFiles)->default_value(-1), "Maximum number of files to run over for each sample")
            ("MaxEv",       po::value<int>(&MaxEv)->default_value(-1), "Maximum number of entries to run over in each file")

            ("nToys",       po::value<int>(&nToys)->default_value(10), "How many toys to generate to assess systematics of lookup tables.")

            ("Output,o",       po::value<std::string>(&OutputFile)->default_value("Histograms.root"), "Specify output file name");

        po::variables_map vm;
        po::store(po::parse_command_line(ac, av, desc), vm);
        po::notify(vm);



        if (vm.count("help") or ac == 1) {
            std::cout << "Usage: ./bin/RunHistogrmmer [options]\n";
            std::cout << desc;
            return 0;
        }
    }
    catch(std::exception& e)
    {
        std::cout << e.what() << "\n";
        return 1;
    }


    /************************************************************************
     *                                                                      *
     * Test that files can be opened, and if so erase contents.             *
     *                                                                      *
     ************************************************************************/


    std::string FriendFileName =  OutputFile.substr(0, OutputFile.size()-5) + "_FriendTree.root";
    {
        TFile* f_test = TFile::Open(OutputFile.c_str(), "RECREATE");
        if ( not f_test )
            return(1);
        f_test->Close();
        delete f_test;

        TFile* f_test2 = TFile::Open(OutputFile.c_str(), "RECREATE");
        if ( not f_test2 )
            return(1);
        f_test2->Close();
        delete f_test2;
    }



    map<string, Systematic*> Systematics_JpsiPhi;
    map<string, Systematic*> Systematics_JpsiK;
    map<string, Systematic*> Systematics_KMuNu;
    map<string, Systematic*> Systematics_DsMuNu;

    KMuNuSystematicsFactory(nToys, Systematics_JpsiPhi, Systematics_JpsiK, Systematics_KMuNu, Systematics_DsMuNu);




    /******************************************************************************************
     *
     *
     *
     *   The Histogrammer Configuration for J/psi K
     *
     *
     *
     *
     *
     ******************************************************************************************/

    Factory_Run_JPsiK(
        OutputFile, FriendFileName,
        MaxEv, MaxFiles, PrintFrequency, SaveTree, SaveNorm, RunSilent,
        Run_JPsiK, Skip_Data, Skip_MCReco,
        Systematics_JpsiK );


    /******************************************************************************************
     *
     *
     *
     *   The Histogrammer Configuration for J/psi Phi
     *
     *
     *
     *
     *
     ******************************************************************************************/

    Factory_Run_JPsiPhi(
        OutputFile, FriendFileName,
        MaxEv, MaxFiles, PrintFrequency, SaveTree, SaveNorm, RunSilent,
        Run_JPsiPhi, Skip_Data, Skip_MCReco,
        Systematics_JpsiPhi );



    /******************************************************************************************
     *
     *
     *
     *   The Histogrammer Configuration for Ds Mu Nu
     *
     *
     *
     *
     *
     ******************************************************************************************/


    Factory_Run_DsMuNu(
        OutputFile, FriendFileName,
        MaxEv, MaxFiles, PrintFrequency, SaveTree, SaveNorm, RunSilent,
        Run_DsMuNu, Skip_Data, Skip_MCReco, Skip_MCGen, RunAddOns,
        Systematics_DsMuNu );



    /******************************************************************************************
     *
     *
     *
     *   The Histogrammer Configuration for KMuNu
     *
     *
     *
     *
     *
     ******************************************************************************************/

/*
    Factory_Run_KMuNu(
        OutputFile, FriendFileName,
        MaxEv, MaxFiles, PrintFrequency, SaveTree, SaveNorm, RunSilent,
        Run_JPsiPhi, Skip_Data, Skip_MCReco, Skip_MCGen, RunFast,
        Systematics_DsMuNu );
*/

/*
    Factory_Run_KMuNu_ScanQ2(
        OutputFile, FriendFileName,
        MaxEv, MaxFiles, PrintFrequency, SaveTree, SaveNorm, RunSilent,
        Run_KMuNu, Skip_Data, Skip_MCReco, Skip_MCGen, RunFast,
        Systematics_KMuNu );
*/
    Factory_Run_KMuNu_Detailed(
        OutputFile, FriendFileName,
        MaxEv, MaxFiles, PrintFrequency, SaveTree, SaveNorm, RunSilent,
        Run_KMuNu, Skip_Data, Skip_MCReco, Skip_MCGen, RunFast,
        Systematics_KMuNu );

    std::cout << "For debugging purposes print the ROOT director and all objects in memory:" << std::endl;
    gDirectory->pwd();
    gDirectory->ls();
}
