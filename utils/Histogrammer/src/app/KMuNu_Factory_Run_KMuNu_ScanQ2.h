#ifndef KMUNU_FACTORY_RUN_KMUNU_SCANQ2
#define KMUNU_FACTORY_RUN_KMUNU_SCANQ2



#include <string>
#include <cstring>
#include <cstdlib>
#include <map>
#include <utility>
#include <fstream>
#include <iostream>
#include <cstdlib>
#include <time.h>

#include "TCanvas.h"
#include "TFile.h"
#include "TTree.h"
#include "TTreeFormula.h"
#include "TChain.h"
#include "TH1.h"
#include "TMath.h"
#include "TRandom.h"
#include "TLorentzVector.h"

#include "Histogrammer.h"
#include "HistogrammerFunctions.h"
#include "Histogrammer_AddOn.h"
//Test file to make sure we can do a simple loop and write a Histogram.

#include "HistogrammerFuncs.h"

#include "KMuNu_Common_Functions.h"

void Factory_Run_KMuNu_ScanQ2(
    std::string OutputFile, std::string FriendFileName,
    int MaxEv, int MaxFiles, int PrintFrequency, bool SaveTree, bool SaveNorm, bool RunSilent,
    bool Run_KMuNu, bool Skip_Data, bool Skip_MCReco, bool Skip_MCGen, bool RunFast,
    map<string, Systematic*>& Systematics_KMuNu )
{

  vector<string> Q2Cuts = vector<string>{"02","06","08","12","16"} ;


  if ( Run_KMuNu )
  {
      Histogrammer H("KMuNu", OutputFile, FriendFileName);
      H.SetMaxEv       (MaxEv);
      H.SetMaxFiles    (MaxFiles);
      H.SetPrintFreq   (PrintFrequency);
      H.SaveFriendTree (SaveTree);
      H.SaveNorm       (SaveNorm);
      H.SetSilent(RunSilent);

      H.AddCut("Nominal", "1");
      H.AddCut("GenCut",
             "0.01 < TMath::ATan(Kminus_TRUEPT/Kminus_TRUEP_Z) && 0.4  > TMath::ATan(Kminus_TRUEPT/Kminus_TRUEP_Z)"
          "&& 0.01 < TMath::ATan(muplus_TRUEPT/muplus_TRUEP_Z) && 0.4  > TMath::ATan(muplus_TRUEPT/muplus_TRUEP_Z)"
      );

      H.AddAlias("True_Q2", "(B_s0_TRUEP_E - Kminus_TRUEP_E)**2 - (B_s0_TRUEP_X - Kminus_TRUEP_X)**2 - (B_s0_TRUEP_Y - Kminus_TRUEP_Y)**2 - (B_s0_TRUEP_Z - Kminus_TRUEP_Z)**2");
      H.AddFormula("Q2", "True_Q2");

      for(auto& Cut: Q2Cuts )
      {
        H.AddCut("Q2_" + Cut + "_High", ("True_Q2 > " + Cut + "e6").c_str());
        H.AddCut("Q2_" + Cut + "_Low" , ("True_Q2 < " + Cut + "e6").c_str());
      }


      H.AddSystematic("Nominal", Systematics_KMuNu.at("Nominal"));

      H.AddHistogram("True_Q2", "True q^{2}", 40, 0, 25e6, "Q2");



      H.AddBranch("B_s0_TRUEP_E");
      H.AddBranch("B_s0_TRUEP_X");
      H.AddBranch("B_s0_TRUEP_Y");
      H.AddBranch("B_s0_TRUEP_Z");
      H.AddBranch("B_s0_TRUEPT", "B_PT");



      H.AddFunction("MagnetPol", Functions_Generic::MagnetPolarity, &H.currentFile);


      H.SetPrintHist("True_Q2", "Nominal", "Nominal");


      if ( !Skip_MCGen)
      {
          RunOverGeneratedMC(H, Systematics_KMuNu);
      }

      H.Reset();

      H.AddAlias("Q2", "Bs_Regression_Q2_BEST/1e6");

      //const TCut Cut_BDT     ("min(kaon_m_IsoMinBDT,muon_p_IsoMinBDT)> -0.9 && TMVA_SS_Afc_BDT_New > (0.07252675 - 0.00844996 * Bs_Regression_Q2_BEST / 1e6 ) && TMVA_Charge_BDT_New > ( 0.135 * exp( -0.0867 * Bs_Regression_Q2_BEST / 1e6) - 0.05 )");
      const TCut Cut_BDT        ("min(kaon_m_IsoMinBDT,muon_p_IsoMinBDT)> -0.9 && TMVA_Charge_BDT_New > (( 0.07778 - 0.00745*Q2 ) * (Q2>0) + 0.018 * (Q2<0)) && TMVA_SS_Afc_BDT_New > (( 0.08425 - 0.01574*Q2 + 0.00059*Q2**2 ) * (Q2>0) + 0.055625 * (Q2<0))");
      const TCut Cut_Iso        ("min(kaon_m_IsoMinBDT,muon_p_IsoMinBDT)> -0.9");
      //const TCut Cut_BDT        ("TMVA_Charge_BDT_New > 0.0843 && TMVA_SS_Afc_BDT_New > 0.061");
      //const TCut Cut_BDT        ("TMVA_Charge_BDT_New > 0.05 && TMVA_SS_Afc_BDT_New > 0.04");

      const TCut Cut_Trig_TOS       (" Bs_Hlt2TopoMu2BodyBBDTDecision_TOS");
      const TCut Cut_Trig_TISTOS    (" Bs_Hlt2TopoMu2BodyBBDTDecision_TIS && Bs_Hlt2TopoMu2BodyBBDTDecision_TOS");
      const TCut Cut_HighQ2     ("Q2 > 7");
      const TCut Cut_LowQ2      ("Q2 < 7 && Q2 > 0");
      const TCut Cut_IsQ2       ("Q2 > 0");

      H.AddFormula("BDTCut" ,        Cut_BDT );
      H.AddFormula("Cut_Iso" ,       Cut_Iso );
      H.AddFormula("Trig_TOS",       Cut_Trig_TOS );
      H.AddFormula("Trig_TISTOS",    Cut_Trig_TISTOS );

      for(auto& Cut: Q2Cuts)
      {


        const TCut Cut_Q2_High((          "Q2 > " + Cut).c_str());
        const TCut Cut_Q2_Low (("Q2 > 0 && Q2 < " + Cut).c_str());

        H.AddFormula(("Q2_" + Cut + "_High").c_str(), Cut_Q2_High );
        H.AddFormula(("Q2_" + Cut + "_Low" ).c_str(), Cut_Q2_Low  );

        H.AddCut("Q2_" + Cut + "_High",       {"Trig_TOS", "BDTCut", "Cut_Iso", "Q2_" + Cut + "_High"} );
        H.AddCut("Q2_" + Cut + "_Low" ,       {"Trig_TOS", "BDTCut", "Cut_Iso", "Q2_" + Cut + "_Low" }  );

        H.AddCut("Q2_" + Cut + "_High_NoBDT", {"Trig_TOS",           "Cut_Iso", "Q2_" + Cut + "_High"} );
        H.AddCut("Q2_" + Cut + "_Low_NoBDT" , {"Trig_TOS",           "Cut_Iso", "Q2_" + Cut + "_Low" }  );

      }


      const TCut Cut_Combi      ("(kaon_m_PX*muon_p_PX>0 || kaon_m_PY*muon_p_PY>0)");
      H.SetCommonCut(Cut_Combi);


      H.AddFormula("Bu_MM", "sqrt((Bs_PE+muon_p_NIsoTr_PE)**2 - (Bs_PX+muon_p_NIsoTr_PX)**2 - (Bs_PY+muon_p_NIsoTr_PY)**2 - (Bs_PZ+muon_p_NIsoTr_PZ)**2)");
      H.AddFormula("B_CombiX", "kaon_m_PX*muon_p_PX");
      H.AddFormula("B_CombiY", "kaon_m_PY*muon_p_PY");


      H.AddBranch("nTracks");
      H.AddBranch("kaon_m_PZ");
      H.AddBranch("Bs_MCORR");
      H.AddBranch("Bs_MCORRERR");

      H.AddBranch("Bs_ETA", "B_ETA");
      H.AddBranch("Bs_PT",  "B_PT");
      H.AddBranch("Bs_PX",  "B_PX");
      H.AddBranch("Bs_PY",  "B_PY");
      H.AddBranch("Bs_PZ",  "B_PZ");

      H.AddBranch("kaon_m_PX");
      H.AddBranch("kaon_m_PY");
      H.AddBranch("kaon_m_PZ");
      H.AddBranch("kaon_m_PE");
      H.AddBranch("kaon_m_P");

      H.AddBranch("muon_p_PX");
      H.AddBranch("muon_p_PY");
      H.AddBranch("muon_p_PZ");
      H.AddBranch("muon_p_PE");
      H.AddBranch("muon_p_P");

      H.AddBranch("Bs_ENDVERTEX_CHI2");
      H.AddBranch("Bs_DOCA");

      H.AddBranch("Bs_Regression_Q2_BEST");


      H.AddFunction("MagnetPol",  Functions_Generic::MagnetPolarity, &H.currentFile);
      H.AddFunction("kaon_m_ETA", Functions_Generic::Part_ETA, new std::string( "kaon_m" ));
      H.AddFunction("muon_p_ETA", Functions_Generic::Part_ETA, new std::string( "muon_p" ));

      H.AddSystematic("sWeight",      Systematics_KMuNu.at("sWeight"));
      H.AddSystematic("Nominal",      Systematics_KMuNu.at("Nominal"));


      H.AddBranch("TMVA_Charge_BDT_New");
      H.AddBranch("TMVA_SS_Afc_BDT_New");


      H.AddHistogram("TMVA_Charge_BDT_New"              ,"TMVA_Charge_BDT_New"              ,100, -0.65 ,0.35, "TMVA_Charge_BDT_New");
      H.AddHistogram("TMVA_Charge_BDT_New_2"            ,"TMVA_Charge_BDT_New"              ,1000, -0.65 ,0.35, "TMVA_Charge_BDT_New");

      H.AddHistogram("TMVA_SS_Afc_BDT_New"              ,"TMVA_SS_Afc_BDT_New"              ,100, -0.65 ,0.35, "TMVA_SS_Afc_BDT_New");
      H.AddHistogram("TMVA_SS_Afc_BDT_New_2"            ,"TMVA_SS_Afc_BDT_New"              ,1000, -0.65 ,0.35, "TMVA_SS_Afc_BDT_New");



      H.AddHistogram("h_Bs_MCORR",   "B_{s} Corrected Mass", 60,  2500,  5370 ,            "Bs_MCORR"   );
      H.AddHistogram("h_Bs_MCORR2",  "B_{s} Corrected Mass", 80,  2500,  6500,             "Bs_MCORR"   );
      H.AddHistogram("h_Bs_MCORRERR","B_{s} Corrected Mass Error", 60,  0,  300 ,          "Bs_MCORRERR"   );
      H.AddHistogram("h_Q2_BEST",    "Best q^{2} solution",  50, 0e6, 25e6,                "Bs_Regression_Q2_BEST");
      H.AddHistogram("h_Bs_PT",      "p_{T} ( B_{s} )",      50, 0, 16000,                 "B_PT");
      H.AddHistogram("h_Quadrantcheck", "",      100, -10e6, 10e6, 100, -10e6, 10e6,       "B_CombiX", "B_CombiY");

      H.AddHistogram("h_nTracks", "Track Multiplicity", 1000, 0, 1000, "nTracks");

      H.AddHistogram("Bs_DOCA", "DOCA K #mu", 50, 0, 0.1, "Bs_DOCA");
      H.AddHistogram("Bs_ENDVERTEX_CHI2", "DOCA K #mu", 50, 0, 4, "Bs_ENDVERTEX_CHI2");

      H.AddHistogram("h_Bu_MM", "B^{+} # rightarrow J/#psi K", 100, 5180, 5400, "Bu_MM");





      H.SetPrintHist("h_Bs_MCORR", "Low_Q2", "Nominal");

      if ( !Skip_Data )
      {
          RunOverData(H, RunFast);
      }

      H.AddBranch("Bs_Regression_Q2_TRUE");

      H.AddHistogram("h_Q2_True_Reco",   "TrueQ2 vs Reco Q2", 1000,    0, 25e6, 1000,    0, 25e6, "Bs_Regression_Q2_TRUE", "Bs_Regression_Q2_BEST");
      H.AddHistogram("h_Q2_True_Reco_2", "TrueQ2 vs Reco Q2", 1000, -1e6, 25e6, 1000, -1e6, 25e6, "Bs_Regression_Q2_TRUE", "Bs_Regression_Q2_BEST");

      SetupSystematic_MonteCarlo(H, Systematics_KMuNu);

      if ( !Skip_MCReco)
      {
          RunOverMonteCarlo(H);
      }
  }

}

#endif
