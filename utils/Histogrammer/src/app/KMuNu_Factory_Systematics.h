#ifndef KMUNU_FACTORY_SYSTEMATIC_H
#define KMUNU_FACTORY_SYSTEMATIC_H

#include <string>
#include <cstring>
#include <cstdlib>
#include <map>
#include <utility>
#include <fstream>
#include <iostream>
#include <cstdlib>
#include <time.h>

#include "TCanvas.h"
#include "TFile.h"
#include "TTree.h"
#include "TTreeFormula.h"
#include "TChain.h"
#include "TH1.h"
#include "TMath.h"
#include "TRandom.h"
#include "TLorentzVector.h"

#include "Histogrammer.h"
#include "HistogrammerFunctions.h"
#include "Histogrammer_AddOn.h"
//Test file to make sure we can do a simple loop and write a Histogram.

void KMuNuSystematicsFactory(int nToys, map<string, Systematic*>& Systematics_JpsiPhi, map<string, Systematic*>& Systematics_JpsiK, map<string, Systematic*>& Systematics_KMuNu, map<string, Systematic*>& Systematics_DsMuNu )
{


    /************************************************************************
     *                                                                      *
     * Initialise The Classes for the systematics you wish to use.          *
     *                                                                      *
     ************************************************************************/

    // Initialise the nominal histogram
    Systematic_Nominal* Syst_Nominal = new Systematic_Nominal(nToys);

    // Initialise the nTracks ReWeighting
    //Systematic_Hist Syst_NT (nToys, "/eos/lhcb/wg/semileptonic/Bs2KmunuAna/Histogrammer/Input/ReWeight/weights_nTracks.root", "tracks_weight", OverflowTreatment::Closest, "nTracks");
    //Systematic_Hist Syst_ETA(nToys, "/eos/lhcb/wg/semileptonic/Bs2KmunuAna/Histogrammer/Input/ReWeight/ReWeight_JPsiK_h_B_ETA_PT.root", "h_B_ETA_PT", OverflowTreatment::Closest, "B_PT", "B_ETA");

    //Systematic_Combiner Syst_ReWeight(Syst_NT, Syst_ETA);

    Systematic_Variable* Syst_ReWeight= new Systematic_Variable(nToys, "RW_BDT");

    // Form factor


    // Initialise t*he Tracking Corrections
    Systematic_Hist* Syst_Track      = new Systematic_Hist( nToys, "/eos/lhcb/wg/semileptonic/Bs2KmunuAna/Histogrammer/Input/Tracking/ratio2012S20_MeV.root", "Ratio", OverflowTreatment::Unity);
    Systematic_Hist* Syst_Track_K_p  = new Systematic_Hist( *Syst_Track, "kaon_p_P", "kaon_p_ETA" );     Syst_Track_K_p  ->SetVerbose(false);   // By setting verbosity to false Histograms will not be created for this systematic
    Systematic_Hist* Syst_Track_K_m  = new Systematic_Hist( *Syst_Track, "kaon_m_P", "kaon_m_ETA" );     Syst_Track_K_m  ->SetVerbose(false);
    Systematic_Hist* Syst_Track_Pi   = new Systematic_Hist( *Syst_Track, "pi_p_P",   "pi_p_ETA"   );     Syst_Track_Pi   ->SetVerbose(false);
    Systematic_Hist* Syst_Track_Mu_p = new Systematic_Hist( *Syst_Track, "muon_p_P", "muon_p_ETA" );     Syst_Track_Mu_p ->SetVerbose(false);
    Systematic_Hist* Syst_Track_Mu_m = new Systematic_Hist( *Syst_Track, "muon_m_P", "muon_m_ETA" );     Syst_Track_Mu_m ->SetVerbose(false);


    // Initialise the PID Corrections
    Systematic_PID* Syst_PID_K_p       = new Systematic_PID( nToys, "K_Tight",   "kaon_p");             Syst_PID_K_p       ->SetVerbose(false);
    Systematic_PID* Syst_PID_K_m       = new Systematic_PID(*Syst_PID_K_p,       "kaon_m");             Syst_PID_K_m       ->SetVerbose(false);
    Systematic_PID* Syst_PID_K_p_Loose = new Systematic_PID( nToys, "K_Loose",   "kaon_p");             Syst_PID_K_p_Loose ->SetVerbose(false);
    Systematic_PID* Syst_PID_K_m_Loose = new Systematic_PID(*Syst_PID_K_p_Loose, "kaon_m");             Syst_PID_K_m_Loose ->SetVerbose(false);
    Systematic_PID* Syst_PID_Mu_p      = new Systematic_PID( nToys, "Mu_Tight",  "muon_p");             Syst_PID_Mu_p      ->SetVerbose(false);
    Systematic_PID* Syst_PID_Mu_m      = new Systematic_PID(*Syst_PID_Mu_p,      "muon_m");             Syst_PID_Mu_m      ->SetVerbose(false);
    Systematic_PID* Syst_PID_Pi        = new Systematic_PID( nToys, "Pi_Loose",  "pi_p"  );             Syst_PID_Pi        ->SetVerbose(false);

    // Initialise the Qsq reweighting for DsMuNu
    Systematic_Hist* ReWeight_Q2 = new Systematic_Hist(nToys, "/eos/lhcb/wg/semileptonic/Bs2KmunuAna/Histogrammer/Input/Q2/Q2_RW.root", "Q2_RW", OverflowTreatment::Unity, "TrueQ2" );


    // Initialise the Qsq reweighting for KMuNu
    Systematic_Hist* ReWeight_Q2_Bouchard  = new Systematic_Hist(nToys, "/eos/lhcb/wg/semileptonic/Bs2KmunuAna/Histogrammer/Input/Q2/FF_RW_K.root", "FF_Bouchard_Ratio", OverflowTreatment::Unity, "Bs_Regression_Q2_TRUE" );
    Systematic_Hist* ReWeight_Q2_Witzel    = new Systematic_Hist(nToys, "/eos/lhcb/wg/semileptonic/Bs2KmunuAna/Histogrammer/Input/Q2/FF_RW_K.root", "FF_Witzel_Ratio",   OverflowTreatment::Unity, "Bs_Regression_Q2_TRUE" );
    Systematic_Hist* ReWeight_Q2_Rusov     = new Systematic_Hist(nToys, "/eos/lhcb/wg/semileptonic/Bs2KmunuAna/Histogrammer/Input/Q2/FF_RW_K.root", "FF_Rusov_Ratio",    OverflowTreatment::Unity, "Bs_Regression_Q2_TRUE" );


    // Initialise the sWeighting for peaking modes
    Systematic_Hist* Syst_Sw_JpsiPhi    = new Systematic_Hist (nToys, "/eos/lhcb/wg/semileptonic/Bs2KmunuAna/Histogrammer/Input/sWeights/sWeights_JpsiPhi.root",     "nsig_sw", OverflowTreatment::Zero, "Bs_M");

    Systematic_Hist* Syst_Sw_JpsiK      = new Systematic_Hist (nToys, "/eos/lhcb/wg/semileptonic/Bs2KmunuAna/Histogrammer/Input/sWeights/sWeights_JpsiK.root",       "nsig_sw", OverflowTreatment::Zero, "Bu_M");

    Systematic_Hist* Syst_Sw_JpsiK_Iso  = new Systematic_Hist (nToys, "/eos/lhcb/wg/semileptonic/Bs2KmunuAna/Histogrammer/Input/sWeights/sWeights_JpsiK_Iso.root",   "nsig_sw", OverflowTreatment::Zero, "Bu_MM");

    Systematic_Hist* Syst_Sw_DsMuNu     = new Systematic_Hist (nToys, "/eos/lhcb/wg/semileptonic/Bs2KmunuAna/Histogrammer/Input/sWeights/sWeights_DsMuNu.root",      "nsig_sw", OverflowTreatment::Zero, "Ds_MM");

    //Combine systematics for the different modes.

    Systematic_Combiner* Syst_PID_JpsiPhi   = new Systematic_Combiner(Syst_PID_K_p, Syst_PID_K_m,       Syst_PID_Mu_p, Syst_PID_Mu_m              );
    Systematic_Combiner* Syst_PID_JpsiK     = new Systematic_Combiner(               Syst_PID_K_m,       Syst_PID_Mu_p, Syst_PID_Mu_m              );
    Systematic_Combiner* Syst_PID_KMuNu     = new Systematic_Combiner(               Syst_PID_K_m,       Syst_PID_Mu_p                              );
    Systematic_Combiner* Syst_PID_DsMuNu    = new Systematic_Combiner(Syst_PID_K_p, Syst_PID_K_m,       Syst_PID_Mu_p,                 Syst_PID_Pi);

    Systematic_Combiner* Syst_PID_NT_JpsiPhi= new Systematic_Combiner(Syst_ReWeight, Syst_PID_JpsiPhi);
    Systematic_Combiner* Syst_PID_NT_JpsiK  = new Systematic_Combiner(Syst_ReWeight, Syst_PID_JpsiK  );
    Systematic_Combiner* Syst_PID_NT_KMuNu  = new Systematic_Combiner(Syst_ReWeight, Syst_PID_KMuNu  );
    Systematic_Combiner* Syst_PID_NT_DsMuNu = new Systematic_Combiner(Syst_ReWeight, Syst_PID_DsMuNu );

    Systematic_Combiner* Syst_Track_JpsiPhi = new Systematic_Combiner(Syst_Track_K_p, Syst_Track_K_m, Syst_Track_Mu_p, Syst_Track_Mu_m                );
    Systematic_Combiner* Syst_Track_JpsiK   = new Systematic_Combiner(                 Syst_Track_K_m, Syst_Track_Mu_p, Syst_Track_Mu_m                );
    Systematic_Combiner* Syst_Track_KMuNu   = new Systematic_Combiner(                 Syst_Track_K_m, Syst_Track_Mu_p                                  );
    Systematic_Combiner* Syst_Track_DsMuNu  = new Systematic_Combiner(Syst_Track_K_p, Syst_Track_K_m, Syst_Track_Mu_p,                   Syst_Track_Pi);

    Systematic_Combiner* Syst_Track_NT_JpsiPhi= new Systematic_Combiner(Syst_ReWeight, Syst_Track_JpsiPhi);
    Systematic_Combiner* Syst_Track_NT_JpsiK  = new Systematic_Combiner(Syst_ReWeight, Syst_Track_JpsiK  );
    Systematic_Combiner* Syst_Track_NT_KMuNu  = new Systematic_Combiner(Syst_ReWeight, Syst_Track_KMuNu  );
    Systematic_Combiner* Syst_Track_NT_DsMuNu = new Systematic_Combiner(Syst_ReWeight, Syst_Track_DsMuNu );


    Systematic_Combiner* Default_JpsiPhi= new Systematic_Combiner(Syst_ReWeight, Syst_Track_JpsiPhi, Syst_PID_JpsiPhi );
    Systematic_Combiner* Default_JpsiK  = new Systematic_Combiner(Syst_ReWeight, Syst_Track_JpsiK  , Syst_PID_JpsiK   );
    Systematic_Combiner* Default_KMuNu  = new Systematic_Combiner(Syst_ReWeight, Syst_Track_KMuNu  , Syst_PID_KMuNu   );
    Systematic_Combiner* Default_DsMuNu = new Systematic_Combiner(Syst_ReWeight, Syst_Track_DsMuNu , Syst_PID_DsMuNu  );

    Systematic_Combiner* Default_KMuNu_sWeight  = new Systematic_Combiner(Syst_ReWeight, Syst_Track_KMuNu  , Syst_PID_KMuNu, Syst_Sw_JpsiK_Iso);

    Systematic_Combiner* Default_KMuNu_Rusov    = new Systematic_Combiner(Syst_ReWeight, Syst_Track_KMuNu  , Syst_PID_KMuNu, ReWeight_Q2_Rusov);
    Systematic_Combiner* Default_KMuNu_Witzel   = new Systematic_Combiner(Syst_ReWeight, Syst_Track_KMuNu  , Syst_PID_KMuNu, ReWeight_Q2_Witzel);
    Systematic_Combiner* Default_KMuNu_Bouchard = new Systematic_Combiner(Syst_ReWeight, Syst_Track_KMuNu  , Syst_PID_KMuNu, ReWeight_Q2_Bouchard);

    // Create lists of systematics for each decay mode.

    // Assign the systematics for JpsiPhi


    Systematics_JpsiPhi["Nominal"] = Syst_Nominal;

    Systematics_JpsiPhi["RW_Kin"]     = Syst_ReWeight;

    Systematics_JpsiPhi["Tracking_K_p"]  = Syst_Track_K_p;
    Systematics_JpsiPhi["Tracking_K_m"]  = Syst_Track_K_m;
    Systematics_JpsiPhi["Tracking_Mu_p"] = Syst_Track_Mu_p;
    Systematics_JpsiPhi["Tracking_Mu_m"] = Syst_Track_Mu_m;
    Systematics_JpsiPhi["Tracking_Comb"] = Syst_Track_JpsiPhi;
    Systematics_JpsiPhi["Tracking_Comb_NT"] = Syst_Track_NT_JpsiPhi;

    Systematics_JpsiPhi["PID_K_p"]  = Syst_PID_K_p;
    Systematics_JpsiPhi["PID_K_m"]  = Syst_PID_K_m;
    Systematics_JpsiPhi["PID_Mu_p"] = Syst_PID_Mu_p;
    Systematics_JpsiPhi["PID_Mu_m"] = Syst_PID_Mu_m;
    Systematics_JpsiPhi["PID_Comb"] = Syst_PID_NT_JpsiPhi;
    Systematics_JpsiPhi["PID_Comb_NT"] = Syst_PID_NT_JpsiPhi;

    Systematics_JpsiPhi["sWeight"]    = Syst_Sw_JpsiPhi;
    //Systematics_JpsiPhi["sWeight_BG"] = Syst_SwBG_JpsiPhi;

    Systematics_JpsiPhi["Default"]    = Default_JpsiPhi;



    // Assign the systematics for JpsiK

    Systematics_JpsiK["Nominal"] = Syst_Nominal;

    Systematics_JpsiK["RW_Kin"]     = Syst_ReWeight;

    Systematics_JpsiK["Tracking_K_m"]  = Syst_Track_K_m;
    Systematics_JpsiK["Tracking_Mu_p"] = Syst_Track_Mu_p;
    Systematics_JpsiK["Tracking_Mu_m"] = Syst_Track_Mu_m;
    Systematics_JpsiK["Tracking_Comb"] = Syst_Track_JpsiK;
    Systematics_JpsiK["Tracking_Comb_NT"] = Syst_Track_NT_JpsiK;

    Systematics_JpsiK["PID_K_m"]  = Syst_PID_K_m;
    Systematics_JpsiK["PID_Mu_p"] = Syst_PID_Mu_p;
    Systematics_JpsiK["PID_Mu_m"] = Syst_PID_Mu_m;
    Systematics_JpsiK["PID_Comb"] = Syst_PID_JpsiK;
    Systematics_JpsiK["PID_Comb_NT"] = Syst_PID_NT_JpsiK;

    Systematics_JpsiK["sWeight"]    = Syst_Sw_JpsiK;
    //Systematics_JpsiK["sWeight_BG"] = Syst_SwBG_JpsiK;

    Systematics_JpsiK["Default"] = Default_JpsiK;



    // Assign the systematics for KMuNu
    Systematics_KMuNu["Nominal"] = Syst_Nominal;

    Systematics_KMuNu["RW_Kin"]     = Syst_ReWeight;


    Systematics_KMuNu["Tracking_K_m"]     = Syst_Track_K_m;
    Systematics_KMuNu["Tracking_Mu_p"]    = Syst_Track_Mu_p;
    Systematics_KMuNu["Tracking_Comb"]    = Syst_Track_KMuNu;
    Systematics_KMuNu["Tracking_Comb_NT"] = Syst_Track_NT_KMuNu;

    Systematics_KMuNu["PID_K_m"]     = Syst_PID_K_m;
    Systematics_KMuNu["PID_Mu_p"]    = Syst_PID_Mu_p;
    Systematics_KMuNu["PID_Comb"]    = Syst_PID_KMuNu;
    Systematics_KMuNu["PID_Comb_NT"] = Syst_PID_NT_KMuNu;

    Systematics_KMuNu["sWeight"]    = Syst_Sw_JpsiK_Iso;
    //Systematics_KMuNu["sWeight_BG"] = Syst_SwBG_JpsiK_Iso;

    Systematics_KMuNu["ReWeight_Q2_Bouchard"] = ReWeight_Q2_Bouchard;
    Systematics_KMuNu["ReWeight_Q2_Witzel"  ] = ReWeight_Q2_Witzel  ;
    Systematics_KMuNu["ReWeight_Q2_Rusov"   ] = ReWeight_Q2_Rusov   ;

    Systematics_KMuNu["Default"] = Default_KMuNu;
    Systematics_KMuNu["Default_sw"] = Default_KMuNu_sWeight;

    Systematics_KMuNu["Default_Bouchard"] = Default_KMuNu_Rusov    ;
    Systematics_KMuNu["Default_Witzel"  ] = Default_KMuNu_Witzel   ;
    Systematics_KMuNu["Default_Rusov"   ] = Default_KMuNu_Bouchard ;


    // Assign the systematics for DsMuNu
    Systematics_DsMuNu["Nominal"] = Syst_Nominal;

    Systematics_DsMuNu["RW_Kin"]     = Syst_ReWeight;

    Systematics_DsMuNu["Tracking_K_p"]     = Syst_Track_K_p;
    Systematics_DsMuNu["Tracking_K_m"]     = Syst_Track_K_m;
    Systematics_DsMuNu["Tracking_Mu_p"]    = Syst_Track_Mu_p;
    Systematics_DsMuNu["Tracking_Pi"]      = Syst_Track_Pi;
    Systematics_DsMuNu["Tracking_Comb"]    = Syst_Track_DsMuNu;
    Systematics_DsMuNu["Tracking_Comb_NT"] = Syst_Track_NT_DsMuNu;

    Systematics_DsMuNu["PID_K_p"]     = Syst_PID_K_p;
    Systematics_DsMuNu["PID_K_m"]     = Syst_PID_K_m;
    Systematics_DsMuNu["PID_Mu_p"]    = Syst_PID_Mu_p;
    Systematics_DsMuNu["PID_Pi"]      = Syst_PID_Pi;
    Systematics_DsMuNu["PID_Comb"]    = Syst_PID_DsMuNu;
    Systematics_DsMuNu["PID_Comb_NT"] = Syst_PID_NT_DsMuNu;

    Systematics_DsMuNu["sWeight"]    = Syst_Sw_DsMuNu;
    //Systematics_DsMuNu["sWeight_BG"] = Syst_SwBG_DsMuNu;

    Systematics_DsMuNu["RW_Q2"] = ReWeight_Q2;

    Systematics_DsMuNu["Default"] = Default_DsMuNu;

}


#endif
