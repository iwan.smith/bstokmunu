#ifndef HISTOGRAMMER_COMMON_FUNCTIONS
#define HISTOGRAMMER_COMMON_FUNCTIONS

#include <string>
#include <cstring>
#include <cstdlib>
#include <map>
#include <utility>
#include <fstream>
#include <iostream>
#include <cstdlib>
#include <time.h>

#include "TCanvas.h"
#include "TFile.h"
#include "TTree.h"
#include "TTreeFormula.h"
#include "TChain.h"
#include "TH1.h"
#include "TMath.h"
#include "TRandom.h"
#include "TLorentzVector.h"

#include "Histogrammer.h"
#include "HistogrammerFunctions.h"
#include "Histogrammer_AddOn.h"


void SetupSystematic_MonteCarlo(Histogrammer& H, map<string, Systematic*>& Systematics_KMuNu)
{




  H.AddFormula   ("RW_BDT", "RW_BDT");
  H.AddSystematic("RW_Kin",        Systematics_KMuNu.at("RW_Kin"));

  H.AddSystematic("Tracking_K_m",  Systematics_KMuNu.at("Tracking_K_m"));
  H.AddSystematic("Tracking_Mu_p", Systematics_KMuNu.at("Tracking_Mu_p"));
  H.AddSystematic("Tracking_Comb", Systematics_KMuNu.at("Tracking_Comb_NT"));

  H.AddSystematic("PID_K_m",       Systematics_KMuNu.at("PID_K_m"));
  H.AddSystematic("PID_Mu_p",      Systematics_KMuNu.at("PID_Mu_p"));
  H.AddSystematic("PID_Comb",      Systematics_KMuNu.at("PID_Comb_NT"));

  H.AddSystematic("Default",       Systematics_KMuNu.at("Default"));
  H.AddSystematic("Default_sw",    Systematics_KMuNu.at("Default_sw"));

  H.AddSystematic("RW_Q2_Bouchard", Systematics_KMuNu.at("ReWeight_Q2_Bouchard"));
  H.AddSystematic("RW_Q2_Witzel"  , Systematics_KMuNu.at("ReWeight_Q2_Witzel"  ));
  H.AddSystematic("RW_Q2_Rusov"   , Systematics_KMuNu.at("ReWeight_Q2_Rusov"   ));

  H.AddSystematic("Default_Bouchard", Systematics_KMuNu.at("Default_Bouchard"));
  H.AddSystematic("Default_Witzel"  , Systematics_KMuNu.at("Default_Witzel"  ));
  H.AddSystematic("Default_Rusov"   , Systematics_KMuNu.at("Default_Rusov"   ));




}


void RunOverGeneratedMC(Histogrammer& H, map<string, Systematic*>& Systematics_KMuNu)
{


  H.InitialiseDataType("MCDecay_NoGenCut");
  H.run("/eos/lhcb/wg/semileptonic/Bs2KmunuAna/Tuples/MCDecay_NoGenCut_Aug17/Bs_MCDecayTree_NoGenCut_13512010_Sim08av2.root", "MCDecayTreeTuple_K/MCDecayTree");
  H.FinaliseDataType();

  H.SetCommonCut("1");

  H.AddBranch("RW_BDT");
  H.AddBranch("nTracks");

  H.AddHistogram("h_RW_BDT", "BDT_ReWeighter", 100, 0, 2, "RW_BDT" );

  H.AddSystematic("RW_Kin",     Systematics_KMuNu.at("RW_Kin"));
  H.AddSystematic("Default",    Systematics_KMuNu.at("Nominal"));

  H.InitialiseDataType("MCDecay");
  H.run("/eos/lhcb/wg/semileptonic/Bs2KmunuAna/Tuples/Bs2KMuNu_MCTUPLES_TRIMMED_08Feb17/DTT_13512010_Bs_Kmunu_DecProdCut_*_Py8_MC12.root", "Bs2KMuNuMCTuple");
  H.FinaliseDataType();

}


void RunOverData(Histogrammer& H, bool RunFast)
{


  H.InitialiseDataType("Data");
  H.run("/eos/lhcb/wg/semileptonic/Bs2KmunuAna/Tuples/Bs2KMuNu_DATATUPLES_TRIMMED_Nov17/Merged_data12_Bs2KMuNu_*.root", "Bs2KmuNuTuple");
  H.FinaliseDataType();


  H.InitialiseDataType("SameSign");
  H.run("/eos/lhcb/wg/semileptonic/Bs2KmunuAna/Tuples/Bs2KMuNu_DATATUPLES_TRIMMED_Nov17/Merged_data12_Bs2KMuNu_*.root", "Bs2KmuNuSSTuple");
  H.FinaliseDataType();

  if ( not RunFast)
  {
    H.InitialiseDataType("FakeK");
    H.run("/eos/lhcb/wg/semileptonic/Bs2KmunuAna/Tuples/Bs2KMuNu_DATATUPLES_TRIMMED_Nov17/Merged_data12_Bs2KMuNu_*.root", "Bs2KmuNuTuple_FakeK");
    H.FinaliseDataType();

    H.InitialiseDataType("FakeMu");
    H.run("/eos/lhcb/wg/semileptonic/Bs2KmunuAna/Tuples/Bs2KMuNu_DATATUPLES_TRIMMED_Nov17/Merged_data12_Bs2KMuNu_*.root", "Bs2KmuNuTuple_FakeMu");
    H.FinaliseDataType();

    H.InitialiseDataType("FakeKMu");
    H.run("/eos/lhcb/wg/semileptonic/Bs2KmunuAna/Tuples/Bs2KMuNu_DATATUPLES_TRIMMED_Nov17/Merged_data12_Bs2KMuNu_*.root", "Bs2KmuNuTuple_FakeKMu");
    H.FinaliseDataType();



    H.InitialiseDataType("SameSign_FakeK");
    H.run("/eos/lhcb/wg/semileptonic/Bs2KmunuAna/Tuples/Bs2KMuNu_DATATUPLES_TRIMMED_Nov17/Merged_data12_Bs2KMuNu_*.root", "Bs2KmuNuSSTuple_FakeK");
    H.FinaliseDataType();

    H.InitialiseDataType("SameSign_FakeMu");
    H.run("/eos/lhcb/wg/semileptonic/Bs2KmunuAna/Tuples/Bs2KMuNu_DATATUPLES_TRIMMED_Nov17/Merged_data12_Bs2KMuNu_*.root", "Bs2KmuNuSSTuple_FakeMu");
    H.FinaliseDataType();

    H.InitialiseDataType("SameSign_FakeKMu");
    H.run("/eos/lhcb/wg/semileptonic/Bs2KmunuAna/Tuples/Bs2KMuNu_DATATUPLES_TRIMMED_Nov17/Merged_data12_Bs2KMuNu_*.root", "Bs2KmuNuSSTuple_FakeKMu");
    H.FinaliseDataType();
  }

}



void RunOverMonteCarlo(Histogrammer& H)
{



  H.InitialiseDataType("MC_13512010");
  H.run("/eos/lhcb/wg/semileptonic/Bs2KmunuAna/Tuples/Bs2KMuNu_MCTUPLES_TRIMMED_08Feb17/DTT_13512010_Bs_Kmunu_DecProdCut_*_Py8_MC12.root", "Bs2KmuNuTuple");
  H.FinaliseDataType();


  H.InitialiseDataType("MC_10010037");
  H.run("/eos/lhcb/wg/semileptonic/Bs2KmunuAna/Tuples/Bs2KMuNu_MCTUPLES_TRIMMED_30May16/10010037_85percent/DTT_10010037_incl_b_CharmtoKplusmu_PPTcuts_InAcc_??_Py8_MC1?.root", "Bs2KmuNuTuple");
  H.FinaliseDataType();
  vector<string> MC_EV_Types;

  MC_EV_Types.push_back("12143001");

  MC_EV_Types.push_back("10010017");
  MC_EV_Types.push_back("11144001");
  MC_EV_Types.push_back("11512011");
  MC_EV_Types.push_back("11512400");
  MC_EV_Types.push_back("11574050");
  MC_EV_Types.push_back("11574051");
  MC_EV_Types.push_back("11676001");
  MC_EV_Types.push_back("11874004");
  MC_EV_Types.push_back("11874010");
  MC_EV_Types.push_back("11874042");
  MC_EV_Types.push_back("12143001");
  MC_EV_Types.push_back("12143401");
  MC_EV_Types.push_back("12513001");
  MC_EV_Types.push_back("12573050");
  MC_EV_Types.push_back("12573200");
  MC_EV_Types.push_back("12573400");
  MC_EV_Types.push_back("12573401");
  MC_EV_Types.push_back("12873002");
  MC_EV_Types.push_back("13144001");
  MC_EV_Types.push_back("13512400");
  MC_EV_Types.push_back("13512410");
  MC_EV_Types.push_back("13512420");
  MC_EV_Types.push_back("13574040");
  MC_EV_Types.push_back("13774000");
  MC_EV_Types.push_back("13774002");
  MC_EV_Types.push_back("13796000");
  MC_EV_Types.push_back("15512013");
  MC_EV_Types.push_back("15512014");

  for( auto evtype: MC_EV_Types ){
      string MCType = "MC_" + evtype;


      /*

      if (evtype == "12143001" )
      {
          H.AddFormula("Iso_ID", "abs(muon_p_NIsoTr_TRUEID) == 13 && muon_p_NIsoTr_MC_MOTHER_ID==443 && abs(muon_p_NIsoTr_MC_GD_MOTHER_ID)==521 && muon_p_NIsoTr_MC_MOTHER_KEY==muon_p_MC_MOTHER_KEY && kaon_m_MC_MOTHER_KEY==muon_p_NIsoTr_MC_GD_MOTHER_KEY");


          H.AddCut("High_Q2_IsoID",       {"BDTCut", "HighQ2", "Iso_ID"});
          H.AddCut("Low_Q2_IsoID",        {"BDTCut", "LowQ2" , "Iso_ID"});
          H.AddCut("Is_Q2_IsoID",         {"BDTCut", "IsQ2"  , "Iso_ID"});
          H.AddCut("No_Q2_IsoID",         {"BDTCut"          , "Iso_ID"});
          H.AddCut("High_Q2_NoBDT_IsoID", {          "HighQ2", "Iso_ID"});
          H.AddCut("Low_Q2_NoBDT_IsoID",  {          "LowQ2" , "Iso_ID"});
          H.AddCut("Is_Q2_NoBDT_IsoID",   {          "IsQ2"  , "Iso_ID"});
          H.AddCut("No_Q2_NoBDT_IsoID",   {                    "Iso_ID"});

          for(auto& Cut: vector<string>{"02","04","06","08","10","12","14","16"} )
          {

            H.AddCut("Q2_" + Cut + "_High_IsoID",       {"Trig_TOS", "BDTCut", "Cut_Iso", "Q2_" + Cut + "_High", "Iso_ID"} );
            H.AddCut("Q2_" + Cut + "_Low_IsoID" ,       {"Trig_TOS", "BDTCut", "Cut_Iso", "Q2_" + Cut + "_Low" , "Iso_ID"} );

            H.AddCut("Q2_" + Cut + "_High_NoBDT_IsoID", {"Trig_TOS",           "Cut_Iso", "Q2_" + Cut + "_High", "Iso_ID"} );
            H.AddCut("Q2_" + Cut + "_Low_NoBDT_IsoID" , {"Trig_TOS",           "Cut_Iso", "Q2_" + Cut + "_Low" , "Iso_ID"} );

          }

      }

      if (evtype == "12873002")
      {
          const TCut TrueMuandK("(abs(muon_p_TRUEID)==13 && abs(kaon_m_TRUEID)==321) &&(abs(kaon_m_MC_MOTHER_ID) == 421)");
          const TCut MCTruthMathcing_Bu = TrueMuandK * TCut("(abs(muon_p_MC_MOTHER_ID)== 521 ) && (muon_p_MC_MOTHER_KEY == kaon_m_MC_GD_MOTHER_KEY)");

          H.AddFormula("Bu_ID", MCTruthMathcing_Bu);

          H.AddCut("High_Q2_BuID",      { "BDTCut", "HighQ2", "Bu_ID"});
          H.AddCut("Low_Q2_BuID",       { "BDTCut", "LowQ2" , "Bu_ID"});
          H.AddCut("Is_Q2_BuID",        { "BDTCut", "IsQ2"  , "Bu_ID"});
          H.AddCut("No_Q2_BuID",        { "BDTCut"          , "Bu_ID"});
          H.AddCut("High_Q2_NoBDT_BuID",{           "HighQ2", "Bu_ID"});
          H.AddCut("Low_Q2_NoBDT_BuID", {           "LowQ2" , "Bu_ID"});
          H.AddCut("Is_Q2_NoBDT_BuID",  {           "IsQ2"  , "Bu_ID"});
          H.AddCut("No_Q2_NoBDT_BuID",  {                     "Bu_ID"});

          for(auto& Cut: vector<string>{"02","04","06","08","10","12","14","16"} )
          {

            H.AddCut("Q2_" + Cut + "_High_BuID",       {"Trig_TOS", "BDTCut", "Cut_Iso", "Q2_" + Cut + "_High", "Bu_ID"} );
            H.AddCut("Q2_" + Cut + "_Low_BuID" ,       {"Trig_TOS", "BDTCut", "Cut_Iso", "Q2_" + Cut + "_Low" , "Bu_ID"} );

            H.AddCut("Q2_" + Cut + "_High_NoBDT_BuID", {"Trig_TOS",           "Cut_Iso", "Q2_" + Cut + "_High", "Bu_ID"} );
            H.AddCut("Q2_" + Cut + "_Low_NoBDT_BuID" , {"Trig_TOS",           "Cut_Iso", "Q2_" + Cut + "_Low" , "Bu_ID"} );

          }

      }


      if (evtype == "11874010")
      {
          const TCut TrueMuandK("(abs(muon_p_TRUEID)==13 && abs(kaon_m_TRUEID)==321) &&(abs(kaon_m_MC_MOTHER_ID) == 421)");
          const TCut MCTruthMathcing_Bd = TrueMuandK * TCut("(abs(muon_p_MC_MOTHER_ID)== 511 ) && (muon_p_MC_MOTHER_KEY == kaon_m_MC_GD_GD_MOTHER_KEY)");

          H.AddFormula("Bd_ID", MCTruthMathcing_Bd);

          H.AddCut("High_Q2_BdID",      { "BDTCut", "HighQ2", "Bd_ID"});
          H.AddCut("Low_Q2_BdID",       { "BDTCut", "LowQ2" , "Bd_ID"});
          H.AddCut("Is_Q2_BdID",        { "BDTCut", "IsQ2"  , "Bd_ID"});
          H.AddCut("No_Q2_BdID",        { "BDTCut"          , "Bd_ID"});
          H.AddCut("High_Q2_NoBDT_BdID",{           "HighQ2", "Bd_ID"});
          H.AddCut("Low_Q2_NoBDT_BdID", {           "LowQ2" , "Bd_ID"});
          H.AddCut("Is_Q2_NoBDT_BdID",  {           "IsQ2"  , "Bd_ID"});
          H.AddCut("No_Q2_NoBDT_BdID",  {                     "Bd_ID"});

          for(auto& Cut: vector<string>{"02","04","06","08","10","12","14","16"} )
          {

            H.AddCut("Q2_" + Cut + "_High_BdID",       {"Trig_TOS", "BDTCut", "Cut_Iso", "Q2_" + Cut + "_High", "Bd_ID"} );
            H.AddCut("Q2_" + Cut + "_Low_BdID" ,       {"Trig_TOS", "BDTCut", "Cut_Iso", "Q2_" + Cut + "_Low" , "Bd_ID"} );

            H.AddCut("Q2_" + Cut + "_High_NoBDT_BdID", {"Trig_TOS",           "Cut_Iso", "Q2_" + Cut + "_High", "Bd_ID"} );
            H.AddCut("Q2_" + Cut + "_Low_NoBDT_BdID" , {"Trig_TOS",           "Cut_Iso", "Q2_" + Cut + "_Low" , "Bd_ID"} );

          }


      }
    */
      // Fix this shite later




      H.InitialiseDataType(MCType);
      H.run("/eos/lhcb/wg/semileptonic/Bs2KmunuAna/Tuples/Bs2KMuNu_MCTUPLES_TRIMMED_30May16/DTT_" + evtype + "*MC12*root", "Bs2KmuNuTuple");
      H.FinaliseDataType();

      vector<std::string> SSTypes{"10010015", "11676001", "11874010", "11874042", "12873002", "10010035", "11874004"};

      if (std::find(SSTypes.begin(), SSTypes.end(), evtype) != SSTypes.end())
      {

          H.InitialiseDataType(MCType+"_SS");
          H.run("/eos/lhcb/wg/semileptonic/Bs2KmunuAna/Tuples/Bs2KMuNu_MCTUPLES_TRIMMED_30May16/DTT_" + evtype + "*MC12*root", "Bs2KmuNuSSTuple");
          H.FinaliseDataType();


      }

/*

      if (evtype == "12143001" )
      {
          H.DeleteFormula("Iso_ID");

          H.DeleteCut("High_Q2_IsoID"      );
          H.DeleteCut("Low_Q2_IsoID"       );
          H.DeleteCut("Is_Q2_IsoID"        );
          H.DeleteCut("No_Q2_IsoID"        );
          H.DeleteCut("High_Q2_NoBDT_IsoID");
          H.DeleteCut("Low_Q2_NoBDT_IsoID" );
          H.DeleteCut("Is_Q2_NoBDT_IsoID"  );
          H.DeleteCut("No_Q2_NoBDT_IsoID"  );
          for(auto& Cut: vector<string>{"02","04","06","08","10","12","14","16"} )
          {
            H.DeleteCut("Q2_" + Cut + "_High_IsoID"      );
            H.DeleteCut("Q2_" + Cut + "_Low_IsoID"       );
            H.DeleteCut("Q2_" + Cut + "_High_NoBDT_IsoID");
            H.DeleteCut("Q2_" + Cut + "_Low_NoBDT_IsoID" );
          }


      }

      if (evtype == "12873002" )
      {
          H.DeleteFormula("Bu_ID");

          H.DeleteCut("High_Q2_BuID"      );
          H.DeleteCut("Low_Q2_BuID"       );
          H.DeleteCut("Is_Q2_BuID"        );
          H.DeleteCut("No_Q2_BuID"        );
          H.DeleteCut("High_Q2_NoBDT_BuID");
          H.DeleteCut("Low_Q2_NoBDT_BuID" );
          H.DeleteCut("Is_Q2_NoBDT_BuID"  );
          H.DeleteCut("No_Q2_NoBDT_BuID"  );
          for(auto& Cut: vector<string>{"02","04","06","08","10","12","14","16"} )
          {
            H.DeleteCut("Q2_" + Cut + "_High_BuID"      );
            H.DeleteCut("Q2_" + Cut + "_Low_BuID"       );
            H.DeleteCut("Q2_" + Cut + "_High_NoBDT_BuID");
            H.DeleteCut("Q2_" + Cut + "_Low_NoBDT_BuID" );
          }


      }
      if (evtype == "11874010" )
      {
          H.DeleteFormula("Bd_ID");

          H.DeleteCut("High_Q2_BdID"      );
          H.DeleteCut("Low_Q2_BdID"       );
          H.DeleteCut("Is_Q2_BdID"        );
          H.DeleteCut("No_Q2_BdID"        );
          H.DeleteCut("High_Q2_NoBDT_BdID");
          H.DeleteCut("Low_Q2_NoBDT_BdID" );
          H.DeleteCut("Is_Q2_NoBDT_BdID"  );
          H.DeleteCut("No_Q2_NoBDT_BdID"  );
          for(auto& Cut: vector<string>{"02","04","06","08","10","12","14","16"} )
          {
            H.DeleteCut("Q2_" + Cut + "_High_BdID"      );
            H.DeleteCut("Q2_" + Cut + "_Low_BdID"       );
            H.DeleteCut("Q2_" + Cut + "_High_NoBDT_BdID");
            H.DeleteCut("Q2_" + Cut + "_Low_NoBDT_BdID" );
          }


      }

*/

  }
}

#endif
