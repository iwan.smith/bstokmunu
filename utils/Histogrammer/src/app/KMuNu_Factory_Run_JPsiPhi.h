#ifndef KMUNU_FACTORY_RUN_JPSIPHI
#define KMUNU_FACTORY_RUN_JPSIPHI

#include <string>
#include <cstring>
#include <cstdlib>
#include <map>
#include <utility>
#include <fstream>
#include <iostream>
#include <cstdlib>
#include <time.h>

#include "TCanvas.h"
#include "TFile.h"
#include "TTree.h"
#include "TTreeFormula.h"
#include "TChain.h"
#include "TH1.h"
#include "TMath.h"
#include "TRandom.h"
#include "TLorentzVector.h"

#include "Histogrammer.h"
#include "HistogrammerFunctions.h"
#include "Histogrammer_AddOn.h"
//Test file to make sure we can do a simple loop and write a Histogram.

void Factory_Run_JPsiPhi(
    std::string OutputFile, std::string FriendFileName,
    int MaxEv, int MaxFiles, int PrintFrequency, bool SaveTree, bool SaveNorm, bool RunSilent,
    bool Run_JPsiPhi, bool Skip_Data, bool Skip_MCReco,
    map<string, Systematic*>& Systematics_JpsiPhi )
{



  if ( Run_JPsiPhi ) {
      Histogrammer H("JPsiPhi", OutputFile, FriendFileName);
      H.SetMaxEv       (MaxEv);
      H.SetMaxFiles    (MaxFiles);
      H.SetPrintFreq   (PrintFrequency);
      H.SaveFriendTree (SaveTree);
      H.SaveNorm       (SaveNorm);
      H.SetSilent(RunSilent);

      H.SetCommonCut("Bs_MM > 5260 && Bs_MM < 5480");
      H.AddCut("Nominal", "1");
      H.AddCut("Trigger_TOS",    "Bs_Hlt2TopoMu2BodyBBDTDecision_TOS");
      H.AddCut("Trigger_TIS",    "Bs_Hlt2TopoMu2BodyBBDTDecision_TIS");
      H.AddCut("Trigger_TISTOS", "Bs_Hlt2TopoMu2BodyBBDTDecision_TOS && Bs_Hlt2TopoMu2BodyBBDTDecision_TIS");


      H.AddBranch("nTracks");
      //H.AddBranch("Bs_M_JpsiConstr", "Bs_M");
      H.AddBranch("Bs_M");

      H.AddBranch("Bs_IP_OWNPV");
      H.AddBranch("Bs_IPCHI2_OWNPV");
      H.AddBranch("Bs_FD_OWNPV");
      H.AddBranch("Bs_ETA", "B_ETA");
      H.AddBranch("Bs_PT",  "B_PT");
      H.AddBranch("Bs_VCHI2_LOKI");

      H.AddBranch("muon_p_PZ");
      H.AddBranch("muon_m_PZ");
      H.AddBranch("kaon_p_PZ");
      H.AddBranch("kaon_m_PZ");

      H.AddBranch("muon_p_P");
      H.AddBranch("muon_m_P");
      H.AddBranch("kaon_p_P");
      H.AddBranch("kaon_m_P");

      H.AddFunction("kaon_m_ETA", Functions_Generic::Part_ETA, new std::string("kaon_m"));
      H.AddFunction("kaon_p_ETA", Functions_Generic::Part_ETA, new std::string("kaon_p"));
      H.AddFunction("muon_m_ETA", Functions_Generic::Part_ETA, new std::string("muon_m"));
      H.AddFunction("muon_p_ETA", Functions_Generic::Part_ETA, new std::string("muon_p"));
      H.AddFunction("MagnetPol",  Functions_Generic::MagnetPolarity, &H.currentFile);

      H.AddSystematic("Nominal",      Systematics_JpsiPhi.at("Nominal"));

      H.AddSystematic("sWeight",      Systematics_JpsiPhi.at("sWeight"));
      //H.AddSystematic("sWeight_BG",   Systematics_JpsiPhi.at("sWeight_BG"));

      H.AddHistogram("h_nTracks", "Track Multiplicity", 1000, 0, 1000, "nTracks");
      H.AddHistogram("h_B_MM",           "B^{+} mass ", 200, 5260, 5480, "Bs_M");
      H.AddHistogram("h_B_MM2",           "B^{+} mass ", 50, 5260, 5480, "Bs_M");
      H.AddHistogram("h_B_IP_OWNPV",     "Bu IP PV", 100, 0, 0.1, "Bs_IP_OWNPV");
      H.AddHistogram("h_B_IPCHI2_OWNPV", "Bu IPCHI2 OWNPV", 100, 0, 20, "Bs_IPCHI2_OWNPV");
      H.AddHistogram("h_B_FD_OWNPV",     "B^{+} Flight Distance", 100, 0, 60, "Bs_FD_OWNPV" );
      H.AddHistogram("h_B_ETA",          "B^{+} #eta", 1000, 1.5, 7, "B_ETA");
      H.AddHistogram("h_B_VCHI2_LOKI",   "B^{+} vertex chi2", 100, 0, 10, "Bs_VCHI2_LOKI" );

      H.AddHistogram("h_nTracks2",
              new TH1F("h_nTracks2", "Track Multiplicity", 41,
                  &std::vector<double>{0, 38, 47, 55, 61, 66, 71, 76, 80, 85, 89, 93, 97, 101, 105, 109, 113, 118, 122, 126, 130, 135, 140, 144, 149, 155, 160, 166, 172, 178, 185, 193, 202, 211, 222, 234, 248, 265, 288, 320, 377, 1000}[0]),
              "nTracks");

      H.AddHistogram("h_nTracks3", "Track Multiplicity", 50, 0, 600, "nTracks");
      H.AddHistogram("h_B_ETA2",   "B^{+} #eta", 50, 1.5, 6, "B_ETA");

      H.AddHistogram("h_B_ETA2",
              new TH1F("h_ETA2", "Pseudorapidity", 6,
                  &std::vector<double>{1.5, 2.7485, 3.051, 3.3205, 3.612, 4.0, 7.0}[0]),
              "B_ETA");

      H.AddHistogram("h_B_ETA_NT",
              new TH2F( "h_B_ETA_NT", "Track Multiplicity vs Pseudorapidity",
              16, &std::vector<double>{0.0, 51.0, 66.0, 78.0, 89.0, 99.0, 110.0, 120.0, 132.0, 144.0, 157.0, 172.0, 190.0, 213.0, 245.0, 299.0, 941.0}[0],
              6,  &std::vector<double>{1.5, 2.7485, 3.051, 3.3205, 3.612, 4.0, 7.0}[0]),
              "nTracks", "B_ETA");


      H.AddHistogram("h_B_ETA_PT",
              new TH2F( "h_B_ETA_PT", "pT vs Pseudorapidity",
              16, &std::vector<double>{0.0, 2240.0, 3360.0, 4320.0, 5120.0, 5800.0, 6480.0, 7080.0, 7720.0, 8400.0, 9120.0, 9920.0, 10880.0, 12080.0, 13720.0, 16600.0, 39960.0}[0],
              6,  &std::vector<double>{1.5, 2.7485, 3.051, 3.3205, 3.612, 4.0, 7.0}[0]),
              "B_PT", "B_ETA");


      H.SetPrintHist("h_B_MM2", "Nominal", "Nominal");


      if ( !Skip_Data )
      {
          H.InitialiseDataType("Data");
          H.run("/eos/lhcb/wg/semileptonic/Bs2KmunuAna/Tuples/B2JpsiX_DATATUPLES_RAW_22October17/DTT_2012_Reco14Strip21_*_DIMUON.root", "Bs2JpsiPhiTuple/DecayTree");
          H.FinaliseDataType();
      }

      //H.DeleteBranch("Bs_M");
      //H.AddBranch("Bs_M");

      H.AddSystematic("Tracking_K_m",  Systematics_JpsiPhi.at("Tracking_K_m"));
      H.AddSystematic("Tracking_K_p",  Systematics_JpsiPhi.at("Tracking_K_p"));
      H.AddSystematic("Tracking_Mu_m", Systematics_JpsiPhi.at("Tracking_Mu_m"));
      H.AddSystematic("Tracking_Mu_p", Systematics_JpsiPhi.at("Tracking_Mu_p"));
      H.AddSystematic("Tracking_Comb", Systematics_JpsiPhi.at("Tracking_Comb_NT"));

      H.AddFormula("RW_BDT", "1");
      H.AddSystematic("RW_Kin",        Systematics_JpsiPhi.at("RW_Kin"));

      H.AddSystematic("PID_K_m",       Systematics_JpsiPhi.at("PID_K_m"));
      H.AddSystematic("PID_K_p",       Systematics_JpsiPhi.at("PID_K_p"));
      H.AddSystematic("PID_Mu_p",      Systematics_JpsiPhi.at("PID_Mu_p"));
      H.AddSystematic("PID_Mu_m",      Systematics_JpsiPhi.at("PID_Mu_m"));
      H.AddSystematic("PID_Comb",      Systematics_JpsiPhi.at("PID_Comb_NT"));

      //H.AddSystematic("Default",       Systematics_JpsiPhi.at("Default"));
      H.AddSystematic("Default",       Systematics_JpsiPhi.at("Nominal"));






      if ( !Skip_MCReco)
      {
          H.InitialiseDataType("Signal_MC");
          H.run("/eos/lhcb/wg/semileptonic/Bs2KmunuAna/Tuples/B2JpsiX_MCTUPLES_RAW_12November16/DTT_13144001_Bs_Jpsiphi_mm_CPV_update2012_DecProdCut_*_Py8_MC12.root", "Bs2JpsiPhiTuple/DecayTree");
          H.FinaliseDataType();
      }


  }



}

#endif
