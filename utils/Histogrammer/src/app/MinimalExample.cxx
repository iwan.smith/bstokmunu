/** \file
 * MinimalExample.cxx
 *
 *  Created on: 21 Jul 2017
 *      Author: ismith
 *
 *  # Minimal Example of usage
 *
 *  A small example is presented showing the steps that need to be taken to process a file and apply a few corrections.
 *
 *  ### The steps needed:
 *  - Declare the corrections/systematics which are applied
 *    -# These can be declared before the corresponding lookup variables
 *  - Initialise the Histogrammer
 *    -# Set how many events/files to run over
 *    -# Set other options, e.g. print frequency
 *  - Specify which branches will be run over
 *  - Implement and add functions which will calculate variables on the fly
 *  - Specify output histograms
 *  - Run over the files:
 *    -# Initialise the datatype for your group of files
 *    -# Run over the files
 *    -# Finalise the histogrammer (Save the output)
 *
 */


#include <string>
#include <cstring>
#include <cstdlib>
#include <map>
#include <utility>
#include <fstream>
#include <iostream>
#include <cstdlib>
#include <time.h>

#include "TCanvas.h"
#include "TFile.h"
#include "TTree.h"
#include "TTreeFormula.h"
#include "TChain.h"
#include "TH1.h"
#include "TMath.h"
#include "TRandom.h"

#include "TMath.h"

#include "Histogrammer.h"



double CalculateETA(std::map<std::string, Variable*>& Vars, void* ParticleName)
{
/**
 * Function used to calculate the pseudorapidity of a particle.
 * This is called automatically by the Histogrammer
 */

  std::string& PartName = *((std::string*)ParticleName);

  double X_PZ = Vars[PartName + "_PZ"]->GetVar();
  double X_P  = Vars[PartName + "_P"]->GetVar();

  return TMath::ATanH(X_PZ / X_P);
}



int main(){

  int nToys = 10;

  // Start by declaring the corrections.
  // We will apply a tracking correction, which also needs a reweighting in nTracks

  // Declare the initial tracking correction.
  Systematic_Hist Syst_Tracking   (nToys, "root://eoslhcb.cern.ch//eos/lhcb/wg/semileptonic/Bs2KmunuAna/Histogrammer/Input/Tracking/ratio2012S20_MeV.root", "Ratio", OverflowTreatment::Closest);

  // Make a copy for each final state particle ( This way toys are preserved between samples)
  Systematic_Hist Syst_Tracking_Km( Syst_Tracking, "kaon_m_P", "kaon_m_ETA"  );
  Systematic_Hist Syst_Tracking_Mu( Syst_Tracking, "muon_p_P", "muon_p_ETA" );

  // Declare the nTracks ReWeighting
  Systematic_Hist Syst_NT(nToys, "root://eoslhcb.cern.ch//eos/lhcb/wg/semileptonic/Bs2KmunuAna/Histogrammer/Input/NTracks/weights.root", "tracks_weight", OverflowTreatment::Closest, "nTracks");

  // Combine them
  Systematic_Combiner Tracking_Correction(&Syst_Tracking_Km, &Syst_Tracking_Mu, &Syst_NT);




  Histogrammer Histogrammer_KMuNu("KMuNu", "SmallExample.root");
  Histogrammer_KMuNu.SetMaxEv      (10000);
  Histogrammer_KMuNu.SetMaxFiles   (2);
  Histogrammer_KMuNu.SetPrintFreq  (1000);
  Histogrammer_KMuNu.SaveFriendTree(false); // We won't save the variables to a tree for now


  // Add the branches we want to analyse

  Histogrammer_KMuNu.AddBranch("nTracks");
  Histogrammer_KMuNu.AddBranch("Bs_MCORR");

  Histogrammer_KMuNu.AddBranch("kaon_m_PZ");
  Histogrammer_KMuNu.AddBranch("kaon_m_P");
  Histogrammer_KMuNu.AddBranch("muon_p_PZ");
  Histogrammer_KMuNu.AddBranch("muon_p_P");


  // And whoever made the NTuple forgot to add the ETA variable, so we will make it on the fly
  Histogrammer_KMuNu.AddFunction("kaon_m_ETA", CalculateETA, new std::string( "kaon_m" ));
  Histogrammer_KMuNu.AddFunction("muon_p_ETA", CalculateETA, new std::string( "muon_p" ));

  // Now we have all the variables we can declare a Histogram

  Histogrammer_KMuNu.AddHistogram("h_Bs_MCORR", "B_{s} Corrected Mass", 60, 2500, 5370, "Bs_MCORR" );

  // Now let's run over some data
  // Note, you can use Wildcards in your file name. This also works when running with xrootd
  Histogrammer_KMuNu.InitialiseDataType("Data");
  Histogrammer_KMuNu.run("root://eoslhcb.cern.ch//eos/lhcb/wg/semileptonic/Bs2KmunuAna/Tuples/Bs2KMuNu_DATATUPLES_TRIMMED_19June16/*_DTT_2012_Reco14Strip21r0p1a_*.root", "reducedTree");
  Histogrammer_KMuNu.FinaliseDataType();


  // Now let's run over some Monte Carlo
  // We didn't need a tracking correction for data, so we add it now
  Histogrammer_KMuNu.AddSystematic("Tracking_Km", &Syst_Tracking_Km);
  Histogrammer_KMuNu.AddSystematic("Tracking_Mu", &Syst_Tracking_Mu);
  Histogrammer_KMuNu.AddSystematic("ReWeight_nTracks", &Syst_NT);

  Histogrammer_KMuNu.AddSystematic("TrackingCorrection", &Tracking_Correction);

  Histogrammer_KMuNu.InitialiseDataType("MonteCarlo");
  Histogrammer_KMuNu.run("root://eoslhcb.cern.ch//eos/lhcb/wg/semileptonic/Bs2KmunuAna/Tuples/Bs2KMuNu_MCTUPLES_TRIMMED_BDT_30May16/DTT_13512010*MC12*.root", "reducedTree");
  Histogrammer_KMuNu.FinaliseDataType();




}



