#ifndef KMUNU_FACTORY_RUN_KMUNU_DETAILED
#define KMUNU_FACTORY_RUN_KMUNU_DETAILED



#include <string>
#include <cstring>
#include <cstdlib>
#include <map>
#include <utility>
#include <fstream>
#include <iostream>
#include <cstdlib>
#include <time.h>

#include "TCanvas.h"
#include "TFile.h"
#include "TTree.h"
#include "TTreeFormula.h"
#include "TChain.h"
#include "TH1.h"
#include "TMath.h"
#include "TRandom.h"
#include "TLorentzVector.h"

#include "Histogrammer.h"
#include "HistogrammerFunctions.h"
#include "Histogrammer_AddOn.h"
//Test file to make sure we can do a simple loop and write a Histogram.

#include "HistogrammerFuncs.h"
#include "KMuNu_Common_Functions.h"

void Factory_Run_KMuNu_Detailed(
    std::string OutputFile, std::string FriendFileName,
    int MaxEv, int MaxFiles, int PrintFrequency, bool SaveTree, bool SaveNorm, bool RunSilent,
    bool Run_KMuNu, bool Skip_Data, bool Skip_MCReco, bool Skip_MCGen, bool RunFast,
    map<string, Systematic*>& Systematics_KMuNu )
{




  if ( Run_KMuNu ) {
      Histogrammer H("KMuNu", OutputFile, FriendFileName);
      H.SetMaxEv       (MaxEv);
      H.SetMaxFiles    (MaxFiles);
      H.SetPrintFreq   (PrintFrequency);
      H.SaveFriendTree (SaveTree);
      H.SaveNorm       (SaveNorm);
      H.SetSilent(RunSilent);

      H.AddCut("Nominal", "1");
      H.AddCut("GenCut",
             "0.01 < TMath::ATan(Kminus_TRUEPT/Kminus_TRUEP_Z) && 0.4  > TMath::ATan(Kminus_TRUEPT/Kminus_TRUEP_Z)"
          "&& 0.01 < TMath::ATan(muplus_TRUEPT/muplus_TRUEP_Z) && 0.4  > TMath::ATan(muplus_TRUEPT/muplus_TRUEP_Z)"
      );

      H.AddAlias("True_Q2", "(B_s0_TRUEP_E - Kminus_TRUEP_E)**2 - (B_s0_TRUEP_X - Kminus_TRUEP_X)**2 - (B_s0_TRUEP_Y - Kminus_TRUEP_Y)**2 - (B_s0_TRUEP_Z - Kminus_TRUEP_Z)**2");
      H.AddFormula("Q2", "True_Q2");

      H.AddCut("High_Q2", "True_Q2>7e6");
      H.AddCut("Low_Q2",  "True_Q2<7e6");


      H.AddSystematic("Nominal", Systematics_KMuNu.at("Nominal"));

      H.AddHistogram("True_Q2", "True q^{2}", 40, 0, 25e6, "Q2");


      for( auto &Particle: vector<string>{"Kminus", "muplus"})
      {

          H.AddBranch(Particle + "_TRUEP_E");
          H.AddBranch(Particle + "_TRUEP_X");
          H.AddBranch(Particle + "_TRUEP_Y");
          H.AddBranch(Particle + "_TRUEP_Z");
          H.AddBranch(Particle + "_TRUEPT");


          H.AddHistogram(Particle + "_theta", Particle + " Theta", 100, 0, 2,
              [Particle](std::map<std::string, Variable*>& Vars, void* Add_Arg)->double
                  {
                      const double& K_PT = Vars[Particle + "_TRUEPT"]->GetVar();
                      const double& K_PZ = Vars[Particle + "_TRUEP_Z"]->GetVar();

                      double theta = TMath::ATan(K_PT/K_PZ);

                      return theta > 0 ? theta : theta + M_PI;
                  });

          H.AddHistogram((Particle + "_theta2").c_str(), (Particle + " Theta").c_str(), 40, 0, 0.4, Particle + "_theta");

          H.AddHistogram(Particle+"_ETA", Particle+" ETA", 100, 0, 7,
              [Particle](std::map<std::string, Variable*>& Vars, void* Add_Arg)->double
                  {
                      const double& K_PT = Vars[Particle + "_TRUEPT"]->GetVar();
                      const double& K_PZ = Vars[Particle + "_TRUEP_Z"]->GetVar();
                      double K_P = TMath::Sqrt( K_PT*K_PT + K_PZ*K_PZ );

                      return TMath::ATanH(K_PZ/K_P);

                  });


      }
      H.AddBranch("B_s0_TRUEP_E");
      H.AddBranch("B_s0_TRUEP_X");
      H.AddBranch("B_s0_TRUEP_Y");
      H.AddBranch("B_s0_TRUEP_Z");
      H.AddBranch("B_s0_TRUEPT", "B_PT");

      H.AddHistogram("B_ETA", "B_{s} ETA", 100, 0, 7,
          [](std::map<std::string, Variable*>& Vars, void* Add_Arg)->double
              {

                  const double& K_PT = Vars["B_PT"]->GetVar();
                  const double& K_PZ = Vars["B_s0_TRUEP_Z"]->GetVar();
                  double K_P = TMath::Sqrt( K_PT*K_PT + K_PZ*K_PZ );

                  return TMath::ATanH(K_PZ/K_P);

              });

      //H.AddHistogram("B_s0_pTETA", "ETA against pT", 500, 0, 50000, 500, 0, 6, "B_PT", "B_s0_ETA");
      H.AddHistogram("B_s0_pTPZ", "ETA against pT", 500, 0, 50000, 500, 0, 1000000, "B_PT", "B_s0_TRUEP_Z");



      H.AddHistogram("Bs_PT", "B_{s} Transverse Momentum", 100, 0, 40000, "B_PT");
      H.AddHistogram("Bs_PE", "B_{s} Energy", 100, 0, 500000, "B_s0_TRUEP_E");


      H.AddFunction("TrueQ2",    Functions_KMuNu::MCTrueQ2);
      H.AddFunction("MagnetPol", Functions_Generic::MagnetPolarity, &H.currentFile);


      H.AddHistogram("h_CosTheta1", "helicityangle", 100, -1., 1.,
            [](std::map<std::string, Variable*>& Vars, void* Add_Arg)->double
          {

            const double& K_PX = Vars["Kminus_TRUEP_X"]->GetVar();
            const double& K_PY = Vars["Kminus_TRUEP_Y"]->GetVar();
            const double& K_PZ = Vars["Kminus_TRUEP_Z"]->GetVar();
            const double& K_PE = Vars["Kminus_TRUEP_E"]->GetVar();


            const double& Mu_PX = Vars["muplus_TRUEP_X"]->GetVar();
            const double& Mu_PY = Vars["muplus_TRUEP_Y"]->GetVar();
            const double& Mu_PZ = Vars["muplus_TRUEP_Z"]->GetVar();
            const double& Mu_PE = Vars["muplus_TRUEP_E"]->GetVar();


            const double& B_PX = Vars["B_s0_TRUEP_X"]->GetVar();
            const double& B_PY = Vars["B_s0_TRUEP_Y"]->GetVar();
            const double& B_PZ = Vars["B_s0_TRUEP_Z"]->GetVar();
            const double& B_PE = Vars["B_s0_TRUEP_E"]->GetVar();


            TLorentzVector TK (K_PX,  K_PY,  K_PZ,  K_PE );
            TLorentzVector TMu(Mu_PX, Mu_PY, Mu_PZ, Mu_PE);
            TLorentzVector TB (B_PX,  B_PY,  B_PZ,  B_PE );



            return calcCosTheta(TB, TB-TK, TMu);

          }
      );


      H.SetPrintHist("True_Q2", "Nominal", "Nominal");


      if ( !Skip_MCGen)
      {

          RunOverGeneratedMC(H, Systematics_KMuNu);
      }

      H.Reset();

      H.AddAlias("Q2", "Bs_Regression_Q2_BEST/1e6");

      //const TCut Cut_BDT     ("min(kaon_m_IsoMinBDT,muon_p_IsoMinBDT)> -0.9 && TMVA_SS_Afc_BDT_New > (0.07252675 - 0.00844996 * Bs_Regression_Q2_BEST / 1e6 ) && TMVA_Charge_BDT_New > ( 0.135 * exp( -0.0867 * Bs_Regression_Q2_BEST / 1e6) - 0.05 )");
      const TCut Cut_BDT        ("min(kaon_m_IsoMinBDT,muon_p_IsoMinBDT)> -0.9 && TMVA_Charge_BDT_New > (( 0.07778 - 0.00745*Q2 ) * (Q2>0) + 0.018 * (Q2<0)) && TMVA_SS_Afc_BDT_New > (( 0.08425 - 0.01574*Q2 + 0.00059*Q2**2 ) * (Q2>0) + 0.055625 * (Q2<0))");
      const TCut Cut_Iso        ("min(kaon_m_IsoMinBDT,muon_p_IsoMinBDT)> -0.9");
      //const TCut Cut_BDT        ("TMVA_Charge_BDT_New > 0.0843 && TMVA_SS_Afc_BDT_New > 0.061");
      //const TCut Cut_BDT        ("TMVA_Charge_BDT_New > 0.05 && TMVA_SS_Afc_BDT_New > 0.04");

      const TCut Cut_Trig_TOS       (" Bs_Hlt2TopoMu2BodyBBDTDecision_TOS");
      const TCut Cut_Trig_TISTOS    (" Bs_Hlt2TopoMu2BodyBBDTDecision_TIS && Bs_Hlt2TopoMu2BodyBBDTDecision_TOS");
      const TCut Cut_Trig_TISNotTOS (" Bs_Hlt2TopoMu2BodyBBDTDecision_TIS && !Bs_Hlt2TopoMu2BodyBBDTDecision_TOS");
      const TCut Cut_HighQ2     ("Q2 > 7");
      const TCut Cut_LowQ2      ("Q2 < 7 && Q2 > 0");
      const TCut Cut_IsQ2       ("Q2 > 0");



      const TCut Cut_Combi      ("(kaon_m_PX*muon_p_PX>0 || kaon_m_PY*muon_p_PY>0)");
      H.SetCommonCut(Cut_Combi);

      H.AddFormula("BDTCut" ,        Cut_BDT );
      H.AddFormula("Cut_Iso" ,       Cut_Iso );
      H.AddFormula("Trig_TOS",       Cut_Trig_TOS );
      H.AddFormula("Trig_TISTOS",    Cut_Trig_TISTOS );
      H.AddFormula("Trig_TISNotTOS", Cut_Trig_TISNotTOS );

      H.AddFormula("HighQ2", Cut_HighQ2);
      H.AddFormula("LowQ2",  Cut_LowQ2 );
      H.AddFormula("IsQ2",   Cut_IsQ2  );


      H.AddCut("Trigger_TISTOS",    {"Trig_TISTOS"});
      H.AddCut("Trigger_TISNotTOS", {"Trig_TISNotTOS"});



      // Cuts will be evaluated from Left to Right, so place the tightest cuts to the left.
      H.AddCut("High_Q2",       {"Trig_TOS", "BDTCut", "Cut_Iso", "HighQ2"});
      H.AddCut("Low_Q2",        {"Trig_TOS", "BDTCut", "Cut_Iso", "LowQ2" });
      H.AddCut("Is_Q2",         {"Trig_TOS", "BDTCut", "Cut_Iso", "IsQ2"  });
      H.AddCut("No_Q2",         {"Trig_TOS", "BDTCut", "Cut_Iso",         });
      H.AddCut("High_Q2_NoBDT", {"Trig_TOS",           "Cut_Iso", "HighQ2"});
      H.AddCut("Low_Q2_NoBDT",  {"Trig_TOS",           "Cut_Iso", "LowQ2" });
      H.AddCut("Is_Q2_NoBDT",   {"Trig_TOS",           "Cut_Iso", "IsQ2"  });
      H.AddCut("No_Q2_NoBDT",   {"Trig_TOS",           "Cut_Iso"          });


      H.AddFormula("Bu_MM", "sqrt((Bs_PE+muon_p_NIsoTr_PE)**2 - (Bs_PX+muon_p_NIsoTr_PX)**2 - (Bs_PY+muon_p_NIsoTr_PY)**2 - (Bs_PZ+muon_p_NIsoTr_PZ)**2)");
      H.AddFormula("B_CombiX", "kaon_m_PX*muon_p_PX");
      H.AddFormula("B_CombiY", "kaon_m_PY*muon_p_PY");


      H.AddBranch("nTracks");
      H.AddBranch("kaon_m_PZ");
      H.AddBranch("Bs_MCORR");
      H.AddBranch("Bs_MCORRERR");

      H.AddBranch("Bs_ETA", "B_ETA");
      H.AddBranch("Bs_PT",  "B_PT");
      H.AddBranch("Bs_PX",  "B_PX");
      H.AddBranch("Bs_PY",  "B_PY");
      H.AddBranch("Bs_PZ",  "B_PZ");

      H.AddBranch("kaon_m_PX");
      H.AddBranch("kaon_m_PY");
      H.AddBranch("kaon_m_PZ");
      H.AddBranch("kaon_m_PE");
      H.AddBranch("kaon_m_P");

      H.AddBranch("muon_p_PX");
      H.AddBranch("muon_p_PY");
      H.AddBranch("muon_p_PZ");
      H.AddBranch("muon_p_PE");
      H.AddBranch("muon_p_P");

      H.AddBranch("Bs_ENDVERTEX_CHI2");
      H.AddBranch("Bs_DOCA");

      H.AddBranch("Bs_Regression_Q2_BEST");


      H.AddFunction("kaon_m_ETA", Functions_Generic::Part_ETA, new std::string( "kaon_m" ));
      H.AddFunction("muon_p_ETA", Functions_Generic::Part_ETA, new std::string( "muon_p" ));
      H.AddFunction("MagnetPol",  Functions_Generic::MagnetPolarity, &H.currentFile);

      H.AddSystematic("sWeight",      Systematics_KMuNu.at("sWeight"));
      //H.AddSystematic("sWeight_BG",   Systematics_KMuNu.at("sWeight_BG"));

      H.AddSystematic("Nominal",       Systematics_KMuNu.at("Nominal"));




      H.AddFormula("BDT_V00", "Bs_ENDVERTEX_CHI2"                            );
      H.AddFormula("BDT_V01", "kaon_m_PT"                                    );
      H.AddFormula("BDT_V02", "Bs_PT"                                        );
      H.AddFormula("BDT_V03", "Bs_PT-muon_p_PT*1.5"                          );
      H.AddFormula("BDT_V04", "muon_p_0.50_cc_asy_P"                         );
      H.AddFormula("BDT_V05", "kaon_m_0.50_IT"                               );
      H.AddFormula("BDT_V06", "kaon_m_0.50_cc_deltaEta"                      );
      H.AddFormula("BDT_V07", "kaon_m_0.50_cc_IT"                            );
      H.AddFormula("BDT_V08", "min(muon_p_ConeIso, kaon_m_ConeIso)"          );
      H.AddFormula("BDT_V09", "max(kaon_m_IsoSumBDT, muon_p_IsoSumBDT)"      );
      H.AddFormula("BDT_V10", "min(kaon_m_IsoMinBDT, muon_p_IsoMinBDT)"      );
      H.AddFormula("BDT_V11", "kaon_m_IsoMinBDT - muon_p_IsoMinBDT"          );
      H.AddFormula("BDT_V12", "muon_p_PAIR_M"                                );
      H.AddFormula("BDT_V13", "kaon_m_PAIR_M"                                );
      H.AddFormula("BDT_V14", "Bs_cosTheta1_star-Bs_cosTheta2_star"          );


      H.AddHistogram("BDT_V00", "Bs_ENDVERTEX_CHI2"                         ,100  ,0.    ,4.0     ,"BDT_V00" );
      H.AddHistogram("BDT_V01", "kaon_m_PT"                                 ,100  ,0        ,10000       ,"BDT_V01" );
      H.AddHistogram("BDT_V02", "Bs_PT"                                     ,100  ,0        ,20000   ,"BDT_V02" );
      H.AddHistogram("BDT_V03", "Bs_PT-muon_p_PT*1.5"                       ,100  ,-8000     ,10000    ,"BDT_V03" );
      H.AddHistogram("BDT_V04", "muon_p_0.50_cc_asy_P"                      ,100  ,-1       ,1.05    ,"BDT_V04" );
      H.AddHistogram("BDT_V05", "kaon_m_0.50_IT"                            ,100  ,0        ,1.05     ,"BDT_V05" );
      H.AddHistogram("BDT_V06", "kaon_m_0.50_cc_deltaEta"                   ,100  ,-1     ,5       ,"BDT_V06" );
      H.AddHistogram("BDT_V07", "kaon_m_0.50_cc_IT"                         ,100  ,-0.     ,1.05    ,"BDT_V07" );
      H.AddHistogram("BDT_V08", "min(muon_p_ConeIso, kaon_m_ConeIso)"       ,100  ,0        ,1.0    ,"BDT_V08" );
      H.AddHistogram("BDT_V09", "max(kaon_m_IsoSumBDT, muon_p_IsoSumBDT)"   ,100  ,-0.6        ,0.05    ,"BDT_V09" );
      H.AddHistogram("BDT_V10", "min(kaon_m_IsoMinBDT, muon_p_IsoMinBDT)"   ,100  ,-0.9     ,1     ,"BDT_V10" );
      H.AddHistogram("BDT_V11", "kaon_m_IsoMinBDT - muon_p_IsoMinBDT"       ,100  ,-1.5       ,1.5       ,"BDT_V11" );
      H.AddHistogram("BDT_V12", "muon_p_PAIR_M"                             ,100  ,0.     ,8000     ,"BDT_V12" );
      H.AddHistogram("BDT_V13", "kaon_m_PAIR_M"                             ,100  ,0      ,8000    ,"BDT_V13" );
      H.AddHistogram("BDT_V14", "Bs_cosTheta1_star-Bs_cosTheta2_star"       ,100  ,-2      ,2    ,"BDT_V14" );






      H.AddFormula("BDT_NC_V00", "kaon_m_PT"                                    );
      H.AddFormula("BDT_NC_V01", "Bs_PT"                                        );
      H.AddFormula("BDT_NC_V02", "Bs_PT-muon_p_PT*1.5"                          );
      H.AddFormula("BDT_NC_V03", "Bs_cosTheta1_star-Bs_cosTheta2_star"          );
      H.AddFormula("BDT_NC_V04", "Bs_DIRA_OWNPV"                                );
      H.AddFormula("BDT_NC_V05", "Bs_FD_S"                                      );
      H.AddFormula("BDT_NC_V06", "Bs_ENDVERTEX_CHI2"                            );
      H.AddFormula("BDT_NC_V07", "kaon_m_MasshPi0"                              );
      H.AddFormula("BDT_NC_V08", "kaon_m_1.00_nc_asy_PT"                        );
      H.AddFormula("BDT_NC_V09", "kaon_m_0.50_nc_IT"                            );



      H.AddHistogram("BDT_NC_V00"  ,"kaon_m_PT"                               ,100   , 0      ,10000       ,"BDT_NC_V00");
      H.AddHistogram("BDT_NC_V01"  ,"Bs_PT"                                   ,100   , 0      ,20000       ,"BDT_NC_V01");
      H.AddHistogram("BDT_NC_V02"  ,"Bs_PT-muon_p_PT*1.5"                     ,100   ,-8000   ,10000       ,"BDT_NC_V02");
      H.AddHistogram("BDT_NC_V03"  ,"Bs_cosTheta1_star-Bs_cosTheta2_star"     ,100   ,-2      ,2           ,"BDT_NC_V03");
      H.AddHistogram("BDT_NC_V04"  ,"Bs_DIRA_OWNPV"                           ,100   , 0.996   ,1.0         ,"BDT_NC_V04");
      H.AddHistogram("BDT_NC_V05"  ,"Bs_FD_S"                                 ,100   , 0      ,300         ,"BDT_NC_V05");
      H.AddHistogram("BDT_NC_V06"  ,"Bs_ENDVERTEX_CHI2"                       ,100   , 0      ,4.0         ,"BDT_NC_V06");
      H.AddHistogram("BDT_NC_V07"  ,"kaon_m_MasshPi0"                         ,100   , 0      ,8000        ,"BDT_NC_V07");
      H.AddHistogram("BDT_NC_V08"  ,"kaon_m_1.00_nc_asy_PT"                   ,100   ,-0.55   ,1.05        ,"BDT_NC_V08");
      H.AddHistogram("BDT_NC_V09"  ,"kaon_m_0.50_nc_IT"                       ,100   , 0.2    ,1.05         ,"BDT_NC_V09");


      H.AddBranch("TMVA_Charge_BDT_New");
      H.AddBranch("TMVA_SS_Afc_BDT_New");


      H.AddHistogram("TMVA_Charge_BDT_New"              ,"TMVA_Charge_BDT_New"              ,100, -0.65 ,0.35, "TMVA_Charge_BDT_New");
      H.AddHistogram("TMVA_Charge_BDT_New_2"            ,"TMVA_Charge_BDT_New"              ,1000, -0.65 ,0.35, "TMVA_Charge_BDT_New");

      H.AddHistogram("TMVA_SS_Afc_BDT_New"              ,"TMVA_SS_Afc_BDT_New"              ,100, -0.65 ,0.35, "TMVA_SS_Afc_BDT_New");
      H.AddHistogram("TMVA_SS_Afc_BDT_New_2"            ,"TMVA_SS_Afc_BDT_New"              ,1000, -0.65 ,0.35, "TMVA_SS_Afc_BDT_New");



      H.AddHistogram("h_K_ETA",      "K Pseudorapidity",     100, 2,     5,                "kaon_m_ETA" );
      H.AddHistogram("h_Bs_MCORR",   "B_{s} Corrected Mass", 60,  2500,  5370 ,            "Bs_MCORR"   );
      H.AddHistogram("h_Bs_MCORR2",  "B_{s} Corrected Mass", 80,  2500,  6500,             "Bs_MCORR"   );
      H.AddHistogram("h_Bs_MCORRERR","B_{s} Corrected Mass Error", 60,  0,  300 ,          "Bs_MCORRERR"   );
      H.AddHistogram("h_Q2_BEST",    "Best q^{2} solution",  50, 0e6, 25e6,                "Bs_Regression_Q2_BEST");
      H.AddHistogram("h_P_Vs_ETA",   "Momentum Against ETA", 100, 10000, 50000, 100, 2, 5, "kaon_m_P", "kaon_m_ETA" );
      H.AddHistogram("h_Bs_PT",      "p_{T} ( B_{s} )",      50, 0, 16000,                 "B_PT");
      H.AddHistogram("h_Quadrantcheck", "",      100, -10e6, 10e6, 100, -10e6, 10e6,       "B_CombiX", "B_CombiY");

      H.AddHistogram("h_nTracks", "Track Multiplicity", 1000, 0, 1000, "nTracks");

      H.AddHistogram("Bs_DOCA", "DOCA K #mu", 50, 0, 0.1, "Bs_DOCA");
      H.AddHistogram("Bs_ENDVERTEX_CHI2", "DOCA K #mu", 50, 0, 4, "Bs_ENDVERTEX_CHI2");

      H.AddHistogram("h_Bu_MM", "B^{+} # rightarrow J/#psi K", 100, 5180, 5400, "Bu_MM");






      //H.AddCut("TightVertex", "Bs_ENDVERTEX_CHI2 < 1");
      //H.AddCut("TightDOCA",   "Bs_DOCA < 0.025");

      H.AddHistogram("h_nTracks2",
              new TH1F("h_nTracks2", "Track Multiplicity", 41,
                  &std::vector<double>{0, 38, 47, 55, 61, 66, 71, 76, 80, 85, 89, 93, 97, 101, 105, 109, 113, 118, 122, 126, 130, 135, 140, 144, 149, 155, 160, 166, 172, 178, 185, 193, 202, 211, 222, 234, 248, 265, 288, 320, 377, 1000}[0]),
              "nTracks");

      TLorentzVector K, Mu, Y, B;
      H.AddHistogram("MissingMass", "Missing Mass", 100, -8e6, 8e6,
        [&K, &Mu, &Y, &B](std::map<std::string, Variable*>& Vars, void* Add_Arg)->double
          {


            const double& K_PX = Vars["kaon_m_PX"]->GetVar();
            const double& K_PY = Vars["kaon_m_PY"]->GetVar();
            const double& K_PZ = Vars["kaon_m_PZ"]->GetVar();

            K.SetXYZM(K_PX, K_PY, K_PZ, 493.7);

            const double& Mu_PX = Vars["muon_p_PX"]->GetVar();
            const double& Mu_PY = Vars["muon_p_PY"]->GetVar();
            const double& Mu_PZ = Vars["muon_p_PZ"]->GetVar();

            Mu.SetXYZM(Mu_PX, Mu_PY, Mu_PZ, 105.);

            Y = K + Mu;


            const double& B_PX = Vars["B_PX"]->GetVar();
            const double& B_PY = Vars["B_PY"]->GetVar();

            const double B_PZ_Guess = 5366.0 /  Y.M() * Y.Pz();

            B.SetXYZM( B_PX, B_PY, B_PZ_Guess, 5366.0);

            return (B - K - Mu).M2();

          }
      );




      H.AddBranch("Bs_Reg_Best_PX");
      H.AddBranch("Bs_Reg_Best_PY");
      H.AddBranch("Bs_Reg_Best_PZ");
      H.AddBranch("Bs_Reg_Best_PE");



      H.AddBranch("Bs_Reg_Worst_PX");
      H.AddBranch("Bs_Reg_Worst_PY");
      H.AddBranch("Bs_Reg_Worst_PZ");
      H.AddBranch("Bs_Reg_Worst_PE");


      H.AddHistogram("h_CosTheta_Best", "helicityangle", 100, -1.2, 1.2,
            [](std::map<std::string, Variable*>& Vars, void* Add_Arg)->double
          {

            const double& K_PX = Vars["kaon_m_PX"]->GetVar();
            const double& K_PY = Vars["kaon_m_PY"]->GetVar();
            const double& K_PZ = Vars["kaon_m_PZ"]->GetVar();
            const double& K_PE = Vars["kaon_m_PE"]->GetVar();


            const double& Mu_PX = Vars["muon_p_PX"]->GetVar();
            const double& Mu_PY = Vars["muon_p_PY"]->GetVar();
            const double& Mu_PZ = Vars["muon_p_PZ"]->GetVar();
            const double& Mu_PE = Vars["muon_p_PE"]->GetVar();


            const double& B_PX = Vars["Bs_Reg_Best_PX"]->GetVar();
            const double& B_PY = Vars["Bs_Reg_Best_PY"]->GetVar();
            const double& B_PZ = Vars["Bs_Reg_Best_PZ"]->GetVar();
            const double& B_PE = Vars["Bs_Reg_Best_PE"]->GetVar();


            TLorentzVector TK (K_PX,  K_PY,  K_PZ,  K_PE );
            TLorentzVector TMu(Mu_PX, Mu_PY, Mu_PZ, Mu_PE);
            TLorentzVector TB (B_PX,  B_PY,  B_PZ,  B_PE );



            return calcCosTheta(TB, TB-TK, TMu);

          }
      );

      H.AddHistogram("h_CosTheta_Worst", "helicityangle", 100, -1.2, 1.2,
            [](std::map<std::string, Variable*>& Vars, void* Add_Arg)->double
          {

            const double& K_PX = Vars["kaon_m_PX"]->GetVar();
            const double& K_PY = Vars["kaon_m_PY"]->GetVar();
            const double& K_PZ = Vars["kaon_m_PZ"]->GetVar();
            const double& K_PE = Vars["kaon_m_PE"]->GetVar();


            const double& Mu_PX = Vars["muon_p_PX"]->GetVar();
            const double& Mu_PY = Vars["muon_p_PY"]->GetVar();
            const double& Mu_PZ = Vars["muon_p_PZ"]->GetVar();
            const double& Mu_PE = Vars["muon_p_PE"]->GetVar();


            const double& B_PX = Vars["Bs_Reg_Worst_PX"]->GetVar();
            const double& B_PY = Vars["Bs_Reg_Worst_PY"]->GetVar();
            const double& B_PZ = Vars["Bs_Reg_Worst_PZ"]->GetVar();
            const double& B_PE = Vars["Bs_Reg_Worst_PE"]->GetVar();


            TLorentzVector TK (K_PX,  K_PY,  K_PZ,  K_PE );
            TLorentzVector TMu(Mu_PX, Mu_PY, Mu_PZ, Mu_PE);
            TLorentzVector TB (B_PX,  B_PY,  B_PZ,  B_PE );



            return calcCosTheta(TB, TB-TK, TMu);

          }
      );



      H.AddHistogram("h_CosTheta_Q2", "helicityangle", 100, -1.2, 1.2, 100, 0, 24e6, "h_CosTheta_Best", "Bs_Regression_Q2_BEST");


      H.SetPrintHist("h_Bs_MCORR", "Low_Q2", "Nominal");

      if ( !Skip_Data )
      {
          RunOverData(H, RunFast);
      }


      SetupSystematic_MonteCarlo(H, Systematics_KMuNu);

      H.AddBranch("Bs_Regression_Q2_TRUE");

      H.AddHistogram("h_Q2_True_Reco",   "TrueQ2 vs Reco Q2", 1000,    0, 25e6, 1000,    0, 25e6, "Bs_Regression_Q2_TRUE", "Bs_Regression_Q2_BEST");
      H.AddHistogram("h_Q2_True_Reco_2", "TrueQ2 vs Reco Q2", 1000, -1e6, 25e6, 1000, -1e6, 25e6, "Bs_Regression_Q2_TRUE", "Bs_Regression_Q2_BEST");



      if ( !Skip_MCReco)
      {

          RunOverMonteCarlo(H);

      }
  }

}

#endif
