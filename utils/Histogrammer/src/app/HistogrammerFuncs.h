#ifndef HISTOGRAMMER_FUNCTIONS
#define HISTOGRAMMER_FUNCTIONS


#include "TLorentzVector.h"
#include "TMath.h"

double calcCosTheta(const TLorentzVector& P, const TLorentzVector& Q, const TLorentzVector& D) {
    double PP = P.M2();
    double PD = P.Dot(D);
    double PQ = P.Dot(Q);
    double QQ = Q.M2();
    double QD = Q.Dot(D);
    double DD = D.M2();

    return (PD*QQ - PQ*QD)/TMath::Sqrt((PQ*PQ-PP*QQ)*(QD*QD-DD*QQ));
}


#endif
