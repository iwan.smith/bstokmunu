#include <iostream>
#include "TH1F.h"



std::ostream& operator<< (std::ostream& lhs, TH1F* rhs)
{

  using namespace std;
  

  const int bins = rhs->GetNbinsX();

  const float MaxY = rhs->GetMaximum();
  const float MinY = rhs->GetMinimum();

  lhs << "Name: " << rhs->GetName() << "  Title: " << rhs->GetTitle() << endl;



  for( int y = bins; y >= 0; y-=2)
  {

    if (y%10 == 0)
      lhs << setw(10) << MinY + (float)y/bins * (MaxY - MinY) << '|';
    else
      lhs << setw(11) << "|";

    for( int x = 0; x < bins; x++)
    {
      char pixel = '#';
      if ( rhs->GetBinContent(x+1) < (MinY + (float)y/bins * (MaxY - MinY)) )
        pixel = ' ';

      lhs << pixel;
    }
    lhs << endl;

  }

  lhs << setw(11) << '+';
  for( int x = 0; x < bins; x++)
  {
    lhs << '-';
  }

  lhs << endl;

  return lhs;
}


int main()
{


  TH1F* h = new TH1F("h1", "h1", 50, -3, 3);
  h->FillRandom("gaus", 10000);

  std::cout << h;


}
