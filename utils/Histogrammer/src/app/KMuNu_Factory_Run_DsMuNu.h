#ifndef KMUNU_FACTORY_RUN_DSMUNU
#define KMUNU_FACTORY_RUN_DSMUNU


#include <string>
#include <cstring>
#include <cstdlib>
#include <map>
#include <utility>
#include <fstream>
#include <iostream>
#include <cstdlib>
#include <time.h>

#include "TCanvas.h"
#include "TFile.h"
#include "TTree.h"
#include "TTreeFormula.h"
#include "TChain.h"
#include "TH1.h"
#include "TMath.h"
#include "TRandom.h"
#include "TLorentzVector.h"

#include "Histogrammer.h"
#include "HistogrammerFunctions.h"
#include "Histogrammer_AddOn.h"
//Test file to make sure we can do a simple loop and write a Histogram.

void Factory_Run_DsMuNu(
    std::string OutputFile, std::string FriendFileName,
    int MaxEv, int MaxFiles, int PrintFrequency, bool SaveTree, bool SaveNorm, bool RunSilent,
    bool Run_DsMuNu, bool Skip_Data, bool Skip_MCReco, bool Skip_MCGen, bool RunAddOns,
    map<string, Systematic*>& Systematics_DsMuNu )
{





  if ( Run_DsMuNu ) {
      Histogrammer H("DsMuNu", OutputFile, FriendFileName);
      H.SetMaxEv       (MaxEv);
      H.SetMaxFiles    (MaxFiles);
      H.SetPrintFreq   (PrintFrequency);
      H.SaveFriendTree (SaveTree);
      H.SaveNorm       (SaveNorm);
      H.SetSilent(RunSilent);

      H.AddCut("Nominal", "1");
      H.AddCut( "GenCut",
          "0.01 < TMath::ATan( Kminus_TRUEPT  / Kminus_TRUEP_Z  ) && 0.4 > TMath::ATan( Kminus_TRUEPT  / Kminus_TRUEP_Z  )"
       "&& 0.01 < TMath::ATan( Kplus_TRUEPT   / Kplus_TRUEP_Z   ) && 0.4 > TMath::ATan( Kplus_TRUEPT   / Kplus_TRUEP_Z   )"
       "&& 0.01 < TMath::ATan( muplus_TRUEPT  / muplus_TRUEP_Z  ) && 0.4 > TMath::ATan( muplus_TRUEPT  / muplus_TRUEP_Z  )"
       "&& 0.01 < TMath::ATan( piminus_TRUEPT / piminus_TRUEP_Z ) && 0.4 > TMath::ATan( piminus_TRUEPT / piminus_TRUEP_Z )");

      for( auto &Particle: vector<string>{"D_sminus", "Kplus", "Kminus", "piminus", "muplus"})
      {

          H.AddBranch(Particle + "_TRUEP_E");
          H.AddBranch(Particle + "_TRUEP_X");
          H.AddBranch(Particle + "_TRUEP_Y");
          H.AddBranch(Particle + "_TRUEP_Z");
          H.AddBranch(Particle + "_TRUEPT");


          H.AddHistogram(Particle + "_theta", Particle + " Theta", 100, 0, 2,
              [Particle](std::map<std::string, Variable*>& Vars, void* Add_Arg)->double
                  {
                      const double& K_PT = Vars[Particle + "_TRUEPT"]->GetVar();
                      const double& K_PZ = Vars[Particle + "_TRUEP_Z"]->GetVar();

                      double theta = TMath::ATan(K_PT/K_PZ);

                      return theta > 0 ? theta : theta + M_PI;
                  });

          H.AddHistogram((Particle + "_theta2").c_str(), (Particle + " Theta").c_str(), 40, 0, 0.4, Particle + "_theta");


      }


      H.AddBranch("B_s0_TRUEP_E");
      H.AddBranch("B_s0_TRUEP_X");
      H.AddBranch("B_s0_TRUEP_Y");
      H.AddBranch("B_s0_TRUEP_Z");
      H.AddBranch("B_s0_TRUEPT", "B_PT");


      H.AddHistogram("B_ETA", "B_{s} ETA", 100, 0, 7,
          [](std::map<std::string, Variable*>& Vars, void* Add_Arg)->double
              {

                  const double& K_PT = Vars["B_PT"]->GetVar();
                  const double& K_PZ = Vars["B_s0_TRUEP_Z"]->GetVar();
                  double K_P = TMath::Sqrt( K_PT*K_PT + K_PZ*K_PZ );

                  return TMath::ATanH(K_PZ/K_P);

              });

      H.AddHistogram("D_sminus_M", "D_sminus_M", 100, 1900, 2000,
          [](std::map<std::string, Variable*>& Vars, void* Add_Arg)->double
              {
                  const double& K_PT = Vars["D_sminus_TRUEPT"]->GetVar();
                  const double& K_PZ = Vars["D_sminus_TRUEP_Z"]->GetVar();
                  const double& K_PE = Vars["D_sminus_TRUEP_E"]->GetVar();
                  return TMath::Sqrt(K_PE*K_PE - K_PZ*K_PZ - K_PT*K_PT);

              });


      H.AddHistogram("Bs_PT", "B_{s} Transverse Momentum", 100, 0, 40000, "B_PT");
      H.AddHistogram("Bs_PE", "B_{s} Energy", 100, 0, 500000, "B_s0_TRUEP_E");


      //AddBranch("nTracks");
      H.AddFunction("TrueQ2", Functions_DsMuNu::MCTrueQ2);


      //AddSystematic("nTracks");
      H.AddSystematic("RW_Q2",    Systematics_DsMuNu.at("RW_Q2"));
      H.AddSystematic("Nominal",  Systematics_DsMuNu.at("Nominal"));

      H.AddHistogram("h_True_Q2", "B_{s} - D_{s} Invariant Mass", 1000, 0, 12e6, "TrueQ2" );

      H.AddHistogram("True_Q2", "True q^{2}", 40, 0, 12e6,
          [](std::map<std::string, Variable*>& Vars, void* Add_Arg)->double
              {
                  const double& Ds_PX = Vars["D_sminus_TRUEP_X"]->GetVar();
                  const double& Ds_PY = Vars["D_sminus_TRUEP_Y"]->GetVar();
                  const double& Ds_PZ = Vars["D_sminus_TRUEP_Z"]->GetVar();
                  const double& Ds_PE = Vars["D_sminus_TRUEP_E"]->GetVar();


                  const double& Bs_PX = Vars["B_s0_TRUEP_X"]->GetVar();
                  const double& Bs_PY = Vars["B_s0_TRUEP_Y"]->GetVar();
                  const double& Bs_PZ = Vars["B_s0_TRUEP_Z"]->GetVar();
                  const double& Bs_PE = Vars["B_s0_TRUEP_E"]->GetVar();

                  return TMath::Power(Bs_PE - Ds_PE, 2) - TMath::Power(Bs_PX - Ds_PX, 2) - TMath::Power(Bs_PY - Ds_PY, 2) - TMath::Power(Bs_PZ - Ds_PZ, 2);
              }
      );




      H.SetPrintHist("True_Q2", "Nominal", "Nominal");


      if ( !Skip_MCGen)
      {
          H.InitialiseDataType("MCDecay_NoGenCut");
          H.run("/eos/lhcb/wg/semileptonic/Bs2KmunuAna/Tuples/MCDecay_NoGenCut_Aug17/Bs_MCDecayTree_NoGenCut_13774000_Sim08iv2.root", "MCDecayTreeTuple_Ds/MCDecayTree");
          H.FinaliseDataType();

          H.AddBranch("RW_BDT");
          H.AddBranch("nTracks");

          H.AddHistogram("h_RW_BDT", "BDT_ReWeighter", 100, 0, 2, "RW_BDT" );

          H.AddSystematic("RW_Kin",     Systematics_DsMuNu.at("RW_Kin"));
          H.AddSystematic("Default",    Systematics_DsMuNu.at("Nominal"));

          H.InitialiseDataType("MCDecay");
          H.run("/eos/lhcb/wg/semileptonic/Bs2KmunuAna/Tuples/Bs2DsMuNu_MCTUPLES_TRIMMED_Aug17/DTT_MC12_Bs2DsMuNu_13774000_Cocktail_SIGNAL_*.root", "Bs2DsMuNuMCTuple");
          H.FinaliseDataType();

      }


      H.Reset();

      TCut Cut;

      TCut Cut_Trigger;
      TCut Cut_PID;
      TCut Cut_InvPIDMu;
      TCut Cut_ISO;
      TCut Cut_Phi;

          // Trigger Selection
      Cut_Trigger = "Bs_Hlt2TopoMu2BodyBBDTDecision_TOS";
          // Isolation Cut
      Cut_ISO = "muon_p_IsoMinBDT > -0.8";
          // Corrected Mass Error Cut
      //Cut += "Bs_MCORRFULLERR < 400";

      // D* -> ( D0 -> KK ) pi Veto
      Cut = "Ds_M - sqrt((kaon_p_PE  + kaon_m_PE )**2 - ((kaon_p_PX + kaon_m_PX)**2 + (kaon_p_PY + kaon_m_PY)**2 + (kaon_p_PZ + kaon_m_PZ)**2)) > 148";

      // Bs->Dspi Vetp
      Cut *= "abs( sqrt( Bs_MM**2 + Bs_PE/muon_p_PE**2*8317) - 5370 ) > 70";

      // Ds Window Cut
      Cut *= "Ds_MM > 1930 && Ds_MM < 2010";

      // Kaon P cut to remove low P events where the RICH doesn't work
      Cut *= "kaon_p_P > 10000";

      // Muon Aligned PIDCut
      Cut_PID *= "muon_p_PIDmu > 3.0 && muon_p_PIDmu-muon_p_PIDp > 0 && muon_p_PIDmu-muon_p_PIDK > 0";

      // Kaon Aligned PIDCut
      Cut_PID *= "kaon_p_PIDK > 5 && kaon_p_PIDK-kaon_p_PIDp > 5.0 && kaon_p_PIDK-kaon_p_PIDmu > 5.0";


      // Muon Aligned PIDCut
      Cut_InvPIDMu *= "!(muon_p_PIDmu > 3.0 && muon_p_PIDmu-muon_p_PIDp > 0 && muon_p_PIDmu-muon_p_PIDK > 0)";

      // Kaon Aligned PIDCut
      Cut_InvPIDMu *= "kaon_p_PIDK > 5 && kaon_p_PIDK-kaon_p_PIDp > 5.0 && kaon_p_PIDK-kaon_p_PIDmu > 5.0";


      // Restrict to Phi Mass region
      Cut_Phi = "abs(sqrt((kaon_m_PE + kaon_p_PE)**2 - (kaon_m_PX + kaon_p_PX)**2 - (kaon_m_PY + kaon_p_PY)**2 - (kaon_m_PZ + kaon_p_PZ)**2) - 1020) < 5";

      H.SetCommonCut(Cut);
      H.AddCut("Nominal", "1");
      H.AddCut("NoPID",                           Cut_Trigger + Cut_ISO );
      H.AddCut("NoPID_Phi",                       Cut_Trigger + Cut_ISO );
      H.AddCut("PID_Tight",        Cut_PID      + Cut_Trigger + Cut_ISO );
      H.AddCut("PID_Tight_Phi",    Cut_PID      + Cut_Trigger + Cut_ISO + Cut_Phi );
      H.AddCut("PIDInv_Tight",     Cut_InvPIDMu + Cut_Trigger + Cut_ISO );
      H.AddCut("PIDInv_Tight_Phi", Cut_InvPIDMu + Cut_Trigger + Cut_ISO + Cut_Phi );


      const TCut Cut_Trig_TOS       (" Bs_Hlt2TopoMu2BodyBBDTDecision_TOS");
      const TCut Cut_Trig_TISTOS    (" Bs_Hlt2TopoMu2BodyBBDTDecision_TIS && Bs_Hlt2TopoMu2BodyBBDTDecision_TOS");
      const TCut Cut_Trig_TISNotTOS (" Bs_Hlt2TopoMu2BodyBBDTDecision_TIS && !Bs_Hlt2TopoMu2BodyBBDTDecision_TOS");

      H.AddFormula("Trig_TOS",       Cut_Trig_TOS );
      H.AddFormula("Trig_TISTOS",    Cut_Trig_TISTOS );
      H.AddFormula("Trig_TISNotTOS", Cut_Trig_TISNotTOS );


      H.AddCut("Trigger_TISTOS",    {"Trig_TISTOS"});
      H.AddCut("Trigger_TISNotTOS", {"Trig_TISNotTOS"});




      H.AddBranch("nTracks");
      H.AddBranch("Bs_MCORR");

      H.AddBranch("Ds_MM");
      H.AddBranch("Ds_PE");
      H.AddBranch("Ds_PX");
      H.AddBranch("Ds_PY");
      H.AddBranch("Ds_PZ");
      H.AddBranch("Ds_PT");

      H.AddBranch("kaon_m_0.50_nc_maxPt_PX", "Gamma0_PX");
      H.AddBranch("kaon_m_0.50_nc_maxPt_PY", "Gamma0_PY");
      H.AddBranch("kaon_m_0.50_nc_maxPt_PZ", "Gamma0_PZ");
      H.AddBranch("kaon_m_0.50_nc_maxPt_PT", "Gamma0_PT");

      H.AddBranch("kaon_m_1.00_nc_maxPt_PX", "Gamma1_PX");
      H.AddBranch("kaon_m_1.00_nc_maxPt_PY", "Gamma1_PY");
      H.AddBranch("kaon_m_1.00_nc_maxPt_PZ", "Gamma1_PZ");
      H.AddBranch("kaon_m_1.00_nc_maxPt_PT", "Gamma1_PT");

      H.AddBranch("kaon_m_1.50_nc_maxPt_PX", "Gamma2_PX");
      H.AddBranch("kaon_m_1.50_nc_maxPt_PY", "Gamma2_PY");
      H.AddBranch("kaon_m_1.50_nc_maxPt_PZ", "Gamma2_PZ");
      H.AddBranch("kaon_m_1.50_nc_maxPt_PT", "Gamma2_PT");

      H.AddBranch("kaon_m_2.00_nc_maxPt_PX", "Gamma3_PX");
      H.AddBranch("kaon_m_2.00_nc_maxPt_PY", "Gamma3_PY");
      H.AddBranch("kaon_m_2.00_nc_maxPt_PZ", "Gamma3_PZ");
      H.AddBranch("kaon_m_2.00_nc_maxPt_PT", "Gamma3_PT");



      H.AddBranch("Bs_Q2SOL1");
      H.AddBranch("Bs_Q2SOL2");


      H.AddBranch("kaon_m_P");
      H.AddBranch("kaon_p_P");
      H.AddBranch("pi_p_P");
      H.AddBranch("muon_p_P");

      H.AddBranch("kaon_m_PZ");
      H.AddBranch("kaon_p_PZ");
      H.AddBranch("pi_p_PZ");
      H.AddBranch("muon_p_PZ");
      H.AddBranch("muon_p_PY");
      H.AddBranch("muon_p_PX");
      H.AddBranch("muon_p_PE");
      H.AddBranch("muon_p_PT");

      H.AddBranch("muon_p_PIDp");
      H.AddBranch("muon_p_PIDK");
      H.AddBranch("muon_p_PIDmu");
      //H.AddBranch("muon_p_PIDe");

      H.AddBranch("muon_p_NIsoTr_PX");
      H.AddBranch("muon_p_NIsoTr_PY");
      H.AddBranch("muon_p_NIsoTr_PZ");

      H.AddBranch("Bs_IPCHI2_OWNPV");
      H.AddBranch("Ds_IPCHI2_OWNPV");
      H.AddBranch("Bs_ENDVERTEX_CHI2");
      H.AddBranch("Bs_DIRA_OWNPV");
      H.AddBranch("Bs_PT",  "B_PT");
      H.AddBranch("Bs_ETA", "B_ETA");
      H.AddBranch("Bs_MM");


      H.AddBranch("TMVA_charge_BDT");
      H.AddBranch("TMVA_neutral_BDT");
      H.AddBranch("TMVA_charge_neutral_BDT");
      H.AddBranch("TMVA_inclb_afc_BDT");
      H.AddBranch("TMVA_SS_aftercut_BDT");


      H.AddHistogram("TMVA_charge_BDT"        , "TMVA_BDT_0", 50, -0.5, 0.3, "TMVA_charge_BDT");
      H.AddHistogram("TMVA_neutral_BDT"       , "TMVA_BDT_0", 50, -0.5, 0.3, "TMVA_neutral_BDT");
      H.AddHistogram("TMVA_charge_neutral_BDT", "TMVA_BDT_0", 50, -0.5, 0.3, "TMVA_charge_neutral_BDT");
      H.AddHistogram("TMVA_inclb_afc_BDT"     , "TMVA_BDT_0", 50, -0.5, 0.3, "TMVA_inclb_afc_BDT");
      H.AddHistogram("TMVA_SS_aftercut_BDT"   , "TMVA_BDT_0", 50, -0.5, 0.3, "TMVA_SS_aftercut_BDT");

      H.AddHistogram("h_Bs_IPCHI2", "B_{s} IP #Chi^{2} to PV", 50, 0, 500,     "Bs_IPCHI2_OWNPV");
      H.AddHistogram("h_Ds_IPCHI2", "D_{s} IP #Chi^{2} to PV", 50, 0, 500,     "Ds_IPCHI2_OWNPV");

      H.AddHistogram("h_Bs_EVTX_CHI2", "B_{s} Decay Vertex \\chi^{2}", 50, 0, 9,"Bs_ENDVERTEX_CHI2");
      H.AddHistogram("h_Bs_DIRA",   "B_{s} DIRA",              50, 0.999, 1,   "Bs_DIRA_OWNPV");

      H.AddHistogram("h_Bs_PT",     "B_{s} p_{T}",             50, 0, 20e3,    "B_PT");
      H.AddHistogram("h_Ds_PT",     "D_{s} p_{T}",             50, 0, 20e3,    "Ds_PT");
      H.AddHistogram("h_Mu_PT",     "#mu p_{T}",             50, 0, 20e3,    "muon_p_PT");

      H.AddHistogram("h_Bs_MM",     "B_{s} Invariant Mass",    50, 2500, 5500, "Bs_MM");
      H.AddHistogram("h_Bs_ETA",    "B_{s} #eta",              50, 2, 5 ,      "B_ETA");


      H.AddFunction("kaon_p_ETA", Functions_Generic::Part_ETA, new std::string("kaon_p" ));
      H.AddFunction("kaon_m_ETA", Functions_Generic::Part_ETA, new std::string("kaon_m" ));
      H.AddFunction("pi_p_ETA",   Functions_Generic::Part_ETA, new std::string("pi_p"   ));
      H.AddFunction("muon_p_ETA", Functions_Generic::Part_ETA, new std::string("muon_p" ));
      H.AddFunction("DsStar_M",   Functions_DsMuNu::DsStar_M  );

      H.AddFunction("MagnetPol",  Functions_Generic::MagnetPolarity, &H.currentFile);

      H.AddSystematic("Nominal",      Systematics_DsMuNu.at("Nominal"));

      H.AddSystematic("sWeight",      Systematics_DsMuNu.at("sWeight"));
      //H.AddSystematic("sWeight_BG",   Systematics_DsMuNu.at("sWeight_BG"));


      H.AddHistogram("h_Ds_MM", "D_{s} Invariant Mass", 100, 1930, 2010, "Ds_MM" );
      H.AddHistogram("h_DsStar_MM", "D_{s} Invariant Mass", 100, 2040, 2200, "DsStar_M" );

      H.AddHistogram("h_Bs_MCORR",   "B_{s} Corrected Mass", 30,  3000,  5370,             "Bs_MCORR"   );
      H.AddHistogram("h_Bs_MCORR2",  "B_{s} Corrected Mass", 40,  3000,  6500,             "Bs_MCORR"   );

      H.AddHistogram("h_Bs_MCORR_Ds",   "B_{s} Corrected Mass", 30,  3000,  5370, 40, 1930, 2010, "Bs_MCORR", "Ds_MM" );
      H.AddHistogram("h_Bs_MCORR2_Ds",  "B_{s} Corrected Mass", 40,  3000,  6500, 40, 1930, 2010, "Bs_MCORR", "Ds_MM" );



      H.AddHistogram("h_nTracks", "Track Multiplicity", 1000, 0, 1000, "nTracks");

      H.AddHistogram("h_Bs_MCORR_Q21", "B_{s} Corrected Mass", 40, 3000, 5370, 6, 0, 12e6, "Bs_MCORR", "Bs_Q2SOL1" );
      H.AddHistogram("h_Bs_MCORR_Q22", "B_{s} Corrected Mass", 40, 3000, 5370, 6, 0, 12e6, "Bs_MCORR", "Bs_Q2SOL2" );




      H.AddHistogram("h_Q2_1_DsStarM", "D_{s} + #gamma Invariant Mass Vs Q^{2} Solution 1", 6, 0, 12e6, 50, 2040, 2200, "Bs_Q2SOL1", "DsStar_M" );
      H.AddHistogram("h_Q2_2_DsStarM", "D_{s} + #gamma Invariant Mass Vs Q^{2} Solution 2", 6, 0, 12e6, 50, 2040, 2200, "Bs_Q2SOL2", "DsStar_M" );

      H.AddHistogram("h_Gamma_pT", "Gammap pT", 50, 200, 2000, "Gamma0_PT" );
      //H.AddHistogram("h_Q2_1_DsStarM_Ds", "D_{s} + #gamma Invariant Mass Vs Q^{2} Solution 1", 6, 0, 12e6, 50, 2040, 2200, 100, 1930, 2010, "Bs_Q2SOL1", "DsStar_M", "Ds_MM" );
      //H.AddHistogram("h_Q2_2_DsStarM_Ds", "D_{s} + #gamma Invariant Mass Vs Q^{2} Solution 2", 6, 0, 12e6, 50, 2040, 2200, 100, 1930, 2010, "Bs_Q2SOL2", "DsStar_M", "Ds_MM" );


      //H.AddHistogram("h_Q2_1_DsStarDM", "D_{s} + #gamma Invariant Mass Vs Q^{2} Solution 1", 6, 0, 12e6, 50, 65, 225, "Bs_Q2SOL1", "DsStar_DM" );
      //H.AddHistogram("h_Q2_2_DsStarDM", "D_{s} + #gamma Invariant Mass Vs Q^{2} Solution 2", 6, 0, 12e6, 50, 65, 225, "Bs_Q2SOL2", "DsStar_DM" );

      //H.AddHistogram("h_Q2_1_DsStarDM_Ds", "D_{s} + #gamma Invariant Mass Vs Q^{2} Solution 1", 6, 0, 12e6, 50, 65, 225, 100, 1930, 2010, "Bs_Q2SOL1", "DsStar_DM", "Ds_MM" );
      //H.AddHistogram("h_Q2_2_DsStarDM_Ds", "D_{s} + #gamma Invariant Mass Vs Q^{2} Solution 2", 6, 0, 12e6, 50, 65, 225, 100, 1930, 2010, "Bs_Q2SOL2", "DsStar_DM", "Ds_MM" );


      H.AddHistogram("h_Q2_1_G_PT", "q^{2} Vs. #gamma p_{T}", 6, 0, 12e6, 50, 1e-10, 4000, "Bs_Q2SOL1", "Gamma0_PT" );
      H.AddHistogram("h_Q2_2_G_PT", "q^{2} Vs. #gamma p_{T}", 6, 0, 12e6, 50, 1e-10, 4000, "Bs_Q2SOL2", "Gamma0_PT" );

      H.AddHistogram("h_nTracks2",
              new TH1F("h_nTracks2", "Track Multiplicity", 41,
                  &std::vector<double>{0, 38, 47, 55, 61, 66, 71, 76, 80, 85, 89, 93, 97, 101, 105, 109, 113, 118, 122, 126, 130, 135, 140, 144, 149, 155, 160, 166, 172, 178, 185, 193, 202, 211, 222, 234, 248, 265, 288, 320, 377, 1000}[0]),
              "nTracks");


      H.AddHistogram("h_Data_PIDK_Cut" ,  "DLL ( K - #pi )",   100, -100, 50, 10, 10000, 100000, 10, 2, 5, "muon_p_PIDK",  "muon_p_P", "muon_p_ETA");
      H.AddHistogram("h_Data_PIDMu_Cut" , "DLL ( #mu - #pi )", 100,  0,   15, 10, 10000, 100000, 10, 2, 5, "muon_p_PIDmu", "muon_p_P", "muon_p_ETA");
      H.AddHistogram("h_Data_PIDP_Cut" ,  "DLL ( P - #pi )",   100, -100, 40, 10, 10000, 100000, 10, 2, 5, "muon_p_PIDp",  "muon_p_P", "muon_p_ETA");
      //H.AddHistogram("h_Data_PIDe_Cut" ,  "DLL ( e - #pi )",   100, -10,  2,  10, 10000, 100000, 10, 2, 5, "muon_p_PIDe",  "muon_p_P", "muon_p_ETA");

      H.AddHistogram("h_Data_PIDK_Fake" ,  "DLL ( K - #pi )",   100, -80,  50, 10, 10000, 100000, 10, 2, 5, "muon_p_PIDK",  "muon_p_P", "muon_p_ETA");
      H.AddHistogram("h_Data_PIDMu_Fake" , "DLL ( #mu - #pi )", 100, -15,  0 , 10, 10000, 100000, 10, 2, 5, "muon_p_PIDmu", "muon_p_P", "muon_p_ETA");
      H.AddHistogram("h_Data_PIDP_Fake" ,  "DLL ( P - #pi )",   100, -80,  40, 10, 10000, 100000, 10, 2, 5, "muon_p_PIDp",  "muon_p_P", "muon_p_ETA");
      //H.AddHistogram("h_Data_PIDe_Fake" ,  "DLL ( e - #pi )",   100, -15,  15, 10, 10000, 100000, 10, 2, 5, "muon_p_PIDe",  "muon_p_P", "muon_p_ETA");

      H.AddHistogram("Bs_DsPi", "Bs reco as Ds pi", 40, 5220, 5520,
          [](std::map<std::string, Variable*>& Vars, void* Add_Arg)->double
              {
                  const double& Ds_PE = Vars["Ds_PE"]->GetVar();
                  const double& Ds_PX = Vars["Ds_PX"]->GetVar();
                  const double& Ds_PY = Vars["Ds_PY"]->GetVar();
                  const double& Ds_PZ = Vars["Ds_PZ"]->GetVar();

                  const double& Mu_PE = Vars["muon_p_PE"]->GetVar();
                  const double& Mu_PX = Vars["muon_p_PX"]->GetVar();
                  const double& Mu_PY = Vars["muon_p_PY"]->GetVar();
                  const double& Mu_PZ = Vars["muon_p_PZ"]->GetVar();

                  double Pi_PE = sqrt(Mu_PE*Mu_PE + 139.57*139.57 - 105.65*105.65);

                  return sqrt(pow(Pi_PE + Ds_PE, 2) - pow(Mu_PX + Ds_PX, 2) - pow(Mu_PY + Ds_PY, 2) - pow(Mu_PZ + Ds_PZ, 2));

              });

      H.AddHistogram("Bc_JpsiDs", "Bc reco as Jpsi Ds", 40, 6020, 6520,
          [](std::map<std::string, Variable*>& Vars, void* Add_Arg)->double
              {
                  const double& Ds_PE = Vars["Ds_PE"]->GetVar();
                  const double& Ds_PX = Vars["Ds_PX"]->GetVar();
                  const double& Ds_PY = Vars["Ds_PY"]->GetVar();
                  const double& Ds_PZ = Vars["Ds_PZ"]->GetVar();

                  const double& Mu_PX = Vars["muon_p_PX"]->GetVar();
                  const double& Mu_PY = Vars["muon_p_PY"]->GetVar();
                  const double& Mu_PZ = Vars["muon_p_PZ"]->GetVar();

                  const double& Mu2_PX = Vars["muon_p_NIsoTr_PX"]->GetVar();
                  const double& Mu2_PY = Vars["muon_p_NIsoTr_PY"]->GetVar();
                  const double& Mu2_PZ = Vars["muon_p_NIsoTr_PZ"]->GetVar();

                  TLorentzVector Mu1;
                  Mu1.SetXYZM( Mu_PX, Mu_PY, Mu_PZ, 105.65);
                  TLorentzVector Mu2;
                  Mu2.SetXYZM( Mu2_PX, Mu2_PY, Mu2_PZ, 105.65);

                  TLorentzVector JPsi = Mu1+Mu2;

                  if ( TMath::Abs( JPsi.M() - 3097) > 20 )
                    return -1;

                  TLorentzVector Ds( Ds_PX, Ds_PY, Ds_PZ, Ds_PE);

                  return (JPsi + Ds).M();



              });

      for( auto &scaling: vector<int>{0, 1, 2, 3})
      {
          for( auto &g: vector<std::string>{"0", "1", "2", "3"})
          {
              H.AddHistogram("h_DsStar_DM_" + g + "_" + std::to_string(scaling), "D_{s}* - D_{s} Invariant Mass", 100, 65, 225,
                  [g, scaling](std::map<std::string, Variable*>& Vars, void* Add_Arg)->double
                  {
                      double Ds_MM = Vars["Ds_MM"]->GetVar();

                      double Ds_PX = Vars["Ds_PX"]->GetVar();
                      double Ds_PY = Vars["Ds_PY"]->GetVar();
                      double Ds_PZ = Vars["Ds_PZ"]->GetVar();
                      double Ds_PE = Vars["Ds_PE"]->GetVar();

                      double G_PX = Vars["Gamma" + g + "_PX"]->GetVar() * (1 + scaling/100.0f);
                      double G_PY = Vars["Gamma" + g + "_PY"]->GetVar() * (1 + scaling/100.0f);
                      double G_PZ = Vars["Gamma" + g + "_PZ"]->GetVar() * (1 + scaling/100.0f);
                      double G_PE = TMath::Sqrt(G_PX*G_PX + G_PY*G_PY + G_PZ*G_PZ);

                      double Dss_PX = Ds_PX + G_PX;
                      double Dss_PY = Ds_PY + G_PY;
                      double Dss_PZ = Ds_PZ + G_PZ;
                      double Dss_PE = Ds_PE + G_PE;

                      return TMath::Sqrt( TMath::Power(Dss_PE, 2.0) - TMath::Power(Dss_PX, 2.0) - TMath::Power(Dss_PY, 2.0) - TMath::Power(Dss_PZ, 2.0) ) - Ds_MM;

                  }
              );
          }
      }

      H.SetPrintHist("h_Bs_MCORR1", "PID_Tight", "Nominal");

      if ( !Skip_Data )
      {
          H.InitialiseDataType("Data");
          H.run("/eos/lhcb/wg/semileptonic/Bs2KmunuAna/Tuples/Bs2KMuNu_DATATUPLES_TRIMMED_Nov17/Merged_data12_Bs2KMuNu_*.root", "Bs2DsMuNuTuple");
          H.FinaliseDataType();

          H.InitialiseDataType("SameSign");
          H.run("/eos/lhcb/wg/semileptonic/Bs2KmunuAna/Tuples/Bs2KMuNu_DATATUPLES_TRIMMED_Nov17/Merged_data12_Bs2KMuNu_*.root", "Bs2DsMuNuSSTuple");
          H.FinaliseDataType();

          H.InitialiseDataType("FakeMu");
          H.run("/eos/lhcb/wg/semileptonic/Bs2KmunuAna/Tuples/Bs2KMuNu_DATATUPLES_TRIMMED_Nov17/Merged_data12_Bs2KMuNu_*.root", "Bs2DsMuNuTuple_FakeMuon");
          H.FinaliseDataType();

          H.InitialiseDataType("SameSign_FakeMu");
          H.run("/eos/lhcb/wg/semileptonic/Bs2KmunuAna/Tuples/Bs2KMuNu_DATATUPLES_TRIMMED_Nov17/Merged_data12_Bs2KMuNu_*.root", "Bs2DsMuNuSSTuple_FakeMuon");
          H.FinaliseDataType();

      }


      H.AddCut("Trigger_TOS",    {"Trig_TOS"});
      H.AddCut("Trigger_None",   {});


      H.AddBranch("Bs_TRUEP_E");
      H.AddBranch("Bs_TRUEP_X");
      H.AddBranch("Bs_TRUEP_Y");
      H.AddBranch("Bs_TRUEP_Z");
      H.AddBranch("Ds_TRUEP_E");
      H.AddBranch("Ds_TRUEP_X");
      H.AddBranch("Ds_TRUEP_Y");
      H.AddBranch("Ds_TRUEP_Z");

      H.AddFunction("TrueQ2", Functions_DsMuNu::TrueQ2);


      H.AddFormula   ("RW_BDT", "RW_BDT");
      H.AddSystematic("RW_Kin",       Systematics_DsMuNu.at("RW_Kin"));

      H.AddSystematic("RW_Q2",        Systematics_DsMuNu.at("RW_Q2"));

      H.AddSystematic("Tracking_K_p", Systematics_DsMuNu.at("Tracking_K_p"));
      H.AddSystematic("Tracking_K_m", Systematics_DsMuNu.at("Tracking_K_m"));
      H.AddSystematic("Tracking_Pi",  Systematics_DsMuNu.at("Tracking_Pi"));
      H.AddSystematic("Tracking_Mu_p",Systematics_DsMuNu.at("Tracking_Mu_p"));
      H.AddSystematic("Tracking_Comb",Systematics_DsMuNu.at("Tracking_Comb_NT"));
      H.AddSystematic("PID_K_m",      Systematics_DsMuNu.at("PID_K_m"));
      H.AddSystematic("PID_K_p",      Systematics_DsMuNu.at("PID_K_p"));
      H.AddSystematic("PID_Mu_p",     Systematics_DsMuNu.at("PID_Mu_p"));
      H.AddSystematic("PID_Pi",       Systematics_DsMuNu.at("PID_Pi"));
      H.AddSystematic("PID_Comb",     Systematics_DsMuNu.at("PID_Comb_NT"));

      H.AddSystematic("Default",      Systematics_DsMuNu.at("Default"));



      H.AddHistogram("h_True_Q2", "B_{s} - D_{s} Invariant Mass", 1000, 0, 12e6, "TrueQ2" );
      H.AddHistogram("h_Bs_MCORR_Q2True", "B_{s} Corrected Mass", 40, 2500, 5370, 6, 0, 12e6, "Bs_MCORR", "TrueQ2" );







      if ( !Skip_MCReco)
      {


          std::string dir = "/eos/lhcb/wg/semileptonic/Bs2KmunuAna/Tuples/Bs2DsMuNu_MCTUPLES_TRIMMED_Aug17";

          for(auto &EV: std::vector<std::string>{"13774000", "13774002", "1377400X"})
          {
              std::string EV2 = EV;
              if (EV == "1377400X")
                EV2 = "1377400?";

              string FileName = dir + "/DTT_MC12_Bs2DsMuNu_"+EV2+"_Cocktail_*.root";
              H.InitialiseDataType(EV);
              H.SetCommonCut( Cut );
              H.run(FileName, "Bs2DsMuNuTuple");
              H.FinaliseDataType();

              H.InitialiseDataType(EV+"_Ds");
              H.SetCommonCut(Cut + "Bs_TM_DsMu");
              H.run(FileName, "Bs2DsMuNuTuple");
              H.FinaliseDataType();

              H.InitialiseDataType(EV+"_DsStar");
              H.SetCommonCut(Cut + "Bs_TM_DsstarMu_Dsg + Bs_TM_DsstarMu_Dspi0");
              H.run(FileName, "Bs2DsMuNuTuple");
              H.FinaliseDataType();


              H.InitialiseDataType(EV+"_DsStar0");
              H.SetCommonCut(Cut + "Bs_TM_Dsstar0Mu_Dspi0 + Bs_TM_Dsstar0Mu_Dspi0pi0 + Bs_TM_Dsstar0Mu_Dspipi + Bs_TM_Dsstar0Mu_Dsstarg_Dsg + Bs_TM_Dsstar0Mu_Dsstarg_Dspi0 > 0");
              H.run(FileName, "Bs2DsMuNuTuple");
              H.FinaliseDataType();


              H.InitialiseDataType(EV+"_DsStar1");
              H.SetCommonCut(Cut + "Bs_TM_Ds12460_Dspi0pi0 + Bs_TM_Ds12460_Dspipi + Bs_TM_Ds12460_Dsstarpi0_Dsg + Bs_TM_Ds12460_Dsstarpi0_Dspi0 + Bs_TM_Ds12536Mu_Dsg + Bs_TM_Ds12536Mu_Dspi0pi0 + Bs_TM_Ds12536Mu_Dspipi + Bs_TM_Ds12536Mu_Dsstarg_Dsg + Bs_TM_Ds12536Mu_Dsstarg_Dspi0 + Bs_TM_Ds12536Mu_Dsstarpi0_Dspi0 + Bs_TM_Ds12536Mu_Dsstarpi0_Dspi0pi0 + Bs_TM_Ds12536Mu_Dsstarpi0_Dspipi > 0");
              H.run(FileName, "Bs2DsMuNuTuple");
              H.FinaliseDataType();


              H.InitialiseDataType(EV+"_DsXtau");
              H.SetCommonCut(Cut + "Bs_TM_DsTau + Bs_TM_DsstarTau_Dsg + Bs_TM_DsstarTau_Dspi0 + Bs_TM_Dsstar0Tau_Dspi0 + Bs_TM_Dsstar0Tau_Dspi0pi0 + Bs_TM_Dsstar0Tau_Dspipi + Bs_TM_Dsstar0Tau_Dsstarg_Dsg + Bs_TM_Dsstar0Tau_Dsstarg_Dspi0 + Bs_TM_Ds12536Tau_Dsg + Bs_TM_Ds12536Tau_Dspi0pi0 +Bs_TM_Ds12536Tau_Dspipi + Bs_TM_Ds12536Tau_Dsstarg_Dsg +  Bs_TM_Ds12536Tau_Dsstarg_Dspi0 + Bs_TM_Ds12536Tau_Dsstarpi0_Dspi0 + Bs_TM_Ds12536Tau_Dsstarpi0_Dspi0pi0 + Bs_TM_Ds12536Tau_Dsstarpi0_Dspipi > 0");
              H.run(FileName, "Bs2DsMuNuTuple");
              H.FinaliseDataType();

          }
          vector<std::string> Event_Types;
          Event_Types.push_back("11876001");
          Event_Types.push_back("11995200");
          Event_Types.push_back("11995202");
          Event_Types.push_back("12875601");
          Event_Types.push_back("12995600");
          Event_Types.push_back("12995602");
          Event_Types.push_back("13873201");
          Event_Types.push_back("13996202");

          for( auto &EV: Event_Types)
          {
              H.InitialiseDataType("MC_" + EV);
              H.SetCommonCut(Cut);
              H.run("/eos/lhcb/wg/semileptonic/Bs2KmunuAna/Tuples/Bs2DsMuNu_MCTUPLES_TRIMMED_13June16/DTT_MC1*" + EV + "*.root", "Bs2DsMuNuTuple");
              H.FinaliseDataType();
          }

          for( auto &EV: vector<std::string>{"14175001", "14175003"})
          {
              H.InitialiseDataType("MC_" + EV);
              H.SetCommonCut(Cut);
              H.run("/eos/lhcb/wg/semileptonic/Bs2KmunuAna/Tuples/Bs2DsMuNu_MCTUPLES_TRIMMED_Aug17/DTT_MC1*" + EV + "*.root", "Bs2DsMuNuTuple");
              H.FinaliseDataType();
          }

      }

      if ( RunAddOns )
      {
          Histogrammer_AddOn::SubtractDs(OutputFile, "Data",     SaveNorm);
          Histogrammer_AddOn::SubtractDs(OutputFile, "FakeMu",   SaveNorm);
          Histogrammer_AddOn::SubtractDs(OutputFile, "SameSign", SaveNorm);
          Histogrammer_AddOn::SubtractDs(OutputFile, "SameSign_FakeMu", SaveNorm);
      }


  }


}

#endif
