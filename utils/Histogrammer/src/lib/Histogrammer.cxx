/** \file
 * HistogrammerKMuNu.cxx
 *
 *  Created on: 28 May 2017
 *      Author: ismith
 */

#include <iostream>
#include <cstdlib>
#include <iomanip>

#include <omp.h>

#include "TDataType.h"
#include "TClass.h"
#include "TBranch.h"
#include "TTree.h"
#include "TDirectory.h"
#include "TTreeFormula.h"

#include "Histogrammer.h"
#include "Histogram.h"
#include "Variable.h"
#include "Systematic.h"

#include "OstreamCache.h"

std::ostream& operator<< (std::ostream& lhs, TH1* rhs)
{
/**
 * Helper function overloading th << operator.
 * It will print an ascii representation of a 1D histogram.
 * e.g:
 * ```
 * Name: True_Q2  Title: True q^{2}
 *      1231|
 *          | #  ##   #
 *          | #########
 *          |############  #
 *          |################
 *    923.25|###################
 *          |####################
 *          |######################
 *          |########################
 *          |#########################
 *     615.5|###########################
 *          |########################### #
 *          |#############################
 *          |##############################
 *          |################################
 *    307.75|################################
 *          |#################################
 *          |##################################
 *          |###################################
 *          |####################################
 *         0|######################################
 *          +----------------------------------------
 *
 * ```
 *
 */

  using namespace std;

  const int bins = rhs->GetNbinsX();

  const float MaxY = rhs->GetMaximum();
  const float MinY = rhs->GetMinimum();

  lhs << "Name: " << rhs->GetName() << "  Title: " << rhs->GetTitle() << endl;



  for( int y = bins; y >= 0; y-=2)
  {

    if (y%10 == 0)
      lhs << setw(10) << std::right << MinY + (float)y/bins * (MaxY - MinY) << '|';
    else
      lhs << setw(11) << "|";

    for( int x = 0; x < bins; x++)
    {
      char pixel = ' ';
      if ( rhs->GetBinContent(x+1) > (MinY + (float)y/bins * (MaxY - MinY)) )
        pixel = '#';

      lhs << pixel;
    }
    lhs << endl;

  }

  lhs << setw(11) << '+';
  for( int x = 0; x < bins; x++)
  {
    lhs << '-';
  }

  lhs << endl;

  return lhs;
}


Histogrammer::Histogrammer(std::string Type, std::string FileName, std::string FriendFileName)
{
/**
 * Constructor for the histogrammer class
 */


  m_HistogramFileName = FileName;
  m_FriendFileName    = (FriendFileName == "") ? FileName.substr(0, FileName.size()-5) + "_FriendTree.root" : FriendFileName;
  RecoAs = Type;

  AddSystematic ("Nominal",   new Systematic_Nominal());

}



void Histogrammer::AddBranch( std::string BranchName, std::string alias)
{
/**
 * Add a branch. Provide a branhc name (and optional alias) and the code will
 * look up the branch from the TTree, determine the variable type being stored.
 * The value will be looked up during the event loop.
 */

  if (alias == "")
    alias = BranchName;

  Branches.emplace(alias, BranchName);
  Variables[alias] = &Branches[alias];
}


void Histogrammer::DeleteBranch( std::string BranchName )
{
/**
 * Remove a branch from the histogrammer.
 */
  int status = 1;
  status *= Variables.erase( BranchName );
  status *= Branches .erase( BranchName );

  if ( status == 0 )
  {
      std::cout << "Tried to erase a non existing Branch: " << BranchName << std::endl;
      std::abort();
  }
}

void Histogrammer::AddFunction(std::string VarName, std::function <double (map<std::string, Variable*>&, void*)> f, void* AdditionalArguments)
{
/**
 * Add a function to the histogrammer.
 * The function will be evaulated for each event.
 * used std::function, so lambdas and function pointers are all valid
 *
 * Note: All branches which are referenced must have been previously added via AddBranch()
 */

  //Functions[VarName] = Variable_Function(Variables, f, AdditionalArguments);
  Functions.emplace(VarName, Variable_Function(Variables, f, AdditionalArguments));
  Variables[VarName] = (Variable*)&Functions[VarName];

}

void Histogrammer::DeleteFunction( std::string FunctionName )
{
/**
 * Remove a function from the histogrammer
 */
  int status = 1;
  status *= Variables.erase( FunctionName );
  status *= Functions.erase( FunctionName );

  if ( status == 0 )
  {
      std::cout << "Tried to erase a non existing Function: " << FunctionName << std::endl;
      std::abort();
  }

}

void Histogrammer::AddFormula(std::string VarName, TCut Formula)
{
/**
 * Add a formula to the histogrammer. Uses TTreeFormula to calculate values.
 */
  if ( Formulas.find(VarName) != Formulas.end() )
  {
      std::cout << VarName << "Has already been added as a formula. CHeck your naming. Exiting" << std::endl;
      std::abort();
  }

  Formulas.emplace(VarName, Variable_Formula(VarName, Formula));
  Variables[VarName] = (Variable*)&Formulas[VarName];

}

void Histogrammer::DeleteFormula( std::string FormulaName )
{
/**
 * Remove a formula from the hisotgrammer
 */

  int status = 1;
  status *= Variables.erase( FormulaName );
  status *= Formulas.erase( FormulaName );

  if ( status == 0 )
  {
      std::cout << "Tried to erase a non existing Formula: " << FormulaName << std::endl;
      std::abort();
  }


}


void Histogrammer::AddCut(std::string VarName, const TCut& Formula)
{
/**
 * Add a cut to the histogrammer.
 * The output file of the histogrammer will have a subfolder for each cut.
 */
  AddFormula(VarName, Formula);
  Weights.emplace(VarName, Combined_Variable_Formula(Formulas, {VarName}));

}

void Histogrammer::AddCut(std::string CutName, std::initializer_list<std::string> Cuts)
{
/**
 * Add a cut to the histogrammer which is the exclusive and pf many cuts.
 * Cuts are evaluated from left to right, and calculation will stop if a cut fails.
 * The tightest/fastest cuts should be listed first.
 */
  Weights.emplace(CutName, Combined_Variable_Formula(Formulas, Cuts));
}


void Histogrammer::DeleteCut( std::string FormulaName )
{
/**
 * Remove a cut from the histogrammer.
 */
  int status = Weights.erase( FormulaName );
  if ( status == 0 )
  {
      std::cout << "Tried to erase a non existing Cut: " << FormulaName << std::endl;
      std::abort();
  }

}


void Histogrammer::AddAlias(std::string name, std::string formula)
{
/**
 * Add an alias to the histogrammer.
 * Uses TTreeAlias.
 * This is useful when adding formulae or specifying cuts.
 */
  Aliases[name] = formula;
}

void Histogrammer::DeleteAlias(std::string name)
{
/**
 * Remove an alias from the histogrammer
 */
  Aliases.erase(name);
}


void Histogrammer::AddHistogram(const char *name,const char *title,Int_t nbins,Double_t xlow, Double_t xup, std::string VarX)
{
/**
 * Add a 1-D histogram to the Histogrammer by specifying range and nBins.
 */
  RootHistograms[name] = BaseHistogram(
                new TH1F(name, title, nbins, xlow, xup),
                {VarX});
}
void Histogrammer::AddHistogram(std::string name, TH1F* Hist, std::string VarX)
{
/**
 * Add a 1-D histogram to the Histogrammer by cloning an existing histogram.
 */
  RootHistograms[name] = BaseHistogram(
                Hist,
                {VarX});
}

void Histogrammer::AddHistogram(const char *name, const char *title, Int_t nbinsx, Double_t xlow, Double_t xup, Int_t nbinsy, Double_t ylow, Double_t yup, std::string VarX, std::string VarY)
{
/**
 * Add a 2-D histogram to the Histogrammer by specifying range and nBins.
 */

  RootHistograms[name] = BaseHistogram(
                new TH2F(name, title, nbinsx, xlow, xup, nbinsy, ylow, yup),
                {VarX, VarY});
}

void Histogrammer::AddHistogram(std::string name, TH2F* Hist, std::string VarX, std::string VarY)
{
/**
 * Add a 2-D histogram to the Histogrammer by cloning an existing histogram.
 */

  RootHistograms[name] = BaseHistogram(
                Hist,
                {VarX, VarY});
}

void Histogrammer::AddHistogram(const char *name, const char *title, Int_t nbinsx, Double_t xlow, Double_t xup, Int_t nbinsy, Double_t ylow, Double_t yup, Int_t nbinsz, Double_t zlow, Double_t zup,  std::string VarX, std::string VarY, std::string VarZ)
{
/**
 * Add a 3-D histogram to the Histogrammer by specifying range and nBins.
 */

  RootHistograms[name] = BaseHistogram(
                new TH3F(name, title, nbinsx, xlow, xup, nbinsy, ylow, yup, nbinsz, zlow, zup),
                {VarX, VarY, VarZ});
}


void Histogrammer::AddHistogram(std::string name, std::string title, Int_t nbins, Double_t low, Double_t high, std::function <double (map<std::string, Variable*>&, void*)> f)
{
/**
 * Add a 1-D histogram to the Histogrammer by specifying the bin ranges and nBins.
 * Instead of referencing a variable by name, a function can be passed which will be
 * calculated and the output used to fill the histogram
 */

  AddFunction (name.c_str(), f);
  AddHistogram(name.c_str(), title.c_str(), nbins, low, high, name);

}


void Histogrammer::DeleteHistogram(std::string HistName)
{
/**
 * Remove a Histogram.
 */

  RootHistograms.erase(HistName);
}

void Histogrammer::AddSystematic(std::string name , Systematic* Syst)
{
/**
 * Add a systematic effect ir correction to the histogrammer
 * There will be a subfolder for each systematic/correction.
 *
 */
  Syst->SetVariables(&Variables);
  Systematics[name] = Syst;

}

void Histogrammer::DeleteSystematic(std::string SystName)
{
/**
 * Remove a Systematic/Correction.
 */
  Systematics.erase(SystName);
}




void Histogrammer::InitialiseDataType(std::string Type)
{
/**
 * Initialise the histogrammer.
 * This is required to be urn before a file is processed.
 *
 * Hitograms will be generated, values initialised, the output file will be write tested and initialised.
 *
 * The Type will be the root folder in the output file
 *
 */



  std::cout << "Initialising Data Type: " << Type << std::endl;

  DataType = Type;

  nfile = 0;

  EventTupleEntries = TNamed(Type.c_str(), "0");
  Luminosity        = TNamed(Type.c_str(), "0");
  LuminosityErr     = TNamed((Type+"_Err").c_str(), "0");

  if ( m_SaveTree ){


    OutputTreeFile = TFile::Open(m_FriendFileName.c_str(), "UPDATE");
    OutputTree     = new TTree("FriendTree", "FriendTree");

    std::cout << "Trying to open " << m_FriendFileName << " to write a friend tree."<< std::endl;


    for (auto &Var: Variables)
    {

      auto pointerinfo  = Var.second->GetPointer();
      std::string BType = std::string(1, pointerinfo.second);
      void* pointer     = pointerinfo.first;
      OutputTree->Branch(Var.first.c_str(), pointer, (Var.first+"/" + BType).c_str());
    }

    for (auto &Var: Systematics)
    {
      auto pointerinfo  = Var.second->GetPointer();
      std::string BType = std::string(1, pointerinfo.second);
      void* pointer     = pointerinfo.first;
      OutputTree->Branch(Var.first.c_str(), pointer, (Var.first+"/" + BType).c_str());
    }


  }




  for( auto& Syst: Systematics)
  {


    if ( not Syst.second->GetVerbose() )
      continue;

    for( auto& Cut: Weights)
    {

      std::string CutName = Cut.first;
      std::string SaveDirectory = Syst.first + "/" + RecoAs + "/" + DataType + "/" + CutName;

      Histogram* NewHist = (Histogram*) new HistogramToy(Syst.second, &Cut.second);
      NewHist->SetDir( SaveDirectory );

      Histograms[CutName].push_back( std::shared_ptr<Histogram>( NewHist ) );

      for ( auto &Hist: RootHistograms)
      {


        int nDim = Hist.second.m_Vars.size();

        std::vector<std::string>& Vars = Hist.second.m_Vars;

        TH1*        h_in    = Hist.second.m_Histogram.get();


        NewHist = nullptr;
        if ( nDim == 1 )
        {
          NewHist = (Histogram*) new Histogram1((TH1F*)h_in, *Variables[Vars[0]], Syst.second, &Cut.second);
        }
        else if ( nDim == 2 )
        {
          NewHist = (Histogram*) new Histogram2((TH2F*)h_in, *Variables[Vars[0]], *Variables[Vars[1]], Syst.second, &Cut.second);
        }
        else if ( nDim == 3 )
        {
          NewHist = (Histogram*) new Histogram3((TH3F*)h_in, *Variables[Vars[0]], *Variables[Vars[1]], *Variables[Vars[2]], Syst.second, &Cut.second);
        }
        else
        {
          std::cout << "We should not be here. We somehow have a histogram with non physical number of dimensions" << std::endl;
          std::abort();
        }
        NewHist->SetDir( SaveDirectory );
        Histograms[CutName].push_back( std::shared_ptr<Histogram>( NewHist ) );

        if ( Syst.first == PrintSystematic &&  PrintCut == Cut.first && PrintName == Hist.first )
          PrintHistogram = Histograms[CutName].back();

      }
    }

  }





}


void Histogrammer::run(std::string FileName, std::string TreeName)
{
/**
 * Process a file from a filename and treename.
 * The filename supports wildcards, even over xrootd!
 *
 * Then run the histogrammer for each file
 *
 */
  auto FileList = HistogrammerTools::WildCards(FileName);

  if ( FileList.size() == 0 )
  {
    std::cout << "\e[1;31;42m";
    std::cout << "No files found matching wildcard: " << FileName;
    std::cout << "\e[0m" << std::endl;
  }
  for( auto &f: FileList)
  {
    runFile(f, TreeName);
  }

}

void Histogrammer::runFile(std::string FileName, std::string TreeName){
/*
 * Run the histogrammer from a filename and treename.
 *
 * No wildcards will be searched for, so the filename must be valid.
 * Invalid filenames and treenames will cause a soft exit.
 *
 */

  Output.reset();

  currentFile = FileName;

  nfile++;
  if ( nfile > (uint)m_MaxFiles )
    return;

  std::cout << "Trying to open file: " << FileName << std::endl;

  // Open the input file:
  TFile* F_in = TFile::Open(FileName.c_str(), "READ");
  if ( ! F_in ){
    std::cout << "\e[1;31;42m";
    std::cout << "Couldn't open file. Skipping.";
    std::cout << "\e[0m" << std::endl;
    delete F_in;
    return;
  }
  std::cout << "Trying to open Tree: " << TreeName << std::endl;

  TTree* T_in = (TTree*) F_in->Get( TreeName.c_str() );;
  if ( ! T_in ){
    std::cout << "\e[1;31;42m";
    std::cout << "Couldn't read Tree from file. Skipping.";
    std::cout << "\e[0m" << std::endl;
    F_in->Close();
    delete F_in;
    return;
  }


  TTree* T_Ev = (TTree*) F_in->Get( "EventTuple" );
  if ( T_Ev )
  {
    if (T_Ev->InheritsFrom("TTree") )
    {
      double OldEntries = std::stod(EventTupleEntries.GetTitle());
      double NewEntries = OldEntries + T_Ev->GetEntries();
      EventTupleEntries.SetTitle( std::to_string(NewEntries).c_str() );
      std::cout << "EventTuple Contained " << T_Ev->GetEntries() << " Entries." << std::endl;

    }

  }

  TTree* T_Lumi = (TTree*) F_in->Get( "LumiTuple" );
  if ( T_Lumi )
  {
    if (T_Lumi->InheritsFrom("TTree") )
    {
        double Lumi_File = 0;
        double Lumi_Err_File = 0;

        TTreeFormula Lumi   ("IntegratedLuminosity",    "IntegratedLuminosity",    T_Lumi);
        TTreeFormula LumiErr("IntegratedLuminosityErr", "IntegratedLuminosityErr", T_Lumi);

        for ( int ev = 0; ev < T_Lumi->GetEntries(); ev++)
        {
          T_Lumi->GetEntry(ev);
          Lumi_File     += Lumi.EvalInstance();
          Lumi_Err_File += pow(LumiErr.EvalInstance(), 2);
        }

        double NewLumi = stod(Luminosity.GetTitle()) + Lumi_File;
        Luminosity.SetTitle( std::to_string(NewLumi).c_str() );

        double NewLumiErr = pow( pow( stod(LuminosityErr.GetTitle()), 2) + Lumi_Err_File, 0.5);
        LuminosityErr.SetTitle(std::to_string(NewLumiErr).c_str());

        std::cout << "LumiTuple Contained " << Lumi_File << " +/- " << Lumi_Err_File << std::endl;

    }
  }

  for(auto& A: Aliases)
  {
    T_in->SetAlias(A.first.c_str(), A.second.c_str());
  }



  TTreeFormula CutFormula("CutFormula", m_CommonCuts, T_in);

  for ( auto &var: Branches){
    var.second.AttachTree(T_in);
  }
  for ( auto &var: Formulas){
    var.second.AttachTree(T_in);
  }
  for ( auto &var: Weights){
    var.second.AttachTree(T_in);
  }




  //


  // Event loop goes here

  int nev = T_in->GetEntries();

  nev = std::min((uint)nev, (uint)m_maxev);

  for ( int ev(0), passev(0); ev < nev; ev++){

    T_in->GetEntry(ev);


    // Make sure at least one selection is met.
    if ( CutFormula.GetExpFormula() != "" ){
      if ( CutFormula.EvalInstance() == 0 )
        continue;
    }

    bool PassCuts = false;
    for ( auto &Var: Weights ){
      Var.second.CalculateVar(ev);
      PassCuts += Var.second.GetVar();
    }
    if ( not PassCuts )
    {
      continue;
    }

    for ( auto &Var: Branches ){
      Var.second.CalculateVar(ev);
    }
    for ( auto &Var: Formulas ){
      Var.second.CalculateVar(ev);
    }
    for ( auto &Var: Functions ){
      Var.second.CalculateVar(ev);
    }
    for ( auto &Var: Systematics){
      Var.second->CalculateVar(ev);
    }


/*    for( auto& Hists: Histograms )
    {
      if (Hists.second[0]->GetFillWeight() == 0.0 )
        continue;
      for(auto& H: Hists.second)
        H->AnalyseEvent(ev);
    }
*/

    // If you want to thread in the future do something like this:

    //#pragma omp parallel for schedule(dynamic) num_threads(2)
    for( size_t NthHist = 0; NthHist < Histograms.size(); ++NthHist )
    {
        auto HistIt = Histograms.begin();
        std::advance(HistIt, NthHist);

        std::vector<std::shared_ptr<Histogram>>& Hists = HistIt->second;

        if ( Hists.empty() )
          continue;

        if (Hists[0]->GetFillWeight() == 0.0 )
          continue;

        for(size_t H = 0; H < Hists.size(); H++)
          Hists[H]->AnalyseEvent(ev);
    }

    if ( passev%m_PrintFreq == 0)
      Print(ev, passev, nev);

    if ( m_SaveTree )
      OutputTree->Fill();

    passev++;



  }



  F_in->Close();



}



void Histogrammer::FinaliseDataType()
{
/**
 * This function must be run after processing all files of a certain type.
 * Histograms will be written to the disk during this step.
 * The output file is kept closed and only opened during this stage.
 * If the histogrammer fails during the RunFile stage (e.g. lost tokens, seg fault)
 * all previously processed histograms will be accessible
 */
  std::cout << "Finalising Data Type. Saving Histograms." << std::endl;

  TFile* f_out = TFile::Open(m_HistogramFileName.c_str(), "UPDATE");

  std::string WriteDir = "";

  for ( auto &CutHists: Histograms){
      for( auto& Hist: CutHists.second )
      {
        WriteDir = Hist->GetDirectory();
        HistogrammerTools::SaveHist( Hist->GetHist(), f_out, WriteDir, "UPDATE", m_SaveNorm);
      }
  }

  if ( EventTupleEntries.GetTitle() != (std::string)"0" )
    HistogrammerTools::SaveObject(&EventTupleEntries, f_out, "EventTuples/" + RecoAs, "UPDATE");

  if ( Luminosity.GetTitle() != (std::string)"0" )
  {
      HistogrammerTools::SaveObject(&Luminosity,    f_out, "Luminosity/" + RecoAs, "UPDATE");
      HistogrammerTools::SaveObject(&LuminosityErr, f_out, "Luminosity/" + RecoAs, "UPDATE");
  }

  f_out->Close();

  if ( m_SaveTree ){

    WriteDir = RecoAs + "/" +  DataType;
    HistogrammerTools::SaveObject( OutputTree, OutputTreeFile, WriteDir, "UPDATE");
    OutputTreeFile->Close();
    //sdelete T_friendTree;
    //delete F_FriendTree;

  }
  Histograms.clear();



}

template<typename Stream, typename T>
void PrintValue(Stream& lhs, T Value, int nspace = 25)
{
/**
 * Small helper function to print tabulated text
 */
  lhs << left << setw(nspace) << setfill(' ') << Value;
}

void Histogrammer::Print(int ev, int passev, int TotalEv)
{
/**
 * Print the status of the histogrammer.
 * Will overwrite previous output so as not to spam the terminal and produce miles of output
 *
 */
  int count = 0;

  Output << "\n";
  Output << "Analysing event: " << left << setw(12)  << ev+1;
  Output << "Selected events: " << left << setw(12) << passev+1 ;
  Output << "Total events: "    << left << setw(12) << TotalEv << "\n";
  Output << "|";
  Output << std::string(  50 * ev / TotalEv + 1, '#');
  Output << std::string(  50 - 50 * ev / TotalEv - 1, '-');
  Output << "|" << "  " << 100* ev / TotalEv << " %\n\n";


  if ( VerboseOutput )
  {
    Output << "\n";
    for ( auto &Var: Variables ){
      PrintValue( Output, "Variable"          , 15 );
      PrintValue( Output, Var.first           , 35 );
      PrintValue( Output, Var.second->GetVar(), 15 );
      Output << (count%3 == 2 ? "\n" : "| ");
      count++;
    }


    for ( auto &Syst: Systematics){
      PrintValue( Output, "Systematic"         , 15 );
      PrintValue( Output, Syst.first           , 35 );
      PrintValue( Output, Syst.second->GetVar(), 15 );
      Output << (count%3 == 2 ? "\n" : "| ");
      count++;
    }

    for ( auto &Cut: Weights){
      PrintValue( Output, "Cut"                , 15 );
      PrintValue( Output, Cut.first            , 35 );
      PrintValue( Output, Cut.second.GetVar()  , 15 );
      Output << (count%3 == 2 ? "\n" : "| ");
      count++;

    }

    Output << "\n\n";

    if ( PrintHistogram.use_count()>0 )
      Output << PrintHistogram->GetHist();
    Output << "\n\n\n";

  }

  cout << Output;
}



void Histogrammer::Reset()
{
/**
 * Reset the histogrammer to a clean state.
 * esentially delete everything
 *
 */

  Histograms.clear();
  RootHistograms.clear();

  Systematics.clear();

  Variables.clear();
  Branches.clear();
  Functions.clear();
  Formulas.clear();

  Aliases.clear();

  Weights.clear();
}

