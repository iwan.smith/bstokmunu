/** \file
 * Histogrammer_AddOn.cxx
 *
 *  Created on: 17 Jul 2017
 *      Author: ismith
 */

#include "Histogrammer_AddOn.h"
#include "HistogrammerTools.h"
#include "Ds_Fitter.h"

void Histogrammer_AddOn::SubtractDs(std::string filename, std::string SampleType, bool SaveNorm)
{
/**
 * Helper function which will run over the Ds histograms with backgrounds to be subtracted.
 * If a Histogram name ends with `_Ds` the background subtraction will be run and a
 * a copy of the histogram generated with `_Ds` replaced with `_rmbg`, and one less dimension
 */

    TFile* f_hist = TFile::Open(filename.c_str(), "UPDATE");

    std::string Syst;
    std::string Mode;
    std::string Sample;
    std::string Cut;

    std::string Path;

    std::vector<std::string> HistogramPaths;
    std::vector<std::string> Histnames;

    Syst = "Nominal";
    Path = Syst;

    auto ModeList = ((TDirectory*)f_hist->Get(Path.c_str()))->GetListOfKeys();
    for( int m = 0; m < ModeList->GetEntries(); m++)
    {
      Mode = ModeList->At(m)->GetName();
      Path = Syst + "/" + Mode;


      auto SampleList = ((TDirectory*)f_hist->Get(Path.c_str()))->GetListOfKeys();
      for( int sm = 0; sm < SampleList->GetEntries(); sm++)
      {
        Sample = SampleList->At(sm)->GetName();
        Path = Syst + "/" + Mode + "/" + Sample;

        if ( Sample != SampleType )
            continue;


        auto SampleList = ((TDirectory*)f_hist->Get(Path.c_str()))->GetListOfKeys();
        for( int sm = 0; sm < SampleList->GetEntries(); sm++)
        {
          Cut = SampleList->At(sm)->GetName();
          Path = Syst + "/" + Mode + "/" + Sample + "/" + Cut;

          HistogramPaths.push_back(Path);
        }
      }
    }



    for ( auto &path: HistogramPaths)
    {

      Histnames.clear();

      auto HistList = ((TDirectory*)f_hist->Get(path.c_str()))->GetListOfKeys();
      for( int h = 0; h < HistList->GetEntries(); h++)
      {
        std::string HistName = HistList->At(h)->GetName();
        if ( HistName.find("_Ds") == HistName.size()-3 )
        {
          Histnames.push_back(HistName);
        }
      }


      for( auto &h:Histnames )
      {
        TH1* h_subtr = (TH1*)f_hist->Get( (path + "/" + h).c_str() );
        if ( h_subtr->InheritsFrom("TH2") or h_subtr->InheritsFrom("TH3") )
            std::cout << path << "/" << h << std::endl;
        else
            continue;


        Ds_Fitter Subtractor( h_subtr );
        TH1* h_subtracted = Subtractor.Subtract();

        std::string OutName = h_subtracted->GetName();

        // remove the "_Ds" from the end of the name
        OutName.pop_back();
        OutName.pop_back();
        OutName.pop_back();

        OutName += "_rmbg";

        h_subtracted->SetName( OutName.c_str() );

        HistogrammerTools::SaveHist(h_subtracted, f_hist, path, "UPDATE", SaveNorm);

        auto fit_plots = Subtractor.GetCanvases();
        for (auto &c: fit_plots)
            HistogrammerTools::SaveObject(c.get(), f_hist, path+"/"+h+"_FitPlots", "UPDATE");

      }
    }

    f_hist->Close();
    delete f_hist;

}


