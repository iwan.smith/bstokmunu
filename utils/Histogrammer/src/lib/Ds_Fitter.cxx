/** \file
 *
 * Ds_Fitter.cxx
 *
 *  Created on: 14 Jul 2017
 *      Author: ismith
 */


#include "Ds_Fitter.h"
#include "RooFit.h"
#include "RooMsgService.h"

Ds_Fitter::Ds_Fitter(TH1* hist)
{
/**
 * Constructor to subtract the \f$D_{s}\f$ sidebands
 *
 * Given a n-D histogram the class will reduce the histogram into n-1 dimensions
 *
 *
*/
  gErrorIgnoreLevel = kInfo;

  RooMsgService::instance().setStreamStatus(1, false);
  RooMsgService::instance().setStreamStatus(0, false);

  if ( hist )
  {
    h_to_subtract = hist;
    n_Dim  = h_to_subtract->GetDimension();

    std::cout << "Dimensions: "<<  n_Dim << std::endl;

    switch ( n_Dim )
    {

    case 2:
      h_Ds   = ((TH2F*)h_to_subtract)->ProjectionY();
      break;
    case 3:
      h_Ds   = ((TH3F*)h_to_subtract)->ProjectionZ();
      break;
    default:
      std::cout << "Histogram Must be 2 or 3 Dimensions in order to use the Fit Subtraction" << std::endl;
    }

    h_mass = h_Ds->GetXaxis()->GetXmax();
    l_mass = h_Ds->GetXaxis()->GetXmin();

    std::cout << l_mass << "  " << h_mass << std::endl;

  }

  D_M.reset(new RooRealVar("Ds_M", "Ds Mass", l_mass, h_mass) );

  D_DG_mu.reset(new RooRealVar("D_mu", "Ds Double Gauss Mean", 1969.72, 1965.3, 1977.3) );

  D_DG_s1.reset(new RooRealVar("D_s1", "Ds Double Gauss sigma 1", 10.7097, 1, 13) );
  D_DG_s2.reset( new RooRealVar("D_s2", "Ds Double Gauss sigma 2", 5.51790, 1, 9) );

  D_Gauss1_pdf.reset( new RooGaussian("D_Gauss1_pdf", "Ds Gaussian 1 pdf", *D_M, *D_DG_mu, *D_DG_s1) );
  D_Gauss2_pdf.reset( new RooGaussian("D_Gauss2_pdf", "Ds Gaussian 2 pdf", *D_M, *D_DG_mu, *D_DG_s2) );

  D_DG_f1.reset( new RooRealVar("D_f1", "Ds Double Gauss fraction 1", 0.237746, 0.0, 1.0) );
  D_DoubleGauss_pdf.reset( new RooAddPdf("D_DoubleGauss_pdf", "Ds Double Gaussian", RooArgList(*D_Gauss1_pdf, *D_Gauss2_pdf), RooArgList( *D_DG_f1) ) );


  pol_var_1.reset( new RooRealVar("pol_var_1", "pol_var_1", 11200, -1000000, 1000000 ) );
  pol_var_2.reset( new RooRealVar("pol_var_2", "pol_var_2", 0, -100, 100 ) );

  poly.reset( new RooPolynomial("poly", "poly", *D_M, RooArgList(*pol_var_1, *pol_var_2)) );



}



void Ds_Fitter::Fit( TH1* Hist )
{

/**
 * Fit a 1-D \f$m(D_s)\f$ histogram and cache the yield
 */


  Data. reset( new RooDataHist ("data", "data", *D_M, Hist) );
  s_nev.reset( new RooRealVar  ("s_nev", "s_nev", Data->sumEntries()*0.5, 0, Data->sumEntries()) );
  b_nev.reset (new RooRealVar  ("b_nev", "b_nev", Data->sumEntries()*0.5, 0, Data->sumEntries()) );


  Ds_pdf.reset( new RooAddPdf("data_dist", "data_dist", RooArgList(*D_DoubleGauss_pdf, *poly), RooArgList(*s_nev, *b_nev)) );

  std::cout << Ds_pdf << "  " << Data << std::endl;

  gErrorIgnoreLevel = kInfo;
  RooMsgService::instance().setStreamStatus(1,false);
  Ds_pdf->fitTo(*Data, RooFit::Extended(), RooFit::NumCPU(8), RooFit::Verbose(kFALSE));



}

void Ds_Fitter::GetSigYield( double &nsig, double &nsig_err ) const
{
/**
 * Return the signal yield with error.
 * Function will update the values passed by reference
 */

  nsig     = s_nev->getValV();
  nsig_err = s_nev->getError();
}

void Ds_Fitter::GetBGYield ( double &nsig, double &nsig_err ) const
{
/**
 * Return the background yield with error.
 * Function will update the values passed by reference
 */

  nsig = b_nev->getValV();
  nsig_err = b_nev->getError();
}


void Ds_Fitter::FixSignal( bool fix ){
  /**
   * Fix the shape parameters of the signal component.
   * Yield will be left free.
   */
  D_DG_mu->setConstant( fix );
  D_DG_s1->setConstant( fix );
  D_DG_s2->setConstant( fix );
  D_DG_f1->setConstant( fix );
}
void Ds_Fitter::FixBG( bool fix ){
/**
 * Fix the shape parameters of the background component.
 * Yield will be left free.
 */

  pol_var_1->setConstant( fix );
  pol_var_2->setConstant( fix );
}




TH1* Ds_Fitter::Subtract_2D()
{
/**
 * Given a 2D Histogram, subtract the combinatorics and return a 1D histogram
 */

    TH2* hist = (TH2*)h_to_subtract;

    Fit_Plots.clear();

    int nBins = h_to_subtract->GetNbinsX();
    std::cout << "Will perfom " << nBins << " Fits to the Ds mass." << std::endl;

    h_subtracted = hist->ProjectionX();
    h_subtracted->Reset();
    h_subtracted->SetName( hist->GetName() );


    Fit(h_Ds);
    std::shared_ptr<TCanvas> canv( new TCanvas("Fit_Canvas_All", "Fit Canvas", 900, 900) );
    Plot(canv);
    Fit_Plots.push_back(canv);
    FixSignal();

    for ( int bin = 1; bin <= nBins; bin++)
    {


        std::cout << D_M->getMin() << "  " << D_M->getMax() << std::endl;

        TH1* h_Slice = hist->ProjectionY("SlicedHist", bin,bin);


        int Bins_Ds = h_Slice->GetNbinsX();
        for ( int bin_Ds=1; bin_Ds <= Bins_Ds; bin_Ds++)
            h_Slice->AddBinContent(bin_Ds, 1);


        Fit(h_Slice);

        double yield(0), error(0);
        GetSigYield(yield, error);

        h_subtracted->SetBinContent(bin, yield);
        h_subtracted->SetBinError  (bin, error);

        std::string canvas_name = "Fit_Canvas_"+ std::to_string(bin);

        canv = std::shared_ptr<TCanvas>(new TCanvas(canvas_name.c_str(), "Fit Canvas", 900, 900));
        Plot(canv);
        Fit_Plots.push_back(canv);


    }
    return h_subtracted;
}


TH1* Ds_Fitter::Subtract_3D()
{
/**
 * Given a 3D Histogram, subtract the combinatorics and return a 2D histogram
 */

    TH3* hist = (TH3*)h_to_subtract;

    Fit_Plots.clear();

    int nBinsX = h_to_subtract->GetNbinsX();
    int nBinsY = h_to_subtract->GetNbinsY();
    std::cout << "Will perfom " << nBinsX << " x " << nBinsY << " Fits to the Ds mass." << std::endl;

    h_subtracted = hist->Project3D("xy");
    h_subtracted->Reset();
    h_subtracted->SetName( hist->GetName() );


    Fit(h_Ds);
    std::shared_ptr<TCanvas> canv( new TCanvas("Fit_Canvas_All", "Fit Canvas", 900, 900) );
    Plot(canv);
    Fit_Plots.push_back(canv);
    FixSignal();

    for ( int binX = 1; binX <= nBinsX; binX++)
    {
        for ( int binY = 1; binY <= nBinsY; binY++)
        {


            std::cout << D_M->getMin() << "  " << D_M->getMax() << std::endl;

            TH1* h_Slice = hist->ProjectionZ("SlicedHist", binX, binX, binY, binY);


            int Bins_Ds = h_Slice->GetNbinsX();
            for ( int bin_Ds=1; bin_Ds <= Bins_Ds; bin_Ds++)
                h_Slice->AddBinContent(bin_Ds, 1);


            Fit(h_Slice);

            double yield(0), error(0);
            GetSigYield(yield, error);

            h_subtracted->SetBinContent(binX, binY, yield);
            h_subtracted->SetBinError  (binX, binY, error);

            std::string canvas_name = "Fit_Canvas_"+ std::to_string(binX) + "_" + std::to_string(binY);

            canv = std::shared_ptr<TCanvas>(new TCanvas(canvas_name.c_str(), "Fit Canvas", 900, 900));
            Plot(canv);
            Fit_Plots.push_back(canv);
        }

    }

    return h_subtracted;

}

TH1* Ds_Fitter::Subtract(){
/**
 * Given a 2 or 3 dimensional histogram, call the correct subtraction function.
 */


    if (n_Dim == 2 )
        return Subtract_2D();
    else if ( n_Dim == 3 )
        return Subtract_3D();

    return nullptr;


}






void Ds_Fitter::Plot( std::shared_ptr<TCanvas> canv ){
    std::shared_ptr<RooPlot> Dframe ( (RooPlot*)D_M->frame(RooFit::Title("K K #pi Invariant Mass")) );
    std::cout << s_nev->getValV()  << std::endl;
    std::cout << s_nev->getError() << std::endl;
    std::cout << b_nev->getValV()  << std::endl;
    std::cout << b_nev->getError() << std::endl;

    Data->plotOn(Dframe.get());
    Ds_pdf->plotOn(Dframe.get(), RooFit::Components(D_DoubleGauss_pdf->GetName()), RooFit::LineColor(3), RooFit::LineStyle(1), RooFit::LineWidth(1));
    Ds_pdf->plotOn(Dframe.get(), RooFit::Components(poly->GetName()), RooFit::LineColor(4), RooFit::LineStyle(1), RooFit::LineWidth(1));
    Ds_pdf->plotOn(Dframe.get(), RooFit::LineColor(2), RooFit::LineWidth(1));

    TPad* pad1 = new TPad("pad1", "The pad 70% of the height",0.0,0.3,1.0,1.0);
    pad1->cd();
    Dframe->DrawClone();

    RooHist* hpull = Dframe->pullHist() ;
    std::shared_ptr<RooPlot> DpullFrame ( (RooPlot*)D_M->frame(RooFit::Title("Fit Pulls")) ) ;
    DpullFrame->addPlotable(hpull,"P") ;

    TPad* pad2 = new TPad("pad2", "The pad 30% of the height",0.0,0.0,1.0,0.3);
    pad2->cd();

    DpullFrame->DrawClone();

    canv->cd();
    pad1->Draw();
    pad2->Draw();

    //TFile* f_out = TFile::Open("/tmp/Out.root", "RECREATE");
    //canv->Write();
    //f_out->Close();


//    /canv->Print("Subtracted.pdf");

}




