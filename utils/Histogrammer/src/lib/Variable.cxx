/** \file
 * Variable.cxx
 *
 *  Created on: 7 Jun 2017
 *      Author: ismith
 */


#include "Variable.h"

#include <iostream>



//=============================================================================
// Class implementation for Variable_Branch
//=============================================================================



Variable_Branch::Variable_Branch(std::string BranchName)
  :m_BranchName(BranchName)
{}


void Variable_Branch::CalculateVar(int ev)
{
/**
 * Read the value from a branch in a ROOT Tree.
 * This should probably be templated to make it more readable
 *
 */
  switch( m_type ){
    case 'i':
        I = i;
        D = i;
        F = i;
        L = i;
        l = i;
        O = i;
        break;
    case 'I':
        i = I;
        D = I;
        F = I;
        L = I;
        l = I;
        O = I;
        break;
    case 'D':
        i = D;
        I = D;
        F = D;
        L = D;
        l = D;
        O = D;
        break;
    case 'F':
        i = F;
        I = F;
        D = F;
        L = F;
        l = F;
        O = F;
        break;
    case 'L':
        i = L;
        I = L;
        D = L;
        F = L;
        l = L;
        O = L;
        break;
    case 'l':
        i = l;
        I = l;
        D = l;
        F = l;
        L = l;
        O = l;
        break;
    case 'O':
        i = O;
        I = O;
        D = O;
        F = O;
        L = O;
        l = O;
        break;
  }
}

pair<void*, char> Variable_Branch::GetPointer(char type)
{
/**
 * Return a pointer to the value and a character declaring type
 */
  switch( type ){
    case 'i':
        return pair<void*, char>(&i, 'i');
    case 'I':
        return pair<void*, char>(&I, 'I');
    case 'D':
        return pair<void*, char>(&D, 'D');
    case 'F':
        return pair<void*, char>(&F, 'F');
    case 'L':
        return pair<void*, char>(&L, 'L');
    case 'l':
        return pair<void*, char>(&l, 'l');
    case 'O':
        return pair<void*, char>(&O, 'O');
  }

  // Return a null pointer if no valid value was given
  return pair<void*, char>(nullptr, 'O');

}

void Variable_Branch::AttachTree(TTree* Tree)
{
/**
 * Link a ROOT Tree to the branch.
 *
 * This allows for a branch to be specified before a file is even opened.
 * Useful when running over multiple files with similar trees
 *
 */

  TBranch* Test_Branch = Tree->GetBranch( m_BranchName.c_str() );
  if( not Test_Branch ){
      std::cout << "Branch: " << m_BranchName << "Does not exist!" << std::endl;
  }
  std::string BranchTitle = Test_Branch->GetTitle();
  m_type = BranchTitle.back();

  switch ( m_type ){
      case 'F':
          Tree->SetBranchAddress( m_BranchName.c_str(), &F );
          break;
      case 'D':
          Tree->SetBranchAddress( m_BranchName.c_str(), &D );
          break;
      case 'I':
          Tree->SetBranchAddress( m_BranchName.c_str(), &I );
          break;
      case 'i':
          Tree->SetBranchAddress( m_BranchName.c_str(), &i );
          break;
      case 'L':
          Tree->SetBranchAddress( m_BranchName.c_str(), &L );
          break;
      case 'l':
          Tree->SetBranchAddress( m_BranchName.c_str(), &l );
          break;
      case 'O':
          Tree->SetBranchAddress( m_BranchName.c_str(), &O );
          break;


  }


}

//=============================================================================
// Class implementation for Variable_Formula
//=============================================================================

Variable_Formula::Variable_Formula(std::string name, const TCut& formula)
    :m_name(name)
    ,m_formula(formula)
{
}

void Variable_Formula::AttachTree(TTree* Tree)
{
/**
 * Link a ROOT Tree to the formula.
 *
 * This allows for a formula to be specified before a file is even opened.
 * Useful when running over multiple files with similar trees
 *
 */

  Tree->SetAlias(m_name.c_str(), m_formula);
  m_TTreeFormula = shared_ptr<TTreeFormula>( new TTreeFormula(m_name.c_str(), m_formula, Tree) );
}

void Variable_Formula::CalculateVar(int ev)
{
/**
 * Calculate and cache the result of a formula
 */

  if ( calculatedevent == ev) return;
  calculatedevent = ev;

  //std::cout << m_TTreeFormula->GetTitle() << std::endl;
  CalculatedResult = m_TTreeFormula->EvalInstance();
}





//=============================================================================
// Class implementation to combine formulae
//=============================================================================


void Combined_Variable_Formula::CalculateVar(int ev)
{
/**
 * Calculate and multiply the results of multiple formulae.
 * This is useful when chaining several cuts.
 *
 */
  if ( ev == calculatedevent ) return;
  calculatedevent = ev;

  CalculatedResult = 1.0;
  for ( auto& F: m_Formulae )
  {
    if ( m_Variables.find(F) == m_Variables.end() )
    {
        std::cout << F << " Is not found as a cut/formula. Please ensure it has been added." << std::endl;
        for ( auto& Form: m_Formulae ) std::cout << Form << "  ";
        std::abort();
    }
    m_Variables.at(F).CalculateVar(ev);
    CalculatedResult *= m_Variables.at(F).GetVar();

    if ( CalculatedResult == 0 )
      return;
  }


}



//=============================================================================
// Class implementation for Variable_Function
//=============================================================================



Variable_Function::Variable_Function(map<std::string, Variable*>& Branches, std::function<double (map<std::string, Variable*>&, void*)> f , void* AdditionalArgs)
    :VariableCalculator(f)
    ,Additional_Arguments(AdditionalArgs)
    ,m_Branches(Branches)
{
}




void Variable_Function::CalculateVar(int ev)
{
    CalculatedResult = VariableCalculator( m_Branches, Additional_Arguments );
}

