/** \file
 * Systematic.cxx
 *
 *  Created on: 7 Jun 2017
 *      Author: ismith
 *
 *  Note: The terms systematic and correction are used interchangeably.
 *      - A systematic/correction is a weight given to each event.
 *      - This can be a correction due to a disagreement between data/MC
 *      - A lookup weight to subtract backgrounds
 *      - etc.
 *
 *  Toys are made for each correction. A toy is a random copy of the correction factor
 *  with all values replaced by random, statistically compatible values
 *
 */

#include "Systematic.h"
#include "HistogrammerTools.h"
#include <iostream>
#include <cstdlib>
#include "TMath.h"
#include <cmath>

#include <assert.h>

using namespace std;

Systematic::Systematic(int toys)
{
/**
 * Default constructor for the systematic base class. The number of toys are required.
 */

  SetNToys(toys);

}

void Systematic::SetNToys( int toys )
{
/**
 * Set the number of toys to be generated.
 * The first toy is the true value, hence `toys+1` in the code.
 */

  nToys = toys;
  Weights = vector<double>(toys+1, 1.);



}


void Systematic::SetVariables(map<string, Variable*> *Vars)
{
/**
 * Set the variable to be used when looking up values
 */
  Variables = Vars;
}


void Systematic::ResetToys()
{
/**
 * Reset the histogram containing toy information
 */

  fill(Weights.begin(), Weights.end(), 1.0);

}


double Systematic::GetVar() const
{
/**
 * Return the true value of the correction
 */
  return Weights[0];
}





/********************************************************
 *
 * Declare the code for the Variable reweighter systematic
 *
 ********************************************************/



void   Systematic_Variable::CalculateVar(int)
{
/**
 * Weight events based on a variable.
 * Lookup a variable from a branch and weight based on this value
 *
 */
  Weights[0] = (*Variables)[m_VarName]->GetVar();

  if (not isfinite(Weights[0]))
    Weights[0] = 1;

  for ( int toy = 1; toy <= nToys; toy++)
    Weights[toy] = Weights[0];

}






/********************************************************
 *
 * Define the Code for the Histogram Re Weighting Class
 *
 ********************************************************/







Systematic_Hist::Systematic_Hist()
{
/**
 * Default constructor for the Systematic_Hist class
 */
  TreatOutOfBounds = OverflowTreatment::Unity;
    Variable1 = "";
    Variable2 = "";
    Variable3 = "";

}

Systematic_Hist::Systematic_Hist(int toys, string FileName, string HistName, OverflowTreatment Treatment, string Var1, string Var2, string Var3)
    :Systematic(toys)
    ,TreatOutOfBounds(Treatment)
{
/**
 * Constructor for the "Histogram Correction" class.
 *
 * A histogram is used as a lookup table and the found values used to weight events
 *
 */


    TFile* F_Syst = TFile::Open(FileName.c_str(), "READ");
    if ( not F_Syst ){
        cout << "Failed to open file: " << FileName << endl;
    }

    TH1* LoadedHist = (TH1*)F_Syst->Get(HistName.c_str());
    if ( not LoadedHist ){
        cout << "Failed to open Histogram: " << HistName << endl;
    }
    LoadedHist->SetDirectory(0);

    F_Syst->Close();

    gDirectory->cd(0);
    h_Syst.push_back( shared_ptr<TH1>(LoadedHist) );

    for (int toy = 0; toy < nToys; toy++){
        h_Syst.push_back( shared_ptr<TH1>( HistogrammerTools::CloneToyHist(LoadedHist)));
    }



    Variable1 = Var1;
    Variable2 = Var2;
    Variable3 = Var3;
}

Systematic_Hist::Systematic_Hist(Systematic_Hist& obj, string Var1, string Var2, string Var3):Systematic(obj.GetnToys())
{
/**
 * "Copy-in-quotes" constructor for the Systematic_Hist class.
 * Often the same lookup table will be used for different variables.
 * This copy constructor will preserve the toys ensuring correlations are preserved.
 *
 */

    TreatOutOfBounds = obj.TreatOutOfBounds;
    //F_Syst = obj.F_Syst;
    h_Syst = obj.h_Syst;

    Variable1 = Var1;
    Variable2 = Var2;
    Variable3 = Var3;
}


void Systematic_Hist::CalculateVar(int ev)
{
/**
 * Calculate the weights and cache the value.
 * You probably won't need to call this method.
 *
 */

    if ( ev == calculatedevent)
        return;
    calculatedevent = ev;


    try
    {
      Value1 = (*Variables).at(Variable1)->GetVar();
      if ( Variable2 != "")
          Value2 = (*Variables).at(Variable2)->GetVar();
      if ( Variable3 != "")
          Value3 = (*Variables).at(Variable3)->GetVar();
    }
    catch( std::out_of_range& err )
    {
        std::cout << err.what() << std::endl;
        std::cout << "One of the following variables not found:\n";
        std::cout << "    " << Variable1 << "\n";
        std::cout << "    " << Variable2 << "\n";
        std::cout << "    " << Variable3 << "\n";
    }
    for ( int toy = 0; toy <= nToys; toy++){

        double Value = HistogrammerTools::FindBinContent(h_Syst[toy].get(), TreatOutOfBounds, Value1, Value2, Value3);

        if ( not TMath::Finite(Value)){
            Value = 1;
            //if ( toy == 0 ) std::cerr << "WARNING! Systematic returned NaN/NotFinite. Setting to 1!" << std::endl;
        }

        Weights[toy]  = Value;
    }

}



/********************************************************
 *
 * Define the Code for the Histogram Combiner Class
 *
 ********************************************************/


void Systematic_Combiner::CalculateVar(int ev)
{
  /**
   * Calculate the weights and cache the value.
   * You probably won't need to call this method.
   *
   */

    if ( ev == calculatedevent)
        return;
    calculatedevent = ev;

    std::fill(Weights.begin(), Weights.end(), 1);

    for (auto& Syst: Systematics){
        Syst->CalculateVar(ev);
        auto S_weights = Syst->GetWeights();

        for ( int toy = 0; toy <= nToys; toy++){
            Weights[toy]*= S_weights[toy];
        }
    }



}




/*******************************************************
 *
 * Implement the PID Systematics classes
 *
 *
 ******************************************************/

Systematic_PID::Systematic_PID(int toys, string PIDType, string Part_name)
    :Systematic(toys)
{
/**
 * Create a PID correction.
 * This is specific to our analysis and shouldn't be used by others
 */
  if ( PIDType == "K_Tight"){

    Data_Up.reset( new Systematic_Hist(nToys,
      "/eos/lhcb/wg/semileptonic/Bs2KmunuAna/Histogrammer/Input/PID/PerfHists_K_Strip20_MagUp_P_ETA_nTracks.root",
      "K_(DLLK-DLLp)>5 && DLLK>5 && (DLLK-DLLmu)>5_All",
      OverflowTreatment::Closest,
      Part_name+"_P", Part_name+"_ETA", "nTracks") );

    Data_Down.reset( new Systematic_Hist(nToys,
      "/eos/lhcb/wg/semileptonic/Bs2KmunuAna/Histogrammer/Input/PID/PerfHists_K_Strip20_MagDown_P_ETA_nTracks.root",
      "K_(DLLK-DLLp)>5 && DLLK>5 && (DLLK-DLLmu)>5_All",
      OverflowTreatment::Closest,
      Part_name+"_P", Part_name+"_ETA", "nTracks") );


    MC_Up.reset( new Systematic_Hist(nToys,
      "/eos/lhcb/wg/semileptonic/Bs2KmunuAna/Histogrammer/Input/PID/PerfHists_K_Strip20_MagUp_P_ETA_nTracks_MC.root",
      "K_(DLLK-DLLp)>5 && DLLK>5 && (DLLK-DLLmu)>5_All",
      OverflowTreatment::Closest,
      Part_name+"_P", Part_name+"_ETA", "nTracks") );

    MC_Down.reset( new Systematic_Hist(nToys,
      "/eos/lhcb/wg/semileptonic/Bs2KmunuAna/Histogrammer/Input/PID/PerfHists_K_Strip20_MagDown_P_ETA_nTracks_MC.root",
      "K_(DLLK-DLLp)>5 && DLLK>5 && (DLLK-DLLmu)>5_All",
      OverflowTreatment::Closest,
      Part_name+"_P", Part_name+"_ETA", "nTracks") );

  }

  if ( PIDType == "K_Loose"){

    Data_Up.reset( new Systematic_Hist(nToys,
      "/eos/lhcb/wg/semileptonic/Bs2KmunuAna/Histogrammer/Input/PID/PerfHists_K_Strip20_MagUp_P_ETA_nTracks.root",
      "K_DLLK>-2_All",
      OverflowTreatment::Closest,
      Part_name+"_P", Part_name+"_ETA", "nTracks") );

    Data_Down.reset( new Systematic_Hist(nToys,
      "/eos/lhcb/wg/semileptonic/Bs2KmunuAna/Histogrammer/Input/PID/PerfHists_K_Strip20_MagDown_P_ETA_nTracks.root",
      "K_DLLK>-2_All",
      OverflowTreatment::Closest,
      Part_name+"_P", Part_name+"_ETA", "nTracks") );


    MC_Up.reset( new Systematic_Hist(nToys,
      "/eos/lhcb/wg/semileptonic/Bs2KmunuAna/Histogrammer/Input/PID/PerfHists_K_Strip20_MagUp_P_ETA_nTracks_MC.root",
      "K_DLLK>-2_All",
      OverflowTreatment::Closest,
      Part_name+"_P", Part_name+"_ETA", "nTracks") );

    MC_Down.reset( new Systematic_Hist(nToys,
      "/eos/lhcb/wg/semileptonic/Bs2KmunuAna/Histogrammer/Input/PID/PerfHists_K_Strip20_MagDown_P_ETA_nTracks_MC.root",
      "K_DLLK>-2_All",
      OverflowTreatment::Closest,
      Part_name+"_P", Part_name+"_ETA", "nTracks") );

  }


  else if ( PIDType == "Mu_Tight"){

    Data_Up.reset( new Systematic_Hist(nToys,
      "/eos/lhcb/wg/semileptonic/Bs2KmunuAna/Histogrammer/Input/PID/PerfHists_Mu_Strip20_MagUp_P_ETA_nTracks.root",
      "Mu_(DLLmu-DLLp)>0 && DLLmu>2 && (DLLmu-DLLK)>0 && IsMuon==1_All",
      OverflowTreatment::Closest,
      Part_name+"_P", Part_name+"_ETA", "nTracks") );

    Data_Down.reset( new Systematic_Hist(nToys,
      "/eos/lhcb/wg/semileptonic/Bs2KmunuAna/Histogrammer/Input/PID/PerfHists_Mu_Strip20_MagDown_P_ETA_nTracks.root",
      "Mu_(DLLmu-DLLp)>0 && DLLmu>2 && (DLLmu-DLLK)>0 && IsMuon==1_All",
      OverflowTreatment::Closest,
      Part_name+"_P", Part_name+"_ETA", "nTracks") );

    MC_Up.reset( new Systematic_Hist(nToys,
      "/eos/lhcb/wg/semileptonic/Bs2KmunuAna/Histogrammer/Input/PID/PerfHists_Mu_Strip20_MagUp_P_ETA_nTracks_MC.root",
      "Mu_(DLLmu-DLLp)>0 && DLLmu>2 && (DLLmu-DLLK)>0 && IsMuon==1_All",
      OverflowTreatment::Closest,
      Part_name+"_P", Part_name+"_ETA", "nTracks") );

    MC_Down.reset( new Systematic_Hist(nToys,
      "/eos/lhcb/wg/semileptonic/Bs2KmunuAna/Histogrammer/Input/PID/PerfHists_Mu_Strip20_MagDown_P_ETA_nTracks_MC.root",
      "Mu_(DLLmu-DLLp)>0 && DLLmu>2 && (DLLmu-DLLK)>0 && IsMuon==1_All",
      OverflowTreatment::Closest,
      Part_name+"_P", Part_name+"_ETA", "nTracks") );
  }

  else if ( PIDType == "Pi_Loose"){
    Data_Up.reset( new Systematic_Hist(nToys,
      "/eos/lhcb/wg/semileptonic/Bs2KmunuAna/Histogrammer/Input/PID/PerfHists_Pi_Strip20_MagUp_P_ETA_nTracks.root",
      "Pi_DLLK<20.0_All",
      OverflowTreatment::Closest,
      Part_name+"_P", Part_name+"_ETA", "nTracks") );

    Data_Down.reset( new Systematic_Hist(nToys,
      "/eos/lhcb/wg/semileptonic/Bs2KmunuAna/Histogrammer/Input/PID/PerfHists_Pi_Strip20_MagDown_P_ETA_nTracks.root",
      "Pi_DLLK<20.0_All",
      OverflowTreatment::Closest,
      Part_name+"_P", Part_name+"_ETA", "nTracks") );

    MC_Up.reset( new Systematic_Hist(nToys,
      "/eos/lhcb/wg/semileptonic/Bs2KmunuAna/Histogrammer/Input/PID/PerfHists_Pi_Strip20_MagUp_P_ETA_nTracks_MC.root",
      "Pi_DLLK<20.0_All",
      OverflowTreatment::Closest,
      Part_name+"_P", Part_name+"_ETA", "nTracks") );

    MC_Down.reset( new Systematic_Hist(nToys,
      "/eos/lhcb/wg/semileptonic/Bs2KmunuAna/Histogrammer/Input/PID/PerfHists_Pi_Strip20_MagDown_P_ETA_nTracks_MC.root",
      "Pi_DLLK<20.0_All",
      OverflowTreatment::Closest,
      Part_name+"_P", Part_name+"_ETA", "nTracks") );
  }



}


Systematic_PID::Systematic_PID(Systematic_PID& obj, string Part_name)
  :Systematic(obj.GetnToys())
{
/**
 * "Copy-in-quotes" constructor for the systematic histograms
 *
 */

  Data_Up  .reset(new Systematic_Hist(*obj.Data_Up,   Part_name + "_P", Part_name + "_ETA", "nTracks") );
  Data_Down.reset(new Systematic_Hist(*obj.Data_Down, Part_name + "_P", Part_name + "_ETA", "nTracks") );
  MC_Up    .reset(new Systematic_Hist(*obj.MC_Up,     Part_name + "_P", Part_name + "_ETA", "nTracks") );
  MC_Down  .reset(new Systematic_Hist(*obj.MC_Down,   Part_name + "_P", Part_name + "_ETA", "nTracks") );

}

void Systematic_PID::SetVariables(map<string, Variable*> *Vars)
{
/**
 * Provide the container used to hold variables so that they can be accesed at run-time.
 */

  Variables = Vars;
  Data_Up  ->SetVariables(Vars);
  Data_Down->SetVariables(Vars);
  MC_Up    ->SetVariables(Vars);
  MC_Down  ->SetVariables(Vars);

}

void Systematic_PID::CalculateVar(int ev)
{

/**
 * Calculate the PID correction
 *
 */
    if ( calculatedevent == ev )
        return;
    calculatedevent = ev;

    shared_ptr<Systematic_Hist> Data;
    shared_ptr<Systematic_Hist> MC;



    if( int((*Variables)["MagnetPol"]->GetVar()) == 1)
    {
        Data = Data_Up;
        MC   = MC_Up;
    }
    else
    {
        Data = Data_Down;
        MC   = MC_Down;
    }

    Data->CalculateVar(ev);
    MC  ->CalculateVar(ev);

    auto DataWeights = Data->GetWeights();
    auto MCWeights   = MC->GetWeights();

    for ( int toy = 0; toy <= nToys; toy++){

        double Value = DataWeights[toy] / MCWeights[toy];
        if ( TMath::IsNaN(Value) or not TMath::Finite(Value)){
            Value = 1;
            //if (toy == 0) std::cerr << "WARNING! Event: " << ev << ". Systematic returned NaN/NotFinite. Setting to 1!" << std::endl;
        }

        Weights[toy] =  Value;
    }



}









