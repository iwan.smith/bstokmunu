/** \file
 * Histogram.cxx
 *
 *  Created on: 15 May 2017
 *      Author: ismith
 */

#include "Histogram.h"
#include <iostream>


// Cache the bin location. I know this is horrible, but the speed boost makes it worth it
map<std::string, std::shared_ptr<CachedBinLocation>> CachedBins;


//=============================================================================
// Functions for Base Class Histogram
//=============================================================================




Histogram::Histogram(Systematic* Syst, Combined_Variable_Formula* Cut)
    :m_Systematic(Syst)
    ,m_Weight(Cut)
{
}




//=============================================================================
// Functions for class Histogram1
//=============================================================================


Histogram1::Histogram1(TH1F* h1, Variable& VX, Systematic* Syst, Combined_Variable_Formula* Cut)
  :TH1F(*h1)
  ,Histogram(Syst, Cut)
  ,VarX(VX)
{
/**
 * Constructor for the 1-D histogram class.
 */

  SetDirectory(0);

  if ( CachedBins.count( h1->GetName() ) )
    CachedBin = CachedBins[h1->GetName()];
  else
  {
    CachedBins[h1->GetName()] =  std::shared_ptr<CachedBinLocation>( new CachedBinLocation );
    CachedBin = CachedBins[h1->GetName()];
  }


}


void Histogram1::AnalyseEvent(int ev)
{
/**
 * Function should be called during the event loop.
 * Will lookup the fill value, event weight, and cut value
 */
  m_ev = ev;

  Fill( VarX.GetVar(), m_Systematic->GetVar() * m_Weight->GetVar() );
}


Int_t Histogram1::Fill(Double_t x, Double_t w)
{
/**
 * Overload of the TH1F fill function.
 * It is likely that many clones of the same histogram will be filled.
 * Instead of searching for the correct bin every time, it searches for
 * the bin once and caches the result.
 */

   if (fBuffer) return BufferFill(x,w);

   Int_t bin;
   fEntries++;

   // Replace this bit with the Cache Lookup
   // bin =fXaxis.FindBin(x);

   // This is not technicaly thread safe, but I have good feeling about it ;)
   if ( m_ev != CachedBin->ev )
   {
     CachedBin->binx =fXaxis.FindBin(x);
     CachedBin->ev = m_ev;
   }
   bin = CachedBin->binx;

   // Back tp normal ROOT code

   if (bin <0) return -1;
   if (!fSumw2.fN && w != 1.0 && !TestBit(TH1::kIsNotW) )  Sumw2();   // must be called before AddBinContent
   if (fSumw2.fN)  fSumw2.fArray[bin] += w*w;
   AddBinContent(bin, w);
   if (bin == 0 || bin > fXaxis.GetNbins()) {
      if (!fgStatOverflows) return -1;
   }
   Double_t z= w;
   fTsumw   += z;
   fTsumw2  += z*z;
   fTsumwx  += z*x;
   fTsumwx2 += z*x*x;
   return bin;
}

//=============================================================================
// Functions for class Histogram2
//=============================================================================



Histogram2::Histogram2(TH2F* h2, Variable& VX, Variable& VY, Systematic* Syst, Combined_Variable_Formula* Cut)
    :TH2F( *h2)
    ,Histogram(Syst, Cut)
    ,VarX(VX)
    ,VarY(VY)
{
/**
 * Constructor for the 2-D histogram class.
 */


    SetDirectory(0);


    if ( CachedBins.count( h2->GetName() ) )
      CachedBin = CachedBins[h2->GetName()];
    else
    {
      CachedBins[h2->GetName()] =  std::shared_ptr<CachedBinLocation>( new CachedBinLocation );
      CachedBin = CachedBins[h2->GetName()];
    }

}

void Histogram2::AnalyseEvent(int ev)
{
/**
 * Function should be called during the event loop.
 * Will lookup the X and Y fill value, event weight, and cut value
 */

    m_ev = ev;
    Fill( VarX.GetVar(), VarY.GetVar(), m_Systematic->GetVar() * m_Weight->GetVar() );

}

Int_t Histogram2::Fill(Double_t x, Double_t y, Double_t w)
{
/**
 * Overload of the TH2F fill function.
 * It is likely that many clones of the same histogram will be filled.
 * Instead of searching for the correct bin every time, it searches for
 * the bin once and caches the result.
 */

   if (fBuffer) return BufferFill(x,y,w);

   Int_t binx, biny, bin;
   fEntries++;

   // Replace this bit with the Cache Lookup

//   binx = fXaxis.FindBin(x);
//   biny = fYaxis.FindBin(y);

   // This is not technicaly thread safe, but I have good feeling about it ;)

   if ( m_ev != CachedBin->ev )
   {
     CachedBin->binx = fXaxis.FindBin(x);
     CachedBin->biny = fYaxis.FindBin(y);
     CachedBin->ev = m_ev;
   }

   binx = CachedBin->binx;
   biny = CachedBin->biny;

   // Back tp normal ROOT code



   if (binx <0 || biny <0) return -1;
   bin  = biny*(fXaxis.GetNbins()+2) + binx;
   if (!fSumw2.fN && w != 1.0 && !TestBit(TH1::kIsNotW))  Sumw2();   // must be called before AddBinContent
   if (fSumw2.fN) fSumw2.fArray[bin] += w*w;
   AddBinContent(bin,w);
   if (binx == 0 || binx > fXaxis.GetNbins()) {
      if (!fgStatOverflows) return -1;
   }
   if (biny == 0 || biny > fYaxis.GetNbins()) {
      if (!fgStatOverflows) return -1;
   }
   Double_t z= w;
   fTsumw   += z;
   fTsumw2  += z*z;
   fTsumwx  += z*x;
   fTsumwx2 += z*x*x;
   fTsumwy  += z*y;
   fTsumwy2 += z*y*y;
   fTsumwxy += z*x*y;
   return bin;
}

//=============================================================================
// Functions for class Histogram3
//=============================================================================



Histogram3::Histogram3(TH3F* h3, Variable& VX, Variable& VY, Variable& VZ, Systematic* Syst, Combined_Variable_Formula* Cut)
  :TH3F( *h3 )
  ,Histogram(Syst, Cut)
  ,VarX(VX)
  ,VarY(VY)
  ,VarZ(VZ)
{
/**
 * Constructor for the 3-D histogram class.
 */

  SetDirectory(0);


  if ( CachedBins.count( h3->GetName() ) )
    CachedBin = CachedBins[h3->GetName()];
  else
  {
    CachedBins[h3->GetName()] =  std::shared_ptr<CachedBinLocation>( new CachedBinLocation );
    CachedBin = CachedBins[h3->GetName()];
  }

}


void Histogram3::AnalyseEvent(int ev)
{
/**
 * Function should be called during the event loop.
 * Will lookup the X, Y and Z fill value, event weight, and cut value
 */

  m_ev = ev;

  Fill( VarX.GetVar(), VarY.GetVar(), VarZ.GetVar(), m_Systematic->GetVar() * m_Weight->GetVar() );

}

Int_t Histogram3::Fill(Double_t x, Double_t y, Double_t z, Double_t w)
{
/**
 * Function should be called during the event loop.
 * Will lookup the X and Y fill value, event weight, and cut value
 */

   if (fBuffer) return BufferFill(x,y,z,w);

   Int_t binx, biny, binz, bin;
   fEntries++;

   // Replace this bit with the Cache Lookup

   //binx = fXaxis.FindBin(x);
   //biny = fYaxis.FindBin(y);
   //binz = fZaxis.FindBin(z);


   // bin =fXaxis.FindBin(x);

   // This is not technicaly thread safe, but I have good feeling about it ;)
   if ( m_ev != CachedBin->ev )
   {
     CachedBin->binx = fXaxis.FindBin(x);
     CachedBin->biny = fYaxis.FindBin(y);
     CachedBin->binz = fZaxis.FindBin(z);
     CachedBin->ev = m_ev;
   }

   binx = CachedBin->binx;
   biny = CachedBin->biny;
   binz = CachedBin->binz;

   // Back tp normal ROOT code

   if (binx <0 || biny <0 || binz<0) return -1;
   bin  =  binx + (fXaxis.GetNbins()+2)*(biny + (fYaxis.GetNbins()+2)*binz);
   if (!fSumw2.fN && w != 1.0 && !TestBit(TH1::kIsNotW))  Sumw2();   // must be called before AddBinContent
   if (fSumw2.fN) fSumw2.fArray[bin] += w*w;
   AddBinContent(bin,w);
   if (binx == 0 || binx > fXaxis.GetNbins()) {
      if (!fgStatOverflows) return -1;
   }
   if (biny == 0 || biny > fYaxis.GetNbins()) {
      if (!fgStatOverflows) return -1;
   }
   if (binz == 0 || binz > fZaxis.GetNbins()) {
      if (!fgStatOverflows) return -1;
   }
   fTsumw   += w;
   fTsumw2  += w*w;
   fTsumwx  += w*x;
   fTsumwx2 += w*x*x;
   fTsumwy  += w*y;
   fTsumwy2 += w*y*y;
   fTsumwxy += w*x*y;
   fTsumwz  += w*z;
   fTsumwz2 += w*z*z;
   fTsumwxz += w*x*z;
   fTsumwyz += w*y*z;
   return bin;
}


//=============================================================================
// Functions for class HistogramToy
//=============================================================================


HistogramToy::HistogramToy(Systematic* Syst, Combined_Variable_Formula* Cut)
  :TH1F("Systematic_Toys", "Toy Yields", Syst->GetnToys()+1, 0, Syst->GetnToys()+1)
  ,Histogram(Syst, Cut)
{
/**
 * Constructor for the toy store.
 * The 1st bin is the true yield.
 * The remaining bins are random toys from variations due to the event weights
 */

  SetDirectory(0);
}

void HistogramToy::AnalyseEvent(int)
{
/**
 * Function should be called during the event loop.
 * Will get the results of the toys used for the weights
 * and store them in the correct bins.
 */

  const vector<double>& Weights = m_Systematic->GetWeights();

  for( unsigned int bin = 1; bin <= Weights.size()+1; bin++)
  {
      AddBinContent( bin, Weights[bin-1] * m_Weight->GetVar() );
  }

}

