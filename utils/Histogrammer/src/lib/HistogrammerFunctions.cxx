/** \file
 * HistogrammerFunctions.cxx
 *
 *  Created on: 28 May 2017
 *      Author: ismith
 */

#include "HistogrammerFunctions.h"

#include <iostream>

#include "TMath.h"
using namespace TMath;


double Functions_Generic::MagnetPolarity(std::map<std::string, Variable*>& Vars, void* filename)
{
/**
 * Deduce the Magnet Polarity from the name of the file being processed
 */
  std::string fname = *(std::string*)filename;

  return (fname.find("Up") < fname.size())*2-1;

}



double Functions_Generic::Part_ETA(std::map<std::string, Variable*>& Vars, void* ParticleName)
{
/**
 * Calculate the pseudorapidity of a particle
 */

  std::string& PartName = *((std::string*)ParticleName);

  double X_PZ = Vars[PartName + "_PZ"]->GetVar();
  double X_P  = Vars[PartName + "_P"]->GetVar();

  return ATanH(X_PZ / X_P);
}





double Functions_KMuNu::Bs_PV_Res(std::map<std::string, Variable*>& Vars, void*)
{
/**
 * Calculate the resolution of the B primary vertex
 */

  double True_Z_PV = Vars["Bs_TRUEORIGINVERTEX_Z"]->GetVar();
  double Reco_Z_PV = Vars["Bs_OWNPV_Z"]->GetVar();

  return True_Z_PV - Reco_Z_PV;
}


double Functions_KMuNu::MCTrueQ2(std::map<std::string, Variable*>& Vars, void*)
{
/**
 * Calculate the True \f$q^2\f$ of the \f$B_{s} \rightarrow K^{-} \mu^{+} \nu_{\mu}\f$ decay.
 * True \f$q^2\f$ is calcualted by subtracting the Kaon from the B
 */


  double K_PX = Vars["Kminus_TRUEP_X"]->GetVar();
  double K_PY = Vars["Kminus_TRUEP_Y"]->GetVar();
  double K_PZ = Vars["Kminus_TRUEP_Z"]->GetVar();
  double K_PE = Vars["Kminus_TRUEP_E"]->GetVar();

  double Bs_PX = Vars["B_s0_TRUEP_X"]->GetVar();
  double Bs_PY = Vars["B_s0_TRUEP_Y"]->GetVar();
  double Bs_PZ = Vars["B_s0_TRUEP_Z"]->GetVar();
  double Bs_PE = Vars["B_s0_TRUEP_E"]->GetVar();

  double Q_PX = Bs_PX - K_PX;
  double Q_PY = Bs_PY - K_PY;
  double Q_PZ = Bs_PZ - K_PZ;
  double Q_PE = Bs_PE - K_PE;

  return Power(Q_PE, 2.0) - Power(Q_PX, 2.0) - Power(Q_PY, 2.0) - Power(Q_PZ, 2.0);


}








double Functions_DsMuNu::DsStar_M(std::map<std::string, Variable*>& Vars, void*)
{

/**
 * Calculate the \f$D_s^*\f$ mass using information from neutral isolation
 */
  double Ds_PX = Vars["Ds_PX"]->GetVar();
  double Ds_PY = Vars["Ds_PY"]->GetVar();
  double Ds_PZ = Vars["Ds_PZ"]->GetVar();
  double Ds_PE = Vars["Ds_PE"]->GetVar();

  double G_PX = Vars["Gamma0_PX"]->GetVar();
  double G_PY = Vars["Gamma0_PY"]->GetVar();
  double G_PZ = Vars["Gamma0_PZ"]->GetVar();
  double G_PE = Sqrt(G_PX*G_PX + G_PY*G_PY + G_PZ*G_PZ);

  double Dss_PX = Ds_PX + G_PX;
  double Dss_PY = Ds_PY + G_PY;
  double Dss_PZ = Ds_PZ + G_PZ;
  double Dss_PE = Ds_PE + G_PE;

  return Sqrt( Power(Dss_PE, 2.0) - Power(Dss_PX, 2.0) - Power(Dss_PY, 2.0) - Power(Dss_PZ, 2.0) );

}


double Functions_DsMuNu::DsStar_DM(std::map<std::string, Variable*>& Vars, void*)
{
/**
 * Calculate the \f$D_s^* - D_s\f$ mass difference using information from neutral isolation
 */

  double Ds_MM = Vars["Ds_MM"]->GetVar();

  double Ds_PX = Vars["Ds_PX"]->GetVar();
  double Ds_PY = Vars["Ds_PY"]->GetVar();
  double Ds_PZ = Vars["Ds_PZ"]->GetVar();
  double Ds_PE = Vars["Ds_PE"]->GetVar();

  double G_PX = Vars["Gamma0_PX"]->GetVar();
  double G_PY = Vars["Gamma0_PY"]->GetVar();
  double G_PZ = Vars["Gamma0_PZ"]->GetVar();
  double G_PE = Sqrt(G_PX*G_PX + G_PY*G_PY + G_PZ*G_PZ);

  double Dss_PX = Ds_PX + G_PX;
  double Dss_PY = Ds_PY + G_PY;
  double Dss_PZ = Ds_PZ + G_PZ;
  double Dss_PE = Ds_PE + G_PE;

  return Sqrt( Power(Dss_PE, 2.0) - Power(Dss_PX, 2.0) - Power(Dss_PY, 2.0) - Power(Dss_PZ, 2.0) ) - Ds_MM;

}



double Functions_DsMuNu::TrueQ2(std::map<std::string, Variable*>& Vars, void*)
{
/**
 * Calculate the True \f$q^2\f$ of the \f$B_{s} \rightarrow D_s^+ \mu^{+} \nu_{\mu}\f$ decay.
 * True \f$q^2\f$ is calculated by subtracting the Ds from the B
 */


  double Ds_PX = Vars["Ds_TRUEP_X"]->GetVar();
  double Ds_PY = Vars["Ds_TRUEP_Y"]->GetVar();
  double Ds_PZ = Vars["Ds_TRUEP_Z"]->GetVar();
  double Ds_PE = Vars["Ds_TRUEP_E"]->GetVar();

  double Bs_PX = Vars["Bs_TRUEP_X"]->GetVar();
  double Bs_PY = Vars["Bs_TRUEP_Y"]->GetVar();
  double Bs_PZ = Vars["Bs_TRUEP_Z"]->GetVar();
  double Bs_PE = Vars["Bs_TRUEP_E"]->GetVar();

  double Q_PX = Bs_PX - Ds_PX;
  double Q_PY = Bs_PY - Ds_PY;
  double Q_PZ = Bs_PZ - Ds_PZ;
  double Q_PE = Bs_PE - Ds_PE;

  return Power(Q_PE, 2.0) - Power(Q_PX, 2.0) - Power(Q_PY, 2.0) - Power(Q_PZ, 2.0);


}
double Functions_DsMuNu::MCTrueQ2(std::map<std::string, Variable*>& Vars, void*)
{
/**
 * Calculate the True \f$q^2\f$ of the \f$B_{s} \rightarrow D_s^+ \mu^{+} \nu_{\mu}\f$ decay.
 * True \f$q^2\f$ is calculated by subtracting the Ds from the B
 */

  double Ds_PX = Vars["D_sminus_TRUEP_X"]->GetVar();
  double Ds_PY = Vars["D_sminus_TRUEP_Y"]->GetVar();
  double Ds_PZ = Vars["D_sminus_TRUEP_Z"]->GetVar();
  double Ds_PE = Vars["D_sminus_TRUEP_E"]->GetVar();

  double Bs_PX = Vars["B_s0_TRUEP_X"]->GetVar();
  double Bs_PY = Vars["B_s0_TRUEP_Y"]->GetVar();
  double Bs_PZ = Vars["B_s0_TRUEP_Z"]->GetVar();
  double Bs_PE = Vars["B_s0_TRUEP_E"]->GetVar();

  double Q_PX = Bs_PX - Ds_PX;
  double Q_PY = Bs_PY - Ds_PY;
  double Q_PZ = Bs_PZ - Ds_PZ;
  double Q_PE = Bs_PE - Ds_PE;

  return Power(Q_PE, 2.0) - Power(Q_PX, 2.0) - Power(Q_PY, 2.0) - Power(Q_PZ, 2.0);


}
