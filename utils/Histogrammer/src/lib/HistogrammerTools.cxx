/** \file
 * HistogrammerTools.cxx
 *
 *  Created on: 18 May 2017
 *      Author: ismith
 */
using namespace std;

#include "HistogrammerTools.h"

#include <iostream>
#include <TRandom.h>

namespace HistogrammerTools {



long GetTimens()
{
/**
 * This function returns the number of nanoseconds after the current second.
 * This is useful as a seed for random number generators.
 */

  timespec ts;

  clock_gettime(CLOCK_MONOTONIC, &ts);

  return ts.tv_nsec;

}


double GetMean( std::vector<double> numbers )
{
/**
 * Given a vector of numbers, return the average.
 * This should probably be replaced by an stl implementation
 */

  double mean = 0;

  for ( auto num: numbers )
    mean += num;
  mean /= numbers.size();


  return mean;
}

double GetSD( std::vector<double> numbers )
{
/**
 * Given a vector of numbers, return the standard deviation.
 * This should probably be replaced by an stl implementation
 */

  double mean = GetMean(numbers);

  double SD = 0;

  for ( auto num: numbers )
    SD += pow( num - mean, 2 );
  SD /= numbers.size();

  return sqrt(SD);


}





std::vector<std::string> WildCards( std::string basename)
{
/**
 * Modified from a TChain function: https://root.cern.ch/doc/master/TChain_8cxx_source.html#l00336
 *
 * Will parse a filename and return a vector of filenames matching the wildcard.
 * This will work over xrootd!
 */


  TString TS_basename = TString( basename );

  std::vector<std::string> filenames;
  TString treename, query, suffix;


  Int_t slashpos =TS_basename.Last('/');
  TString directory;
  if (slashpos>=0) {
    directory = TS_basename(0,slashpos); // Copy the directory name
    TS_basename.Remove(0,slashpos+1);      // and remove it from basename
  } else {
    directory = gSystem->UnixPathName(gSystem->WorkingDirectory());
  }

  const char *file;
  const char *epath = gSystem->ExpandPathName(directory.Data());
  void *dir = gSystem->OpenDirectory(epath);
  delete [] epath;
  if (dir) {
    //create a TList to store the file names (not yet sorted)
    TList l;
    TRegexp re(TS_basename,kTRUE);
    while ((file = gSystem->GetDirEntry(dir))) {
     if (!strcmp(file,".") || !strcmp(file,"..")) continue;
     TString s = file;
     if ( (TS_basename!=file) && s.Index(re) == kNPOS) continue;
     l.Add(new TObjString(file));
    }
    gSystem->FreeDirectory(dir);
    //sort the files in alphanumeric order
    l.Sort();
    TIter next(&l);
    TObjString *obj;
    while ((obj = (TObjString*)next())) {
     file = obj->GetName();

     filenames.push_back(std::string(directory.Data()) + "/" + std::string(file) );

    }
    l.Delete();
  }
  return filenames;

}




double FindBinContent(TH1* hist, OverflowTreatment overflowmethod, double Value1, double Value2, double Value3)
{
/**
 * Given a histogram and a series of values corresponding to points on the axes,
 * return the contents of the bin at that point.
 * Adds functionality to the TH1* method by requiring a specification of overflow tratment.
 */

  int BinNumber = hist->FindBin(Value1, Value2, Value3);

  if ( hist->IsBinOverflow(BinNumber) or hist->IsBinUnderflow(BinNumber) ){
    // Do some stuff with the bin number to handle the overflow bins

    if ( overflowmethod == OverflowTreatment::Unity)
      return 1;
    if ( overflowmethod == OverflowTreatment::Zero)
      return 0;
    if ( overflowmethod == OverflowTreatment::Closest){
      //ToDo
      return 1;
      //Some clever maths to get the content of the closest bin
    }


  }

  return hist->GetBinContent(BinNumber);



}

TH1* CloneToyHist( TH1* H, long seed )
{
/**
 * Return a toy clone of a histogram.
 * Every bin will be replaced by a random number from a Gaussian
 * distribution with mean equal to bin contents, and width equal to bin error.
 */

  if (seed == 0 )
    seed = GetTimens();
  TRandom RND( seed );


  TH1* h_Out = (TH1*)H->Clone();

  h_Out->SetDirectory(0);

  int Nbins_X = h_Out->GetNbinsX();
  int Nbins_Y = h_Out->GetNbinsY();
  int Nbins_Z = h_Out->GetNbinsZ();

  for ( int X = 1; X<=Nbins_X; X++ ){
    for ( int Y = 1; Y<=Nbins_Y; Y++ ){
      for ( int Z = 1; Z<=Nbins_Z; Z++ ){

        double val_bin = h_Out->GetBinContent(X, Y, Z);
        double err_bin = h_Out->GetBinError(X, Y, Z);

        double fractional_error = err_bin/val_bin;

        double shift = -1000;
        do
          shift = RND.Gaus(1, fractional_error);
        while
           (shift < 0) ;

        h_Out->SetBinContent(X, Y, Z, val_bin*shift);

        //std::cout << val_bin << "  " << err_bin << "  " << h->GetBinContent(X, Y, Z) << std::endl;

      }
    }
  }
  return h_Out;

}




void SaveObject(TObject* object, TFile* f_out, std::string folder, std::string openhow)
{
/**
 * Save an object to a ROOT file.
 * A check will be performed to determine if an object with that name already exists.
 * If it does it will be deleted and replaced with the new object.
 */
  std::string histname = object->GetName();


  if ( not f_out->Get( folder.c_str() ) )          // Check to make sure the folder exists
    f_out->mkdir( folder.c_str() );

  f_out->cd( folder.c_str() );

  if ( gDirectory->Get( histname.c_str() ) )             // If the histogram exists within the folder
    gDirectory->Delete( (histname + ";1").c_str() );   // Delete the histogram from the file


  object->Write();

}



void SaveObject(TObject* object, std::string filename, std::string folder, std::string openhow)
{
/**
 * Helper overload of SaveObject to aid functionality
 */

  TFile* f_out = TFile::Open(filename.c_str(), openhow.c_str());
  SaveObject( object, f_out, folder, openhow);
  f_out->Close();
  delete f_out;
}

void SaveHist(TH1* hist, TFile* f_out, std::string folder, std::string openhow, bool Normalise)
{
/**
 * Save a histogram to a ROOT file using the SaveObject() method with some additional features.
 */
  SaveObject( hist, f_out, folder, openhow);

  if (not Normalise)
    return;

  TH1* h_normalised = (TH1*)hist->Clone();
  h_normalised->Scale(1.0 / h_normalised->Integral() );

  std::string histname = h_normalised->GetName();
  histname += "_norm";

  h_normalised->SetName(histname.c_str());


  SaveObject( h_normalised, f_out, folder, openhow);


}


void SaveHist(TH1* hist, std::string filename, std::string folder, std::string openhow, bool Normalise)
{
/**
 * Overload to add functionality to the SaveHist method.
 */

  TFile* f_out = TFile::Open(filename.c_str(), openhow.c_str());
  SaveHist( hist, f_out, folder, openhow, Normalise);
  f_out->Close();


}









}
