 #!/usr/bin/env python
import ROOT
import os

from ROOT import *
from math import *

__all__ = ["GenerateTemplate"]


#Helpful functions
###########################################################################
def ReadLumi(Histograms, modename):
    mylumi   = float( Histograms.Get("Luminosity/KMuNu/{}"    .format(modename)).GetTitle() )
    mylumi_e = float( Histograms.Get("Luminosity/KMuNu/{}_Err".format(modename)).GetTitle() )
    
    
    print "Total Luminosity : " , round(mylumi/1000,4) , " +/- " , round(mylumi_e/1000,4) , " fb-1"
    return mylumi/1000
###########################################################################
###########################################################################




def getPhysTemplates(Histograms, Cut, PlotHistogram, EfficiencyHistogram, SystematicName ):
    
    NoBDT = "" if Cut[-5:] == "NoBDT" else "_NoBDT"
    
    # ===========================================================================
    # 1-  How many B+->Jpsi we have in the Sample ?
    #os.mkdir(os.getcwd() +'/'+"CutName" )
    gSystem.mkdir(os.getcwd() +'/'+"CutName", 1 )
    InvMass_h_name = "InvMass_h"
    print "Nominal/KMuNu/SameSign/{Cut}/h_Bu_MM".format(Cut=Cut+NoBDT) 
    Bu_M  = Histograms.Get("Nominal/KMuNu/SameSign/{Cut}/h_Bu_MM".format(Cut=Cut+NoBDT) )#TH1D("Bu_M" , "Bu_M" , 100 , 5100 , 5500);
    c_BplusMass_data  = TCanvas("B+ Mass (WS data,"+ "CutName"+")","B+Mass (WS data"+"CutName"+")");
    #chain_SSdata.Project( "Bu_M" , InvMass_str , str(Cuts) , "" , 10000000000 , 0 ) ;
    print "Nominal/KMuNu/SameSign/{Cut}/h_Bu_MM".format(Cut=Cut+ ("" if (Cut[-5:] == "NoBDT") else "_NoBDT"))
    Bu_M.Draw();
    #c_BplusMass_data.Print(os.getcwd() +'/'+"CutName"+"/Bu_mass_WSdata_"+"CutName"+".root")
    #c_BplusMass_data.Print(os.getcwd() +'/'+"CutName"+"/Bu_mass_WSdata_"+"CutName"+".pdf")
    # get the fit from Splot_MassBu.py
    from Splot_MassBu import fitBuMass
    n_Bu2JpsiK_in_SS_smaple , e_n_Bu2JpsiK_in_SS_smaple = fitBuMass(Bu_M)
    print "nb of Bu2JpsiK in SS smaple",  n_Bu2JpsiK_in_SS_smaple , " +/- " , e_n_Bu2JpsiK_in_SS_smaple
    
    
    # ===========================================================================
    
    # ===========================================================================
    # Now I need to subtract physics background from SS sample, to do that I need:
    # 1- Yields of the Phsyics backgrounds :
    from B2Xmun_BKG_BRs import Give_bphysics_Yields
    
    bu_yields, e_bu_yields , Bs_MCORR_Bu2D0mu , bd_yields , e_bd_yields, Bs_MCORR_Bd2Dstmu = Give_bphysics_Yields(
        n_Bu2JpsiK_in_SS_smaple,e_n_Bu2JpsiK_in_SS_smaple,Histograms, PlotHistogram, EfficiencyHistogram, Cut, SystematicName )
    
    print "Expected Bu->D0munu Yields in the sample : ", bu_yields , " +/-" , e_bu_yields
    print "Expected Bd->D*munu Yields in the sample : ", bd_yields , " +/-" , e_bd_yields
    print "Nb of Bu2DmunuX events are : " ,Bs_MCORR_Bu2D0mu.Integral()
    print "Nb of Bd2DmunuX events are : " ,Bs_MCORR_Bd2Dstmu.Integral()
    
    B2DmunuX = Bs_MCORR_Bu2D0mu.Clone("B2DmunuX")
    B2DmunuX.Add(Bs_MCORR_Bd2Dstmu ,1 )
    print "Nb of B2DmunuX events are : " , B2DmunuX.Integral()
    
    #c_MCORR_MCBu2D0mu = TCanvas("MCORR Bu2D0mu(WS,"+"CutName"+")","MCORR Bu2D0mu(WS,"+"CutName"+")")
    #c_MCORR_MCBu2D0mu.cd()
    B2DmunuX.SetName('B2DmunuX');B2DmunuX.SetTitle('B2DmunuX')
    B2DmunuX.SetLineColor(kBlue);B2DmunuX.SetLineWidth(2)
    B2DmunuX.SetStats(0)
    #B2DmunuX.Draw()
    #Bs_MCORR_Bu2D0mu.Draw("same")
    #Bs_MCORR_Bd2Dstmu.Draw("same")
    #c_MCORR_MCBu2D0mu.BuildLegend(0.7 , 0.75, 1 , 1)
    #c_MCORR_MCBu2D0mu.Modified() 
    #c_MCORR_MCBu2D0mu.Update() 

    #c_MCORR_MCBu2D0mu.Print( os.getcwd() +'/'+"CutName"+"/B2DmunuX_MC_" +"CutName"+".root")
    #c_MCORR_MCBu2D0mu.SaveAs(os.getcwd() +'/'+"CutName"+"/B2DmunuX_MC_"+"CutName"+".pdf")
    
    return B2DmunuX


def GenerateTemplate(Histograms, Cut, PlotHistogram, EfficiencyHistogram, SystematicName):
    
    B2DmunuX_FQ2 = getPhysTemplates( Histograms, Cut, PlotHistogram, EfficiencyHistogram, SystematicName ) # Full Q2

    # Let us look at RAW SS data distribution and save them to a file:  
    Bs_MCORR_SSdata_FQ2 = Histograms.Get("Nominal/KMuNu/SameSign/{Cut}/{Name}".format(Cut=Cut, Name=PlotHistogram))
    Bs_MCORR_SSdata_FQ2.SetStats(0)
    
    SS_physicssubtracted_FQ2= Bs_MCORR_SSdata_FQ2.Clone("SS_physicssubtracted_FQ2")
    SS_physicssubtracted_FQ2.Add( B2DmunuX_FQ2 , -1)
    
    
    return SS_physicssubtracted_FQ2
    
    
# ===========================================================================


if __name__ == "__main__":
    
    #Histograms = ROOT.TFile("/eos/lhcb/wg/semileptonic/Bs2KmunuAna/Histogrammer/Output/Histograms.root", "READ")
    Histograms = ROOT.TFile("../Histograms.root", "READ")
    
    
    ReadLumi(Histograms, "Data")

    Cut                 = "Is_Q2"
    PlotHistogram       = "h_Bs_MCORR"
    EfficiencyHistogram = "h_Bs_MCORR"
    SystematicName      = "Nominal"


    B2DmunuX_FQ2 = GenerateTemplate( Histograms, Cut, PlotHistogram, EfficiencyHistogram, SystematicName ) # Full Q2
    
    # Let us look at RAW SS data distribution and save them to a file:  
    Bs_MCORR_SSdata_FQ2 = Histograms.Get("Nominal/KMuNu/SameSign/{Cut}/{Name}".format(Cut=Cut, Name=PlotHistogram))
    Bs_MCORR_SSdata_FQ2.SetStats(0)
    
    c_MCORR_SSdata_bPhysicsSubtracted  = TCanvas()
    SS_physicssubtracted_FQ2= Bs_MCORR_SSdata_FQ2.Clone("SS_physicssubtracted_FQ2")
    SS_physicssubtracted_FQ2.Add( B2DmunuX_FQ2 , -1)
    SS_physicssubtracted_FQ2.SetLineColor(kOrange);SS_physicssubtracted_FQ2.SetLineWidth(2);SS_physicssubtracted_FQ2.Draw("same")
    
    
    print "Nb of RAW SS events in FQ2: " , Bs_MCORR_SSdata_FQ2.Integral()
    
    print "Nb of SS sample after Subtracting B2DmunuX events in FQ2 region: " , SS_physicssubtracted_FQ2.Integral()
    
    Results_file = TFile("BsMCORR_SS_bphysicssubtracted_histos.root" ,"RECREATE")
    Bs_MCORR_SSdata_FQ2.Write()
    SS_physicssubtracted_FQ2.Write()
    Results_file.Close()
