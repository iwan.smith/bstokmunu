import ROOT
from ROOT import *
import os 
import math
nbcpu=23

###########################################################################
def prepareAndDrawPads( mypad1 , mypad2 ):
    mypad1.SetBorderMode(0)
    mypad1.SetBottomMargin( 0.1 )
    mypad2.SetBottomMargin( 0.30  )
    mypad2.SetTopMargin( 0.1 )
    mypad2.SetBorderMode(0)
    mypad1.Draw()
    mypad2.Draw()
    return 
###########################################################################


###########################################################################
def MakeResidual(  mypdf , mydata  , frame, t):
    PlusTwosigma = TLine(t.getMin() , 2 ,t.getMax() , 2 ) 
    zero_line    = TLine(t.getMin() , 0 ,t.getMax() , 0 ) 
    MinusTwosigma= TLine(t.getMin() ,-2 ,t.getMax() ,-2 ) 
    zero_line.SetLineColor(kBlack) 
    PlusTwosigma.SetLineColor(kRed) 
    MinusTwosigma.SetLineColor(kRed) 
    hpull1 = frame.pullHist( mydata ,mypdf, False)  
    hpull1.SetMarkerSize(0.8) 
    frame_pull  = t.frame() 
    frame_pull.addObject(PlusTwosigma) 
    frame_pull.addObject(MinusTwosigma) 
    frame_pull.addObject(zero_line) 
    frame_pull.addObject(hpull1,"P" )  
    frame_pull.SetAxisRange(-5 , 5,  "Y") 
    frame_pull.GetXaxis().SetTitle("") 
    frame_pull.GetYaxis().SetTitle("") 
    frame_pull.GetXaxis().SetLabelSize(0.1) 
    frame_pull.GetYaxis().SetLabelSize(0.1) 
    frame_pull.GetYaxis().SetNdivisions(504) 
    frame_pull.SetTitle("") 
    return frame_pull 
###########################################################################


###########################################################################
# Fit data :
# =======================================================================================
# TFile *dataOS_BplusM_file = TFile("./Bplus_indata_f.root") 
#eos_dir  =  "/eos/lhcb/wg/semileptonic/Bs2KmunuAna/Tuples/Bs2KMuNu_DATATUPLES_TRIMMED_19June16/";
#SS_data  =  eos_dir_DATATuple + "Merged_DTT_2012_Reco14Strip21r0p1a_*_SEMILEPTONIC.root";
## Cuts 
#MyCuts       = "TMVA_charge_BDT > 0.107 && TMVA_SS_aftercut_BDT> 0.1 && "
##
#InvMass_h_name = "InvMass_h" 
#InvMass_str    = "sqrt( Bs_M*Bs_M + 105.658*105.658 + 2*( Bs_PE*muon_p_NIsoTr_PE - ( Bs_PX*muon_p_NIsoTr_PX  + Bs_PY*muon_p_NIsoTr_PY  +  Bs_PZ*muon_p_NIsoTr_PZ  ) ) )"
#
#SSdata_2012_tree = TChain("data_SS","data_SS");
#SSdata_2012_tree.Add( SS_data);
#
#Bu_M  = TH1D("Bu_M" , "Bu_M" ,100 , 5100 , 5500);
#SSdata_2012_tree.Project( "Bu_M" , InvMass_str , MyCuts ,"" , 10000000000 , 0 ) ;
#
#Bu_MCORR  = TH1D("Bu_MCORR" , "Bu_MCORR" , 100 , 3000 , 7000);
#SSdata_2012_tree.Project( "Bu_MCORR" , "Bs_MCORR" , MyCuts ,"" , 10000000000 , 0 ) ;
#
#c_BplusMass_data  = TCanvas("c_BplusMass_data","c_BplusMass_data");
#Bu_M.Draw();
#
gROOT.SetStyle("Plain") 
gStyle.SetTitle("") 
Saving_dir = '../outputs/'


import KMuNuTools

#observables:
def fitBuMass( Bu_M ):
    Yield = KMuNuTools.FitJpsiK(Bu_M, "")
    return Yield.first, Yield.second
"""
    # Notice that the TCut argument here is just to add a suffix to the names of output plots/fitresults/etc ...
    B_M        = RooRealVar("Bu_MASS","Bu_MASS", 5100 , 5500, "MeV/c^{2}" ) 
    #*************************************
    #Yield variables
    #*************************************
    n_sig   = RooRealVar("N_{BuJpsiK^{+}}" ,"N_{BuJpsiK^{+}}" , 60 , 10 , 600000 ) 
    n_part  = RooRealVar("n_{Part.}" ,"n_{Part.}"    , 600 , 10 , 600000 ) 
    n_comb  = RooRealVar("n_{Comb}","n_{Comb}", 600,  10 , 60000 ) 

    Bumean    = RooRealVar("\\mu_{Bu}", "Mean Bu mass", 5275 , 5260 , 5300 ,"MeV/c^{2}")   
    Busigma1  = RooRealVar("\\sigma^{1}_{Bu}", "Bu mass resolution 1",17,0,30.) 
    Busigma2  = RooRealVar("\\sigma^{2}_{Bu}","Busigma2", 10, 0 , 40.)
    Busigma2R = RooRealVar("#sigma^{2}_{Bu}","Bu sigma2/sigma1", 0.6,0.001,40) 

    frac_core_Bu = RooRealVar("f^{core}_{Bu}","fraction of core Gaussian", 0.5, 0.,1.) 
    B_M_pdf_sig1 = RooGaussian("Bu_mass_pdf_sig1", "Bu signal mass pdf core" , B_M, Bumean, Busigma1) 
    B_M_pdf_sig2 = RooGaussian("Bu_mass_pdf_sig2", "Bu signal mass pdf tail" , B_M, Bumean, Busigma2) 

    cbn          = RooRealVar("n_{CB, Bu}","cbn", 2. , 0. , 10.) 
    CBsigma      = RooRealVar("\\sigma^{1}_{CB,Bu}", "CB Bu mass resolution 1",10,1,40.) 
    cbalpha      = RooRealVar("\\alpha_{CB, Bu}","cbalpha", 2., 0.1, 30)  #cbn.setVal(0.000001)  cbn.setConstant(kTRUE) 
    frac_cball   = RooRealVar("f_{CB, Bu}","frac_cball", 0.2,0.,1.) 
    cball_Bu_Pdf = RooCBShape("cball_Bu_Pdf", "crystal ball_Bu_Pdf", B_M, Bumean , CBsigma , cbalpha, cbn ) 

    B_M_pdf_sig  = RooAddPdf("Bu_mass_pdf_sig","Bu signal", RooArgList(B_M_pdf_sig1 ,
                                                                       #cball_Bu_Pdf ),
                                                                       B_M_pdf_sig2 ), 
                             RooArgList(frac_core_Bu ) ) 
    sig_mass     = RooExtendPdf("sig_mass","extended signal mass PDF",  B_M_pdf_sig  , n_sig)  # ,"signalRegion"

    #partially reconstructed background 
    mean_part    = RooRealVar("mean_part"    , "mean_part",  5222 , 5220 , 5235 ) 
    Sigma_CB     = RooRealVar("Sigma_CB"     , "Sigma_CB" ,  2    ,   1  ,  17  )  
    BKG_pdf_part = RooGaussian("BKG_pdf_part", "BKG_pdf_part", B_M, mean_part, Sigma_CB) 
    mean_CB      = RooRealVar("mean_CB"          ,"mean_CB"  ,  5225 , 5220 , 5235 ) 
    cbsigma_BKG  = RooRealVar("\\sigma_{CB,BKG}" , "cbsigma" ,   5    ,   1  ,  25  ) 
    cbsigmaR_BKG = RooRealVar("CB_{#sigma} BKG"  , "Crystal Ball #sigma/sigma1" , 0.3 , 0.01, 15)

    # partially reconstructed : 
    argpar  = RooRealVar("argpar","argus shape parameter",-1.0 , -5 , 5)  
    cutoff  = RooRealVar("cutoff","argus cutoff",5180.0 , 5160 , 5200.0)  
    argus   = RooArgusBG("argus","Argus PDF", B_M  , cutoff,argpar )  
    PartialPhys_mass = RooExtendPdf("PartialPhys_mass","PartialPhys_mass",  argus  ,n_part) 
    # ,"signalRegion" 
    # Combinatiorial
    a_dplusmass = RooRealVar("a_{Comb,Bu}","a_dplusmass", -0.002, -0.1, 0.1) 
    B_M_pdf_bkgcomb = RooExponential("B_M_pdf_bkgcomb", "background PDF", B_M , a_dplusmass ) 
    bkg_mass_comb= RooExtendPdf("bkg_mass_comb","extended mass comb background PDF",B_M_pdf_bkgcomb,n_comb) #,"signalRegion" 
    #*************
    #SIGNAL+BACKGROUND 
    #*******************************
    pdf_mass = RooAddPdf("pdf_mass","mass and time signal & bkg",
                         RooArgList(bkg_mass_comb   ,
                                    #PartialPhys_mass ,
                                    sig_mass        
                                    ))

    #*********************************************
    #READ DATA FROM FILE    
    #*********************************************
    #data = RooDataHist("data","data", RooArgList(B_M ), RooFit.Import(Bplus_M_h) )
    data = RooDataHist("data","data", RooArgList(B_M ), RooFit.Import( Bu_M ) ) 

    res_mass = RooFitResult() 
    nll_nof  = RooNLLVar("nll_nof", "-log(L)",
                         pdf_mass,
                         data,
                         RooFit.Extended(True), RooFit.Offset(True) , RooFit.NumCPU(nbcpu))  #nll without offset    
    m1 = RooMinuit(nll_nof) 
    m1.optimizeConst(True)
    m1.setVerbose(False)
    m1.setStrategy(1)
    m1.migrad()   #m1.hesse()
    m1.setStrategy(2)
    m1.migrad()
    m1.hesse()
    res_mass = m1.save()
    res_mass.Print("V")

    return n_sig.getVal() , n_sig.getError()  

    #pdf_params = pdf_mass.getParameters(data) 
    #Params_iterator = pdf_params.createIterator() 
    #myParam = RooAbsArg()
    #ofstream output("Results_massfit_MomScale.txt") 
    #output<< "\\begin{table} \n \\begin{center} \n \\begin{tabular}{|c|c|c|} \n \\hline"<<endl 
    #output<<" Parameter &   Fit value  &  Error  \\\\ \n \\hline"<<endl 
    #while( (myParam=(RooAbsArg)Params_iterator.Next()) ){
    #  RooRealVar myRRV = dynamic_cast<RooRealVar>(myParam)  
    #  output<< "$"<<myRRV.GetName() <<"$"
    #        << " & " 
    #        << setprecision(3) << fixed <<myRRV.getVal()   
    #        << " & "
    #        << setprecision(3) << fixed << myRRV.getError() 
    #        << "\\\\ "
    #        << endl 
    #}
    #output<<"\\hline \n \\end{tabular} \n \\end{center} \n \\end{table}"<<endl 
    res_mass.Print("v") 

    Bu_mass_c = TCanvas("B+ Mass (WS data,)","B+ Mass (WS data,)") 
    Bu_mass_c.cd() 
    Bu_mass_frame = B_M.frame(RooFit.Title("B+ Mass (WS data,)")) 
    data.plotOn(Bu_mass_frame , RooFit.Name("dataFIT_massOnly"), RooFit.MarkerSize(0.8))  
    pdf_mass.plotOn(Bu_mass_frame,RooFit.LineColor(kBlue), RooFit.NumCPU(nbcpu), RooFit.Name("pdf_mass")) 
    pdf_mass.plotOn(Bu_mass_frame,RooFit.Components("sig_mass"), RooFit.LineStyle(kDashed),RooFit.LineColor(kGreen+2), RooFit.NumCPU(nbcpu)) 
    pdf_mass.plotOn(Bu_mass_frame,RooFit.Components("bkg_mass_comb") ,RooFit.LineStyle(kDashed),RooFit.LineColor(kRed), RooFit.NumCPU(nbcpu)) 
    chi2 = Bu_mass_frame.chiSquare("pdf_mass","dataFIT_massOnly",3);
    chi2_var = RooRealVar("Chi2" , "#chi^{2}" , chi2 , 0, 50 )
    myparams = RooArgSet( n_sig , chi2_var )
    myparams.Print()
    pdf_mass.paramOn(Bu_mass_frame, RooFit.Parameters( myparams )  , RooFit.Layout(0.6, 0.7, 0.8) ) 
    myparams = pdf_mass.getParameters(data)
    myparams.add(chi2_var)
    myparams.writeToFile(os.getcwd() +'/Bu_WSdata_.txt')
    #pdf_mass.plotOn(Bu_mass_frame,RooFit.Components("PartialPhys_mass") ,RooFit.LineStyle(kDashed),RooFit.LineColor(kBlack) , RooFit.NumCPU(nbcpu)) 
    #Bu_mass_frame.GetXaxis().SetTitle("B_{d} M (3 constraints) [MeV/c^{2}]") 
    pad_B_M_FS     = TPad("pad_B_M_FS","pad_B_M_FS",0 ,0.25 ,1 ,1)  
    pad_B_M_FS_pull= TPad("pad_B_M_FS_pull","pad_B_M_FS_pull",0 ,0,1 ,0.25) 
    prepareAndDrawPads(pad_B_M_FS ,pad_B_M_FS_pull ) 
    frame_BuMass_pull = MakeResidual("pdf_mass","dataFIT_massOnly",Bu_mass_frame,B_M) 
    Bu_mass_frame.getAttText().SetTextSize(0.07) 
    Bu_mass_frame.getAttLine().SetLineColor(0) 
    Bu_mass_frame.getAttFill().SetFillColor(0) 
    pad_B_M_FS.cd() 
    Bu_mass_frame.Draw() 
    pad_B_M_FS_pull.cd()   
    frame_BuMass_pull.Draw() 
    Bu_mass_c.Print(os.getcwd() +"/Bu_massfit_.pdf") 
    Bu_mass_c.Print(os.getcwd() +"/Bu_massfit_.root") 
    #print "finished"
    return n_sig.getVal() , n_sig.getError()  



"""

  
  
  
