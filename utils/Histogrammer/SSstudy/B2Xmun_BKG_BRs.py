from math import *
from ROOT import *
# Calculate background yields in the wrong-sign (same sign) sample.
# Methode is to normalize all physics background to the known B+->Jpsi(mumu)K+ yield

# 1- calculate Bu->JpsiK yield in the same sign sample by making the invariant mass of Kmu+ NonIsolating track (run Splot_MassBu.py)
# this is the normalization you need  

# 2- Use the PDG and assumptions about eff. to get the absolute yields  
# Bs -> Ds+ mu- nu ; Ds->K+ X

# Formulae used : 
#                                                           f_{Bs} x BR(Bs->Ds(*)+mu-nu) x BR(Ds->K- X) x eff(cuts,Dsmunu) x eff(gen, Dsmunu)
# N( Bs -> Ds+ mu- nu ; Ds+->K- X ) = N(B+->Jpsi(mumu)K+) x _________________________________________________________________________________
#                                                            
#                                                           f_{Bu} x BR(B+->J/psiK+) x BR(Jpsi->mumu) x eff(cuts,JpsiK) x eff(gen, Jpsi K)
#
# eff(cuts) means the efficiency of the Reco, stripping, trigger, offlinecuts with the exception of BDT_Charge_TMVA cut
# ingrediants : 
# PDG inputs 
# SL Bs background :

# Physics constants :
f_Bs      = 0.08
f_Bu      = 0.35
f_Bd      = 0.35
f_Lb      = 0.22
# B+ -> Jpsi(mu+mu-) K+
BR_BJpsiK          =  1.026e-3
e_BR_BJpsiK        =  0.031e-3
BR_Jpsi_mumu       =  5.961e-2 
e_BR_Jpsi_mumu     =  0.033e-2
# B+ -> D0 Mu+ nu
BR_D0_Kpi          = 3.89e-2 + 14.2e-2 + 3.97e-3 + 3.37e-3# Adding Kpi, Kpipi0 , KK, KKpi0 BR, eff is assumed to be the same(mostly due to lack of MC)
e_BR_D0_Kpi        = 0.5e-2 

BR_BuDmunu         =  9.22e-2#D0munu(2.27+-0.11), D*0munu(5.69+-0.19) , D(*)npimunu(1.87+-0.26)*(67.7+-0.5)%)          
e_BR_BuDmunu       =  0.34e-2
# Bd -> D*- mu+ nu
BR_BdD0munuX       = 4.9e-2  #[D*munu(4.93+-0.11) + D(*)npimunu(2.3+-0.5)]*D*->D0pi(67.7+-0.5)% 
e_BR_BdD0munuX     = 0.1e-2

import KMuNuTools

from KMuNuTools import PairToTuple
"""
def EffError(a,b):
    print a,b
    r ,  sr = 1 , 1
    if( b!=0):
        r = a/b
        sr = sqrt(abs(r*(1-r)/b))
    return sr
"""                            
def GetEff( Histograms, Cut,  modename, HistName, Systematic ):
    
    
    CorrectionFactor,      CorrectionFactorErr      = PairToTuple(KMuNuTools.GetCalculatedCorrection(Histograms, modename, "Default", "RW_Kin", Cut, KMuNuTools.Decay.KMuNu))
    
    UncorrectedEfficiency, UncorrectedEfficiencyErr = PairToTuple(KMuNuTools.GetFullEfficiency(Histograms, KMuNuTools.Decay.KMuNu, modename, Cut, ""))
    
    print "GetEffFunction Test:", CorrectionFactorErr, CorrectionFactor, UncorrectedEfficiencyErr, UncorrectedEfficiency
    CorrectedEfficiency = UncorrectedEfficiency * CorrectionFactor
    CorrectedError      = CorrectedEfficiency * sqrt( pow(CorrectionFactorErr/CorrectionFactor, 2) + pow(UncorrectedEfficiencyErr/UncorrectedEfficiency, 2))
    
    """
    N_selected_afc       = Histograms.Get("{Systematic}/KMuNu/{Mode}/{Cut}/{HistName}".format(
        Systematic = Systematic,
        Cut=Cut, 
        Mode=modename,
        HistName=HistName)).Integral()#Tuple.GetEntries(str(Cut))

    N_genrated_inLHCbacc = Histograms.Get("EventTuples/KMuNu/{Mode}".format(
        Mode=modename)).GetTitle()
        
        
    print "Total Number of slected events are :" , N_selected_afc
    print "Total Number of generated events are :" , N_genrated_inLHCbacc
    print "Eff. for "+ modename + " mode is :" , float(N_selected_afc)/float(N_genrated_inLHCbacc) 
    
    return float(N_selected_afc)/float(N_genrated_inLHCbacc), EffError(float(N_selected_afc), float(N_genrated_inLHCbacc))
    """
    return CorrectedEfficiency, CorrectedError


def GetCutEff( Histograms, CutN, CutD, modename, HistName, Systematic ):
    
    CutEff, CutEffErr = PairToTuple(KMuNuTools.GetCalculatedEfficiency (Histograms, modename, Systematic, CutN, CutD, KMuNuTools.Decay.KMuNu))

    
    """
    N_selected_N       = Histograms.Get("{Systematic}/KMuNu/{Mode}/{Cut}/{HistName}".format(
        Systematic=Systematic,
        Cut=CutN, 
        Mode=modename,
        HistName=HistName)).Integral()
    
    
    N_selected_D       = Histograms.Get("{Systematic}/KMuNu/{Mode}/{Cut}/{HistName}".format(
        Systematic=Systematic,
        Cut=CutD, 
        Mode=modename,
        HistName=HistName)).Integral()
    
    print "Total events in Numerator :" , N_selected_N
    print "Total events in Denominator :" , N_selected_D
    print "Eff. for "+ modename + " mode is :" , float(N_selected_N)/float(N_selected_D) 
    """
    return CutEff, CutEffErr

  
def Give_bphysics_Yields(N_Bu2JpsiK, e_N_Bu2JpsiK , Histograms, PlotHist, EffHist, HistCut, Systematic ):
    
    NoBDT = "" if (HistCut[-5:] == "NoBDT") else "_NoBDT"
    
    eff_gencut_JpsiK      = 15.81e-2; e_eff_gencut_JpsiK = 0.01e-2
    eff_gencut_D0munu     = 17.98e-2; e_eff_gencut_D0munu = 0.05e-2  #
    eff_gencut_BdD0munuX  = eff_gencut_D0munu; e_eff_gencut_BdD0munuX =e_eff_gencut_D0munu 

    # Get the eff. for B+ -> Jpsi K+  

    # There is a subtility here: the JpisK peak is made using the isolation requiremnt ==>
    # we need to account for the efficiency of such requiremnt using MC B+->JpsiK sample
    # Notice also that the eff. should be accounted for before the BDTs cuts
    #MatchNonIsoTrack_MC = "abs(muon_p_NIsoTr_TRUEID) == 13 && muon_p_NIsoTr_MC_MOTHER_ID==443 && abs(muon_p_NIsoTr_MC_GD_MOTHER_ID)==521 && muon_p_NIsoTr_MC_MOTHER_KEY==muon_p_MC_MOTHER_KEY && kaon_m_MC_MOTHER_KEY==muon_p_NIsoTr_MC_GD_MOTHER_KEY"
    
    
    eff_Iso_BuJpsiK, eff_Iso_BuJpsiK_err = GetCutEff(Histograms, HistCut+NoBDT + "_IsoID", HistCut+ NoBDT, "MC_12143001", EffHist, Systematic);
    print "Isolation eff. for Bu->Jpsi K = " , eff_Iso_BuJpsiK, "+/-", eff_Iso_BuJpsiK_err

    
    # finally the BDTs + Q2 cuts(named Additional_cuts for lack of a better word) should be
    # accounted for as well to get the final yields
    eff_cuts_JpsiK , e_eff_cuts_JpsiK = GetEff(Histograms, HistCut+NoBDT + "_IsoID", "MC_12143001", EffHist, Systematic)

    # Calcualte Norm term from B+->JpsiK+
    Normalization_term_Bu   = N_Bu2JpsiK/(f_Bu * BR_BJpsiK * BR_Jpsi_mumu * eff_cuts_JpsiK * eff_Iso_BuJpsiK * eff_gencut_JpsiK)
    e_Normalization_term_Bu = Normalization_term_Bu*sqrt( (e_BR_BJpsiK/BR_BJpsiK)**2 + (e_BR_Jpsi_mumu/BR_Jpsi_mumu )**2 + ( e_N_Bu2JpsiK/N_Bu2JpsiK )**2 + (e_eff_cuts_JpsiK/eff_cuts_JpsiK)**2  + (e_eff_gencut_JpsiK/eff_gencut_JpsiK)**2 )
    print 'Normalization_term_Bu : ' , Normalization_term_Bu , ' +/- ' , e_Normalization_term_Bu
    
    # Calculate the eff. of the Physics background

    print HistCut+ NoBDT + "_BuID", "MC_12873002_SS", EffHist, Systematic
    eff_cuts_BuD0munu  , e_eff_cuts_BuD0munu  = GetEff(Histograms, HistCut+ NoBDT + "_BuID", "MC_12873002_SS", EffHist, Systematic)
    eff_cuts_BdD0munuX , e_eff_cuts_BdD0munuX = GetEff(Histograms, HistCut+ NoBDT + "_BdID", "MC_11874010_SS", EffHist, Systematic)
    #Get the genrator level eff.
    
    # Calcualte the B+ -> D0 mu nu expected Yields 
    N_BuD0munu   = ( (BR_BuDmunu * BR_D0_Kpi * eff_cuts_BuD0munu * eff_gencut_D0munu) ) * Normalization_term_Bu * f_Bu
    e_N_BuD0munu = N_BuD0munu*sqrt((e_BR_BuDmunu/BR_BuDmunu)**2 + (e_BR_D0_Kpi/BR_D0_Kpi)**2 + (e_eff_cuts_BuD0munu/eff_cuts_BuD0munu)**2 + (e_eff_gencut_D0munu/eff_gencut_D0munu)**2  +  (e_Normalization_term_Bu/Normalization_term_Bu)**2 ) 
    print 'Bu SL background Yield is : ' , N_BuD0munu , ' +/- ' , e_N_BuD0munu 
    
    
    # Calcualte the Bd -> D* mu nu expected Yields 
    N_BdD0munuX = ( (BR_BdD0munuX * BR_D0_Kpi * eff_cuts_BdD0munuX * eff_gencut_BdD0munuX ) ) * Normalization_term_Bu * f_Bd
    e_N_BdD0munuX = N_BdD0munuX*sqrt( (e_BR_BdD0munuX/BR_BdD0munuX)**2 + (e_BR_D0_Kpi/BR_D0_Kpi)**2 + (e_eff_cuts_BdD0munuX/eff_cuts_BdD0munuX)**2 + (e_eff_gencut_BdD0munuX/eff_gencut_BdD0munuX)**2  + (e_Normalization_term_Bu/Normalization_term_Bu)**2 ) 
    print 'Bd SL background Yield is : ' , N_BdD0munuX , ' +/- ' , e_N_BdD0munuX 
    
    
    # retrun results
    eff_Iso_BuD0munu,  eff_Iso_BuD0munu_err  = GetCutEff(Histograms, HistCut+"", HistCut+NoBDT, "MC_12873002_SS", EffHist, Systematic)
    eff_Iso_BdD0munuX, eff_Iso_BdD0munuX_err = GetCutEff(Histograms, HistCut+"", HistCut+NoBDT, "MC_11874010_SS", EffHist, Systematic)
    print 'Expected BuD0munu Yields after the BDTs cuts : ' , N_BuD0munu*eff_Iso_BuD0munu
    print 'Expected BdD0munuX Yields after the BDTs cuts : ' , N_BdD0munuX*eff_Iso_BdD0munuX

    # 2- Shapes of the physcis backgrounds :
    #Bs_MCORR_Bu2D0mu  = Histograms.Get("{Syst}/KMuNu/MC_12873002_SS/{Cut}/{Name}".format(Syst=Systematic, Cut=HistCut+"_BuID", Name=PlotHist))
    Bs_MCORR_Bu2D0mu   =   Histograms.Get("{Syst}/KMuNu/MC_10010032_SS/{Cut}/{Name}".format(Syst=Systematic, Cut=HistCut, Name=PlotHist))
    Bs_MCORR_Bu2D0mu .Add( Histograms.Get("{Syst}/KMuNu/MC_10010035_SS/{Cut}/{Name}".format(Syst=Systematic, Cut=HistCut, Name=PlotHist)))
    Bs_MCORR_Bu2D0mu .Add( Histograms.Get("{Syst}/KMuNu/MC_10010037_SS/{Cut}/{Name}".format(Syst=Systematic, Cut=HistCut, Name=PlotHist)))
    Bs_MCORR_Bu2D0mu.SetLineColor(kRed)
    #Bs_MCORR_Bd2Dstmu = Histograms.Get("{Syst}/KMuNu/MC_11874010_SS/{Cut}/{Name}".format(Syst=Systematic, Cut=HistCut+"_BdID", Name=PlotHist))
    Bs_MCORR_Bd2Dstmu  =   Histograms.Get("{Syst}/KMuNu/MC_10010032_SS/{Cut}/{Name}".format(Syst=Systematic, Cut=HistCut, Name=PlotHist))
    Bs_MCORR_Bd2Dstmu.Add( Histograms.Get("{Syst}/KMuNu/MC_10010035_SS/{Cut}/{Name}".format(Syst=Systematic, Cut=HistCut, Name=PlotHist)))
    Bs_MCORR_Bd2Dstmu.Add( Histograms.Get("{Syst}/KMuNu/MC_10010037_SS/{Cut}/{Name}".format(Syst=Systematic, Cut=HistCut, Name=PlotHist)))
    Bs_MCORR_Bd2Dstmu.SetLineColor(kGreen+2)

    
        
    print "Bs_MCORR_Bd2Dstmu.Integral() = " , Bs_MCORR_Bd2Dstmu.Integral() 
    Bs_MCORR_Bu2D0mu.Scale ( N_BuD0munu  * eff_Iso_BuD0munu / max(Bs_MCORR_Bu2D0mu.Integral() , 1.0))
    Bs_MCORR_Bd2Dstmu.Scale( N_BdD0munuX * eff_Iso_BdD0munuX/ max(Bs_MCORR_Bd2Dstmu.Integral(), 1.0))
    print "Bs_MCORR_Bd2Dstmu.Integral() = " , Bs_MCORR_Bd2Dstmu.Integral()
    
    print "These Numbers are calculated for the follwing cuts :"
    #print "Cut : " , str(Cut)
    #print "Additional_Cuts" , Additional_cuts
    return N_BuD0munu*eff_Iso_BuD0munu,e_N_BuD0munu,Bs_MCORR_Bu2D0mu,N_BdD0munuX*eff_Iso_BdD0munuX,e_N_BdD0munuX,Bs_MCORR_Bd2Dstmu
    
