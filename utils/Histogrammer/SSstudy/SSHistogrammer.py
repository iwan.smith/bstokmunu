import ROOT
from GetCombComponent_SS import GenerateTemplate

ROOT.gROOT.SetBatch(True)
import os
ROOT.gROOT.ProcessLine(".x "+os.environ["BSTOKMUNUROOT"]+"/include/official.C")

Histograms = ROOT.TFile("/eos/lhcb/wg/semileptonic/Bs2KmunuAna/Histogrammer/Output/Histograms.root", "READ")
HistogramsOut = ROOT.TFile("/eos/lhcb/wg/semileptonic/Bs2KmunuAna/Histogrammer/Output/Histograms_SS.root", "RECREATE")

BannedSystematics = [
    "EventTuples",
    "Luminosity",
    ""
]
BannedDecays = [
    "MCDecay", 
    "MCDecay_NoGenCut", 
    "Data", 
    "FakeK", 
    "FakeMu", 
    "FakeKMu", 
    "SameSign", 
    "SameSign_FakeK", 
    "SameSign_FakeMu", 
    "SameSign_FakeKMu"
]

BannedCuts = [
    "High_MCORRERR",
    "Low_MCORRERR",
    "Nominal"]



BannedHists = ["Systematic_Toys"]

def SaveObject(object, f_out, folder):
    histname = object.GetName();


    if not f_out.Get( folder ):
        f_out.mkdir( folder );

    f_out.cd( folder );

    #if f_out.Get( folder + "/" + histname ):
    #    gDirectory.Delete( (histname + ";1"));


    object.Write();



Path = ""
for Syst in Histograms.GetListOfKeys():
    SystName = Syst.GetName()
    if SystName in BannedSystematics:
        continue
    Path = SystName
    
    Mode = "KMuNu"
    Path = SystName + "/" + Mode

    DecayName = "MC_13512010"
    Path = SystName + "/" + Mode + "/" + DecayName
    
    print Path
    if ( not Histograms.Get(Path) ):
        continue
    
    for Cut in Histograms.Get(Path).GetListOfKeys():
        CutName = Cut.GetName()
        if CutName in BannedCuts:
            continue
        
        Path = SystName + "/" + Mode + "/" + DecayName + "/" + CutName
        print Path
        for Hist in Histograms.Get(Path).GetListOfKeys():
            HistName = Hist.GetName()
            if HistName in BannedHists:
                continue
    

            print Path
            
            print CutName, HistName, "h_Bs_MCORR", SystName
            
            try:
                Histogram = GenerateTemplate(Histograms, CutName, HistName, "h_Bs_MCORR", SystName)
                
                NewPath = SystName + "/" + Mode + "/SameSign_PhysSubtr/" + CutName
                
                Histogram.SetName(HistName)
                
                SaveObject(Histogram, HistogramsOut, NewPath)
            
            except:
                print "Histogram: {0} - FAILED".format( CutName + HistName + SystName)

HistogramsOut.Close()
            
            
