/** \file
 * HistogrammerTools.h
 *
 *  Created on: 18 May 2017
 *      Author: ismith
 */

#ifndef HISTOGRAMMER_INCLUDE_HISTOGRAMMERTOOLS_H_
#define HISTOGRAMMER_INCLUDE_HISTOGRAMMERTOOLS_H_

#include "TSystem.h"
#include "TRegexp.h"
#include "TString.h"
#include "TObjString.h"
#include "TCanvas.h"
#include "TFile.h"
#include "TTree.h"
#include "TTreeFormula.h"
#include "TChain.h"
#include "TH1.h"
#include "TMath.h"
#include "TRandom.h"

#include <map>

#define BLINDED true
int BLINDING_NUMBER = 1485260613;



namespace HistogrammerTools {
	long GetTimens();
	double GetMean(std::vector<double> numbers);
	double GetSD(std::vector<double> numbers);
	std::vector<std::string> WildCards( std::string basename);

	/*
	struct BranchStore {
		std::string   BranchName;
		char          type = '\0'; // Character corresponding to one of the types below
		unsigned int  i =  1000;
		int           I = -1000;
		double        D = -1000;
		float         F = -1000;
		long          L = -1000;
		unsigned long l = -1000;
		bool          O = -1000;

		void ConvertTypes();
	};


	struct BranchHolder{

		std::map< std::string, BranchStore> Branches;

		BranchStore* operator[](std::string);

		std::map< std::string, BranchStore>::iterator begin(){
			return Branches.begin();
		}

		std::map< std::string, BranchStore>::iterator end(){
			return Branches.end();
		}

		void clear(){
			Branches.clear();
		}
	};
*/

	enum class OverflowTreatment{ Zero, Unity, Closest };

	double FindBinContent(TH1*, OverflowTreatment, double Value1, double Value2 = 0, double Value3 = 0);


	TH1* CloneToyHist(TH1*, long seed = 0);


	void SaveObject(TObject*, std::string, std::string folder = "", std::string openhow = "UPDATE");
	void SaveObject(TObject*, TFile*,      std::string folder = "", std::string openhow = "UPDATE");
	void SaveHist  (TH1*,     std::string, std::string folder = "", std::string openhow = "UPDATE", bool Normalise = true);
	void SaveHist  (TH1*,     TFile*,      std::string folder = "", std::string openhow = "UPDATE", bool Normalise = true);

}


#endif /* HISTOGRAMMER_INCLUDE_HISTOGRAMMERTOOLS_H_ */
