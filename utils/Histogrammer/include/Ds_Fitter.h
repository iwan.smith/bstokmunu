/** \file
 * Ds_Fitter.h
 *
 *  Created on: 14 Jul 2017
 *      Author: ismith
 */

#ifndef HISTOGRAMMER_INCLUDE_DS_FITTER_H_
#define HISTOGRAMMER_INCLUDE_DS_FITTER_H_

#ifndef FITCLASS
#define FITCLASS 1


#include "TH1.h"
#include "TH2F.h"
#include "TH3F.h"
#include "TH3D.h"

#include "TCanvas.h"
#include "TPad.h"
#include "TVirtualPad.h"
#include "TFile.h"

#include "RooRealVar.h"
#include "RooGaussian.h"
#include "RooAddPdf.h"
#include "RooPolynomial.h"
#include "RooHist.h"
#include "RooDataHist.h"
#include "RooPlot.h"

#include <memory>

class Ds_Fitter{

private:
  double l_mass = 0;
  double h_mass = 0;


  std::shared_ptr<RooRealVar>    D_M = nullptr;             // The variable which varies In this case Charm Mass

  std::shared_ptr<RooDataHist>   Data = nullptr;            // The Data histogram

  std::shared_ptr<RooRealVar>    D_DG_mu = nullptr;          // Mean Ds Mass
  std::shared_ptr<RooRealVar>    D_DG_s1 = nullptr;          // Sigma 1 value
  std::shared_ptr<RooRealVar>    D_DG_s2 = nullptr;          // Sigma 2 value
  std::shared_ptr<RooGaussian>   D_Gauss1_pdf = nullptr;     // Gauss 1
  std::shared_ptr<RooGaussian>   D_Gauss2_pdf = nullptr;     // Gauss 2
  std::shared_ptr<RooRealVar>    D_DG_f1 = nullptr;           // Fraction of Gauss 1
  std::shared_ptr<RooAddPdf>     D_DoubleGauss_pdf = nullptr;  // The DoubleGauss PDF

  std::shared_ptr<RooRealVar>    pol_var_1 = nullptr;          // Background x^0 value
  std::shared_ptr<RooRealVar>    pol_var_2 = nullptr;      // Background x^1 value
  std::shared_ptr<RooPolynomial> poly = nullptr;              // Polynomial PDF

  std::shared_ptr<RooRealVar>    s_nev = nullptr;        // Number of Signal
  std::shared_ptr<RooRealVar>    b_nev = nullptr;        // Number of Background
  std::shared_ptr<RooAddPdf>     Ds_pdf = nullptr;      // Data Distribution


  TH1* h_to_subtract = nullptr;   // Histogram which will have the background subtracted from
  TH1* h_subtracted  = nullptr;   // Histogram with background subtracted
  TH1* h_Ds          = nullptr;   // Histogram of the Ds Mass integrated over all bins

  int n_Dim  = 0;                 // Dimensions of histogram. Should be 2 or 3

  std::vector< std::shared_ptr<TCanvas> > Fit_Plots;// Plots from the fits will be saved here.

public:



  Ds_Fitter(TH1* hist = nullptr);
  Ds_Fitter() = default;
  ~Ds_Fitter() = default;

  void Fit( TH1* );

  void GetSigYield( double&, double& ) const;
  void GetBGYield ( double&, double& ) const;

  void FixSignal( bool fix = true );
  void FixBG( bool fix = true );


  TH1* Subtract_2D();
  TH1* Subtract_3D();
  TH1* Subtract();

  std::vector<std::shared_ptr<TCanvas>> GetCanvases() const { return Fit_Plots; };


  void Plot( std::shared_ptr<TCanvas> );

};

#endif




#endif /* HISTOGRAMMER_INCLUDE_DS_FITTER_H_ */
