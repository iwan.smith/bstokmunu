/** \file
 * Systematic.h
 *
 *  Created on: 7 Jun 2017
 *    Author: ismith
 */

#ifndef HISTOGRAMMER_SRC_LIB_SYSTEMATIC_H_
#define HISTOGRAMMER_SRC_LIB_SYSTEMATIC_H_

#include "Variable.h"

#include "TH2F.h"
#include <algorithm>
#include <memory>
#include "HistogrammerTools.h"



using namespace std;

class Systematic: public Variable
{


protected:



  map<string, Variable*>* Variables = nullptr;

  int nToys;
  vector<double> Weights;

  void SetNToys( int toys );


  bool is_Verbose = true;
public:

  // Make it uncopyable
  Systematic(Systematic&) = delete;
  Systematic& operator=(Systematic&) = delete;

  Systematic(int toys = 10);
  virtual ~Systematic(){};

  virtual void SetVariables(map<string, Variable*> *);

  virtual void     CalculateVar (int ev = 0)   =0;
  double       GetVar     () const;
  pair<void*, char>  GetPointer   (char type = 'F'){return pair<void*, char>(&Weights[0], 'D');};
  const vector<double>&  GetWeights   () const     { return Weights;};

  int  GetnToys() const {return nToys;};
  void ResetToys();

    void SetVerbose(bool Verbosity){is_Verbose = Verbosity;};
    bool GetVerbose()        {return is_Verbose;};
};


/********************************************************
 *
 * Declare the Nominal systematic
 *
 ********************************************************/

class Systematic_Nominal: public Systematic
{
public:

  //double GetVar(int ev);
  void   CalculateVar(int ev){};

  Systematic_Nominal(int toys = 10)
    :Systematic(toys)
    {};
  virtual ~Systematic_Nominal(){};
};



/********************************************************
 *
 * Declare the Variabel reweighter systematic
 *
 ********************************************************/


class Systematic_Variable: public Systematic
{
public:

    //double GetVar(int ev);
    void   CalculateVar(int ev = 0);

    Systematic_Variable(int toys, std::string VarName)
      :Systematic(toys)
      ,m_VarName(VarName)
      {};
    virtual ~Systematic_Variable(){};

private:
   string m_VarName;
};




/********************************************************
 *
 * Declare the Hisotgram reweighting class
 *
 ********************************************************/


using HistogrammerTools::OverflowTreatment;

class Systematic_Hist: public Systematic
{

  //shared_ptr<TFile>     F_Syst= nullptr;
  vector<shared_ptr<TH1>> h_Syst;

  string Variable1;
  string Variable2;
  string Variable3;

  double Value1 = 0;
  double Value2 = 0;
  double Value3 = 0;

  OverflowTreatment TreatOutOfBounds;
              // Options Are: Unity Zero Closest

public:
  Systematic_Hist();
  Systematic_Hist( int, string FileName, string HistName, OverflowTreatment Treatment, string Var1 = "", string Var2 = "", string Var3 = "");
  Systematic_Hist( Systematic_Hist& obj, string Var1, string Var2 = "", string Var3 = "");
  virtual ~Systematic_Hist() = default;


  void     CalculateVar(int);


};

/********************************************************
 *
 * Declare the Histogram Combiner class
 *
 ********************************************************/




class Systematic_Combiner: public Systematic
{

  vector<Systematic*> Systematics;

public:

  template<typename ...Args>
  Systematic_Combiner(Args ... args)
    :Systematics( vector<Systematic*>{args ...} )
  {


    SetNToys(Systematics[0]->GetnToys());
  }
  virtual ~Systematic_Combiner()
  {
  }

  void   CalculateVar(int);
  //double GetVar(int);



};



/*******************************************************
 *
 * Declare the PID Systematics classes
 *
 *
 ******************************************************/



class Systematic_PID: public Systematic
{
  shared_ptr<Systematic_Hist> Data_Up;
  shared_ptr<Systematic_Hist> Data_Down;
  shared_ptr<Systematic_Hist> MC_Up;
  shared_ptr<Systematic_Hist> MC_Down;


public:

  Systematic_PID(int, string, string);
  Systematic_PID(Systematic_PID&, string);
  ~Systematic_PID() = default;

  void SetVariables(map<string, Variable*>*);
  void   CalculateVar(int);
  //double GetVar(int);



};






#endif /* HISTOGRAMMER_SRC_LIB_SYSTEMATIC_H_ */
