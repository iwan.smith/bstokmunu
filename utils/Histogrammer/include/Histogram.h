/** \file
 * Histogram.h
 *
 *  Created on: 15 May 2017
 *      Author: ismith
 */

#ifndef HISTOGRAMMER_SRC_LIB_HISTOGRAM_H_
#define HISTOGRAMMER_SRC_LIB_HISTOGRAM_H_

#include <memory>

#include "TH1F.h"
#include "TH2F.h"
#include "TH3F.h"
#include "Variable.h"
#include "Systematic.h"
//=============================================================================
// Declaration of base class Histogram
//=============================================================================


struct BaseHistogram
{
    shared_ptr<TH1> m_Histogram;
    std::vector<std::string> m_Vars;

    BaseHistogram(){}
    BaseHistogram(TH1* h, std::vector<std::string> Vars)
        :m_Histogram(shared_ptr<TH1>(h))
        ,m_Vars(Vars)
    {}

};


struct CachedBinLocation
{

  int binx = -1;
  int biny = -1;
  int binz = -1;
  int ev = -1;

};


class Histogram
{

public:


    Histogram(Systematic*, Combined_Variable_Formula*);
    virtual ~Histogram() = default;


    double GetWeight() const { return 1.0;};

    virtual TH1* GetHist() const      = 0;
    virtual void AnalyseEvent(int ev) = 0;

    void        SetDir( const std::string& Directory){m_Directory = Directory;}
    std::string GetDirectory() const {return m_Directory;}

    double GetFillWeight() const { return m_Weight->GetVar(); };

protected:



    Systematic*       m_Systematic;
    Combined_Variable_Formula* m_Weight;

    std::string m_Directory = "";

    std::shared_ptr<CachedBinLocation> CachedBin;

};


//=============================================================================
// Declaration of class Histogram1
//=============================================================================




class Histogram1 : public TH1F, public Histogram
{

public:

    Histogram1(TH1F*, Variable&, Systematic*, Combined_Variable_Formula*);
    Histogram1();
    virtual ~Histogram1() = default;

    void AnalyseEvent(int ev) override;

    TH1* GetHist() const {return (TH1*) this;}


    // Override the Fill Member of TH1 so that one can lookup the cached values
    Int_t Fill(Double_t x, Double_t w);

private:
    Variable& VarX;

   int m_ev = -1;

};



//=============================================================================
// Declaration of class Histogram2
//=============================================================================


class Histogram2 : public TH2F, public Histogram
{

public:

    Histogram2(TH2F*, Variable&, Variable&, Systematic*, Combined_Variable_Formula*);
    Histogram2();
    virtual ~Histogram2() = default;

    void AnalyseEvent(int ev) override;
    TH1* GetHist() const {return (TH1*) this;}

    Int_t Fill(Double_t x, Double_t y, Double_t w);
private:
    Variable& VarX;
    Variable& VarY;
    int m_ev = -1;

};

//=============================================================================
// Declaration of class Histogram3
//=============================================================================




class Histogram3 : public TH3F, public Histogram
{

public:

    Histogram3(TH3F*, Variable&, Variable&, Variable&, Systematic*, Combined_Variable_Formula*);
    Histogram3();
    virtual ~Histogram3() = default;

    void AnalyseEvent(int ev) override;
    TH1* GetHist() const {return (TH1*) this;}

    Int_t Fill(Double_t x, Double_t y, Double_t z, Double_t w);

private:
    Variable& VarX;
    Variable& VarY;
    Variable& VarZ;
    int m_ev = -1;

};


//=============================================================================
// Declaration of class for toy histograms
//=============================================================================

class HistogramToy: public TH1F, public Histogram
{
public:
    HistogramToy( Systematic*, Combined_Variable_Formula* );
    virtual ~HistogramToy(){};

    void AnalyseEvent(int) override;
    TH1* GetHist() const {return (TH1*) this;}



};





#endif /* HISTOGRAMMER_SRC_LIB_HISTOGRAM_H_ */
