/** \file
 * Variable.h
 *
 *  Created on: 7 Jun 2017
 *      Author: ismith
 */


#ifndef INCLUDE_VARIABLE_H
#define INCLUDE_VARIABLE_H 1

#include <vector>
#include <map>
#include <memory>
#include <utility>
#include <functional>

#include <iostream>

#include "TLeaf.h"
#include "TCutG.h"
#include "TFormLeafInfo.h"

#include "TFile.h"
#include "TCut.h"
#include "TString.h"
#include "TNamed.h"
#include "HistogrammerTools.h"
#include <cstdarg>

using namespace std;
/** @class Variable Variable.h
 *  
 *
 * @author Paul Seyfert <pseyfert@cern.ch>
 * @date   2017-03-08
 */

/***********************************************
 *
 *            Base Class of type variable
 *         This will be the base for all "variable" class types.
 *                 -> Branches, Functions, Systematics
 *
 ***********************************************/



class Variable
{

protected:
    int calculatedevent = -1;

public:

    Variable() = default;
    virtual ~Variable() = default;


    virtual double             GetVar      ( ) const =0 ;
    virtual void               CalculateVar( int ev = 0 ) =0 ;
    virtual pair<void*, char>  GetPointer(char type = 'F') = 0;

};


class Variable_Value: public Variable
{
    float Value=0;

public:
    Variable_Value  (){};
    Variable_Value  (double v){Value = v;};
    ~Variable_Value () {};

    void              SetVar       ( double v       ) { Value=v;};
    double            GetVar       (    ) const { return Value; };
    void              CalculateVar ( int  ev = 0    ) {};
    pair<void*, char> GetPointer   ( char type = 'F') { return pair<void*, char>(&Value, 'F');};

};


class Variable_Branch: public Variable
{


private:


    std::string   m_BranchName;
    char          m_type = '\0'; // Character corresponding to one of the types below
    unsigned int  i =  1000;
    int           I = -1000;
    double        D = -1000;
    float         F = -1000;
    long          L = -1000;
    unsigned long l = -1000;
    bool          O = -1000;

    void ConvertTypes();

public:

    /// Standard Constructor

    Variable_Branch(){};
    Variable_Branch(std::string);
    virtual ~Variable_Branch() = default;

    void AttachTree( TTree* );

    std::string GetName() const{ return m_BranchName;};
    char        GetType() const{ return m_type;};

    void              CalculateVar(int);
    double            GetVar() const { return D;};
    pair<void*, char> GetPointer(char type = 'F');




};


class Variable_Formula: public Variable
{

public:
    Variable_Formula(){};
    Variable_Formula(std::string, const TCut&);
    virtual ~Variable_Formula(){}; ///< Destructor


    void AttachTree( TTree* );

    void              CalculateVar(int);
    double            GetVar() const {return CalculatedResult;};
    pair<void*, char> GetPointer(char type = 'D'){ return pair<void*, char>(&CalculatedResult, 'D');};

private:
    double CalculatedResult = -1000;

    std::string m_name;
    TCut m_formula;

    std::shared_ptr<TTreeFormula> m_TTreeFormula;

};




class Combined_Variable_Formula: public Variable
{

public:
  Combined_Variable_Formula()
    :m_Variables(*(new map<std::string, Variable_Formula>)){};

  //template<class ...Ts>
  Combined_Variable_Formula(map<std::string, Variable_Formula>& Vars, std::initializer_list<std::string> inputs)
    :m_Formulae{inputs}
    ,m_Variables(Vars){}

  virtual ~Combined_Variable_Formula(){}; ///< Destructor


  void AttachTree( TTree* ){};

  void              CalculateVar(int);
  double            GetVar() const              {return CalculatedResult;};
  pair<void*, char> GetPointer(char type = 'D') { return pair<void*, char>(&CalculatedResult, 'D');};

private:
  double CalculatedResult = -1000;

  vector<std::string> m_Formulae;
  std::map<std::string, Variable_Formula>& m_Variables;

};






class Variable_Function: public Variable
{


public:
    Variable_Function()
      :m_Branches(*(new map<std::string, Variable*>)){};

    Variable_Function(map<std::string, Variable*>&, std::function <double (map<std::string, Variable*>&, void*)> f , void* AdditionalArgs = nullptr);
    virtual ~Variable_Function(){};

    void              CalculateVar(int);
    double            GetVar() const {return CalculatedResult;};
    pair<void*, char> GetPointer(char type = 'D') { return pair<void*, char>(&CalculatedResult, 'D');};


private:
    std::function<double (map<std::string, Variable*>&, void*)> VariableCalculator;

    //double (*VariableCalculator)(map<std::string, Variable*>&, void*);
    void* Additional_Arguments = nullptr;
    double CalculatedResult = -1000;

    map<std::string, Variable*>& m_Branches;



};


#endif // INCLUDE_VARIABLE_H

