#ifndef HISTOGRAMMER_ADDON_H
#define HISTOGRAMMER_ADDON_H

/** \file
 * Histogrammer_AddOn.h
 *
 *  Created on: 17 Jul 2017
 *      Author: ismith
 *  This is for addons which will be run once the histogrammer has completed execution
 *  e.g. non-trivial background subtraction. - Ds Fitting, SameSign physics subtraction.
 *
 */

// This is for addons which will be run once the histogrammer has completed execution
// e.g. non-trivial background subtraction. - Ds Fitting, SameSign physics subtraction.

#include <iostream>
#include <memory>

#include "TH1F.h"
#include "TH2F.h"
#include "TH3F.h"

#include "TFile.h"

namespace Histogrammer_AddOn{

	void SubtractDs(std::string, std::string SampleType = "Data", bool SaveNorm = false);


}

#endif
