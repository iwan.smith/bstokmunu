/** \file
 * HistogrammerKMuNu.h
 *
 *  Created on: 28 May 2017
 *      Author: ismith
 */

#ifndef HISTOGRAMMER_INCLUDE_HISTOGRAMMER_H_
#define HISTOGRAMMER_INCLUDE_HISTOGRAMMER_H_

#include "HistogrammerTools.h"
#include "Histogram.h"
#include "Systematic.h"
#include "Variable.h"

#include "TCut.h"

#include "TNamed.h"
#include "TFile.h"

#include <functional>
#include <vector>
#include <map>

#include "OstreamCache.h"

class Histogrammer
{


public:

  std::string currentFile = "";

  //COnstructors and Destructors
  Histogrammer() = default;
  Histogrammer(std::string, std::string, std::string FriendFileName = "");
  virtual ~Histogrammer() = default;

  void AddHistogram(const char*, const char*, Int_t, Double_t, Double_t, std::string);
  void AddHistogram(std::string, TH1F*, std::string);
  void AddHistogram(std::string, std::string, Int_t, Double_t, Double_t, std::function <double (map<std::string, Variable*>&, void*)>);

  void AddHistogram(const char*, const char*, Int_t, Double_t, Double_t, Int_t, Double_t, Double_t, std::string, std::string);
  void AddHistogram(std::string, TH2F*, std::string, std::string);

  void AddHistogram(const char*, const char*, Int_t, Double_t, Double_t, Int_t, Double_t, Double_t, Int_t, Double_t, Double_t, std::string, std::string, std::string);
  void AddHistogram(std::string, TH3F*, std::string, std::string, std::string);

  void DeleteHistogram( std::string );


  void AddBranch( std::string, std::string shortname = "" );
  void DeleteBranch( std::string );

  void AddFunction( std::string, std::function <double (map<std::string, Variable*>&, void*)>, void* AdditionalArguments = nullptr );
  void DeleteFunction( std::string );

  void AddFormula( std::string, TCut );
  void DeleteFormula( std::string );

  void AddSystematic(std::string, Systematic*);
  void DeleteSystematic( std::string );

  void AddAlias    (std::string name, std::string formula);
  void DeleteAlias (std::string name);

  void AddCut( std::string, const TCut& );

  void AddCut(std::string CutName, std::initializer_list<std::string> Cuts);

  //void AddCut( std::string, std::string );
  void DeleteCut( std::string );

  //void AddVariable(std::string, Variable*);
  //void DeleteVariable( std::string );


  void SetCommonCut    (TCut CommonCut)        { m_CommonCuts     = CommonCut;};

  void SetPrintFreq     ( int PF )             { m_PrintFreq       = PF; };
  void SetMaxEv         ( int ME )             { m_maxev           = ME; };
  void SetMaxFiles      ( int MF )             { m_MaxFiles        = MF; };
  void SaveFriendTree   ( bool savetree = true){ m_SaveTree        = savetree;};
  void SaveNorm         ( bool savenorm = true){ m_SaveNorm        = savenorm;};

  void SetPrintHist     (std::string h_Name, std::string h_Cut, std::string h_Systematic)
  {
    PrintName       = h_Name;
    PrintCut        = h_Cut;
    PrintSystematic = h_Systematic;
  }

  // Run over a file.
  // This will open the tree from the file and loop over all events filling the histograms and trees.
  void InitialiseDataType(std::string);
  void runFile(std::string, std::string);
  void run(std::string, std::string);
  void FinaliseDataType();

  void Reset();
  void SetSilent(bool Silent = true){VerboseOutput = 1-Silent;};

  TH1* GetHist( std::string );

  void Print(int, int, int);


private:

  std::string m_HistogramFileName;
  std::string m_FriendFileName;

  std::string RecoAs;
  std::string DataType;

  TFile* OutputTreeFile = nullptr;
  TTree* OutputTree     = nullptr;


  TCut m_CommonCuts = "1";


  // Map of histograms which will be used as templates
  std::map<std::string, BaseHistogram> RootHistograms;
  // The histograms which will be saved to the ROOT FILE
  std::map<std::string, std::vector<std::shared_ptr<Histogram>>> Histograms;

  std::string PrintName = "";
  std::string PrintSystematic = "";
  std::string PrintCut = "";
  std::shared_ptr<Histogram> PrintHistogram;

  // Vector Holding the variables which will be calculated/found
  std::map<std::string, Variable*> Variables;


  // Vector Holding the variables which will be calculated/found
  std::map<std::string, Variable_Branch>   Branches;
  std::map<std::string, Variable_Function> Functions;
  std::map<std::string, Variable_Formula>  Formulas;

  std::map<std::string, std::string>  Aliases;


  TNamed EventTupleEntries;
  TNamed Luminosity, LuminosityErr;


  // Vector Holding the Systematics Classes
  std::map<std::string, Systematic*> Systematics;

  // Vector Holding the Cuts or Weights
  std::map<std::string, Combined_Variable_Formula> Weights;

  //HistogrammerTools::BranchHolder BranchInfo;


  int m_PrintFreq = 100;
  int m_maxev = -1;
  int m_MaxFiles = -1;

  bool m_SaveTree = false;
  bool m_SaveNorm = false;

  uint nfile = 0; // File counter. Reset for every new sample


  bool VerboseOutput = true;
  OstreamCache Output;

protected:

};

#endif /* HISTOGRAMMER_INCLUDE_HISTOGRAMMER_H_ */
