/** \file
 * HistogrammerFunctions.h
 *
 *  Created on: 14 Jun 2017
 *      Author: ismith
 */

#ifndef HISTOGRAMMER_INCLUDE_HISTOGRAMMERFUNCTIONS_H_
#define HISTOGRAMMER_INCLUDE_HISTOGRAMMERFUNCTIONS_H_

#include "HistogrammerTools.h"
#include "Variable.h"

namespace Functions_KMuNu
{
double Bs_PV_Res (std::map<std::string, Variable*>&, void* Add_Arg = nullptr);
double MCTrueQ2  (std::map<std::string, Variable*>&, void* Add_Arg = nullptr);
}


namespace Functions_DsMuNu
{
double DsStar_M ( std::map<std::string, Variable*>&, void* Add_Arg = nullptr);
double DsStar_DM( std::map<std::string, Variable*>&, void* Add_Arg = nullptr);
double TrueQ2   ( std::map<std::string, Variable*>&, void* Add_Arg = nullptr);
double MCTrueQ2 ( std::map<std::string, Variable*>&, void* Add_Arg = nullptr);
}


namespace Functions_Generic
{
double MagnetPolarity(std::map<std::string, Variable*>&, void*);
double Part_ETA      (std::map<std::string, Variable*>&, void*);
}



#endif /* HISTOGRAMMER_INCLUDE_HISTOGRAMMERFUNCTIONS_H_ */
