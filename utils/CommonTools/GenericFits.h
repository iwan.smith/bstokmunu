
#ifndef GENERICIFITS
#define GENERICIFITS

#include "TH1F.h"
#include <utility>
std::pair<double,double> FitJpsiK(TH1F* Hist_B_MM, const std::string& SaveFile);

#endif
