#include <iostream>
#include <cstdlib>

#include <cmath>

#include "Efficiencies.h"

#include "KMuNuTools.h"
#include "GeneratorEfficiencies.h" // These shouldn't be accessible by user code unless the user explicitly includes them

using std::string;


/*****************************************************
 *
 *
 * A series of methods to calculate efficiencies
 *
 *
 *****************************************************/


TObject* SaferGet(TFile* f, const char* Name)
{

  TObject* obj = f->Get(Name);


  //std::cout << "trying to load: " << Name << "\n";
  if ( not obj )
  {
    std::cout << "Unable to load: " << Name << "\n";
    std::abort();
  }

  return obj;

}

TObject* SaferGet(TFile* f, std::string Name)
{
  return SaferGet(f, Name.c_str());
}


namespace Efficiencies
{


  /*****************************************************
   *
   *
   * Return Generator efficiencies from Lookup table
   *
   *
   *****************************************************/


  std::pair<double,double> GetLookupGeneratorEfficiency(string EventType, MagPol Mag  ) noexcept
  {

    try
    {
      if ( Mag == MagPol::MagAll )
      {
          const double eff = ( Eff_Gen_Down    .at(EventType) + Eff_Gen_Up.    at(EventType) ) /2.0;
          const double err = ( Eff_Gen_Down_err.at(EventType) + Eff_Gen_Up_err.at(EventType) ) /2.0;
          return {eff, err};

      }
      if ( Mag == MagPol::MagUp )
      {
        const double eff = Eff_Gen_Up    .at(EventType);
        const double err = Eff_Gen_Up_err.at(EventType);
        return {eff, err};
      }
      if ( Mag == MagPol::MagDown )
      {
        const double eff = Eff_Gen_Down    .at(EventType);
        const double err = Eff_Gen_Down_err.at(EventType);
        return {eff, err};
      }
    }
    catch( std::out_of_range& e )
    {

        std::cout << "Trying to read efficiency for event Type: " << EventType << " Which doesn't exist";
        std::abort();
    }

    return {1.0, 1.0};

  }

  /*****************************************************
   *
   *
   * Return Generator efficiencies by cutting on MCTuple
   *
   *
   *****************************************************/


  std::pair<double,double> GetCalculatedGeneratorEfficiency(TFile* f_hist, Decay Dec, float Q2Cut1_Low, float Q2Cut1_High, float Q2Cut2_Low = -1, float Q2Cut2_High = -1) noexcept
  {

    if ( Dec == Decay::Ratio )
    {
        double effKMuNu, errKMuNu, effDsMuNu, errDsMuNu;

        std::tie(effKMuNu,  errKMuNu ) = GetCalculatedGeneratorEfficiency(f_hist, Decay::KMuNu,  Q2Cut1_Low, Q2Cut1_High);
        std::tie(effDsMuNu, errDsMuNu) = GetCalculatedGeneratorEfficiency(f_hist, Decay::DsMuNu, Q2Cut2_Low, Q2Cut2_High);

        double eff = effKMuNu / effDsMuNu;
        double err = sqrt( pow(errKMuNu / effKMuNu, 2) + pow(errDsMuNu / effDsMuNu, 2) ) * eff;

        return {eff, err};

    }
    else
    {
      string DecayName = Dec==Decay::KMuNu  ? "KMuNu"  : "DsMuNu";


      TH1* h_before = (TH1*)SaferGet(f_hist, "Nominal/" + DecayName + "/MCDecay_NoGenCut/Nominal/True_Q2" );
      TH1* h_after  = (TH1*)SaferGet(f_hist, "Nominal/" + DecayName + "/MCDecay_NoGenCut/GenCut/True_Q2" );

      int binNumber_Low  = h_before->FindBin(Q2Cut1_Low );
      int binNumber_High = h_before->FindBin(Q2Cut1_High);

      double N_before = h_before->Integral(binNumber_Low, binNumber_High-1);
      double N_after  = h_after ->Integral(binNumber_Low, binNumber_High-1);

      std::cout << "Requested Q2 Range: " <<  Q2Cut1_Low << " -> " << Q2Cut1_High << " Got Range:" <<  h_before->GetBinLowEdge(binNumber_Low) << " -> " << h_before->GetBinLowEdge(binNumber_High) << std::endl;

      double efficiency = N_after/N_before * 0.5;
      double error = sqrt(N_before * efficiency * (1-efficiency) ) / N_after * 0.5;

      return {efficiency, error};
    }

    return {1.0, 1.0};

  }

  /*****************************************************
   *
   *
   * Return Efficiency WRT Fractions
   *
   *
   *****************************************************/
  std::pair<double,double> GetRatio(TFile* f_hist, string Path1, string Path2, bool BinomialError) noexcept
  {

    TH1F* h_after  = (TH1F*)SaferGet(f_hist, Path1 );
    TH1F* h_before = (TH1F*)SaferGet(f_hist, Path2 );

    if ( not h_before)
    {
        std::cout << "Couldn't open histogram: \n" << Path2 << "\n";
        std::abort();
    }

    if ( not h_after)
    {
        std::cout << "Couldn't open histogram: \n" << Path1 << "\n";
        std::abort();
    }

    const int nToy = h_after->GetNbinsX() - 1 ;

    //std::cout << "Toys: " << nToy << "\n";

    float* BinContents_after  = h_after ->GetArray();
    float* BinContents_before = h_before->GetArray();


    float eff = BinContents_after[1] / BinContents_before[1];
    float err = 0;
    {
      std::vector<float> Efficiencies(nToy, 0);

      for( int Toy = 0; Toy < nToy; Toy++)
        Efficiencies[Toy] = BinContents_after[Toy+2] / BinContents_before[Toy+2];


      float sum  = std::accumulate(Efficiencies.begin(), Efficiencies.end(), 0.0);
      float mean = sum / nToy;

      float sq_sum = std::inner_product(Efficiencies.begin(), Efficiencies.end(), Efficiencies.begin(), 0.0);
      err    = std::sqrt(sq_sum / nToy - mean * mean);
    }

    if ( BinomialError )
    {
      float BinError = sqrt( BinContents_before[1]  * eff * ( 1.0 - eff ) ) / BinContents_after[1] ;
      err = sqrt( pow(err,2) + pow(BinError, 2) );
    }

    //std::cout << "GetEff " << eff << "  " << err << "\n";
    return{ eff, err};


  }

  std::pair<double,double> GetRatio(TFile* f_hist, string Path1, string Path2, string Path3, string Path4, bool BinomialError) noexcept
  {

    /**
     * Get the ratio of ( Path1/Path2 ) / ( Path3/Path4 )
     *
     */


    TH1F* h_after   = (TH1F*)SaferGet(f_hist, Path1 );
    TH1F* h_before  = (TH1F*)SaferGet(f_hist, Path2 );
    TH1F* h_after2  = (TH1F*)SaferGet(f_hist, Path3 );
    TH1F* h_before2 = (TH1F*)SaferGet(f_hist, Path4 );

    if ( not h_before)
    {
        std::cout << "Couldn't open histogram: \n" << Path2 << "\n";
        std::abort();
    }

    if ( not h_after)
    {
        std::cout << "Couldn't open histogram: \n" << Path1 << "\n";
        std::abort();
    }

    if ( not h_before2)
    {
        std::cout << "Couldn't open histogram: \n" << Path4 << "\n";
        std::abort();
    }

    if ( not h_after2)
    {
        std::cout << "Couldn't open histogram: \n" << Path3 << "\n";
        std::abort();
    }


    const int nToy = h_after->GetNbinsX() - 1 ;

    //std::cout << "Toys: " << nToy << "\n";

    float* BinContents_after   = h_after ->GetArray();
    float* BinContents_before  = h_before->GetArray();
    float* BinContents_after2  = h_after2 ->GetArray();
    float* BinContents_before2 = h_before2->GetArray();


    float eff = ( BinContents_after[1] / BinContents_before[1] ) / ( BinContents_after2[1] / BinContents_before2[1] );
    float err = 0;
    {
      std::vector<float> Efficiencies(nToy, 0);

      for( int Toy = 0; Toy < nToy; Toy++)
        Efficiencies[Toy] = ( BinContents_after[Toy+2] / BinContents_before[Toy+2]) / ( BinContents_after2[Toy+2] / BinContents_before2[Toy+2] );


      float sum  = std::accumulate(Efficiencies.begin(), Efficiencies.end(), 0.0);
      float mean = sum / nToy;

      float sq_sum = std::inner_product(Efficiencies.begin(), Efficiencies.end(), Efficiencies.begin(), 0.0);
      err    = std::sqrt(sq_sum / nToy - mean * mean);
    }

    if ( BinomialError )
    {
      float BinError = sqrt( BinContents_before[1]  * eff * ( 1.0 - eff ) ) / BinContents_after[1] ;
      err = sqrt( pow(err,2) + pow(BinError, 2) );
    }

    //std::cout << "GetEff " << eff << "  " << err << "\n";
    return{ eff, err};


  }


  std::pair<double,double> GetCalculatedCorrection(TFile* f_hist, string EventType, string Correction1, string Correction2, string CutName, Decay Dec, bool BinomialError) noexcept
  {

    if ( Dec == Decay::Ratio )
    {
      double effKMuNu, errKMuNu, effDsMuNu, errDsMuNu;

      std::tie(effKMuNu,  errKMuNu ) = GetCalculatedCorrection(f_hist, EventType, Correction1, Correction2, CutName, Decay::KMuNu,  BinomialError);
      std::tie(effDsMuNu, errDsMuNu) = GetCalculatedCorrection(f_hist, EventType, Correction1, Correction2, CutName, Decay::DsMuNu, BinomialError);

      double eff = effKMuNu / effDsMuNu;
      double err = sqrt( pow(errKMuNu / effKMuNu, 2) + pow(errDsMuNu / effDsMuNu, 2) ) * eff;

      return {eff, err};

    }
    else
    {
      string DecayName = Dec==Decay::KMuNu  ? "KMuNu"        : "DsMuNu";

      string HistAfterName  = Correction1 + "/" + DecayName + "/" + EventType + "/" + CutName + "/Systematic_Toys";
      string HistBeforeName = Correction2 + "/" + DecayName + "/" + EventType + "/" + CutName + "/Systematic_Toys";

      return GetRatio(f_hist, HistAfterName, HistBeforeName, BinomialError);

    }
    return {1.0, 1.0};

  }

  std::pair<double,double> GetCalculatedCorrection(TFile* f_hist, string EventType, string EventType2, string Correction1, string Correction2, string CutName, string CutName2, bool BinomialError) noexcept
  {

    string HistAfterName   = Correction1 + "/KMuNu/"  + EventType  + "/" + CutName + "/Systematic_Toys";
    string HistBeforeName  = Correction2 + "/KMuNu/"  + EventType  + "/" + CutName + "/Systematic_Toys";
    string HistAfterName2  = Correction1 + "/DsMuNu/" + EventType2 + "/" + CutName2 + "/Systematic_Toys";
    string HistBeforeName2 = Correction2 + "/DsMuNu/" + EventType2 + "/" + CutName2 + "/Systematic_Toys";

    return GetRatio(f_hist, HistAfterName, HistBeforeName, HistAfterName2, HistBeforeName2, BinomialError);

  }


  std::pair<double,double> GetCalculatedEfficiency(TFile* f_hist, string EventType, string Correction, string CutName1, string CutName2, Decay Dec, bool BinomialError) noexcept
  {

    if ( Dec == Decay::Ratio )
    {
        double effKMuNu, errKMuNu, effDsMuNu, errDsMuNu;

        std::tie(effKMuNu,  errKMuNu ) = GetCalculatedEfficiency(f_hist, EventType, Correction, CutName1, CutName2, Decay::KMuNu,  BinomialError);
        std::tie(effDsMuNu, errDsMuNu) = GetCalculatedEfficiency(f_hist, EventType, Correction, CutName1, CutName2, Decay::DsMuNu, BinomialError);

        double eff = effKMuNu / effDsMuNu;
        double err = sqrt( pow(errKMuNu / effKMuNu, 2) + pow(errDsMuNu / effDsMuNu, 2) ) * eff;

        return {eff, err};

    }
    else
    {
        string DecayName = Dec==Decay::KMuNu  ? "KMuNu"        : "DsMuNu";

        string HistAfterName  = Correction + "/" + DecayName + "/" + EventType + "/" + CutName1 + "/Systematic_Toys";
        string HistBeforeName = Correction + "/" + DecayName + "/" + EventType + "/" + CutName2 + "/Systematic_Toys";

        return GetRatio(f_hist, HistAfterName, HistBeforeName, BinomialError);

    }
    return {1.0, 1.0};

  }

  std::pair<double,double> GetCalculatedEfficiency(TFile* f_hist, string EventType1, string EventType2, string Correction, string CutName1, string CutName2, string CutName3, string CutName4, bool BinomialError) noexcept
  {


    string HistAfterName   = Correction + "/KMuNu/"  + EventType1 + "/" + CutName1 + "/Systematic_Toys";
    string HistBeforeName  = Correction + "/KMuNu/"  + EventType1 + "/" + CutName2 + "/Systematic_Toys";
    string HistAfterName2  = Correction + "/DsMuNu/" + EventType2 + "/" + CutName3 + "/Systematic_Toys";
    string HistBeforeName2 = Correction + "/DsMuNu/" + EventType2 + "/" + CutName4 + "/Systematic_Toys";

    return GetRatio(f_hist, HistAfterName, HistBeforeName, HistAfterName2, HistBeforeName2, BinomialError);

  }

  /*****************************************************
   *
   *
   * Return Lookup Luminosity/MCDecayYield from Histogram file
   *
   *
   *****************************************************/

  double LookupValueFromTObject(TFile* f_hist, string Path) noexcept
  {

    TObject* obj    = f_hist->Get( Path.c_str()    );

    if ( not obj )
    {
        std::cout << "Couldn't read object from file: " << Path << "\n";
        std::abort();
    }

    double val = 0;

    try
    {
        val = std::stod(obj->GetTitle());
    }
    catch(...)
    {
        std::cout << "Couldn't convert: " << obj->GetTitle() << " To a double.\n";
        std::abort();
    }

    return val;

  }



  std::pair<double, double> GetLookupLumi(TFile* f_hist, Decay Dec) noexcept
  {

    string DecayName;
    if ( Dec == Decay::KMuNu )
      DecayName =  "KMuNu";
    else if ( Dec == Decay::DsMuNu )
      DecayName =  "DsMuNu";
    else
      std::abort();

    double Lumi    = LookupValueFromTObject(f_hist, "Luminosity/" + DecayName + "/Data");
    double LumiErr = LookupValueFromTObject(f_hist, "Luminosity/" + DecayName + "/DataErr");

    return {Lumi, LumiErr};

  }

  std::pair<double, double> GetLookupEventTuple(TFile* f_hist, Decay Dec, string EventType) noexcept
  {

    string DecayName;
    if ( Dec == Decay::KMuNu )
      DecayName =  "KMuNu";
    else if ( Dec == Decay::DsMuNu )
      DecayName =  "DsMuNu";
    else
      std::abort();

    double EvtTuple    = LookupValueFromTObject(f_hist, "EventTuples/" + DecayName + "/" + EventType);

    return {EvtTuple, 0};

  }




  std::pair<double, double> GetFullEfficiency(TFile* f_hist, Decay Dec, std::string EventType, std::string CutName, std::string CutName2)
  {

    string DecayName;
    if ( Dec == Decay::KMuNu )
      DecayName =  "KMuNu";
    else if ( Dec == Decay::DsMuNu )
      DecayName =  "DsMuNu";
    else
      std::abort();

    std::string HistLoc = "Nominal/" + DecayName + "/" + EventType + "/" + CutName + "/h_Bs_MCORR";
    TH1F* h_AfterCut = (TH1F*)SaferGet(f_hist, HistLoc );

    double nEvTuple, Eff_Gen;

    std::tie(nEvTuple, std::ignore) = GetLookupEventTuple(f_hist, Dec, EventType);
    std::tie(Eff_Gen,  std::ignore) = GetLookupGeneratorEfficiency(string( EventType.begin()+3, EventType.begin()+3+8), MagPol::MagAll );

    const double nHistogram = h_AfterCut->GetEntries();//Integral();

    const double TotalEv = nEvTuple / Eff_Gen;

    const double Eff = nHistogram / TotalEv;

    // Assume purely binomial uncertainies.

    const double Err = sqrt( TotalEv * Eff * (1-Eff)  ) / TotalEv;

    std::cout << string( EventType.begin()+3, EventType.end()) << "  nHist " << nHistogram << " Eff_Gen " << Eff_Gen << " nEvTuple " << nEvTuple << "\n";

    return {Eff, Err};



  }


  /*****************************************************
   *
   *
   * Return Branching Fractions
   *
   *
   *****************************************************/

  std::pair<double, double> GetBranchingFractions(Decay Dec) noexcept
  {
    if ( Dec == Decay::KMuNu )
      return {1.0, 0.0};

    if ( Dec == Decay::DsMuNu )
      return {0.0545, 0.0017};

    if ( Dec == Decay::Ratio )
      return {18.35, 0.57};

    return {1.0, 1.0};

  }



}
