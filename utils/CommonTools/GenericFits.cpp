using namespace std;

#include "GenericFits.h"

#include "RooWorkspace.h"
#include "RooPlot.h"
#include "RooDataHist.h"
#include "RooArgList.h"
#include "RooRealVar.h"
#include "RooAbsPdf.h"
#include "RooDataHist.h"
#include "RooHist.h"
#include "RooPlotable.h"
#include "TCanvas.h"

#include "RooMsgService.h"

pair<double,double> FitJpsiK(TH1F* Hist_B_MM, const string& SaveFile = "Bu_Fit.pdf")
{

  //RooFit::RooMsgService::instance().setSilentMode(true);


  float nev = Hist_B_MM->Integral();

  RooWorkspace w;
  w.factory("Gaussian::g1(x[5180, 5400],mu[5290, 5275, 5320], sigma [5 ,20])");
  w.factory("Gaussian::g2(x            ,mu                  , sigma2[20,50])");
  w.factory("SUM::g(f1[0.5,0,1]*g1,f2[0.5,0,1]*g2)");

  w.factory("Exponential::e(x,tau[-.0045,-0.02, 0.02])");
  w.factory(Form("SUM::model(s[%f, 0, %f]*g, b[%f, 0, %f]*e)", nev*0.5, nev, nev*0.5, nev));

  RooRealVar* x = w.var("x");
  RooAbsPdf* pdf = w.pdf("model");
  RooDataHist data("Hist_Data", "Data Histogram", RooArgList(*x), Hist_B_MM);

  /*RooFitResult* fitResult = */pdf->fitTo(data,RooFit::Save(), RooFit::PrintEvalErrors(-1));


  if (SaveFile.empty())
    return { w.var("s")->getValV(), w.var("s")->getError() };


  RooPlot* frame = x->frame();
  frame->SetTitle("Fit to B invariant mass");

  data.plotOn(frame);

  pdf->plotOn( frame, RooFit::Components("g"), RooFit::LineStyle(2), RooFit::LineColor(2));
  pdf->plotOn( frame, RooFit::Components("e"), RooFit::LineStyle(2), RooFit::LineColor(3));
  pdf->plotOn( frame );

  RooHist* hresid = frame->pullHist();
  RooPlot* frame2 = x->frame();
  frame2->SetTitle("Fit Residuals");
  frame2->addPlotable(hresid,"P");

  TCanvas c1("c1", "c1", 900, 900);
  c1.Divide(1,2);
  //c1.SetLogy();
  c1.cd(1);
  frame->Draw();
  c1.cd(2);
  frame2->Draw();


  c1.Print(SaveFile.c_str());


  return { w.var("s")->getValV(), w.var("s")->getError() };
}


/**************************************************************
 *
 * Add the wrappers so that the fit can be run with python
 *
 **************************************************************/

/*
#include <boost/python.hpp>
#include "

double FitWrapper(TH1F h1)
{

  return FitJpsiK(&h1, "");

}


BOOST_PYTHON_MODULE(GenericFits)
{
    using namespace boost::python;
    def("FitJpsiK", FitWrapper);
}
*/

