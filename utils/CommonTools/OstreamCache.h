/*
 * OstreamCache.h
 *
 *  Created on: 16 Jan 2018
 *      Author: ismith
 */

#ifndef COMMONTOOLS_OSTREAMCACHE_H_
#define COMMONTOOLS_OSTREAMCACHE_H_

#include <sstream>
#include <iostream>
#include <cstdlib>

#include <algorithm>

class OstreamCache
{


public:
  OstreamCache () = default;
  virtual ~OstreamCache () = default;


  // Declare friend functions for the two stream functions.
  // One for printing the date
  friend std::ostream& operator<<(std::ostream& lhs, OstreamCache& rhs);


  //One for storing the data
  template<typename T>
  friend OstreamCache& operator<<(OstreamCache& lhs, T rhs);


  void reset();



private:

  std::ostringstream m_data;
  int nlines = 0;

};


template<typename T>
OstreamCache& operator<<(OstreamCache& lhs, T rhs)
{
  lhs.m_data << rhs;
  return lhs;


}



#endif /* COMMONTOOLS_OSTREAMCACHE_H_*/
