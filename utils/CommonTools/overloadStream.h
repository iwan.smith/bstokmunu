#ifndef SPECIALCOUT
#define SPECIALCOUT

#include <ostream>

#include <vector>
#include <map>


#include "TH1F.h"

std::ostream& operator<< (std::ostream& lhs, const TH1* const rhs);


template<typename T>
std::ostream& operator <<( std::ostream& lhs, std::vector<T> rhs);


template<typename T1, typename T2>
std::ostream& operator <<( std::ostream& lhs, std::map<T1, T2> rhs);



#endif
