import ROOT

#__all__ = ["FitJpsiK"]
"""

Load Fits from the fitter

"""

ROOT.gSystem.Load("GenericFits_cpp.so")

FitJpsiK = ROOT.FitJpsiK


"""

Load Efficiency Calculations

"""


ROOT.gSystem.Load("Efficiencies_cpp.so")

class _MP:
    MagDown = 0
    MagUp   = 1
    MagAll  = 2

class _Dec:
    KMuNu   = 0
    DsMuNu  = 1
    Ratio   = 2

#enum class MagPol { MagDown, MagUp,   MagAll};
#enum class Decay  { KMuNu,   DsMuNu,  Ratio};

MagPol = _MP()
Decay  = _Dec()


GetLookupGeneratorEfficiency      = ROOT.Efficiencies.GetLookupGeneratorEfficiency     

GetCalculatedGeneratorEfficiency  = ROOT.Efficiencies.GetCalculatedGeneratorEfficiency 

GetRatio                          = ROOT.Efficiencies.GetRatio                         

GetCalculatedCorrection           = ROOT.Efficiencies.GetCalculatedCorrection          

GetCalculatedEfficiency           = ROOT.Efficiencies.GetCalculatedEfficiency          

GetLookupLumi                     = ROOT.Efficiencies.GetLookupLumi                    

GetLookupEventTuple               = ROOT.Efficiencies.GetLookupEventTuple              

GetFullEfficiency                 = ROOT.Efficiencies.GetFullEfficiency              

GetBranchingFractions             = ROOT.Efficiencies.GetBranchingFractions





"""

Some helper functions

"""



def PairToTuple(m_pair):
    return m_pair.first, m_pair.second 

            