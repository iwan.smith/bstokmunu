
#ifndef SPLITSTRING
  #include "SplitString.h"
#endif

#ifndef SPECIALCOUT
  #include "overloadStream.h"
#endif

#ifndef GENERICIFITS
  #include "GenericFits.h"
#endif


#ifndef COMMONTOOLS_OSTREAMCACHE_H_
  #include "OstreamCache.h"
#endif

#ifndef EFFICIENCIES_H
  #include "Efficiencies.h"
#endif
