#ifndef EFFICIENCIES_H
#define EFFICIENCIES_H

#include <utility>

#include "TFile.h"
#include "TH1.h"

void doBanner(){};

namespace Efficiencies
{

  enum class MagPol { MagDown, MagUp,   MagAll};
  enum class Decay  { KMuNu,   DsMuNu,  Ratio};


  std::pair<double, double> GetLookupGeneratorEfficiency      (std::string EventType, MagPol Mag = MagPol::MagAll ) noexcept;

  std::pair<double, double> GetCalculatedGeneratorEfficiency  (TFile* f_hist, Decay Dec, float Q2Cut1_Low, float Q2Cut1_High, float Q2Cut2_Low, float Q2Cut2_High) noexcept;

  std::pair<double, double> GetRatio                          (TFile* f_hist, std::string Path1, std::string Path2, bool BinomialError) noexcept;

  std::pair<double, double> GetRatio                          (TFile* f_hist, std::string Path1, std::string Path2, std::string Path3, std::string Path4, bool BinomialError) noexcept;

  std::pair<double, double> GetCalculatedCorrection           (TFile* f_hist, std::string EventType, std::string Correction1, std::string Correction2, std::string CutName, Decay Dec, bool BinomialError=false) noexcept;

  std::pair<double, double> GetCalculatedEfficiency           (TFile* f_hist, std::string EventType, std::string Correction, std::string CutName1, std::string CutName2, Decay Dec, bool BinomialError=false) noexcept;

  std::pair<double, double> GetCalculatedEfficiency           (TFile* f_hist, std::string EventType1, std::string EventType2, std::string Correction, std::string CutName1, std::string CutName2, std::string CutName3, std::string CutName4, bool BinomialError) noexcept;

  std::pair<double, double> GetLookupLumi                     (TFile* f_hist, Decay Dec) noexcept;

  std::pair<double, double> GetLookupEventTuple               (TFile* f_hist, Decay Dec, std::string EventType) noexcept;

  std::pair<double, double> GetFullEfficiency                 (TFile* f_hist, Decay Dec, std::string EventType, std::string CutName, std::string CutName2);

  std::pair<double, double> GetBranchingFractions             (Decay Dec) noexcept;


}
#endif
