/*
 * OstreamCache.cpp
 *
 *  Created on: 16 Jan 2018
 *      Author: ismith
 */

#include "OstreamCache.h"

#include <algorithm>


using namespace std;


void OstreamCache::reset()
{
  nlines = 0;
  m_data.clear();
}



ostream& operator<<(ostream& lhs, OstreamCache& rhs)
{

  std::string data = rhs.m_data.str();


  //start by erasing the number of lines previous

  char deletelines[50];
  sprintf( deletelines, "\033[%iA", rhs.nlines);


  // count the number of lines in this string, and save
  rhs.nlines = std::count(data.begin(), data.end(), '\n');

  lhs << deletelines;
  lhs << data << std::flush;

  rhs.m_data.str("");
  rhs.m_data.clear();

  return lhs;
}
/*
template<typename T>
OstreamCache& operator<<(OstreamCache& lhs, T rhs)
{
  lhs.m_data << rhs;
  return lhs;
}
*/


