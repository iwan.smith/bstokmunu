#include <cstdlib>
#include <iostream>
#include <map>
#include <string>
#include <deque>
#include <algorithm>
#include <cmath>
#include <memory>

#include "TTreeFormula.h"
#include "TChain.h"
#include "TFile.h"
#include "TTree.h"
#include "TString.h"
#include "TObjString.h"
#include "TSystem.h"
#include "TROOT.h"
#include "TChain.h"
#include "TEnv.h"

using std::size_t;

int MergeNTuples(std::string OutputFileName, std::string InputFilenames, std::string TreeName = "Bs2KmuNuTuple/DecayTree", uint MaxEv = -1)
{


  TChain* InputTrees  = new TChain(TreeName.c_str(), TreeName.c_str());
  InputTrees->Add(InputFilenames.c_str());

  TFile* f_Out = TFile::Open(OutputFileName.c_str(), "RECREATE");
  TTree* T_Out = new TTree("TrainingTree", "TrainingTree");

  std::vector<std::string> Branches;

  std::vector<std::string> Branches_F;
  std::vector<std::string> Branches_D;
  std::vector<std::string> Branches_I;
  std::vector<std::string> Branches_O;
  std::vector<std::string> Branches_l;

  const std::vector<std::shared_ptr<TTreeFormula>> Formulas
  {
    std::shared_ptr<TTreeFormula>(new TTreeFormula("Bu_MM", "sqrt((Bs_PE+muon_p_NIsoTr_PE)**2 - (Bs_PX+muon_p_NIsoTr_PX)**2 - (Bs_PY+muon_p_NIsoTr_PY)**2 - (Bs_PZ+muon_p_NIsoTr_PZ)**2)", InputTrees) ),
    std::shared_ptr<TTreeFormula>(new TTreeFormula("Jpsi_MM", "muon_p_PAIR_M", InputTrees) )
  };

  const int nFormula = Formulas.size();


  // Miscellaneous Branches
  Branches.push_back("Bs_Hlt2TopoMu2BodyBBDTDecision_TOS");
  Branches.push_back("Bs_M");

  // Branches for training
  Branches.push_back("nTracks");
  Branches.push_back("Bs_PT");
  Branches.push_back("Bs_ETA");
  Branches.push_back("kaon_m_PT");
  Branches.push_back("muon_p_PT");
  Branches.push_back("kaon_m_TRACK_GhostProb");
  Branches.push_back("muon_p_TRACK_GhostProb");

  for( auto& BName: Branches)
  {
    char Type = ((std::string)InputTrees->GetBranch(BName.c_str())->GetTitle()).back();

    if ( not InputTrees->GetBranch(BName.c_str()) )
      std::cout << "Fuck Branch: " << BName << std::endl;

    switch(Type){
      case 'F': Branches_F.push_back(BName); break;
      case 'D': Branches_D.push_back(BName); break;
      case 'I': Branches_I.push_back(BName); break;
      case 'O': Branches_O.push_back(BName); break;
      case 'l': Branches_l.push_back(BName); break;
      default : std::cout << "FUCK Recode this to include Type: " << Type << std::endl;
    }
  }
  std::vector<float> Values_Formula;

  std::vector<float>     Values_F      (Branches_F.size(), 0.0);
  std::vector<double>    Values_D      (Branches_D.size(), 0.0);
  std::vector<int>       Values_I      (Branches_I.size(), 0  );
  std::deque<bool>       Values_O      (Branches_O.size(), 0  );
  std::vector<ULong64_t> Values_l      (Branches_l.size(), 0.0);

  {
    InputTrees->SetCacheSize(100000000);
    for ( int b = 0; b < Branches_F.size(); b++)
    {
        InputTrees->SetBranchAddress(Branches_F[b].c_str(), &Values_F[b]);
    }
    for ( int b = 0; b < Branches_D.size(); b++)
    {
        InputTrees->SetBranchAddress(Branches_D[b].c_str(), &Values_D[b]);
    }
    for ( int b = 0; b < Branches_I.size(); b++)
    {
        InputTrees->SetBranchAddress(Branches_I[b].c_str(), &Values_I[b]);
    }
    for ( int b = 0; b < Branches_O.size(); b++)
    {
        InputTrees->SetBranchAddress(Branches_O[b].c_str(), &Values_O[b]);
    }
    for ( int b = 0; b < Branches_l.size(); b++)
    {
        InputTrees->SetBranchAddress(Branches_l[b].c_str(), &Values_l[b]);
    }

  }

  {
    Values_Formula = std::vector<float>(nFormula, 0.0);
    for ( int F = 0; F < nFormula; F++)
    {
      T_Out->Branch(Formulas[F]->GetName(), &Values_Formula[F], ((std::string)Formulas[F]->GetName() + "/F").c_str());
    }
  }




  for ( int b = 0; b < Branches_F.size(); b++)
  {
    T_Out->Branch(Branches_F[b].c_str(), &Values_F[b], (Branches_F[b] + "/F").c_str());
  }

  for ( int b = 0; b < Branches_D.size(); b++)
  {
    T_Out->Branch(Branches_D[b].c_str(), &Values_D[b], (Branches_D[b] + "/D").c_str());
  }

  for ( int b = 0; b < Branches_I.size(); b++)
  {
      T_Out->Branch(Branches_I[b].c_str(), &Values_I[b], (Branches_I[b] + "/I").c_str());
  }

  for ( int b = 0; b < Branches_O.size(); b++)
  {
      T_Out->Branch(Branches_O[b].c_str(), &Values_O[b], (Branches_O[b] + "/O").c_str());
  }

  for ( int b = 0; b < Branches_l.size(); b++)
  {
      T_Out->Branch(Branches_l[b].c_str(), &Values_l[b], (Branches_l[b] + "/O").c_str());
  }


  auto ContainsNAN = [](const auto& Container)->bool
    {
      auto NanLoc = find_if( Container.begin(), Container.end(), [](auto v){ return not std::isfinite(v);});
      return NanLoc < Container.end();
    };

  auto PrintStatus = [&OutputFileName](size_t ev, size_t Tot)
    {

      const int PercentComplete = (double)50*ev/Tot + 0.5;

      std::cout << std::setw(30) << std::left << OutputFileName << "  |";
      std::cout << std::string(   PercentComplete, '#');
      std::cout << std::string(50-PercentComplete, '-');
      std::cout << "|  " << ev << "/" << Tot;
      std::cout << std::string( 20, ' ') << "\r" << std::flush;

    };


  size_t TotEntries = InputTrees->GetEntries();

  TotEntries =std::min(TotEntries, (size_t)MaxEv);

  bool Flag = true;
  for ( size_t ev = 0; Flag; ev++)
  {

    if ( ev %1000 == 0)
      PrintStatus(ev, TotEntries);


    Flag = false;
    {

      if (ev >= InputTrees->GetEntries() )
        continue;

      Flag = true;
      InputTrees->GetEntry(ev);

      for( int F = 0; F < nFormula; F++)
      {
          Values_Formula[F] = Formulas[F]->EvalInstance();
      }


      if ( Values_Formula.back() < 3050 or Values_Formula.back() >3150 )
        continue;

      if ( Values_Formula.front() < 5180 or Values_Formula.front() >5400 )
        continue;



      if (ContainsNAN(Values_F))
        continue;
      if (ContainsNAN(Values_D))
        continue;
      if (ContainsNAN(Values_I))
        continue;
      if (ContainsNAN(Values_l))
        continue;

      T_Out->Fill();


    }

    if (ev > MaxEv)
      Flag = false;

  }


  f_Out->cd();
  T_Out->Write();
  f_Out->Close();


  std::cout << std::endl;

  return EXIT_SUCCESS;
};


int main()
{


  MergeNTuples("MC_ForTraining_Up_JPsiK.root",
      "/eos/lhcb/wg/semileptonic/Bs2KmunuAna/Tuples/Bs2KMuNu_MCTUPLES_TRIMMED_30May16/DTT_12143001_Bu_JpsiK_mm_DecProdCut_Up_Py8_MC12.root",
   "Bs2KmuNuTuple");

  MergeNTuples("MC_ForTraining_Dn_JPsiK.root",
      "/eos/lhcb/wg/semileptonic/Bs2KmunuAna/Tuples/Bs2KMuNu_MCTUPLES_TRIMMED_30May16/DTT_12143001_Bu_JpsiK_mm_DecProdCut_Dn_Py8_MC12.root",
   "Bs2KmuNuTuple");

  MergeNTuples("Data_ForTraining_Up_JPsiK.root",
      "/eos/lhcb/wg/semileptonic/Bs2KmunuAna/Tuples/Bs2KMuNu_DATATUPLES_TRIMMED_Nov17/Merged_data12_Bs2KMuNu_Up.root",
   "Bs2KmuNuTuple");

  MergeNTuples("Data_ForTraining_Dn_JPsiK.root",
      "/eos/lhcb/wg/semileptonic/Bs2KmunuAna/Tuples/Bs2KMuNu_DATATUPLES_TRIMMED_Nov17/Merged_data12_Bs2KMuNu_Down.root",
   "Bs2KmuNuTuple");


}
