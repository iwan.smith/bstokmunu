#include <TChain.h>
#include <string>
#include <iostream>
#include <TFile.h>
#include <regex>

/*
 *
 * usage:
 *   root -l -b -q setting_aliases.C\(\"root://eoslhcb.cern.ch//eos/lhcb/wg/semileptonic/Bs2KmunuAna/Tuples/B2JpsiX_DATATUPLES_RAW_15November16/DTT_2011_Reco14Strip21r1_Down_DIMUON.root\",\"Bu2JpsiKTuple/DecayTree\"\)
 *
 *
 *
 */

void setting_aliases(TString fname, TString tname) {
  TChain* ch = new TChain(tname.Data());
  ch->Add(fname.Data());
  ch->SetBranchStatus("Bs_DOCA",0);
  std::regex e("\\.root");
  std::string oname;

  /// in MC, the filename contains if we're dealing with jpsi phi or jpsi k
  /// for data, this needs to be distinguished by means of the branch name
  if ((std::regex_match(fname.Data(),std::regex(".*Jpsiphi.*")))||(std::regex_match(tname.Data(),std::regex(".*JpsiPhi.*")))) {
    oname = std::regex_replace(fname.Data(),e,"_jpsiphi.root");
  } else {
    oname = std::regex_replace(fname.Data(),e,"_jpsik.root");
  }
  TFile* of = TFile::Open(oname.c_str(),"RECREATE");
  of->cd();
  TTree* cl = ch->CloneTree();
  if ((std::regex_match(fname.Data(),std::regex(".*Jpsiphi.*")))||(std::regex_match(tname.Data(),std::regex(".*JpsiPhi.*")))) {
    std::cout << "only redoing Bs_DOCA branch" << std::endl;
    cl->SetAlias("Bs_DOCA","(eventNumber%2)*Bs_DOCA_Kmu_mp+((eventNumber+1)%2)*Bs_DOCA_Kmu_pm");
  } else {
    std::cout << "creating many Bs branches" << std::endl;
    cl->SetAlias("Bs_DOCA","Bu_DOCA_Kmu_OS");
    cl->SetAlias("Bs_PT","Bu_PT");
    cl->SetAlias("Bs_FD_S","Bu_FD_S");
    cl->SetAlias("Bs_cosTheta1_star","Bu_cosTheta1_star");
    cl->SetAlias("Bs_cosTheta2_star","Bu_cosTheta2_star");
  }
  cl->Write();
  of->WriteTObject(cl);
  of->Close();
}
