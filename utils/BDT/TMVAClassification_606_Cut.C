// @(#)root/tmva $Id$
/**********************************************************************************
 * Project   : TMVA - a ROOT-integrated toolkit for multivariate data analysis    *
 * Package   : TMVA                                                               *
 * Root Macro: TMVAClassification                                                 *
 *                                                                                *
 * This macro provides examples for the training and testing of the               *
 * TMVA classifiers.                                                              *
 *                                                                                *
 * As input data is used a toy-MC sample consisting of four Gaussian-distributed  *
 * and linearly correlated input variables.                                       *
 *                                                                                *
 * The methods to be used can be switched on and off by means of booleans, or     *
 * via the prompt command, for example:                                           *
 *                                                                                *
 *    root -l ./TMVAClassification.C\(\"Fisher,Likelihood\"\)                     *
 *                                                                                *
 * (note that the backslashes are mandatory)                                      *
 * If no method given, a default set of classifiers is used.                      *
 *                                                                                *
 * The output file "TMVA.root" can be analysed with the use of dedicated          *
 * macros (simply say: root -l <macro.C>), which can be conveniently              *
 * invoked through a GUI that will appear at the end of the run of this macro.    *
 * Launch the GUI via the command:                                                *
 *                                                                                *
 *    root -l ./TMVAGui.C                                                         *
 *                                                                                *
 * You can also compile and run the example with the following commands           *
 *                                                                                *
 *    make                                                                        *
 *    ./TMVAClassification <Methods>                                              *
 *                                                                                *
 * where: <Methods> = "method1 method2"                                           *
 *        are the TMVA classifier names                                           *
 *                                                                                *
 * example:                                                                       *
 *    ./TMVAClassification Fisher LikelihoodPCA BDT                               *
 *                                                                                *
 * If no method given, a default set is of classifiers is used                    *
 **********************************************************************************/

#include <cstdlib>
#include <iostream>
#include <map>
#include <string>

#include "TChain.h"
#include "TFile.h"
#include "TTree.h"
#include "TString.h"
#include "TObjString.h"
#include "TSystem.h"
#include "TROOT.h"

#include "TMVA/Factory.h"
#include "TMVA/Tools.h"
#include "TMVA/TMVAGui.h"

int TMVAClassification()
{
   TMVA::Tools::Instance();


   // Select methods (don't look at this code - not of interest)

   // --------------------------------------------------------------------------------------------------

   // --- Here the preparation phase begins

   // Create a ROOT output file where TMVA will store ntuples, histograms, etc.
   TString outfileName( "CutTraining.root" );
   TFile* outputFile = TFile::Open( outfileName, "RECREATE" );

   TMVA::Factory *factory = new TMVA::Factory( "Weight_CutTraining", outputFile,
                                               "V:Color:DrawProgressBar:AnalysisType=Classification" );



     //factory->AddVariable("Bs_DIRA_OWNPV"                                ,'F');
     //factory->AddVariable("Bs_ENDVERTEX_CHI2"                            ,'F');
     //factory->AddVariable("kaon_m_PT",                                    'F');
     //factory->AddVariable("muon_p_PT",                                    'F');
     //factory->AddVariable("Bs_PT",                                        'F', 0, 25000);
     //factory->AddVariable("kaon_m_0.50_IT",                               'F');
     //factory->AddVariable("kaon_m_0.50_cc_deltaEta",                      'F');
     //factory->AddVariable("kaon_m_1.00_nc_asy_PT",                        'F');
     //factory->AddVariable("kaon_m_0.50_nc_IT",                            'F');
     //factory->AddVariable("kaon_m_0.50_cc_IT",                            'F');
     //factory->AddVariable("kaon_m_MasshPi0",                              'F');
     //factory->AddVariable("min(muon_p_ConeIso, kaon_m_ConeIso)",          'F');
     factory->AddVariable("max(kaon_m_IsoSumBDT, muon_p_IsoSumBDT)",      'F');
     factory->AddVariable("min(kaon_m_IsoMinBDT, muon_p_IsoMinBDT)",      'F');
     factory->AddVariable("abs(kaon_m_IsoMinBDT - muon_p_IsoMinBDT)",          'F');
     //factory->AddVariable("muon_p_PAIR_M",                                'F', 0, 6000);
     //factory->AddVariable("kaon_m_PAIR_M",                                'F', 0, 5000);
     //factory->AddVariable("Bs_cosTheta1_star-Bs_cosTheta2_star",          'F');
     //factory->AddVariable("muon_p_0.50_cc_asy_P",                         'F');
     //factory->AddVariable("Bs_FD_S",                                      'F');




   TFile* f_signal_Up     = TFile::Open("DataForTraining/SignalForTraining_Up.root", "READ");
   TFile* f_signal_Dn     = TFile::Open("DataForTraining/SignalForTraining_Dn.root", "READ");
   TFile* f_background_Up = TFile::Open("DataForTraining/ChargedCocktailBGForTraining_Up.root", "READ");
   TFile* f_background_Dn = TFile::Open("DataForTraining/ChargedCocktailBGForTraining_Dn.root", "READ");

   TTree *signal_Up     = (TTree*)f_signal_Up    ->Get("TrainingTree");
   TTree *signal_Dn     = (TTree*)f_signal_Dn    ->Get("TrainingTree");
   TTree *background_Up = (TTree*)f_background_Up->Get("TrainingTree");
   TTree *background_Dn = (TTree*)f_background_Dn->Get("TrainingTree");

   factory->AddSignalTree    ( signal_Up     );
   factory->AddSignalTree    ( signal_Dn     );
   factory->AddBackgroundTree( background_Up );
   factory->AddBackgroundTree( background_Dn );


   factory->SetBackgroundWeightExpression( "RW_BDT" );
   factory->SetSignalWeightExpression(     "RW_BDT" );


   factory->PrepareTrainingAndTestTree( "", //NoCut
                                        "SplitMode=Random:NormMode=EqualNumEvents:!V" );

   factory->BookMethod( TMVA::Types::kCuts, "Cuts",
                           "FitMethod=SA"\
              ":CutRangeMin[0]= -0.84750 :CutRangeMax[0]=   0.020523" \
              ":CutRangeMin[1]= -0.99845 :CutRangeMax[1]=    0.99984" \
              ":CutRangeMin[2]=  -1.7873 :CutRangeMax[2]=     1.6891" \
   );

/*
   factory->BookMethod( TMVA::Types::kCuts, "Cuts2",
                           "!H:!V:FitMethod=MC:EffSel:SampleSize=200000:VarProp=FSmart"
   );
*/
   factory->TrainAllMethods();

   // ---- Evaluate all MVAs using the set of test events
   factory->TestAllMethods();

   // ----- Evaluate and compare performance of all configured MVAs
   factory->EvaluateAllMethods();

   // --------------------------------------------------------------

   // Save the output
   outputFile->Close();

   std::cout << "==> Wrote root file: " << outputFile->GetName() << std::endl;
   std::cout << "==> TMVAClassification is done!" << std::endl;

   delete factory;

   // Launch the GUI for the root macros
   if (!gROOT->IsBatch()) TMVA::TMVAGui( outfileName );

   return 0;
}

int main( int argc, char** argv )
{
    TMVAClassification();



  return EXIT_SUCCESS;
}
