# Training BDTs

### To Train BDTs
```
make 
./TMVAClassification_606
```

### BDTs weights are saved in eos.
To Update weights used in the analysis copy weight files to:
```
/eos/lhcb/wg/semileptonic/Bs2KmunuAna/TMVAWeights/Iwan/Weights/
```


### To Classify NTuples
Classification is done with the trimmer:  

Open `Trimmer/reducer.cc` and edit the section on BDTs. Paul wrote a nice library to 
automatically read variables and apply the classification.  

