#include <iostream>
#include <fstream>
#include <string>

#include "Bs_Ds_Fit_Conf.h"
	
Bs_Ds_Fit_Conf::Bs_Ds_Fit_Conf(std::string file_name){
	
	//Constructor which reads in the values from a specified configuration file
	Parse_File( file_name );
	
	
}

void Bs_Ds_Fit_Conf::ParseLine( std::string line){
	
	bool b_dbl = false;
	bool b_int = false;
	bool b_str = false;
	bool b_bln = false;
	
	
	// Skip the commented or empty lines
	
	if ( line.size() == 0 )
		return;
	if ( line[0] == '#' )
		return;
	
	
	//Find the type of the configuration option.
	std::string config_type = line.substr(0, 3);
	
	if ( config_type == "dbl")
		b_dbl = true;
	else if ( config_type == "int")
		b_int = true;
	else if ( config_type == "str")
		b_str = true;
	else if ( config_type == "bln")
		b_bln = true;
	else{
		std::cout << "Parsing the variable type messed up. Type: " << config_type << " Is Unknown!" << std::endl;
		return;
	}
	
	// Now parse the line:
	// Find the start and end of the variable names;
	
	int var_start = 4;
	int var_end = 0;
	int val_start = 0;
	int val_end = line.size();
	
	bool appendstr = false;
	for ( int it_char = var_start; it_char < val_end; it_char++ ){
		//std::cout << "|" <<  line.substr(it_char, 3) << "|" << std::endl;
		if ( line.substr(it_char, 3)  == " = " ){
			var_end = it_char - 1;
			val_start = it_char + 3;
			break;
		}
		else if ( line.substr(it_char, 3)  == " +=" and b_str){
			var_end = it_char - 1;
			val_start = it_char + 4;
			appendstr = true;
			break;
		}
	}
	
	if ( var_end == 0 or val_start == 0){
		std::cout << "Parsing messed up! Skipping line: " << line << std::endl;
		return;
	}
	
	std::string var_name = line.substr(var_start, var_end - var_start + 1);
	std::string var_val  = line.substr(val_start, val_end - val_start + 1);
	if ( b_bln ){
		if ( var_val == "true" )
			blns[var_name] == true;
		else
			blns[var_name] == false;
	}
	if ( b_dbl )
		dbls[var_name] = std::stod(var_val);
	if ( b_int )
		ints[var_name] = std::stoi(var_val);
	if ( b_str ){
		
		//If the string starts with File append the folder path
		//if ( var_name.substr(0, 4) == "File" )
		//	var_val = strs["OutputFolder"] + var_val;
		
		if ( appendstr )
			var_val = strs[var_name] + var_val;

		strs[var_name] = var_val;
		
		
	}
	std::cout << var_name << " " << var_val << std::endl;
}




void Bs_Ds_Fit_Conf::Parse_File( std::string file_name){
	
	std::string config_line;
	std::ifstream config_file ( file_name );
	
	
	if ( config_file.is_open() ){
		
		while ( getline (config_file, config_line) ){
			ParseLine(config_line);
		}
		
	}
	
	
	}

void Bs_Ds_Fit_Conf::SetPar( std::string var, bool val){
	
	blns[var] = val;
	
	}
void Bs_Ds_Fit_Conf::SetPar( std::string var, int val){
	
	ints[var] = val;
	
	}
void Bs_Ds_Fit_Conf::SetPar( std::string var, double val){
	
	dbls[var] = val;
	
	}
void Bs_Ds_Fit_Conf::SetPar( std::string var, std::string val){
	
	strs[var] = val;
	
	}

bool Bs_Ds_Fit_Conf::GetPar_b( std::string par ){
	return blns[ par ];
	}

int Bs_Ds_Fit_Conf::GetPar_i( std::string par ){
	return ints[ par ];
	}

double Bs_Ds_Fit_Conf::GetPar_d( std::string par ){
	return dbls[ par ];
	}

std::string Bs_Ds_Fit_Conf::GetPar_s( std::string par ){
	return strs[ par ];
	}
