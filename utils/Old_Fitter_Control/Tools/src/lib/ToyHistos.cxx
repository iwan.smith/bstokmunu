#include <iostream>
#include <random>

#include "TFile.h"
#include "TTree.h"
#include "TH1F.h"
#include "TCanvas.h"
#include <vector>
#include <string>
void ToyHistos( const char* in_file,const char* out_file,
				std::vector<std::string> hist_names, std::vector<double> nev){
	
	std::vector<TH1F*> Source_Histos;
	int n_hist = hist_names.size();

	if(hist_names.size() == 0)
		return;
	
	//Read in histograms
	TFile* f_in = TFile::Open( in_file, "READ");
	
	for( std::string const& histogram_name: hist_names){
		Source_Histos.push_back( (TH1F* )f_in->Get( histogram_name.c_str() )->Clone() );
		Source_Histos.back()->SetDirectory(0);
	}
	
	TH1F* h_data = (TH1F*)Source_Histos.at(0)->Clone();
	h_data->SetDirectory(0);
	h_data->Reset();
	h_data->SetName("Data");
	h_data->SetTitle("Data Histogram");
	
	
	std::random_device rd;
	std::mt19937 gen(rd());

	for( unsigned int it = 0; it <n_hist; it++ ){
		
		std::cout << hist_names.at(it) << "\t" << nev.at(it) << std::endl;
		
		//if ( nev.at(it) > 0 ){		//Generate a pseudodata distribution
			//for (int ev = 0; ev < nev.at(it); ev++){
		*h_data = *h_data + *Source_Histos.at(it)*nev.at(it);
			//}
		//}
		//else if ( nev.at(it) == -1 )
		//	*h_data = *h_data + *Source_Histos.at(it);
	
		for (int bin = 1; bin <=Source_Histos.at(it)->GetSize()-2; bin++){	// Wiggle the source Histograms within their errors
			std::normal_distribution<double> Distributon(Source_Histos.at(it)->GetBinContent(bin), Source_Histos.at(it)->GetBinError(bin));
			double toy_val = Distributon(gen);
			while ( toy_val <= 0 )
				toy_val = Distributon(gen);
			Source_Histos.at(it)->SetBinContent(bin, toy_val);
			
		}
	
	}
	
	Source_Histos.push_back( h_data );
	
	TFile* f_out = TFile::Open(out_file, "RECREATE");
	f_out->cd();
	
	for ( TH1F* const& hist: Source_Histos)
		hist->Write();


	f_in->Close();
	f_out->Close();
	
	
}


void GetErrComparison( const char* file_name = "Source_Histograms.root" ){
	
	
	TFile* hist_file = TFile::Open(file_name);
	
	TH1F* h_data = (TH1F*)hist_file->Get("Data");
	
	
	
	TH1F* h_errcomp = (TH1F*)h_data->Clone();
	h_errcomp->Sumw2(false);
	for (int bin = 1; bin <=40; bin++){
		
		h_errcomp->SetBinContent( bin, h_data->GetBinError(bin) / sqrt(h_data->GetBinContent(bin) ) );
		
	}
	
	TCanvas* c1 = new TCanvas("c1", "c1", 1600, 1600);
	
	h_errcomp->Draw();
	
	
	
	
}
