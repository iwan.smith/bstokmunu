#ifndef BS_DS_CONF
#define BS_DS_CONF 1

#include <iostream>
#include <vector>
#include <map>

class Bs_Ds_Fit_Conf {
	
	public:
		
		Bs_Ds_Fit_Conf(std::string);
		
		void ParseLine( std::string );
		
		bool        GetPar_b( std::string );
		int         GetPar_i( std::string );
		double      GetPar_d( std::string );
		std::string GetPar_s( std::string );
		
		void SetPar( std::string, bool );
		void SetPar( std::string, int );
		void SetPar( std::string, double );
		void SetPar( std::string, std::string );
		
		
	private:

		void Parse_File( std::string);
		
		std::map<std::string, bool> blns;
		std::map<std::string, int> ints;
		std::map<std::string, double> dbls;
		std::map<std::string, std::string> strs;
	
};

#endif
