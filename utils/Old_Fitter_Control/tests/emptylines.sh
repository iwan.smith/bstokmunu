#!/bin/bash
code=0
for v in `find ../ -name *.conf` ; do
  echo "checking $v for whitespace-only lines"
  \grep -e "^[[:space:]]\+$" $v
  if [ $? -eq 0 ]
  then
    code=1
    echo "FAILED TEST!"
    echo "whitespace only lines in" $v
  else
    echo ".......... -> passed test"
  fi
done
exit $code
