#include <string>
#include <iostream>
#include <fstream>
#include <utility>
#include <map>
#include <vector>
#include "HistFactory_BsDs.h"

#include "TPaveText.h"
#include "ToyHistos.h"
#include "TF1.h"
#include "TFile.h"
#include "TCanvas.h"
#include "TH1F.h"
#include "TH2F.h"
#include <random>
#include "TStyle.h"
#include "TGaxis.h"
#include "TTree.h"
#include "THStack.h"

#include "Bs2Ds_BiasTest.h"


std::vector<std::string> splitstring(std::string work, char delim = ' ') {
    std::vector<std::string> flds;
    std::string buf = "";
    int i = 0;
    while (i < work.length()) {
        if (work[i] != delim)
            buf += work[i];
        else if (buf.length() > 0) {
            flds.push_back(buf);
            buf = "";
        }
        i++;
    }
    if (!buf.empty())
        flds.push_back(buf);
    return flds;
}


void Bs2Ds_BiasTest(
		const char* Toy_results, const char* Real_Data, const char* Toy_Data, std::string FitterConfFile, std::string save_folder, int n_toys,
		std::map<std::string, std::pair<double, double>> result_tuple
		){


	std::random_device rd;
	std::mt19937 gen(rd());


	std::vector<std::string> HistNames;
	std::vector<double> HistYields;
	std::vector<std::string> vars; // Variables which will be saved into the Toy results NTuple which are not yields. Correlations, EDM etc.
	
	std::vector<double> nev_fit(100);
	std::vector<double> nev_fit_err(100);
	std::vector<double> fit_pull(100);
	std::vector<double> fit_res(100);
	
	std::vector< TBranch* > branch_in(100);
	std::vector< TBranch* > branch_fit(100);
	std::vector< TBranch* > branch_fit_err(100);

	std::vector< std::normal_distribution<double> > Distributions;

	std::vector< double > var_vals;
	std::vector< TBranch* > branch_var (100);
	

	//TFile* f_hist = TFile::Open(Real_Data, "READ");
	
	std::string line;
	std::ifstream myfile (FitterConfFile);
	std::string POI = "";
	
	if ( !myfile.is_open() )
		return;
	while ( getline (myfile,line) ){
		//std::cout << line << std::endl;
		
		if ( line.length() == 0 ) continue;
		if ( line[0] == '#' ) continue;
		
		
		//Parse The config File
		std::vector<std::string> line_elements = splitstring( line, ' ');
		std::string HistName = line_elements.at(0);
		//Theese two lines can be ignored. They control grouping of histograms, and are not needed for toy generation
		getline (myfile,line);
		getline (myfile,line);
		
		
		//Start loading the data
		
		//TH1F* Histogram = (TH1F*)f_hist->Get( HistName.c_str() )->Clone();
		
		HistNames.push_back(HistName);
		HistYields.push_back( result_tuple[HistName].first );
	}
	myfile.close();
	
	
	
	for( auto it: result_tuple){
		
		std::string var_name = it.first;
		double var_val = it.second.first;
		
		if ( std::find(HistNames.begin(), HistNames.end(), var_name) != HistNames.end() ) // If the var name is in tne hist names skip
			continue;
		
		vars.push_back( var_name );
		var_vals.push_back( var_val );
		
		
	}
	
	
	

	int toy_number = -1000;

	TFile* f_Tree = TFile::Open(Toy_results, "RECREATE");
	TTree* resultsTree = new TTree("Toy_results", "Toy Results");

	for ( int it = 0; it < vars.size(); it++ ){ // Fill initial vectors
		branch_var.push_back( resultsTree->Branch( vars.at(it).c_str(), &var_vals.at(it),      ( vars.at(it)+"_in/D").c_str() ) );
	}

	for ( int it = 0; it < HistNames.size(); it++ ){ // Fill initial vectors
		
		nev_fit.at(it) = -1000;
		nev_fit_err.at(it) = -1000;
		fit_pull.at(it) = -1000;
		fit_res.at(it) = -1000;

		branch_in.push_back( resultsTree->Branch(( HistNames.at(it) + "_in"      ).c_str(), &HistYields.at(it),  ( HistNames.at(it)+"_in/D").c_str() ) );
		branch_in.push_back( resultsTree->Branch(( HistNames.at(it) + "_fit"     ).c_str(), &nev_fit.at(it),     ( HistNames.at(it)+"_fit/D").c_str() ) );
		branch_in.push_back( resultsTree->Branch(( HistNames.at(it) + "_fit_err" ).c_str(), &nev_fit_err.at(it), ( HistNames.at(it)+"_fit_err/D").c_str() ) );
		branch_in.push_back( resultsTree->Branch(( HistNames.at(it) + "_pull"    ).c_str(), &fit_pull.at(it),    ( HistNames.at(it)+"_pull/D").c_str() ) );
		branch_in.push_back( resultsTree->Branch(( HistNames.at(it) + "_res"     ).c_str(), &fit_res.at(it),     ( HistNames.at(it)+"_res/D").c_str() ) );

		Distributions.push_back( std::normal_distribution<double>( HistYields.at(it), sqrt(HistYields.at(it)) ) );
	}



	resultsTree->Branch("Toy_Number", &toy_number, "Toy_Number/I");
	
	for (int toy_num = 0; toy_num < n_toys; toy_num++){
		
		for ( int it = 0; it < HistNames.size(); it++ ){ //Replace input values with random point from normal distribution
			std::cout << HistYields.at(it) << "\t";
			HistYields.at(it) = Distributions.at(it)(gen); // We don't care about replacing this number since the value is now stored in the prng
			while ( HistYields.at(it) < 0)
				HistYields.at(it) = Distributions.at(it)(gen);
				
			std::cout << HistYields.at(it) << std::endl;
		}
			
		
		std::cout << "About to start producing toy histograms" << std::endl;
		ToyHistos(Real_Data, Toy_Data, HistNames, HistYields);
		std::cout << "Toy Histograms made" << std::endl;

//HistFactory_BsDs(const char* file_name, std::string FitterConfFile, bool saveplots, std::string save_folder) {
		std::map<std::string, std::pair<double, double>> fit_results = HistFactory_BsDs(Toy_Data, FitterConfFile, false, save_folder);
		
		for ( int it = 0; it < HistNames.size(); it++ ){ //Fill vectors with the results from the fit
			nev_fit.at(it)     = fit_results[ HistNames.at(it) ].first;
			nev_fit_err.at(it) = fit_results[ HistNames.at(it) ].second;
			fit_pull.at(it)    = (fit_results[ HistNames.at(it) ].first-HistYields.at(it) ) / fit_results[ HistNames.at(it) ].second ;
			fit_res.at(it)     = fit_results[ HistNames.at(it) ].first-HistYields.at(it);
		}
		
		for ( int it = 0; it < vars.size(); it++ ){ //Fill vectors with the results from the fit
			var_vals.at(it) = fit_results[ vars.at(it) ].first;
		}

		toy_number=toy_num;
		
		resultsTree->Fill();
	}
	
	
	f_Tree->cd();
	resultsTree->Write();
	f_Tree->Close();
	
}

void Bs2Ds_BiasTest( Bs_Ds_Fit_Conf Configuration, std::map<std::string, std::pair<double, double>> result_tuple){
	
	std::string save_folder    = Configuration.GetPar_s("OutputFolder");
	std::string Toy_results    = save_folder + Configuration.GetPar_s( "File_Toy_Results_ROOT" );
	std::string Real_Data      = save_folder + Configuration.GetPar_s( "File_Histogram_Name" );
	std::string Toy_Data       = save_folder + Configuration.GetPar_s( "File_Histogram_Toy" );
	std::string FitterConfFile = Configuration.GetPar_s("Fitter_Configuration");
	
	int ntoys = Configuration.GetPar_i("Ntoys");
	
	Bs2Ds_BiasTest( 
	Toy_results.c_str(), Real_Data.c_str(), Toy_Data.c_str(), FitterConfFile, save_folder, ntoys,
	result_tuple
	);

	
	//Fix this function call
	//Bs2Ds_BiasTest( Toy_results.c_str(), Real_Data.c_str(), Toy_Data.c_str(), ntoys,
	//hist_names, HistNames, HistYields, vars,
	//*FitterFunc);
	
	
}



void PrintToyResults( const char* Toy_results, std::string save_folder ){
	
	gStyle->SetOptStat(0);
	
	
	TCanvas* c1 = new TCanvas("c1", "c1", 1600, 1600);
	c1->Print( ( save_folder + "Toy_Plots.pdf[" ).c_str() ); //Open the pdf for writing
	
	TFile* f_ToyResults = TFile::Open(Toy_results, "READ");
	TTree* ToyResults = (TTree*)f_ToyResults->Get("Toy_results");
	
	TObjArray* ListOfBranches = ToyResults->GetListOfBranches();
	
	int nBranch = ListOfBranches->GetEntries();
	
	
	
	// Iterate through the branches, find the pulls, and plot the pulls
	
	for (int it = 0; it < nBranch; it++){
		TObject* Branch = ListOfBranches->At(it);
		std::string BranchName = Branch->GetName();
		if ( BranchName.substr( std::max( 0, int(BranchName.size() - 5)), 5 ) != "_pull" )
			continue;
		
		std::cout << BranchName << std::endl;
		
		c1->Clear();
		
		TH1F* h_Pull = new TH1F("h_Pull", BranchName.c_str(), 50, -3.5, 3.5 );
		h_Pull->Sumw2();
		ToyResults->Draw( ( BranchName + ">>h_Pull").c_str() );
		
		if ( h_Pull->Integral() == 0 ){
			delete h_Pull;
			continue;
		}

		
		h_Pull->GetXaxis()->SetTitle( "Pulls" );
		h_Pull->Draw();
		
		TF1* fit_func = new TF1("FitFunc", "gaus(0)");
		
		h_Pull->Fit(fit_func);
		
		TPaveText *pt = new TPaveText(.6,0.8,0.89,0.89, "NDC,NB");
		pt->SetFillColor(0);
		pt->AddText( ( "Mean: " +  std::to_string(fit_func->GetParameter("Mean"))  + " #pm " + std::to_string(fit_func->GetParError(1)  )).c_str() );
		pt->AddText( ( "Sigma: " + std::to_string(fit_func->GetParameter("Sigma")) + " #pm " + std::to_string(fit_func->GetParError(2) )).c_str() );
		pt->Draw();
		
		
		
		c1->Print( ( save_folder + "Toy_Plots.pdf" ).c_str() );
		
		delete h_Pull;
		delete pt;
		delete fit_func;
		
		
	}
	
	
	//Create 2D plots of Pull_1 vs Pull_2 so we can see correlations.
	for (int it = 0; it < nBranch; it++){
		TObject* Branch = ListOfBranches->At(it);
		std::string BranchName = Branch->GetName();
		if ( BranchName.substr( std::max( 0, int(BranchName.size() - 5)), 5 ) != "_pull" )
			continue;
		for (int it2 = 0; it2 < nBranch; it2++){
			TObject* Branch2 = ListOfBranches->At(it2);
			std::string BranchName2 = Branch2->GetName();
			if ( BranchName2.substr( std::max( 0, int(BranchName2.size() - 5)), 5 ) != "_pull" )
				continue;
			if ( BranchName == BranchName2 )
				break;
			
			c1->Clear();
			
			TH2F* h_Pull2 = new TH2F("h_Pull", "", 50, -3.5, 3.5, 50, -3.5, 3.5 );
			
			
			ToyResults->Draw( ( BranchName + ":" + BranchName2 + ">>h_Pull").c_str() );
			
			if ( h_Pull2->Integral() == 0 ){
				delete h_Pull2;
				continue;
			}
			
			h_Pull2->GetXaxis()->SetTitle( BranchName2.c_str() );
			h_Pull2->GetYaxis()->SetTitle( BranchName.c_str() );
			
			h_Pull2->Draw("COLZ");
			
			TPaveText *pt = new TPaveText(.6,0.85,0.89,0.89, "NDC,NB");
			pt->SetFillColor(0);
			pt->AddText( ( "CorrelationFactor: " +  std::to_string( h_Pull2->GetCorrelationFactor() )).c_str() );
			pt->Draw();
			
			c1->Print( ( save_folder + "Toy_Plots.pdf" ).c_str() );
			
			delete pt;
			delete h_Pull2;
		}
	}
			
			
			

	
	
	c1->Clear();
	c1->Print( ( save_folder + "Toy_Plots.pdf]" ).c_str() ); //Close the pdf for writing
	
	
}


void PrintToyResults( Bs_Ds_Fit_Conf Configuration ){
	
	std::string save_folder    = Configuration.GetPar_s("OutputFolder");
	std::string Toy_results    = save_folder + Configuration.GetPar_s( "File_Toy_Results_ROOT" );
	
	
	
	PrintToyResults( Toy_results.c_str(), save_folder );
	
	
}



/*
void SaveToyPlots_Shape(std::string toy_file, std::string plot_file){
	gStyle->SetOptStat(0);

	TFile* f_toy = TFile::Open( toy_file.c_str() );
	TTree* T_Toy = (TTree*)f_toy->Get("Toy_results");
	
	//Start Plotting correlations.
	
	TCanvas* c1 = new TCanvas("c1", "c1", 1600, 1600);
	
	TH2F* h_Corr;
	
	h_Corr = new TH2F("h_CORR", "", 50, -2, 2, 50, -2, 2);
	T_Toy->Draw("Ds_Yield_pull:DsStar_Yield_pull>>h_CORR");
	h_Corr->GetXaxis()->SetTitle("D_{s}* Pulls");
	h_Corr->GetYaxis()->SetTitle("D_{s} Pulls");
	h_Corr->Draw("BOX");
	c1->Print((plot_file+"(").c_str() );
	delete h_Corr;
	
	h_Corr = new TH2F("h_CORR", "", 50, -2, 2, 50, -2, 2);
	T_Toy->Draw("Ds_Yield_pull:DsStar0_Yield_pull>>h_CORR");
	h_Corr->GetXaxis()->SetTitle("D_{s0,1}* Pulls");
	h_Corr->GetYaxis()->SetTitle("D_{s} Pulls");
	h_Corr->Draw("BOX");
	c1->Print( plot_file.c_str() );
	delete h_Corr;
	
	h_Corr = new TH2F("h_CORR", "", 50, -2, 2, 50, -2, 2);
	T_Toy->Draw("Ds_Yield_pull:BG1_Yield_pull>>h_CORR");
	h_Corr->GetXaxis()->SetTitle("Background 1 Pulls");
	h_Corr->GetYaxis()->SetTitle("D_{s} Pulls");
	h_Corr->Draw("BOX");
	c1->Print( plot_file.c_str() );
	delete h_Corr;


	h_Corr = new TH2F("h_CORR", "", 50, -2, 2, 50, -2, 2);
	T_Toy->Draw("Ds_Yield_pull:BG2_Yield_pull>>h_CORR");
	h_Corr->GetXaxis()->SetTitle("Background 2 Pulls");
	h_Corr->GetYaxis()->SetTitle("D_{s} Pulls");
	h_Corr->Draw("BOX");
	c1->Print( plot_file.c_str() );
	delete h_Corr;
	
	std::vector<std::string> Pulls;
	Pulls.push_back("Ds_Yield_pull");
	Pulls.push_back("DsStar_Yield_pull");
	Pulls.push_back("DsStar0_Yield_pull");
	Pulls.push_back("BG1_Yield_pull");
	Pulls.push_back("BG2_Yield_pull");
	
	for ( auto& Pull: Pulls ){
		
		TH1F* h_pull = new TH1F("h_Pull", "", 100, -2, 2);
		T_Toy->Draw( (Pull + ">>h_Pull").c_str() );
		h_pull->GetXaxis()->SetTitle( Pull.c_str() );
		h_pull->Draw();
		
		TF1* fit_func = new TF1("FitFunc", "gaus(0)");
		
		h_pull->Fit(fit_func);
		
		TPaveText *pt = new TPaveText(.6,0.8,0.89,0.89, "NDC,NB");
		pt->SetFillColor(0);
		pt->AddText( ( "Mean: " +  std::to_string(fit_func->GetParameter("Mean"))  + " #pm " + std::to_string(fit_func->GetParError(1)  )).c_str() );
		pt->AddText( ( "Sigma: " + std::to_string(fit_func->GetParameter("Sigma")) + " #pm " + std::to_string(fit_func->GetParError(2) )).c_str() );
		pt->Draw();
		c1->Print( plot_file.c_str() );
		
		delete fit_func;
		delete h_pull;
		delete pt;
		

	}
	
	std::vector<std::string> Corrs;
	Corrs.push_back( "Corr_Ds_DsStar" );
	Corrs.push_back( "Corr_Ds_DsStar0" );
	Corrs.push_back( "Corr_Ds_BG1" );
	Corrs.push_back( "Corr_Ds_BG2" );
	
	c1->Clear();
	c1->Divide(2,2);
	TH1F* corr = new TH1F("h_Corr", "", 100, -1, 1);
	for(int it = 0; it < 4; it++){
		c1->cd(it+1);
		
		T_Toy->Draw( (Corrs[it] + ">>h_Corr").c_str() );
		corr->GetXaxis()->SetTitle( Corrs[it].c_str() );
		corr->DrawClone();
	}
	
	c1->Print( plot_file.c_str() );
	
	TH1F* h_CovQual = new TH1F("h_CovQual", "", 400, 0, 4);
	T_Toy->Draw( "CovQual>>h_CovQual" );
	
	c1->Clear();
	c1->SetLogy();
	h_CovQual->Draw();
	h_CovQual->GetXaxis()->SetTitle("Covariance Quality");
	c1->Print( plot_file.c_str() );

		
	TH1F* h_EDM = new TH1F("h_EDM", "", 100, 0, 0.001);
	T_Toy->Draw( "EDM>>h_EDM" );
	
	c1->Clear();
	c1->SetLogy(false);
	TGaxis::SetMaxDigits(3); 

	h_EDM->Draw();
	h_EDM->GetXaxis()->SetTitle("EDM");
	c1->Print( (plot_file + ")").c_str() );

	
	
	}

void SaveToyPlots_Shape(Bs_Ds_Fit_Conf Configuration){
	
	std::string Toy_results = Configuration.GetPar_s("OutputFolder") + Configuration.GetPar_s( "File_Toy_Results_ROOT_Shape" );
	std::string file_plots = Configuration.GetPar_s("OutputFolder") + "Toy_Plots_Shape.pdf";
	
	SaveToyPlots_Shape(Toy_results, file_plots);
	
}


void SaveToyPlots_Phys(std::string toy_file, std::string plot_file){
	gStyle->SetOptStat(0);

	TFile* f_toy = TFile::Open( toy_file.c_str() );
	TTree* T_Toy = (TTree*)f_toy->Get("Toy_results");
	
	//Start Plotting correlations.
	
	TCanvas* c1 = new TCanvas("c1", "c1", 1600, 1600);
	
	TH2F* h_Corr;
	
	h_Corr = new TH2F("h_CORR", "", 50, -2, 2, 50, -2, 2);
	T_Toy->Draw("Ds_Yield_pull:DsStar_Yield_pull>>h_CORR");
	h_Corr->GetXaxis()->SetTitle("D_{s}* Pulls");
	h_Corr->GetYaxis()->SetTitle("D_{s} Pulls");
	h_Corr->Draw("BOX");
	c1->Print((plot_file+"(").c_str() );
	delete h_Corr;
	
	h_Corr = new TH2F("h_CORR", "", 50, -2, 2, 50, -2, 2);
	T_Toy->Draw("Ds_Yield_pull:DsStar0_Yield_pull>>h_CORR");
	h_Corr->GetXaxis()->SetTitle("D_{s0,1}* Pulls");
	h_Corr->GetYaxis()->SetTitle("D_{s} Pulls");
	h_Corr->Draw("BOX");
	c1->Print( plot_file.c_str() );
	delete h_Corr;
	
	h_Corr = new TH2F("h_CORR", "", 50, -2, 2, 50, -2, 2);
	T_Toy->Draw("Ds_Yield_pull:DsTau_Yield_pull>>h_CORR");
	h_Corr->GetXaxis()->SetTitle("D_{s} #tau Pulls");
	h_Corr->GetYaxis()->SetTitle("D_{s} Pulls");
	h_Corr->Draw("BOX");
	c1->Print( plot_file.c_str() );
	delete h_Corr;


	h_Corr = new TH2F("h_CORR", "", 50, -2, 2, 50, -2, 2);
	T_Toy->Draw("Ds_Yield_pull:DD_Yield_pull>>h_CORR");
	h_Corr->GetXaxis()->SetTitle("D_{q} D_{s} Pulls");
	h_Corr->GetYaxis()->SetTitle("D_{s} Pulls");
	h_Corr->Draw("BOX");
	c1->Print( plot_file.c_str() );
	delete h_Corr;
	
	std::vector<std::string> Pulls;
	Pulls.push_back("Ds_Yield_pull");
	Pulls.push_back("DsStar_Yield_pull");
	Pulls.push_back("DsStar0_Yield_pull");
	Pulls.push_back("DsTau_Yield_pull");
	Pulls.push_back("DD_Yield_pull");
	
	for ( auto& Pull: Pulls ){
		
		TH1F* h_pull = new TH1F("h_Pull", "", 100, -2, 2);
		T_Toy->Draw( (Pull + ">>h_Pull").c_str() );
		h_pull->GetXaxis()->SetTitle( Pull.c_str() );
		h_pull->Draw();
		
		TF1* fit_func = new TF1("FitFunc", "gaus(0)");
		
		h_pull->Fit(fit_func);
		
		TPaveText *pt = new TPaveText(.6,0.8,0.89,0.89, "NDC,NB");
		pt->SetFillColor(0);
		pt->AddText( ( "Mean: " +  std::to_string(fit_func->GetParameter("Mean"))  + " #pm " + std::to_string(fit_func->GetParError(1)  )).c_str() );
		pt->AddText( ( "Sigma: " + std::to_string(fit_func->GetParameter("Sigma")) + " #pm " + std::to_string(fit_func->GetParError(2) )).c_str() );
		pt->Draw();
		c1->Print( plot_file.c_str() );
		
		delete fit_func;
		delete h_pull;
		delete pt;
		

	}
	
	std::vector<std::string> Corrs;
	Corrs.push_back( "Corr_Ds_DsStar" );
	Corrs.push_back( "Corr_Ds_DsStar0" );
	Corrs.push_back( "Corr_Ds_DsTau" );
	Corrs.push_back( "Corr_Ds_DD" );
	
	c1->Clear();
	c1->Divide(2,2);
	TH1F* corr = new TH1F("h_Corr", "", 100, -1, 1);
	for(int it = 0; it < 4; it++){
		c1->cd(it+1);
		
		T_Toy->Draw( (Corrs[it] + ">>h_Corr").c_str() );
		corr->GetXaxis()->SetTitle( Corrs[it].c_str() );
		corr->DrawClone();
	}
	
	c1->Print( plot_file.c_str() );
	
	TH1F* h_CovQual = new TH1F("h_CovQual", "", 400, 0, 4);
	T_Toy->Draw( "CovQual>>h_CovQual" );
	
	c1->Clear();
	c1->SetLogy();
	h_CovQual->Draw();
	h_CovQual->GetXaxis()->SetTitle("Covariance Quality");
	c1->Print( plot_file.c_str() );

	
	
	
	TH1F* h_EDM = new TH1F("h_EDM", "", 100, 0, 0.001);
	T_Toy->Draw( "EDM>>h_EDM" );
	
	c1->Clear();
	c1->SetLogy(false);
	TGaxis::SetMaxDigits(3); 

	h_EDM->Draw();
	h_EDM->GetXaxis()->SetTitle("EDM");
	c1->Print( (plot_file + ")").c_str() );

	
	
	}

void SaveToyPlots_Phys(Bs_Ds_Fit_Conf Configuration){
	
	std::string Toy_results = Configuration.GetPar_s("OutputFolder") + Configuration.GetPar_s( "File_Toy_Results_ROOT_Phys" );
	std::string file_plots = Configuration.GetPar_s("OutputFolder") + "Toy_Plots_Phys.pdf";
	
	SaveToyPlots_Phys(Toy_results, file_plots);
	
}
*/
