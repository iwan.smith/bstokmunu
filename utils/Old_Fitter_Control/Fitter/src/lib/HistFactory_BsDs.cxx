#include <iostream>
#include <fstream>
#include <stdio.h>
#include <utility>
#include <vector>
#include <map>
#include <string>

#include "TRandom3.h"
#include "TCanvas.h"
#include "TDatime.h"
#include "TStopwatch.h"
#include "TLegend.h"
#include "TIterator.h"
#include "TH3.h"
#include "TLatex.h"
#include "THStack.h"
#include "TPaveText.h"
#include "TAttText.h"
#include "TStyle.h"
#include "TObjArray.h"
#include "TF1.h"

#include "RooChi2Var.h"
#include "RooAbsData.h"
#include "RooRealSumPdf.h"
#include "RooPoisson.h"
#include "RooGaussian.h"
#include "RooRealVar.h"
#include "RooMCStudy.h"
#include "RooMinuit.h"
#include "RooCategory.h"
#include "RooHistPdf.h"
#include "RooSimultaneous.h"
#include "RooExtendPdf.h"
#include "RooDataSet.h"
#include "RooDataHist.h"
#include "RooFitResult.h"
#include "RooMsgService.h"
#include "RooParamHistFunc.h"
#include "RooHist.h"
#include "RooRandom.h"
#include "RooMinimizer.h"
#include "RooStats/ModelConfig.h"
#include "RooStats/ToyMCSampler.h"
#include "RooStats/MinNLLTestStat.h"

#include "RooStats/HistFactory/FlexibleInterpVar.h"
#include "RooStats/HistFactory/PiecewiseInterpolation.h"
#include "RooStats/HistFactory/HistFactorySimultaneous.h"
#include "RooStats/HistFactory/Channel.h"
#include "RooStats/HistFactory/MakeModelAndMeasurementsFast.h"
#include "RooStats/HistFactory/Measurement.h"
#include "RooStats/HistFactory/ParamHistFunc.h"
#include "RooStats/HistFactory/HistFactoryModelUtils.h"
#include "RooStats/HistFactory/RooBarlowBeestonLL.h"
#include "RooStats/HistFactory/HistFactoryNavigation.h"

#include "HistFactory_BsDs.h"

using namespace RooFit;
using namespace RooStats;
using namespace HistFactory;
using namespace std;


double FitterHistogram::Yield(){

	double  Yield(1);

	for ( auto& RRV_Factor: Vars ){
		Yield *= RRV_Factor->getValV();
	}

	return Yield;

}

double FitterHistogram::Error(bool Verbose){

	double Error(0), Yield(1);

	for ( auto& RRV_Factor: Vars ){

		double FracError = sqrt( pow(RRV_Factor->getError() / RRV_Factor->getValV(), 2.0 ) + pow(Error/Yield, 2.0 ) );
		Yield *= RRV_Factor->getValV();
		Error = FracError * Yield;

		if ( Verbose )
			std::cout << "    Analysing Factor: " << RRV_Factor->GetName()<< "\t" << RRV_Factor->getValV() << "\t" << RRV_Factor->getError() << std::endl;


	}

	return Error;


}


void FitterHistogram::ActivateStatError( bool activate ){

	if ( StatActive )
		Sample.ActivateStatError();

}

FitterHistogram::~FitterHistogram(){

}


std::vector<FitterHistogram>::iterator FitterObjects::begin(){
	return Components.begin();
}
std::vector<FitterHistogram>::iterator FitterObjects::end(){
	return Components.begin();
}


FitterObjects::~FitterObjects(){
	//std::vector<RooRealVar*> Vars;

	delete data;
	delete Data;

	//RooRealVar* poi;
	delete results;
	delete Minuit;
	//delete nll_hf;
	//delete HFSimultaneous;
	delete Simultaneous;
	delete MC;
	//delete Workspace;

	for( auto Hist: Components){
		delete Hist.Hist;
		delete Hist.Hist_After;
		delete Hist.Hist_Scaled;

	}

}


std::vector<std::string> splitstring(std::string work, char delim = ' ') {
    vector<std::string> flds;
    std::string buf = "";
    int i = 0;
    while (i < work.length()) {
        if (work[i] != delim)
            buf += work[i];
        else if (buf.length() > 0) {
            flds.push_back(buf);
            buf = "";
        }
        i++;
    }
    if (!buf.empty())
        flds.push_back(buf);
    return flds;
}





std::map<std::string, std::pair<double, double>> HistFactory_BsDs(const char* HistogramFile, std::string FitterConfFile, bool saveplots, std::string save_folder){



	FitterObjects* Fitter = new FitterObjects;

	Fitter->HistogramFile = HistogramFile;
	Fitter->FitterConfig = FitterConfFile;
	Fitter->OutputFolder = save_folder;



	//Perform an initial quick fit without the Beeston Barlo Method
	BsDs_Fitter_Parser( Fitter, false);

	BsDs_Fitter_Workspace( Fitter );

	BsDs_Fitter_Fit( Fitter );

	BsDs_Fitter_SaveTuple( Fitter );

	FitterObjects* FitterBB = new FitterObjects;

	FitterBB->HistogramFile = HistogramFile;
	FitterBB->FitterConfig = FitterConfFile;
	FitterBB->OutputFolder = save_folder;

	//Build a second model with the Beeston Barlow
	BsDs_Fitter_Parser( FitterBB, true);

	for ( auto& Histogram: FitterBB->Components){
		for( auto& NormFactor: Histogram.Sample.GetNormFactorList() ){
			double NFVal = Fitter->ResultTuple[ NormFactor.GetName() ].first;
			NormFactor.SetVal(NFVal);
		}
		//Loop over all histograms and over all normfactors to set the values.
	}

	BsDs_Fitter_Workspace( FitterBB );

	BsDs_Fitter_Fit( FitterBB );

	BsDs_Fitter_SaveTuple( FitterBB );

	if ( saveplots){

		BsDs_Fitter_SaveBeforeAfter( FitterBB );

		//BsDs_Fitter_Plotter( FitterBB, false );
		BsDs_Fitter_Plotter( FitterBB, true );
	}
	BsDs_Fitter_Print( FitterBB );

	std::map<std::string, std::pair<double, double>> ResultTuple= FitterBB->ResultTuple;

	delete Fitter;
	delete FitterBB;

	return ResultTuple;

}

std::map<std::string, std::pair<double, double>> HistFactory_BsDs(	Bs_Ds_Fit_Conf Configuration, bool saveplots ){


	std::string HistogramFile = Configuration.GetPar_s("OutputFolder") + Configuration.GetPar_s("File_Histogram_Name");
	std::string save_folder = Configuration.GetPar_s("OutputFolder");
	std::string FitterConfFile = Configuration.GetPar_s("Fitter_Configuration");
	//if ( saveplots == false)
	//	save_folder = "";



	std::map<std::string, std::pair<double, double>> FitResult_Tuple = HistFactory_BsDs(HistogramFile.c_str(), FitterConfFile, saveplots, save_folder);

	return FitResult_Tuple;
}


void BsDs_Fitter_Parser( FitterObjects* Fitter, bool ActiveStat ){


	/* Fill vectors while parsing the config file
	 *
	 *
	 */


	std::vector< RooStats::HistFactory::Sample > Samples;
	std::vector< TH1F*> Histograms;
	std::vector< std::string > ParameterNames;
	std::vector< THStack* > MergedHists;

	TFile* f_hist = TFile::Open(Fitter->HistogramFile.c_str(), "READ");

	Fitter->Data = (TH1F*)f_hist->Get("Data")->Clone();
	Fitter->Data->SetDirectory(0);

	double n_data = Fitter->Data->Integral();

	std::string line;
	ifstream myfile (Fitter->FitterConfig);
	std::string POI = "";


	if ( ! myfile.is_open())
		return;

	while ( getline (myfile,line) ){
		//std::cout << line << std::endl;

		if ( line.length() == 0 ) continue;
		if ( line[0] == '#' ) continue;
		if ( line == "Constraints" ) break;

		std::vector<std::string> line_elements = splitstring( line, ' ');
		std::string samplename = line_elements.at(0);
		getline (myfile,line);
		std::string CombinationID = line;
		getline (myfile,line);
		std::string CombinationName = line;

		FitterHistogram Histogram;

		Histogram.Hist        = (TH1F*)f_hist->Get( samplename.c_str() )->Clone();
		Histogram.Hist_Scaled = (TH1F*)f_hist->Get( samplename.c_str() )->Clone();

		if ( Fitter->CombinedModel.find( CombinationID ) == Fitter->CombinedModel.end() )
			Fitter->CombinedModel[ CombinationID ] = new THStack( CombinationID.c_str(), CombinationName.c_str() );
		Fitter->CombinedModel[ CombinationID ] -> Add( Histogram.Hist_Scaled );

		//Histograms.push_back( (TH1F*)Histogram->Clone() );
		RooStats::HistFactory::Sample sample( samplename, samplename, Fitter->HistogramFile );

		for (auto it = line_elements.begin(); it != line_elements.end(); it++){

			if ( *it == "STAT" ){
				Histogram.StatActive = true;
				if ( ActiveStat )
					sample.ActivateStatError();
			}
			else if ( *it == "NormFactorScaled" or *it == "NormFactorFraction" ){

				double scaling = 1.0;
				if (*it == "NormFactorScaled")
					scaling = n_data;
				it++;
				std::string NF_name = *it;
				it++;
				double n_start = std::stod( *it ) * scaling;
				it++;
				double n_low = std::stod( *it ) * scaling;
				it++;
				double n_high = std::stod( *it ) * scaling;

				sample.AddNormFactor(NF_name, n_start, n_low, n_high);

				if ( POI == "" )
					POI = NF_name;

				if ( std::find( ParameterNames.begin(), ParameterNames.end(), NF_name ) == ParameterNames.end() ){
					ParameterNames.push_back( NF_name );
				}

			}


			else if ( *it == "SYSTEMATIC" ){
				it++;
				std::string Syst_name = *it;
				it++;
				double Syst_Low = std::stod( *it );
				it++;
				double Syst_High = std::stod( *it );

				sample.AddOverallSys( Syst_name, Syst_Low, Syst_High );


			}

			sample.SetNormalizeByTheory(false);

		}
		Histogram.Sample = sample;

		Fitter->Components.push_back(Histogram);

		Fitter->POIName = POI;

	}

	myfile.close();


}



void BsDs_Fitter_Workspace(FitterObjects* Fitter){

	Fitter->Measurement = RooStats::HistFactory::Measurement("Bs2DsTest", "Bs2DsTest");

	Fitter->Measurement.SetOutputFilePrefix( ( Fitter->OutputFolder + "results_Bs_DsMuNu" ).c_str() );
	Fitter->Measurement.SetExportOnly(true);


	Fitter->Measurement.SetLumi(1.0);
	Fitter->Measurement.SetLumiRelErr(0.000010);

	Fitter->Channel = RooStats::HistFactory::Channel("channel");

	Fitter->Channel.SetStatErrorConfig(1e-5,"Pois");
	Fitter->Channel.SetData("Data", Fitter->HistogramFile);

	for( auto& Histogram: Fitter->Components){
		Fitter->Channel.AddSample(Histogram.Sample);
		std::cout << Histogram.Hist->GetTitle() << std::endl;
	}

	Fitter->Measurement.SetPOI( Fitter->POIName.c_str() );

	Fitter->Measurement.AddChannel(Fitter->Channel);
	Fitter->Measurement.CollectHistograms();

	Fitter->Measurement.PrintTree();

	Fitter->Workspace = RooStats::HistFactory::MakeModelAndMeasurementFast(Fitter->Measurement);
	Fitter->MC = (ModelConfig*) Fitter->Workspace->obj("ModelConfig");

	((RooRealVar*)(Fitter->MC->GetNuisanceParameters()->find("Lumi")))->setConstant(true);

	Fitter -> Simultaneous = (RooSimultaneous*) Fitter->MC->GetPdf();
	Fitter -> data = (RooDataSet*) Fitter->MC->GetWorkspace()->data("obsData");
	Fitter -> poi = (RooRealVar*) Fitter->MC->GetParametersOfInterest()->createIterator()->Next();
	Fitter -> HFSimultaneous = new HistFactorySimultaneous( *Fitter->Simultaneous );
	Fitter -> nll_hf = Fitter->HFSimultaneous->createNLL( *Fitter->data, Offset(kTRUE), NumCPU(8) );

	Fitter -> Minuit = new RooMinuit( *Fitter->nll_hf ) ;
	Fitter -> Minuit -> setStrategy(2);
    //Fitter -> Minuit -> setPrintLevel(-1);
    Fitter -> Minuit -> optimizeConst(1);
    Fitter -> Minuit -> setErrorLevel(0.5); // 1-sigma = DLL of 0.5
    Fitter -> Minuit -> setOffsetting(kTRUE);

    //((RooRealVar*)(Fitter->MC->GetNuisanceParameters()->find( "SS_Yield" )))->setConstant(true);

	for( auto& Histogram: Fitter->Components){ // Fill the histogram Classes with the list of variables
		std::vector< RooStats::HistFactory::NormFactor > ListOfFactors = Histogram.Sample.GetNormFactorList();

		for ( auto& factor: ListOfFactors ){
			std::string FactorName = factor.GetName();
			RooRealVar* RRV_Factor = (RooRealVar*)(Fitter->MC->GetNuisanceParameters()->find( FactorName.c_str() ));
			if ( ! RRV_Factor ) // If the factor wasn't found in the Nuisance pars, asign it to the signal parameter.
				RRV_Factor = Fitter->poi;

			Histogram.Vars.push_back( RRV_Factor );

		}

	}


}

void BsDs_Fitter_Fit(FitterObjects* Fitter){

	std::vector< double > Yields;


	int status_hesse_0 = 0; //try different minimisers
	int status_migrad = 0; //try different minimisers
	int status_simplex = 0; //try different minimisers
	int status_hesse_1 = 0; //try different minimisers
	int status_minos = 0;

	status_hesse_0 = Fitter -> Minuit ->hesse(); //try different minimisers
	status_simplex = Fitter -> Minuit->simplex(); //try different minimisers
	status_migrad = Fitter -> Minuit->migrad(); //try different minimisers
	status_hesse_1 = Fitter -> Minuit->hesse(); //try different minimisers
	status_minos = Fitter -> Minuit ->minos(RooArgSet(*Fitter->poi));

	std::cout << "Fitter POI: " << Fitter->poi->GetName() << std::endl;

	Fitter->results = Fitter->Minuit->save();

	std::pair<double, double> temp_pair(status_hesse_0, 0);
	Fitter->ResultTuple["Status_Hesse"] = temp_pair;
	temp_pair.first = status_simplex;
	Fitter->ResultTuple["Status_Simplex"] = temp_pair;
	temp_pair.first = status_migrad;
	Fitter->ResultTuple["Status_Migrad"] = temp_pair;
	temp_pair.first = status_minos;
	Fitter->ResultTuple["Status_Minos"] = temp_pair;
	temp_pair.first = status_hesse_1;
	Fitter->ResultTuple["Status_Hesse_2"] = temp_pair;

	temp_pair.first = Fitter->results->covQual();
	Fitter->ResultTuple["CovQual"] = temp_pair;
	temp_pair.first = Fitter->results->edm();
	Fitter->ResultTuple["EDM"] = temp_pair;



}

void BsDs_Fitter_SaveBeforeAfter(FitterObjects *Fitter){



	TCanvas* c_bef_aft = new TCanvas("c_bef_aft", "c_bef_aft", 1600, 1600);
	TPaveText *pt = new TPaveText(.05, .85, .35, .95, "NDC");
	pt->AddText("Before")->SetTextColor(1);
	pt->AddText("After")->SetTextColor(2);

	c_bef_aft->Print( ( Fitter->OutputFolder + "CompareBeforeAfter.pdf[").c_str() );


	RooStats::HistFactory::HistFactoryNavigation HF_Nav( Fitter->MC );

	for ( auto& Histogram : Fitter->Components ){
		std::string h_name = Histogram.Hist->GetName();
		Histogram.Hist_After = (TH1F*)HF_Nav.GetSampleHist("channel" , h_name.c_str() ,(h_name + "_after" ).c_str() );
		//Save the before and after plots
		//for (unsigned int x = 0; x < Histograms.size(); x++){
		c_bef_aft->Clear();

		TPad *pad1 = new TPad("pad1.1", "The pad 80% of the height",0.0,0.3,1.0,1.0);
		pad1->cd();

		Histogram.Hist_After->Sumw2();
		Histogram.Hist_After->SetMarkerColor(2);
		Histogram.Hist_After->SetLineColor(2);
		Histogram.Hist_After->Scale( Histogram.Hist->Integral()/Histogram.Hist_After->Integral() );

		Histogram.Hist->SetMarkerColor(1);
		Histogram.Hist->SetLineColor(1);

		Histogram.Hist->Draw();
		Histogram.Hist_After->Draw("SAME");

		pt->Draw();

		TPad *pad2 = new TPad("pad2.2", "The pad 20% of the height",0.0,0.0,1.0,0.3);
		pad2->cd();
		TH1F* h_ratio = (TH1F*)Histogram.Hist->Clone();
		h_ratio->SetTitle("(After-Before)/#sigma_{Before}");
		for( int bin = 1 ; bin <= h_ratio->GetSize()-2; bin++ ){
			h_ratio->SetBinContent(bin, (Histogram.Hist_After->GetBinContent(bin) - Histogram.Hist->GetBinContent(bin))/Histogram.Hist->GetBinError(bin) );
			h_ratio->SetBinError(bin, 1e-10);
		}

		h_ratio->DrawClone("p");

		c_bef_aft->cd();
		pad1->Draw();
		pad2->Draw();



		c_bef_aft->Print(( Fitter->OutputFolder + "CompareBeforeAfter.pdf").c_str() );
	}
	c_bef_aft->Clear();
	c_bef_aft->Print(( Fitter->OutputFolder + "CompareBeforeAfter.pdf]").c_str());


}

void BsDs_Fitter_SaveTuple( FitterObjects* Fitter ){


	Fitter->Vars.push_back( (RooRealVar*) Fitter->MC->GetParametersOfInterest()->createIterator()->Next() );


	//Save the results for returning
	int n_frac = 0;
	for ( auto& Histogram: Fitter->Components ){

		//Analyse the histogram and get the histogram yields
		std::cout << "\nAnalysing sample: " << Histogram.Sample.GetName() << std::endl;

		double Yield = Histogram.Yield();
		double Error = Histogram.Error( true );

		std::cout << "Finishing sample: " << Histogram.Sample.GetName();
		std::cout << "\t" << Yield << "\t" << Error << "\t" << Error / Yield << std::endl;
		std::pair<double, double> temp_pair(Yield, Error);

		Fitter->ResultTuple[Histogram.Sample.GetName()] = temp_pair;

		//Loop over the Normalisation Factors within the histogram and get their values:

		for ( auto& NormFactor: Histogram.Vars){

			temp_pair = std::pair<double, double>( NormFactor->getValV(), NormFactor->getError() );
			std::string NormFactorName = NormFactor->GetName();

			Fitter->ResultTuple[NormFactorName] = temp_pair;

		}

		n_frac++;

	}




}

void BsDs_Fitter_Plotter( FitterObjects* Fitter, bool before_BB ){

	gStyle->SetOptStat(0);

	for (auto& Histogram: Fitter->Components){
		if ( before_BB )
			*Histogram.Hist_Scaled = *Histogram.Hist;
		else
			*Histogram.Hist_Scaled = *Histogram.Hist_After;
		Histogram.Hist_Scaled->Scale( Histogram.Yield() );
		std::cout << "Scaling Histogram: " << Histogram.Hist->GetName() << " By " << Histogram.Yield() << std::endl;
	}


	TCanvas* c1 = new TCanvas("c1", "c1", 1600, 1600);
	THStack* FitPlot = new THStack("h_Fit", "Fit Plot");

	TPad *pad1 = new TPad("pad1.1", "The pad 70% of the height",0.0,0.3,1.0,1.0);
	TPad *pad2 = new TPad("pad1.2", "The pad 30% of the height",0.0,0.0,1.0,0.3);

	Fitter->Data->SetTitle("");

	TLegend *leg_1 = new TLegend(0.11, 0.6, 0.35, 0.89);
	leg_1->SetBorderSize(0);
	leg_1->AddEntry(Fitter->Data, "Data", "lep");

	int it = 2;
	for (auto& s: Fitter->CombinedModel ){

		THStack* Combination = s.second;

		TLegend* leg_combi = new TLegend(0.11, 0.5, 0.4, 0.8);
		leg_combi->SetBorderSize(0);

		c1->Clear();

		TList* Hists = Combination->GetHists();
		((TH1F*)Combination->GetStack()->Last())->SetLineColor(1);
		((TH1F*)Combination->GetStack()->Last())->SetMinimum(0);
		Combination->GetStack()->Last()->Draw();

		for ( int n = 0; n < Hists->GetEntries(); n++ ){
			TH1F* hist = (TH1F*)Hists->At(n);
			hist->SetLineColor(n+2);
			hist->Draw("SAME");
			leg_combi->AddEntry( hist, hist->GetTitle(), "lep" );

		}
		leg_combi->Draw();
		c1->Print( (Fitter->OutputFolder + (std::string)Combination->GetName() + ".pdf").c_str() );

		TH1F* Hist = (TH1F*)Combination->GetStack()->Last()->Clone();

		//Hist->Sumw2(false);
		Hist->SetFillColorAlpha(it, 0.1);
		Hist->SetLineColor(it);
		FitPlot->Add( (TH1F*)Hist->Clone() );
		leg_1->AddEntry(Hist, Combination->GetTitle(), "f");

		it++;
	}


	TH1F* h_All = (TH1F*)FitPlot->GetStack()->Last()->Clone();


	pad2->cd();



	TH1F* Pulls = (TH1F*) Fitter->Data -> Clone();
	Pulls->Add( h_All, Fitter->Data, 1, -1 );
	for ( int bin = 0; bin <= Pulls->GetNbinsX(); bin++ ){
		double pull_X = Pulls->GetBinContent( bin ) / Pulls->GetBinError( bin );
		Pulls->SetBinContent(bin, pull_X);
		Pulls->SetBinError(bin, 1e-10);

	}

	Pulls->SetMaximum( 2.5 );
	Pulls->SetMinimum(-2.5 );

	double min_hist = Pulls->GetXaxis()->GetXmin();
	double max_hist = Pulls->GetXaxis()->GetXmax();
	TF1* Pull_Line = new TF1("Pull_Line", "pol1", min_hist, max_hist);
	Pulls->Fit(Pull_Line);

	Pulls->Draw();
	for (int stacked = 1; stacked >=0 ; stacked--){
		pad1->Clear();
		Fitter->Data->DrawClone();
		pad1->cd();



		h_All->Sumw2();
		h_All->SetFillColorAlpha(0, 0);

		if ( stacked ){
			FitPlot->DrawClone("SAME HIST");
			h_All->SetLineColor(it-1);
			h_All->SetMarkerColor(it-1);

		}
		else{
			//FitPlot->DrawClone("SAME");
			TList* Hists = FitPlot->GetHists();
			for ( int n = 0; n < Hists->GetEntries(); n++){
				TH1F* h = (TH1F*)Hists->At(n);
				((TH1F*)h)->Draw("SAME HIST");
			}
			h_All->SetLineColor(it);
			h_All->SetMarkerColor(it);
			leg_1->AddEntry(h_All, "Template Combination", "lep");
		}

		h_All->DrawClone("SAME");

		Fitter->Data->DrawClone("SAME");
		leg_1->DrawClone();

		c1->cd();
		pad1->DrawClone();
		pad2->DrawClone();

		std::string afterBB = "";
		if (! before_BB)
			afterBB = "_After_BB";

		if ( stacked )
			c1->Print( (Fitter->OutputFolder + "FitPlot_stack" + afterBB + ".pdf").c_str() );
		else
			c1->Print( (Fitter->OutputFolder + "FitPlot_nostack" + afterBB + ".pdf").c_str() );
	}

	std::pair<double, double> Pull_Gradient;
	Pull_Gradient.first = Pull_Line->GetParameter(1);
	Pull_Gradient.second = Pull_Line->GetParError(1);

	Fitter->ResultTuple["Pull_Gradient"] = Pull_Gradient;


}


void BsDs_Fitter_Print( FitterObjects* Fitter){





	
/*
std::cout <<
"{\\tiny \n\
\\begin{table}[]		\n\
\\centering 			\n\
\\tiny 			\n\
\\label{my-label}		\n\
\\begin{tabular}{llll}	\n\
 & Yield & $\sigma$ & Corr(\Ds) \\\\" << std::endl;
 
   for( RooRealVar* const& nuispar: NuisanceVars){
		std::string parname = nuispar->GetName();
		std::cout << std::setprecision(3) << parname.substr(0, parname.size()-6) << "\t& " << nuispar->getVal() << "\t& " << nuispar->getError() << std::setprecision(2) << " (" << nuispar->getError() / nuispar->getVal() <<  ")\t& " <<results->correlation("Ds_Yield",nuispar->GetName())  << "\t \\\\" << std::endl;
	}
std::cout <<
"Fit EDM: &" << results->edm() << " & " << "Cov. Qual:&" << results->covQual() << "\\\\" << std::endl<<
"\\end{tabular}\n\
\\end{table}}" << std::endl;
*/

	std::cout << std::setprecision(5);
	for ( auto& it: Fitter->ResultTuple ){
		std::cout << it.first << "\t" << it.second.first << "\t" << it.second.second << std::endl;
	}





}

