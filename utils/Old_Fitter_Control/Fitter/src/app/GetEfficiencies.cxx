#include <string>
#include <cstdlib>
#include <map>
#include <utility>
#include "TCanvas.h"
#include "TGraph.h"
#include "TFile.h"
#include "TChain.h"
#include "TH1F.h"
#include "Bs_Ds_Fit_Conf.h"

int doBanner()
{
  //Quick and dirty workaround to prevent RooFit's copyright message from showing
  //This is universal so no need to edit rootrc
  return 0;
}

template<typename T> void printElement(T t)
{
    std::cout << std::left << std::setw(20) << std::setfill(' ') << t;
}

int main(int argc, char* argv[]){
	
	
	/* The following are the numbers of events in bookkeeping for event type 1374000
13774000	/MC/2012/Beam4000GeV-2012-MagUp-Nu2.5-Pythia8/Sim08i/Digi13/Trig0x409f0045/Reco14c/Stripping21NoPrescalingFlagged/13774000/ALLSTREAMS.DST	dddb-20130929-1	sim-20130522-1-vc-mu100	3113002
13774000	/MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-Pythia8/Sim08i/Digi13/Trig0x409f0045/Reco14c/Stripping21NoPrescalingFlagged/13774000/ALLSTREAMS.DST	dddb-20130929-1	sim-20130522-1-vc-md100	3325809
13774000	/MC/2011/Beam3500GeV-2011-MagUp-Nu2-Pythia8/Sim08i/Digi13/Trig0x40760037/Reco14c/Stripping21r1NoPrescalingFlagged/13774000/ALLSTREAMS.DST	dddb-20130929	sim-20130522-vc-mu100	1662153
13774000	/MC/2011/Beam3500GeV-2011-MagDown-Nu2-Pythia8/Sim08i/Digi13/Trig0x40760037/Reco14c/Stripping21r1NoPrescalingFlagged/13774000/ALLSTREAMS.DST	dddb-20130929	sim-20130522-vc-md100	1655040
13774000	/MC/2015/Beam6500GeV-2015-MagUp-Nu1.6-25ns-Pythia8/Sim09a/Trig0x411400a2/Reco15a/Turbo02/Stripping24NoPrescalingFlagged/13774000/ALLSTREAMS.DST	dddb-20150724	sim-20160606-vc-mu100	5191040
13774000	/MC/2015/Beam6500GeV-2015-MagDown-Nu1.6-25ns-Pythia8/Sim09a/Trig0x411400a2/Reco15a/Turbo02/Stripping24NoPrescalingFlagged/13774000/ALLSTREAMS.DST	dddb-20150724	sim-20160606-vc-md100 5001335
	*/
	

	
	
	std::string ConfFile = "";
	std::string HistConfFile = "";	
	
	if ( argc >=2 )
		ConfFile = argv[1];

	Bs_Ds_Fit_Conf Configuration = Bs_Ds_Fit_Conf(ConfFile);

	for ( int l = 2; l < argc; l++ ){
		Configuration.ParseLine( argv[l] );
	}
	
	TChain* Chain_MC = new TChain("Bs2DsMuNuTuple/DecayTree");
	Chain_MC->Add("root://eoslhcb.cern.ch//eos/lhcb/wg/semileptonic/Bs2KmunuAna/Tuples/Bs2DsMuNu_MCTUPLES_RAW_13June16/DTT_MC*_Bs2DsMuNu_13774000_Cocktail_SIGNAL_*.root");
	Chain_MC->SetAlias( "Bs_Q2SOLRAND",  "Bs_Q2SOL1 + ((eventNumber/2)%2)*(Bs_Q2SOL2 - Bs_Q2SOL1) ");

	std::string Hist_file = Configuration.GetPar_s("OutputFolder") + Configuration.GetPar_s("File_Histogram_Name");
	TFile* f_hist = TFile::Open(Hist_file.c_str(), "READ");

	
	double n_BKK_D_11 = 1655040;
	double n_BKK_U_11 = 1662153;
	double n_BKK_D_12 = 3113002;
	double n_BKK_U_12 = 3325809;
	
	double n_Gen_D_11 = n_BKK_D_11 / 0.17689;
	double n_Gen_U_11 = n_BKK_U_11 / 0.17743;
	double n_Gen_D_12 = n_BKK_D_12 / 0.17948;
	double n_Gen_U_12 = n_BKK_U_12 / 0.18006;	
	
	std::vector<std::string> HistNames;
	HistNames.push_back("All13774000");
	HistNames.push_back("Dsmunu");
	HistNames.push_back("DsStarmunu");
	HistNames.push_back("DsStar0munu");
	HistNames.push_back("DsStar1munu");
	HistNames.push_back("DsXtaunu");
	
	std::vector<std::string> EventType;
	EventType.push_back("13774000");
	EventType.push_back("13774000");
	EventType.push_back("13774000");
	EventType.push_back("13774000");
	EventType.push_back("13774000");
	EventType.push_back("13774000");
	
	std::vector<std::string> EventDescription;
	EventDescription.push_back("Bs_Dsmunu=cocktail,hqet2,DsmunuInAcc");
	EventDescription.push_back("Bs_Dsmunu=cocktail,hqet2,DsmunuInAcc");
	EventDescription.push_back("Bs_Dsmunu=cocktail,hqet2,DsmunuInAcc");
	EventDescription.push_back("Bs_Dsmunu=cocktail,hqet2,DsmunuInAcc");
	EventDescription.push_back("Bs_Dsmunu=cocktail,hqet2,DsmunuInAcc");
	EventDescription.push_back("Bs_Dsmunu=cocktail,hqet2,DsmunuInAcc");
	
	std::map<std::string, double> Gen_frac;
	Gen_frac["All13774000"]	= 1;
	Gen_frac["Dsmunu"]		= 0.0210/0.0919700;
	Gen_frac["DsStarmunu"]	= 0.0510/0.0919700;
	Gen_frac["DsStar0munu"]	= 0.0070/0.0919700;
	Gen_frac["DsStar1munu"]	= 0.0080/0.0919700;
	Gen_frac["DsXtaunu"]	= 0.0050/0.0919700;
	
	std::map<std::string, std::string> Sample_Cut;
	Sample_Cut["All13774000"]		= "1";
	Sample_Cut["Dsmunu"]		= "Bs_TM_DsMu";
	Sample_Cut["DsStarmunu"]	= "Bs_TM_DsstarMu_Dsg + Bs_TM_DsstarMu_Dspi0 > 0";
	Sample_Cut["DsStar0munu"]	= "Bs_TM_Dsstar0Mu_Dspi0 + Bs_TM_Dsstar0Mu_Dspi0pi0 + Bs_TM_Dsstar0Mu_Dspipi + Bs_TM_Dsstar0Mu_Dsstarg_Dsg + Bs_TM_Dsstar0Mu_Dsstarg_Dspi0 >0";
	Sample_Cut["DsStar1munu"]	= "Bs_TM_Ds12460_Dspi0pi0 + Bs_TM_Ds12460_Dspipi + Bs_TM_Ds12460_Dsstarpi0_Dsg + Bs_TM_Ds12460_Dsstarpi0_Dspi0 + Bs_TM_Ds12536Mu_Dsg + Bs_TM_Ds12536Mu_Dspi0pi0 + Bs_TM_Ds12536Mu_Dspipi + Bs_TM_Ds12536Mu_Dsstarg_Dsg + Bs_TM_Ds12536Mu_Dsstarg_Dspi0 + Bs_TM_Ds12536Mu_Dsstarpi0_Dspi0 + Bs_TM_Ds12536Mu_Dsstarpi0_Dspi0pi0 + Bs_TM_Ds12536Mu_Dsstarpi0_Dspipi > 0";
	Sample_Cut["DsXtaunu"]		= "abs(muon_p_MC_MOTHER_ID) == 15 && (Bs_BKGCAT == 50 || Bs_BKGCAT == 40)";
	
	
	
	double n_Gen        = n_Gen_D_11 + n_Gen_U_11 + n_Gen_D_12 + n_Gen_U_12;
	std::vector<double> n_Gen_sample;
	
	double n_BKK       = 3113002+3325809+1662153+1655040;
	std::vector<double> n_BKK_sample;
	
	double n_NTuple       = Chain_MC->GetEntries();
	std::vector<double> n_NTuple_sample;
	
	double n_Cuts   = Chain_MC->GetEntries(Configuration.GetPar_s("Common_Cuts").c_str());
	std::vector<double> n_Cuts_sample;
	
	double n_Hist    = ((TH1F*)f_hist->Get("All13774000"))->Integral();
	std::vector<double> n_Hist_sample;



	for( auto& Hname: HistNames ){
		n_Gen_sample.push_back( n_Gen * Gen_frac[Hname] );
		n_BKK_sample.push_back(0);
		n_NTuple_sample.push_back( Chain_MC->GetEntries( Sample_Cut[Hname].c_str() ) );
		n_Cuts_sample.push_back( Chain_MC->GetEntries( ( Sample_Cut[Hname] + "&&" + Configuration.GetPar_s("Common_Cuts")).c_str() ) );
		n_Hist_sample.push_back( ((TH1F*)f_hist->Get(Hname.c_str()))->Integral() );
		
	
	}
	n_BKK_sample[0]=n_BKK;
	Chain_MC->Reset();
	Chain_MC->Add("root://eoslhcb.cern.ch//eos/lhcb/wg/semileptonic/Bs2KmunuAna/Tuples/Bs2DsMuNu_MCTUPLES_RAW_13June16/DTT_MC11_11995202_*.root");
	Chain_MC->SetAlias( "Bs_Q2SOLRAND",  "Bs_Q2SOL1 + ((eventNumber/2)%2)*(Bs_Q2SOL2 - Bs_Q2SOL1) ");
	HistNames.push_back("Bd_DD");
	n_Gen_sample.push_back(561856/0.031037 + 549400/0.031043);
	n_BKK_sample.push_back(1111256);
	n_NTuple_sample.push_back( Chain_MC->GetEntries( /*"Bs_BKGCAT==50 || Bs_BKGCAT==40"*/ ) );
	n_Cuts_sample.push_back( Chain_MC->GetEntries( (/*"(Bs_BKGCAT==50 || Bs_BKGCAT==40 ) &&" +*/ Configuration.GetPar_s("Common_Cuts")).c_str() ) );
	n_Hist_sample.push_back( ((TH1F*)f_hist->Get("Bd_DD"))->Integral() );
	EventType.push_back("11995202");
	
	Chain_MC->Reset();
	Chain_MC->Add("root://eoslhcb.cern.ch//eos/lhcb/wg/semileptonic/Bs2KmunuAna/Tuples/Bs2DsMuNu_MCTUPLES_RAW_13June16/DTT_MC11_12995602_*.root");
	Chain_MC->SetAlias( "Bs_Q2SOLRAND",  "Bs_Q2SOL1 + ((eventNumber/2)%2)*(Bs_Q2SOL2 - Bs_Q2SOL1) ");
	HistNames.push_back("Bu_DD");
	n_Gen_sample.push_back(503178/0.18484 + 546525/0.18500);
	n_BKK_sample.push_back(503178+546525);
	n_NTuple_sample.push_back( Chain_MC->GetEntries( /*"Bs_BKGCAT==50 || Bs_BKGCAT==40"*/ ) );
	n_Cuts_sample.push_back( Chain_MC->GetEntries( (/*"(Bs_BKGCAT==50 || Bs_BKGCAT==40) &&" + */Configuration.GetPar_s("Common_Cuts")).c_str() ) );
	n_Hist_sample.push_back( ((TH1F*)f_hist->Get("Bu_DD"))->Integral() );	
	EventType.push_back("12995602");
	
	Chain_MC->Reset();
	Chain_MC->Add("root://eoslhcb.cern.ch//eos/lhcb/wg/semileptonic/Bs2KmunuAna/Tuples/Bs2DsMuNu_MCTUPLES_RAW_13June16/DTT_MC11_13996202_*.root");
	Chain_MC->SetAlias( "Bs_Q2SOLRAND",  "Bs_Q2SOL1 + ((eventNumber/2)%2)*(Bs_Q2SOL2 - Bs_Q2SOL1) ");
	HistNames.push_back("Bs_DD");
	n_Gen_sample.push_back(542937/0.09265  + 550424/0.09219);
	n_BKK_sample.push_back(542937+550424);
	n_NTuple_sample.push_back( Chain_MC->GetEntries( /*"Bs_BKGCAT==50 || Bs_BKGCAT==40"*/ ) );
	n_Cuts_sample.push_back( Chain_MC->GetEntries( (/*"(Bs_BKGCAT==50 || Bs_BKGCAT==40) &&"  +*/ Configuration.GetPar_s("Common_Cuts")).c_str() ) );
	n_Hist_sample.push_back( ((TH1F*)f_hist->Get("Bs_DD"))->Integral() );		
	EventType.push_back("13996202");
	
	
	std::cout << "Numbers of Events" << std::endl;
	std::cout << std::setprecision(3); printElement( " " );			printElement(" "); printElement( "Generator" );	printElement( "BookKeeping" );	printElement( "NTuple" );		printElement( "Selection" );	printElement( "Histogram" );	std::cout << std::endl;
	for( int it = 0; it<HistNames.size(); it++ ){
		std::cout << std::setprecision(3); printElement( HistNames.at(it) ); printElement(EventType.at(it)); printElement( n_Gen_sample.at(it)      );		printElement( n_BKK_sample.at(it) );		printElement( n_NTuple_sample.at(it)); 	printElement( n_Cuts_sample.at(it) );		printElement( n_Hist_sample.at(it)  ); 	std::cout << std::endl;
	}
	
	
	std::cout << "Fractions of Events" << std::endl;
	std::cout << std::setprecision(3); printElement( " " );			printElement(" "); printElement( "Generator" );	printElement( "BookKeeping" );	printElement( "NTuple" );		printElement( "Selection" );	printElement( "Histogram" );	std::cout << std::endl;
	for( int it = 0; it<HistNames.size(); it++ ){
		std::cout << std::setprecision(3); printElement( HistNames.at(it) );printElement(EventType.at(it)); printElement( n_Gen_sample.at(it)/n_Gen_sample.at(it)      );		printElement( n_BKK_sample.at(it)/n_Gen_sample.at(it)      );		printElement( n_NTuple_sample.at(it)/n_Gen_sample.at(it)   ); 	printElement( n_Cuts_sample.at(it)/n_Gen_sample.at(it)     );		printElement( n_Hist_sample.at(it)/n_Gen_sample.at(it)     ); 	std::cout << std::endl;
	}	
	
	
	/*
	std::cout <<std::setprecision(2) <<
	"" << std::left << std::setw(20) << std::setfill(' ') << "Cocktail"	<< "\t(" << "frac" 			<< ")\t" << "Ds" 		<<"\t(" << "frac" 				<< ")\n" <<
	"Generator Level:   " << std::left << std::setw(20) << std::setfill(' ') << n_Gen		<< "\t(" << n_Gen/n_Gen		<< ")\t" << n_GenDs		<<"\t(" << n_GenDs/n_GenDs		<< ")\n" <<
	"BookKeeping Level: " << std::left << std::setw(20) << std::setfill(' ') << n_BKK		<< "\t(" << n_BKK/n_Gen		<< ")\t" << n_BKKDs		<<"\t(" << n_BKKDs/n_GenDs		<< ")\n" <<
	"NTuple Level:      " << std::left << std::setw(20) << std::setfill(' ') << n_NTuple	<< "\t(" << n_NTuple/n_Gen	<< ")\t" << n_NTupleDs	<<"\t(" << n_NTupleDs/n_GenDs	<< ")\n" <<
	"After Selection:   " << std::left << std::setw(20) << std::setfill(' ') << n_Cuts		<< "\t(" << n_Cuts/n_Gen	<< ")\t" << n_CutsDs	<<"\t(" << n_CutsDs/n_GenDs		<< ")\n" <<
	"Histogram Level:   " << std::left << std::setw(20) << std::setfill(' ') << n_Hist		<< "\t(" << n_Hist/n_Gen	<< ")\t" << n_HistDs	<<"\t(" << n_HistDs/n_GenDs		<< ")\n";
	*/
	
	
	
	
	
	
	/*
	std::cout <<std::setprecision(3) << "                   "; printElement( "Cocktail" ); printElement( "Fraction"     ); printElement( "Ds"       ); printElement( "Fraction"         ); std::cout << std::endl;
	std::cout <<std::setprecision(3) << "Generator Level:   "; printElement( n_Gen      ); printElement( n_Gen/n_Gen    ); printElement( n_GenDs    ); printElement( n_GenDs/n_GenDs    ); std::cout << std::endl;
	std::cout <<std::setprecision(3) << "BookKeeping Level: "; printElement( n_BKK      ); printElement( n_BKK/n_Gen    ); printElement( n_BKKDs    ); printElement( n_BKKDs/n_GenDs    ); std::cout << std::endl;
	std::cout <<std::setprecision(3) << "NTuple Level:      "; printElement( n_NTuple   ); printElement( n_NTuple/n_Gen ); printElement( n_NTupleDs ); printElement( n_NTupleDs/n_GenDs ); std::cout << std::endl;
	std::cout <<std::setprecision(3) << "After Selection:   "; printElement( n_Cuts     ); printElement( n_Cuts/n_Gen   ); printElement( n_CutsDs   ); printElement( n_CutsDs/n_GenDs   ); std::cout << std::endl;
	std::cout <<std::setprecision(3) << "Histogram Level:   "; printElement( n_Hist     ); printElement( n_Hist/n_Gen   ); printElement( n_HistDs   ); printElement( n_HistDs/n_GenDs   ); std::cout << std::endl;
	*/
	
	
	
	
}
/*
 

 The Dec file for the decay: /afs/cern.ch/lhcb/software/releases/DBASE/Gen/DecFiles/v29r8/dkfiles/Bs_Dsmunu=cocktail,hqet2,DsmuInAcc.dec
 
 
 * 
 * # EventType: 13774000
#
# Descriptor: {[[B_s0]nos => (D_s- => K+ K- pi-) nu_mu mu+]cc, [[B_s0]os => (D_s+ => K- K+ pi+) anti_nu_mu mu-]cc}

Decay B_s0sig
  #such that the Ds* /Ds = 2.42 and the Ds** /(AllDs) = 0.172
  0.0210   MyD_s-     mu+    nu_mu       PHOTOS  HQET2 1.17 1.074;
  0.0510   MyD_s*-    mu+    nu_mu       PHOTOS  HQET2 1.16 0.921 1.37 0.845;
  0.0070   MyD_s0*-   mu+    nu_mu       PHOTOS  ISGW2;
  0.0040   MyD_s1-    mu+    nu_mu       PHOTOS  ISGW2;
  0.0040   MyD'_s1-   mu+    nu_mu       PHOTOS  ISGW2;
  # correct for the fact that we force tau -> mu anti_nu_mu nu_tau (17.3%)
  0.00138  MyD_s-     Mytau+   nu_tau    PHOTOS ISGW2;
  0.00277  MyD_s*-    Mytau+   nu_tau    PHOTOS ISGW2;
  0.00038  MyD_s0*-   Mytau+   nu_tau    PHOTOS ISGW2;
  0.00022  MyD_s1-    Mytau+   nu_tau    PHOTOS ISGW2;
  0.00022  MyD'_s1-   Mytau+   nu_tau    PHOTOS ISGW2;
Enddecay
CDecay anti-B_s0sig
#
Decay MyD_s+
  0.055     K+    K-     pi+          PHOTOS D_DALITZ;
Enddecay
CDecay MyD_s-
#
Decay MyD_s*+
  0.942   MyD_s+  gamma               PHOTOS VSP_PWAVE;
  0.058   MyD_s+  pi0                 PHOTOS VSS;
Enddecay
CDecay MyD_s*-
#
Decay MyD_s1+
  0.0337  MyD_s+  gamma                PHOTOS PHSP;
  0.097   MyD_s*+ pi0                  PHOTOS PHSP;
  0.0077  MyD_s+  pi+ pi-              PHOTOS PHSP;
  0.0038  MyD_s+  pi0 pi0              PHOTOS PHSP;
  0.008   MyD_s+  gamma gamma          PHOTOS PHSP;
  0.008   MyD_s*+ gamma                PHOTOS PHSP;
Enddecay
CDecay MyD_s1-
#
Decay MyD_s0*+
  1.000   MyD_s+   pi0                 PHOTOS PHSP;
Enddecay
CDecay MyD_s0*-
#
Decay MyD'_s1+
  0.5   MyD_s*+   gamma              PHOTOS PHSP;
  0.5   MyD_s+    pi+ pi-            PHOTOS PHSP;
Enddecay
CDecay MyD'_s1-
#
Decay Mytau+
  0.1731     mu+  nu_mu   anti-nu_tau  PHOTOS  TAULNUNU;
Enddecay
CDecay Mytau-
#
End

*/
