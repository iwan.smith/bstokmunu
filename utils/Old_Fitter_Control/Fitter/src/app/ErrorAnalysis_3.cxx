#include <string>
#include <cstdlib>
#include <map>
#include <utility>
#include "TCanvas.h"

#include "RooWorkspace.h"
#include "RooStats/ModelConfig.h"
#include "RooArgList.h"
#include "RooFormulaVar.h"
#include "RooRealVar.h"
#include "RooProfileLL.h"
#include "RooAbsReal.h"
#include "RooStats/HistFactory/HistFactorySimultaneous.h"
#include "RooDataSet.h"
#include "RooAbsData.h"
#include "RooPlot.h"
#include "TFile.h"
#include "TGraph.h"
#include "TLegend.h"
#include "TLine.h"
#include "TF1.h"

#include "HistFactory_BsDs.h"
#include "Bs_Ds_Fit_Conf.h"

using namespace RooFit;
using namespace RooStats;
using namespace HistFactory;

/******************************************************
* Try to understand why the errors are so variable:
*
* Consider the following:
* Fix all parameters.
* 	Perform LL scan
* Allow individual templates to vary under BB
* perform LL scans
* Perform toy studies to see if the change in likelihood corresponds to the increasing likelihood.
*******************************************************/


int doBanner()
{
  //Quick and dirty workaround to prevent RooFit's copyright message from showing
  //This is universal so no need to edit rootrc
  return 0;
}

int main(int argc, char* argv[]){



	FitterObjects FitterBB = FitterObjects();

	FitterBB.HistogramFile = "Output/Default/Source_Histograms.root";
	FitterBB.FitterConfig = "FitterConfigPhysics";
	FitterBB.OutputFolder = "Output/Default/";

	BsDs_Fitter_Parser( &FitterBB, false);
	BsDs_Fitter_Workspace( &FitterBB );
	BsDs_Fitter_Fit( &FitterBB );



	TCanvas* c1 = new TCanvas("c1", "c1", 1600, 1600);
	FitterBB.Minuit -> fit("smh");
	FitterBB.Minuit -> minos(RooArgSet(*FitterBB.poi));



	RooPlot* frame1 = FitterBB.poi->frame(RooFit::Bins(10),RooFit::Range(FitterBB.poi->getMin(), FitterBB.poi->getMax())) ;

	double poival = FitterBB.poi->getValV();
	double low = poival* 0.85;
	double high= poival* 1.15;
	FitterBB.poi->setMin(low);
	FitterBB.poi->setMax(high);


	TLegend *leg1 = new TLegend(0.65,0.73,1,1);
	leg1->SetFillColor(kWhite);
	leg1->SetLineColor(kWhite);


	TF1* x = new TF1("f0", "gaus(0)", low, high);
	x->SetParameters(1, poival, FitterBB.poi->getError() );
	x->SetLineColor(6);
	leg1->AddEntry(x, "No BeestonBarlow", "L");

	x->DrawClone();
	//leg1->Draw();

	int nComponents = FitterBB.Components.size();



	for ( int component = 0; component < nComponents; component++){
		FitterObjects FitterBB = FitterObjects();

		FitterBB.HistogramFile = "Output/Default/Source_Histograms.root";
		FitterBB.FitterConfig = "FitterConfigPhysics";
		FitterBB.OutputFolder = "Output/Default/";

		BsDs_Fitter_Parser( &FitterBB, false);

		if ( ! FitterBB.Components[component].StatActive )
				continue;

		for ( int c = 0; c <= component; c++)
			FitterBB.Components[c].ActivateStatError();


		std::string comp_name = FitterBB.Components[component].Hist->GetName();

		BsDs_Fitter_Workspace( &FitterBB );
		BsDs_Fitter_Fit( &FitterBB );



		FitterBB.Minuit -> fit("smh");
		FitterBB.Minuit -> minos(RooArgSet(*FitterBB.poi));

		double poival = FitterBB.poi->getValV();

		//FitterBB.nll_hf->plotOn(frame1, RooFit::Name((comp_name+"Legend").c_str()),  RooFit::ShiftToZero(), RooFit::LineColor(component%5 + 1), RooFit::LineStyle( int(component>=5) + 1));
		TF1* x = new TF1("f1", "gaus(0)", low, high);
		x->SetParameters(1, poival, FitterBB.poi->getError() );
		x->SetLineColor(component%5 + 1);
		x->SetLineStyle(int(component>=5) + 1 );
		x->DrawClone("SAME");
		leg1->AddEntry(x,comp_name.c_str() , "L");



	}

	//FitterBB.nll_hf->plotOn(frame1, RooFit::ShiftToZero(), RooFit::LineColor(6), RooFit::Name("NoBBLegend"));


	//frame1->SetMinimum(0);
	//frame1->SetMaximum(3);
	//frame1->Draw();
	//frame1->SetTitle("");
	leg1->Draw();

	c1->Print("Likelihood_Gaus.pdf");

}
