#include <string>
#include <cstdlib>
#include <map>
#include <utility>
#include "TCanvas.h"

#include "RooWorkspace.h"
#include "RooStats/ModelConfig.h"
#include "RooArgList.h"
#include "RooFormulaVar.h"
#include "RooRealVar.h"
#include "RooProfileLL.h"
#include "RooAbsReal.h"
#include "RooStats/HistFactory/HistFactorySimultaneous.h"
#include "RooDataSet.h"
#include "RooAbsData.h"
#include "RooPlot.h"
#include "TFile.h"
#include "TGraph.h"

#include "HistFactory_BsDs.h"
#include "Bs_Ds_Fit_Conf.h"

using namespace RooFit;
using namespace RooStats;
using namespace HistFactory;


int doBanner()
{
  //Quick and dirty workaround to prevent RooFit's copyright message from showing
  //This is universal so no need to edit rootrc
  return 0;
}

int main(int argc, char* argv[]){

	FitterObjects* FitterBB = new FitterObjects;

	FitterBB->HistogramFile = "Output/Default/Source_Histograms.root";
	FitterBB->FitterConfig = "FitterConfigPhysics";
	FitterBB->OutputFolder = "Output/Default/";

	//Build a second model with the Beeston Barlow
	BsDs_Fitter_Parser( FitterBB, true);

	BsDs_Fitter_Workspace( FitterBB );

	BsDs_Fitter_Fit( FitterBB );



	TCanvas* c1 = new TCanvas("c1", "c1", 1600, 1600);


	FitterBB -> Minuit -> hesse();		//try different minimisers
	FitterBB -> Minuit -> simplex();	//try different minimisers
	FitterBB -> Minuit -> migrad();		//try different minimisers
	FitterBB -> Minuit -> hesse();		//try different minimisers
	FitterBB -> Minuit -> minos(RooArgSet(*FitterBB->poi));

	RooProfileLL* profile_hf = (RooProfileLL*) FitterBB->nll_hf->createProfile(*FitterBB->poi);
	std::cout << profile_hf->getVal() << std::endl;


	RooPlot* frame1 = FitterBB->poi->frame(RooFit::Bins(10),RooFit::Range(FitterBB->poi->getMin(),FitterBB->poi->getMax())) ;


	FitterBB->nll_hf->plotOn(frame1, RooFit::ShiftToZero());
	profile_hf->plotOn(frame1, RooFit::LineColor(kRed) );

	frame1->SetMinimum(0);
	frame1->SetMaximum(100);
	frame1->Draw();
	TFile* f_LL = TFile::Open("LL_Out.root", "RECREATE");
	c1->Write();
	profile_hf->Write();

	c1->Print("Likelihood.pdf");

	f_LL->Close();



}
