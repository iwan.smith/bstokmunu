#include <string>
#include <cstdlib>
#include <map>
#include <utility>
#include "TCanvas.h"
#include <numeric>

#include "RooWorkspace.h"
#include "RooStats/ModelConfig.h"
#include "RooArgList.h"
#include "RooFormulaVar.h"
#include "RooRealVar.h"
#include "RooProfileLL.h"
#include "RooAbsReal.h"
#include "RooStats/HistFactory/HistFactorySimultaneous.h"
#include "RooDataSet.h"
#include "RooAbsData.h"
#include "RooPlot.h"
#include "TFile.h"
#include "TGraph.h"
#include "TLegend.h"
#include "TLine.h"
#include "TF1.h"

#include "HistFactory_BsDs.h"
#include "Bs_Ds_Fit_Conf.h"
#include "ToyHistos.h"


using namespace RooFit;
using namespace RooStats;
using namespace HistFactory;

/******************************************************
* Try to understand why the errors are so variable:
*
* Consider the following:
* Fix all parameters.
* 	Perform LL scan
* Allow individual templates to vary under BB
* perform LL scans
* Perform toy studies to see if the change in likelihood corresponds to the increasing likelihood.
*******************************************************/


int doBanner()
{
  //Quick and dirty workaround to prevent RooFit's copyright message from showing
  //This is universal so no need to edit rootrc
  return 0;
}

int main(int argc, char* argv[]){


	int nwithBB = 0;
	//if (argc == 2)
	//	nwithBB = atoi(argv[1]);

	FitterObjects FitterBB = FitterObjects();

	FitterBB.HistogramFile = "Output/Default/Source_Histograms.root";
	FitterBB.FitterConfig = "FitterConfigPhysics";
	FitterBB.OutputFolder = "Output/Default/";

	BsDs_Fitter_Parser( &FitterBB, false);


	BsDs_Fitter_Workspace( &FitterBB );
	BsDs_Fitter_Fit( &FitterBB );



	TCanvas* c1 = new TCanvas("c1", "c1", 1600, 1600);
	FitterBB.Minuit -> fit("smh");
	FitterBB.Minuit -> minos(RooArgSet(*FitterBB.poi));


	double poival = FitterBB.poi->getValV();
	double low = poival* 0.85;
	double high= poival* 1.15;
	FitterBB.poi->setMin(low);
	FitterBB.poi->setMax(high);


	TLegend *leg1 = new TLegend(0.65,0.73,1,1);
	leg1->SetFillColor(kWhite);
	leg1->SetLineColor(kWhite);


	TF1* x = new TF1("f0", "gaus(0)", low, high);
	double stddev1 = FitterBB.poi->getError();

	x->SetParameters(1, poival, FitterBB.poi->getError() );
	x->SetLineColor(6);
	leg1->AddEntry(x, "No BeestonBarlow", "L");

	x->DrawClone();
	//leg1->Draw();

	int nToy = 50;

	std::vector<double> Means;

	for ( int toy = 0; toy < nToy; toy++){
		FitterObjects FitterBB = FitterObjects();

		FitterBB.HistogramFile = "Output/Default/Source_Histograms.root";
		FitterBB.FitterConfig = "FitterConfigPhysics";
		FitterBB.OutputFolder = "Output/Default/";

		BsDs_Fitter_Parser( &FitterBB, false);

		std::vector<std::string> ToyHistograms;
		ToyHistograms.push_back(FitterBB.Components[0].Hist->GetName());
		ToyHistograms.push_back(FitterBB.Components[1].Hist->GetName());

		std::vector<double> ToyHistogramsnev;
		ToyHistogramsnev.push_back(100);
		ToyHistogramsnev.push_back(100);


		ToyHistos( "Output/Default/Source_Histograms.root", "TempInputHistogram.root",
				ToyHistograms, ToyHistogramsnev);

		FitterBB.Components[0].Sample.SetInputFile("TempInputHistogram.root");
		FitterBB.Components[1].Sample.SetInputFile("TempInputHistogram.root");


		BsDs_Fitter_Workspace( &FitterBB );
		BsDs_Fitter_Fit( &FitterBB );



		FitterBB.Minuit -> fit("smh");
		FitterBB.Minuit -> minos(RooArgSet(*FitterBB.poi));

		double poival = FitterBB.poi->getValV();

		Means.push_back(poival);


	}
	double sum = std::accumulate(Means.begin(), Means.end(), 0.0);
	double mean = sum / Means.size();

	double sq_sum = std::inner_product(Means.begin(), Means.end(), Means.begin(), 0.0);
	double stdev = std::sqrt(sq_sum / Means.size() - mean * mean);

	x = new TF1("f0", "gaus(0)", low, high);
	x->SetParameters(1, mean, stdev );
	x->SetLineColor(2);
	leg1->AddEntry(x, "Toy Width", "L");
	x->DrawClone("SAME");

	x = new TF1("f0", "gaus(0)", low, high);
	x->SetParameters(1, (mean+poival)/2., sqrt(pow(stdev,2) + pow(stddev1,2) ));
	x->SetLineColor(3);
	leg1->AddEntry(x, "NoBB + Toy", "L");
	x->DrawClone("SAME");


	FitterBB = FitterObjects();

	FitterBB.HistogramFile = "Output/Default/Source_Histograms.root";
	FitterBB.FitterConfig = "FitterConfigPhysics";
	FitterBB.OutputFolder = "Output/Default/";

	BsDs_Fitter_Parser( &FitterBB, false);

	FitterBB.Components[0].ActivateStatError();
	FitterBB.Components[1].ActivateStatError();



	BsDs_Fitter_Workspace( &FitterBB );
	BsDs_Fitter_Fit( &FitterBB );

	FitterBB.Minuit -> fit("smh");
	FitterBB.Minuit -> minos(RooArgSet(*FitterBB.poi));

	poival = FitterBB.poi->getValV();

	x = new TF1("f0", "gaus(0)", low, high);
	x->SetParameters(1, FitterBB.poi->getValV(), FitterBB.poi->getError());
	x->SetLineColor(4);
	leg1->AddEntry(x, "BB", "L");
	x->DrawClone("SAME");


	leg1->Draw();

	c1->Print( "Likelihood_Toys_2var.pdf" );


	std::cout << "Widths are:" << std::endl;
	std::cout << "No Beeston Barlow: " << stddev1 << std::endl;
	std::cout << "Width of toys: "<< stdev << std:: endl;
	std::cout << "Toys + NoBB: " << sqrt(pow(stdev,2) + pow(stddev1,2)) << std::endl;
	std::cout << "Beeston Barlow: " << FitterBB.poi->getError() << std::endl;



}
