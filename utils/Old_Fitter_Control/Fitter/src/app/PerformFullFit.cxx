#include <string>
#include <cstdlib>
#include <map>
#include <utility>
#include "TCanvas.h"

#include "HistFactory_BsDs.h"
#include "Bs2Ds_BiasTest.h"

#include "Bs_Ds_Fit_Conf.h"

int doBanner()
{
  //Quick and dirty workaround to prevent RooFit's copyright message from showing
  //This is universal so no need to edit rootrc
  return 0;
}

int main(int argc, char* argv[]){
	

	
	if ( argc == 1 ){
		std::cout << "Must be run with arguments: PerformFullFit Option [ConfigurationFile]\n";
		std::cout << "\n";
		std::cout << "Options are:\n";
		std::cout << "\t0 - Perform full analysis.\n";
		std::cout << "\t1 - Generate The Histograms.\n";
		std::cout << "\t2 - Perform The Fit.\n";
		std::cout << "\n";
		std::cout << "The configuration file is optional. It is based on: Tools/Default_Config.conf\n\n";
		return 1;
	}
	
	int step = 0;
	
	if ( argc < 2 ){
		std::cout << "You Must Specify a Config File" << std::endl;
		return 1;
	}
	
	Bs_Ds_Fit_Conf Configuration = Bs_Ds_Fit_Conf(argv[1]);


	if ( argc >=3 )
		step = atoi(argv[2]);

	

	for ( int l = 3; l < argc; l++ ){
		Configuration.ParseLine( argv[l] );
	}

	
	
	
	std::string fit_name = Configuration.GetPar_s("run_name");
	std::string fit_folder = Configuration.GetPar_s("OutputFolder");
	
	system( ("mkdir -p " + fit_folder).c_str()); //Ensure we have a folder to save the output in
	

	
	std::map<std::string, std::pair<double, double>> fit_results, fit_results_temp;
	if ( step == 0 or step == 2 or step == 3){// Fit the Histogram
		fit_results = HistFactory_BsDs( Configuration, true );
	}
	
	
	if ( step == 0 or step == 3 ){// Run the toy studies
		Bs2Ds_BiasTest( Configuration, fit_results );
	}

	if ( step == 0 or step == 4 ){// Run the toy studies
		PrintToyResults( Configuration );
	}
	
	
	
	
}
