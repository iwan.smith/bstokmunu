#include <algorithm>
#include <fstream>
#include <functional>
#include <iostream>
#include <map>
#include <memory>
#include <stdio.h>
#include <string>
#include <utility>
#include <vector>

#include "TAttText.h"
#include "TCanvas.h"
#include "TDatime.h"
#include "TF1.h"
#include "TH3.h"
#include "THStack.h"
#include "TIterator.h"
#include "TLatex.h"
#include "TLegend.h"
#include "TObjArray.h"
#include "TPaveText.h"
#include "TRandom3.h"
#include "TStopwatch.h"
#include "TStyle.h"

#include "RooAbsData.h"
#include "RooCategory.h"
#include "RooChi2Var.h"
#include "RooDataHist.h"
#include "RooDataSet.h"
#include "RooExtendPdf.h"
#include "RooFitResult.h"
#include "RooGaussian.h"
#include "RooHist.h"
#include "RooHistPdf.h"
#include "RooMCStudy.h"
#include "RooMinimizer.h"
#include "RooMinuit.h"
#include "RooMsgService.h"
#include "RooParamHistFunc.h"
#include "RooPoisson.h"
#include "RooRandom.h"
#include "RooRealSumPdf.h"
#include "RooRealVar.h"
#include "RooSimultaneous.h"
#include "RooStats/MinNLLTestStat.h"
#include "RooStats/ModelConfig.h"
#include "RooStats/ToyMCSampler.h"

#include "RooStats/HistFactory/Channel.h"
#include "RooStats/HistFactory/FlexibleInterpVar.h"
#include "RooStats/HistFactory/HistFactoryModelUtils.h"
#include "RooStats/HistFactory/HistFactoryNavigation.h"
#include "RooStats/HistFactory/HistFactorySimultaneous.h"
#include "RooStats/HistFactory/MakeModelAndMeasurementsFast.h"
#include "RooStats/HistFactory/Measurement.h"
#include "RooStats/HistFactory/ParamHistFunc.h"
#include "RooStats/HistFactory/PiecewiseInterpolation.h"
#include "RooStats/HistFactory/RooBarlowBeestonLL.h"


using namespace RooFit;
using namespace RooStats;

using std::string;
using std::cout;
using std::endl;
using std::vector;
using std::map;
using std::shared_ptr;
using std::reference_wrapper;

void doBanner()
{
}

int PerformFit();

int main()
{

	PerformFit();

	cout << "Print all root objects in memory to test for leaks" << endl;
	gDirectory->pwd();
	gDirectory->ls();

	return EXIT_SUCCESS;

}

shared_ptr<TH1F> GetHist(TFile* f, string hname, bool normalise = false, bool smooth = false);


class FitterObjects:
{
public:

	shared_ptr<TH1F> Hist;
	shared_ptr<TH1F> Hist_After;

	HistFactory::Sample Sample;

	map<string, double> NormFactors;

	bool isStatActive = false;

	vector<RooRealVar*> FitParameters;

	FitterObjects()
	{
	}


	void operator=( const shared_ptr<TH1F> &Shape )
	{
		Hist = Shape;
	}


};


class CombinedFitterObjects : public FitterObjects
{
public:

	vector< reference_wrapper<FitterObjects> > FOs;


	template<class ... FitterObjects>
	CombinedFitterObjects(FitterObjects& ... args):FOs{ args...}
	{

		cout << FOs.size();
		if (FOs.size() == 0 ) return;

		for( auto &FO: FOs)
		{

			shared_ptr<TH1F> h_temp ( (TH1F*) FO.get().Hist->Clone() );
			for ( auto &par: FO.get().FitParameters)
			{
				h_temp->Scale(par->getValV());
			}

			if ( Hist )
			{
				Hist->Add(h_temp.get());
			}
			else
			{
				Hist = h_temp;
			}

		}




	}



};


void PrintBeforeAfter(const map<string, FitterObjects>&);
void PrintFitPlot    (const shared_ptr<TH1F>, const map<string, FitterObjects>&, bool before = true);


int PerformFit()
{

   gStyle->SetOptStat(0);
   bool SetActivateStat = true;

   // Open the Histogrammer Output.
   TFile* f_hist = TFile::Open("/eos/lhcb/wg/semileptonic/Bs2KmunuAna/Histogrammer/Output/Histograms.root", "READ");

   /***********************************************
    *    Maps to store fitter objects
    ***********************************************/



   map<string, FitterObjects> FitTemplates;


   /***********************************************
    *    Load The Data
    ***********************************************/
   shared_ptr<TH1F> h_data = GetHist(f_hist, "Nominal/DsMuNu/Data/h_Bs_MCORR1_rmbg" );
   double n_data = h_data->Integral();


   /***********************************************
    *    Load The Template shapes
    ***********************************************/


   FitTemplates["DsMuNu"]      = GetHist(f_hist, "Default/DsMuNu/13774000_Ds/h_Bs_MCORR1",     true );
   FitTemplates["DsStarMuNu"]  = GetHist(f_hist, "Default/DsMuNu/13774000_DsStar/h_Bs_MCORR1", true );
   FitTemplates["DsStar0MuNu"] = GetHist(f_hist, "Default/DsMuNu/13774000_DsStar0/h_Bs_MCORR1",true );
   FitTemplates["DsStar1MuNu"] = GetHist(f_hist, "Default/DsMuNu/13774000_DsStar1/h_Bs_MCORR1",true );
   FitTemplates["DsXTauNu"]    = GetHist(f_hist, "Default/DsMuNu/13774000_DsXtau/h_Bs_MCORR1", true );
   FitTemplates["Bd_DD"]       = GetHist(f_hist, "Default/DsMuNu/MC_11876001/h_Bs_MCORR1", true, true );
   FitTemplates["Bs_DD"]       = GetHist(f_hist, "Default/DsMuNu/MC_13873201/h_Bs_MCORR1", true, true );
   FitTemplates["Bu_DD"]       = GetHist(f_hist, "Default/DsMuNu/MC_12875601/h_Bs_MCORR1", true, true );
   FitTemplates["BcJpsiDs"]    = GetHist(f_hist, "sWeight/DsMuNu/MC_14175001/h_Bs_MCORR1", true );
   FitTemplates["FakeMu"]      = GetHist(f_hist, "sWeight/DsMuNu/FakeMu/h_Bs_MCORR1", false, true );

   f_hist->Close();
   //delete f_hist;


   /***********************************************
    *    Setup the Fitter
    ***********************************************/

   cout << FitTemplates["FakeMu"].Hist->Integral() << endl;

   HistFactory::Measurement Ds_Fitter("Bs2DsMuNu_Fitter", "Fitter for B_s #rightarrow D_s #mu #nu");
   Ds_Fitter.SetOutputFilePrefix("results/Bs_DsMuNu_Fit");
   Ds_Fitter.SetExportOnly(true);
   Ds_Fitter.SetPOI("Ds_Yield");
   Ds_Fitter.SetLumi(1.0);
   Ds_Fitter.SetLumiRelErr(0.0);

   /***********************************************
    *    Setup the Channel and assign data
    ***********************************************/

   RooStats::HistFactory::Channel FitChannel("MCORR");
   FitChannel.SetStatErrorConfig( 1e-5, "Pois" );
   FitChannel.SetData( (TH1F*)h_data->Clone() );


   /***********************************************
    *    Create Samples from the templates
    ***********************************************/


   // HistFactory::Sample Assumes ownership of the histogram.
   // Clone it when calling SetHisto otherwise horrible stuff will happen when destructors are called.

   for( auto &T: FitTemplates)
   {
	   string sampleName = T.first;
	   FitterObjects& FO = T.second;


	   TH1F*   Histogram  = FO.Hist.get();
	   Histogram->SetTitle(sampleName.c_str());

	   FO.Sample = HistFactory::Sample(sampleName);
	   FO.Sample.SetHisto( (TH1F*)Histogram->Clone( ( sampleName+"_forFit").c_str() ));
	   FO.Sample.SetNormalizeByTheory(false);

   }

   // Use the .at() to access elements as it will cause a error. [] will call the constructor. Fail fast and quickly!
   FitTemplates.at("DsMuNu")     .Sample.AddNormFactor("Ds_Yield", n_data*0.25 , 0.0, n_data);

   FitTemplates.at("DsStarMuNu") .Sample.AddNormFactor("DsStar_Yield", n_data*0.75 , 0.0, n_data);

   FitTemplates.at("DsStar0MuNu").Sample.AddNormFactor("DsStar0_Fraction", 0.273 , 0.273, 0.273, true);
   FitTemplates.at("DsStar0MuNu").Sample.AddNormFactor("DsStarN_Yield",    n_data*0.05, 0, n_data);

   FitTemplates.at("DsStar1MuNu").Sample.AddNormFactor("DsStar1_Fraction", 0.727 , 0.727, 0.727, true);
   FitTemplates.at("DsStar1MuNu").Sample.AddNormFactor("DsStarN_Yield",    n_data*0.05, 0, n_data);

   FitTemplates.at("DsXTauNu")   .Sample.AddNormFactor("DsXTauNu_Yield", n_data*0.05 , 0.0, n_data);

   FitTemplates.at("Bd_DD")      .Sample.AddNormFactor("BX_DD_Yield", 1000 , 1000, 1000);
   FitTemplates.at("Bd_DD")      .Sample.AddNormFactor("Bd_DD_Fraction", 0.4 , 0.4, 0.4, true);

   FitTemplates.at("Bu_DD")      .Sample.AddNormFactor("BX_DD_Yield", 1000 , 1000, 1000);
   FitTemplates.at("Bu_DD")      .Sample.AddNormFactor("Bu_DD_Fraction", 0.4 , 0.4, 0.4, true);

   FitTemplates.at("Bs_DD")      .Sample.AddNormFactor("BX_DD_Yield", 1000 , 1000, 1000);
   FitTemplates.at("Bs_DD")      .Sample.AddNormFactor("Bs_DD_Fraction", 0.2 , 0.2, 0.2, true);

   FitTemplates.at("BcJpsiDs")   .Sample.AddNormFactor("Bc_Yield", 0.01*n_data, 0, n_data);

   FitTemplates.at("FakeMu")     .Sample.AddNormFactor("FakeMu_Yield", 1 , 1, 1, true);

   if ( SetActivateStat)
   {
	   FitTemplates.at("DsMuNu")      .Sample.ActivateStatError();
	   FitTemplates.at("DsStarMuNu")  .Sample.ActivateStatError();
	   FitTemplates.at("DsStar0MuNu") .Sample.ActivateStatError();
	   FitTemplates.at("DsStar1MuNu") .Sample.ActivateStatError();
	   FitTemplates.at("DsXTauNu")    .Sample.ActivateStatError();
	   FitTemplates.at("BcJpsiDs")    .Sample.ActivateStatError();
	 //Samples.at("Bd_DD")       .ActivateStatError();
	 //Samples.at("Bs_DD")       .ActivateStatError();
	 //Samples.at("Bu_DD")       .ActivateStatError();
	 //Samples.at("FakeMu"]      .ActivateStatError();

   }


   /***********************************************
    *    Add the samples to the channel
    ***********************************************/


   for( auto &S: FitTemplates)
   {
	   FitChannel.AddSample(S.second.Sample);
   }

   Ds_Fitter.AddChannel(FitChannel);
   Ds_Fitter.PrintTree();
   //Ds_Fitter.CollectHistograms();

   /***********************************************
    *    Build The Workspaces and models
    ***********************************************/



   RooWorkspace* ws = HistFactory::MakeModelAndMeasurementFast(Ds_Fitter);
   ModelConfig* mc = (ModelConfig*) ws->obj("ModelConfig");

   ((RooRealVar*)(mc->GetNuisanceParameters()->find("Lumi")))->setConstant(true);


   RooSimultaneous* model = (RooSimultaneous*) mc->GetPdf(); //ws->pdf("simPdf");
   RooDataSet* data = (RooDataSet*) ws->data("obsData");

   RooRealVar* poi = (RooRealVar*) mc->GetParametersOfInterest()->createIterator()->Next();
   std::cout << "Param of Interest: " << poi->GetName() << std::endl;

   shared_ptr<HistFactory::HistFactorySimultaneous> model_hf( new HistFactory::HistFactorySimultaneous( *model ) );

   shared_ptr<RooAbsReal> nll_hf( model_hf->createNLL( *data ,Offset(kTRUE) ) );   //Add external constarints in the argument.

   shared_ptr<RooMinuit> minuit_hf( new RooMinuit( *nll_hf ) );
   minuit_hf->setErrorLevel(0.5);
   minuit_hf->setStrategy(2);



   /***********************************************
    *    Run The Fit
    ***********************************************/



   minuit_hf->fit("smh");
   minuit_hf->minos(RooArgSet(*poi));

   shared_ptr<RooFitResult> results (minuit_hf->save() );


	for( auto &T: FitTemplates)
	{
	   string sampleName = T.first;
	   FitterObjects& FO = T.second;


	   for( auto& NF: FO.Sample.GetNormFactorList() )
	   {
		   std::string NFName = NF.GetName();
		   RooRealVar* FitPar = (RooRealVar*)mc->GetNuisanceParameters()->find(NFName.c_str());

			if ( not FitPar)
			// If the paramtere is a nullpointer use the POI
			{
			   FitPar = (RooRealVar*) mc->GetParametersOfInterest()->createIterator()->Next();
			}
			FO.FitParameters.push_back( FitPar );

	   }
	}






   HistFactory::HistFactoryNavigation HF_Nav( mc );

   for( auto &T: FitTemplates)
   {
	   string sampleName = T.first;
	   FitterObjects& FO = T.second;

	   FO.Hist_After = shared_ptr<TH1F>( (TH1F*)HF_Nav.GetSampleHist("MCORR", sampleName, sampleName+ "_after") );


   }


   PrintBeforeAfter(FitTemplates);

   map<string, FitterObjects> PlotTemplates;
   PlotTemplates["B_{s} #rightarrow D_{s} #mu #nu"]          = CombinedFitterObjects( FitTemplates.at("DsMuNu") );
   PlotTemplates["B_{s} #rightarrow D_{s}* #mu #nu"]         = CombinedFitterObjects( FitTemplates.at("DsStarMuNu") );
   PlotTemplates["B_{s} #rightarrow D_{s}*^{0,1} #mu #nu"]   = CombinedFitterObjects( FitTemplates.at("DsStar1MuNu"), FitTemplates.at("DsStar1MuNu") );
   PlotTemplates["B_{s} #rightarrow D_{s} #tau #nu"]         = CombinedFitterObjects( FitTemplates.at("DsXTauNu") );
   PlotTemplates["B_{q} #rightarrow D_{s} D_q^{(*)} #mu #nu"]= CombinedFitterObjects( FitTemplates.at("Bu_DD"), FitTemplates.at("Bd_DD"), FitTemplates.at("Bs_DD") );
   PlotTemplates["B_{c} #rightarrow D_{s} J/#psi"]= CombinedFitterObjects( FitTemplates.at("Bu_DD"), FitTemplates.at("Bd_DD"), FitTemplates.at("Bs_DD") );
   PlotTemplates["FakeMu"]                               = CombinedFitterObjects( FitTemplates.at("FakeMu") );

   PrintFitPlot(h_data, PlotTemplates);
   return 1;

}



shared_ptr<TH1F> GetHist(TFile* f, string hname, bool normalise, bool smooth)
{

	cout << "Trying to load histogram: " << hname ;


	shared_ptr<TH1F> hist( (TH1F*) f->Get(hname.c_str()) );

	cout << " Stored at: " << hist.get() << endl;

	hist->SetDirectory(0);
	hist->ClearUnderflowAndOverflow();
	if ( not hist->GetSumw2N() ) hist->Sumw2();

	int nBinsX = hist->GetNbinsX();

	for(int BX = 1; BX <= nBinsX; BX++)
	{
		if ( hist->GetBinContent(BX) <= 0.0 )
		{
			hist->SetBinContent(BX, 1e-3);
			hist->SetBinError(BX, 1e-3);
		}
	}

	if ( smooth )
	{
		hist->Smooth();
	}

	if (normalise)
	{
		hist->Scale( 1.0 / hist->Integral() );
	}

	return hist;
}


void PrintBeforeAfter(const map<string, FitterObjects>& FitTemplates)
{

	shared_ptr<TCanvas> c_bef_aft ( new TCanvas("c_bef_aft", "c_bef_aft", 1600, 1600) );

	shared_ptr<TPad> pad1 ( new TPad("pad1.1", "The pad 80% of the height",0.0,0.3,1.0,1.0) );
	shared_ptr<TPad> pad2 ( new TPad("pad2.2", "The pad 20% of the height",0.0,0.0,1.0,0.3) );

	shared_ptr<TPaveText> pt ( new TPaveText(.65, .85, .95, .95, "NDC") );
	pt->AddText("Before")->SetTextColor(1);
	pt->AddText("After")->SetTextColor(2);
	pt->SetBorderSize(0);
	pt->SetFillColor(0);




	c_bef_aft->Print("CompareBeforeAfter.pdf[");

	for( auto &T: FitTemplates)
	{
		const string& sampleName= T.first;
		const FitterObjects& FO = T.second;

		shared_ptr<TH1F> h_before ( (TH1F*)FO.Hist->Clone() );
		shared_ptr<TH1F> h_after  ( (TH1F*)FO.Hist_After->Clone() );

		h_before->Scale( 1.0/h_before->Integral() );
		h_before->SetMarkerColor(1);
		h_before->SetLineColor(1);

		h_after->Sumw2();
		h_after->SetMarkerColor(2);
		h_after->SetLineColor(2);
		h_after->Scale( 1.0/h_after->Integral() );

		pad1->cd();
		h_before->Draw();
		h_after->Draw("SAME");
		pt->Draw();

		c_bef_aft->cd();
		pad1->Draw();

		shared_ptr<TH1F> h_ratio ( (TH1F*)h_before->Clone() );
		h_ratio->SetTitle("(After-Before)/#sigma_{Before}");
		h_ratio->SetMaximum(3);
		h_ratio->SetMinimum(-3);
		for( int bin = 1 ; bin <= h_ratio->GetSize()-2; bin++ )
		{
			h_ratio->SetBinContent(bin, (h_after->GetBinContent(bin) - h_before->GetBinContent(bin))/h_before->GetBinError(bin) );
			h_ratio->SetBinError(bin, 1e-10);
		}
		pad2->cd();
		h_ratio->Draw();

		c_bef_aft->cd();
		pad2->Draw();


		c_bef_aft->Print("CompareBeforeAfter.pdf");
	}

	c_bef_aft->Print("CompareBeforeAfter.pdf]");

}



void PrintFitPlot(const shared_ptr<TH1F> h_data, const map<string, FitterObjects>& FitTemplates, bool before)
{


	vector<shared_ptr<TH1F>> FitHistograms;

	for (auto &T: FitTemplates)
	{

		const string& sampleName= T.first;
		const FitterObjects& FO = T.second;


		shared_ptr<TH1F> Hist ( (TH1F*)FO.Hist->Clone() );

		Hist->SetTitle(sampleName.c_str());

		if ( not before)
		{
			Hist = shared_ptr<TH1F>( (TH1F*)FO.Hist_After->Clone() );
		}

		for( auto NF: FO.FitParameters )
		{
			Hist->Scale(NF->getValV());
		}
		FitHistograms.push_back(Hist);
	}


	std::sort(FitHistograms.begin(), FitHistograms.end(),
		[](shared_ptr<TH1F> a, shared_ptr<TH1F> b) -> bool
		{
		return a->Integral() < b->Integral();
		}
	);


	THStack* stack = new THStack("Stack", "Fit to data");
	TLegend *leg_1 = new TLegend(0.11, 0.5, 0.4, 0.8);
	leg_1->SetBorderSize(0);
	leg_1->AddEntry(h_data.get(), "Data", "lep");

	shared_ptr<TH1F> h_StackPulls ( (TH1F*)h_data->Clone() );
	h_StackPulls->Reset();
	h_StackPulls->SetTitle("( Data - Template ) / #sigma_{Data}");

	int nHist = FitHistograms.size();

	for (int x = 0; x < nHist ; x++){
	  //*Histograms.at(x) = NuisanceVars.at(x)->getVal() * *Histograms.at(x);
		FitHistograms.at(x)->SetFillColor(7-x);
		FitHistograms.at(x)->Sumw2(false);
		FitHistograms.at(x)->SetMarkerColor(1);
		FitHistograms.at(x)->SetLineColor(1);

		stack->Add( FitHistograms.at(x).get() );
		*h_StackPulls = *h_StackPulls + *FitHistograms.at(x);

		leg_1->AddEntry(FitHistograms.at(x).get(), FitHistograms.at(x)->GetTitle(), "f");
	}

	TCanvas *c1 = new TCanvas("c1", "c1", 1600, 1600);

	TPad *pad1 = new TPad("pad1", "The pad 80% of the height",0.0,0.3,1.0,1.0);
	pad1->cd();
	stack->Draw();

	leg_1->Draw();
	h_data->SetMarkerSize(3);
	h_data->SetLineWidth(2);
	h_data->SetLineColor(kBlack);
	h_data->Draw("SAME");


	TPad *pad2 = new TPad("pad2", "The pad 20% of the height",0.0,0.0,1.0,0.3);
	pad2->cd();
	for ( int bin = 1; bin <= h_StackPulls->GetSize()-2; bin++){
	  h_StackPulls->SetBinContent(bin, (h_data->GetBinContent(bin) - h_StackPulls->GetBinContent(bin))/h_data->GetBinError(bin) );
	  h_StackPulls->SetBinError(bin,1e-10);
	  //h_StackPulls->Add(h_data, h_StackPulls, 1.0, -1.0);
	}

	h_StackPulls->Draw();

	c1->cd();
	pad1->Draw();
	pad2->Draw();

	c1->Print("FitPlot.pdf");


}


