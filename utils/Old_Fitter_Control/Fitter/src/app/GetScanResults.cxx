#include <string>
#include <cstdlib>
#include <map>
#include <utility>
#include "TCanvas.h"
#include "TGraph.h"
#include "HistFactory_BsDs.h"

#include "Bs_Ds_Fit_Conf.h"

int doBanner()
{
  //Quick and dirty workaround to prevent RooFit's copyright message from showing
  //This is universal so no need to edit rootrc
  return 0;
}

int main(int argc, char* argv[]){
	


	std::string ConfFile = "Tools/Current_Config.conf";
	std::string HistConfFile = "";	

	Bs_Ds_Fit_Conf Configuration = Bs_Ds_Fit_Conf(ConfFile);
	
	int npoint = 11;
	
	double q2cut[12] = {0};
	double Yield[12] = {0};
	double Err[12] = {0};

	for ( int q2 = 0; q2 <npoint; q2++ ){
	
		std::string q2cutstr = std::to_string(q2);
		if (q2cutstr.size() == 1 )
			q2cutstr = "0"+q2cutstr;
		
		std::string Confline = "str OutputFolder = Output/Q2Cut_" + q2cutstr + "/";
		
		Configuration.ParseLine( Confline );
		
		std::map<std::string, std::pair<double, double>> fit_results;
		//MakeHistogramFile_Shape( Configuration );
		fit_results = HistFactory_BsDs( Configuration, false );

		
		q2cut[q2] = q2;
		Yield[q2] = fit_results["Ds_Yield"].first;
		Err  [q2] = fit_results["Ds_Yield"].second / fit_results["Ds_Yield"].first;
		
	
	}
	
	TCanvas* c1 = new TCanvas("c1", "c1", 1600, 1600);
	
	for ( int q2 = 0; q2 <npoint; q2++ ) 
		std::cout << q2cut[q2] << "\t" << Yield[q2] << "\t" << Err[q2] << std::endl;
	
	
	TGraph* g_Yield = new TGraph(npoint, q2cut, Yield);
	TGraph* g_Err = new TGraph(npoint, q2cut, Err);
	
	c1->Clear();
	g_Yield->Draw();
	
	c1->Print("q2scan.pdf");
	
	c1->Clear();
	g_Err->Draw();
	c1->Print("q2scanErr.pdf");
	
	
	
}
