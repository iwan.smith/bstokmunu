#ifndef BIASTEST
#define BIASTEST 1

#include "THStack.h"
#include "Bs_Ds_Fit_Conf.h"


void Bs2Ds_BiasTest(
		const char*, const char*, const char*, std::string, std::string, int,
		std::map<std::string, std::pair<double, double>>
		);
	
void Bs2Ds_BiasTest( Bs_Ds_Fit_Conf, std::map<std::string, std::pair<double, double>> );


void PrintToyResults( const char*, std::string );
void PrintToyResults( Bs_Ds_Fit_Conf );
#endif
