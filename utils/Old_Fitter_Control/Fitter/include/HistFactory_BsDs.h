#ifndef HISTFAC_SHAPE
#define HISTFAC_SHAPE 1

#include "Bs_Ds_Fit_Conf.h"
#include "RooStats/ModelConfig.h"
#include "RooStats/HistFactory/Sample.h"
#include "RooStats/HistFactory/Measurement.h"
#include "RooStats/HistFactory/HistFactorySimultaneous.h"
#include "RooMinuit.h"
#include "THStack.h"
#include "RooWorkspace.h"


using namespace RooFit;
using namespace RooStats;
using namespace HistFactory;


std::map<std::string, std::pair<double, double>> HistFactory_BsDs(const char*, std::string, bool saveplots = false, std::string save_folder = "");
std::map<std::string, std::pair<double, double>> HistFactory_BsDs(	Bs_Ds_Fit_Conf, bool );


struct FitterHistogram{

	TH1F* Hist = nullptr;
	TH1F* Hist_After = nullptr;
	TH1F* Hist_Scaled = nullptr;
	RooStats::HistFactory::Sample Sample;
	std::string Title;

	std::vector<RooRealVar*> Vars;

	bool StatActive = false;

	void ActivateStatError( bool activate = true);
	double Yield();
	double Error(bool Verbose = false);

	~FitterHistogram();

};



struct FitterObjects{


	std::string HistogramFile;
	std::string FitterConfig;
	std::string OutputFolder;

	std::vector<FitterHistogram> Components;
	std::vector<RooRealVar*> Vars;

	std::map< std::string, THStack* > CombinedModel;

	RooDataSet* data;
	TH1F* Data;

	std::string POIName;
	RooRealVar* poi;


	RooStats::HistFactory::Measurement Measurement;
	RooStats::HistFactory::Channel Channel;
	RooWorkspace* Workspace;
	ModelConfig* MC;
	RooSimultaneous* Simultaneous;
	HistFactorySimultaneous* HFSimultaneous;
	RooAbsReal* nll_hf;
	RooMinuit* Minuit;
	RooFitResult* results;

	std::map<std::string, std::pair<double, double>> ResultTuple;

	std::vector<FitterHistogram>::iterator begin();
	std::vector<FitterHistogram>::iterator end();

	~FitterObjects();

};





void BsDs_Fitter_Parser( FitterObjects*, bool ActiveStat = true );

void BsDs_Fitter_Workspace(FitterObjects*);

void BsDs_Fitter_Fit(FitterObjects*);

void BsDs_Fitter_SaveTuple(FitterObjects*);

void BsDs_Fitter_SaveBeforeAfter( FitterObjects* );

void BsDs_Fitter_Plotter( FitterObjects*, bool before_BB = true );

void BsDs_Fitter_Print( FitterObjects* Fitter);



#endif
