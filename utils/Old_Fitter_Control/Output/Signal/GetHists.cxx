#include "TCanvas.h"
#include "TH1F.h"
#include "TFile.h"
#include "TList.h"

#include <iostream>
void DeleteIfExists( TFile* file, const char* hname){

	if ( file->Get( hname) ){
		//Delete the histogram in the file if it exists
		file->Delete( ((std::string)hname + ";1").c_str() );
	}

}

void SaveHistogram(TFile* file, TH1F* Hist){
	
	
	int n_bins = Hist->GetSize() -2;
	
	file->cd();
	
	Hist->Sumw2();
	
	Hist->SetBinContent(0, 0);
	Hist->SetBinContent(n_bins+1, 0);
	Hist->SetBinError(0, 0);
	Hist->SetBinError(n_bins+1, 0);
	/*
	if ( UseSmoothing ){
		TH1F* h_temp = (TH1F*) Hist->Clone();
		for ( int bin = 2; bin < n_bins; bin++ ){
			Hist->SetBinContent(bin, 0.25 * h_temp->GetBinContent(bin-1) + 0.5 * h_temp->GetBinContent(bin) + 0.25 * h_temp->GetBinContent(bin+1) );
			Hist->SetBinError(bin, sqrt(pow(0.25 * h_temp->GetBinError(bin-1), 2) + pow( 0.5 * h_temp->GetBinError(bin), 2) + pow( 0.25 * h_temp->GetBinError(bin+1), 2)) );
		}
		Hist->SetBinContent(1, 0.65 * h_temp->GetBinContent(1) + 0.35 * h_temp->GetBinContent(2) );
		Hist->SetBinError(1, sqrt( pow( 0.65 * h_temp->GetBinError(1), 2) + pow( 0.35 * h_temp->GetBinError(2), 2)) );

		Hist->SetBinContent(n_bins, 0.65 * h_temp->GetBinContent(n_bins) + 0.35 * h_temp->GetBinContent(n_bins-1) );
		Hist->SetBinError(n_bins, sqrt( pow( 0.65 * h_temp->GetBinError(n_bins), 2) + pow( 0.35 * h_temp->GetBinError(n_bins-1), 2)) );
		
	}
	*/

	Hist->SetDirectory(0);
	
	for ( int bin = 1; bin <= n_bins; bin++ ){
		if ( Hist->GetBinContent(bin) == 0 ){
			Hist->SetBinContent( bin, 1e-10 );
			Hist->SetBinError( bin, 1e-10 );
		}
	}
	
	
	DeleteIfExists(file, Hist->GetName());
	Hist->Write();
	
	TH1F* SmoothHist = (TH1F*)Hist->Clone();
	SmoothHist->SetName( (std::string( SmoothHist->GetName() ) + "_Smooth" ).c_str() );
	SmoothHist->Smooth();
	DeleteIfExists(file, SmoothHist->GetName() );
	SmoothHist->Write();
	
	
	double scale_factor = 1.0 / Hist->Integral();
	Hist->Scale( scale_factor );
	Hist->SetName( (std::string( Hist->GetName() ) + "_norm" ).c_str() );
	DeleteIfExists(file, Hist->GetName() );
	Hist->Write();
	
	scale_factor = 1.0 / SmoothHist->Integral();
	SmoothHist->Scale( scale_factor );
	SmoothHist->SetName( (std::string( SmoothHist->GetName() ) + "_norm" ).c_str() );
	DeleteIfExists(file, SmoothHist->GetName() );
	SmoothHist->Write();
	
	

	
	
	}



void GetHists(){
	
	
	TFile* f_out = TFile::Open("Source_Histograms.root", "RECREATE");
	
	TFile* f_in_MC  = TFile::Open("root://eoslhcb.cern.ch//eos/lhcb/wg/semileptonic/Bs2KmunuAna/Tuples/SignalFit_input/Histos_allMCfiles_newBDToptimization.root", "READ");
	TFile* f_in_Dat = TFile::Open("root://eoslhcb.cern.ch//eos/lhcb/wg/semileptonic/Bs2KmunuAna/Tuples/SignalFit_input/Histos_datafile_newBDToptimization.root", "READ");
	
	TH1F* h_Dat = (TH1F*)f_in_Dat->Get("OS_2012_Bs_MCORR")->Clone("Data");
	
	SaveHistogram(f_out, h_Dat);
	
	TList* ListOfHists = f_in_MC->GetListOfKeys();
	
	int nhist = ListOfHists->GetEntries();
	
	for ( int it = 0; it < nhist; it++ ){
		
		const char* h_name = ListOfHists->At(it)->GetName();
		
		TH1F* h_it = (TH1F*)f_in_MC->Get( h_name )->Clone();
		
		SaveHistogram(f_out, h_it);
		
	}
	
	f_out->Close();
	
	
	
	
	
	
	
	
	
	
	
}
