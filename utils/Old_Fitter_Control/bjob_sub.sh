#!/bin/bash

#
# Documentation is here:
# https://twiki.cern.ch/twiki/bin/view/Main/BatchJobs

# Lxplus Batch Job Script

echo "Submitting Job To The Batch System"

echo "
cd $(dirname `pwd`)
pwd
source /cvmfs/lhcb.cern.ch/lib/lhcb/LBSCRIPTS/prod/InstallArea/scripts/LbLogin.sh -c x86_64-slc6-gcc49-opt
source /cvmfs/lhcb.cern.ch/lib/lhcb/LBSCRIPTS/prod/InstallArea/scripts/SetupProject.sh ROOT 6.06.02

#Put our commands below me to perform your PIDCalib analysis



" | tee /dev/tty | bsub -q 1nd -J Bs_Ds_Fitter


