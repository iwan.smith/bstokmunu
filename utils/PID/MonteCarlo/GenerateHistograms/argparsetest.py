import argparse

parser = argparse.ArgumentParser(description='ReWeight MonteCarlo to agree with data, and create PID Histograms from MC.')
parser.add_argument("--Mag", "-M", required=True, choices=["Up", "Down"], )
parser.add_argument("--Particle", "-P", required=True, choices=["K", "Mu", "Pi"], )
parser.add_argument("--test", "-T", type = int, default=-1)
args = parser.parse_args()


