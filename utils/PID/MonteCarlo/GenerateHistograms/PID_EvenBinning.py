from iminuit import Minuit
import ROOT
import pickle
import numpy as np

from math import pow


def GetBinning(nbins, low, high, data, weights=None):
    
    nev = data.shape[0]
    
    granul = nev/10
    
    Hist   = ROOT.TH1F("h_Binning",   "", granul, low, high)
    
        
    if weights == None:
        weights = np.ones(nev)
        
    Hist.FillN(nev, data.astype(np.float), weights.astype(np.float))
    Hist.SetMinimum(0)
    
    
    
    Integral = Hist.Integral()
    
    
    bins = [0.0 ]        
    
    def f(bin_1, bin_2):
        Sum = abs(Hist.Integral(int(bin_1*granul), int(bin_2*granul)) - Integral/nbins)
        return Sum
    
    
    for x in range(nbins-1):
        m = Minuit(f,
                   bin_1 = bins[x],     fix_bin_1=True,
                   bin_2 = bins[x]+0.03, error_bin_2 = 1.0, limit_bin_2=(bins[x], 1.0),
                   print_level=1
                   )
        m.migrad()
        
        bins += [m.values['bin_2']] 
            
    
    bins = np.asarray(bins + [1.0])
    
    bins *= (high-low)
    bins += low
    return bins



if __name__ == "__main__":
    
    
    Granularity = 100000
        
    import pickle


    Target_tuple = ()
    f_data = open( "DSt_K_MagDown_Strip20.pkl", "rb")
    
    while True:
        try:
            
            Target_tuple += ( pickle.load(f_data), )
            print "Opened", Target_tuple[-1].shape[0], "Events"
        except:
            break
    f_data.close()
    
    
    try:
        Target = np.concatenate(Target_tuple)
        nev = Target.shape[0]
        print "Merged", nev, "Events"
    except:
        print "\nData has not been saved in pickle file."
        print "    Please Run PID_PickleData.py"
        print exit()
    
        
    Hist = ROOT.TH1F("Hist", "", Granularity, 0, 100000)
    Hist.FillN(nev, Target[:,0].astype(np.float), Target[:,3].astype(np.float))
    
    Integral = Hist.Integral()
    
    
    def FindBinning( x ):
        
        args=x*Granularity
        args =args.astype(np.int)
        
        nbounds = len(args)
        nbins   = len(args)+1
        
        Sum = 0
        
        for it in range(nbounds):
            Expecetd_Left  = Integral * (it+1) / nbins
            Expecetd_Right = Integral - Expecetd_Left
            
            S  = abs( Hist.Integral(1,          args[it]    ) - Expecetd_Left  )
            S *= abs( Hist.Integral(args[it]+1, Granularity ) - Expecetd_Right )
                
            Sum += S
        
        print args, Sum
            
        return Sum
        
    from scipy.optimize import basinhopping
    
    BH = basinhopping(FindBinning, np.linspace(0.2, 0.8, 4), stepsize=0.001, T=100000, interval=20)
    print BH
    
    #B2 = BH["x"]

    #BH = basinhopping(FindBinning, B2, stepsize=0.01, T=1000, interval=1000)
    #print BH
    
    Bins = np.concatenate(([0], BH["x"], [1]))
    
    print Bins
    
    Bins = Bins*100000
    
    Hist2 = ROOT.TH1F("Hist2", "", 5, Bins)
    Hist2.FillN(nev, Target[:,0].astype(np.float), Target[:,3].astype(np.float))

    
    