from PID_Config import Particles
import ROOT

import numpy as np
from root_numpy import tree2array, root2array
import pickle


"""
Parse the arguments
"""

def ParseArguments():
    import argparse
    
    parser = argparse.ArgumentParser(description='Pickle data for use by the reweighter')
    
    parser.add_argument("--Mag", "-M",      metavar="Up,Down", required=True, choices=["Up", "Down"]   , help="Specify magnet polarity." )
    parser.add_argument("--Particle", "-P", metavar="K,Pi,Mu", required=True, choices=["K", "Mu", "Pi"], help="Specify particle to treat." )
    parser.add_argument("--Prescale", type=float,  default=0.02,   help="Apply a Prescale to data.")
    parser.add_argument("--NFiles", "-N", type=int, default=-1, help="Exit after processing N NTuples")
    
    args = parser.parse_args()
        
    return args


import pickle
import re

def ReadData( ParticleConfig, Step=None ):

    
    
    Particle_Data = Particle["Part_Data"]
    branches_Data = [
        "{0}_P".format(Particle_Data), 
        "{0}_Eta".format(Particle_Data), 
        "nTracks*1.0", 
        "nsig_sw"
    ]


    print "\nLoading data from NTuples\n"
   
    
    PID_K  = ROOT.TChain( ParticleConfig["DataTree"] )
    n_Data = PID_K.Add( ParticleConfig["DataPath"] )
    print "Added", n_Data, "Data NTuples\n" 

    FileList = PID_K.GetListOfFiles()
    
    Files = [f.GetTitle() for f in FileList]


    PickleFile = open(ParticleConfig["PickleFile"], "wb" )
        
    for it, FName in enumerate(Files):
        
        if it == args.NFiles:
            break
        
        try:
            print "\nOpening file:", FName
            File = ROOT.TFile.Open(FName)
            Tree = File.Get(ParticleConfig["DataTree"])
            print "    Number of events:", Tree.GetEntries(), "Skipping every", Step, "events"
        except:
            print "    Failed to open file! Skipping."
            continue

        Data = tree2array(Tree, branches=branches_Data, step=Step)
        Target = Data.view(( Data.dtype[0], len( Data.dtype.names ) ))

        
        print "    Saving", Target.shape[0], "to:", ParticleConfig["PickleFile"] 
        pickle.dump( Target, PickleFile)

    PickleFile.close()
    
if __name__ == '__main__':
    
    args=ParseArguments()

    Particle =Particles(args.Mag, args.Particle)
    
    Step = int( 1.0/ args.Prescale )
    ReadData(Particle, Step)
    
  