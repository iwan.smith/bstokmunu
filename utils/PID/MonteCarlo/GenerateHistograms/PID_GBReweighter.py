#!/usr/bin/python

import ROOT
import numpy as np
from root_numpy import tree2array


"""
Parse the arguments
"""

import argparse

parser = argparse.ArgumentParser(description='ReWeight MonteCarlo to agree with data, and create PID Histograms from MC.')

parser.add_argument("--Mag", "-M",      metavar="Up,Down", required=True, choices=["Up", "Down"]   , help="Specify magnet polarity." )
parser.add_argument("--Particle", "-P", metavar="K,Pi,Mu", required=True, choices=["K", "Mu", "Pi"], help="Specify particle to treat." )
parser.add_argument("--Prescale", default=0.02, type=float, help="Apply a prescale to Data")
parser.add_argument("--Stop", type=int,  default=None,   help="Stop loading data after N events.")
parser.add_argument("--Pickle",  action="store_true", help="Load/Save the data from a Pickle file")
parser.add_argument("--SavePlots", action="store_true", help="Generate plots validating the method ")
parser.add_argument("--Verbose", "-V",  action="store_true", help="Verbose mode. Print bin contents etc.")

args = parser.parse_args()

from PID_Config import Particles
Particle = Particles(args.Mag, args.Particle)




""""
Test for hep_ml
"""

try:
    from hep_ml.reweight import GBReweighter
except:
    print "Please install hep_ml"
    exit(1)



"""
Load the Performance histograms from the output of PIDCalib
Parse the histogram names to get the cuts
"""





File_PerfHist = ROOT.TFile.Open(Particle["PIDHistFile"])

PIDCuts = []
PIDHists = []

N_PID_Cuts = 0

print "\nInvestigating PID Cuts:"
for key in File_PerfHist.GetListOfKeys():
    name = key.GetName()
    if not ("_All" in name ):
        continue
    if "TotalHist" in name:
        continue
    if "PassedHist" in name:
        continue
    
    name = name.replace("{0}_".format(Particle["Part_Data"]), "")     # Remove the first label
    name = name.replace("_All", "")                                   # Remove the _All at the end  
    name = name.replace("DLL", "{0}_PID".format(Particle["Part_MC"][0])) # Change DLL labels to match MC branches
    name = name.replace("IsMuon", "{0}_isMuon".format(Particle["Part_MC"][0]) )
    PIDCuts.append(name)
    print "PID Cut:", name

    Hist = key.ReadObj().Clone()
    Hist.SetDirectory(0)
    PIDHists.append(Hist)
    
    N_PID_Cuts += 1
    
File_PerfHist.Close()



"""
Load the data either from a pickle file or from the NTuples
"""


# Start by loading the data
import pickle


Target_tuple = ()
f_data = open( Particle["PickleFile"], "rb")

while True:
    try:
        
        Target_tuple += ( pickle.load(f_data), )
    except:
        break
f_data.close()


try:
    Target = np.concatenate(Target_tuple)
except:
    print "\nData has not been saved in pickle file."
    print "    Please Run PID_PickleData.py"
    print exit()








Particle_Data = Particle["Part_Data"]


try:        
    Source = pickle.load( open( Particle["PickleFileMC"], "rb" ) )
    print "\nLoading data from pickle file"
except:
    
    Source = ()
    
    for P in Particle["Part_MC"]:
        
        Particle_MC = P
        
        branches_MC = [
            "{0}_P".format(Particle_MC), 
            "{0}_ETA".format(Particle_MC), 
            "nTracks*1.0"
        ]
    
        for cut in PIDCuts:
            branches_MC += ["( {Cut} ) * 1.0".format(Cut=cut).replace(Particle["Part_MC"][0], Particle_MC)]
        print "\nAbout to load the following variables from Monte Carlo NTuples"        
        for branch in branches_MC:
            print branch
    
           
        MC_D0   = ROOT.TChain( Particle["MCTree"] )
        n_MC = MC_D0.Add( Particle["MCPath"] )
        print "Added", n_MC, "Monte Carlo NTuples" 
        
        
        print "Number of Monte Carlo events:", MC_D0.GetEntries()
        
        # Load the data and Monte Carlo
        MC_K   = tree2array(MC_D0,  branches=branches_MC,   selection = Particle["MCCut"],   stop=args.Stop)
        Source += ( MC_K.view((   MC_K.dtype[0],   len( MC_K.dtype.names ) )), )
    
    Source = np.concatenate(Source)
    
    pickle.dump( Source, open( Particle["PickleFileMC"], "wb" ) )
    print "Saving MC Data to pickle file"


print "\nEvents in Data:", Target.shape[0]
print "Events in MC:", Source.shape[0]
for it in range(N_PID_Cuts):
    print "Events in MC:", Source[:,3+it].sum()
  


"""
Perform the training and classification of the GBReweighter
"""


# Split the Monte Carlo into two datasets:
Source_1, Source_2 = np.array_split( Source, 2 )

gb1 = GBReweighter(n_estimators=100, max_depth=3, min_samples_leaf=100)
gb1.fit(Source_1[:, 0:3], Target[:,0:3], target_weight=Target[:,3])

gb2 = GBReweighter(n_estimators=100, max_depth=3, min_samples_leaf=100)
gb2.fit(Source_2[:, 0:3], Target[:,0:3], target_weight=Target[:,3])

O1 = gb2.predict_weights(Source_1[:,0:3])  # Predict Weights of dataset 1 from thetraining of dataset 2
O2 = gb1.predict_weights(Source_2[:,0:3])  # Predict Weights of dataset 2 from thetraining of dataset 1
    
GBRW_Weights = np.concatenate((O1, O2))



"""
Create the New PID Histograms
"""




File_Out = ROOT.TFile.Open( Particle["PIDHistFile"].replace(".root", "_MC.root"), "RECREATE" )

OutHists = []
for it, Hist in enumerate(PIDHists):
    Hist = Hist.Clone()
    Hist.Reset()
    Hist_NoCut = Hist.Clone()
    Hist_PIDCut = Hist.Clone()

    print "Filling Histogram:", Hist.GetName()
    
    for row, w in zip(Source, GBRW_Weights):
        Hist_PIDCut.Fill( row[0], row[1], row[2], w* row[3+it] )
        Hist_NoCut.Fill(  row[0], row[1], row[2], w )
    
    Hist_PIDCut.Divide(Hist_PIDCut, Hist_NoCut, 1, 1, "B")
    
    Hist_PIDCut.SetDirectory(0)
    OutHists.append( Hist_PIDCut.Clone() )
    
    
    import math
    for X in range( Hist_PIDCut.GetXaxis().GetNbins() ):
        for Y in range( Hist_PIDCut.GetYaxis().GetNbins() ):
            
            if args.Verbose:
                print "\nMC PID Efficiency for Bins:", X, Y,":",
            
            for Z in range( Hist_PIDCut.GetZaxis().GetNbins() ):
                val = Hist_PIDCut.GetBinContent(X+1, Y+1, Z+1)
                if math.isnan( val ):
                    Hist_PIDCut.SetBinContent(X+1, Y+1, Z+1, 1)
                    Hist_PIDCut.SetBinError(X+1, Y+1, Z+1, 1)
                if args.Verbose:
                    print round(Hist_PIDCut.GetBinContent(X+1, Y+1, Z+1)*100, 2),
    Hist_PIDCut.Write()
File_Out.Close()


if not args.SavePlots:
    exit(0)


"""
Make Validation Plots
"""



from PID_Config import HistConfig1

c1=ROOT.TCanvas("c1", "c1", 900, 900)


h_Weights = ROOT.TH1F( *HistConfig1(0, Particle_Data, "Weights1") ) 
nev = GBRW_Weights.shape[0]
h_Weights.FillN(nev, GBRW_Weights, np.ones(nev) )
h_Weights.Draw()
c1.Print("output/weights1.pdf")


h_Weights = ROOT.TH1F( *HistConfig1(0, Particle_Data, "Weights2") )
nev = GBRW_Weights.shape[0]
h_Weights.FillN(nev, GBRW_Weights, np.ones(nev) )
c1.SetLogy()
h_Weights.Draw()
c1.Print("output/weights2.pdf")

c1.SetLogy(False)


for i, Var in enumerate(["P", "ETA", "nTracks"]):
    h_Data = ROOT.TH1F( *HistConfig1(0, Particle_Data, Var) )
    h_MC   = ROOT.TH1F( *HistConfig1(1, Particle_Data, Var) )
    h_MCrw = ROOT.TH1F( *HistConfig1(2, Particle_Data, Var) )
    
    nev = Target.shape[0]
    h_Data.FillN(nev, Target[:,i].astype(np.double), Target[:,3].astype(np.double))
    h_Data.SetMinimum(0)
    h_Data.Sumw2(False)
    
    nev = Source.shape[0]
    h_MC.  FillN(nev, Source[:,i].astype(np.double), np.ones(nev))
    h_MCrw.FillN(nev, Source[:,i].astype(np.double), GBRW_Weights)
    
    h_Data.DrawNormalized()

    h_MC.SetLineColor(2)
    h_MC.DrawNormalized("SAME")

    h_MCrw.SetLineColor(2)
    h_MCrw.SetMarkerColor(2)
    
    h_MCrw.DrawNormalized("SAME")
    
    c1.Print("output/{0}.pdf".format(h_Data.GetName()))
    
    
c2=ROOT.TCanvas("c2", "c2", 3000, 3000)
c2.Divide(5, 5)
    
from PID_EvenBinning import GetBinning

Bins_Eta = GetBinning( 10, 1.5, 5.5, Target[:,1], Target[:,3])
Bins_NT  = GetBinning( 5 , 0, 600,   Target[:,2], Target[:,3] )
Bins_P = np.linspace(0, 90000, 51)

print Bins_Eta
print Bins_NT

h3_Data  = ROOT.TH3F("h_P_3D", "",    50, Bins_P, 10, Bins_Eta, 5, Bins_NT)    
h3_MCRW  = ROOT.TH3F("h_P_3D_MC", "", 50, Bins_P, 10, Bins_Eta, 5, Bins_NT)    

for row, w in zip(Source, GBRW_Weights):
    h3_MCRW.Fill(  row[0], row[1], row[2], w )
    
for row in Target:
    h3_Data.Fill(  row[0], row[1], row[2], row[3] )
    
     
h3_Data.Sumw2(False)
        
hist = 0
hists = []


for xmin in range(5):
    for ymin in range(5):
        hist+=1
        c2.cd(hist)
        h_Data = h3_Data.ProjectionX("h_P_3D_{0}_{1}"   .format(xmin, ymin), xmin+1, xmin+1, ymin+1, ymin+1)
        h_MCRW = h3_MCRW.ProjectionX("h_P_3D_MC_{0}_{1}".format(xmin, ymin), xmin+1, xmin+1, ymin+1, ymin+1)
        hists += [h_Data, h_MCRW]
        h_Data.Sumw2(False)
        h_MCRW.SetMarkerColor(2)
        h_MCRW.SetLineColor(2)
        h_Data.DrawNormalized()
        h_MCRW.DrawNormalized("SAME")
   
c2.Print("output/Slices_P_{0}.pdf".format(Particle_Data))      
        
    
    
   

