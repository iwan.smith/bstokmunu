

# You should run these first
# SetupProject LHCbDirac v8r2p55
# lhcb-proxy-init
# ./UraniaDev_v6r1/run bash


N=0
for FILE in `find /eos/lhcb/grid/prod/lhcb/calib/lhcb/calib/pid/CalibData -iname '*root'|grep Reco14|egrep "DSt|Jpsi_Mu"|grep Strip20_`
do
	python $PIDPERFSCRIPTSROOT/scripts/python/Plots/CreateTTreeFromDataset.py $FILE $(echo $FILE|sed -e 's@.*/@/eos/lhcb/user/i/ismith/PID_Data/@g') &
	N=`expr $N + 1`
	if [ $N -eq 8 ]
	then
		wait
		N=0
	fi   
done
