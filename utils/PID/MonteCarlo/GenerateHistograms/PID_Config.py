""""
Declare the Configurations for running
"""

def Particles( MagnetPolarity, PName ):
    
    P = {}
    
    P["K"] = \
    {
        "Part_Data"   : "K",
        "Part_MC"     : ["K"],
        "DataPath"    : "root://eoslhcb.cern.ch//eos/lhcb/wg/semileptonic/Bs2KmunuAna/Tuples/PID/Data/DSt_K_Mag{Mag}_Strip20_*.root".format(Mag=MagnetPolarity),
        "DataTree"    : "RSDStCalib",
        "MCPath"      : "root://eoslhcb.cern.ch//eos/lhcb/wg/semileptonic/Bs2KmunuAna/Tuples/PID/MC/DSt_Mag{Mag}_Strip20.root".format(Mag=MagnetPolarity),
        "MCTree"      : "PID_Dstar/DecayTree",
        "MCCut"       : "D0_BKGCAT == 0",
        "PIDHistFile" : "../../Data/PerfHists_K_Strip20_Mag{Mag}_P_ETA_nTracks.root".format(Mag=MagnetPolarity),
        "PickleFile"  : "DSt_K_Mag{Mag}_Strip20.pkl".format(Mag=MagnetPolarity),
        "PickleFileMC": "DSt_K_Mag{Mag}_Strip20_MC.pkl".format(Mag=MagnetPolarity)
    }
    
    P["Pi"] = \
    {
        "Part_Data"   : "Pi",
        "Part_MC"     : ["Pi"],
        "DataPath"    : "root://eoslhcb.cern.ch//eos/lhcb/wg/semileptonic/Bs2KmunuAna/Tuples/PID/Data/DSt_Pi_Mag{Mag}_Strip20_*.root".format(Mag=MagnetPolarity),
        "DataTree"    : "RSDStCalib",
        "MCPath"      : "root://eoslhcb.cern.ch//eos/lhcb/wg/semileptonic/Bs2KmunuAna/Tuples/PID/MC/DSt_Mag{Mag}_Strip20.root".format(Mag=MagnetPolarity),
        "MCTree"      : "PID_Dstar/DecayTree",    
        "MCCut"       : "D0_BKGCAT == 0",
        "PIDHistFile" : "../../Data/PerfHists_Pi_Strip20_Mag{Mag}_P_ETA_nTracks.root".format(Mag=MagnetPolarity),
        "PickleFile"  : "DSt_Pi_Mag{Mag}_Strip20.pkl".format(Mag=MagnetPolarity),
        "PickleFileMC": "DSt_Pi_Mag{Mag}_Strip20_MC.pkl".format(Mag=MagnetPolarity)
    }
    
    P["Mu"] = \
    {
        "Part_Data"   : "Mu",
        "Part_MC"     : ["mu_m", "mu_p"],
        "DataPath"    : "root://eoslhcb.cern.ch//eos/lhcb/wg/semileptonic/Bs2KmunuAna/Tuples/PID/Data/Jpsi_Mu_Mag{Mag}_Strip20_*.root".format(Mag=MagnetPolarity),
        "DataTree"    : "JpsiCalib",
        "MCPath"      : "root://eoslhcb.cern.ch//eos/lhcb/wg/semileptonic/Bs2KmunuAna/Tuples/PID/MC/Jpsi_Mu_Mag{Mag}_Strip20.root".format(Mag=MagnetPolarity),
        "MCTree"      : "PID_JPsi/DecayTree",    
        "MCCut"       : "JPsi_BKGCAT == 0",
        "PIDHistFile" : "../../Data/PerfHists_Mu_Strip20_Mag{Mag}_P_ETA_nTracks.root".format(Mag=MagnetPolarity),
        "PickleFile"  : "Jpsi_Mu_Mag{Mag}_Strip20.pkl".format(Mag=MagnetPolarity),
        "PickleFileMC": "Jpsi_Mu_Mag{Mag}_Strip20_MC.pkl".format(Mag=MagnetPolarity)
    }
    
    return P[PName]


def HistConfig1( ID=0, Particle="Mu", Var="P"   ):
    
    low={}
    high = {}
    
    low ["P"] = 0
    high["P"] = 80000

    low ["ETA"] = 2
    high["ETA"] = 5

    low ["nTracks"] = 0
    high["nTracks"] = 500
    
    low ["Weights1"] = 0
    high["Weights1"] = 5
    
    low ["Weights2"] = -10
    high["Weights2"] = 20
    
    HistName = "h_{Pname}_{var}_{id}".format( Pname=Particle, var=Var, id=ID )
    
    
    return ( HistName, "", 100, low[Var], high[Var] )
    
    
    
    
    
    
    
    
    
    
    
    
    
    