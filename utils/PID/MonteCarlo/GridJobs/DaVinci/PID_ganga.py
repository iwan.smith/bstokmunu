

optsfile = '/afs/cern.ch/user/i/ismith/ANA/BsToKMuNu/utils/PID/MonteCarlo/GridJobs/DaVinci/PID_options_s23r1.py'
nfiles = 10
maxev = "-1"




f = open('BookKeepingDatabase.conf', 'r')
for line in f:
	
	if line == "\n":
		continue
	
	BK_Info = line.split()
	
	BK_Number = BK_Info[0]
	BKPath = BK_Info[1]
	Polarity = "MagDown" if ( "MagDown" in  BKPath ) else "MagUp"
	Year = "2012" if ( "-2012-" in BKPath ) else ( "2011" if ("-2011-" in BKPath ) else "" )
	DDDB = BK_Info[2]
	CondDB = BK_Info[3]
	
	if Year != "2012":
		continue #Only care about 2012 for now
	
	if BK_Number != "24142001":
	        continue
	
	bk_query = BKQuery( path =  BKPath )
	ds = []
	ds.extend( bk_query.getDataset() )
		

	Application = DaVinci()
	Application.version = 'v38r1p1'

	JobName = "PID_{0}_{1}".format(BK_Number, Polarity)

	OutputFileName = 'PID_MonteCarlo_{0}.root'.format(Polarity)
	
	Splitter = SplitByFiles(filesPerJob = nfiles, maxFiles = -1, ignoremissing = True, bulksubmit=False)

	Options = [ File ( optsfile ) ]
	Application.optsfile = Options

	j = Job ()
	j.name         = JobName
	j.application  = Application
	j.application.user_release_area = '/afs/cern.ch/user/i/ismith/cmtuser'


	#Output   = [ LocalFile('summary.xml'), DiracFile(namePattern="PID_MonteCarlo_{0}.root".format(Polarity),  locations=["CERN-USER"]) ]
	j.splitter     = Splitter

	j.application.extraopts = ""
	j.application.extraopts += "DaVinci().EvtMax = -1 \n"
	j.application.extraopts += "DaVinci().PrintFreq = 1000 \n"

	j.application.extraopts += "DaVinci().TupleFile = \"" + OutputFileName + "\"\n"
	j.application.extraopts += "DaVinci().CondDBtag = '{0}' \n".format(CondDB)
	j.application.extraopts += "DaVinci().DDDBtag = '{0}' \n".format(DDDB)


	j.backend      = Dirac()
	#j.outputfiles  = Output
	j.outputfiles= [ DiracFile(OutputFileName) ] 
	j.inputdata  = ds
	j.do_auto_resubmit = True


	queues.add(j.submit)

