from GaudiConf import IOHelper
from Configurables import DaVinci, DecayTreeTuple
from DecayTreeTuple.Configuration import *
from Configurables import LoKi__Hybrid__EvtTupleTool# as LoKiEvtTool






DecayTreeTuples = []
stream = 'AllStreams'

"""
		Analyse the Dstar decays
		
		This will give us Kaons and Pions
		
"""

Tuple_Dstar_line = 'NoPIDDstarWithD02RSKPiLine'

Tuple_Dstar = DecayTreeTuple('PID_Dstar')

Tuple_Dstar.Decay = '[D*(2010)+ -> ^(D0 -> ^K- ^pi+) ^pi+ ]CC'
Tuple_Dstar.addBranches({
					'Dstar' : '^[D*(2010)+ -> (D0 -> K- pi+) pi+ ]CC',
					'D0'    : '[D*(2010)+ -> ^(D0 -> K- pi+) pi+ ]CC',
					'K'     : '[D*(2010)+ -> (D0 -> ^K- pi+) pi+ ]CC',
					'Pi'     : '[D*(2010)+ -> (D0 -> K- ^pi+) pi+ ]CC',
					'Pi_soft'     : '[D*(2010)+ -> (D0 -> K- pi+) ^pi+ ]CC'
})


Dstar_line = 'NoPIDDstarWithD02RSKPiLine'
Tuple_Dstar.Inputs = ['/Event/{0}/Phys/{1}/Particles'.format(stream, Dstar_line)]

DecayTreeTuples.append(Tuple_Dstar)

"""
		Analyse the Lambda decays
		
		This will give us protons
		
"""



Tuple_Lambda_line = 'PIDCalibL02ppiLine'

Tuple_Lambda = DecayTreeTuple('PID_Lambda')

Tuple_Lambda.Decay = '[Lambda0 -> ^p+ ^pi- ]CC'
Tuple_Lambda.addBranches({
					'Lambda'	: '^[Lambda0 -> p+ pi- ]CC',
					'P' 		: '[Lambda0 -> ^p+ pi- ]CC',
					'Pi' 		: '[Lambda0 -> p+ ^pi- ]CC',

})


Tuple_Lambda.Inputs = ['/Event/{0}/Phys/{1}/Particles'.format(stream, Tuple_Lambda_line)]

DecayTreeTuples.append(Tuple_Lambda)

"""
		Analyse the J/psi decays
		
		This will give us Muons
		
"""


Tuple_JPsi_line = 'MuIDCalib_JpsiFromBNoPID'

Tuple_JPsi = DecayTreeTuple('PID_JPsi')

Tuple_JPsi.Decay = '[J/psi(1S) -> ^mu+ ^mu- ]CC'
Tuple_JPsi.addBranches({
					'JPsi'	: '^[J/psi(1S) -> mu+ mu- ]CC',
					'mu_p' 		: '[J/psi(1S) -> ^mu+ mu- ]CC',
					'mu_m' 		: '[J/psi(1S) -> mu+ ^mu- ]CC',
})


Tuple_JPsi.Inputs = ['/Event/{0}/Phys/{1}/Particles'.format(stream, Tuple_JPsi_line)]


DecayTreeTuples.append(Tuple_JPsi)

from Configurables import TupleToolTISTOS
for DTT in DecayTreeTuples:
	
	DTT.addTupleTool('TupleToolPrimaries')
	DTT.addTupleTool("TupleToolRecoStats")
	DTT.addTupleTool("TupleToolTrackInfo")
	DTT.TupleToolTrackInfo.Verbose = True
	DTT.addTupleTool("TupleToolMCBackgroundInfo")

	LoKiTool = DTT.addTupleTool("LoKi::Hybrid::TupleTool/LoKiTool")
	LoKiTool.Variables = {
		"ETA" : "ETA"
	}
	TriggerTool = ""

	MCTruth=DTT.addTupleTool("TupleToolMCTruth")

	try:
		TriggerTool = DTT.Dstar.addTupleTool('TupleToolTISTOS/TupleToolTISTOS')
		#MCTruth=DTT.Dstar.addTupleTool("TupleToolMCTruth")
	except:
		pass
	try:
		TriggerTool = DTT.Lambda.addTupleTool('TupleToolTISTOS/TupleToolTISTOS')
		#MCTruth=DTT.Lambda.addTupleTool("TupleToolMCTruth")
	except:
		pass
	try:
		TriggerTool = DTT.JPsi.addTupleTool('TupleToolTISTOS/TupleToolTISTOS')
		#MCTruth=DTT.JPsi.addTupleTool("TupleToolMCTruth")
	except:
		pass
		
	TriggerTool.VerboseL0 = True
	TriggerTool.VerboseHlt1 = True
	TriggerTool.VerboseHlt2 = True             
	TriggerTool.TriggerList=[
	   "Hlt2TopoMu4BodyBBDTDecision",
	   "L0HadronDecision",
	   "L0MuonDecision",
	   "L0DiMuonDecision",
	   "Hlt2SingleMuonDecision",
	   "Hlt2TopoMu2BodyBBDTDecision",
	   "Hlt2TopoMu3BodyBBDTDecision",
	   "Hlt1TrackMuonDecision",
	   "Hlt2MuTrackDecision",
	   "Hlt2Topo3BodySimpleDecision",  
	   "Hlt1TrackAllL0Decision",
	   "Hlt2SingleMuonHighPTDecision",
	   "Hlt2SingleMuonLowPTDecision",
	   "Hlt2IncPhiDecision"
	   "Hlt2TFBc2JpsiMuXDecision",
	   "Hlt2TFBc2JpsiMuXSignalDecision",
	   #"Hlt2Topo2BodySimpleDecision",
	   #"Hlt2B2HHPi0_MergedDecision",
	   #"Hlt2CharmHadD2HHHDecision",
	   #"Hlt2Topo4BodySimpleDecision",
	   #"Hlt2CharmHadD2HHHWideMassDecision",
	   #"Hlt2DiMuonJPsiHighPTDecision",
	   #"Hlt2DiMuonBDecision",
	   #"Hlt2Topo4BodyBBDTDecision",
	   #"Hlt2DiMuonZDecision",
	   #"Hlt2DiMuonDetachedDecision",
	   #"Hlt2DiMuonDetachedHeavyDecision",
	   #"Hlt2DiMuonDetachedJPsiDecision",
	   #"Hlt2TriMuonDetachedDecision",
	   #"Hlt2TopoE3BodyBBDTDecision",
	   #"Hlt2TriMuonTauDecision",
	   #"Hlt2TopoE4BodyBBDTDecision",
	   #"Hlt2CharmHadD02HHHHDecision",
	   #"Hlt2CharmHadD02HHHHWideMassDecision",
	   #"Hlt2CharmHadD02HHKsLLDecision",
	   #"Hlt2B2HHLTUnbiasedDecision",
	   #"Hlt2Dst2PiD02PiPiDecision",
	   #"Hlt2CharmHadD02HH_D02PiPiDecision",
	   #"Hlt2Dst2PiD02MuMuDecision",
	   #"Hlt2CharmHadD02HH_D02PiPiWideMassDecision",
	   #"Hlt2Dst2PiD02KMuDecision",
	   #"Hlt2CharmHadD02HH_D02KKDecision",    
	   
	   ]
	MCTruth.addTupleTool("MCTupleToolHierarchy")


DaVinci().UserAlgorithms += DecayTreeTuples
DaVinci().InputType = 'DST'
DaVinci().PrintFreq = 100
DaVinci().EvtMax = 1000


DaVinci().TupleFile = 'PID_Proton_Perf_2012.root'
DaVinci().DataType = '2012'
DaVinci().Simulation = True
DaVinci().Lumi = False
#DaVinci().CondDBtag = 'default' # This is changed in the ganga scripte
#DaVinci().DDDBtag = 'default'

"""
IOHelper().inputFiles([
#	'/eos/lhcb/grid/prod/lhcb/MC/2012/ALLSTREAMS.DST/00030158/0000/00030158_00000130_1.allstreams.dst', # Test the jpsi lines 13442001
	'/eos/lhcb/grid/prod/lhcb/MC/2012/ALLSTREAMS.DST/00025340/0000/00025340_00000116_1.allstreams.dst' # 27163003
], clear=True)

"""
