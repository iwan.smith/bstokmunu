

def CopyToCERNUser( jobid ):
	j = jobs(jobid)
	for sj in j.subjobs:
		# Get all output files which are DiracFile objects
		for df in sj.outputfiles.get(DiracFile):
			# No need to replicate if it's already at CERN
			if 'CERN-USER' not in df.locations:
				df.replicate('CERN-USER')



def SaveEosLFNs( joblist, grid_file_to_write ):
	for jobid in joblist:
		queues.add( CopyToCERNUser, args=[jobid] )
		j=jobs( jobid )
		for sj in j.subjobs.select(status="completed"):
			for DF in sj.outputfiles.get(DiracFile):
				if len(DF.lfn) > 0:
					grid_file_string = "root://eoslhcb.cern.ch//eos/lhcb/grid/user" + DF.lfn + "\n"
					grid_file_to_write.write( grid_file_string )

Job_BK_ID = [ 11442001, 13442001, 12442001, 27163003 ]

JobNames = []

for Mag in ["MagUp", "MagDown"]:
	for BK in Job_BK_ID:
		JobNames.append( "PID_{0}_{1}_Performance".format( BK, Mag ) )

JobList = []
FileList = []

for jname in JobNames:
	jlist = []
	for j in jobs:
		if j.name == jname:
			print j.name
			jlist.append( int( j.fqid ) )
	
	JobList.append(jlist)
	FileList.append( open( jname + ".filelist", 'w') )



for job, File in zip(JobList, FileList):
	SaveEosLFNs( job, File )
	File.close()
	
	print "hadd root://eoslhcb.cern.ch//eos/lhcb/wg/semileptonic/Bs2KmunuAna/Tuples/PID_MC/" + File.name.replace("_Performance.filelist", ".root") + " @" + File.name

	

