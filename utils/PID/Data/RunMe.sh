#!/bin/bash
for MAGNET in "MagDown" "MagUp"
do
    for STRIPPING in "20" "20r1"
    do
        python UraniaDev_v6r1/PIDCalib/PIDPerfScripts/scripts/python/MultiTrack/MakePerfHistsRunRange.py \
            "$STRIPPING" $MAGNET 'K' \
            "[(DLLmu-DLLp)>0 && DLLmu>3 && (DLLmu-DLLK)>0 && IsMuon==1,(DLLK-DLLp)>5 && DLLK>5 && (DLLK-DLLmu)>5,DLLK>-2,DLLK<-2]" \
            -c "DLLK>-2" \
            "P" "ETA" "nTracks"
    
    
        mv PerfHists_K_Strip${STRIPPING}_${MAGNET}_P_ETA_nTracks.root PerfHists_K_Strip${STRIPPING}_${MAGNET}_P_ETA_nTracks_WRT_Stripping.root
    
    
        python UraniaDev_v6r1/PIDCalib/PIDPerfScripts/scripts/python/MultiTrack/MakePerfHistsRunRange.py \
            "$STRIPPING" $MAGNET 'K' \
            "[(DLLmu-DLLp)>0 && DLLmu>3 && (DLLmu-DLLK)>0 && IsMuon==1,(DLLK-DLLp)>5 && DLLK>5 && (DLLK-DLLmu)>5,DLLK>-2,DLLK<-2]" \
            "P" "ETA" "nTracks"
    
    
        python UraniaDev_v6r1/PIDCalib/PIDPerfScripts/scripts/python/MultiTrack/MakePerfHistsRunRange.py \
            "$STRIPPING" $MAGNET 'Mu' \
            "[(DLLmu-DLLp)>0 && DLLmu>3 && (DLLmu-DLLK)>0 && IsMuon==1,(DLLmu-DLLp)>0 && DLLmu>2 && (DLLmu-DLLK)>0 && IsMuon==1,DLLmu>0.0,DLLmu<-0.0001,DLLK<-2]" \
            "P" "ETA" "nTracks"
    
    
        python UraniaDev_v6r1/PIDCalib/PIDPerfScripts/scripts/python/MultiTrack/MakePerfHistsRunRange.py \
            "$STRIPPING" $MAGNET 'Pi' \
            "[(DLLmu-DLLp)>0 && DLLmu>3 && (DLLmu-DLLK)>0 && IsMuon==1,DLLK<20.0,DLLK<-2]" \
            "P" "ETA" "nTracks"

    done
done
