/*
 * FitterHeader.h
 *
 *  Created on: 16 Aug 2017
 *      Author: ismith
 */

#ifndef NEWFITTER_CONTROL_INCLUDE_FITTERHEADER_H_
#define NEWFITTER_CONTROL_INCLUDE_FITTERHEADER_H_


#include <algorithm>
#include <fstream>
#include <functional>
#include <iostream>
#include <map>
#include <memory>
#include <stdio.h>
#include <string>
#include <utility>
#include <vector>

#include "TAttText.h"
#include "TCanvas.h"
#include "TDatime.h"
#include "TF1.h"
#include "TH3.h"
#include "THStack.h"
#include "TIterator.h"
#include "TLatex.h"
#include "TLegend.h"
#include "TObjArray.h"
#include "TPaveText.h"
#include "TRandom3.h"
#include "TStopwatch.h"
#include "TStyle.h"

#include "RooAbsData.h"
#include "RooCategory.h"
#include "RooChi2Var.h"
#include "RooDataHist.h"
#include "RooDataSet.h"
#include "RooExtendPdf.h"
#include "RooFitResult.h"
#include "RooGaussian.h"
#include "RooHist.h"
#include "RooHistPdf.h"
#include "RooMCStudy.h"
#include "RooMinimizer.h"
#include "RooMinuit.h"
#include "RooMsgService.h"
#include "RooParamHistFunc.h"
#include "RooPoisson.h"
#include "RooRandom.h"
#include "RooRealSumPdf.h"
#include "RooRealVar.h"
#include "RooSimultaneous.h"
#include "RooStats/MinNLLTestStat.h"
#include "RooStats/ModelConfig.h"
#include "RooStats/ToyMCSampler.h"

#include "RooStats/HistFactory/Channel.h"
#include "RooStats/HistFactory/FlexibleInterpVar.h"
#include "RooStats/HistFactory/HistFactoryModelUtils.h"
#include "RooStats/HistFactory/HistFactoryNavigation.h"
#include "RooStats/HistFactory/HistFactorySimultaneous.h"
#include "RooStats/HistFactory/MakeModelAndMeasurementsFast.h"
#include "RooStats/HistFactory/Measurement.h"
#include "RooStats/HistFactory/ParamHistFunc.h"
#include "RooStats/HistFactory/PiecewiseInterpolation.h"
#include "RooStats/HistFactory/RooBarlowBeestonLL.h"
#include "RooStats/HistFactory/Sample.h"

using std::string;
using std::cout;
using std::endl;
using std::vector;
using std::map;
using std::shared_ptr;
using std::reference_wrapper;

using namespace RooFit;
using namespace RooStats;

enum Colours{ White, Black, Red, Green, Navy, Yellow, Pink, Blue, Green2, Blue2, White2, Grey=13 };

#endif /* NEWFITTER_CONTROL_INCLUDE_FITTERHEADER_H_ */
