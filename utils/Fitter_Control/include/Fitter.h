
#ifndef FITTER_CONTROL_INCLUDE_FITTER_H_
#define FITTER_CONTROL_INCLUDE_FITTER_H_


#include "FitterHeader.h"
#include "RooFitHeader.h"

#include "FitterFunctions.h"
#include "FitterObjects.h"

#include "HistogrammerTools.h"

using std::reference_wrapper;

class ChannelBuilder
{
public:

  // Constructors, assignments and destructors

  ChannelBuilder(string ChanName):ChannelName(ChanName){};
  ChannelBuilder(): ChannelBuilder(""){};

  ChannelBuilder(const ChannelBuilder&) = default;
  ChannelBuilder(ChannelBuilder&&) = default;

  ChannelBuilder& operator=(const ChannelBuilder&) = default;
  ChannelBuilder& operator=(ChannelBuilder&&) = default;

  virtual ~ChannelBuilder() = default;

  // Begin the implementation


  vector<shared_ptr<TH1F>>&        DataTemplates    ()  {return h_data;};
  map<string, TemplateObjects>&    TemplateContainer()  {return FitTemplates;};
  TemplateObjects&                 at(string name);
  map<string, TemplateCombiner>&   CombinedTemplates()  {return CombinedHists;};

  map<string,double>               GetResults() const   {return results;};

  vector<vector<shared_ptr<TH1F>>> GetPseudoData()      { return h_data_Toys;};
  vector<map<string,double>>       GetPseudoDataYieds() { return h_data_Toy_Yields;};

  void                             SetPseudoData(vector<vector<shared_ptr<TH1F>>> PseudoData){ h_data_Toys=PseudoData;};


  HistFactory::Channel&            GetChannel();
  const string&                    GetName() const {return ChannelName;};

  void ProcessFit(const FitterObjects&, map<string,string>  ConstrainedPars = map<string,string>{});
  void BuildPseudoData(int nToys);

  void Reset();



private:

  HistFactory::Channel DsMuNu_Fit_Channel;

  string ChannelName;

  PseudoDataBuilder m_Builder;

  vector<shared_ptr<TH1F>> h_data;
  vector<vector<shared_ptr<TH1F>>> h_data_Toys;
  vector<map<string,double>> h_data_Toy_Yields;

  map<string, TemplateObjects> FitTemplates;
  map<string, TemplateCombiner> CombinedHists;

  PseudoDataBuilder Builder;

  map<string,double> results;



};
typedef reference_wrapper<ChannelBuilder> ref_Channel;




class Fitter
{
public:

  // Constructors, assignments and destructors

  Fitter() = default;

  Fitter(Fitter&) = delete;
  Fitter(Fitter&&) = delete;

  Fitter& operator=(Fitter&) = delete;
  Fitter& operator=(Fitter&&) = delete;

  virtual ~Fitter() = default;

  // Begin the implementation

  void AddChannel(ChannelBuilder& C){ Channels.push_back( ref_Channel(C) );};

  void Fit(string, map<string,string>  ConstrainedPars = map<string,string>{});
  void BuildPseudoData(int nToys);

  void Reset();


  shared_ptr<RooFitResult>         GetFitResults()  { return FitResults;};

private:
  shared_ptr<RooFitResult> FitResults;
  vector<ref_Channel> Channels;



};

#endif
