/*
 * FitterFunctions.h
 *
 *  Created on: 16 Aug 2017
 *      Author: ismith
 */

#ifndef NEWFITTER_CONTROL_INCLUDE_FITTERFUNCTIONS_H_
#define NEWFITTER_CONTROL_INCLUDE_FITTERFUNCTIONS_H_


#include "FitterHeader.h"
#include "FitterObjects.h"

#include <cstring>

#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>

HistFactory::Measurement GenerateMeasurement()
{

    HistFactory::Measurement Fitter("Bs2DsMuNu_Fitter", "Fitter for B_s #rightarrow D_s #mu #nu");
    Fitter.SetOutputFilePrefix("results/Bs_DsMuNu_Fit");
    Fitter.SetExportOnly(true);
    Fitter.SetPOI("Ds_Yield");
    Fitter.SetLumi(1.0);
    Fitter.SetLumiRelErr(0.0);

    return Fitter;

}

void GenerateChannel(
        HistFactory::Channel& FitChannel,
        shared_ptr<TH1F> h_data,
        map<string, TemplateObjects>& FitTemplates)
{
    FitChannel.SetStatErrorConfig( 1e-5, "Pois" );
    FitChannel.SetData( (TH1F*)h_data->Clone() );

    for( auto &T: FitTemplates)
    {
        cout << T.second.m_Name << endl;

        //cout << T.second.GetHistoName() << endl;

        FitChannel.AddSample(T.second);
    }

}





shared_ptr<TH1F> GetHist(TFile* f, string hname, bool normalise = true, bool smooth = false, bool ClearUnderFlow = false, bool isMakeToy = false, int Rebin = 1)
{

    cout << "Trying to load histogram: " << hname ;


    shared_ptr<TH1F> hist( (TH1F*) f->Get(hname.c_str()) );
    if ( not hist)
    {
      cout << "Histogram: " << hname << "not found. Returning empty histogram"<< endl;
      return shared_ptr<TH1F>( new TH1F() );
    }


    hist->SetDirectory(0);

    cout << " Stored at: " << hist.get() << endl;

    if ( ClearUnderFlow )
    {
      hist->ClearUnderflowAndOverflow();
    }
    if ( not hist->GetSumw2N() ) hist->Sumw2();


    if ( Rebin > 1 )
      hist->Rebin(Rebin);

    int nBinsX = hist->GetNbinsX();

    // If any bins have bin content <= 0, set the bin content to a small number
    for(int BX = 1; BX <= nBinsX; BX++)
    {
        if ( hist->GetBinContent(BX) <= 0.0 )
        {
            hist->SetBinContent(BX, 1e-3);
            hist->SetBinError(BX, 1e-3);
        }
    }

    // If a toy histogram is requested:
    if ( isMakeToy )
    {
      const gsl_rng_type * T;
      gsl_rng * prng;

      gsl_rng_env_setup();

      T = gsl_rng_default;
      prng = gsl_rng_alloc (T);
      gsl_rng_set (prng, rand());

      for(int BX = 1; BX <= nBinsX; BX++)
      {
          double newBinContent = -1;

          double BinContent = hist->GetBinContent(BX);
          double BinError   = hist->GetBinError(BX);

          while ( newBinContent < 0.0 )
          {
              newBinContent = BinContent + gsl_ran_gaussian (prng, BinError);
          }
          hist->SetBinContent(BX, newBinContent);
      }
    }

    if ( smooth )
    {
        hist->Smooth();
    }

    if (normalise)
    {
        hist->Scale( 1.0 / hist->Integral(0, nBinsX+1) );
    }

    return hist;
}




void PrintBeforeAfter(const map<string, TemplateObjects>& FitTemplates)
{

    shared_ptr<TCanvas> c_bef_aft ( new TCanvas("c_bef_aft", "c_bef_aft", 1600, 1600) );

    shared_ptr<TPad> pad1 ( new TPad("pad1.1", "The pad 80% of the height",0.0,0.3,1.0,1.0) );
    shared_ptr<TPad> pad2 ( new TPad("pad2.2", "The pad 20% of the height",0.0,0.0,1.0,0.3) );

    shared_ptr<TPaveText> pt ( new TPaveText(.65, .85, .95, .95, "NDC") );
    pt->AddText("Before")->SetTextColor(1);
    pt->AddText("After")->SetTextColor(2);
    pt->SetBorderSize(0);
    pt->SetFillColor(0);




    c_bef_aft->Print("CompareBeforeAfter.pdf[");

    for( auto &T: FitTemplates)
    {
        //const string& sampleName  = T.first;
        const TemplateObjects& FO = T.second;

        shared_ptr<TH1F> h_before ( (TH1F*)FO.Hist->Clone() );
        shared_ptr<TH1F> h_after  ( (TH1F*)FO.Hist_After->Clone() );

        h_before->Scale( 1.0/h_before->Integral() );
        h_before->SetMarkerColor(1);
        h_before->SetLineColor(1);

        h_after->Sumw2();
        h_after->SetMarkerColor(2);
        h_after->SetLineColor(2);
        h_after->Scale( 1.0/h_after->Integral() );

        pad1->cd();
        h_before->Draw();
        h_after->Draw("SAME");
        pt->Draw();

        c_bef_aft->cd();
        pad1->Draw();

        shared_ptr<TH1F> h_ratio ( (TH1F*)h_before->Clone() );
        h_ratio->SetTitle("(After-Before)/#sigma_{Before}");
        h_ratio->SetMaximum(3);
        h_ratio->SetMinimum(-3);
        for( int bin = 1 ; bin <= h_ratio->GetSize()-2; bin++ )
        {
            h_ratio->SetBinContent(bin, (h_after->GetBinContent(bin) - h_before->GetBinContent(bin))/h_before->GetBinError(bin) );
            h_ratio->SetBinError(bin, 1);
        }
        pad2->cd();
        h_ratio->Draw();

        c_bef_aft->cd();
        pad2->Draw();


        c_bef_aft->Print("CompareBeforeAfter.pdf");
    }

    c_bef_aft->Print("CompareBeforeAfter.pdf]");

}




class SortableHist
{

public:
  SortableHist(string SortKey, shared_ptr<TH1F> Hist)
    :m_SortKey(SortKey)
    ,m_Hist(Hist)
  {};

  string GetSortKey(){return m_SortKey;};

  shared_ptr<TH1F>  operator-> ( ) {return  m_Hist;};
  TH1F&             operator*  ( ) {return *m_Hist;};

  TH1F* get() {return m_Hist.get();};
private:
  string m_SortKey;
  shared_ptr<TH1F>  m_Hist;


};


enum class HistSort {Integral, Alpha, IntegralR, AlphaR, None};

void PrintFitPlot(const shared_ptr<TH1F> h_data, const map<string, TemplateCombiner>& FitTemplates, bool before, int id =0,
    std::string Savename = "FitPlot.pdf",
    std::string HistogramTitle = "Fit To Data",
    bool isScale = false,
    bool isSetLog = false,
    double leg_x = 0.11,
    double leg_y = 0.5,
    HistSort sort = HistSort::Integral,
    bool PlotPulls = true)
{

    vector<int> Colors{7,6,5,4,3,2,kOrange, kPink+10, kViolet, kViolet+5, kBlue, kBlue+3, kAzure-5, kAzure+10, kTeal-6, kGreen+3, kGreen, kSpring+10, kYellow-3, kOrange-3};


    gStyle->SetOptStat(0);

    vector<SortableHist> FitHistograms;
    THStack* stack = new THStack("Stack", HistogramTitle.c_str());
    TLegend *leg_1 = new TLegend(leg_x, leg_y, leg_x + 0.25, leg_y + 0.3);
    leg_1->SetBorderSize(0);
    leg_1->AddEntry(h_data.get(), "Data", "lep");

    double TemplateIntegral = 0;
    {
    int x = 0;
      for (auto &T: FitTemplates)
      {

          const string& sampleName= T.first;
          const TemplateCombiner& FO = T.second;


          shared_ptr<TH1F> Hist = FO.ConstructHist(id);

          Hist->SetTitle(sampleName.c_str());

          FitHistograms.emplace_back(FO.SimpleName ,Hist);

          TemplateIntegral += Hist->Integral();


          Hist->SetFillColor(Colors[x]);
          //Hist->Sumw2(false);
          Hist->SetMarkerColor(1);
          Hist->SetLineColor(1);

          leg_1->AddEntry(Hist.get(), Hist->GetTitle(), "f");

          x++;


      }
    }

    if ( sort == HistSort::Integral )
    {
      std::sort(FitHistograms.begin(), FitHistograms.end(),
        [](SortableHist a, SortableHist b) -> bool
          {
            return a->Integral() < b->Integral();
          }
      );
    }
    else if ( sort == HistSort::Integral )
    {
      std::sort(FitHistograms.begin(), FitHistograms.end(),
        [](SortableHist a, SortableHist b) -> bool
          {
            return a->Integral() > b->Integral();
          }
      );
    }
    else if ( sort == HistSort::Alpha )
    {
      std::sort(FitHistograms.begin(), FitHistograms.end(),
        [](SortableHist a, SortableHist b) -> bool
          {
            return a.GetSortKey() < b.GetSortKey();
          }
      );
    }
    else if ( sort == HistSort::AlphaR )
    {
      std::sort(FitHistograms.begin(), FitHistograms.end(),
        [](SortableHist a, SortableHist b) -> bool
          {
            return a.GetSortKey() > b.GetSortKey();
          }
      );
    }



    shared_ptr<TH1F> h_StackPulls ( (TH1F*)h_data->Clone() );
    h_StackPulls->Reset();
    h_StackPulls->SetTitle("( Data - Template ) / #sigma_{Data}");

    int nHist = FitHistograms.size();

    for (int x = 0; x < nHist ; x++){
      //*Histograms.at(x) = NuisanceVars.at(x)->getVal() * *Histograms.at(x);

        if (isScale )
          FitHistograms.at(x)->Scale(h_data->Integral() / TemplateIntegral);

        FitHistograms.at(x)->SetLineWidth(1);
        stack->Add( FitHistograms.at(x).get() );
        *h_StackPulls = *h_StackPulls + *FitHistograms.at(x);

    }

    stack->SetMinimum(0);
    if ( isSetLog )
      stack->SetMinimum(100);

    TCanvas *c1 = new TCanvas("c1", "c1", 1600, 1600);



    TPad *pad1 = nullptr;
    if ( PlotPulls )
    {
      pad1 = new TPad("pad1", "The pad 80% of the height",0.0,0.3,1.0,1.0);
      pad1->SetBottomMargin(0);
    }
    else
      pad1 = new TPad("pad1", "The pad 80% of the height",0.0,0.0,1.0,1.0);

    if ( isSetLog)
      pad1->SetLogy();
    pad1->cd();
    stack->Draw("Hist");

    //TH1* h_Stack = (TH1*)stack->GetHistogram()->Clone();

    shared_ptr<TH1F> H_Error = shared_ptr<TH1F>((TH1F*)h_StackPulls->Clone());

    H_Error->SetFillColorAlpha(1, 0.2);
    H_Error->Draw("SAME E2");

    leg_1->Draw();
    h_data->SetMarkerSize(3);
    h_data->SetLineWidth(2);
    h_data->SetLineColor(kBlack);
    h_data->Draw("SAME");


    TPad *pad2 = new TPad("pad2", "The pad 20% of the height",0.0,0.0,1.0,0.3);
    pad2->cd();
    pad2->SetTopMargin(0);
    for ( int bin = 1; bin <= h_StackPulls->GetSize()-2; bin++){

      double BinErr = sqrt( pow(h_data->GetBinError(bin),2) + pow(h_StackPulls->GetBinError(bin),2));
      h_StackPulls->SetBinContent(bin, (h_data->GetBinContent(bin) - h_StackPulls->GetBinContent(bin))/BinErr );
      h_StackPulls->SetBinError(bin,1);
      h_StackPulls->SetMinimum(-3);
      h_StackPulls->SetMaximum( 3);

    }

    h_StackPulls->SetTitle(HistogramTitle.c_str());
    h_StackPulls->Draw();

    c1->cd();
    pad1->Draw();
    if ( PlotPulls )
      pad2->Draw();

    c1->Print(Savename.c_str());


}





#endif /* NEWFITTER_CONTROL_INCLUDE_FITTERFUNCTIONS_H_ */
