/*
 * FitterObjects.h
 *
 *  Created on: 16 Aug 2017
 *      Author: ismith
 */

#ifndef NEWFITTER_CONTROL_INCLUDE_FITTEROBJECTS_H_
#define NEWFITTER_CONTROL_INCLUDE_FITTEROBJECTS_H_

#include "FitterHeader.h"

#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>

#include "functional"
using HistFactory::HistFactorySimultaneous;


struct FitterObjects
{
       RooWorkspace*        Fit_ws    = nullptr;
       ModelConfig*         Fit_mc    = nullptr;

       RooSimultaneous*     Fit_model = nullptr;

       RooDataSet*          Fit_data  = nullptr;
       RooRealVar*          Fit_poi   = nullptr;


       shared_ptr<HistFactorySimultaneous> Fit_model_hf;
       shared_ptr<RooAbsReal>   Fit_nll_hf;
       shared_ptr<RooMinuit>    Fit_minuit_hf;

       shared_ptr<RooFitResult> Fit_results;

       shared_ptr<HistFactory::HistFactoryNavigation> Fit_Nav;

       /***********************************************
        *    Run The Fit
        ***********************************************/

       shared_ptr<RooFitResult> FitMeasurement(HistFactory::Measurement&, map<string,string>  ConstrainedPars = map<string,string>{});


       //shared_ptr<RooFitResult> results (Fit_model_hf->save() );

};


class BaseFitterTemplate
{
public:

    BaseFitterTemplate(){};
    virtual ~BaseFitterTemplate(){};

    virtual shared_ptr<TH1F> ConstructHist(int id = 0) const = 0;
};



class TemplateObjects
        :public BaseFitterTemplate
        ,public HistFactory::Sample
{
public:
    TemplateObjects():isStatActive(true){};
    TemplateObjects(string, shared_ptr<TH1F>, bool IsStat= false);
    TemplateObjects(string, vector<shared_ptr<TH1F>>, bool IsStat= false);
    virtual ~TemplateObjects(){};

    std::string m_Name;

    vector<shared_ptr<TH1F>> Histograms;
    shared_ptr<TH1F> Hist;
    shared_ptr<TH1F> Hist_After;

    bool isStatActive;

    vector<RooRealVar*> FitParameters;

    shared_ptr<TH1F> ConstructHist(int id = 0) const;

    double GetYield() const;
    double GetError() const;
    TemplateObjects& AddNormFactor( std::string Name, Double_t Val, Double_t Low, Double_t High, bool Const=false );
    TemplateObjects& AddOverallSys( std::string Name, Double_t Low, Double_t High );


};





class ChannelHolder
    :public HistFactory::Channel
{
public:
    ChannelHolder(){};
    ChannelHolder( string channelName, shared_ptr<TH1F> DataHist )
        :HistFactory::Channel(channelName)
        ,h_data(DataHist)
        {
        SetStatErrorConfig( 1e-5, "Pois" );
        SetData( (TH1F*)DataHist->Clone(( (string)DataHist->GetName() + "_ForFit").c_str() ) );
        };

    void Finalise()
    {
        for(auto &T: FitTemplates)
            AddSample(T);
    }
    shared_ptr<TH1F> h_data;
    vector<TemplateObjects> FitTemplates;

};





class TemplateCombiner
        :public BaseFitterTemplate
{
public:


    TemplateCombiner(){};
    virtual ~TemplateCombiner(){};

    template<class ... TemplateObjects>
    TemplateCombiner(string Name, TemplateObjects& ... args)
      :SimpleName(Name)
      ,FOs{ args...}
    {
    }

    string SimpleName;
    shared_ptr<TH1F> ConstructHist(int id = 0) const;
    double           GetYield() const;
    double           GetError() const;
    vector< reference_wrapper<TemplateObjects> > FOs;


};



class PseudoDataBuilder
{
public:
  PseudoDataBuilder()
    :HistogramsLoaded(false)
  {
    T=nullptr;
    prng=nullptr;
  }

  PseudoDataBuilder(vector<shared_ptr<TH1F>> Data, map<string, TemplateCombiner> FOs)
    :HistogramsLoaded(true)
    ,DataHists(Data)
    ,FitterObjects(FOs)
  {
    gsl_rng_env_setup();
    T = gsl_rng_default;
    prng = gsl_rng_alloc (T);
    gsl_rng_set (prng, rand());

  }



  bool HistogramsLoaded;

  vector<shared_ptr<TH1F>>      DataHists;
  map<string, TemplateCombiner> FitterObjects;

  vector<shared_ptr<TH1F>> BuildPseudoData();

  map<string, double> ToyYields;


private:
  const gsl_rng_type * T;
  gsl_rng * prng;




};


class TreeBuilder
{

public:

  TreeBuilder(const std::string&, const std::string&);
  ~TreeBuilder();

  void init(const map<string,double>&);
  void AddEntry(const map<string,double>&);
  void Close();


  TFile* f_Out;
  TTree* T_Out;

  double BranchValues[200];


};






#endif /* NEWFITTER_CONTROL_INCLUDE_FITTEROBJECTS_H_ */
