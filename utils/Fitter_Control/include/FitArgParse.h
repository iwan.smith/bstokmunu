/*
 * FitArgParse.h
 *
 *  Created on: 8 Mar 2018
 *      Author: ismith
 */

#ifndef FITTER_CONTROL_SRC_LIB_FITARGPARSE_H_
#define FITTER_CONTROL_SRC_LIB_FITARGPARSE_H_

#include <iostream>

#include <boost/program_options.hpp>
namespace po = boost::program_options;

class FitArgParse
{

public:

  FitArgParse ();

  virtual
  ~FitArgParse ();

  void Init (
       std::string ROOTInputName = "/eos/lhcb/wg/semileptonic/Bs2KmunuAna/Histogrammer/Output/Histograms.root"
      ,std::string FitHistogram  = "h_Bs_MCORR"
      ,std::string CutName       = "PID_Tight"
      ,std::string Correction    = "Default"
      ,int         nToys         = 0
      ,std::string ToyOutputName = "Fit_Toys.root");


  void Parse(int ac, char* av[]);

  std::string m_ROOTInputName;
  std::string m_FitHistogram ;
  std::string m_CutName      ;
  std::string m_Correction   ;
  int         m_nToys        ;
  std::string m_ToyOutputName;

private:


  po::options_description desc;
  po::variables_map       vm;

  // Variables to store:




};

#endif /* FITTER_CONTROL_SRC_LIB_FITARGPARSE_H_ */
