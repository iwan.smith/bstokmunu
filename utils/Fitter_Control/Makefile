CC         = g++
SHELL      = /bin/bash
RM         = rm -f

ROOTCFLAGS = $(shell root-config --cflags|sed -e 's/c++11/c++14/g')
ROOTLIBS   = $(shell root-config --libs)
EXTRA_ROOTLIBS = -lRooFit -lRooStats -lRooFitCore -lHistFactory -lTreePlayer

# Extensions
SRCEXT     = cxx
HDREXT     = h
OBJEXT     = o
LIBEXT     = so

# Directories
PWD        = $(shell pwd)
SRCDIR     = src
BINSRCDIR  = app
LIBSRCDIR  = lib
HDRDIR     = include
OBJDIR     = build
LIBDIR     = $(PWD)/lib
BINDIR     = bin
COMHDRDIR  = $(COMMONDIR)/include
# Get files and make list of objects and libraries
BINSRCS   := $(shell find $(SRCDIR)/$(BINSRCDIR) -name '*.$(SRCEXT)')
LIBSRCS   := $(shell find $(SRCDIR)/$(LIBSRCDIR) -name '*.$(SRCEXT)')
HDRS      := $(shell find $(HDRDIR) -name '*.$(HDREXT)')
LIBS      := $(patsubst $(SRCDIR)/$(LIBSRCDIR)/%.$(SRCEXT), $(LIBDIR)/lib%.$(LIBEXT), $(LIBSRCS))
BINS      := $(patsubst $(SRCDIR)/$(BINSRCDIR)/%.$(SRCEXT), $(BINDIR)/%, $(BINSRCS))

# Where the output is
OUTPUT     = $(OBJDIR)/*/*.$(OBJEXT) $(OBJDIR)/*.$(OBJEXT) $(LIBDIR)/*.$(LIBEXT) $(BINDIR)/*

# Compiler flags
CXXFLAGS   = -g -Wall -Wshadow -fPIC -I$(HDRDIR) -I../Histogrammer/include -I../CommonTools/ $(ROOTCFLAGS) -std=c++14
LIBFLAGS   = -g -Wl,--as-needed -Wl,--no-undefined -Wl,--no-allow-shlib-undefined -L$(LIBDIR) -L../Histogrammer/lib -Wl,-rpath=$(LIBDIR) $(COMLIBFLAGS) $(ROOTLIBS) $(EXTRA_ROOTLIBS) -L../CommonTools -lKMuNuTools -lFitter -lFitterObjects -lFitArgParse -lrt -lboost_program_options     

all : $(LIBS) $(BINS) 
# Build binaries
$(BINDIR)/% : $(OBJDIR)/$(BINSRCDIR)/%.$(OBJEXT) $(LIBS) $(COMLIBS) | $(BINDIR)
	$(CC) $< -o $@ $(LIBFLAGS)
# Build libraries
$(LIBDIR)/lib%.$(LIBEXT) : $(OBJDIR)/$(LIBSRCDIR)/%.$(OBJEXT) $(HDRS) | $(LIBDIR)
	$(CC) -shared $< -o $@ -Wl,--as-needed $(COMLIBFLAGS) $(ROOTLIBS) $(EXTRA_ROOTLIBS)
# Build objects
$(OBJDIR)/%.$(OBJEXT) : $(SRCDIR)/%.$(SRCEXT) $(HDRS) | $(OBJDIR) $(OBJDIR)/$(LIBSRCDIR) $(OBJDIR)/$(BINSRCDIR)
	$(CC) $(CXXFLAGS) -c $< -o $@
# Make directories
$(BINDIR) $(LIBDIR) $(OBJDIR) $(OBJDIR)/$(LIBSRCDIR) $(OBJDIR)/$(BINSRCDIR) :
	mkdir -p $@
# Remove all the output
clean :
	$(RM) $(OUTPUT)
