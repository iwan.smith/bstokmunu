/*
 * FitterObjects.cpp
 *
 *  Created on: 16 Aug 2017
 *      Author: ismith
 */

#include "FitterObjects.h"
#include "RooFitHeader.h"
#include "functional"

using namespace HistFactory;

using std::vector;
using std::pair;

shared_ptr<RooFitResult> FitterObjects::FitMeasurement(HistFactory::Measurement& Fitter, map<string,string> ConstrainedPars)
{



    Fit_ws = HistFactory::MakeModelAndMeasurementFast( Fitter );
    Fit_mc = (ModelConfig*) Fit_ws->obj("ModelConfig");

    ((RooRealVar*)(Fit_mc->GetNuisanceParameters()->find("Lumi")))->setConstant(true);

    Fit_model = (RooSimultaneous*) Fit_mc->GetPdf(); //ws->pdf("simPdf");


    // Set the low fraction to be 1-HighFraction
    if ( not ConstrainedPars.empty())
    {
        RooCustomizer *changedPdf = new RooCustomizer(*Fit_model,"changedPdf");
        for (auto & Mode: ConstrainedPars)
        {
          RooRealVar* f_K_Low = Fit_ws->var(Mode.first.c_str());
          RooRealVar* f_K_High= Fit_ws->var(Mode.second.c_str());

          cout << f_K_Low << "  " << f_K_High << std::endl;

          RooFormulaVar *F_new_Low = new RooFormulaVar(Mode.first.c_str(),"1 - @0",RooArgList(*f_K_High));

          changedPdf->replaceArg(*f_K_Low,*F_new_Low);

          //RooRealVar* VartoReplace = (RooRealVar*)Fit_mc->GetNuisanceParameters()->find(Mode.second.c_str());
          //*VartoReplace = *(RooRealVar*)F_new_High;

        }
        Fit_model = (RooSimultaneous*)changedPdf->build();


    }


    Fit_data = (RooDataSet*) Fit_ws->data("obsData");
    Fit_poi = (RooRealVar*) Fit_mc->GetParametersOfInterest()->createIterator()->Next();

    Fit_model_hf = shared_ptr<HistFactory::HistFactorySimultaneous>( new HistFactory::HistFactorySimultaneous( *Fit_model ) );

    Fit_nll_hf = shared_ptr<RooAbsReal>( Fit_model_hf->createNLL( *Fit_data ,Offset(kTRUE) ) );   //Add external constarints in the argument.


    Fit_minuit_hf = shared_ptr<RooMinuit>( new RooMinuit( *Fit_nll_hf ) );
    Fit_minuit_hf->setErrorLevel(0.5);
    Fit_minuit_hf->setStrategy(2);



    /***********************************************
    *    Run The Fit
    ***********************************************/


    Fit_minuit_hf->fit("smh");
    //Fit_minuit_hf->minos(RooArgSet(*Fit_poi));


    Fit_results = shared_ptr<RooFitResult>(Fit_minuit_hf->save() );

    Fit_Nav = shared_ptr<HistFactoryNavigation>( new HistFactoryNavigation( Fit_mc ) );


    return Fit_results;

}






/**********************************************************************
 * A class which contains all the required information for the template
 **********************************************************************/


TemplateObjects::TemplateObjects(string Name, shared_ptr<TH1F> H, bool IsStat)
    :HistFactory::Sample( Name )
    ,m_Name( Name )
    ,Histograms{H}
    ,Hist(H)
    ,isStatActive(IsStat)
{

    cout << isStatActive << "  " << IsStat << endl;
    SetHisto( (TH1F*)Hist->Clone( ( Name+"_forFit").c_str() ));
    SetNormalizeByTheory(false);
    if ( isStatActive )
    {
        ActivateStatError();
    }
}

TemplateObjects::TemplateObjects(string Name, vector<shared_ptr<TH1F>> Hs, bool IsStat)
        :HistFactory::Sample( Name )
        ,m_Name( Name )
        ,Histograms(Hs)
        ,Hist(Hs.at(0))
        ,isStatActive(IsStat)
    {

        cout << isStatActive << "  " << IsStat << endl;
        SetHisto( (TH1F*)Hist->Clone( ( Name+"_forFit").c_str() ));
        SetNormalizeByTheory(false);
        if ( isStatActive )
        {
            ActivateStatError();
        }
    }


shared_ptr<TH1F> TemplateObjects::ConstructHist(int id) const
{

    shared_ptr<TH1F> ScaledHist ( (TH1F*) Histograms.at(id)->Clone() );

    ScaledHist->Scale( GetYield() );

    return ScaledHist;

}

double TemplateObjects::GetYield() const
{

    double Yield = 1;

    for( auto NF: FitParameters )
    {
        Yield *= NF->getValV();
    }


    return Yield;

}
double TemplateObjects::GetError() const
{

    double Errorsq = 0;
    double Yield = 1;


    for( auto NF: FitParameters )
    {
        Yield *= NF->getValV();
        Errorsq += pow(NF->getError()/NF->getValV(), 2);
    }


    return Yield * sqrt(Errorsq);

}


TemplateObjects& TemplateObjects::AddNormFactor(std::string Name, Double_t Val, Double_t Low, Double_t High, bool Const)
{
    HistFactory::Sample::AddNormFactor(Name, Val, Low, High, Const);

    return *this;
}

TemplateObjects& TemplateObjects::AddOverallSys( std::string Name, Double_t Low, Double_t High ){

    HistFactory::Sample::AddOverallSys(Name, Low, High);

    return *this;

}


/**********************************************************************
 * A class which combines templates from above
 **********************************************************************/


shared_ptr<TH1F> TemplateCombiner::ConstructHist(int id) const
{

    shared_ptr<TH1F> ScaledHist;

    for( auto &FO: FOs)
    {
        shared_ptr<TH1F> h_temp = FO.get().ConstructHist(id);

        if ( ScaledHist )
        {
            ScaledHist->Add(h_temp.get());
        }
        else
        {
            ScaledHist = h_temp;
        }
    }

    return ScaledHist;
};

double TemplateCombiner::GetYield() const
{
  double Yield = 0;
  for (auto& FO: FOs)
  {
    Yield += FO.get().GetYield();
  }

  return Yield;
}
double TemplateCombiner::GetError() const
{
  double Errorsq = 0;
  for (auto& FO: FOs)
  {
      Errorsq += pow(FO.get().GetError(), 2);
  }

  return sqrt( Errorsq );
}




vector<shared_ptr<TH1F>> PseudoDataBuilder::BuildPseudoData()
{

  vector<shared_ptr<TH1F>> PseudoData;

  for ( auto& h: DataHists)
  {
      shared_ptr<TH1F> NewHist( (TH1F*)h->Clone() );
      NewHist->Reset();
      PseudoData.push_back(NewHist);
  }

  for( auto& FO:FitterObjects)
  {

    double Yield = -1;
    double Error = FO.second.GetError();
    while ( Yield < 0 )
    {
      Yield = FO.second.GetYield() + gsl_ran_gaussian (prng, Error);
    }
    cout << FO.second.SimpleName << "  " << FO.second.GetYield() << "  " << FO.second.GetError() <<"  " << Yield << "  "<<endl;

    ToyYields[FO.second.SimpleName] = Yield;

    for( int id_h = 0; id_h < (int)PseudoData.size(); id_h++)
    {
        shared_ptr<TH1F> h_base = FO.second.ConstructHist(id_h);
        shared_ptr<TH1F> h_out  = PseudoData.at(id_h);

      for (int ev = 0; ev < Yield; ev++)
      {
        h_out->Fill(h_base->GetRandom());
      }
    }
  }

  return PseudoData;

}





TreeBuilder::TreeBuilder(const string& filename, const string& treename)
{
  f_Out = TFile::Open(filename.c_str(), "RECREATE");
  T_Out = new TTree(treename.c_str(), treename.c_str());

}

void TreeBuilder::init(const map<string, double>& Branches)
{

  int branch = 0;
  for( auto& Branch:Branches)
  {
    const string& BranchName = Branch.first;
    T_Out->Branch(BranchName.c_str(), &BranchValues[branch], (BranchName+"/D").c_str());
    branch++;
  }

}


void TreeBuilder::AddEntry(const map<string, double>& Branches)
{

  int branch = 0;
  for( auto& Branch:Branches)
  {
    const double& BranchVal = Branch.second;
    BranchValues[branch] = BranchVal;
    branch++;
  }

  T_Out->Fill();

}


void TreeBuilder::Close()
{

  f_Out->cd();
  T_Out->Write();
  f_Out->Close();


}
TreeBuilder::~TreeBuilder()
{
  //delete T_Out;
  //delete f_Out;

}

