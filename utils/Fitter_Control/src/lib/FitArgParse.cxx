/*
 * FitArgParse.cpp
 *
 *  Created on: 8 Mar 2018
 *      Author: ismith
 */


#include "FitArgParse.h"

#include <boost/program_options.hpp>
using namespace boost;
namespace po = boost::program_options;

using std::string;

FitArgParse::FitArgParse()
    :desc("Allowed options")
    ,m_ROOTInputName("")
    ,m_FitHistogram ("")
    ,m_CutName      ("")
    ,m_Correction   ("")
    ,m_nToys        (0)
    ,m_ToyOutputName("")
{}



void FitArgParse::Init
    (string ROOTInputName
    ,string FitHistogram
    ,string CutName
    ,string Correction
    ,int    nToys
    ,string ToyOutputName)
{

      desc.add_options()
          ("help,h", "produce help message")

          ("HistFile",   po::value<string>(&m_ROOTInputName)->default_value(ROOTInputName), "Input file containing histograms")
          ("FitHist",    po::value<string>(&m_FitHistogram) ->default_value(FitHistogram ), "The histogram name to fit to" )
          ("Cut",        po::value<string>(&m_CutName)      ->default_value(CutName      ), "The Cut to be used when fitting" )
          ("Correction", po::value<string>(&m_Correction)   ->default_value(Correction   ), "The correction to be used when fitting" )
          ("nToys",      po::value<int>   (&m_nToys)        ->default_value(nToys        ), "Number of Toys to generate and fit")
          ("Output,o",   po::value<string>(&m_ToyOutputName)->default_value(ToyOutputName), "Output file to store toy results");


  // TODO Auto-generated constructor stub

}

FitArgParse::~FitArgParse ()
{
  // TODO Auto-generated destructor stub
}




void FitArgParse::Parse(int ac, char* av[])
{
  try{

  po::store(po::parse_command_line(ac, av, desc), vm);
  po::notify(vm);


    if (vm.count("help"))
    {
      std::cout << "Usage: ./bin/RunHistogrmmer [options]\n";
      std::cout << desc;

      std::exit(0);
    }
  }
  catch(std::exception& e)
  {
    std::cout << e.what() << "\n";
    std::abort();
  }
}
