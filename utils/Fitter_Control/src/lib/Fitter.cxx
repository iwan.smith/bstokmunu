#include "Fitter.h"



HistFactory::Channel& ChannelBuilder::GetChannel()
{

  DsMuNu_Fit_Channel = HistFactory::Channel(ChannelName);
  GenerateChannel(
      DsMuNu_Fit_Channel,
      h_data.at(0),
      FitTemplates);

  return DsMuNu_Fit_Channel;
}

TemplateObjects&   ChannelBuilder::at(string name)
{
  try
  {

      TemplateObjects& FT = FitTemplates.at(name);
      return FT;
  }
  catch (std::out_of_range& e) {
      cout << name << " Is not found. ABORTING!"<< endl;
      cout << e.what() << endl;
      abort();
  }

  return FitTemplates[name];
}

void ChannelBuilder::ProcessFit(const FitterObjects& DsFitter, map<string,string>  ConstrainedPars)
{


  for( auto &T: FitTemplates)
  {
      string sampleName = T.first;
      TemplateObjects& FO = T.second;

      FO.Hist_After = shared_ptr<TH1F>( (TH1F*)DsFitter.Fit_Nav->GetSampleHist(ChannelName, sampleName, sampleName+ "_after") );


     for( auto& NF: FO.GetNormFactorList() )
     {
         RooRealVar* FitPar = nullptr;
         std::string NFName = NF.GetName();

         auto ParModified = ConstrainedPars.find(NFName);

         if (ParModified != ConstrainedPars.end())
         {


             RooRealVar* OtherPar = (RooRealVar*)DsFitter.Fit_mc->GetNuisanceParameters()->find(ParModified->second.c_str());

             double val = OtherPar->getVal();
             double err = OtherPar->getError();

             FitPar =  new RooRealVar(ParModified->first.c_str(), ParModified->first.c_str(), 1.0-val, 0.0, 1.0);
             FitPar->setError(err);



         }

        if ( not FitPar )
          FitPar = (RooRealVar*)DsFitter.Fit_mc->GetNuisanceParameters()->find(NFName.c_str());

        if ( not FitPar)
        // If the paramtere is a nullpointer use the POI
           FitPar = (RooRealVar*) DsFitter.Fit_mc->GetParametersOfInterest()->createIterator()->Next();

        cout << "Fit Parameter: " << FitPar->GetName() << "  :  "<< FitPar->getValV() << endl;

        FO.FitParameters.push_back( FitPar );
        //FitPar->GetName();
     }
  }







  for ( auto& CH: CombinedHists)
  {
    string name = CH.second.SimpleName;
    auto&  Hist = CH.second;


    double FitYield = Hist.GetYield();
    double FitErr   = Hist.GetError();


    results[name]        = FitYield;
    results[name+"_err"] = FitErr;

    if ( Builder.HistogramsLoaded )
    {
      double FitInput = Builder.ToyYields.at(CH.second.SimpleName);

      results[name+"_in"]   = FitInput;
      results[name+"_res"]  = FitYield - FitInput;
      results[name+"_pull"] = (FitYield - FitInput)/FitErr;


    }

  }
  //results["Fit_Covariance"]  = FitResults->covQual();
  //results["Fit_EDM"]         = FitResults->edm();







}


void ChannelBuilder::BuildPseudoData(int nToys)
{

  Builder = PseudoDataBuilder(h_data, CombinedHists);
  for (int toy = 0; toy < nToys; toy++)
  {
    h_data_Toys.push_back( Builder.BuildPseudoData() );
    h_data_Toy_Yields.push_back( Builder.ToyYields);
  }
}




void Fitter::BuildPseudoData(int nToys)
{

  for ( auto& c: Channels)
  {
      c.get().BuildPseudoData(nToys);
  }


}
void Fitter::Fit(string POI, map<string,string> ConstrainedPars)
{

  HistFactory::Measurement DsMuNu_Fitter = GenerateMeasurement();
  DsMuNu_Fitter.SetPOI(POI);

  for ( auto& c: Channels)
  {
      HistFactory::Channel& TempChannel = c.get().GetChannel();

      DsMuNu_Fitter.AddChannel(TempChannel);
  }



  FitterObjects DsFitter;
  FitResults = DsFitter.FitMeasurement(DsMuNu_Fitter, ConstrainedPars);

  for ( auto& c: Channels)
  {
      c.get().ProcessFit(DsFitter, ConstrainedPars);
  }


}

