/*
 * Newfitter2.cxx
 *
 *  Created on: 17 Aug 2017
 *      Author: ismith
 */

// header including everything
#include "FitterHeader.h"
#include "RooFitHeader.h"

#include "FitterFunctions.h"
#include "FitterObjects.h"

#include "HistogrammerTools.h"

#include "Fitter.h"

#include "FitArgParse.h"

vector<shared_ptr<TH1F>> LoadHistograms(std::string Histname, TFile* f_hist, std::string Cut, std::string Systematic = "", bool isSmeared = false)
{


  vector<shared_ptr<TH1F>> Histograms;


  if ( Histname == "Data")
  {
    bool normalise = false;
    bool smooth = false;
    bool ClearUnderFlow = true;

    Histograms = {
      GetHist(f_hist, "Nominal/DsMuNu/Data/" + Cut + "/h_Bs_MCORR2_rmbg", normalise, smooth, ClearUnderFlow, isSmeared ),
      GetHist(f_hist, "sWeight/DsMuNu/Data/" + Cut + "/h_DsStar_DM_0_0",  normalise, smooth, false         , isSmeared ),
      GetHist(f_hist, "sWeight/DsMuNu/Data/" + Cut + "/h_Bs_IPCHI2",      normalise, smooth, false         , isSmeared ),
      GetHist(f_hist, "sWeight/DsMuNu/Data/" + Cut + "/h_Ds_IPCHI2",      normalise, smooth, false         , isSmeared ),
      GetHist(f_hist, "sWeight/DsMuNu/Data/" + Cut + "/h_Bs_DIRA",        normalise, smooth, false         , isSmeared ),
      GetHist(f_hist, "sWeight/DsMuNu/Data/" + Cut + "/h_Bs_ETA",         normalise, smooth, false         , isSmeared ),
      GetHist(f_hist, "sWeight/DsMuNu/Data/" + Cut + "/h_Bs_PT",          normalise, smooth, false         , isSmeared ),
      GetHist(f_hist, "sWeight/DsMuNu/Data/" + Cut + "/h_Bs_MM",          normalise, smooth, false         , isSmeared ),
      GetHist(f_hist, "sWeight/DsMuNu/Data/" + Cut + "/h_Bs_EVTX_CHI2",   normalise, smooth, false         , isSmeared ),
      GetHist(f_hist, "sWeight/DsMuNu/Data/" + Cut + "/h_Gamma_pT",       normalise, smooth, false         , isSmeared ),
    };
  }


  else if ( Histname == "DsMuNu")
  {
    bool normalise = true;
    bool smooth = false;
    bool ClearUnderFlow = true;

    Histograms = {
      GetHist(f_hist, (Systematic + "/DsMuNu/1377400X_Ds/" + Cut + "/h_Bs_MCORR2").c_str(),     normalise, smooth, ClearUnderFlow, isSmeared ),
      GetHist(f_hist, (Systematic + "/DsMuNu/1377400X_Ds/" + Cut + "/h_DsStar_DM_0_2").c_str(), normalise, smooth, false         , isSmeared ),
      GetHist(f_hist, (Systematic + "/DsMuNu/1377400X_Ds/" + Cut + "/h_Bs_IPCHI2").c_str(),     normalise, smooth, false         , isSmeared ),
      GetHist(f_hist, (Systematic + "/DsMuNu/1377400X_Ds/" + Cut + "/h_Ds_IPCHI2").c_str(),     normalise, smooth, false         , isSmeared ),
      GetHist(f_hist, (Systematic + "/DsMuNu/1377400X_Ds/" + Cut + "/h_Bs_DIRA").c_str(),       normalise, smooth, false         , isSmeared ),
      GetHist(f_hist, (Systematic + "/DsMuNu/1377400X_Ds/" + Cut + "/h_Bs_ETA").c_str(),        normalise, smooth, false         , isSmeared ),
      GetHist(f_hist, (Systematic + "/DsMuNu/1377400X_Ds/" + Cut + "/h_Bs_PT").c_str(),         normalise, smooth, false         , isSmeared ),
      GetHist(f_hist, (Systematic + "/DsMuNu/1377400X_Ds/" + Cut + "/h_Bs_MM").c_str(),         normalise, smooth, false         , isSmeared ),
      GetHist(f_hist, (Systematic + "/DsMuNu/1377400X_Ds/" + Cut + "/h_Bs_EVTX_CHI2").c_str(),  normalise, smooth, false         , isSmeared ),
      GetHist(f_hist, (Systematic + "/DsMuNu/1377400X_Ds/" + Cut + "/h_Gamma_pT").c_str(),      normalise, smooth, false         , isSmeared ),
    };
  }
  else if ( Histname == "DsStarMuNu")
  {
    bool normalise = true;
    bool smooth = false;
    bool ClearUnderFlow = true;

    Histograms = {
      GetHist(f_hist, (Systematic + "/DsMuNu/13774000_DsStar/" + Cut + "/h_Bs_MCORR2").c_str(),      normalise, smooth, ClearUnderFlow, isSmeared ),
      GetHist(f_hist, (Systematic + "/DsMuNu/13774000_DsStar/" + Cut + "/h_DsStar_DM_0_2").c_str(),  normalise, smooth, false         , isSmeared ),
      GetHist(f_hist, (Systematic + "/DsMuNu/13774000_DsStar/" + Cut + "/h_Bs_IPCHI2").c_str(),      normalise, smooth, false         , isSmeared ),
      GetHist(f_hist, (Systematic + "/DsMuNu/13774000_DsStar/" + Cut + "/h_Ds_IPCHI2").c_str(),      normalise, smooth, false         , isSmeared ),
      GetHist(f_hist, (Systematic + "/DsMuNu/13774000_DsStar/" + Cut + "/h_Bs_DIRA").c_str(),        normalise, smooth, false         , isSmeared ),
      GetHist(f_hist, (Systematic + "/DsMuNu/13774000_DsStar/" + Cut + "/h_Bs_ETA").c_str(),         normalise, smooth, false         , isSmeared ),
      GetHist(f_hist, (Systematic + "/DsMuNu/13774000_DsStar/" + Cut + "/h_Bs_PT").c_str(),          normalise, smooth, false         , isSmeared ),
      GetHist(f_hist, (Systematic + "/DsMuNu/13774000_DsStar/" + Cut + "/h_Bs_MM").c_str(),          normalise, smooth, false         , isSmeared ),
      GetHist(f_hist, (Systematic + "/DsMuNu/13774000_DsStar/" + Cut + "/h_Bs_EVTX_CHI2").c_str(),   normalise, smooth, false         , isSmeared ),
      GetHist(f_hist, (Systematic + "/DsMuNu/13774000_DsStar/" + Cut + "/h_Gamma_pT").c_str(),       normalise, smooth, false         , isSmeared ),
    };
  }
  else if ( Histname == "DsStar0MuNu")
  {
    bool normalise = true;
    bool smooth = false;
    bool ClearUnderFlow = true;

    Histograms = {
      GetHist(f_hist, (Systematic+"/DsMuNu/1377400X_DsStar0/" + Cut + "/h_Bs_MCORR2").c_str(),     normalise, smooth, ClearUnderFlow, isSmeared ),
      GetHist(f_hist, (Systematic+"/DsMuNu/1377400X_DsStar0/" + Cut + "/h_DsStar_DM_0_2").c_str(), normalise, smooth, false         , isSmeared ),
      GetHist(f_hist, (Systematic+"/DsMuNu/1377400X_DsStar0/" + Cut + "/h_Bs_IPCHI2").c_str(),     normalise, smooth, false         , isSmeared ),
      GetHist(f_hist, (Systematic+"/DsMuNu/1377400X_DsStar0/" + Cut + "/h_Ds_IPCHI2").c_str(),     normalise, smooth, false         , isSmeared ),
      GetHist(f_hist, (Systematic+"/DsMuNu/1377400X_DsStar0/" + Cut + "/h_Bs_DIRA").c_str(),       normalise, smooth, false         , isSmeared ),
      GetHist(f_hist, (Systematic+"/DsMuNu/1377400X_DsStar0/" + Cut + "/h_Bs_ETA").c_str(),        normalise, smooth, false         , isSmeared ),
      GetHist(f_hist, (Systematic+"/DsMuNu/1377400X_DsStar0/" + Cut + "/h_Bs_PT").c_str(),         normalise, smooth, false         , isSmeared ),
      GetHist(f_hist, (Systematic+"/DsMuNu/1377400X_DsStar0/" + Cut + "/h_Bs_MM").c_str(),         normalise, smooth, false         , isSmeared ),
      GetHist(f_hist, (Systematic+"/DsMuNu/1377400X_DsStar0/" + Cut + "/h_Bs_EVTX_CHI2").c_str(),  normalise, smooth, false         , isSmeared ),
      GetHist(f_hist, (Systematic+"/DsMuNu/1377400X_DsStar0/" + Cut + "/h_Gamma_pT").c_str(),      normalise, smooth, false         , isSmeared ),
    };
  }

  else if ( Histname == "DsStar1MuNu")
  {
    bool normalise = true;
    bool smooth = false;
    bool ClearUnderFlow = true;

    Histograms = {
      GetHist(f_hist, (Systematic + "/DsMuNu/1377400X_DsStar1/" + Cut + "/h_Bs_MCORR2").c_str(),     normalise, smooth, ClearUnderFlow, isSmeared ),
      GetHist(f_hist, (Systematic + "/DsMuNu/1377400X_DsStar1/" + Cut + "/h_DsStar_DM_0_2").c_str(), normalise, smooth, false         , isSmeared ),
      GetHist(f_hist, (Systematic + "/DsMuNu/1377400X_DsStar1/" + Cut + "/h_Bs_IPCHI2").c_str(),     normalise, smooth, false         , isSmeared ),
      GetHist(f_hist, (Systematic + "/DsMuNu/1377400X_DsStar1/" + Cut + "/h_Ds_IPCHI2").c_str(),     normalise, smooth, false         , isSmeared ),
      GetHist(f_hist, (Systematic + "/DsMuNu/1377400X_DsStar1/" + Cut + "/h_Bs_DIRA").c_str(),       normalise, smooth, false         , isSmeared ),
      GetHist(f_hist, (Systematic + "/DsMuNu/1377400X_DsStar1/" + Cut + "/h_Bs_ETA").c_str(),        normalise, smooth, false         , isSmeared ),
      GetHist(f_hist, (Systematic + "/DsMuNu/1377400X_DsStar1/" + Cut + "/h_Bs_PT").c_str(),         normalise, smooth, false         , isSmeared ),
      GetHist(f_hist, (Systematic + "/DsMuNu/1377400X_DsStar1/" + Cut + "/h_Bs_MM").c_str(),         normalise, smooth, false         , isSmeared ),
      GetHist(f_hist, (Systematic + "/DsMuNu/1377400X_DsStar1/" + Cut + "/h_Bs_EVTX_CHI2").c_str(),  normalise, smooth, false         , isSmeared ),
      GetHist(f_hist, (Systematic + "/DsMuNu/1377400X_DsStar1/" + Cut + "/h_Gamma_pT").c_str(),      normalise, smooth, false         , isSmeared ),
    };
  }

  else if ( Histname == "DsXTauNu")
  {
    bool normalise = true;
    bool smooth = false;
    bool ClearUnderFlow = true;

    Histograms = {
      GetHist(f_hist, (Systematic + "/DsMuNu/1377400X_DsXtau/" + Cut + "/h_Bs_MCORR2").c_str(),     normalise, smooth, ClearUnderFlow, isSmeared ),
      GetHist(f_hist, (Systematic + "/DsMuNu/1377400X_DsXtau/" + Cut + "/h_DsStar_DM_0_2").c_str(), normalise, smooth, false         , isSmeared ),
      GetHist(f_hist, (Systematic + "/DsMuNu/1377400X_DsXtau/" + Cut + "/h_Bs_IPCHI2").c_str(),     normalise, smooth, false         , isSmeared ),
      GetHist(f_hist, (Systematic + "/DsMuNu/1377400X_DsXtau/" + Cut + "/h_Ds_IPCHI2").c_str(),     normalise, smooth, false         , isSmeared ),
      GetHist(f_hist, (Systematic + "/DsMuNu/1377400X_DsXtau/" + Cut + "/h_Bs_DIRA").c_str(),       normalise, smooth, false         , isSmeared ),
      GetHist(f_hist, (Systematic + "/DsMuNu/1377400X_DsXtau/" + Cut + "/h_Bs_ETA").c_str(),        normalise, smooth, false         , isSmeared ),
      GetHist(f_hist, (Systematic + "/DsMuNu/1377400X_DsXtau/" + Cut + "/h_Bs_PT").c_str(),         normalise, smooth, false         , isSmeared ),
      GetHist(f_hist, (Systematic + "/DsMuNu/1377400X_DsXtau/" + Cut + "/h_Bs_MM").c_str(),         normalise, smooth, false         , isSmeared ),
      GetHist(f_hist, (Systematic + "/DsMuNu/1377400X_DsXtau/" + Cut + "/h_Bs_EVTX_CHI2").c_str(),  normalise, smooth, false         , isSmeared ),
      GetHist(f_hist, (Systematic + "/DsMuNu/1377400X_DsXtau/" + Cut + "/h_Gamma_pT").c_str(),      normalise, smooth, false         , isSmeared ),
    };
  }
  else if ( Histname == "Bd_DD")
  {
    bool normalise = true;
    bool smooth = true;
    bool ClearUnderFlow = true;


    Histograms = {
      GetHist(f_hist, (Systematic + "/DsMuNu/MC_11876001/" + Cut + "/h_Bs_MCORR2").c_str(),     normalise, smooth, ClearUnderFlow, isSmeared ),
      GetHist(f_hist, (Systematic + "/DsMuNu/MC_11876001/" + Cut + "/h_DsStar_DM_0_2").c_str(), normalise, smooth, false         , isSmeared ),
      GetHist(f_hist, (Systematic + "/DsMuNu/MC_11876001/" + Cut + "/h_Bs_IPCHI2").c_str(),     normalise, smooth, false         , isSmeared ),
      GetHist(f_hist, (Systematic + "/DsMuNu/MC_11876001/" + Cut + "/h_Ds_IPCHI2").c_str(),     normalise, smooth, false         , isSmeared ),
      GetHist(f_hist, (Systematic + "/DsMuNu/MC_11876001/" + Cut + "/h_Bs_DIRA").c_str(),       normalise, smooth, false         , isSmeared ),
      GetHist(f_hist, (Systematic + "/DsMuNu/MC_11876001/" + Cut + "/h_Bs_ETA").c_str(),        normalise, smooth, false         , isSmeared ),
      GetHist(f_hist, (Systematic + "/DsMuNu/MC_11876001/" + Cut + "/h_Bs_PT").c_str(),         normalise, smooth, false         , isSmeared ),
      GetHist(f_hist, (Systematic + "/DsMuNu/MC_11876001/" + Cut + "/h_Bs_MM").c_str(),         normalise, smooth, false         , isSmeared ),
      GetHist(f_hist, (Systematic + "/DsMuNu/MC_11876001/" + Cut + "/h_Bs_EVTX_CHI2").c_str(),  normalise, smooth, false         , isSmeared ),
      GetHist(f_hist, (Systematic + "/DsMuNu/MC_11876001/" + Cut + "/h_Gamma_pT").c_str(),      normalise, smooth, false         , isSmeared ),
    };
  }

  else if ( Histname == "Bu_DD")
  {
    bool normalise = true;
    bool smooth = true;
    bool ClearUnderFlow = true;

    Histograms = {
      GetHist(f_hist, (Systematic + "/DsMuNu/MC_12875601/" + Cut + "/h_Bs_MCORR2").c_str(),     normalise, smooth, ClearUnderFlow, isSmeared ),
      GetHist(f_hist, (Systematic + "/DsMuNu/MC_12875601/" + Cut + "/h_DsStar_DM_0_0").c_str(), normalise, smooth, false         , isSmeared ),
      GetHist(f_hist, (Systematic + "/DsMuNu/MC_12875601/" + Cut + "/h_Bs_IPCHI2").c_str(),     normalise, smooth, false         , isSmeared ),
      GetHist(f_hist, (Systematic + "/DsMuNu/MC_12875601/" + Cut + "/h_Ds_IPCHI2").c_str(),     normalise, smooth, false         , isSmeared ),
      GetHist(f_hist, (Systematic + "/DsMuNu/MC_12875601/" + Cut + "/h_Bs_DIRA").c_str(),       normalise, smooth, false         , isSmeared ),
      GetHist(f_hist, (Systematic + "/DsMuNu/MC_12875601/" + Cut + "/h_Bs_ETA").c_str(),        normalise, smooth, false         , isSmeared ),
      GetHist(f_hist, (Systematic + "/DsMuNu/MC_12875601/" + Cut + "/h_Bs_PT").c_str(),         normalise, smooth, false         , isSmeared ),
      GetHist(f_hist, (Systematic + "/DsMuNu/MC_12875601/" + Cut + "/h_Bs_MM").c_str(),         normalise, smooth, false         , isSmeared ),
      GetHist(f_hist, (Systematic + "/DsMuNu/MC_12875601/" + Cut + "/h_Bs_EVTX_CHI2").c_str(),  normalise, smooth, false         , isSmeared ),
      GetHist(f_hist, (Systematic + "/DsMuNu/MC_12875601/" + Cut + "/h_Gamma_pT").c_str(),      normalise, smooth, false         , isSmeared ),
    };
  }

  else if ( Histname == "Bs_DD")
  {
    bool normalise = true;
    bool smooth = true;
    bool ClearUnderFlow = true;

    Histograms = {
      GetHist(f_hist, (Systematic + "/DsMuNu/MC_13873201/" + Cut + "/h_Bs_MCORR2").c_str(),     normalise, smooth, ClearUnderFlow, isSmeared ),
      GetHist(f_hist, (Systematic + "/DsMuNu/MC_13873201/" + Cut + "/h_DsStar_DM_0_2").c_str(), normalise, smooth, false         , isSmeared ),
      GetHist(f_hist, (Systematic + "/DsMuNu/MC_13873201/" + Cut + "/h_Bs_IPCHI2").c_str(),     normalise, smooth, false         , isSmeared ),
      GetHist(f_hist, (Systematic + "/DsMuNu/MC_13873201/" + Cut + "/h_Ds_IPCHI2").c_str(),     normalise, smooth, false         , isSmeared ),
      GetHist(f_hist, (Systematic + "/DsMuNu/MC_13873201/" + Cut + "/h_Bs_DIRA").c_str(),       normalise, smooth, false         , isSmeared ),
      GetHist(f_hist, (Systematic + "/DsMuNu/MC_13873201/" + Cut + "/h_Bs_ETA").c_str(),        normalise, smooth, false         , isSmeared ),
      GetHist(f_hist, (Systematic + "/DsMuNu/MC_13873201/" + Cut + "/h_Bs_PT").c_str(),         normalise, smooth, false         , isSmeared ),
      GetHist(f_hist, (Systematic + "/DsMuNu/MC_13873201/" + Cut + "/h_Bs_MM").c_str(),         normalise, smooth, false         , isSmeared ),
      GetHist(f_hist, (Systematic + "/DsMuNu/MC_13873201/" + Cut + "/h_Bs_EVTX_CHI2").c_str(),  normalise, smooth, false         , isSmeared ),
      GetHist(f_hist, (Systematic + "/DsMuNu/MC_13873201/" + Cut + "/h_Gamma_pT").c_str(),      normalise, smooth, false         , isSmeared ),
    };
  }

  else if ( Histname == "FakeMu")
  {
    bool normalise = false;
    bool smooth = true;
    bool ClearUnderFlow = true;

    Histograms = {
      GetHist(f_hist, ("Nominal/DsMuNu/FakeMu/Nominal/h_Bs_MCORR2_rmbg"),     normalise, smooth, ClearUnderFlow, isSmeared ),
      GetHist(f_hist, ("sWeight/DsMuNu/FakeMu/Nominal/h_DsStar_DM_0_0"), normalise, smooth, false         , isSmeared ),
      GetHist(f_hist, ("sWeight/DsMuNu/FakeMu/Nominal/h_Bs_IPCHI2"),     normalise, smooth, false         , isSmeared ),
      GetHist(f_hist, ("sWeight/DsMuNu/FakeMu/Nominal/h_Ds_IPCHI2"),     normalise, smooth, false         , isSmeared ),
      GetHist(f_hist, ("sWeight/DsMuNu/FakeMu/Nominal/h_Bs_DIRA"),       normalise, smooth, false         , isSmeared ),
      GetHist(f_hist, ("sWeight/DsMuNu/FakeMu/Nominal/h_Bs_ETA"),        normalise, smooth, false         , isSmeared ),
      GetHist(f_hist, ("sWeight/DsMuNu/FakeMu/Nominal/h_Bs_PT"),         normalise, smooth, false         , isSmeared ),
      GetHist(f_hist, ("sWeight/DsMuNu/FakeMu/Nominal/h_Bs_MM"),         normalise, smooth, false         , isSmeared ),
      GetHist(f_hist, ("sWeight/DsMuNu/FakeMu/Nominal/h_Bs_EVTX_CHI2"),  normalise, smooth, false         , isSmeared ),
      GetHist(f_hist, ("sWeight/DsMuNu/FakeMu/Nominal/h_Gamma_pT"),      normalise, smooth, false         , isSmeared ),
    };

  }

  else if ( Histname == "SameSign")
  {
    bool normalise = false;
    bool smooth = true;
    bool ClearUnderFlow = true;

    Histograms = {
      GetHist(f_hist, ("Nominal/DsMuNu/FakeMu/Nominal/h_Bs_MCORR2_rmbg"),     normalise, smooth, ClearUnderFlow, isSmeared ),
      GetHist(f_hist, ("sWeight/DsMuNu/FakeMu/Nominal/h_DsStar_DM_0_0"), normalise, smooth, false         , isSmeared ),
      GetHist(f_hist, ("sWeight/DsMuNu/FakeMu/Nominal/h_Bs_IPCHI2"),     normalise, smooth, false         , isSmeared ),
      GetHist(f_hist, ("sWeight/DsMuNu/FakeMu/Nominal/h_Ds_IPCHI2"),     normalise, smooth, false         , isSmeared ),
      GetHist(f_hist, ("sWeight/DsMuNu/FakeMu/Nominal/h_Bs_DIRA"),       normalise, smooth, false         , isSmeared ),
      GetHist(f_hist, ("sWeight/DsMuNu/FakeMu/Nominal/h_Bs_ETA"),        normalise, smooth, false         , isSmeared ),
      GetHist(f_hist, ("sWeight/DsMuNu/FakeMu/Nominal/h_Bs_PT"),         normalise, smooth, false         , isSmeared ),
      GetHist(f_hist, ("sWeight/DsMuNu/FakeMu/Nominal/h_Bs_MM"),         normalise, smooth, false         , isSmeared ),
      GetHist(f_hist, ("sWeight/DsMuNu/FakeMu/Nominal/h_Bs_EVTX_CHI2"),  normalise, smooth, false         , isSmeared ),
      GetHist(f_hist, ("sWeight/DsMuNu/FakeMu/Nominal/h_Gamma_pT"),      normalise, smooth, false         , isSmeared ),
    };
  }

  return Histograms;
}

int main(int ac, char* av[])
{

  FitArgParse Arguments;

  Arguments.Init(
      "/eos/lhcb/wg/semileptonic/Bs2KmunuAna/Histogrammer/Output/Histograms.root"
     ,"h_Bs_MCORR"
     ,"PID_Tight"
     ,"Default"
     ,0
     ,"Fit_Toys_DsMuNu.root"
      );

  Arguments.Parse( ac, av);

  string Systematic = Arguments.m_Correction;
  TreeBuilder SaveToys(Arguments.m_ToyOutputName, "FitToys");

  vector<vector<shared_ptr<TH1F>>> PseudoData      ;//= DsMuNuChannel.GetPseudoData();
  vector<map<string,double>>       PseudoDataYields;//= DsMuNuChannel.GetPseudoDataYieds();


  for(int toy = -1; toy < Arguments.m_nToys; toy++)
  {

    bool SmearMC = toy>=0;

    ChannelBuilder DsMuNuChannel("DsMuNuChannel");


      TFile* f_hist = TFile::Open(
              "/eos/lhcb/wg/semileptonic/Bs2KmunuAna/Histogrammer/Output/Histograms.root",
              //"../Histogrammer/Histograms2.root",
              "READ");

      if ( toy == -1 )
        DsMuNuChannel.DataTemplates() = LoadHistograms("Data", f_hist, Arguments.m_CutName, "");
      else
        DsMuNuChannel.DataTemplates() = PseudoData.at(toy);

      DsMuNuChannel.TemplateContainer()["DsMuNu"]     = TemplateObjects(
          "DsMuNu",
          LoadHistograms("DsMuNu", f_hist, Arguments.m_CutName, Arguments.m_Correction, SmearMC),
          true);

      DsMuNuChannel.TemplateContainer()["DsStarMuNu"] = TemplateObjects(
          "DsStarMuNu",
          LoadHistograms("DsStarMuNu", f_hist, Arguments.m_CutName, Arguments.m_Correction, SmearMC),
          true);

      DsMuNuChannel.TemplateContainer()["DsStar0MuNu"] = TemplateObjects(
          "DsStar0MuNu",
          LoadHistograms("DsStar0MuNu", f_hist, Arguments.m_CutName, Arguments.m_Correction, SmearMC),
          false);

      DsMuNuChannel.TemplateContainer()["DsStar1MuNu"] = TemplateObjects(
          "DsStar1MuNu",
          LoadHistograms("DsStar1MuNu", f_hist, Arguments.m_CutName, Arguments.m_Correction, SmearMC),
          false);

      DsMuNuChannel.TemplateContainer()["DsXTauNu"]    = TemplateObjects(
          "DsXTauNu",
          LoadHistograms("DsXTauNu", f_hist, Arguments.m_CutName, Arguments.m_Correction, SmearMC),
          false);

      DsMuNuChannel.TemplateContainer()["Bd_DD"]       = TemplateObjects(
          "Bd_DD",
          LoadHistograms("Bd_DD", f_hist, Arguments.m_CutName, Arguments.m_Correction, SmearMC),
          false);

      DsMuNuChannel.TemplateContainer()["Bs_DD"]       = TemplateObjects(
          "Bs_DD",
          LoadHistograms("Bs_DD", f_hist, Arguments.m_CutName, Arguments.m_Correction, SmearMC),
          false);

      DsMuNuChannel.TemplateContainer()["Bu_DD"]       = TemplateObjects(
          "Bu_DD",
          LoadHistograms("Bu_DD", f_hist, Arguments.m_CutName, Arguments.m_Correction, SmearMC),
          false);

      DsMuNuChannel.TemplateContainer()["FakeMu"]      = TemplateObjects(
          "FakeMu",
          LoadHistograms("FakeMu", f_hist, Arguments.m_CutName, Arguments.m_Correction, SmearMC),
          false);


      DsMuNuChannel.TemplateContainer()["SameSign"]      = TemplateObjects(
          "SameSign",
          LoadHistograms("SameSign", f_hist, Arguments.m_CutName, Arguments.m_Correction, SmearMC),
          false);


      double n_data = DsMuNuChannel.DataTemplates().at(0)->Integral();

      DsMuNuChannel.TemplateContainer().at("DsMuNu")     .AddNormFactor("Ds_Yield",         n_data*0.25,  0.0,     n_data);

      DsMuNuChannel.TemplateContainer().at("DsStarMuNu") .AddNormFactor("DsStar_Yield",     n_data*0.75,  0.0,     n_data);

      DsMuNuChannel.TemplateContainer().at("DsStar0MuNu").AddNormFactor("DsStar0_Fraction", 0.273 ,       0.273,   0.273,    true);
      DsMuNuChannel.TemplateContainer().at("DsStar0MuNu").AddNormFactor("DsStarN_Yield",    n_data*0.05,  0,       n_data);

      DsMuNuChannel.TemplateContainer().at("DsStar1MuNu").AddNormFactor("DsStar1_Fraction", 0.727 ,       0.727,   0.727,    true);
      DsMuNuChannel.TemplateContainer().at("DsStar1MuNu").AddNormFactor("DsStarN_Yield",    n_data*0.05,  0,       n_data);

      DsMuNuChannel.TemplateContainer().at("DsXTauNu")   .AddNormFactor("DsXTauNu_Yield",   n_data*0.05 , 0.0,     n_data);

      DsMuNuChannel.TemplateContainer().at("Bd_DD")      .AddNormFactor("BX_DD_Yield",      n_data*0.01 , 0,       n_data*0.1);
      DsMuNuChannel.TemplateContainer().at("Bd_DD")      .AddNormFactor("Bd_DD_Fraction",   0.4 ,         0.4,     0.4,      true);
      DsMuNuChannel.TemplateContainer().at("Bd_DD")      .AddOverallSys("Bd_DD_Syst",       0.8,         1.2);

      DsMuNuChannel.TemplateContainer().at("Bu_DD")      .AddNormFactor("BX_DD_Yield",      n_data*0.01 , 0,       n_data*0.1);
      DsMuNuChannel.TemplateContainer().at("Bu_DD")      .AddNormFactor("Bu_DD_Fraction",   0.4 ,         0.4,     0.4,      true);
      DsMuNuChannel.TemplateContainer().at("Bu_DD")      .AddOverallSys("Bu_DD_Syst",       0.8,         1.2);

      DsMuNuChannel.TemplateContainer().at("Bs_DD")      .AddNormFactor("BX_DD_Yield",      n_data*0.01 , 0,       n_data*0.1);
      DsMuNuChannel.TemplateContainer().at("Bs_DD")      .AddNormFactor("Bs_DD_Fraction",   0.2 ,         0.2,     0.2,      true);
      DsMuNuChannel.TemplateContainer().at("Bs_DD")      .AddOverallSys("Bs_DD_Syst",       0.8,         1.2);


      DsMuNuChannel.TemplateContainer().at("FakeMu")     .AddNormFactor("FakeMu_Yield",     0.1 ,           0,       1        );
      DsMuNuChannel.TemplateContainer().at("SameSign")   .AddNormFactor("SameSign_Yield",     0.1 ,           0,       1        );


      DsMuNuChannel.CombinedTemplates()["B_{s} #rightarrow D_{s} #mu #nu"]             = TemplateCombiner( "DsMuNu",      DsMuNuChannel.TemplateContainer().at("DsMuNu") );
      DsMuNuChannel.CombinedTemplates()["B_{s} #rightarrow D_{s}* #mu #nu"]            = TemplateCombiner( "DsStarMuNu",  DsMuNuChannel.TemplateContainer().at("DsStarMuNu") );
      DsMuNuChannel.CombinedTemplates()["B_{s} #rightarrow D_{s}*^{0,1} #mu #nu"]      = TemplateCombiner( "DsStarXMuNu", DsMuNuChannel.TemplateContainer().at("DsStar1MuNu"), DsMuNuChannel.TemplateContainer().at("DsStar1MuNu") );
      DsMuNuChannel.CombinedTemplates()["B_{s} #rightarrow D_{s} #tau #nu"]            = TemplateCombiner( "DsTauNu",     DsMuNuChannel.TemplateContainer().at("DsXTauNu") );
      DsMuNuChannel.CombinedTemplates()["B_{q} #rightarrow D_{s} D_{q}^{(*)} #mu #nu"] = TemplateCombiner( "Bx_DD",       DsMuNuChannel.TemplateContainer().at("Bu_DD"), DsMuNuChannel.TemplateContainer().at("Bd_DD"), DsMuNuChannel.TemplateContainer().at("Bs_DD") );
      DsMuNuChannel.CombinedTemplates()["FakeMu"]                                      = TemplateCombiner( "FakeMu",      DsMuNuChannel.TemplateContainer().at("FakeMu") );
      DsMuNuChannel.CombinedTemplates()["Same Sign"]                                   = TemplateCombiner( "SameSign",    DsMuNuChannel.TemplateContainer().at("SameSign") );


      Fitter DsMuNuFitter;
      DsMuNuFitter.AddChannel(DsMuNuChannel);


      DsMuNuFitter.Fit("Ds_Yield");

      if (toy == -1)
      {

        PrintBeforeAfter(DsMuNuChannel.TemplateContainer());

        PrintFitPlot(DsMuNuChannel.DataTemplates().at(0), DsMuNuChannel.CombinedTemplates(), true, 0, "FitPlot.pdf", ";m_{Corr}( B_{s} ) [MeV];");

        PrintFitPlot(DsMuNuChannel.DataTemplates().at(1), DsMuNuChannel.CombinedTemplates(), true, 1, "FitPlot_1.pdf", ";m(D_{s}*) - m(D_{s}) [MeV];",   true, false );
        PrintFitPlot(DsMuNuChannel.DataTemplates().at(2), DsMuNuChannel.CombinedTemplates(), true, 2, "FitPlot_2.pdf", ";B_{s} IP #chi^{2};",            true, true, 0.6, 0.6 );
        PrintFitPlot(DsMuNuChannel.DataTemplates().at(3), DsMuNuChannel.CombinedTemplates(), true, 3, "FitPlot_3.pdf", ";D_{s} IP #chi^{2};",            true, true, 0.6, 0.6 );
        PrintFitPlot(DsMuNuChannel.DataTemplates().at(4), DsMuNuChannel.CombinedTemplates(), true, 4, "FitPlot_4.pdf", ";B_{s} DIRA;",                   true, true );
        PrintFitPlot(DsMuNuChannel.DataTemplates().at(5), DsMuNuChannel.CombinedTemplates(), true, 5, "FitPlot_5.pdf", ";B_{s} #eta;",                   true, false, 0.6, 0.6 );
        PrintFitPlot(DsMuNuChannel.DataTemplates().at(6), DsMuNuChannel.CombinedTemplates(), true, 6, "FitPlot_6.pdf", ";p_{T} B_{s} [MeV]",             true, false, 0.6, 0.6 );
        PrintFitPlot(DsMuNuChannel.DataTemplates().at(7), DsMuNuChannel.CombinedTemplates(), true, 7, "FitPlot_7.pdf", ";m_{inv} ( D_{s} + #mu ) [MeV]", true, false, 0.6, 0.6 );
        PrintFitPlot(DsMuNuChannel.DataTemplates().at(8), DsMuNuChannel.CombinedTemplates(), true, 8, "FitPlot_8.pdf", ";B_{s} Decay Vertex #chi^{2}",   true, true, 0.6, 0.6 );
        PrintFitPlot(DsMuNuChannel.DataTemplates().at(9), DsMuNuChannel.CombinedTemplates(), true, 9, "FitPlot_9.pdf", ";p_{T} ( #gamma ) from cone isolation",   true, false, 0.6, 0.6 );
      }

      auto results = DsMuNuChannel.GetResults();

      results["corr_Ds_DsStar"]  = DsMuNuFitter.GetFitResults()->correlation("Ds_Yield", "DsStar_Yield");
      results["corr_Ds_DsStarX"] = DsMuNuFitter.GetFitResults()->correlation("Ds_Yield", "DsStarN_Yield");
      results["corr_Ds_DsTau"]   = DsMuNuFitter.GetFitResults()->correlation("Ds_Yield", "DsXTauNu_Yield");
      results["corr_Ds_BxDD"]    = DsMuNuFitter.GetFitResults()->correlation("Ds_Yield", "BX_DD_Yield");


      if ( toy == -1 )
      {
        std::ofstream OutputFile;
        OutputFile.open ("DsMuNuYields.txt");

        OutputFile << DsMuNuChannel.CombinedTemplates()["B_{s} #rightarrow D_{s} #mu #nu"].GetYield();
        OutputFile <<"  ";
        OutputFile << DsMuNuChannel.CombinedTemplates()["B_{s} #rightarrow D_{s} #mu #nu"].GetError();

        OutputFile.close();



        DsMuNuChannel.BuildPseudoData(Arguments.m_nToys);
        PseudoData      = DsMuNuChannel.GetPseudoData();
        PseudoDataYields= DsMuNuChannel.GetPseudoDataYieds();


      }


      if ( toy >= 0 )
      {
          for( auto& Input:PseudoDataYields.at(toy))
          {
              string name = Input.first;
              double InputYield = Input.second;
              double FitYield   = results[name];
              double FitErr     = results[name+"_err"];

              results[name + "_In"]   = InputYield;
              results[name + "_res"]  = FitYield - InputYield;
              results[name + "_pull"] = (FitYield - InputYield)/FitErr;






          }
      }

      if ( toy == 0 )
      {
          SaveToys.init(results);


      }

      if ( toy >= 0 )
      {
         SaveToys.AddEntry(results);
      }

  }

  SaveToys.Close();

  return EXIT_SUCCESS;
    //return results;
}

