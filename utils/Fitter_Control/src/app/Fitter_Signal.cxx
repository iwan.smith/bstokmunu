/*
 * Newfitter2.cxx
 *
 *  Created on: 17 Aug 2017
 *      Author: ismith
 */

// header including everything
#include "FitterHeader.h"
#include "RooFitHeader.h"

#include "FitterFunctions.h"
#include "FitterObjects.h"

#include "HistogrammerTools.h"

#include "Fitter.h"
#include "TString.h"

#include "GeneratorEfficiencies.h"

#include "KMuNuTools.h"


double GetBDTEfficiency(vector<shared_ptr<TH1F>> Histograms, bool ScaleByBu = false, string SaveBFit = "")
{
  if ( ScaleByBu )
    {

      double Measured_B_Yield = 0.0;
      std::tie(Measured_B_Yield, std::ignore) = FitJpsiK(Histograms[4].get(), SaveBFit);
      //double Actual_B_Yield   = Histograms[2]->Integral();

      return Histograms[1]->Integral() / Measured_B_Yield;

    }


  return Histograms[1]->Integral() / Histograms[2]->Integral();
}

double GetEfficiency(shared_ptr<TH1F> Histogram, std::string EventTupleLoc, TFile* f)
{
  double YieldAtProduction = 1.00;


  TObject* EventTuple = f->Get(("EventTuples/" + EventTupleLoc).c_str());
  if ( EventTuple)
    YieldAtProduction = std::stod( EventTuple->GetTitle());
  double YieldInHistogram  = Histogram->Integral();


  return YieldInHistogram/YieldAtProduction;
}



vector<shared_ptr<TH1F>> LoadHistograms(std::string Histname, TFile* f_hist, std::string Cut, std::string Systematic = "", bool isSmeared = false)
{

  cout << Cut << endl;

  string NoBDT = Cut.find("NoBDT") < Cut.size() ? "" : "_NoBDT";

  string EventType  ="";

  map<string,string> NameToEventType{
    {"KMuNu",        "MC_13512010"},
    {"KstMuNu",      "MC_13512400"},
    {"K2stMuNu",     "MC_13512410"},
    {"Kst1430MuNu",  "MC_13512420"},
    {"Bu_D0pi",      "MC_12573050"},
    {"Bs_DsMuNu",    "MC_13774000"},
    {"Bd_Dpi",       "MC_11574051"},
    {"Lb_pMuNu_LCSR","MC_15512013"},
    {"Lb_pMuNu_LQCD","MC_15512014"},
    {"Bs_JpsiPhi",   "MC_13144001"},
    {"Bu_JpsiKst",   "MC_12143401"},
    {"Bu_JpsiK",     "MC_12143001"},
    {"Bd_JpsiKst",   "MC_11144001"},
    {"Bu_D0MuNu",    "MC_12873002"},
    {"Bu_rhoMuNu",   "MC_12513001"},
    {"Bd_DMuNu",     "MC_11874042"},
    {"Bd_rhomunu",   "MC_11512400"},
    {"Bd_pimunu",    "MC_11512011"}
  };

 if ( NameToEventType.count(Histname) )
   EventType = NameToEventType[Histname];
 else
   EventType = Histname;





  vector<shared_ptr<TH1F>> Histograms;

  /*
   * Loader for Data
   */


  if ( EventType == "Data")
  {
    bool smooth = false;
    bool ClearUnderFlow = true;

    Histograms = {
        GetHist(f_hist, "Nominal/KMuNu/Data/" + Cut + "/TMVA_charge_BDT",       false, smooth, ClearUnderFlow, isSmeared ),
        GetHist(f_hist, "Nominal/KMuNu/Data/" + Cut + "/h_Bs_MCORR",       false, smooth, ClearUnderFlow, isSmeared ),
        GetHist(f_hist, "Nominal/KMuNu/Data/" + Cut + NoBDT +"/h_Bs_MCORR", false, smooth, ClearUnderFlow, isSmeared ),
        GetHist(f_hist, "Nominal/KMuNu/Data/" + Cut + "/h_Bu_MM",          false, smooth, ClearUnderFlow, isSmeared ),
        GetHist(f_hist, "Nominal/KMuNu/Data/" + Cut + NoBDT +"/h_Bu_MM",    false, smooth, ClearUnderFlow, isSmeared ),
        GetHist(f_hist, "Nominal/KMuNu/Data/" + Cut + "/MissingMass",      false, smooth, ClearUnderFlow, isSmeared ),
        GetHist(f_hist, "Nominal/KMuNu/Data/" + Cut + "/h_Bs_PT",      false, smooth, ClearUnderFlow, isSmeared ),
        GetHist(f_hist, "Nominal/KMuNu/Data/" + Cut + "/TMVA_charge_BDT",      false, smooth, ClearUnderFlow, isSmeared ),
        GetHist(f_hist, "Nominal/KMuNu/Data/" + Cut + "/TMVA_SS_aftercut_BDT",      false, smooth, ClearUnderFlow, isSmeared ),
        GetHist(f_hist, "Nominal/KMuNu/Data/" + Cut + "/TMVA_CCharged_BDT",      false, smooth, ClearUnderFlow, isSmeared ),
/*
        GetHist(f_hist, "Nominal/KMuNu/Data/" + Cut + "/BDT_NC_V00",   false, smooth, ClearUnderFlow, isSmeared ),
        GetHist(f_hist, "Nominal/KMuNu/Data/" + Cut + "/BDT_NC_V01",   false, smooth, ClearUnderFlow, isSmeared ),
        GetHist(f_hist, "Nominal/KMuNu/Data/" + Cut + "/BDT_NC_V02",   false, smooth, ClearUnderFlow, isSmeared ),
        GetHist(f_hist, "Nominal/KMuNu/Data/" + Cut + "/BDT_NC_V03",   false, smooth, ClearUnderFlow, isSmeared ),
        GetHist(f_hist, "Nominal/KMuNu/Data/" + Cut + "/BDT_NC_V04",   false, smooth, ClearUnderFlow, isSmeared ),
        GetHist(f_hist, "Nominal/KMuNu/Data/" + Cut + "/BDT_NC_V05",   false, smooth, ClearUnderFlow, isSmeared ),
        GetHist(f_hist, "Nominal/KMuNu/Data/" + Cut + "/BDT_NC_V06",   false, smooth, ClearUnderFlow, isSmeared ),
        GetHist(f_hist, "Nominal/KMuNu/Data/" + Cut + "/BDT_NC_V07",   false, smooth, ClearUnderFlow, isSmeared ),
        GetHist(f_hist, "Nominal/KMuNu/Data/" + Cut + "/BDT_NC_V08",   false, smooth, ClearUnderFlow, isSmeared ),
        GetHist(f_hist, "Nominal/KMuNu/Data/" + Cut + "/BDT_NC_V09",   false, smooth, ClearUnderFlow, isSmeared ),
        GetHist(f_hist, "Nominal/KMuNu/Data/" + Cut + "/BDT_NC_V10",   false, smooth, ClearUnderFlow, isSmeared ),
        GetHist(f_hist, "Nominal/KMuNu/Data/" + Cut + "/BDT_NC_V11",   false, smooth, ClearUnderFlow, isSmeared ),
        GetHist(f_hist, "Nominal/KMuNu/Data/" + Cut + "/BDT_NC_V12",   false, smooth, ClearUnderFlow, isSmeared ),
        GetHist(f_hist, "Nominal/KMuNu/Data/" + Cut + "/BDT_NC_V13",   false, smooth, ClearUnderFlow, isSmeared ),
        GetHist(f_hist, "Nominal/KMuNu/Data/" + Cut + "/BDT_NC_V14",   false, smooth, ClearUnderFlow, isSmeared ),
        GetHist(f_hist, "Nominal/KMuNu/Data/" + Cut + "/BDT_NC_V15",   false, smooth, ClearUnderFlow, isSmeared ),
        GetHist(f_hist, "Nominal/KMuNu/Data/" + Cut + "/BDT_NC_V16",   false, smooth, ClearUnderFlow, isSmeared ),
        GetHist(f_hist, "Nominal/KMuNu/Data/" + Cut + "/BDT_NC_V17",   false, smooth, ClearUnderFlow, isSmeared ),
        GetHist(f_hist, "Nominal/KMuNu/Data/" + Cut + "/BDT_NC_V18",   false, smooth, ClearUnderFlow, isSmeared ),
        GetHist(f_hist, "Nominal/KMuNu/Data/" + Cut + "/BDT_NC_V19",   false, smooth, ClearUnderFlow, isSmeared ),
        GetHist(f_hist, "Nominal/KMuNu/Data/" + Cut + "/BDT_NC_V20",   false, smooth, ClearUnderFlow, isSmeared ),
        GetHist(f_hist, "Nominal/KMuNu/Data/" + Cut + "/BDT_NC_V21",   false, smooth, ClearUnderFlow, isSmeared ),
        GetHist(f_hist, "Nominal/KMuNu/Data/" + Cut + "/BDT_NC_V22",   false, smooth, ClearUnderFlow, isSmeared ),
        GetHist(f_hist, "Nominal/KMuNu/Data/" + Cut + "/BDT_NC_V23",   false, smooth, ClearUnderFlow, isSmeared ),


        GetHist(f_hist, "Nominal/KMuNu/Data/" + Cut + "/BDT_V00",   false, smooth, ClearUnderFlow, isSmeared ),
        GetHist(f_hist, "Nominal/KMuNu/Data/" + Cut + "/BDT_V01",   false, smooth, ClearUnderFlow, isSmeared ),
        GetHist(f_hist, "Nominal/KMuNu/Data/" + Cut + "/BDT_V02",   false, smooth, ClearUnderFlow, isSmeared ),
        GetHist(f_hist, "Nominal/KMuNu/Data/" + Cut + "/BDT_V03",   false, smooth, ClearUnderFlow, isSmeared ),
        GetHist(f_hist, "Nominal/KMuNu/Data/" + Cut + "/BDT_V04",   false, smooth, ClearUnderFlow, isSmeared ),
        GetHist(f_hist, "Nominal/KMuNu/Data/" + Cut + "/BDT_V05",   false, smooth, ClearUnderFlow, isSmeared ),
        GetHist(f_hist, "Nominal/KMuNu/Data/" + Cut + "/BDT_V06",   false, smooth, ClearUnderFlow, isSmeared ),
        GetHist(f_hist, "Nominal/KMuNu/Data/" + Cut + "/BDT_V07",   false, smooth, ClearUnderFlow, isSmeared ),
        GetHist(f_hist, "Nominal/KMuNu/Data/" + Cut + "/BDT_V08",   false, smooth, ClearUnderFlow, isSmeared ),
        GetHist(f_hist, "Nominal/KMuNu/Data/" + Cut + "/BDT_V09",   false, smooth, ClearUnderFlow, isSmeared ),
        GetHist(f_hist, "Nominal/KMuNu/Data/" + Cut + "/BDT_V10",   false, smooth, ClearUnderFlow, isSmeared ),
        GetHist(f_hist, "Nominal/KMuNu/Data/" + Cut + "/BDT_V11",   false, smooth, ClearUnderFlow, isSmeared ),
        GetHist(f_hist, "Nominal/KMuNu/Data/" + Cut + "/BDT_V12",   false, smooth, ClearUnderFlow, isSmeared ),
        GetHist(f_hist, "Nominal/KMuNu/Data/" + Cut + "/BDT_V13",   false, smooth, ClearUnderFlow, isSmeared ),
        GetHist(f_hist, "Nominal/KMuNu/Data/" + Cut + "/BDT_V14",   false, smooth, ClearUnderFlow, isSmeared ),
        GetHist(f_hist, "Nominal/KMuNu/Data/" + Cut + "/BDT_V15",   false, smooth, ClearUnderFlow, isSmeared ),
        GetHist(f_hist, "Nominal/KMuNu/Data/" + Cut + "/BDT_V16",   false, smooth, ClearUnderFlow, isSmeared ),
        GetHist(f_hist, "Nominal/KMuNu/Data/" + Cut + "/BDT_V17",   false, smooth, ClearUnderFlow, isSmeared ),
        GetHist(f_hist, "Nominal/KMuNu/Data/" + Cut + "/BDT_V18",   false, smooth, ClearUnderFlow, isSmeared ),
*/
    };
  }

  else if ( EventType == "SameSign")
  {
    bool smooth = false;
    bool ClearUnderFlow = true;

    Histograms = {
        GetHist(f_hist, "Nominal/KMuNu/SameSign_PhysSubtr/" + Cut + "/TMVA_charge_BDT",       false, smooth, ClearUnderFlow, isSmeared ),
        GetHist(f_hist, "Nominal/KMuNu/SameSign_PhysSubtr/" + Cut + "/h_Bs_MCORR",       false, smooth, ClearUnderFlow, isSmeared ),
        GetHist(f_hist, "Nominal/KMuNu/SameSign_PhysSubtr/" + Cut + NoBDT +"/h_Bs_MCORR", false, smooth, ClearUnderFlow, isSmeared ),
        GetHist(f_hist, "Nominal/KMuNu/SameSign_PhysSubtr/" + Cut + "/h_Bu_MM",          false, smooth, ClearUnderFlow, isSmeared ),
        GetHist(f_hist, "Nominal/KMuNu/SameSign_PhysSubtr/" + Cut + NoBDT +"/h_Bu_MM",    false, smooth, ClearUnderFlow, isSmeared ),
        GetHist(f_hist, "Nominal/KMuNu/SameSign_PhysSubtr/" + Cut + "/MissingMass",      false, smooth, ClearUnderFlow, isSmeared ),
        GetHist(f_hist, "Nominal/KMuNu/SameSign_PhysSubtr/" + Cut + "/h_Bs_PT",      false, smooth, ClearUnderFlow, isSmeared ),
        GetHist(f_hist, "Nominal/KMuNu/SameSign_PhysSubtr/" + Cut + "/TMVA_charge_BDT",      false, smooth, ClearUnderFlow, isSmeared ),
        GetHist(f_hist, "Nominal/KMuNu/SameSign_PhysSubtr/" + Cut + "/TMVA_SS_aftercut_BDT",      false, smooth, ClearUnderFlow, isSmeared ),
        GetHist(f_hist, "Nominal/KMuNu/SameSign_PhysSubtr/" + Cut + "/TMVA_CCharged_BDT",      false, smooth, ClearUnderFlow, isSmeared ),
/*
        GetHist(f_hist, "Nominal/KMuNu/SameSign_PhysSubtr/" + Cut + "/BDT_NC_V00",   false, smooth, ClearUnderFlow, isSmeared ),
        GetHist(f_hist, "Nominal/KMuNu/SameSign_PhysSubtr/" + Cut + "/BDT_NC_V01",   false, smooth, ClearUnderFlow, isSmeared ),
        GetHist(f_hist, "Nominal/KMuNu/SameSign_PhysSubtr/" + Cut + "/BDT_NC_V02",   false, smooth, ClearUnderFlow, isSmeared ),
        GetHist(f_hist, "Nominal/KMuNu/SameSign_PhysSubtr/" + Cut + "/BDT_NC_V03",   false, smooth, ClearUnderFlow, isSmeared ),
        GetHist(f_hist, "Nominal/KMuNu/SameSign_PhysSubtr/" + Cut + "/BDT_NC_V04",   false, smooth, ClearUnderFlow, isSmeared ),
        GetHist(f_hist, "Nominal/KMuNu/SameSign_PhysSubtr/" + Cut + "/BDT_NC_V05",   false, smooth, ClearUnderFlow, isSmeared ),
        GetHist(f_hist, "Nominal/KMuNu/SameSign_PhysSubtr/" + Cut + "/BDT_NC_V06",   false, smooth, ClearUnderFlow, isSmeared ),
        GetHist(f_hist, "Nominal/KMuNu/SameSign_PhysSubtr/" + Cut + "/BDT_NC_V07",   false, smooth, ClearUnderFlow, isSmeared ),
        GetHist(f_hist, "Nominal/KMuNu/SameSign_PhysSubtr/" + Cut + "/BDT_NC_V08",   false, smooth, ClearUnderFlow, isSmeared ),
        GetHist(f_hist, "Nominal/KMuNu/SameSign_PhysSubtr/" + Cut + "/BDT_NC_V09",   false, smooth, ClearUnderFlow, isSmeared ),
        GetHist(f_hist, "Nominal/KMuNu/SameSign_PhysSubtr/" + Cut + "/BDT_NC_V10",   false, smooth, ClearUnderFlow, isSmeared ),
        GetHist(f_hist, "Nominal/KMuNu/SameSign_PhysSubtr/" + Cut + "/BDT_NC_V11",   false, smooth, ClearUnderFlow, isSmeared ),
        GetHist(f_hist, "Nominal/KMuNu/SameSign_PhysSubtr/" + Cut + "/BDT_NC_V12",   false, smooth, ClearUnderFlow, isSmeared ),
        GetHist(f_hist, "Nominal/KMuNu/SameSign_PhysSubtr/" + Cut + "/BDT_NC_V13",   false, smooth, ClearUnderFlow, isSmeared ),
        GetHist(f_hist, "Nominal/KMuNu/SameSign_PhysSubtr/" + Cut + "/BDT_NC_V14",   false, smooth, ClearUnderFlow, isSmeared ),
        GetHist(f_hist, "Nominal/KMuNu/SameSign_PhysSubtr/" + Cut + "/BDT_NC_V15",   false, smooth, ClearUnderFlow, isSmeared ),
        GetHist(f_hist, "Nominal/KMuNu/SameSign_PhysSubtr/" + Cut + "/BDT_NC_V16",   false, smooth, ClearUnderFlow, isSmeared ),
        GetHist(f_hist, "Nominal/KMuNu/SameSign_PhysSubtr/" + Cut + "/BDT_NC_V17",   false, smooth, ClearUnderFlow, isSmeared ),
        GetHist(f_hist, "Nominal/KMuNu/SameSign_PhysSubtr/" + Cut + "/BDT_NC_V18",   false, smooth, ClearUnderFlow, isSmeared ),
        GetHist(f_hist, "Nominal/KMuNu/SameSign_PhysSubtr/" + Cut + "/BDT_NC_V19",   false, smooth, ClearUnderFlow, isSmeared ),
        GetHist(f_hist, "Nominal/KMuNu/SameSign_PhysSubtr/" + Cut + "/BDT_NC_V20",   false, smooth, ClearUnderFlow, isSmeared ),
        GetHist(f_hist, "Nominal/KMuNu/SameSign_PhysSubtr/" + Cut + "/BDT_NC_V21",   false, smooth, ClearUnderFlow, isSmeared ),
        GetHist(f_hist, "Nominal/KMuNu/SameSign_PhysSubtr/" + Cut + "/BDT_NC_V22",   false, smooth, ClearUnderFlow, isSmeared ),
        GetHist(f_hist, "Nominal/KMuNu/SameSign_PhysSubtr/" + Cut + "/BDT_NC_V23",   false, smooth, ClearUnderFlow, isSmeared ),


        GetHist(f_hist, "Nominal/KMuNu/SameSign_PhysSubtr/" + Cut + "/BDT_V00",   false, smooth, ClearUnderFlow, isSmeared ),
        GetHist(f_hist, "Nominal/KMuNu/SameSign_PhysSubtr/" + Cut + "/BDT_V01",   false, smooth, ClearUnderFlow, isSmeared ),
        GetHist(f_hist, "Nominal/KMuNu/SameSign_PhysSubtr/" + Cut + "/BDT_V02",   false, smooth, ClearUnderFlow, isSmeared ),
        GetHist(f_hist, "Nominal/KMuNu/SameSign_PhysSubtr/" + Cut + "/BDT_V03",   false, smooth, ClearUnderFlow, isSmeared ),
        GetHist(f_hist, "Nominal/KMuNu/SameSign_PhysSubtr/" + Cut + "/BDT_V04",   false, smooth, ClearUnderFlow, isSmeared ),
        GetHist(f_hist, "Nominal/KMuNu/SameSign_PhysSubtr/" + Cut + "/BDT_V05",   false, smooth, ClearUnderFlow, isSmeared ),
        GetHist(f_hist, "Nominal/KMuNu/SameSign_PhysSubtr/" + Cut + "/BDT_V06",   false, smooth, ClearUnderFlow, isSmeared ),
        GetHist(f_hist, "Nominal/KMuNu/SameSign_PhysSubtr/" + Cut + "/BDT_V07",   false, smooth, ClearUnderFlow, isSmeared ),
        GetHist(f_hist, "Nominal/KMuNu/SameSign_PhysSubtr/" + Cut + "/BDT_V08",   false, smooth, ClearUnderFlow, isSmeared ),
        GetHist(f_hist, "Nominal/KMuNu/SameSign_PhysSubtr/" + Cut + "/BDT_V09",   false, smooth, ClearUnderFlow, isSmeared ),
        GetHist(f_hist, "Nominal/KMuNu/SameSign_PhysSubtr/" + Cut + "/BDT_V10",   false, smooth, ClearUnderFlow, isSmeared ),
        GetHist(f_hist, "Nominal/KMuNu/SameSign_PhysSubtr/" + Cut + "/BDT_V11",   false, smooth, ClearUnderFlow, isSmeared ),
        GetHist(f_hist, "Nominal/KMuNu/SameSign_PhysSubtr/" + Cut + "/BDT_V12",   false, smooth, ClearUnderFlow, isSmeared ),
        GetHist(f_hist, "Nominal/KMuNu/SameSign_PhysSubtr/" + Cut + "/BDT_V13",   false, smooth, ClearUnderFlow, isSmeared ),
        GetHist(f_hist, "Nominal/KMuNu/SameSign_PhysSubtr/" + Cut + "/BDT_V14",   false, smooth, ClearUnderFlow, isSmeared ),
        GetHist(f_hist, "Nominal/KMuNu/SameSign_PhysSubtr/" + Cut + "/BDT_V15",   false, smooth, ClearUnderFlow, isSmeared ),
        GetHist(f_hist, "Nominal/KMuNu/SameSign_PhysSubtr/" + Cut + "/BDT_V16",   false, smooth, ClearUnderFlow, isSmeared ),
        GetHist(f_hist, "Nominal/KMuNu/SameSign_PhysSubtr/" + Cut + "/BDT_V17",   false, smooth, ClearUnderFlow, isSmeared ),
        GetHist(f_hist, "Nominal/KMuNu/SameSign_PhysSubtr/" + Cut + "/BDT_V18",   false, smooth, ClearUnderFlow, isSmeared ),
*/
    };
  }

  /*
   * Loader for K Mu Nu Modes
   */


  else
  {

      bool normalise = true;
      bool smooth = false;
      bool ClearUnderFlow = true;

      Histograms = {
          GetHist(f_hist, Systematic + "/KMuNu/"+EventType+"/" + Cut + "/TMVA_charge_BDT",       normalise, smooth, ClearUnderFlow, isSmeared ),
          GetHist(f_hist, Systematic + "/KMuNu/"+EventType+"/" + Cut + "/h_Bs_MCORR",       false,     smooth, ClearUnderFlow, isSmeared ),
          GetHist(f_hist, Systematic + "/KMuNu/"+EventType+"/" + Cut + NoBDT +"/h_Bs_MCORR", normalise, smooth, ClearUnderFlow, isSmeared ),
          GetHist(f_hist, Systematic + "/KMuNu/"+EventType+"/" + Cut + "/h_Bu_MM",          false,     smooth, ClearUnderFlow, isSmeared ),
          GetHist(f_hist, Systematic + "/KMuNu/"+EventType+"/" + Cut + NoBDT +"/h_Bu_MM",    false,     smooth, ClearUnderFlow, isSmeared ),
          GetHist(f_hist, Systematic + "/KMuNu/"+EventType+"/" + Cut + "/MissingMass",      true,      smooth, ClearUnderFlow, isSmeared ),
          GetHist(f_hist, Systematic + "/KMuNu/"+EventType+"/" + Cut + "/h_Bs_PT",      true,      smooth, ClearUnderFlow, isSmeared ),
          GetHist(f_hist, Systematic + "/KMuNu/"+EventType+"/" + Cut + "/TMVA_charge_BDT",      true,      smooth, ClearUnderFlow, isSmeared ),
          GetHist(f_hist, Systematic + "/KMuNu/"+EventType+"/" + Cut + "/TMVA_SS_aftercut_BDT",      true,      smooth, ClearUnderFlow, isSmeared ),
          GetHist(f_hist, Systematic + "/KMuNu/"+EventType+"/" + Cut + "/TMVA_CCharged_BDT",      true,      smooth, ClearUnderFlow, isSmeared ),
/*
          GetHist(f_hist, Systematic + "/KMuNu/"+EventType+"/" + Cut + "/BDT_NC_V00",   true, smooth, ClearUnderFlow, isSmeared ),
          GetHist(f_hist, Systematic + "/KMuNu/"+EventType+"/" + Cut + "/BDT_NC_V01",   true, smooth, ClearUnderFlow, isSmeared ),
          GetHist(f_hist, Systematic + "/KMuNu/"+EventType+"/" + Cut + "/BDT_NC_V02",   true, smooth, ClearUnderFlow, isSmeared ),
          GetHist(f_hist, Systematic + "/KMuNu/"+EventType+"/" + Cut + "/BDT_NC_V03",   true, smooth, ClearUnderFlow, isSmeared ),
          GetHist(f_hist, Systematic + "/KMuNu/"+EventType+"/" + Cut + "/BDT_NC_V04",   true, smooth, ClearUnderFlow, isSmeared ),
          GetHist(f_hist, Systematic + "/KMuNu/"+EventType+"/" + Cut + "/BDT_NC_V05",   true, smooth, ClearUnderFlow, isSmeared ),
          GetHist(f_hist, Systematic + "/KMuNu/"+EventType+"/" + Cut + "/BDT_NC_V06",   true, smooth, ClearUnderFlow, isSmeared ),
          GetHist(f_hist, Systematic + "/KMuNu/"+EventType+"/" + Cut + "/BDT_NC_V07",   true, smooth, ClearUnderFlow, isSmeared ),
          GetHist(f_hist, Systematic + "/KMuNu/"+EventType+"/" + Cut + "/BDT_NC_V08",   true, smooth, ClearUnderFlow, isSmeared ),
          GetHist(f_hist, Systematic + "/KMuNu/"+EventType+"/" + Cut + "/BDT_NC_V09",   true, smooth, ClearUnderFlow, isSmeared ),
          GetHist(f_hist, Systematic + "/KMuNu/"+EventType+"/" + Cut + "/BDT_NC_V10",   true, smooth, ClearUnderFlow, isSmeared ),
          GetHist(f_hist, Systematic + "/KMuNu/"+EventType+"/" + Cut + "/BDT_NC_V11",   true, smooth, ClearUnderFlow, isSmeared ),
          GetHist(f_hist, Systematic + "/KMuNu/"+EventType+"/" + Cut + "/BDT_NC_V12",   true, smooth, ClearUnderFlow, isSmeared ),
          GetHist(f_hist, Systematic + "/KMuNu/"+EventType+"/" + Cut + "/BDT_NC_V13",   true, smooth, ClearUnderFlow, isSmeared ),
          GetHist(f_hist, Systematic + "/KMuNu/"+EventType+"/" + Cut + "/BDT_NC_V14",   true, smooth, ClearUnderFlow, isSmeared ),
          GetHist(f_hist, Systematic + "/KMuNu/"+EventType+"/" + Cut + "/BDT_NC_V15",   true, smooth, ClearUnderFlow, isSmeared ),
          GetHist(f_hist, Systematic + "/KMuNu/"+EventType+"/" + Cut + "/BDT_NC_V16",   true, smooth, ClearUnderFlow, isSmeared ),
          GetHist(f_hist, Systematic + "/KMuNu/"+EventType+"/" + Cut + "/BDT_NC_V17",   true, smooth, ClearUnderFlow, isSmeared ),
          GetHist(f_hist, Systematic + "/KMuNu/"+EventType+"/" + Cut + "/BDT_NC_V18",   true, smooth, ClearUnderFlow, isSmeared ),
          GetHist(f_hist, Systematic + "/KMuNu/"+EventType+"/" + Cut + "/BDT_NC_V19",   true, smooth, ClearUnderFlow, isSmeared ),
          GetHist(f_hist, Systematic + "/KMuNu/"+EventType+"/" + Cut + "/BDT_NC_V20",   true, smooth, ClearUnderFlow, isSmeared ),
          GetHist(f_hist, Systematic + "/KMuNu/"+EventType+"/" + Cut + "/BDT_NC_V21",   true, smooth, ClearUnderFlow, isSmeared ),
          GetHist(f_hist, Systematic + "/KMuNu/"+EventType+"/" + Cut + "/BDT_NC_V22",   true, smooth, ClearUnderFlow, isSmeared ),
          GetHist(f_hist, Systematic + "/KMuNu/"+EventType+"/" + Cut + "/BDT_NC_V23",   true, smooth, ClearUnderFlow, isSmeared ),

          GetHist(f_hist, Systematic + "/KMuNu/"+EventType+"/" + Cut + "/BDT_V00",   true, smooth, ClearUnderFlow, isSmeared ),
          GetHist(f_hist, Systematic + "/KMuNu/"+EventType+"/" + Cut + "/BDT_V01",   true, smooth, ClearUnderFlow, isSmeared ),
          GetHist(f_hist, Systematic + "/KMuNu/"+EventType+"/" + Cut + "/BDT_V02",   true, smooth, ClearUnderFlow, isSmeared ),
          GetHist(f_hist, Systematic + "/KMuNu/"+EventType+"/" + Cut + "/BDT_V03",   true, smooth, ClearUnderFlow, isSmeared ),
          GetHist(f_hist, Systematic + "/KMuNu/"+EventType+"/" + Cut + "/BDT_V04",   true, smooth, ClearUnderFlow, isSmeared ),
          GetHist(f_hist, Systematic + "/KMuNu/"+EventType+"/" + Cut + "/BDT_V05",   true, smooth, ClearUnderFlow, isSmeared ),
          GetHist(f_hist, Systematic + "/KMuNu/"+EventType+"/" + Cut + "/BDT_V06",   true, smooth, ClearUnderFlow, isSmeared ),
          GetHist(f_hist, Systematic + "/KMuNu/"+EventType+"/" + Cut + "/BDT_V07",   true, smooth, ClearUnderFlow, isSmeared ),
          GetHist(f_hist, Systematic + "/KMuNu/"+EventType+"/" + Cut + "/BDT_V08",   true, smooth, ClearUnderFlow, isSmeared ),
          GetHist(f_hist, Systematic + "/KMuNu/"+EventType+"/" + Cut + "/BDT_V09",   true, smooth, ClearUnderFlow, isSmeared ),
          GetHist(f_hist, Systematic + "/KMuNu/"+EventType+"/" + Cut + "/BDT_V10",   true, smooth, ClearUnderFlow, isSmeared ),
          GetHist(f_hist, Systematic + "/KMuNu/"+EventType+"/" + Cut + "/BDT_V11",   true, smooth, ClearUnderFlow, isSmeared ),
          GetHist(f_hist, Systematic + "/KMuNu/"+EventType+"/" + Cut + "/BDT_V12",   true, smooth, ClearUnderFlow, isSmeared ),
          GetHist(f_hist, Systematic + "/KMuNu/"+EventType+"/" + Cut + "/BDT_V13",   true, smooth, ClearUnderFlow, isSmeared ),
          GetHist(f_hist, Systematic + "/KMuNu/"+EventType+"/" + Cut + "/BDT_V14",   true, smooth, ClearUnderFlow, isSmeared ),
          GetHist(f_hist, Systematic + "/KMuNu/"+EventType+"/" + Cut + "/BDT_V15",   true, smooth, ClearUnderFlow, isSmeared ),
          GetHist(f_hist, Systematic + "/KMuNu/"+EventType+"/" + Cut + "/BDT_V16",   true, smooth, ClearUnderFlow, isSmeared ),
          GetHist(f_hist, Systematic + "/KMuNu/"+EventType+"/" + Cut + "/BDT_V17",   true, smooth, ClearUnderFlow, isSmeared ),
          GetHist(f_hist, Systematic + "/KMuNu/"+EventType+"/" + Cut + "/BDT_V18",   true, smooth, ClearUnderFlow, isSmeared ),
*/
      };


    cout << "Dta Type: " << Histname << " Not found. Searching for it!" << endl;
  }

  return Histograms;
}


int main()
{



  string Systematic = "Default";

  vector<vector<shared_ptr<TH1F>>> PseudoData      ;//= KMuNuChannel.GetPseudoData();
  vector<map<string,double>>       PseudoDataYields;//= KMuNuChannel.GetPseudoDataYieds();

  TreeBuilder SaveToys("FitToys_Signal.root", "FitToys");


  const int nToys = 0;

  for(int toy = -1; toy < nToys; toy++)
  {

    bool SmearMC = toy>=0;

    TFile* f_hist = TFile::Open(
            "../Histogrammer/Histograms.root",
            //"/eos/lhcb/wg/semileptonic/Bs2KmunuAna/Histogrammer/Output/Histograms.root",
            "READ");

    TFile* f_hist_SS = TFile::Open(
            "../Histogrammer/Histograms_SS.root",
            //"/eos/lhcb/wg/semileptonic/Bs2KmunuAna/Histogrammer/Output/Histograms_SS.root",
            "READ");

    Fitter KMuNuFitter;



    ChannelBuilder KMuNuChannel("Is_Q2_NoBDT");


      const string& Cut = KMuNuChannel.GetName();


      /*
       *
       * Setup Data Templates
       *
       */
      if ( toy == -1 )
        KMuNuChannel.DataTemplates() = LoadHistograms("Data", f_hist, Cut, "");
      else
        KMuNuChannel.DataTemplates() = PseudoData.at(toy);

      KMuNuChannel.TemplateContainer()["SameSign"]     = TemplateObjects(
          "SameSign",
          LoadHistograms("SameSign", f_hist_SS, Cut, Systematic, SmearMC),
          false);


      /*
       *
       * Setup KMuNu Templates
       *
       */

      KMuNuChannel.TemplateContainer()["KMuNu"]     = TemplateObjects(
          "KMuNu",
          LoadHistograms("KMuNu", f_hist, Cut, Systematic, SmearMC),
          false);

      KMuNuChannel.TemplateContainer()["KstMuNu"]     = TemplateObjects(
          "KstMuNu",
          LoadHistograms("KstMuNu", f_hist, Cut, Systematic, SmearMC),
          false);

      KMuNuChannel.TemplateContainer()["K2stMuNu"]     = TemplateObjects(
          "K2stMuNu",
          LoadHistograms("K2stMuNu", f_hist, Cut, Systematic, SmearMC),
          false);

      KMuNuChannel.TemplateContainer()["Kst1430MuNu"]     = TemplateObjects(
          "Kst1430MuNu",
          LoadHistograms("Kst1430MuNu", f_hist, Cut, Systematic, SmearMC),
          false);

      /*
       *
       * Setup Inclusive Templates
       *
       */


      KMuNuChannel.TemplateContainer()["MC_10010037"]     = TemplateObjects(
          "MC_10010037",
          LoadHistograms("MC_10010037", f_hist, Cut, Systematic, SmearMC),
          false);

      /*
       *
       * Setup JpsiX Templates
       *
       */



      KMuNuChannel.TemplateContainer()["Bu_JpsiK"]     = TemplateObjects(
          "Bu_JpsiK",
          LoadHistograms("Bu_JpsiK", f_hist, Cut, Systematic, SmearMC),
          false);

      KMuNuChannel.TemplateContainer()["Bu_JpsiKst"]     = TemplateObjects(
          "Bu_JpsiKst",
          LoadHistograms("Bu_JpsiKst", f_hist, Cut, Systematic, SmearMC),
          false);

      KMuNuChannel.TemplateContainer()["Bs_JpsiPhi"]     = TemplateObjects(
          "Bs_JpsiPhi",
          LoadHistograms("Bs_JpsiPhi", f_hist, Cut, Systematic, SmearMC),
          false);


      /*
       *
       * Setup Charm Templates
       *
       */

      KMuNuChannel.TemplateContainer()["Bs_DsMuNu"]     = TemplateObjects(
          "Bs_DsMuNu",
          LoadHistograms("Bs_DsMuNu", f_hist, Cut, Systematic, SmearMC),
          false);


      KMuNuChannel.TemplateContainer()["Bd_DMuNu"]     = TemplateObjects(
          "Bd_DMuNu",
          LoadHistograms("Bd_DMuNu", f_hist, Cut, Systematic, SmearMC),
          false);



      /*
       *
       * Setup Vub Templates
       *
       */

      KMuNuChannel.TemplateContainer()["Bd_rhomunu"]     = TemplateObjects(
          "Bd_rhomunu",
          LoadHistograms("Bd_rhomunu", f_hist, Cut, Systematic, SmearMC),
          false);


      KMuNuChannel.TemplateContainer()["Bd_pimunu"]     = TemplateObjects(
          "Bd_pimunu",
          LoadHistograms("Bd_pimunu", f_hist, Cut, Systematic, SmearMC),
          false);

      KMuNuChannel.TemplateContainer()["Lb_pMuNu"]     = TemplateObjects(
          "Lb_pMuNu",
          LoadHistograms("Lb_pMuNu_LQCD", f_hist, Cut, Systematic, SmearMC),
          false);







    /**********************************************************************************
     *
     * Add Normalisation factors
     *
     **********************************************************************************/


    double n_data   = KMuNuChannel.DataTemplates().at(0)->Integral();


    // Templates with yields floating



    const double K_Eff   = GetEfficiency(KMuNuChannel.at("KMuNu").Histograms[1],   "KMuNu/MC_13512010",f_hist);
    //const double Kst_Eff = GetEfficiency(KMuNuChannel.at("KstMuNu").Histograms[1], "KMuNu/MC_13512400",f_hist);
    KMuNuChannel.at("KMuNu")       .AddNormFactor("K_Yield",           2.11776e+05, 2.11776e+05, 2.11776e+05);

    KMuNuChannel.at("KstMuNu")     .AddNormFactor("Kst_Yield",         n_data*0.1,    0.0,           n_data);


    //KMuNuChannel.at("K2stMuNu")    .AddNormFactor("K2st_Yield",        n_data*0.1,    0.0,           n_data);

    KMuNuChannel.at("Kst1430MuNu") .AddNormFactor("Kst1430_Yield",     n_data*0.1,    0.0,           n_data);

    KMuNuChannel.at("MC_10010037") .AddNormFactor("Inclusive_Yield",   n_data*0.1,    0.0,           n_data);

    KMuNuChannel.at("SameSign")    .AddNormFactor("SS_Yield",           1.0,  0.5,     1.5);

    // Templates constrained to the J/Psi K


    double Bu_JpsiK_Yield;
    std::tie(Bu_JpsiK_Yield, std::ignore) = FitJpsiK(KMuNuChannel.DataTemplates().at(4).get(), "Data_Bu_MM"+Cut+".pdf");
    const double Bu_JpsiK_BDT_Eff   = GetBDTEfficiency(KMuNuChannel.at("Bu_JpsiK").Histograms, true, "Bu_MC_Fit.pdf");
    const double TotalJpsiKYield = Bu_JpsiK_Yield * Bu_JpsiK_BDT_Eff;

    const double JpsiK_Eff = GetEfficiency(KMuNuChannel.at("Bu_JpsiK").Histograms[1], "KMuNu/MC_12143001",f_hist);
    KMuNuChannel.at("Bu_JpsiK")    .AddNormFactor("JPsiK_Yield"+Cut,       n_data*0.1,    TotalJpsiKYield,           n_data);

    const double JpsiKst_Eff = GetEfficiency(KMuNuChannel.at("Bu_JpsiKst").Histograms[1], "KMuNu/MC_12143401",f_hist);
    KMuNuChannel.at("Bu_JpsiKst")  .AddNormFactor("JPsiK_Yield"+Cut,       n_data*0.1,    0.0,           n_data);


    const double JpsiPhi_Eff = GetEfficiency(KMuNuChannel.at("Bs_JpsiPhi").Histograms[1], "KMuNu/MC_13144001", f_hist);
    KMuNuChannel.at("Bs_JpsiPhi")  .AddNormFactor("JPsiK_Yield"+Cut,     n_data*0.1,    0.0,           n_data);

    const double DsMuNu_Eff = GetEfficiency(KMuNuChannel.at("Bs_DsMuNu").Histograms[1], "KMuNu/MC_13774000", f_hist);
    KMuNuChannel.at("Bs_DsMuNu")  .AddNormFactor("JPsiK_Yield"+Cut,        n_data*0.1,    0.0,           n_data);


    const double DMuNu_Eff = GetEfficiency(KMuNuChannel.at("Bd_DMuNu").Histograms[1], "KMuNu/MC_11874042", f_hist);
    KMuNuChannel.at("Bd_DMuNu")  .AddNormFactor("JPsiK_Yield"+Cut,       n_data*0.1,    0.0,           n_data);

    const double rhomunu_Eff = GetEfficiency(KMuNuChannel.at("Bd_rhomunu").Histograms[1], "KMuNu/MC_11512400",f_hist);
    KMuNuChannel.at("Bd_rhomunu") .AddNormFactor("K_Yield",      2e5, 2e5, 2e5);



    /*
    const double pimunu_Eff = GetEfficiency(KMuNuChannel.at("Bd_pimunu").Histograms[1], "KMuNu/MC_11512011",f_hist);
    KMuNuChannel.at("Bd_pimunu")  .AddNormFactor("K_Yield",         n_data*0.1,        0.0,              n_data);

    const double pmunu_Eff = GetEfficiency(KMuNuChannel.at("Lb_pMuNu").Histograms[1], "KMuNu/MC_15512013",f_hist);
    KMuNuChannel.at("Lb_pMuNu")   .AddNormFactor("K_Yield",      n_data*0.1,    0.0,           n_data);

*/
    /**********************************************************************************
     *
     * Setup for plotting
     *
     **********************************************************************************/

    KMuNuChannel.CombinedTemplates()["B_{s} #rightarrow K^{-} #mu #nu"]             = TemplateCombiner( "KMuNu",      KMuNuChannel.at("KMuNu") );
    KMuNuChannel.CombinedTemplates()["B_{s} #rightarrow K^{-}* #mu #nu"]            = TemplateCombiner( "KstMuNu",    KMuNuChannel.at("KstMuNu") );
    KMuNuChannel.CombinedTemplates()["B_{s} #rightarrow K^{-2}* #mu #nu"]           = TemplateCombiner( "K2stMuNu",   KMuNuChannel.at("K2stMuNu") );
    KMuNuChannel.CombinedTemplates()["B_{s} #rightarrow K^{-}*(1430) #mu #nu"]      = TemplateCombiner( "Kst1430MuNu",KMuNuChannel.at("Kst1430MuNu") );


    KMuNuChannel.CombinedTemplates()["Inclusive K + #mu"]                           = TemplateCombiner( "Inclusive",  KMuNuChannel.at("MC_10010037") );

    KMuNuChannel.CombinedTemplates()["B^{+} #rightarrow J/#psi K^{+}"]              = TemplateCombiner( "JpsiK",      KMuNuChannel.at("Bu_JpsiK") );
    KMuNuChannel.CombinedTemplates()["B^{+} #rightarrow J/#psi K^{+}*"]             = TemplateCombiner( "JpsiKst",    KMuNuChannel.at("Bu_JpsiKst") );
    KMuNuChannel.CombinedTemplates()["B^{+} #rightarrow J/#psi #phi"]               = TemplateCombiner( "Bs_JpsiPhi", KMuNuChannel.at("Bs_JpsiPhi") );

    KMuNuChannel.CombinedTemplates()["B_{s} #rightarrow D_{s} #mu #nu"]             = TemplateCombiner( "Bs_DsMuNu",  KMuNuChannel.at("Bs_DsMuNu") );
    KMuNuChannel.CombinedTemplates()["B^{0} #rightarrow D^{+} #mu #nu"]             = TemplateCombiner( "B0_DpMuNu",  KMuNuChannel.at("Bd_DMuNu") );
    KMuNuChannel.CombinedTemplates()["Same Sign"]                                   = TemplateCombiner( "SameSign",   KMuNuChannel.at("SameSign") );
    KMuNuChannel.CombinedTemplates()["Vub inclusive"]                               = TemplateCombiner( "SameSign",   KMuNuChannel.at("Bd_rhomunu"),   KMuNuChannel.at("Bd_pimunu"),   KMuNuChannel.at("Lb_pMuNu") );
    //KMuNuChannel.CombinedTemplates()["B^{0} #rightarrow #rho #mu #nu"]              = TemplateCombiner( "B0_rhoMuNu", KMuNuChannel.at("Bd_rhomunu") );
    //KMuNuChannel.CombinedTemplates()["B^{0} #rightarrow #pi #mu #nu"]               = TemplateCombiner( "B0_piMuNu",  KMuNuChannel.at("Bd_pimunu") );
    //KMuNuChannel.CombinedTemplates()["#Lambda_{b} #rightarrow p #mu #nu"]           = TemplateCombiner( "Bs_DsMuNu",  KMuNuChannel.at("Lb_pMuNu") );


    //cout << TotalJpsiKYield << endl;
    KMuNuFitter.AddChannel(KMuNuChannel);



    KMuNuFitter.Fit("K_Yield");
    //exit(0);
    if (toy == -1)
    {

      PrintBeforeAfter(KMuNuChannel.TemplateContainer());

      PrintFitPlot(KMuNuChannel.DataTemplates().at(0), KMuNuChannel.CombinedTemplates(), true, 0, KMuNuChannel.GetName()+"FitPlot.pdf", ";m_{Corr}( B_{s} ) [MeV];", false, false, 0.11, 0.59);
      PrintFitPlot(KMuNuChannel.DataTemplates().at(4), KMuNuChannel.CombinedTemplates(), true, 3, KMuNuChannel.GetName()+"FitPlot_4.pdf", ";m_{Corr}( B_{s} ) [MeV];", false, false, 0.11, 0.59);
      PrintFitPlot(KMuNuChannel.DataTemplates().at(5), KMuNuChannel.CombinedTemplates(), true, 5, KMuNuChannel.GetName()+"FitPlot_5.pdf", ";Missing Mass^{2} [MeV^{2}];", false, false, 0.6, 0.59);
      PrintFitPlot(KMuNuChannel.DataTemplates().at(6), KMuNuChannel.CombinedTemplates(), true, 6, KMuNuChannel.GetName()+"FitPlot_6.pdf", ";p_{T} B_{s}^{0} [MeV];", false, false, 0.6, 0.59);
      PrintFitPlot(KMuNuChannel.DataTemplates().at(7), KMuNuChannel.CombinedTemplates(), true, 7, KMuNuChannel.GetName()+"FitPlot_7.pdf", ";TMVA_charge_BDT;", false, false, 0.11, 0.59);
      PrintFitPlot(KMuNuChannel.DataTemplates().at(8), KMuNuChannel.CombinedTemplates(), true, 8, KMuNuChannel.GetName()+"FitPlot_8.pdf", ";TMVA_SS_aftercut_BDT;", false, false, 0.11, 0.59);
      PrintFitPlot(KMuNuChannel.DataTemplates().at(9), KMuNuChannel.CombinedTemplates(), true, 9, KMuNuChannel.GetName()+"FitPlot_9.pdf", ";TMVA_CCharged_BDT;", false, false, 0.11, 0.59);
/*
      TCanvas* cx = new TCanvas("cx", "cx", 1600, 1600);
      cx->Print((KMuNuChannel.GetName()+"_Validation_Neutral.pdf[").c_str());
      PrintFitPlot(KMuNuChannel.DataTemplates().at( 9), KMuNuChannel.CombinedTemplates(), true,  9, KMuNuChannel.GetName()+"_Validation_Neutral.pdf", ";min(kaon_m_IsoMinBDT, muon_p_IsoMinBDT) ;", false, false, 0.11, 0.59);
      PrintFitPlot(KMuNuChannel.DataTemplates().at(10), KMuNuChannel.CombinedTemplates(), true, 10, KMuNuChannel.GetName()+"_Validation_Neutral.pdf", ";muon_p_IsoSumBDT                        ;", false, false, 0.11, 0.59);
      PrintFitPlot(KMuNuChannel.DataTemplates().at(11), KMuNuChannel.CombinedTemplates(), true, 11, KMuNuChannel.GetName()+"_Validation_Neutral.pdf", ";kaon_m_PT                               ;", false, false, 0.11, 0.59);
      PrintFitPlot(KMuNuChannel.DataTemplates().at(12), KMuNuChannel.CombinedTemplates(), true, 12, KMuNuChannel.GetName()+"_Validation_Neutral.pdf", ";kaon_m_1.00_cc_asy_P                    ;", false, false, 0.11, 0.59);
      PrintFitPlot(KMuNuChannel.DataTemplates().at(13), KMuNuChannel.CombinedTemplates(), true, 13, KMuNuChannel.GetName()+"_Validation_Neutral.pdf", ";muon_p_1.00_cc_asy_P                    ;", false, false, 0.11, 0.59);
      PrintFitPlot(KMuNuChannel.DataTemplates().at(14), KMuNuChannel.CombinedTemplates(), true, 14, KMuNuChannel.GetName()+"_Validation_Neutral.pdf", ";muon_p_ConeIso                          ;", false, false, 0.11, 0.59);
      PrintFitPlot(KMuNuChannel.DataTemplates().at(15), KMuNuChannel.CombinedTemplates(), true, 15, KMuNuChannel.GetName()+"_Validation_Neutral.pdf", ";muon_p_PT                               ;", false, false, 0.11, 0.59);
      PrintFitPlot(KMuNuChannel.DataTemplates().at(16), KMuNuChannel.CombinedTemplates(), true, 16, KMuNuChannel.GetName()+"_Validation_Neutral.pdf", ";muon_p_0.50_cc_asy_P                    ;", false, false, 0.11, 0.59);
      PrintFitPlot(KMuNuChannel.DataTemplates().at(17), KMuNuChannel.CombinedTemplates(), true, 17, KMuNuChannel.GetName()+"_Validation_Neutral.pdf", ";kaon_m_TRACK_CHI2NDOF                   ;", false, false, 0.11, 0.59);
      PrintFitPlot(KMuNuChannel.DataTemplates().at(18), KMuNuChannel.CombinedTemplates(), true, 18, KMuNuChannel.GetName()+"_Validation_Neutral.pdf", ";Bs_FD_S                                 ;", false, false, 0.11, 0.59);
      PrintFitPlot(KMuNuChannel.DataTemplates().at(19), KMuNuChannel.CombinedTemplates(), true, 19, KMuNuChannel.GetName()+"_Validation_Neutral.pdf", ";Bs_PT                                   ;", false, false, 0.11, 0.59);
      PrintFitPlot(KMuNuChannel.DataTemplates().at(20), KMuNuChannel.CombinedTemplates(), true, 20, KMuNuChannel.GetName()+"_Validation_Neutral.pdf", ";kaon_m_0.50_nc_IT                       ;", false, false, 0.11, 0.59);
      PrintFitPlot(KMuNuChannel.DataTemplates().at(21), KMuNuChannel.CombinedTemplates(), true, 21, KMuNuChannel.GetName()+"_Validation_Neutral.pdf", ";muon_p_1.50_IT                          ;", false, false, 0.11, 0.59);
      PrintFitPlot(KMuNuChannel.DataTemplates().at(22), KMuNuChannel.CombinedTemplates(), true, 22, KMuNuChannel.GetName()+"_Validation_Neutral.pdf", ";kaon_m_0.50_cc_IT                       ;", false, false, 0.11, 0.59);
      PrintFitPlot(KMuNuChannel.DataTemplates().at(23), KMuNuChannel.CombinedTemplates(), true, 23, KMuNuChannel.GetName()+"_Validation_Neutral.pdf", ";muon_p_2.00_nc_PZ                       ;", false, false, 0.11, 0.59);
      PrintFitPlot(KMuNuChannel.DataTemplates().at(24), KMuNuChannel.CombinedTemplates(), true, 24, KMuNuChannel.GetName()+"_Validation_Neutral.pdf", ";kaon_m_0.50_nc_asy_P                    ;", false, false, 0.11, 0.59);
      PrintFitPlot(KMuNuChannel.DataTemplates().at(25), KMuNuChannel.CombinedTemplates(), true, 25, KMuNuChannel.GetName()+"_Validation_Neutral.pdf", ";muon_p_1.00_nc_asy_P                    ;", false, false, 0.11, 0.59);
      PrintFitPlot(KMuNuChannel.DataTemplates().at(26), KMuNuChannel.CombinedTemplates(), true, 26, KMuNuChannel.GetName()+"_Validation_Neutral.pdf", ";kaon_m_pi0_mult                         ;", false, false, 0.11, 0.59);
      PrintFitPlot(KMuNuChannel.DataTemplates().at(27), KMuNuChannel.CombinedTemplates(), true, 27, KMuNuChannel.GetName()+"_Validation_Neutral.pdf", ";Bs_cosTheta1_star-Bs_cosTheta2_star     ;", false, false, 0.11, 0.59);
      PrintFitPlot(KMuNuChannel.DataTemplates().at(28), KMuNuChannel.CombinedTemplates(), true, 28, KMuNuChannel.GetName()+"_Validation_Neutral.pdf", ";kaon_m_Pi0_PZ                           ;", false, false, 0.11, 0.59);
      PrintFitPlot(KMuNuChannel.DataTemplates().at(29), KMuNuChannel.CombinedTemplates(), true, 29, KMuNuChannel.GetName()+"_Validation_Neutral.pdf", ";kaon_m_MasshPi0                         ;", false, false, 0.11, 0.59);
      PrintFitPlot(KMuNuChannel.DataTemplates().at(30), KMuNuChannel.CombinedTemplates(), true, 30, KMuNuChannel.GetName()+"_Validation_Neutral.pdf", ";kaon_m_1.00_nc_asy_PT                   ;", false, false, 0.11, 0.59);
      PrintFitPlot(KMuNuChannel.DataTemplates().at(31), KMuNuChannel.CombinedTemplates(), true, 31, KMuNuChannel.GetName()+"_Validation_Neutral.pdf", ";muon_p_1.50_nc_mult                     ;", false, false, 0.11, 0.59);
      PrintFitPlot(KMuNuChannel.DataTemplates().at(32), KMuNuChannel.CombinedTemplates(), true, 32, KMuNuChannel.GetName()+"_Validation_Neutral.pdf", ";kaon_m_1.00_nc_mult                     ;", false, false, 0.11, 0.59);
      cx->Print((KMuNuChannel.GetName()+"_Validation_Neutral.pdf]").c_str());



      cx->Print((KMuNuChannel.GetName()+"_Validation_Charged.pdf[").c_str());
      PrintFitPlot(KMuNuChannel.DataTemplates().at(33), KMuNuChannel.CombinedTemplates(), true, 33, KMuNuChannel.GetName()+"_Validation_Charged.pdf", ";Bs_DIRA_OWNPV                            ;", false, false, 0.11, 0.59);
      PrintFitPlot(KMuNuChannel.DataTemplates().at(34), KMuNuChannel.CombinedTemplates(), true, 34, KMuNuChannel.GetName()+"_Validation_Charged.pdf", ";Bs_ENDVERTEX_CHI2                        ;", false, false, 0.60, 0.59);
      PrintFitPlot(KMuNuChannel.DataTemplates().at(35), KMuNuChannel.CombinedTemplates(), true, 35, KMuNuChannel.GetName()+"_Validation_Charged.pdf", ";kaon_m_PT                                ;", false, false, 0.60, 0.59);
      PrintFitPlot(KMuNuChannel.DataTemplates().at(36), KMuNuChannel.CombinedTemplates(), true, 36, KMuNuChannel.GetName()+"_Validation_Charged.pdf", ";kaon_m_0.50_IT                           ;", false, false, 0.11, 0.59);
      PrintFitPlot(KMuNuChannel.DataTemplates().at(37), KMuNuChannel.CombinedTemplates(), true, 37, KMuNuChannel.GetName()+"_Validation_Charged.pdf", ";kaon_m_1.00_cc_asy_P                     ;", false, false, 0.11, 0.59);
      PrintFitPlot(KMuNuChannel.DataTemplates().at(38), KMuNuChannel.CombinedTemplates(), true, 38, KMuNuChannel.GetName()+"_Validation_Charged.pdf", ";muon_p_1.50_cc_mult+kaon_m_1.50_cc_mult  ;", false, false, 0.60, 0.59);
      PrintFitPlot(KMuNuChannel.DataTemplates().at(39), KMuNuChannel.CombinedTemplates(), true, 39, KMuNuChannel.GetName()+"_Validation_Charged.pdf", ";kaon_m_0.50_cc_deltaEta                  ;", false, false, 0.60, 0.59);
      PrintFitPlot(KMuNuChannel.DataTemplates().at(40), KMuNuChannel.CombinedTemplates(), true, 40, KMuNuChannel.GetName()+"_Validation_Charged.pdf", ";kaon_m_1.00_nc_asy_PT                    ;", false, false, 0.11, 0.59);
      PrintFitPlot(KMuNuChannel.DataTemplates().at(41), KMuNuChannel.CombinedTemplates(), true, 41, KMuNuChannel.GetName()+"_Validation_Charged.pdf", ";muon_p_1.00_IT                           ;", false, false, 0.11, 0.59);
      PrintFitPlot(KMuNuChannel.DataTemplates().at(42), KMuNuChannel.CombinedTemplates(), true, 42, KMuNuChannel.GetName()+"_Validation_Charged.pdf", ";min(muon_p_ConeIso, kaon_m_ConeIso)      ;", false, false, 0.11, 0.59);
      PrintFitPlot(KMuNuChannel.DataTemplates().at(43), KMuNuChannel.CombinedTemplates(), true, 43, KMuNuChannel.GetName()+"_Validation_Charged.pdf", ";max(kaon_m_IsoSumBDT, muon_p_IsoSumBDT)  ;", false, false, 0.11, 0.59);
      PrintFitPlot(KMuNuChannel.DataTemplates().at(44), KMuNuChannel.CombinedTemplates(), true, 44, KMuNuChannel.GetName()+"_Validation_Charged.pdf", ";min(kaon_m_IsoMinBDT, muon_p_IsoMinBDT)  ;", false, false, 0.60, 0.59);
      PrintFitPlot(KMuNuChannel.DataTemplates().at(45), KMuNuChannel.CombinedTemplates(), true, 45, KMuNuChannel.GetName()+"_Validation_Charged.pdf", ";kaon_m_IsoMinBDT - muon_p_IsoMinBDT      ;", false, false, 0.11, 0.59);
      PrintFitPlot(KMuNuChannel.DataTemplates().at(46), KMuNuChannel.CombinedTemplates(), true, 46, KMuNuChannel.GetName()+"_Validation_Charged.pdf", ";muon_p_PAIR_M                            ;", false, false, 0.60, 0.59);
      PrintFitPlot(KMuNuChannel.DataTemplates().at(47), KMuNuChannel.CombinedTemplates(), true, 47, KMuNuChannel.GetName()+"_Validation_Charged.pdf", ";kaon_m_PAIR_M                            ;", false, false, 0.60, 0.59);
      PrintFitPlot(KMuNuChannel.DataTemplates().at(48), KMuNuChannel.CombinedTemplates(), true, 48, KMuNuChannel.GetName()+"_Validation_Charged.pdf", ";muon_p_PT                                ;", false, false, 0.60, 0.59);
      PrintFitPlot(KMuNuChannel.DataTemplates().at(49), KMuNuChannel.CombinedTemplates(), true, 49, KMuNuChannel.GetName()+"_Validation_Charged.pdf", ";kaon_m_TRACK_PCHI2                       ;", false, false, 0.60, 0.59);
      PrintFitPlot(KMuNuChannel.DataTemplates().at(50), KMuNuChannel.CombinedTemplates(), true, 50, KMuNuChannel.GetName()+"_Validation_Charged.pdf", ";Bs_PT                                    ;", false, false, 0.60, 0.59);
      PrintFitPlot(KMuNuChannel.DataTemplates().at(51), KMuNuChannel.CombinedTemplates(), true, 51, KMuNuChannel.GetName()+"_Validation_Charged.pdf", ";Bs_cosTheta1_star-Bs_cosTheta2_star      ;", false, false, 0.11, 0.59);
      cx->Print((KMuNuChannel.GetName()+"_Validation_Charged.pdf]").c_str());
*/


    }
    map<string,double> results = KMuNuChannel.GetResults();

    // To save correlations edit this line
    //results["corr_Ds_DsStar"]  = KMuNuChannel.GetFitResults()->correlation("Ds_Yield", "DsStar_Yield");


    if ( toy == -1 )
    {
      KMuNuChannel.BuildPseudoData(nToys);
      PseudoData      = KMuNuChannel.GetPseudoData();
      PseudoDataYields= KMuNuChannel.GetPseudoDataYieds();
    }


    if ( toy >= 0 )
    {
        for( auto& Input:PseudoDataYields.at(toy))
        {
            string name = Input.first;
            double InputYield = Input.second;
            double FitYield   = results[name];
            double FitErr     = results[name+"_err"];

            results[name + "_In"]   = InputYield;
            results[name + "_res"]  = FitYield - InputYield;
            results[name + "_pull"] = (FitYield - InputYield)/FitErr;

        }
    }

    if ( toy == 0 )
    {
        SaveToys.init(results);
    }

    if ( toy >= 0 )
    {
       SaveToys.AddEntry(results);
    }


  }

  SaveToys.Close();

  return EXIT_SUCCESS;
    //return results;
}
