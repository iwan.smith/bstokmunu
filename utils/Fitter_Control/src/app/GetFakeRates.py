

import ROOT
import pickle
import numpy as np

from math import pow, sqrt
try:
    Histograms = pickle.load(open("Histograms.pkl", "rb"))
except:
    Chains = {}
    Chains["K"] = ROOT.TChain("RSDStCalib")
    Chains["K"].Add("/eos/lhcb/wg/semileptonic/Bs2KmunuAna/Tuples/PID/Data/DSt_K_Mag*_Strip20_*.root")
    
    Chains["Pi"] = ROOT.TChain("RSDStCalib")
    Chains["Pi"].Add("/eos/lhcb/wg/semileptonic/Bs2KmunuAna/Tuples/PID/Data/DSt_Pi_Mag*_Strip20_*.root")
    
    Chains["Mu"] = ROOT.TChain("JpsiCalib")
    Chains["Mu"].Add("/eos/lhcb/wg/semileptonic/Bs2KmunuAna/Tuples/PID/Data/Jpsi_Mu_Mag*_Strip20_*.root")

    Chains["e"] = ROOT.TChain("JpsieeCalib")
    Chains["e"].Add("/eos/lhcb/wg/semileptonic/Bs2KmunuAna/Tuples/PID/Data/Jpsi_e_Mag*_Strip20_*.root")

    Chains["P"] = ROOT.TChain("Lam0Calib")
    Chains["P"].Add("/eos/lhcb/wg/semileptonic/Bs2KmunuAna/Tuples/PID/Data/Lam0_P_Mag*_Strip20_*.root")
    
    # Make these plots with the histogrammer in future:
    """
    Chains["FakeMu"] = ROOT.TChain("Bs2DsMuNuTuple_fakeMu")
    Chains["FakeMu"].Add("/eos/lhcb/wg/semileptonic/Bs2KmunuAna/Tuples/Bs2KMuNu_DATATUPLES_TRIMMED_19June16/Merged_DTT_2012_Reco14Strip21r0p1a_*_SEMILEPTONIC.root")
    
    Chains["Data"] = ROOT.TChain("Bs2DsMuNuTuple")
    Chains["Data"].Add("/eos/lhcb/wg/semileptonic/Bs2KmunuAna/Tuples/Bs2KMuNu_DATATUPLES_TRIMMED_19June16/Merged_DTT_2012_Reco14Strip21r0p1a_*_SEMILEPTONIC.root")
    """
    
    
    Triggers = {
        "K":"K_Unbias_HLT1",
        "Pi":"Pi_Unbias_HLT1",
        "Mu":"Mu_Tag_TOS",
        "e":"1",
        "P":"P_Unbias_HLT12"
    }
    
    Histograms = {}
    for P in ["K", "Pi", "Mu", "e", "P", "Data"]:
        Histograms[P ]   = {"Cut":[], "Fake":[], "SameSign":[]}        
        
        Histograms[P]["Cut"]  += [ ROOT.TH3D("h_{0}_PIDK_Cut".format(P),  "DLL ( K - #pi )",   100, -100, 50, 10, 10000, 100000, 10, 2, 5) ]
        Histograms[P]["Cut"]  += [ ROOT.TH3D("h_{0}_PIDMu_Cut".format(P), "DLL ( #mu - #pi )", 100,  0,   15, 10, 10000, 100000, 10, 2, 5) ]
        Histograms[P]["Cut"]  += [ ROOT.TH3D("h_{0}_PIDP_Cut".format(P),  "DLL ( P - #pi )",   100, -100, 40, 10, 10000, 100000, 10, 2, 5) ]
        Histograms[P]["Cut"]  += [ ROOT.TH3D("h_{0}_PIDe_Cut".format(P),  "DLL ( e - #pi )",   100, -10,  2,  10, 10000, 100000, 10, 2, 5) ]
        
        Histograms[P]["Fake"] += [ ROOT.TH3D("h_{0}_PIDK_Fake".format(P),  "DLL ( K - #pi )",   100, -80, 50,  10, 10000, 100000, 10, 2, 5) ]
        Histograms[P]["Fake"] += [ ROOT.TH3D("h_{0}_PIDMu_Fake".format(P), "DLL ( #mu - #pi )", 100, -15, 0 ,  10, 10000, 100000, 10, 2, 5) ]
        Histograms[P]["Fake"] += [ ROOT.TH3D("h_{0}_PIDP_Fake".format(P),  "DLL ( P - #pi )",   100, -80, 40,  10, 10000, 100000, 10, 2, 5) ]
        Histograms[P]["Fake"] += [ ROOT.TH3D("h_{0}_PIDe_Fake".format(P),  "DLL ( e - #pi )",   100, -15, 15,  10, 10000, 100000, 10, 2, 5) ]

        if P == "Data":
            continue
        
        
        
        Chains[P].Draw("{0}_Eta : {0}_P : {0}_CombDLLK                 >> h_{0}_PIDK_Cut"   .format(P), ("nsig_sw * ({0}_CombDLLmu > 3 && {0}_IsMuon && {1})").format(P, Triggers[P]))
        Chains[P].Draw("{0}_Eta : {0}_P : {0}_CombDLLmu                >> h_{0}_PIDMu_Cut"  .format(P), ("nsig_sw * ({0}_CombDLLmu > 3 && {0}_IsMuon && {1})").format(P, Triggers[P]))
        Chains[P].Draw("{0}_Eta : {0}_P : {0}_CombDLLp                 >> h_{0}_PIDP_Cut"   .format(P), ("nsig_sw * ({0}_CombDLLmu > 3 && {0}_IsMuon && {1})").format(P, Triggers[P]))
        Chains[P].Draw("{0}_Eta : {0}_P : {0}_CombDLLe                 >> h_{0}_PIDe_Cut"   .format(P), ("nsig_sw * ({0}_CombDLLmu > 3 && {0}_IsMuon && {1})").format(P, Triggers[P]))

        Chains[P].Draw("{0}_Eta : {0}_P : {0}_CombDLLK                  >> h_{0}_PIDK_Fake"   .format(P), ("nsig_sw * ({0}_CombDLLmu < 0 && {1} )").format(P, Triggers[P]))
        Chains[P].Draw("{0}_Eta : {0}_P : {0}_CombDLLmu                 >> h_{0}_PIDMu_Fake"  .format(P), ("nsig_sw * ({0}_CombDLLmu < 0 && {1})").format(P, Triggers[P]))
        Chains[P].Draw("{0}_Eta : {0}_P : {0}_CombDLLp                  >> h_{0}_PIDP_Fake"   .format(P), ("nsig_sw * ({0}_CombDLLmu < 0 && {1})").format(P, Triggers[P]))
        Chains[P].Draw("{0}_Eta : {0}_P : {0}_CombDLLe                  >> h_{0}_PIDe_Fake"   .format(P), ("nsig_sw * ({0}_CombDLLmu < 0 && {1})").format(P, Triggers[P]))
    """
    Chains["Data"].Draw  (" muon_p_TRACK_Eta : muon_p_P : muon_p_PIDK                 >> h_Data_PIDK_Cut"  )
    Chains["Data"].Draw  (" muon_p_TRACK_Eta : muon_p_P : muon_p_PIDmu                >> h_Data_PIDMu_Cut" )
    Chains["Data"].Draw  (" muon_p_TRACK_Eta : muon_p_P : muon_p_PIDp                 >> h_Data_PIDP_Cut"  )
                            
    Chains["FakeMu"].Draw(" muon_p_TRACK_Eta : muon_p_P : muon_p_PIDK                >> h_Data_PIDK_Fake"   )
    Chains["FakeMu"].Draw(" muon_p_TRACK_Eta : muon_p_P : muon_p_PIDmu               >> h_Data_PIDMu_Fake"  )
    Chains["FakeMu"].Draw(" muon_p_TRACK_Eta : muon_p_P : muon_p_PIDp                >> h_Data_PIDP_Fake"   )
    """
    pickle.dump(Histograms, open("Histograms.pkl", "wb") )
    



file_data = ROOT.TFile.Open("../Histogrammer/Histograms.root")
Histograms["Data"]["Cut"] += [ file_data.Get("sWeight/DsMuNu/Data/PID_Tight/h_Data_PIDK_Cut") ]
Histograms["Data"]["Cut"] += [ file_data.Get("sWeight/DsMuNu/Data/PID_Tight/h_Data_PIDMu_Cut") ]
Histograms["Data"]["Cut"] += [ file_data.Get("sWeight/DsMuNu/Data/PID_Tight/h_Data_PIDP_Cut") ]
Histograms["Data"]["Cut"] += [ file_data.Get("sWeight/DsMuNu/Data/PID_Tight/h_Data_PIDe_Cut") ]

Histograms["Data"]["Fake"] += [ file_data.Get("sWeight/DsMuNu/FakeMu/Nominal/h_Data_PIDK_Fake") ]
Histograms["Data"]["Fake"] += [ file_data.Get("sWeight/DsMuNu/FakeMu/Nominal/h_Data_PIDMu_Fake") ]
Histograms["Data"]["Fake"] += [ file_data.Get("sWeight/DsMuNu/FakeMu/Nominal/h_Data_PIDP_Fake") ]
Histograms["Data"]["Fake"] += [ file_data.Get("sWeight/DsMuNu/FakeMu/Nominal/h_Data_PIDe_Fake") ]

Histograms["Data"]["SameSign"] += [ file_data.Get("sWeight/DsMuNu/SameSign/Nominal/h_Data_PIDK_Cut") ]
Histograms["Data"]["SameSign"] += [ file_data.Get("sWeight/DsMuNu/SameSign/Nominal/h_Data_PIDMu_Cut") ]
Histograms["Data"]["SameSign"] += [ file_data.Get("sWeight/DsMuNu/SameSign/Nominal/h_Data_PIDP_Cut") ]
Histograms["Data"]["SameSign"] += [ file_data.Get("sWeight/DsMuNu/SameSign/Nominal/h_Data_PIDe_Cut") ]

print Histograms["Data"]


SLicedHistograms = {}


from math import pow
def f(N_K, N_Pi, N_e, N_Mu, N_P):

    Chisq = 0;
    
    global isFake
    
    n_MC = N_K + N_Pi + N_e + N_Mu + N_P

    
    for Hist in range(4):
        
        LL_Weight = 1;
        
        if Hist==1:
            LL_Weight = 2
        
        nData = SLicedHistograms["Data"][isFake][Hist].Integral()
        
        Ratio = nData/n_MC
    
        N_K1 = N_K  *Ratio
        N_Pi1= N_Pi *Ratio
        N_e1 = N_e  *Ratio
        N_Mu1= N_Mu *Ratio
        N_P1 = N_P  *Ratio

        h_model = N_K1 * SLicedHistograms["K"][isFake][Hist] + N_Pi1 * SLicedHistograms["Pi"][isFake][Hist] + N_Mu1 * SLicedHistograms["Mu"][isFake][Hist] + N_e1 * SLicedHistograms["e"][isFake][Hist] + N_P1 * SLicedHistograms["P"][isFake][Hist]
        
        # If Using Chi-Squared
        
        delta   = ( h_model - SLicedHistograms["Data"][isFake][Hist] ) 
        delta.Divide(SLicedHistograms["Data"][isFake][Hist])
        
        # If Using likelihood
                
        bins = delta.GetNbinsX()
        for bin in range(1, bins+1): 
            if delta.GetBinError(bin) == 0:
                continue
            Chisq += abs( delta.GetBinContent(bin))
            #Chisq += pow( delta.GetBinContent(bin) / delta.GetBinError(bin), 2 )
    
    return Chisq

from iminuit import Minuit



PlotHistograms = {}
for Particle in ["K", "Pi", "Mu", "e", "P", "Data"]:
    PlotHistograms[Particle] = {"Cut":[], "Fake":[]}
    
    for isFake in ["Fake", "Cut"]:
        for Hist in Histograms[Particle][isFake]:
            
            NewHist = Hist.ProjectionX("_px", 0, -1, 0, -1)
            NewHist.Reset()
            
            PlotHistograms[Particle][isFake] += [NewHist]

PlotYields = {}
for Particle in ["K", "Pi", "Mu", "e", "P"]:
    PlotYields[Particle] = {"Cut":(0,0), "Fake":(0,0)}

for isFake in ["Fake", "Cut"]:
    
    for P in range(1, 11):
        for Eta in range(1,11):
            
            SLicedHistograms = {}
            nData = 0
            for Particle in ["K", "Pi", "Mu", "e", "P"]:
                SLicedHistograms[Particle] = {"Cut":[], "Fake":[]}
                for Hist in Histograms[Particle][isFake]:
                    
                    NewHist = Hist.ProjectionX("_px", P, P, Eta, Eta)
                    if NewHist.Integral() > 0:
                        NewHist.Scale( 1.0/NewHist.Integral() )
                        
                    for bin in range( NewHist.GetNbinsX() ):
                        if NewHist.GetBinContent(bin+1) < 0:
                            NewHist.SetBinContent(bin+1, 0)
                        
                    SLicedHistograms[Particle][isFake] += [NewHist]
            
            SLicedHistograms["Data"] = {"Cut":[], "Fake":[]}
            for Hist in Histograms["Data"][isFake]:
                
                NewHist = Hist.ProjectionX("_px", P, P, Eta, Eta)
                SLicedHistograms["Data"][isFake] += [NewHist]
                
                nData = NewHist.Integral()
            
            if nData < 0:
                continue
            
            nSample = nData/4          
            if isFake == "Fake":
                m = Minuit(f, N_K = nData*0.05, N_Pi = nData*0.05, N_e=nData*0.05, N_Mu=nData*0.05, N_P=nData*0.05, limit_N_K = (0,nData+1), limit_N_Pi = (0,nData*0.1+1), limit_N_e = (0,nData+1), limit_N_P = (0,nData+1), limit_N_Mu = (0,nData+1)   )
            elif isFake == "Cut":
                m = Minuit(f, N_K = nData*0.05, N_Pi = nData*0.05, N_e=nData*0.05, N_Mu=nData*0.8, N_P=nData*0.05, limit_N_K = (0,nData+1), limit_N_Pi = (0,nData*0.1+1), limit_N_Mu = (0,nData+1), limit_N_P = (0,nData+1), limit_N_e = (0,nData+1)   )
                
            m.migrad();
            
            
            n_K, n_Pi, n_e, n_Mu, n_P = m.args
            
            n_MC = n_K + n_Pi + n_e + n_Mu + n_P
            Ratio = nData/n_MC
            
            print "Yields Are:"
            print "K:\t",  n_K  *Ratio
            print "Pi:\t", n_Pi *Ratio
            print "Mu:\t", n_Mu *Ratio
            print "e:\t",  n_e *Ratio
            print "P:\t",  n_P *Ratio
            
            Yields = {
                "K":n_K  *Ratio,
                "Pi":n_Pi *Ratio,
                "Mu":n_Mu  *Ratio,
                "e":n_e  *Ratio,
                "P":n_P  *Ratio,
                      }
                
            for Particle in ["K", "Pi", "Mu", "e", "P"]:
                
                
                y, e = PlotYields[Particle][isFake]
                
                y += m.values["N_" + Particle] * Ratio
                if ( m.errors["N_" + Particle] ):
                    e  = sqrt( e*e + pow( m.errors["N_" + Particle] * Ratio, 2) )
                
                PlotYields[Particle][isFake] = (y, e)
                
                for n in range( len( Histograms[Particle][isFake] ) ):
                    PlotHistograms[Particle][isFake][n] += Yields[Particle] * SLicedHistograms[Particle][isFake][n]
                    
            for n in range( len( Histograms[Particle][isFake] ) ):
                PlotHistograms["Data"][isFake][n] += SLicedHistograms["Data"][isFake][n]


for isFake in ["Fake", "Cut"]:
    
    
    c1 = ROOT.TCanvas("c1", "c1", 1600, 1600)
    
    for Hist in range(4):
    
        c1.Clear()
    
        PlotHistograms["Data"][isFake][Hist].Draw()
        
        PlotHistograms["Data"][isFake][Hist].Sumw2(False)
        
        
        Hist_Sum = PlotHistograms["K"][isFake][Hist] + PlotHistograms["Pi"][isFake][Hist] + PlotHistograms["Mu"][isFake][Hist] + PlotHistograms["e"][isFake][Hist] + PlotHistograms["P"][isFake][Hist]

        ratio = PlotHistograms["Data"][isFake][Hist].Integral() / Hist_Sum.Integral()

        Hist_Sum.Scale(ratio)
        Hist_Sum.Sumw2()
        Hist_Sum.SetLineColor(2)
        Hist_Sum.SetMarkerColor(2)
        Hist_Sum.Draw("SAME")
        
        h_K = PlotHistograms["K"][isFake][Hist] 
        h_K.Scale(ratio)
        h_K.Sumw2()
        h_K.SetLineColor(3)
        h_K.SetMarkerColor(3)
        h_K.Draw("SAME")
        
        h_Pi = PlotHistograms["Pi"][isFake][Hist]
        h_Pi.Scale(ratio)
        h_Pi.Sumw2()
        h_Pi.SetLineColor(7)
        h_Pi.SetMarkerColor(7)
        h_Pi.Draw("SAME")
        
        h_Mu = PlotHistograms["Mu"][isFake][Hist]
        h_Mu.Scale(ratio)
        h_Mu.Sumw2()
        h_Mu.SetLineColor(6)
        h_Mu.SetMarkerColor(6)
        h_Mu.Draw("SAME")
    
        h_e = PlotHistograms["e"][isFake][Hist]
        h_e.Scale(ratio)
        h_e.Sumw2()
        h_e.SetLineColor(8)
        h_e.SetMarkerColor(8)
        h_e.Draw("SAME")
    
        h_P = PlotHistograms["P"][isFake][Hist]
        h_P.Scale(ratio)
        h_P.Sumw2()
        h_P.SetLineColor(9)
        h_P.SetMarkerColor(9)
        h_P.Draw("SAME")
        
        legend = ROOT.TLegend(0.1,0.7,0.3,0.9)
        for Particle in ["Data", "K", "Pi", "Mu", "e", "P"]:
            legend.AddEntry(PlotHistograms[Particle][isFake][Hist], Particle,  "lep")
        
        legend.Draw()
        c1.Print("PIDFit_{0}_{1}.pdf".format(isFake, Hist))
        
    

print PlotYields