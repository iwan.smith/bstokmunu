import numpy as np

Yields = np.asarray([18000, 98300, 7500, 212800, 1000, 5000], dtype=np.float)
nExp = 100000000


nEv = np.sum(Yields)
nSample = Yields.shape[0]
NewYields = np.zeros((nSample,nExp), dtype=np.float)

ToyYields = np.random.normal(0.0, 1.0, (nSample,nExp))*np.sqrt(Yields)[:,None] + Yields[:,None] 
Normalisation = nEv/np.sum(ToyYields, axis=0)
NewYields = Normalisation[None,:] * ToyYields 


import ROOT
ROOT.gStyle.SetNumberContours(999)
ROOT.gStyle.SetPalette(ROOT.kRainBow)
ROOT.gStyle.SetOptStat(0)



c1 = ROOT.TCanvas("c1", "c1", 900, 900)
c1.Divide(2,2)

c1.cd(1).SetLogz()
h1 = ROOT.TH2F("h1", ";D_{s} Pull;D_{s}* Pull", 500, -3, 3, 500,-3, 3)
h1.FillN(nExp, (NewYields[1,:]-Yields[1])/np.sqrt(Yields[1]), (NewYields[3,:]-Yields[3])/np.sqrt(Yields[3]), np.ones(nExp) )
h1.Draw("COLZ")


c1.cd(2).SetLogz()
h2 = ROOT.TH2F("h2", ";D_{s} Pull;B #rightarrow D_{s} D Pull", 500, -3, 3, 500,-3, 3)
h2.FillN(nExp, (NewYields[1,:]-Yields[1])/np.sqrt(Yields[1]), (NewYields[0,:]-Yields[0])/np.sqrt(Yields[0]), np.ones(nExp) )
h2.Draw("COLZ")


c1.cd(3).SetLogz()
h3 = ROOT.TH2F("h3", ";D_{s} Pull;D_{s}*^{1,0} Pull", 500, -3, 3, 500,-3, 3)
h3.FillN(nExp, (NewYields[1,:]-Yields[1])/np.sqrt(Yields[1]), (NewYields[2,:]-Yields[2])/np.sqrt(Yields[2]), np.ones(nExp) )
h3.Draw("COLZ")


c1.cd(4).SetLogz()
h4 = ROOT.TH2F("h4", ";D_{s} Pull;D_{s} #tau Pull", 500, -3, 3, 500,-3, 3)
h4.FillN(nExp, (NewYields[1,:]-Yields[1])/np.sqrt(Yields[1]), (NewYields[5,:]-Yields[5])/np.sqrt(Yields[5]), np.ones(nExp) )
h4.Draw("COLZ")

c1.Draw()