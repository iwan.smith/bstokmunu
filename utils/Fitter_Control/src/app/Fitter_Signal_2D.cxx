/*
 * Newfitter2.cxx
 *
 *  Created on: 17 Aug 2017
 *      Author: ismith
 */

// header including everything
#include "FitterHeader.h"
#include "RooFitHeader.h"

#include "FitterFunctions.h"
#include "FitterObjects.h"

#include "HistogrammerTools.h"

#include "Fitter.h"
#include "TString.h"

#include "KMuNuTools.h"

#include "FitArgParse.h"

double GetBDTEfficiency(vector<shared_ptr<TH1F>> Histograms, bool ScaleByBu = false, string SaveBFit = "")
{
  if ( ScaleByBu )
    {

      double Measured_B_Yield = 0.0;
      std::tie(Measured_B_Yield, std::ignore) = FitJpsiK(Histograms[4].get(), SaveBFit);
      //double Actual_B_Yield   = Histograms[2]->Integral();

      return Histograms[1]->Integral() / Measured_B_Yield;

    }


  return Histograms[1]->Integral() / Histograms[2]->Integral();
}

double GetEfficiency(shared_ptr<TH1F> Histogram, std::string EventTupleLoc, TFile* f)
{
  double YieldAtProduction = 1.00;


  TObject* EventTuple = f->Get(("EventTuples/" + EventTupleLoc).c_str());
  if ( EventTuple)
    YieldAtProduction = std::stod( EventTuple->GetTitle());
  double YieldInHistogram  = Histogram->Integral();


  return YieldInHistogram/YieldAtProduction;
}


double GetFraction(ChannelBuilder& Channel, vector<ChannelBuilder>& v_Channel, string HistName)
{
  double n_channel = Channel.TemplateContainer().at(HistName).Histograms[1]->Integral();

  double n_Total(0.0);
  n_Total += v_Channel[0].TemplateContainer().at(HistName).Histograms[1]->Integral();
  n_Total += v_Channel[1].TemplateContainer().at(HistName).Histograms[1]->Integral();

  return n_channel/n_Total;


}


vector<shared_ptr<TH1F>> LoadHistograms(std::string Histname, TFile* f_hist, std::string Cut, std::string Systematic = "", bool isSmeared = false)
{


  string EventType  ="";

  map<string,string> NameToEventType{
    {"KMuNu",        "MC_13512010"},
    {"KstMuNu",      "MC_13512400"},
    {"K2stMuNu",     "MC_13512410"},
    {"Kst1430MuNu",  "MC_13512420"},
    {"Bu_D0pi",      "MC_12573050"},
    {"Bd_Dpi",       "MC_11574051"},
    {"Lb_pMuNu_LCSR","MC_15512013"},
    {"Lb_pMuNu_LQCD","MC_15512014"},
    {"Bs_JpsiPhi",   "MC_13144001"},
    {"Bu_JpsiKst",   "MC_12143401"},
    {"Bu_JpsiK",     "MC_12143001"},
    {"Bd_JpsiKst",   "MC_11144001"},
    {"Bu_D0MuNu",    "MC_12873002"},
    {"Bu_rhoMuNu",   "MC_12513001"},
    {"Bd_DMuNu",     "MC_11874042"},
    {"Bd_rhomunu",   "MC_11512400"},
    {"Bd_pimunu",    "MC_11512011"}
  };

 if ( NameToEventType.count(Histname) )
   EventType = NameToEventType[Histname];
 else
   EventType = Histname;





  vector<shared_ptr<TH1F>> Histograms;

  /*
   * Loader for Data
   */


  if ( EventType == "Data")
  {
    bool smooth = false;
    bool ClearUnderFlow = true;

    Histograms = {
        GetHist(f_hist, "Nominal/KMuNu/Data/" + Cut + "/h_Bs_MCORR",       false, smooth, ClearUnderFlow, isSmeared ),
        GetHist(f_hist, "Nominal/KMuNu/Data/" + Cut + "/h_Bs_MCORR",       false, smooth, ClearUnderFlow, isSmeared ),
        GetHist(f_hist, "Nominal/KMuNu/Data/" + Cut + "_NoBDT/h_Bs_MCORR", false, smooth, ClearUnderFlow, isSmeared ),
        GetHist(f_hist, "Nominal/KMuNu/Data/" + Cut + "/h_Bu_MM",          false, smooth, ClearUnderFlow, isSmeared ),
        GetHist(f_hist, "Nominal/KMuNu/Data/" + Cut + "_NoBDT/h_Bu_MM",    false, smooth, ClearUnderFlow, isSmeared ),
        GetHist(f_hist, "Nominal/KMuNu/Data/" + Cut + "/MissingMass",      false, smooth, ClearUnderFlow, isSmeared ),
        GetHist(f_hist, "Nominal/KMuNu/Data/" + Cut + "/h_Bs_PT",      false, smooth, ClearUnderFlow, isSmeared ),
        GetHist(f_hist, "Nominal/KMuNu/Data/" + Cut + "/h_Bs_PT",      false, smooth, ClearUnderFlow, isSmeared ),
        GetHist(f_hist, "Nominal/KMuNu/Data/" + Cut + "/h_Bs_PT",      false, smooth, ClearUnderFlow, isSmeared ),
        //GetHist(f_hist, "Nominal/KMuNu/Data/" + Cut + "/TMVA_CCharged_BDT",      false, smooth, ClearUnderFlow, isSmeared ),


        GetHist(f_hist, "Nominal/KMuNu/Data/" + Cut + "/BDT_NC_V00",   false, smooth, ClearUnderFlow, isSmeared ),
        GetHist(f_hist, "Nominal/KMuNu/Data/" + Cut + "/BDT_NC_V01",   false, smooth, ClearUnderFlow, isSmeared ),
        GetHist(f_hist, "Nominal/KMuNu/Data/" + Cut + "/BDT_NC_V02",   false, smooth, ClearUnderFlow, isSmeared ),
        GetHist(f_hist, "Nominal/KMuNu/Data/" + Cut + "/BDT_NC_V03",   false, smooth, ClearUnderFlow, isSmeared ),
        GetHist(f_hist, "Nominal/KMuNu/Data/" + Cut + "/BDT_NC_V04",   false, smooth, ClearUnderFlow, isSmeared ),
        GetHist(f_hist, "Nominal/KMuNu/Data/" + Cut + "/BDT_NC_V05",   false, smooth, ClearUnderFlow, isSmeared ),
        GetHist(f_hist, "Nominal/KMuNu/Data/" + Cut + "/BDT_NC_V06",   false, smooth, ClearUnderFlow, isSmeared ),
        GetHist(f_hist, "Nominal/KMuNu/Data/" + Cut + "/BDT_NC_V07",   false, smooth, ClearUnderFlow, isSmeared ),
        GetHist(f_hist, "Nominal/KMuNu/Data/" + Cut + "/BDT_NC_V08",   false, smooth, ClearUnderFlow, isSmeared ),
        GetHist(f_hist, "Nominal/KMuNu/Data/" + Cut + "/BDT_NC_V09",   false, smooth, ClearUnderFlow, isSmeared ),


        GetHist(f_hist, "Nominal/KMuNu/Data/" + Cut + "/BDT_V00",   false, smooth, ClearUnderFlow, isSmeared ),
        GetHist(f_hist, "Nominal/KMuNu/Data/" + Cut + "/BDT_V01",   false, smooth, ClearUnderFlow, isSmeared ),
        GetHist(f_hist, "Nominal/KMuNu/Data/" + Cut + "/BDT_V02",   false, smooth, ClearUnderFlow, isSmeared ),
        GetHist(f_hist, "Nominal/KMuNu/Data/" + Cut + "/BDT_V03",   false, smooth, ClearUnderFlow, isSmeared ),
        GetHist(f_hist, "Nominal/KMuNu/Data/" + Cut + "/BDT_V04",   false, smooth, ClearUnderFlow, isSmeared ),
        GetHist(f_hist, "Nominal/KMuNu/Data/" + Cut + "/BDT_V05",   false, smooth, ClearUnderFlow, isSmeared ),
        GetHist(f_hist, "Nominal/KMuNu/Data/" + Cut + "/BDT_V06",   false, smooth, ClearUnderFlow, isSmeared ),
        GetHist(f_hist, "Nominal/KMuNu/Data/" + Cut + "/BDT_V07",   false, smooth, ClearUnderFlow, isSmeared ),
        GetHist(f_hist, "Nominal/KMuNu/Data/" + Cut + "/BDT_V08",   false, smooth, ClearUnderFlow, isSmeared ),
        GetHist(f_hist, "Nominal/KMuNu/Data/" + Cut + "/BDT_V09",   false, smooth, ClearUnderFlow, isSmeared ),
        GetHist(f_hist, "Nominal/KMuNu/Data/" + Cut + "/BDT_V10",   false, smooth, ClearUnderFlow, isSmeared ),
        GetHist(f_hist, "Nominal/KMuNu/Data/" + Cut + "/BDT_V11",   false, smooth, ClearUnderFlow, isSmeared ),
        GetHist(f_hist, "Nominal/KMuNu/Data/" + Cut + "/BDT_V12",   false, smooth, ClearUnderFlow, isSmeared ),
        GetHist(f_hist, "Nominal/KMuNu/Data/" + Cut + "/BDT_V13",   false, smooth, ClearUnderFlow, isSmeared ),
        GetHist(f_hist, "Nominal/KMuNu/Data/" + Cut + "/BDT_V14",   false, smooth, ClearUnderFlow, isSmeared ),


        GetHist(f_hist, "Nominal/KMuNu/Data/" + Cut + "/h_Q2_BEST",        false, smooth, ClearUnderFlow, isSmeared ),
        GetHist(f_hist, "Nominal/KMuNu/Data/" + Cut + "/h_CosTheta_Worst", false, smooth, ClearUnderFlow, isSmeared ),
        GetHist(f_hist, "Nominal/KMuNu/Data/" + Cut + "/h_CosTheta_Best",  false, smooth, ClearUnderFlow, isSmeared ),



    };
  }

  else if ( EventType == "SameSign")
  {
    bool smooth = false;
    bool ClearUnderFlow = true;

    Histograms = {
        GetHist(f_hist, "Nominal/KMuNu/SameSign_PhysSubtr/" + Cut + "/h_Bs_MCORR",       false, smooth, ClearUnderFlow, isSmeared ),
        GetHist(f_hist, "Nominal/KMuNu/SameSign_PhysSubtr/" + Cut + "/h_Bs_MCORR",       false, smooth, ClearUnderFlow, isSmeared ),
        GetHist(f_hist, "Nominal/KMuNu/SameSign_PhysSubtr/" + Cut + "_NoBDT/h_Bs_MCORR", false, smooth, ClearUnderFlow, isSmeared ),
        GetHist(f_hist, "Nominal/KMuNu/SameSign_PhysSubtr/" + Cut + "/h_Bu_MM",          false, smooth, ClearUnderFlow, isSmeared ),
        GetHist(f_hist, "Nominal/KMuNu/SameSign_PhysSubtr/" + Cut + "_NoBDT/h_Bu_MM",    false, smooth, ClearUnderFlow, isSmeared ),
        GetHist(f_hist, "Nominal/KMuNu/SameSign_PhysSubtr/" + Cut + "/MissingMass",      false, smooth, ClearUnderFlow, isSmeared ),
        GetHist(f_hist, "Nominal/KMuNu/SameSign_PhysSubtr/" + Cut + "/h_Bs_PT",      false, smooth, ClearUnderFlow, isSmeared ),
        GetHist(f_hist, "Nominal/KMuNu/SameSign_PhysSubtr/" + Cut + "/h_Bs_PT",      false, smooth, ClearUnderFlow, isSmeared ),
        GetHist(f_hist, "Nominal/KMuNu/SameSign_PhysSubtr/" + Cut + "/h_Bs_PT",      false, smooth, ClearUnderFlow, isSmeared ),
        //GetHist(f_hist, "Nominal/KMuNu/SameSign_PhysSubtr/" + Cut + "/TMVA_CCharged_BDT",      false, smooth, ClearUnderFlow, isSmeared ),

        GetHist(f_hist, "Nominal/KMuNu/SameSign_PhysSubtr/" + Cut + "/BDT_NC_V00",   false, smooth, ClearUnderFlow, isSmeared ),
        GetHist(f_hist, "Nominal/KMuNu/SameSign_PhysSubtr/" + Cut + "/BDT_NC_V01",   false, smooth, ClearUnderFlow, isSmeared ),
        GetHist(f_hist, "Nominal/KMuNu/SameSign_PhysSubtr/" + Cut + "/BDT_NC_V02",   false, smooth, ClearUnderFlow, isSmeared ),
        GetHist(f_hist, "Nominal/KMuNu/SameSign_PhysSubtr/" + Cut + "/BDT_NC_V03",   false, smooth, ClearUnderFlow, isSmeared ),
        GetHist(f_hist, "Nominal/KMuNu/SameSign_PhysSubtr/" + Cut + "/BDT_NC_V04",   false, smooth, ClearUnderFlow, isSmeared ),
        GetHist(f_hist, "Nominal/KMuNu/SameSign_PhysSubtr/" + Cut + "/BDT_NC_V05",   false, smooth, ClearUnderFlow, isSmeared ),
        GetHist(f_hist, "Nominal/KMuNu/SameSign_PhysSubtr/" + Cut + "/BDT_NC_V06",   false, smooth, ClearUnderFlow, isSmeared ),
        GetHist(f_hist, "Nominal/KMuNu/SameSign_PhysSubtr/" + Cut + "/BDT_NC_V07",   false, smooth, ClearUnderFlow, isSmeared ),
        GetHist(f_hist, "Nominal/KMuNu/SameSign_PhysSubtr/" + Cut + "/BDT_NC_V08",   false, smooth, ClearUnderFlow, isSmeared ),
        GetHist(f_hist, "Nominal/KMuNu/SameSign_PhysSubtr/" + Cut + "/BDT_NC_V09",   false, smooth, ClearUnderFlow, isSmeared ),


        GetHist(f_hist, "Nominal/KMuNu/SameSign_PhysSubtr/" + Cut + "/BDT_V00",   false, smooth, ClearUnderFlow, isSmeared ),
        GetHist(f_hist, "Nominal/KMuNu/SameSign_PhysSubtr/" + Cut + "/BDT_V01",   false, smooth, ClearUnderFlow, isSmeared ),
        GetHist(f_hist, "Nominal/KMuNu/SameSign_PhysSubtr/" + Cut + "/BDT_V02",   false, smooth, ClearUnderFlow, isSmeared ),
        GetHist(f_hist, "Nominal/KMuNu/SameSign_PhysSubtr/" + Cut + "/BDT_V03",   false, smooth, ClearUnderFlow, isSmeared ),
        GetHist(f_hist, "Nominal/KMuNu/SameSign_PhysSubtr/" + Cut + "/BDT_V04",   false, smooth, ClearUnderFlow, isSmeared ),
        GetHist(f_hist, "Nominal/KMuNu/SameSign_PhysSubtr/" + Cut + "/BDT_V05",   false, smooth, ClearUnderFlow, isSmeared ),
        GetHist(f_hist, "Nominal/KMuNu/SameSign_PhysSubtr/" + Cut + "/BDT_V06",   false, smooth, ClearUnderFlow, isSmeared ),
        GetHist(f_hist, "Nominal/KMuNu/SameSign_PhysSubtr/" + Cut + "/BDT_V07",   false, smooth, ClearUnderFlow, isSmeared ),
        GetHist(f_hist, "Nominal/KMuNu/SameSign_PhysSubtr/" + Cut + "/BDT_V08",   false, smooth, ClearUnderFlow, isSmeared ),
        GetHist(f_hist, "Nominal/KMuNu/SameSign_PhysSubtr/" + Cut + "/BDT_V09",   false, smooth, ClearUnderFlow, isSmeared ),
        GetHist(f_hist, "Nominal/KMuNu/SameSign_PhysSubtr/" + Cut + "/BDT_V10",   false, smooth, ClearUnderFlow, isSmeared ),
        GetHist(f_hist, "Nominal/KMuNu/SameSign_PhysSubtr/" + Cut + "/BDT_V11",   false, smooth, ClearUnderFlow, isSmeared ),
        GetHist(f_hist, "Nominal/KMuNu/SameSign_PhysSubtr/" + Cut + "/BDT_V12",   false, smooth, ClearUnderFlow, isSmeared ),
        GetHist(f_hist, "Nominal/KMuNu/SameSign_PhysSubtr/" + Cut + "/BDT_V13",   false, smooth, ClearUnderFlow, isSmeared ),
        GetHist(f_hist, "Nominal/KMuNu/SameSign_PhysSubtr/" + Cut + "/BDT_V14",   false, smooth, ClearUnderFlow, isSmeared ),

        GetHist(f_hist, "Nominal/KMuNu/SameSign_PhysSubtr/" + Cut + "/h_Q2_BEST",        false, smooth, ClearUnderFlow, isSmeared ),
        GetHist(f_hist, "Nominal/KMuNu/SameSign_PhysSubtr/" + Cut + "/h_CosTheta_Worst", false, smooth, ClearUnderFlow, isSmeared ),
        GetHist(f_hist, "Nominal/KMuNu/SameSign_PhysSubtr/" + Cut + "/h_CosTheta_Best",  false, smooth, ClearUnderFlow, isSmeared ),

    };
  }

  /*
   * Loader for K Mu Nu Modes
   */


  else
  {

      bool normalise = true;
      bool smooth = false;
      bool ClearUnderFlow = true;

      Histograms = {
          GetHist(f_hist, Systematic + "/KMuNu/"+EventType+"/" + Cut + "/h_Bs_MCORR",       normalise, smooth, ClearUnderFlow, isSmeared ),
          GetHist(f_hist, Systematic + "/KMuNu/"+EventType+"/" + Cut + "/h_Bs_MCORR",       false,     smooth, ClearUnderFlow, isSmeared ),
          GetHist(f_hist, Systematic + "/KMuNu/"+EventType+"/" + Cut + "_NoBDT/h_Bs_MCORR", normalise, smooth, ClearUnderFlow, isSmeared ),
          GetHist(f_hist, Systematic + "/KMuNu/"+EventType+"/" + Cut + "/h_Bu_MM",          false,     smooth, ClearUnderFlow, isSmeared ),
          GetHist(f_hist, Systematic + "/KMuNu/"+EventType+"/" + Cut + "_NoBDT/h_Bu_MM",    false,     smooth, ClearUnderFlow, isSmeared ),
          GetHist(f_hist, Systematic + "/KMuNu/"+EventType+"/" + Cut + "/MissingMass",      true,      smooth, ClearUnderFlow, isSmeared ),
          GetHist(f_hist, Systematic + "/KMuNu/"+EventType+"/" + Cut + "/h_Bs_PT",      true,      smooth, ClearUnderFlow, isSmeared ),
          GetHist(f_hist, Systematic + "/KMuNu/"+EventType+"/" + Cut + "/h_Bs_PT",      true,      smooth, ClearUnderFlow, isSmeared ),
          GetHist(f_hist, Systematic + "/KMuNu/"+EventType+"/" + Cut + "/h_Bs_PT",      true,      smooth, ClearUnderFlow, isSmeared ),
          //GetHist(f_hist, Systematic + "/KMuNu/"+EventType+"/" + Cut + "/TMVA_CCharged_BDT",      true,      smooth, ClearUnderFlow, isSmeared ),

          GetHist(f_hist, Systematic + "/KMuNu/"+EventType+"/" + Cut + "/BDT_NC_V00",   true, smooth, ClearUnderFlow, isSmeared ),
          GetHist(f_hist, Systematic + "/KMuNu/"+EventType+"/" + Cut + "/BDT_NC_V01",   true, smooth, ClearUnderFlow, isSmeared ),
          GetHist(f_hist, Systematic + "/KMuNu/"+EventType+"/" + Cut + "/BDT_NC_V02",   true, smooth, ClearUnderFlow, isSmeared ),
          GetHist(f_hist, Systematic + "/KMuNu/"+EventType+"/" + Cut + "/BDT_NC_V03",   true, smooth, ClearUnderFlow, isSmeared ),
          GetHist(f_hist, Systematic + "/KMuNu/"+EventType+"/" + Cut + "/BDT_NC_V04",   true, smooth, ClearUnderFlow, isSmeared ),
          GetHist(f_hist, Systematic + "/KMuNu/"+EventType+"/" + Cut + "/BDT_NC_V05",   true, smooth, ClearUnderFlow, isSmeared ),
          GetHist(f_hist, Systematic + "/KMuNu/"+EventType+"/" + Cut + "/BDT_NC_V06",   true, smooth, ClearUnderFlow, isSmeared ),
          GetHist(f_hist, Systematic + "/KMuNu/"+EventType+"/" + Cut + "/BDT_NC_V07",   true, smooth, ClearUnderFlow, isSmeared ),
          GetHist(f_hist, Systematic + "/KMuNu/"+EventType+"/" + Cut + "/BDT_NC_V08",   true, smooth, ClearUnderFlow, isSmeared ),
          GetHist(f_hist, Systematic + "/KMuNu/"+EventType+"/" + Cut + "/BDT_NC_V09",   true, smooth, ClearUnderFlow, isSmeared ),

          GetHist(f_hist, Systematic + "/KMuNu/"+EventType+"/" + Cut + "/BDT_V00",   true, smooth, ClearUnderFlow, isSmeared ),
          GetHist(f_hist, Systematic + "/KMuNu/"+EventType+"/" + Cut + "/BDT_V01",   true, smooth, ClearUnderFlow, isSmeared ),
          GetHist(f_hist, Systematic + "/KMuNu/"+EventType+"/" + Cut + "/BDT_V02",   true, smooth, ClearUnderFlow, isSmeared ),
          GetHist(f_hist, Systematic + "/KMuNu/"+EventType+"/" + Cut + "/BDT_V03",   true, smooth, ClearUnderFlow, isSmeared ),
          GetHist(f_hist, Systematic + "/KMuNu/"+EventType+"/" + Cut + "/BDT_V04",   true, smooth, ClearUnderFlow, isSmeared ),
          GetHist(f_hist, Systematic + "/KMuNu/"+EventType+"/" + Cut + "/BDT_V05",   true, smooth, ClearUnderFlow, isSmeared ),
          GetHist(f_hist, Systematic + "/KMuNu/"+EventType+"/" + Cut + "/BDT_V06",   true, smooth, ClearUnderFlow, isSmeared ),
          GetHist(f_hist, Systematic + "/KMuNu/"+EventType+"/" + Cut + "/BDT_V07",   true, smooth, ClearUnderFlow, isSmeared ),
          GetHist(f_hist, Systematic + "/KMuNu/"+EventType+"/" + Cut + "/BDT_V08",   true, smooth, ClearUnderFlow, isSmeared ),
          GetHist(f_hist, Systematic + "/KMuNu/"+EventType+"/" + Cut + "/BDT_V09",   true, smooth, ClearUnderFlow, isSmeared ),
          GetHist(f_hist, Systematic + "/KMuNu/"+EventType+"/" + Cut + "/BDT_V10",   true, smooth, ClearUnderFlow, isSmeared ),
          GetHist(f_hist, Systematic + "/KMuNu/"+EventType+"/" + Cut + "/BDT_V11",   true, smooth, ClearUnderFlow, isSmeared ),
          GetHist(f_hist, Systematic + "/KMuNu/"+EventType+"/" + Cut + "/BDT_V12",   true, smooth, ClearUnderFlow, isSmeared ),
          GetHist(f_hist, Systematic + "/KMuNu/"+EventType+"/" + Cut + "/BDT_V13",   true, smooth, ClearUnderFlow, isSmeared ),
          GetHist(f_hist, Systematic + "/KMuNu/"+EventType+"/" + Cut + "/BDT_V14",   true, smooth, ClearUnderFlow, isSmeared ),

          GetHist(f_hist, Systematic + "/KMuNu/"+EventType+"/" + Cut + "/h_Q2_BEST",        true, smooth, ClearUnderFlow, isSmeared ),
          GetHist(f_hist, Systematic + "/KMuNu/"+EventType+"/" + Cut + "/h_CosTheta_Worst", true, smooth, ClearUnderFlow, isSmeared ),
          GetHist(f_hist, Systematic + "/KMuNu/"+EventType+"/" + Cut + "/h_CosTheta_Best",  true, smooth, ClearUnderFlow, isSmeared ),

      };


    cout << "Data Type: " << Histname << " Not found. Searching for it!" << endl;
  }

  return Histograms;
}

int main(int ac, char* av[])
{

  FitArgParse Arguments;

  Arguments.Init(
      "/eos/lhcb/wg/semileptonic/Bs2KmunuAna/Histogrammer/Output/Histograms.root"
     ,"h_Bs_MCORR"
     ,"%s_Q2"
     ,"Default"
     ,0
     ,"FitToys_Signal.root"
      );

  Arguments.Parse( ac, av);


  string Systematic = "Nominal";

  vector<vector<shared_ptr<TH1F>>> PseudoData      ;//= KMuNuChannel.GetPseudoData();
  vector<map<string,double>>       PseudoDataYields;//= KMuNuChannel.GetPseudoDataYieds();

  TreeBuilder SaveToys(Arguments.m_ToyOutputName, "FitToys");


  for(int toy = -1; toy < Arguments.m_nToys; toy++)
  {

    bool SmearMC = toy>=0;

    TFile* f_hist = TFile::Open(
            //"../Histogrammer/Histograms.root",
        //"../Histogrammer/Histograms.root",
        Arguments.m_ROOTInputName.c_str(),
            "READ");

    std::string SSFileName = Arguments.m_ROOTInputName;
    SSFileName.insert(SSFileName.size()-5, "_SS");

    TFile* f_hist_SS = TFile::Open(
            //"../Histogrammer/Histograms_SS.root",
            SSFileName.c_str(),
            "READ");

    Fitter KMuNuFitter;

    std::string Cut_Low  = TString::Format(Arguments.m_CutName.c_str(), "Low" ).Data();
    std::string Cut_High = TString::Format(Arguments.m_CutName.c_str(), "High").Data();

    cout << Arguments.m_CutName << "  " << Cut_Low << " " << Cut_High << endl;

    vector<ChannelBuilder> KMuNu_Channels{ChannelBuilder(Cut_Low), ChannelBuilder(Cut_High)};

    for( auto & KMuNuChannel: KMuNu_Channels)
    {

        const string& Cut = KMuNuChannel.GetName();


        /*
         *
         * Setup Data Templates
         *
         */
        if ( toy == -1 )
          KMuNuChannel.DataTemplates() = LoadHistograms("Data", f_hist, Cut, "");
        else
          KMuNuChannel.DataTemplates() = PseudoData.at(toy);


        KMuNuChannel.TemplateContainer()["SameSign"]     = TemplateObjects(
            "SameSign",
            LoadHistograms("SameSign", f_hist_SS, Cut, Systematic, SmearMC),
            true);


        /*
         *
         * Setup KMuNu Templates
         *
         */

        KMuNuChannel.TemplateContainer()["KMuNu"]     = TemplateObjects(
            "KMuNu",
            LoadHistograms("KMuNu", f_hist, Cut, Systematic, SmearMC),
            true);

        KMuNuChannel.TemplateContainer()["KstMuNu"]     = TemplateObjects(
            "KstMuNu",
            LoadHistograms("KstMuNu", f_hist, Cut, Systematic, SmearMC),
            false);

        KMuNuChannel.TemplateContainer()["K2stMuNu"]     = TemplateObjects(
            "K2stMuNu",
            LoadHistograms("K2stMuNu", f_hist, Cut, Systematic, SmearMC),
            false);

        KMuNuChannel.TemplateContainer()["Kst1430MuNu"]     = TemplateObjects(
            "Kst1430MuNu",
            LoadHistograms("Kst1430MuNu", f_hist, Cut, Systematic, SmearMC),
            false);

        /*
         *
         * Setup Inclusive Templates
         *
         */


        KMuNuChannel.TemplateContainer()["MC_10010037"]     = TemplateObjects(
            "MC_10010037",
            LoadHistograms("MC_10010037", f_hist, Cut, Systematic, SmearMC),
            false);

        /*
         *
         * Setup JpsiX Templates
         *
         */



        KMuNuChannel.TemplateContainer()["Bu_JpsiK"]     = TemplateObjects(
            "Bu_JpsiK",
            LoadHistograms("Bu_JpsiK", f_hist, Cut, Systematic, SmearMC),
            false);
/*
        KMuNuChannel.TemplateContainer()["Bu_JpsiKst"]     = TemplateObjects(
            "Bu_JpsiKst",
            LoadHistograms("Bu_JpsiKst", f_hist, Cut, Systematic, SmearMC),
            false);

        KMuNuChannel.TemplateContainer()["Bs_JpsiPhi"]     = TemplateObjects(
            "Bs_JpsiPhi",
            LoadHistograms("Bs_JpsiPhi", f_hist, Cut, Systematic, SmearMC),
            false);

*/
        /*
         *
         * Setup Charm Templates
         *
         */


/*
        KMuNuChannel.TemplateContainer()["Bd_DMuNu"]     = TemplateObjects(
            "Bd_DMuNu",
            LoadHistograms("Bd_DMuNu", f_hist, Cut, Systematic, SmearMC),
            false);

        KMuNuChannel.TemplateContainer()["Bs_DsMuNu"]     = TemplateObjects(
            "Bs_DsMuNu",
            LoadHistograms("MC_13774000", f_hist, Cut, Systematic, SmearMC),
            false);

*/
        /*
         *
         * Setup Vub Templates
         *
         */

/*
        KMuNuChannel.TemplateContainer()["Bd_rhomunu"]     = TemplateObjects(
            "Bd_rhomunu",
            LoadHistograms("Bd_rhomunu", f_hist, Cut, Systematic, SmearMC),
            false);
*/

        KMuNuChannel.TemplateContainer()["Bd_pimunu"]     = TemplateObjects(
            "Bd_pimunu",
            LoadHistograms("Bd_pimunu", f_hist, Cut, Systematic, SmearMC),
            false);

        KMuNuChannel.TemplateContainer()["Lb_pMuNu"]     = TemplateObjects(
            "Lb_pMuNu",
            LoadHistograms("Lb_pMuNu_LQCD", f_hist, Cut, Systematic, SmearMC),
            false);






    }
      /**********************************************************************************
       *
       * Add Normalisation factors
       *
       **********************************************************************************/
    for( auto & KMuNuChannel: KMuNu_Channels)
    {

      const string& Cut = KMuNuChannel.GetName();

      double n_data   = KMuNuChannel.DataTemplates().at(0)->Integral();


      // Templates with yields floating



      const bool IsFullFit = Cut.substr(0, 2) == "Is";

      const double K_Eff   = GetEfficiency(KMuNuChannel.TemplateContainer().at("KMuNu").Histograms[1],   "KMuNu/MC_13512010",f_hist);
      //const double Kst_Eff = GetEfficiency(KMuNuChannel.TemplateContainer().at("KstMuNu").Histograms[1], "KMuNu/MC_13512400",f_hist);
      const double f_KmuNu = GetFraction(KMuNuChannel, KMuNu_Channels, "KMuNu");
      KMuNuChannel.TemplateContainer().at("KMuNu")       .AddNormFactor("K_Yield",           n_data*0.1,  0.0,     n_data);
      KMuNuChannel.TemplateContainer().at("KMuNu")       .AddNormFactor("f_K_"+Cut,           0.5,     0,1.0);

      const double f_KstmuNu = GetFraction(KMuNuChannel, KMuNu_Channels, "KstMuNu");
      KMuNuChannel.TemplateContainer().at("KstMuNu")     .AddNormFactor("K_Yield",           n_data*0.1,    0.0,           n_data);
      KMuNuChannel.TemplateContainer().at("KstMuNu")     .AddNormFactor("f_Kst_"+Cut, f_KstmuNu,    0, 1);// f_KstmuNu,     f_KstmuNu);
      KMuNuChannel.TemplateContainer().at("KstMuNu")     .AddNormFactor("KstoverK", 2,1,3);



      const double f_2KstmuNu = GetFraction(KMuNuChannel, KMuNu_Channels, "K2stMuNu");
      KMuNuChannel.TemplateContainer().at("K2stMuNu")    .AddNormFactor("K2st_Yield",        n_data*0.05,    0.0,           n_data);
      KMuNuChannel.TemplateContainer().at("K2stMuNu")    .AddNormFactor("f_K2st"+Cut,            f_2KstmuNu,  f_2KstmuNu, f_2KstmuNu);

      const double f_Kst1430muNu = GetFraction(KMuNuChannel, KMuNu_Channels, "Kst1430MuNu");
      KMuNuChannel.TemplateContainer().at("Kst1430MuNu") .AddNormFactor("Kst1430_Yield",     n_data*0.05,    0.0,           n_data);
      KMuNuChannel.TemplateContainer().at("Kst1430MuNu") .AddNormFactor("f_Kst1430"+Cut,         f_Kst1430muNu, f_Kst1430muNu, f_Kst1430muNu);

      const double f_Inc = GetFraction(KMuNuChannel, KMuNu_Channels, "MC_10010037");
      KMuNuChannel.TemplateContainer().at("MC_10010037") .AddNormFactor("Inclusive_Yield"+Cut,   n_data*0.1,  0.0,     n_data);
      //KMuNuChannel.TemplateContainer().at("MC_10010037") .AddNormFactor("f_Inclusive"+Cut,       f_Inc,       0,   1);

      KMuNuChannel.TemplateContainer().at("SameSign")    .AddNormFactor("SS_Yield"+Cut,           1.0,  1.0,     1.0);

      // Templates constrained to the J/Psi K


      double Bu_JpsiK_Yield;
      std::tie(Bu_JpsiK_Yield, std::ignore) = FitJpsiK(KMuNuChannel.DataTemplates().at(4).get(), "Data_Bu_MM"+Cut+".pdf");
      const double Bu_JpsiK_BDT_Eff   = GetBDTEfficiency(KMuNuChannel.TemplateContainer().at("Bu_JpsiK").Histograms, true, "Bu_MC_Fit.pdf");
      const double TotalJpsiKYield = Bu_JpsiK_Yield * Bu_JpsiK_BDT_Eff;

      /*
      const double JpsiK_Eff = GetEfficiency(KMuNuChannel.TemplateContainer().at("Bu_JpsiK").Histograms[1], "KMuNu/MC_12143001",f_hist);
      KMuNuChannel.TemplateContainer().at("Bu_JpsiK")    .AddNormFactor("JPsiK_Yield"+Cut,       TotalJpsiKYield,      TotalJpsiKYield*0.5,         TotalJpsiKYield*1.5);
      KMuNuChannel.TemplateContainer().at("Bu_JpsiK")    .AddNormFactor("JpsiK_BF",          1.0,                 1.0,                    1.0  );

      const double JpsiKst_Eff = GetEfficiency(KMuNuChannel.TemplateContainer().at("Bu_JpsiKst").Histograms[1], "KMuNu/MC_12143401",f_hist);
      KMuNuChannel.TemplateContainer().at("Bu_JpsiKst")  .AddNormFactor("JPsiK_Yield"+Cut,       TotalJpsiKYield,       TotalJpsiKYield*0.5,       TotalJpsiKYield*1.5);
      KMuNuChannel.TemplateContainer().at("Bu_JpsiKst")  .AddNormFactor("JpsiKst_BF",        1.39,                  1.39,                  1.39  );
      KMuNuChannel.TemplateContainer().at("Bu_JpsiKst")  .AddNormFactor("JpsiKst_Eff",       JpsiKst_Eff/JpsiK_Eff, JpsiKst_Eff/JpsiK_Eff, JpsiKst_Eff/JpsiK_Eff  );

      const double JpsiPhi_Eff = GetEfficiency(KMuNuChannel.TemplateContainer().at("Bs_JpsiPhi").Histograms[1], "KMuNu/MC_13144001", f_hist);
      KMuNuChannel.TemplateContainer().at("Bs_JpsiPhi")  .AddNormFactor("JPsiK_Yield"+Cut,     TotalJpsiKYield,       TotalJpsiKYield*0.5,       TotalJpsiKYield*1.5);
      KMuNuChannel.TemplateContainer().at("Bs_JpsiPhi")  .AddNormFactor("fsOverfd",          0.267,                 0.267,                 0.267      );
      KMuNuChannel.TemplateContainer().at("Bs_JpsiPhi")  .AddNormFactor("JPsiPhi_BF",        1.05,                  1.05,                  1.05      );
      KMuNuChannel.TemplateContainer().at("Bs_JpsiPhi")  .AddNormFactor("JpsiPhi_Eff",       JpsiPhi_Eff/JpsiK_Eff, JpsiPhi_Eff/JpsiK_Eff, JpsiPhi_Eff/JpsiK_Eff  );

      const double DsMuNu_Eff = GetEfficiency(KMuNuChannel.TemplateContainer().at("Bs_DsMuNu").Histograms[1], "KMuNu/MC_13774000", f_hist);
      KMuNuChannel.TemplateContainer().at("Bs_DsMuNu")  .AddNormFactor("JPsiK_Yield"+Cut,        TotalJpsiKYield,       TotalJpsiKYield*0.5,       TotalJpsiKYield*1.5);
      KMuNuChannel.TemplateContainer().at("Bs_DsMuNu")  .AddNormFactor("DsMuNu_BF",          30,                   30,                    30);
      KMuNuChannel.TemplateContainer().at("Bs_DsMuNu")  .AddNormFactor("DsMuNu_KKPi_BF",     0.0545,               0.0545,                0.0545);
      KMuNuChannel.TemplateContainer().at("Bs_DsMuNu")  .AddNormFactor("DsMuNu_fCocktail",   0.228,                0.228,                 0.228);
      KMuNuChannel.TemplateContainer().at("Bs_DsMuNu")  .AddNormFactor("DsMuNu_Eff",         DsMuNu_Eff/JpsiK_Eff, DsMuNu_Eff/JpsiK_Eff, DsMuNu_Eff/JpsiK_Eff  );


      const double DMuNu_Eff = GetEfficiency(KMuNuChannel.TemplateContainer().at("Bd_DMuNu").Histograms[1], "KMuNu/MC_11874042", f_hist);
      KMuNuChannel.TemplateContainer().at("Bd_DMuNu")  .AddNormFactor("JPsiK_Yield"+Cut,       TotalJpsiKYield,       TotalJpsiKYield*0.5,       TotalJpsiKYield*1.5);
      KMuNuChannel.TemplateContainer().at("Bd_DMuNu")  .AddNormFactor("DMuNu_BF",          10.03,                 10.03,                 10.03);
      KMuNuChannel.TemplateContainer().at("Bd_DMuNu")  .AddNormFactor("DMuNu_KPiPi_BF",    0.0898,                0.0898,                0.0898);
      KMuNuChannel.TemplateContainer().at("Bd_DMuNu")  .AddNormFactor("DMuNu_fCocktail",   0.4384,                0.4384,                0.4384);
      KMuNuChannel.TemplateContainer().at("Bd_DMuNu")  .AddNormFactor("DMuNu_Eff",         DMuNu_Eff/JpsiK_Eff,   DMuNu_Eff/JpsiK_Eff,   DMuNu_Eff/JpsiK_Eff  );

      const double rhomunu_Eff = GetEfficiency(KMuNuChannel.TemplateContainer().at("Bd_rhomunu").Histograms[1], "KMuNu/MC_11512400",f_hist);
      KMuNuChannel.TemplateContainer().at("Bd_rhomunu") .AddNormFactor("K_Yield",      n_data*0.1,        0.0,               n_data);
      KMuNuChannel.TemplateContainer().at("Bd_rhomunu") .AddNormFactor("rho_rel_eff",  rhomunu_Eff/K_Eff, rhomunu_Eff/K_Eff, rhomunu_Eff/K_Eff);
*/

      const double pimunu_Eff = GetEfficiency(KMuNuChannel.TemplateContainer().at("Bd_pimunu").Histograms[1], "KMuNu/MC_11512011",f_hist);
      KMuNuChannel.TemplateContainer().at("Bd_pimunu")  .AddNormFactor("K_Yield",         n_data*0.1,        0.0,              n_data);
      KMuNuChannel.TemplateContainer().at("Bd_pimunu")  .AddNormFactor("pimunu_rel_eff",  pimunu_Eff/K_Eff,  pimunu_Eff/K_Eff, pimunu_Eff/K_Eff);
      KMuNuChannel.TemplateContainer().at("Bd_pimunu")  .AddNormFactor("BdOverBs",   2,2,2);

      const double pmunu_Eff = GetEfficiency(KMuNuChannel.TemplateContainer().at("Lb_pMuNu").Histograms[1], "KMuNu/MC_15512013",f_hist);
      KMuNuChannel.TemplateContainer().at("Lb_pMuNu")   .AddNormFactor("K_Yield",      n_data*0.1,  0.0,     n_data);
      KMuNuChannel.TemplateContainer().at("Lb_pMuNu")   .AddNormFactor("pmunu_rel_eff",  pmunu_Eff/K_Eff,  pmunu_Eff/K_Eff, pmunu_Eff/K_Eff);
      KMuNuChannel.TemplateContainer().at("Lb_pMuNu")   .AddNormFactor("LboverBs",   2,2,2);


      /**********************************************************************************
       *
       * Setup for plotting
       *
       **********************************************************************************/

      KMuNuChannel.CombinedTemplates()["B_{s} #rightarrow K^{-} #mu #nu"]             = TemplateCombiner( "0_KMuNu",      KMuNuChannel.TemplateContainer().at("KMuNu") );
      KMuNuChannel.CombinedTemplates()["B_{s} #rightarrow K^{-}* #mu #nu"]            = TemplateCombiner( "1_KstMuNu",    KMuNuChannel.TemplateContainer().at("KstMuNu") );
      KMuNuChannel.CombinedTemplates()["B_{s} #rightarrow K^{-}*(1430) #mu #nu"]      = TemplateCombiner( "2_Kst1430MuNu",KMuNuChannel.TemplateContainer().at("Kst1430MuNu") );
      KMuNuChannel.CombinedTemplates()["B_{s} #rightarrow K^{-2}* #mu #nu"]           = TemplateCombiner( "3_K2stMuNu",   KMuNuChannel.TemplateContainer().at("K2stMuNu") );


      KMuNuChannel.CombinedTemplates()["Inclusive K + #mu"]                           = TemplateCombiner( "Inclusive",  KMuNuChannel.TemplateContainer().at("MC_10010037") );

      //KMuNuChannel.CombinedTemplates()["B^{+} #rightarrow J/#psi K^{+}"]              = TemplateCombiner( "JpsiK",      KMuNuChannel.TemplateContainer().at("Bu_JpsiK") );
      //KMuNuChannel.CombinedTemplates()["B^{+} #rightarrow J/#psi K^{+}*"]             = TemplateCombiner( "JpsiKst",    KMuNuChannel.TemplateContainer().at("Bu_JpsiKst") );
      //KMuNuChannel.CombinedTemplates()["B^{+} #rightarrow J/#psi #phi"]               = TemplateCombiner( "Bs_JpsiPhi", KMuNuChannel.TemplateContainer().at("Bs_JpsiPhi") );

      //KMuNuChannel.CombinedTemplates()["B_{s} #rightarrow D_{s} #mu #nu"]             = TemplateCombiner( "Bs_DsMuNu",  KMuNuChannel.TemplateContainer().at("Bs_DsMuNu") );
      //KMuNuChannel.CombinedTemplates()["B^{0} #rightarrow D^{+} #mu #nu"]             = TemplateCombiner( "B0_DpMuNu",  KMuNuChannel.TemplateContainer().at("Bd_DMuNu") );
      KMuNuChannel.CombinedTemplates()["Same Sign"]                                     = TemplateCombiner( "SameSign",   KMuNuChannel.TemplateContainer().at("SameSign") );
      KMuNuChannel.CombinedTemplates()["Vub MisID"]                                     = TemplateCombiner( "VubMisID",   /*KMuNuChannel.TemplateContainer().at("Bd_rhomunu"),*/   KMuNuChannel.TemplateContainer().at("Bd_pimunu"),   KMuNuChannel.TemplateContainer().at("Lb_pMuNu") );
      //KMuNuChannel.CombinedTemplates()["B^{0} #rightarrow #rho #mu #nu"]              = TemplateCombiner( "B0_rhoMuNu", KMuNuChannel.TemplateContainer().at("Bd_rhomunu") );
      //KMuNuChannel.CombinedTemplates()["B^{0} #rightarrow #pi #mu #nu"]               = TemplateCombiner( "B0_piMuNu",  KMuNuChannel.TemplateContainer().at("Bd_pimunu") );
      //KMuNuChannel.CombinedTemplates()["#Lambda_{b} #rightarrow p #mu #nu"]           = TemplateCombiner( "Bs_DsMuNu",  KMuNuChannel.TemplateContainer().at("Lb_pMuNu") );


      //cout << TotalJpsiKYield << endl;
      KMuNuFitter.AddChannel(KMuNuChannel);
    }


    KMuNuFitter.Fit("K_Yield", map<string,string>{{"f_K_" + (string)Cut_Low, "f_K_" + (string)Cut_High},/* {"f_InclusiveLow_Q2" + cut, "f_InclusiveHigh_Q2" + cut},*/ {"f_Kst_" + (string)Cut_Low, "f_Kst_" + (string)Cut_High}});
    //exit(0);
    for( auto KMuNuChannel : KMuNu_Channels)
    {
      if (toy == -1)
      {

        //PrintBeforeAfter(KMuNuChannel.TemplateContainer());

        PrintFitPlot(KMuNuChannel.DataTemplates().at(0), KMuNuChannel.CombinedTemplates(), true, 0, KMuNuChannel.GetName()+"FitPlot.pdf",      ";m_{Corr}( B_{s} ) [MeV];",    false, false, 0.11, 0.59, HistSort::Alpha);
        PrintFitPlot(KMuNuChannel.DataTemplates().at(4), KMuNuChannel.CombinedTemplates(), true, 3, KMuNuChannel.GetName()+"FitPlot_4.pdf",    ";m_{Corr}( B_{s} ) [MeV];",    false, false, 0.11, 0.59);
        PrintFitPlot(KMuNuChannel.DataTemplates().at(5), KMuNuChannel.CombinedTemplates(), true, 5, KMuNuChannel.GetName()+"FitPlot_5.pdf",    ";Missing Mass^{2} [MeV^{2}];", false, false, 0.6, 0.59);
        PrintFitPlot(KMuNuChannel.DataTemplates().at(6), KMuNuChannel.CombinedTemplates(), true, 6, KMuNuChannel.GetName()+"FitPlot_6.pdf",    ";p_{T} B_{s}^{0} [MeV];",      false, false, 0.6, 0.59);
        PrintFitPlot(KMuNuChannel.DataTemplates().at(7), KMuNuChannel.CombinedTemplates(), true, 7, KMuNuChannel.GetName()+"FitPlot_7.pdf",    ";TMVA_charge_BDT;",            false, false, 0.6, 0.59);
        PrintFitPlot(KMuNuChannel.DataTemplates().at(34), KMuNuChannel.CombinedTemplates(), true, 34, KMuNuChannel.GetName()+"FitPlot_8.pdf",  ";Best q^{2};",                 false, false, 0.6, 0.59, HistSort::Alpha);
        PrintFitPlot(KMuNuChannel.DataTemplates().at(35), KMuNuChannel.CombinedTemplates(), true, 35, KMuNuChannel.GetName()+"FitPlot_9.pdf",  ";cos( #theta ) Worst;",        false, false, 0.6, 0.59, HistSort::Alpha);
        PrintFitPlot(KMuNuChannel.DataTemplates().at(36), KMuNuChannel.CombinedTemplates(), true, 36, KMuNuChannel.GetName()+"FitPlot_10.pdf", ";cos( #theta ) Best;",         false, false, 0.6, 0.59, HistSort::Alpha);




        TCanvas* cx = new TCanvas("cx", "cx", 1600, 1600);
        cx->Print((KMuNuChannel.GetName()+"_Validation_Neutral.pdf[").c_str());
        PrintFitPlot(KMuNuChannel.DataTemplates().at( 9), KMuNuChannel.CombinedTemplates(), true,  9, KMuNuChannel.GetName()+"_Validation_Neutral.pdf", ";kaon_m_PT                          ;", false, false, 0.59, 0.59);
        PrintFitPlot(KMuNuChannel.DataTemplates().at(10), KMuNuChannel.CombinedTemplates(), true, 10, KMuNuChannel.GetName()+"_Validation_Neutral.pdf", ";Bs_PT                              ;", false, false, 0.59, 0.59);
        PrintFitPlot(KMuNuChannel.DataTemplates().at(11), KMuNuChannel.CombinedTemplates(), true, 11, KMuNuChannel.GetName()+"_Validation_Neutral.pdf", ";Bs_PT-muon_p_PT*1.5                ;", false, false, 0.11, 0.59);
        PrintFitPlot(KMuNuChannel.DataTemplates().at(12), KMuNuChannel.CombinedTemplates(), true, 12, KMuNuChannel.GetName()+"_Validation_Neutral.pdf", ";Bs_cosTheta1_star-Bs_cosTheta2_star;", false, false, 0.11, 0.59);
        PrintFitPlot(KMuNuChannel.DataTemplates().at(13), KMuNuChannel.CombinedTemplates(), true, 13, KMuNuChannel.GetName()+"_Validation_Neutral.pdf", ";Bs_DIRA_OWNPV                      ;", false, false, 0.11, 0.59);
        PrintFitPlot(KMuNuChannel.DataTemplates().at(14), KMuNuChannel.CombinedTemplates(), true, 14, KMuNuChannel.GetName()+"_Validation_Neutral.pdf", ";Bs_FD_S                            ;", false, false, 0.59, 0.59);
        PrintFitPlot(KMuNuChannel.DataTemplates().at(15), KMuNuChannel.CombinedTemplates(), true, 15, KMuNuChannel.GetName()+"_Validation_Neutral.pdf", ";Bs_ENDVERTEX_CHI2                  ;", false, false, 0.59, 0.59);
        PrintFitPlot(KMuNuChannel.DataTemplates().at(16), KMuNuChannel.CombinedTemplates(), true, 16, KMuNuChannel.GetName()+"_Validation_Neutral.pdf", ";kaon_m_MasshPi0                    ;", false, false, 0.11, 0.59);
        PrintFitPlot(KMuNuChannel.DataTemplates().at(17), KMuNuChannel.CombinedTemplates(), true, 17, KMuNuChannel.GetName()+"_Validation_Neutral.pdf", ";kaon_m_1.00_nc_asy_PT              ;", false, false, 0.11, 0.59);
        PrintFitPlot(KMuNuChannel.DataTemplates().at(18), KMuNuChannel.CombinedTemplates(), true, 18, KMuNuChannel.GetName()+"_Validation_Neutral.pdf", ";kaon_m_0.50_nc_IT                  ;", false, false, 0.11, 0.59);
        cx->Print((KMuNuChannel.GetName()+"_Validation_Neutral.pdf]").c_str());



        cx->Print((KMuNuChannel.GetName()+"_Validation_Charged.pdf[").c_str());
        PrintFitPlot(KMuNuChannel.DataTemplates().at(19), KMuNuChannel.CombinedTemplates(), true, 19, KMuNuChannel.GetName()+"_Validation_Charged.pdf", ";Bs_ENDVERTEX_CHI2                        ;", false, false, 0.11, 0.59);
        PrintFitPlot(KMuNuChannel.DataTemplates().at(20), KMuNuChannel.CombinedTemplates(), true, 20, KMuNuChannel.GetName()+"_Validation_Charged.pdf", ";kaon_m_PT                                ;", false, false, 0.60, 0.59);
        PrintFitPlot(KMuNuChannel.DataTemplates().at(21), KMuNuChannel.CombinedTemplates(), true, 21, KMuNuChannel.GetName()+"_Validation_Charged.pdf", ";Bs_PT                                    ;", false, false, 0.60, 0.59);
        PrintFitPlot(KMuNuChannel.DataTemplates().at(22), KMuNuChannel.CombinedTemplates(), true, 22, KMuNuChannel.GetName()+"_Validation_Charged.pdf", ";Bs_PT-muon_p_PT*1.5                      ;", false, false, 0.11, 0.59);
        PrintFitPlot(KMuNuChannel.DataTemplates().at(23), KMuNuChannel.CombinedTemplates(), true, 23, KMuNuChannel.GetName()+"_Validation_Charged.pdf", ";muon_p_0.50_cc_asy_P                     ;", false, false, 0.11, 0.59);
        PrintFitPlot(KMuNuChannel.DataTemplates().at(24), KMuNuChannel.CombinedTemplates(), true, 24, KMuNuChannel.GetName()+"_Validation_Charged.pdf", ";kaon_m_0.50_IT                           ;", false, false, 0.60, 0.59);
        PrintFitPlot(KMuNuChannel.DataTemplates().at(25), KMuNuChannel.CombinedTemplates(), true, 25, KMuNuChannel.GetName()+"_Validation_Charged.pdf", ";kaon_m_0.50_cc_deltaEta                  ;", false, false, 0.60, 0.59);
        PrintFitPlot(KMuNuChannel.DataTemplates().at(26), KMuNuChannel.CombinedTemplates(), true, 26, KMuNuChannel.GetName()+"_Validation_Charged.pdf", ";kaon_m_0.50_cc_IT                        ;", false, false, 0.11, 0.59);
        PrintFitPlot(KMuNuChannel.DataTemplates().at(27), KMuNuChannel.CombinedTemplates(), true, 27, KMuNuChannel.GetName()+"_Validation_Charged.pdf", ";min(muon_p_ConeIso, kaon_m_ConeIso)      ;", false, false, 0.11, 0.59);
        PrintFitPlot(KMuNuChannel.DataTemplates().at(28), KMuNuChannel.CombinedTemplates(), true, 28, KMuNuChannel.GetName()+"_Validation_Charged.pdf", ";max(kaon_m_IsoSumBDT, muon_p_IsoSumBDT)  ;", false, false, 0.11, 0.59);
        PrintFitPlot(KMuNuChannel.DataTemplates().at(29), KMuNuChannel.CombinedTemplates(), true, 29, KMuNuChannel.GetName()+"_Validation_Charged.pdf", ";min(kaon_m_IsoMinBDT, muon_p_IsoMinBDT)  ;", false, false, 0.11, 0.59);
        PrintFitPlot(KMuNuChannel.DataTemplates().at(30), KMuNuChannel.CombinedTemplates(), true, 30, KMuNuChannel.GetName()+"_Validation_Charged.pdf", ";kaon_m_IsoMinBDT - muon_p_IsoMinBDT      ;", false, false, 0.60, 0.59);
        PrintFitPlot(KMuNuChannel.DataTemplates().at(31), KMuNuChannel.CombinedTemplates(), true, 31, KMuNuChannel.GetName()+"_Validation_Charged.pdf", ";muon_p_PAIR_M                            ;", false, false, 0.11, 0.59);
        PrintFitPlot(KMuNuChannel.DataTemplates().at(32), KMuNuChannel.CombinedTemplates(), true, 32, KMuNuChannel.GetName()+"_Validation_Charged.pdf", ";kaon_m_PAIR_M                            ;", false, false, 0.60, 0.59);
        PrintFitPlot(KMuNuChannel.DataTemplates().at(33), KMuNuChannel.CombinedTemplates(), true, 33, KMuNuChannel.GetName()+"_Validation_Charged.pdf", ";Bs_cosTheta1_star-Bs_cosTheta2_star      ;", false, false, 0.60, 0.59);
        cx->Print((KMuNuChannel.GetName()+"_Validation_Charged.pdf]").c_str());

      }
      map<string,double> results = KMuNuChannel.GetResults();

      // To save correlations edit this line
      //results["corr_Ds_DsStar"]  = KMuNuChannel.GetFitResults()->correlation("Ds_Yield", "DsStar_Yield");


      if ( toy == -1 )
      {

        std::ofstream OutputFile;
        OutputFile.open ("KMuNuYields_" + KMuNuChannel.GetName() + ".txt");

        OutputFile << KMuNuChannel.CombinedTemplates()["B_{s} #rightarrow K^{-} #mu #nu"].GetYield();
        OutputFile <<"  ";
        OutputFile << KMuNuChannel.CombinedTemplates()["B_{s} #rightarrow K^{-} #mu #nu"].GetError();

        OutputFile.close();

        KMuNuChannel.BuildPseudoData(Arguments.m_nToys);
        PseudoData      = KMuNuChannel.GetPseudoData();
        PseudoDataYields= KMuNuChannel.GetPseudoDataYieds();
      }


      if ( toy >= 0 )
      {
          for( auto& Input:PseudoDataYields.at(toy))
          {
              string name = Input.first;
              double InputYield = Input.second;
              double FitYield   = results[name];
              double FitErr     = results[name+"_err"];

              results[name + "_In"]   = InputYield;
              results[name + "_res"]  = FitYield - InputYield;
              results[name + "_pull"] = (FitYield - InputYield)/FitErr;

          }
      }

      if ( toy == 0 )
      {
          SaveToys.init(results);
      }

      if ( toy >= 0 )
      {
         SaveToys.AddEntry(results);
      }
    }

  }

  SaveToys.Close();

  return EXIT_SUCCESS;
    //return results;
}
