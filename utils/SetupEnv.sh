#!/bin/bash
# LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/afs/cern.ch/sw/lcg/contrib/gcc/4.9.3/x86_64-slc6-gcc49-opt/lib64:/afs/cern.ch/sw/lcg/releases/LCG_84/ROOT/6.06.02/x86_64-slc6-gcc49-opt/lib:`pwd`/Tools/lib/:`pwd`Constraints/lib/:`pwd`/Fitter/lib/

OLDOLDPWD=$OLDPWD

if [ -n "$USER" ]
then
  echo "hello $USER"
else
  export USER=nobody
fi
if [ -n "$HOME" ]
then
  echo "feel at home"
else
  export HOME=`pwd`
fi

SOURCE=${BASH_ARGV[0]}
if [ "x$SOURCE" = "x" ]; then
  SOURCE=${(%):-%N} # for zsh
fi

utilsdir=$(cd "$(dirname "${SOURCE}")"; pwd)
export BSTOKMUNUROOT=$(dirname $utilsdir)
export BSTOKMUNUEOS=/eos/lhcb/wg/semileptonic/Bs2KmunuAna/

echo "Settining up LHCb version:

"
source /cvmfs/lhcb.cern.ch/lib/lhcb/LBSCRIPTS/prod/InstallArea/scripts/LbLogin.sh -c x86_64-slc6-gcc49-opt

echo "

Setting up ROOT Version 6.06.02

"
#source /cvmfs/lhcb.cern.ch/lib/lhcb/LBSCRIPTS/prod/InstallArea/scripts/SetupProject.sh ROOT 6.06.02
source /cvmfs/lhcb.cern.ch/lib/lhcb/LBSCRIPTS/prod/InstallArea/scripts/SetupProject.sh Urania v5r0
echo "

Setting Up Library Paths:

"
# make array BSTOKMUNULIBRARIES read only
# http://stackoverflow.com/a/10887691
# for loop written for compatibility with most shells
# http://stackoverflow.com/a/30270126 (and comment below)
declare -a BSTOKMUNULIBRARIES
BSTOKMUNULIBRARIES=(
"$BSTOKMUNUROOT/lib/"
"$utilsdir/CommonTools/"
"$utilsdir/Fitter_Control/Tools/lib/"
"$utilsdir/Fitter_Control/Constraints/lib/"
"$utilsdir/Fitter_Control/Fitter/lib/"
"/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_84/gcc/4.9.3/x86_64-slc6/lib64"
"/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_84/ROOT/6.06.02/x86_64-slc6-gcc49-opt/lib"
)
declare -r BSTOKMUNULIBRARIES



# determine if `set -e` has been applied (to figure out if it should be reenabled
# 
# this echo will not close the shell: either e is set (then grep succeeds) or
#                                     e is not set (then grep fails, but the shell doesn't quit on failure)
echo $- | \grep e
if [ $? -ne 0 ]
then
  RESETE=false
else
  RESETE=true
fi

# set +e suggested here
# http://stackoverflow.com/questions/39466770/gitlab-ci-scripts-during-which-is-allowed-to-be-non-zero
set +e
for LIBRARY in "${BSTOKMUNULIBRARIES[@]}"
do
  echo $LD_LIBRARY_PATH | \grep -v $LIBRARY > /dev/null
  if [ $? -ne 0 ]
  then
    echo "found $LIBRARY"
  else
    echo Adding $LIBRARY
    LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$LIBRARY
  fi
done

PATH=$BSTOKMUNUROOT/bin/:$PATH
PYTHONPATH=$BSTOKMUNUROOT/utils/CommonTools:$PYTHONPATH

if [ "$RESETE" = true ]
then
  set -e
fi

export LCGENV_PATH=/cvmfs/sft.cern.ch/lcg/releases
TMPPYTHONOVERHEAD=$(mktemp --suffix=.sh)
/cvmfs/sft.cern.ch/lcg/releases/lcgenv/latest/lcgenv -p LCG_85swan2 --ignore Grid --ignore ROOT x86_64-slc6-gcc49-opt root_numpy > ${TMPPYTHONOVERHEAD}
source ${TMPPYTHONOVERHEAD}


export PYTHONPATH=`pwd`:$PYTHONPATH

OLDPWD=$OLDOLDPWD
