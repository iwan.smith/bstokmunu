#!/usr/bin/env python 
# Mika Vesterinen
# script to take z parameterisation of FFs for Bs->K/Ds 
# and make plots of the kinematic distributions for Bs->K/Ds mu nu.
import numpy as np
from array import array
import Utils


def GetFFIntegral(Mode, Q2Cut):

    NTOYS=250
    
    # an array of toys 
    # these are the top level "data" for the study
    Toys = []
    for i in range(NTOYS):
        Toys.append({"FFparams"          :np.zeros(0),
                     "DecayRateVersusq2" :np.zeros(0),
                      "q2Values"         :np.zeros(0)})
        
    
    # get the form factor parameters and generate toys
    # from the error matrix for the three "b" parameters 
    
    if Mode == "DsMuNu":
        import FormFactorsBailey2012
    
        q2Bins = np.linspace(0.0,11,250)
    
        
        b0,b1,b2 = np.random.multivariate_normal(FormFactorsBailey2012.CentralValues(), 
                                                 FormFactorsBailey2012.ErrorMatrix(), 
                                                 NTOYS).T
        for iToy in range(len(Toys)):
            Toys[iToy]["FFparams"] = np.asarray( [b0[iToy],b1[iToy],b2[iToy]] )
        # the first toy uses the default params
        Toys[0]["FFparams"] = np.asarray( FormFactorsBailey2012.CentralValues() ) 
        
        ## main loop over toys
        for Toy in Toys:
            for q2 in q2Bins:
                PicoSeconds = 1.e-12
                Prefactor = PicoSeconds**-1 * Utils.getVub()**-2
                fVector = FormFactorsBailey2012.fVector(q2,Toy["FFparams"])
                Mb   = 5.366 # b mass                                                                              
                Ml   = 0.105 # lepton mass                                                                        
                Mp   = 1.968 # recoil mass (Ds meson in this case)
                Toy["DecayRateVersusq2"] = np.append( Toy["DecayRateVersusq2"], Prefactor * Utils.Rate(q2,fVector,Mb,Mp,Ml) ) 
                Toy["q2Values"]          = np.append( Toy["q2Values"]         , q2 )
        
                #print q2, Prefactor * Utils.Rate(q2,fVector,Mb,Mp,Ml)
    elif Mode == "KMuNu":
    
        import FormFactorsFlynn2015
        
        q2Bins = np.linspace(0.0,25,250)
    
        
        b0,b1,b2 = np.random.multivariate_normal(FormFactorsFlynn2015.CentralValues(), 
                                                 FormFactorsFlynn2015.ErrorMatrix(), 
                                                 NTOYS).T
        for iToy in range(len(Toys)):
            Toys[iToy]["FFparams"] = [b0[iToy],b1[iToy],b2[iToy]]
        # the first toy uses the default params
        Toys[0]["FFparams"] = FormFactorsFlynn2015.CentralValues() 
        
        ## main loop over toys
        for Toy in Toys:
            Toy["DecayRateVersusq2"] = []
            Toy["q2Values"] = []
            for q2 in q2Bins:
                PicoSeconds = 1.e-12
                Prefactor = PicoSeconds**-1 * Utils.getVub()**-2
                fVector = FormFactorsFlynn2015.fVector(q2,Toy["FFparams"])
                Mb   = 5.366 # b mass                                                                          
                Ml   = 0.105 # lepton mass                                                                     
                Mp   = 0.498 # recoil mass
                Toy["DecayRateVersusq2"] = np.append( Toy["DecayRateVersusq2"], Prefactor * Utils.Rate(q2,fVector,Mb,Mp,Ml) ) 
                Toy["q2Values"]          = np.append( Toy["q2Values"]         , q2 )
        
    
    
    ToysLow = []
    ToysHigh = []
    
    # find dividing bin:
    ix = np.abs(q2Bins - Q2Cut).argmin()
    
    for Toy in Toys:
        ToysLow.append({
            "FFparams"         :Toy["FFparams"][:ix],
            "DecayRateVersusq2":Toy["DecayRateVersusq2"][:ix],
            "q2Values"         :Toy["q2Values"][:ix]})
        
        ToysHigh.append({
            "FFparams"         :Toy["FFparams"][ix:],
            "DecayRateVersusq2":Toy["DecayRateVersusq2"][ix:],
            "q2Values"         :Toy["q2Values"][ix:]})
    
    
    Q2Low = []
    Q2High = []
    
    Q2BinSize = q2Bins[1] - q2Bins[0]
    
    for Toy in ToysLow:
        Q2Low.append( np.sum(  Toy["DecayRateVersusq2"]  ) * Q2BinSize  )
        
    for Toy in ToysHigh:
        Q2High.append( np.sum(  Toy["DecayRateVersusq2"]  ) * Q2BinSize )
    
    
    # print "Q2Low:", np.mean(Q2Low), "+/-", np.std(Q2Low)
    # print "Q2High:", np.mean(Q2High), "+/-", np.std(Q2High)
    
    return (np.mean(Q2Low), np.std(Q2Low) ), (np.mean(Q2High), np.std(Q2High) )

if __name__ == "__main__":
    
    print GetFFIntegral("DsMuNu", 5)
    print GetFFIntegral("KMuNu", 5)

