import ROOT
import numpy as np
from FormFactors import *

nPoint = 1000
Q2Low  = 0.00
Q2High = (5.36689 - 0.493677)**2
nToys  = 1000


Mode = "Rate"
q2Bins = np.linspace(Q2Low,Q2High,nPoint)

f_FF = ROOT.TFile.Open("FF_K.root", "RECREATE")


"""
Bouchard Form Factors
"""

ff_Bouch = FormFactors()

ff_Bouch.SetMean    ( 0.368,    -0.750,     2.72    )
ff_Bouch.SetErr     ( 0.0214,    0.193,     1.458   )
ff_Bouch.SetCov   ([[ 0.0004577, 0.001157, -0.00131 ],
                    [ 0.001157,  0.03721,   0.1858  ],
                    [ -0.001309, 0.1858,    2.124   ]])

ff_Bouch.ConstructToys(nToys)

fPlot_Bouch = FormFactorPlotter(ff_Bouch)
if Mode == "Vector":
    fPlot_Bouch.SetYLabel("f_{+}^{B_{s} K}")
Objects_Bouch = fPlot_Bouch._PlotterObjects(q2Bins, Mode )


V_Bouch, V_E_L_Bouch, V_E_H_Bouch = ff_Bouch.GetRate(q2Bins)
V_E_Bouch = (V_E_L_Bouch + V_E_H_Bouch)/2

V2_Bouch = ff_Bouch._fVector(q2Bins)

"""
Witzel Form Factors
"""



ff_Witzel = FormFactors()

ff_Witzel.SetMean     ( 0.338, -1.161, -0.458 )
ff_Witzel.SetErr      ( 0.024,  0.192,  1.009 )
ff_Witzel.SetCorr   ([[ 1.0,    0.255,  0.146 ],
                      [ 0.255,  1.0,    0.823 ],
                      [ 0.146,  0.823,  1.0   ]])

ff_Witzel.ConstructToys(nToys)

fPlot_Witzel = FormFactorPlotter(ff_Witzel, (ROOT.kRed, 0.1), ROOT.kRed)
if Mode == "Vector":
    fPlot_Witzel.SetYLabel("f_{+}^{B_{s} K}")
Objects_Witzel = fPlot_Witzel._PlotterObjects(q2Bins, Mode)


V_Witz, V_E_L_Witz, V_E_H_Witz = ff_Witzel.GetVector(q2Bins)
V_E_Witz = (V_E_L_Witz + V_E_H_Witz)/2

V2_Witz = ff_Witzel._fVector(q2Bins)


"""
Rusov Form Factors
"""


ff_Rusov = FormFactors()

ff_Rusov.SetMean     ( 0.336, -2.53 )
ff_Rusov.SetErr      ( 0.023,  1.17 )
ff_Rusov.SetCorr   ([[ 1.00,   0.79 ],
                     [ 0.79,   1.00 ]])

ff_Rusov.ConstructToys(nToys)

fPlot_Rusov = FormFactorPlotter(ff_Rusov, (ROOT.kGreen, 0.1), ROOT.kGreen)
if Mode == "Vector":
    fPlot_Rusov.SetYLabel("f_{+}^{B_{s} K}")
Objects_Rusov = fPlot_Rusov._PlotterObjects(q2Bins, Mode)

V_Rusov, V_E_L_Rusov, V_E_H_Rusov = ff_Rusov.GetVector(q2Bins)
V_E_Rusov = (V_E_L_Rusov + V_E_H_Rusov)/2

V2_Rusov = ff_Rusov._fVector(q2Bins)

"""
# Calculate the Weighted Average
"""

"""
if ( Mode == "Vector"):
    V_Avg   = ( V_Rusov/V_E_Rusov**2 + V_Bouch/V_E_Bouch**2 + V_Witz/V_E_Witz**2 ) / ( 1.0/V_E_Rusov**2 + 1.0/V_E_Bouch**2 + 1.0/V_E_Witz**2 )
    V_E_Avg = 1 / np.sqrt(1.0/V_E_Rusov**2 + 1.0/V_E_Bouch**2 + 1.0/V_E_Witz**2)
if ( Mode == "Rate"):
    V = ( V2_Bouch + V2_Rusov + V2_Witz ) / 3
    V_Avg, V_E_Avg_L, V_E_Avg_H = ff_Rusov.GetRate(q2Bins, V)
    V_E_Avg = ( V_E_Avg_H + V_E_Avg_L)/2


V_Avg_Plot = []
grBand = ROOT.TGraphAsymmErrors(nPoint,
                                q2Bins          .astype(np.float64),
                                V_Avg           .astype(np.float64),
                                np.zeros(nPoint).astype(np.float64),
                                np.zeros(nPoint).astype(np.float64),
                                V_E_Avg          .astype(np.float64),
                                V_E_Avg         .astype(np.float64)
                                  )


grBand.SetFillColorAlpha( 1, 0.1 )
grBand.SetLineColor( 1 )
ROOT.SetOwnership(grBand,False)

V_Avg_Plot += [grBand]

for var in [0.,-V_E_Avg,V_E_Avg]:
    grVar = ROOT.TGraph(nPoint,
                        q2Bins   .astype(np.float64),
                        (V_Avg + var).astype(np.float64)
                       )
    grVar.SetLineColor(1)
    grVar.SetLineWidth(1)
    ROOT.SetOwnership(grVar,False)
    V_Avg_Plot.append(grVar)
"""

"""
Now do the plotting
"""

Leg = ROOT.TLegend(0.65, 0.65, 0.93, 0.92, "", "NDC")
Leg.AddEntry(Objects_Rusov[0],  "Khodjamirian and Rusov", "f")
Leg.AddEntry(Objects_Bouch[0],  "Bouchard et.al.", "f")
Leg.AddEntry(Objects_Witzel[0], "Flynn et.al.", "f")


c = ROOT.TCanvas("c1", "c1", 1600, 1200)
#Objects_Rusov[0].SetMinimum(0)
#Objects_Rusov[0].SetMaximum(0.9)

Objects_Rusov[0].SetMaximum(7e-19)
Objects_Rusov[0].Draw("AL4")
Objects_Rusov[2].Draw("SAME L")
Objects_Rusov[3].Draw("SAME L")

Objects_Bouch[0].Draw("SAME L4")
Objects_Bouch[2].Draw("SAME L")
Objects_Bouch[3].Draw("SAME L")


Objects_Witzel[0].Draw("SAME L4")
Objects_Witzel[2].Draw("SAME L")
Objects_Witzel[3].Draw("SAME L")

#V_Avg_Plot[0].Draw("SAME L4")
#V_Avg_Plot[2].Draw("SAME L")
#V_Avg_Plot[3].Draw("SAME L")

#Leg.AddEntry(V_Avg_Plot[0], "Weighted Mean.", "f")

box = ROOT.TBox(0, 0, 12, 7e-19)
box.SetFillColorAlpha(1, 0.1)

NoDataTxt = ROOT.TPaveText(4, 5.5e-19, 12, 6.5e-19)
NoDataTxt.AddText("No LQCD Data")
NoDataTxt.SetFillColorAlpha(0, 0.0)
NoDataTxt.SetTextColor(1)
NoDataTxt.SetBorderSize(0)


c.RedrawAxis()

Leg.Draw()
NoDataTxt.Draw()
box.Draw()

c.Print("dGamma_dq2_Band_K.pdf")


f_FF.cd()
Objects_Bouch[0]  .SetTitle("FF_Bouchard")  
Objects_Witzel[0] .SetTitle("FF_Witzel")
Objects_Rusov[0]  .SetTitle("FF_Rusov")

Objects_Bouch[0]  .SetName("FF_Bouchard")  
Objects_Witzel[0] .SetName("FF_Witzel")
Objects_Rusov[0]  .SetName("FF_Rusov")

Objects_Bouch[0]  .Write()
Objects_Witzel[0] .Write()
Objects_Rusov[0]  .Write()

f_FF.Close()