import ROOT
import numpy as np
from FormFactors import *


"""
Bouchard Form Factors
"""
def FF_Bouchard(q2Low, q2High, nToys=1000, nPoint=1000):
    
    q2Bins = np.linspace(q2Low,q2High,nPoint)

    FF = FormFactors()
    
    FF.SetMean    ( 0.368,    -0.750,     2.72    )
    FF.SetErr     ( 0.0214,    0.193,     1.458   )
    FF.SetCov   ([[ 0.0004577, 0.001157, -0.00131 ],
                        [ 0.001157,  0.03721,   0.1858  ],
                        [ -0.001309, 0.1858,    2.124   ]])
    
    FF.ConstructToys(nToys)
    
    return FF.GetRateIntegral(q2Bins)

"""
Witzel Form Factors
"""


def FF_Witzel(q2Low, q2High, nToys=1000, nPoint=1000):

    q2Bins = np.linspace(q2Low,q2High,nPoint)

    FF = FormFactors()
    
    FF.SetMean     ( 0.338, -1.161, -0.458 )
    FF.SetErr      ( 0.024,  0.192,  1.009 )
    FF.SetCorr   ([[ 1.0,    0.255,  0.146 ],
                      [ 0.255,  1.0,    0.823 ],
                      [ 0.146,  0.823,  1.0   ]])
    
    FF.ConstructToys(nToys)
    
    return FF.GetRateIntegral(q2Bins)
    

def FF_WitzelBouch(q2Low, q2High, nToys=1000, nPoint=1000):
    
    FFWitzel = FF_Witzel  (q2Low, q2High, nToys, nPoint)
    FFBouch  = FF_Bouchard(q2Low, q2High, nToys, nPoint)
    
    val = (FFWitzel[0] + FFBouch[0])/2.0
    err = val * (FFWitzel[1]+FFBouch[1])/(FFWitzel[0]+FFBouch[1]) 
    # A 100% correlation is assumed in the errors on the two form factor models
    
    return val, err

"""
Rusov Form Factors
"""
def FF_Rusov(q2Low, q2High, nToys=1000, nPoint=1000):
    
    q2Bins = np.linspace(q2Low,q2High,nPoint)

    FF = FormFactors()
    
    FF.SetMean     ( 0.336, -2.53 )
    FF.SetErr      ( 0.023,  1.17 )
    FF.SetCorr   ([[ 1.00,   0.79 ],
                         [ 0.79,   1.00 ]])
    
    FF.ConstructToys(nToys)
    
    return FF.GetRateIntegral(q2Bins)

"""
Bailey Form Factors
"""


def FF_Bailey(q2Low, q2High, nToys=1000, nPoint=1000):
    
    q2Bins = np.linspace(q2Low,q2High,nPoint)

    FF = FormFactors("DsMuNu",mB = 5.36689, ml = 0.105, mP = 1.968, mBst = 5.325)
    
    FF.SetMean    ( 0.01081,-0.066,0.18   )
    FF.SetErr     ( 0.00004,0.002,0.06   )
    FF.SetCorr   ([[ 1.0,-0.05,-0.054],
                          [-0.05,1.0,-0.233],
                          [-0.054,-0.233,1.0]])
    
    FF.ConstructToys(nToys)
    
    return FF.GetRateIntegral(q2Bins)
