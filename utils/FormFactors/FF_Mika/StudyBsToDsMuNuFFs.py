#!/usr/bin/env python 
# Mika Vesterinen
# script to take z parameterisation of FFs for Bs->K/Ds 
# and make plots of the kinematic distributions for Bs->K/Ds mu nu.
import sys
print sys.argv[0]
from ROOT import gROOT
gROOT.SetBatch(True)
import os, Utils,math, ROOT, numpy as np
from array import array
ROOT.gStyle.SetTitleSize(0.06,"X")
ROOT.gStyle.SetTitleSize(0.06,"Y")

f_out = ROOT.TFile.Open("DsMuNu_Out.root", "RECREATE")

plotPrefix = "StudyBsToDsMuNuFFs__"

rand = ROOT.TRandom3(1)

q2Bins = np.linspace(0.0,11,250)

NTOYS=250

# an array of toys 
# these are the top level "data" for the study
Toys = []
for i in range(NTOYS):
    Toys.append({"FFparams":[],
                 "DecayRateVersusq2":[],
                  "q2Values":[]})
    

# get the form factor parameters and generate toys
# from the error matrix for the three "b" parameters 
import FormFactorsBailey2012
b0,b1,b2 = np.random.multivariate_normal(FormFactorsBailey2012.CentralValues(), 
                                         FormFactorsBailey2012.ErrorMatrix(), 
                                         NTOYS).T
for iToy in range(len(Toys)):
    Toys[iToy]["FFparams"] = [b0[iToy],b1[iToy],b2[iToy]]
# the first toy uses the default params
Toys[0]["FFparams"] = FormFactorsBailey2012.CentralValues() 

## main loop over toys
for Toy in Toys:
    Toy["DecayRateVersusq2"] = []
    Toy["q2Values"] = []
    for q2 in q2Bins:
        PicoSeconds = 1.e-12
        Prefactor = PicoSeconds**-1 * Utils.getVub()**-2
        fVector = FormFactorsBailey2012.fVector(q2,Toy["FFparams"])
        Mb   = 5.366 # b mass                                                                              
        Ml   = 0.105 # lepton mass                                                                        
        Mp   = 1.968 # recoil mass (Ds meson in this case)
        Toy["DecayRateVersusq2"].append(Prefactor * Utils.Rate(q2,fVector,Mb,Mp,Ml)) 
        Toy["q2Values"].append(q2)

# plotting starts here
c = ROOT.TCanvas()

# make a graph of the "default" dGamma/dg2
gr = ROOT.TGraph(len(Toys[0]["q2Values"]),
                 array('f',Toys[0]["q2Values"]),
                 array('f',Toys[0]["DecayRateVersusq2"]))
Utils.PrettifyGraph(gr,"q^{2} [GeV^{2}]","|V_{ub}|^{-2} d#Gamma/dq^{2} [s^{-1}GeV^{-2}]")
gr.Draw("ALP")
gr.SetName("Graph_Q2")
gr.Write()


c.Print(plotPrefix+"dGamma_dq2.png")

# make a graph with an error band
# first make an array (versus q2) with the RMS over the toys
RMS = []
for iq2 in range(len(q2Bins)):
    a = [Toy["DecayRateVersusq2"][iq2] for Toy in Toys]
    rms = np.std(a,ddof=1)
    RMS.append(rms)
grBand = ROOT.TGraphErrors(len(Toys[0]["q2Values"]),
                           array('f',Toys[0]["q2Values"]),
                           array('f',Toys[0]["DecayRateVersusq2"]),
                           array('f',[0]*len(Toys[0]["q2Values"])),
                           array('f',RMS))
# then make the plot
grBand.SetName("Graph_Q2_Err")
grBand.Write()
grBand.Draw("AL4")
grBand.SetFillColor(ROOT.kAzure-9)
Utils.PrettifyGraph(grBand,"q^{2} [GeV^{2}]","|V_{ub}|^{-2} d#Gamma/dq^{2} [s^{-1}GeV^{-2}]")
for variation in [0.,-1.,1.]:
    grVar = ROOT.TGraph(len(Toys[0]["q2Values"]),
                        array('f',Toys[0]["q2Values"]),
                        array('f',[Toys[0]["DecayRateVersusq2"][iQ2]+variation*RMS[iQ2] for iQ2 in range(len(q2Bins))]))
    grVar.SetLineColor(ROOT.kAzure)
    grVar.SetLineWidth(1)
    grVar.Draw("SAME L")
    ROOT.SetOwnership(grVar,False)
grBand.GetYaxis().Draw("SAME")
c.RedrawAxis()
c.Print(plotPrefix+"dGamma_dq2_Band.png")

# then make a plot of the error on the integral for given cut
LATuncert = []
Q2cuts = np.linspace(0.2,11.,50)
for lower_q2_cut in Q2cuts:
    integrals = []
    for Toy in Toys:
        dGdq2 = [Toy["DecayRateVersusq2"][iQ2] for iQ2 in range(len(q2Bins))]
        integral = 0.0
        dq2 = q2Bins[1]-q2Bins[0] # assumes that we used uniform bin sizes
        for i in range(len(q2Bins)):
            if q2Bins[i] < lower_q2_cut: continue
            integral += dGdq2[i]*dq2
        integrals.append(integral)
    LATuncert.append(100.*np.std(integrals,ddof=1)/np.mean(integrals))

gr = ROOT.TGraph(len(Q2cuts),
                 array('f',Q2cuts),
                 array('f',LATuncert))
Utils.PrettifyGraph(gr,"Lower q^{2} cut [GeV^{2}]","Rel. err. on  #int |Vub|^{-2} dq^{2}d#Gamma/dq^{2} [%]")
gr.Draw("ALP")
gr.SetName("Graph_Q2_Cut")
gr.Write()
c.Print(plotPrefix+"FormFactorError_versus_lower_q2_cut.png")


f_out.Close()
