
import ROOT
import numpy as np
from numpy import sqrt
from array import array
import math
from ROOT import (TFile,TTree,TLorentzVector,TH1F,TCanvas,THStack,TLegend,TH2F,TLatex,TProfile,TVector3,TGraph)

GF   = 1.664e-5
Vub  = 3.65e-3 #exclusive http://www.slac.stanford.edu/xorg/hfag/semi/summer16/html/ExclusiveVub/exclPilnu.html



def PrettifyGraph(gr,xtitle = "" ,ytitle = ""):
    """
    Make the graph nicer:
        Set markers, colours, center titles
        Optionally set axis labels
    """
    for ax in [gr.GetXaxis(),gr.GetYaxis()]:
        ax.CenterTitle()
    gr.SetMarkerStyle(8)
    gr.SetMarkerColor(ROOT.kBlue)
    
    if xtitle != "":
        gr.GetXaxis().SetTitle(xtitle)
        
    if ytitle != "":
        gr.GetYaxis().SetTitle(ytitle)

def DrawBand(Xvalues,Yvalues,xtitle,ytitle,outfile):
    c = ROOT.TCanvas()
    RMS = [sqrt(np.mean(np.square(Yvalues[i]-np.mean(Yvalues[i])))) for i in range(0,len(Xvalues))]
    gr1 = ROOT.TGraphErrors(len(Xvalues),array('f',Xvalues),array('f',[np.mean(Yvalues[i]) for i in range(len(Xvalues))]),
                            array('f',[0.]*len(Xvalues)),array('f',RMS))
    gr0 = ROOT.TGraph(len(Xvalues),array('f',Xvalues),array('f',[np.mean(Yvalues[i]) for i in range(len(Xvalues))]))
    grUp = ROOT.TGraph(len(Xvalues),array('f',Xvalues),array('f',[np.mean(Yvalues[i])+RMS[i] for i in range(len(Xvalues))]))
    grDn = ROOT.TGraph(len(Xvalues),array('f',Xvalues),array('f',[np.mean(Yvalues[i])-RMS[i] for i in range(len(Xvalues))]))
    
    gr1.Draw("AL4")
    gr0.Draw("SAME L")
    gr1.GetYaxis().Draw("SAME")
    gr1.SetFillColor(ROOT.kAzure-9)
    for gr in [grUp,grDn,gr0]:
        gr.SetLineWidth(1)
        gr.SetLineColor(ROOT.kAzure)
        gr.Draw("SAME L")
    gr1.GetXaxis().SetTitle(xtitle) 
    gr1.GetYaxis().SetTitle(ytitle) 
    PrettifyGraph(gr1)
    c.RedrawAxis()
    c.Print(outfile)
    


# methods start here...

# access to constants...
def getVub():
    return Vub

def getGFermi():
    return GF

def Rate(q2,fVector,Mb,Mp,Ml):
    #Prefactor = GF**2*Vub**2/(24.*math.pi**3)
    prefactor = ( GF**2 / (192 * math.pi**3 * Mb**3) )
    R = 1e-5 * prefactor * ( ( Mb**2 + Mp**2 - q2)**2 - 4. * Mb**2 * Mp**2 )**(3./2.) * fVector**2 
    return max(0.,R)

