# Mika Vesterinen
# contains the FF data
# Bailey et al., 1202.6346v2 (2012)

import math
from numpy import sqrt
Mb   = 5.366
ml   = 0.105
Mp   = 1.968 
Mbst = 5.325

mean = [0.01081,-0.066,0.18]
errors = [0.00004,0.002,0.06]
cor = [[1.0,-0.05,-0.054],
       [-0.05,1.0,-0.233],
       [-0.054,-0.233,1.0]]
cov = [[0.,0.,0.],
       [0.,0.,0.],
       [0.,0.,0.]]
for i in range(3):
    for j in range(3):
        cov[i][j] = errors[i]*errors[j]*cor[i][j]

for i in range(3):
    for j in range(3):
        cov[i][j] = errors[i]*errors[j]*cor[i][j]

def CentralValues():
    return mean

def ErrorMatrix():
    return cov

def z(q2):
    t0 = (Mb+Mp)*(math.sqrt(Mb)-math.sqrt(Mp))**2
    tp = (Mb + Mp)**2
    tm = (Mb - Mp)**2
    return (math.sqrt(1-q2/tp) - math.sqrt(1-t0/tp))/(math.sqrt(1-q2/tp) + math.sqrt(1-t0/tp))


def fVector(q2,FFparams):
    Blaschke = 1.0
    r = Mp/Mb
    phi = 1.1213 * (1+z(q2))*(1-z(q2))**0.5 * ((1+r)*(1-z(q2)) + 2*sqrt(r)*(1+z(q2)))**-5
    N = 3
    f = (Blaschke*phi)**-1
    sum = 0.0
    for n in range(N):
        sum += FFparams[n]*z(q2)**n
    return f*sum
