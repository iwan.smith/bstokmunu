# Mika Vesterinen
# contains the FF data
# Flynn et al., 1501.05373v3 (2015)
import math
# fixed parameters for Bs->Kmunu
Mb   = 5.366 # b mass                                                                                             
ml   = 0.105 # lepton mass                                                                                        
Mp   = 0.4937 # recoil mass   

# best fit parameters from the paper
"""
# Paper
Mbst = 5.325
mean = [0.338,-1.161,-0.458]
errors = [0.024,0.192,1.009]
cor = [[1.0,0.255,0.146],
       [0.255,1.0,0.823],
       [0.146,0.823,1.0]]
cov = [[0.,0.,0.],
       [0.,0.,0.],
       [0.,0.,0.]]


for i in range(3):
    for j in range(3):
        cov[i][j] = errors[i]*errors[j]*cor[i][j]
"""
"""

#Bouchard
mean = [0.368,-0.750,2.72]
errors = [0.0214,0.193,1.458]
cor = [[1.0,0.255,0.146],
       [0.255,1.0,0.823],
       [0.146,0.823,1.0]]
cov = [[0.0004577, 0.001157,  -0.001309],
       [0.001157,  0.03721,    0.1858  ],
       [-0.001309, 0.1858,     2.124]]

"""
"""
def CentralValues():
    return mean

def ErrorMatrix():
    return cov

# kinematic methods...                                                                                            
# transformation from q2 to z
def z(q2):
    t0 = (Mb+Mp)*(math.sqrt(Mb)-math.sqrt(Mp))**2
    tp = (Mb + Mp)**2
    tm = (Mb - Mp)**2
    return (math.sqrt(1-q2/tp) - math.sqrt(1-t0/tp))/(math.sqrt(1-q2/tp) + math.sqrt(1-t0/tp))

# vector form factor                                                                                              
def fVector(q2,FFparams):
    K = 3
    PF = (1-(q2/Mbst**2))**-1
    SecondFactor = 0.0
    for k in range(K):
        SecondFactor += (FFparams[k])*(z(q2)**k - (-1.)**(k-K) * (k/K)*z(q2)**K)
    return PF*SecondFactor


# best fit parameters from the paper
"""
""" QCD from Rosinov
Mbst = 5.325
mean = [0.336, -2.53, 0]
errors = [0.023, 1.17, 0]
cor = [[1.0,0.79, 0],
       [0.79,1.0, 0],
       [0,0,1]]


mean = [0.4696, -0.73, 0.39]
errors = [0.023, 1.17, 0]
cor = [[1.0,0.79, 0],
       [0.79,1.0, 0],
       [0,0,1]]

cov = [[0.,0.,0.0],
       [0.,0.,0.0],
       [0., 0., 0.]]

for i in range(2):
    for j in range(2):
        cov[i][j] = errors[i]*errors[j]*cor[i][j]

def CentralValues():
    return mean

def ErrorMatrix():
    return cov

# kinematic methods...                                                                                            
# transformation from q2 to z
def z(q2):
    t0 = (Mb+Mp)*(math.sqrt(Mb)-math.sqrt(Mp))**2
    tp = (Mb + Mp)**2
    tm = (Mb - Mp)**2
    return (math.sqrt(1-q2/tp) - math.sqrt(1-t0/tp))/(math.sqrt(1-q2/tp) + math.sqrt(1-t0/tp))

# vector form factor                                                                                              
def fVector(q2,FFparams):
        
    return FFparams[0]/(1-(q2/Mbst**2)) *  ( 1 + FFparams[1]*(z(q2) - z(0) + 0.5*(z(q2)**2 - z(0)**2)) )


"""



#Oliver V2



#FLAG

Mbst = 5.325
mean =   [0.360,-0.828,-1.11]
errors = [0.014, 0.083, 0.55]
cor =   [[1.0,   0.098,-0.216],
         [0.098, 1.0,   0.459],
        [-0.216, 0.459, 1.0  ]]
cov = [[0.,0.,0.],
       [0.,0.,0.],
       [0.,0.,0.]]
"""
mean =   [0.421,-0.35,-0.41]
errors = [0.,0.,0.]
cor =   [[1.0,   0.098,-0.216],
         [0.098, 1.0,   0.459],
        [-0.216, 0.459, 1.0  ]]
cov = [[0.,0.,0.],
       [0.,0.,0.],
       [0.,0.,0.]]
"""
for i in range(2):
    for j in range(2):
        cov[i][j] = errors[i]*errors[j]*cor[i][j]


def CentralValues():
    return mean

def ErrorMatrix():
    return cov

# kinematic methods...                                                                                            
# transformation from q2 to z
def z(q2):
    t0 = (Mb+Mp)*(math.sqrt(Mb)-math.sqrt(Mp))**2
    tp = (Mb + Mp)**2
    tm = (Mb - Mp)**2
    return (math.sqrt(1-q2/tp) - math.sqrt(1-t0/tp))/(math.sqrt(1-q2/tp) + math.sqrt(1-t0/tp))

# vector form factor                                                                                              
def fVector(q2,FFparams):
    K = 3
    PF = (1-(q2/Mbst**2))**-1
    SecondFactor = 0.0
    for k in range(K):
        SecondFactor += (FFparams[k])*(z(q2)**k)
    return PF*SecondFactor

