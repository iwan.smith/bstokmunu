import ROOT
import numpy as np
from FormFactors import *

nPoint = 1000
Q2Low  = 0.00
Q2High = (5.36689 - 1.968)**2
nToys  = 1000


Mode = "Rate"
q2Bins = np.linspace(Q2Low,Q2High,nPoint)


f_FF = ROOT.TFile.Open("FF_Ds.root", "RECREATE")

"""
Bailey Form Factors
"""

ff_Bailey = FormFactors("DsMuNu",mB = 5.36689, ml = 0.105, mP = 1.968, mBst = 5.325)

ff_Bailey.SetMean    ( 0.01081,-0.066,0.18   )
ff_Bailey.SetErr     ( 0.00004,0.002,0.06   )
ff_Bailey.SetCorr   ([[ 1.0,-0.05,-0.054],
                      [-0.05,1.0,-0.233],
                      [-0.054,-0.233,1.0]])

ff_Bailey.ConstructToys(nToys)
fPlot_Bailey = FormFactorPlotter(ff_Bailey)

fPlot_Bailey.SetYLabel("|V_{cb}|^{-2} d#Gamma/dq^{2} [ps^{-1}GeV^{-2}]")
if Mode == "Vector":
    fPlot_Bailey.SetYLabel("f_{+}^{B_{s} D_{s}}")

Objects_Bailey = fPlot_Bailey._PlotterObjects(q2Bins, Mode )


V_Bailey, V_E_L_Bailey, V_E_H_Bailey = ff_Bailey.GetRate(q2Bins)
V_E_Bailey = (V_E_L_Bailey + V_E_H_Bailey)/2

V2_Bailey = ff_Bailey._fVector(q2Bins)


"""
Now do the plotting
"""
if ( Mode == "Rate"):
    Leg = ROOT.TLegend(0.7, 0.65, 0.9, 0.75, "", "NDC")
if ( Mode == "Vector"):
    Leg = ROOT.TLegend(0.7, 0.25, 0.9, 0.35, "", "NDC")

Leg.AddEntry(Objects_Bailey[0],  "Bailey  et.al.", "f")


c = ROOT.TCanvas("c1", "c1", 1600, 1200)

#Objects_Bailey[0].SetMaximum(7e-18)
#Objects_Bailey[0].SetMinimum(0)
Objects_Bailey[0].Draw("SAME AL4")
Objects_Bailey[2].Draw("SAME L")
Objects_Bailey[3].Draw("SAME L")


box = ROOT.TBox(0, 0, 12, 7e-18)
box.SetFillColorAlpha(1, 0.1)

NoDataTxt = ROOT.TPaveText(4, 5.5e-18, 12, 6.5e-18)
NoDataTxt.AddText("No LQCD Data")
NoDataTxt.SetFillColor(0)
NoDataTxt.SetTextColor(1)
NoDataTxt.SetBorderSize(0)


c.RedrawAxis()

Leg.Draw()

c.Print("dGamma_dq2_Band_Ds.pdf")

f_FF.cd()
Objects_Bailey[0].SetName("FF_Bailey")
Objects_Bailey[0].SetTitle("FF_Bailey")
Objects_Bailey[0].Write()
f_FF.Close()
