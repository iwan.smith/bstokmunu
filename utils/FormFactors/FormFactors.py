# Mika Vesterinen and Iwan Smith
#

__all__ = ["FormFactors", "FormFactorPlotter"]

import math
import numpy as np

import sys
from ROOT import gROOT
import os,math, ROOT, numpy as np
from array import array

GF   = 1.664e-5
Vub = 1.0##Vub  = 3.65e-3 #exclusive http://www.slac.stanford.edu/xorg/hfag/semi/summer16/html/ExclusiveVub/exclPilnu.html


"""
Helper Functions below here
"""


def asymmstd(a, axis=None, ddof=0,):
    """
    
    Returns the assymetrical standard deviation of an array
    
    """
    """
    mu = np.mean(a, axis=axis)
    
    delta = a - mu
    
    deltaHigh = np.copy(delta)
    deltaHigh[delta<0] = 0
        
    deltaLow = np.copy(delta)
    deltaLow[delta>0] = 0

    std_High = np.sqrt(np.mean(deltaHigh**2, axis=axis))
    std_Low  = np.sqrt(np.mean(deltaLow **2, axis=axis))
    """
    return np.std(a, axis), np.std(a, axis) 
    
    
    
    
    





class FormFactors:
    """
    Class to manage form factor calculations.
    
    """
    def __init__(self, Decay = "KMuNu", mB = 5.36689, ml = 0.105, mP = 0.493677, mBst = 5.325):
        """
        Constructor.
        Set the mass of the B, lepton, recoil, hadron, and excited B
        
        """
        self.Decay = Decay
        
        self.mB   = mB # b mass                                                                                             
        self.ml   = ml # lepton mass                                                                                        
        self.mP   = mP # recoil mass   
        self.mBst = mBst
    
        self.Mean       = np.zeros(3)
        self.Errors     = np.zeros(3)
        self.Covariance = np.zeros((3,3))

        self.nToys = 0
        self.t_Mean     = np.zeros((1,3))
                
        self.  Rate       = None
        self.e_Rate_L     = None
        self.e_Rate_H     = None
        self.FFVector     = None
        self.e_FFVector_L = None
        self.e_FFVector_H = None
        
        
    def SetMean(self, *args):
        """
        Set the values of the BCL fit
        """

        self.nPar = len(args)


        self.Mean  = np.asarray(args)
        self.t_Mean = self.Mean.reshape((1,self.nPar))
    
    def SetErr(self, *args):
        """
        Set the errors from the BCL fit
        """
        self.Errors = np.asarray(args)
    
    def SetCorr(self, Correlation):
        """
        Set covariance matrix using the correlation matrix and errors
        """
        self.Covariance = self.Errors * self.Errors.reshape(self.nPar, 1) * Correlation
        
    def SetCov(self, Covariance):
        """
        Set the covariance matrix
        """
        self.Covariance = Covariance
    
    def ConstructToys(self, nToys):
        self.nToys = nToys
        try:
            toys = np.random.multivariate_normal(self.Mean, self.Covariance,nToys, check_valid="ignore")
        except:
            toys = np.random.multivariate_normal(self.Mean, self.Covariance,nToys)

        self.t_Mean = np.concatenate((self.t_Mean, toys))


    def GetCentralValues(self):
        return self.Mean
    
    def GetErrorMatrix(self):
        return self.Covariance
    
    
    def _z(self, q2):
        """
        calculate the z parameterisation from the qsq
        """
        t0 = ( self.mB + self.mP )*( np.sqrt(self.mB) - np.sqrt(self.mP) )**2
        tp = ( self.mB + self.mP )**2
        tm = ( self.mB - self.mP )**2
        with np.errstate(invalid='ignore'):
            return ( np.sqrt(1-q2/tp) - np.sqrt(1-t0/tp) )/( np.sqrt(1-q2/tp) + np.sqrt(1-t0/tp) )

    # vector form factor                                                                                              
    def _fVector(self, q2):
        """
        Calculate the vector form factors given a qsq
        """
        zPar = self._z(q2)
        z0   = self._z(0)
        K = self.nPar

        if ( self.Decay == "KMuNu"):
            
            if self.nPar == 2:
                
                b0 = self.t_Mean[:,0].reshape((self.nToys+1, 1))
                b1 = self.t_Mean[:,1].reshape((self.nToys+1, 1))
    
                return b0/(1-(q2/self.mBst**2)) *  ( 1 + b1*(zPar - z0 + 0.5*(zPar**2 - z0**2)) )
            
            elif self.nPar == 3:
                PF = (1-(q2/self.mBst**2))**-1
                
                SecondFactor = np.zeros((self.nToys+1,q2.shape[0]))
                
                for k in range(3):
                    bN = self.t_Mean[:,k]
                    bN = bN.reshape((self.nToys+1, 1))
                    SecondFactor += bN * (zPar**k - (-1.)**(k-K) * (k/K)*zPar**K)
                
                return PF*SecondFactor
            
        elif ( self.Decay == "DsMuNu"):
            Blaschke = 1.0
            r = self.mP/self.mB
            phi = 1.1213 * np.power((1+zPar)*(1-zPar), 0.5) * np.power((1+r)*(1-zPar) + 2*np.sqrt(r)*(1+zPar), -5)
            N = 3
            f = (Blaschke*phi)**-1
            sum = 0.0
            for n in range(N):
                sum += self.t_Mean[:,n].reshape((self.nToys+1, 1))*np.power(zPar, n)
            return f*sum


    def GetRate(self, q2, Vector = None):
        """
        Calculate the differential Branching Fraction, and return the error
        """
        if Vector is None:
            Vector = self._fVector(q2)
        
        
        prefactor = ( GF**2 / (192 * math.pi**3 * self.mB**3) )
        self.Rate = 0.12/1.8*1e-5 * prefactor * ( ( self.mB**2 + self.mP**2 - q2)**2 - 4. * self.mB**2 * self.mP**2 )**(3./2.) * Vector**2 
        
            
        self.Rate[ np.isnan(self.Rate) ] = 0
        if self.Decay == "DsMuNu":
            self.Rate *= 0.3725
        self.e_Rate_L, self.e_Rate_H = asymmstd( self.Rate[1:], axis = 0)
        
        return self.Rate[0], self.e_Rate_L, self.e_Rate_H

    def GetRateIntegral(self, q2, Vector = None):
        """
        Calculate the differential Branching Fraction, and return the error
        """
        if Vector is None:
            Vector = self._fVector(q2)
        
        dQ2 = (q2[-1] - q2[0]) / q2.shape[0]
        
        prefactor = ( GF**2 / (192 * math.pi**3 * self.mB**3) )
        with np.errstate(invalid='ignore'):
            self.Rate = 0.12/1.8*1e-5 * prefactor * ( ( self.mB**2 + self.mP**2 - q2)**2 - 4. * self.mB**2 * self.mP**2 )**(3./2.) * Vector**2 

        self.Rate[ np.isnan(self.Rate) ] = 0
        if self.Decay == "DsMuNu":
            self.Rate *= 0.3725

        IntegratedRate = self.Rate.sum(axis = 1) * dQ2
                
        return IntegratedRate[0], np.std(IntegratedRate[1:]) 
        

    def GetVector(self, q2):
        """
        Calculate the vector form factor
        """
        
        self.FFVector = self._fVector(q2)
        self.FFVector[ np.isnan(self.FFVector) ] = 0
        
        self.e_FFVector_L, self.e_FFVector_H = asymmstd( self.FFVector[1:], axis = 0)
        
        return self.FFVector[0], self.e_FFVector_L, self.e_FFVector_H
    
class FormFactorPlotter():
    
    def __init__(self, FFModel, FillColour = (ROOT.kAzure-9, 0.5), LineColour = ROOT.kAzure):
        self.FFModel = FFModel
        gROOT.SetBatch(True)
        ROOT.gStyle.SetTitleSize(0.06,"X")
        ROOT.gStyle.SetTitleSize(0.06,"Y")
        
        self.Objects=None
        self.FillColour = FillColour
        self.LineColour = LineColour
        
        self.XLabel = "q^{2} [GeV^{2}]"
        self.YLabel = "|V_{ub}|^{-2} d#Gamma/dq^{2} [ps^{-1}GeV^{-2}]"
    
    def SetXLabel(self, XLabel):
        self.XLabel = XLabel

    def SetYLabel(self, YLabel):
        self.YLabel = YLabel
    
    def _PrettifyGraph(self,gr,xtitle = "" ,ytitle = ""):
        """
        Make the graph nicer:
            Set markers, colours, center titles
            Optionally set axis labels
        """
        for ax in [gr.GetXaxis(),gr.GetYaxis()]:
            ax.CenterTitle()
        gr.SetMarkerStyle(8)
        gr.SetMarkerColor(ROOT.kBlue)
        
        if xtitle != "":
            gr.GetXaxis().SetTitle(xtitle)
            
        if ytitle != "":
            gr.GetYaxis().SetTitle(ytitle)

    
    def _PlotterObjects(self, q2Bins, Mode = "Rate"):
        nPoint = q2Bins.shape[0]
        
        if Mode == "Rate":
            r, elow, ehigh = self.FFModel.GetRate(q2Bins)
        elif Mode == "Vector":
            r, elow, ehigh = self.FFModel.GetVector(q2Bins)
        
        self.Objects = []


        grBand = ROOT.TGraphAsymmErrors(nPoint,
                                        q2Bins          .astype(np.float64),
                                        r               .astype(np.float64),
                                        np.zeros(nPoint).astype(np.float64),
                                        np.zeros(nPoint).astype(np.float64),
                                        elow            .astype(np.float64),
                                        ehigh           .astype(np.float64)
                                        )
                                       
        grBand.SetFillColorAlpha( *self.FillColour )
        grBand.SetLineColor(self.FillColour[0])
        ROOT.SetOwnership(grBand,False)
        self._PrettifyGraph(grBand,self.XLabel,self.YLabel)
        
        self.Objects.append(grBand)
        
        for var in [0.,-elow,ehigh]:
            grVar = ROOT.TGraph(nPoint,
                                q2Bins   .astype(np.float64),
                                (r + var).astype(np.float64)
                               )
            grVar.SetLineColor(self.LineColour)
            grVar.SetLineWidth(1)
            ROOT.SetOwnership(grVar,False)
            self.Objects.append(grVar)
        
        return self.Objects
    
    def Plot(self, q2Bins, Mode="Rate"):
        
        Objects = self._PlotterObjects(q2Bins, Mode)

        plotPrefix = "StudyBsToKMuNuFFs__"
                
        # plotting starts here
        c = ROOT.TCanvas("c1", "c1", 1600, 1600)
        
        
        Objects[0].Draw("AL4")
        Objects[1].Draw("SAME L")
        Objects[2].Draw("SAME L")
        Objects[3].Draw("SAME L")
        
        Objects[0].GetYaxis().Draw("SAME")
        c.RedrawAxis()
        Objects[0].SetName("FF")
        c.Print(plotPrefix+"dGamma_dq2_Band.png")
        


    