
./TestTrimmedNTuples 2>/dev/null |tee TmpFileList

NFILE=0

for F in `cat TmpFileList` 
do 
    LUMISOURCE=`echo /eos/lhcb/wg/semileptonic/Bs2KmunuAna/Tuples/$F|sed -e 's/.root/_LumiTuple.root/g'|sed -e 's@DATATUPLES_RAW_Nov17@DATATUPLES_RAW_Nov17/BackupLumiTuple@g'`
    LUMITARGET=`echo /eos/lhcb/wg/semileptonic/Bs2KmunuAna/Tuples/$F|sed -e 's/.root/_LumiTuple.root/g'|sed -e 's@RAW@TRIMMED@g'`

    cp -v $LUMISOURCE $LUMITARGET
    
    bash TrimFile.sh $F & 
    NFILE=`expr 1 + $NFILE`
    
    if [ $NFILE -eq 8 ] 
    then 
        wait; 
        NFILE=0; 
    fi
done

wait 
rm TmpFileList
