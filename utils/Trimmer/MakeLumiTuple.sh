#!/bin/bash


for JOB in `seq 0 103`
do
    LUMI=`cat /afs/cern.ch/user/i/ismith/gangadir/workspace/ismith/LocalXML/638/$JOB/output/stdout |grep "Integrated Luminosity"|sed -e 's@.*: \(.*\) *+/- *\(.*\) .*@\1 \2@g'`
    echo $LUMI
    python MakeLumiTuple.py /eos/lhcb/wg/semileptonic/Bs2KmunuAna/Tuples/Bs2KMuNu_DATATUPLES_RAW_Nov17/BackupLumiTuple/${JOB}_data12_Bs2KMuNu_Up_LumiTuple.root $LUMI
done &


for JOB in `seq 0 108`
do
    LUMI=`cat /afs/cern.ch/user/i/ismith/gangadir/workspace/ismith/LocalXML/639/$JOB/output/stdout |grep "Integrated Luminosity"|sed -e 's@.*: \(.*\) *+/- *\(.*\) .*@\1 \2@g'`
    echo $LUMI
    python MakeLumiTuple.py /eos/lhcb/wg/semileptonic/Bs2KmunuAna/Tuples/Bs2KMuNu_DATATUPLES_RAW_Nov17/BackupLumiTuple/${JOB}_data12_Bs2KMuNu_Down_LumiTuple.root $LUMI
done &


for JOB in `seq 0 47`
do
    LUMI=`cat /afs/cern.ch/user/i/ismith/gangadir/workspace/ismith/LocalXML/653/$JOB/output/stdout |grep "Integrated Luminosity"|sed -e 's@.*: \(.*\) *+/- *\(.*\) .*@\1 \2@g'`
    echo $LUMI
    python MakeLumiTuple.py /eos/lhcb/wg/semileptonic/Bs2KmunuAna/Tuples/Bs2KMuNu_DATATUPLES_RAW_Nov17/BackupLumiTuple/${JOB}_data11_Bs2KMuNu_Up_LumiTuple.root $LUMI
done &


for JOB in `seq 0 60`
do
    LUMI=`cat /afs/cern.ch/user/i/ismith/gangadir/workspace/ismith/LocalXML/654/$JOB/output/stdout |grep "Integrated Luminosity"|sed -e 's@.*: \(.*\) *+/- *\(.*\) .*@\1 \2@g'`
    echo $LUMI
    python MakeLumiTuple.py /eos/lhcb/wg/semileptonic/Bs2KmunuAna/Tuples/Bs2KMuNu_DATATUPLES_RAW_Nov17/BackupLumiTuple/${JOB}_data11_Bs2KMuNu_Down_LumiTuple.root $LUMI
done &


wait