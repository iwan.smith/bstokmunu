#ifndef Q2CALC_H
#define Q2CALC_H

#include "Q2calculation.h"

#include <vector>
#include <iostream>
#include <TLorentzVector.h>

namespace Q2calculation
{

  void QuadraticMomentum( float * solutions, TLorentzVector & VIS, TVector3 & FLIGHT, const float mB, float& Discriminant, TLorentzVector& B1, TLorentzVector& B2){
    B1.SetXYZM(0,0,0,0);
    B2.SetXYZM(0,0,0,0);

    TVector3 FLIGHTU = FLIGHT.Unit();
    const float PVIS_T = VIS.Vect().Cross(FLIGHT.Unit()).Mag();
    const float PVIS_L = VIS.Vect().Dot(FLIGHT.Unit());

    float VISM = VIS.M();
    float VISE = VIS.E();

    float a = (mB*mB - VISM*VISM - 2*PVIS_T*PVIS_T)*PVIS_L;
    a /= 2*(PVIS_L*PVIS_L - VISE*VISE);

    float r = pow(mB*mB - VISM*VISM - 2*PVIS_T*PVIS_T,2)*VISE*VISE;
    r /= 4*pow(PVIS_L*PVIS_L - VISE*VISE,2);
    r += (VISE*VISE * PVIS_T*PVIS_T)/(PVIS_L*PVIS_L - VISE*VISE);

    std::vector<float> signs = {-1.,1.};

    int i = -1;
    for ( auto sign : signs ) {
      i+=1;
      float PL = -a + sign*sqrt(r);
      //      std::couta//a/ << "PL = " << PL << std::endl;
      float PXnu = FLIGHTU.X()*PL - (VIS.Px()-VIS.P()*FLIGHTU.X());
      float PYnu = FLIGHTU.Y()*PL - (VIS.Py()-VIS.P()*FLIGHTU.Y());
      float PZnu = FLIGHTU.Z()*PL - (VIS.Pz()-VIS.P()*FLIGHTU.Z());

      TLorentzVector nuINF(PXnu,PYnu,PZnu,sqrt(PXnu*PXnu + PYnu*PYnu +PZnu*PZnu ));

      TLorentzVector B_INFERED(PXnu+VIS.Px(),
                               PYnu+VIS.Py(),
                               PZnu+VIS.Pz(),
                               nuINF.E()+VIS.E());

      if (r > 0.){
        solutions[i] = B_INFERED.P();

        if ( sign == -1 )
          B1 = B_INFERED;
        else
          B2 = B_INFERED;
        //solutions.push_back(B_INFERED.P());
      }else{
        solutions[i] = -1.0;//B_INFERED.P();
      }


    }
    Discriminant = r;
  }
  
  float Q2( TLorentzVector & D, TVector3 & FLIGHT, const float mB,const float pB){

    TLorentzVector B(pB*FLIGHT.X()/FLIGHT.Mag(),
                     pB*FLIGHT.Y()/FLIGHT.Mag(),
                     pB*FLIGHT.Z()/FLIGHT.Mag(),
                     sqrt(mB*mB + pB*pB));
    TLorentzVector W(B-D);
    if ( W.M2() > 0 )
      return W.M2();
    return -1000;

  }
}

#endif
