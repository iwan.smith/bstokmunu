
cp -fv /eos/lhcb/wg/semileptonic/Bs2KmunuAna/Tuples/Bs2KMuNu_DATATUPLES_RAW_Nov17/BackupLumiTuple/* /eos/lhcb/wg/semileptonic/Bs2KmunuAna/Tuples/Bs2KMuNu_DATATUPLES_TRIMMED_Nov17/. 

echo "Submitting Jobs"

for v in `ls /eos/lhcb/wg/semileptonic/Bs2KmunuAna/Tuples/Bs2KMuNu_DATATUPLES_RAW_Nov17/*data1*root|sed -e 's@.*/@@g'` ; do bsub -q 8nh -J $v << EOF
echo $v
cd /afs/cern.ch/user/i/ismith/ANA/BsToKMuNu/utils
source ./SetupEnv.sh
cd /afs/cern.ch/user/i/ismith/ANA/BsToKMuNu/utils/Trimmer
./Trimmer Bs2KMuNu_DATATUPLES_RAW_Nov17/$v
EOF
done

NREMAIN=999
while [ $NREMAIN -gt 0 ]
do
  sleep 180
  NREMAIN=$(bjobs|grep root|wc -l)
  echo $NREMAIN "Jobs Remaining"
done


cd /eos/lhcb/wg/semileptonic/Bs2KmunuAna/Tuples/Bs2KMuNu_DATATUPLES_TRIMMED_Nov17/

NSIM=0
for FILE in `ls *_LumiTuple.root`
do
  NSIM=`expr $NSIM + 1`
  SHORTFILE=`echo $FILE|sed -e 's/_LumiTuple.root//g'`
  echo $FILE
  echo $SHORTFILE 
  hadd -O -f -k ${SHORTFILE}{,_Bs2DsMuNuSSTuple_FakeMuon,_Bs2DsMuNuSSTuple,_Bs2DsMuNuTuple_FakeMuon,_Bs2DsMuNuTuple,_Bs2KmuNuSSTuple_FakeKMu,_Bs2KmuNuSSTuple_FakeK,_Bs2KmuNuSSTuple_FakeMu,_Bs2KmuNuSSTuple,_Bs2KmuNuTuple_FakeKMu,_Bs2KmuNuTuple_FakeK,_Bs2KmuNuTuple_FakeMu,_Bs2KmuNuTuple,_LumiTuple}.root && \
  rm  -v ${SHORTFILE}{_Bs2DsMuNuSSTuple_FakeMuon,_Bs2DsMuNuSSTuple,_Bs2DsMuNuTuple_FakeMuon,_Bs2DsMuNuTuple,_Bs2KmuNuSSTuple_FakeKMu,_Bs2KmuNuSSTuple_FakeK,_Bs2KmuNuSSTuple_FakeMu,_Bs2KmuNuSSTuple,_Bs2KmuNuTuple_FakeKMu,_Bs2KmuNuTuple_FakeK,_Bs2KmuNuTuple_FakeMu,_Bs2KmuNuTuple,_LumiTuple}.root &
  

  if [ $NSIM -eq 16 ]
  then
    NSIM=0
    wait
  fi
done

wait

cd /afs/cern.ch/user/i/ismith/ANA/BsToKMuNu/utils/Trimmer
bash FixBroken.sh