
"""import root_numpy
import numpy as np

LumiTuple=np.ones(2, dtype={'names':["IntegratedLuminosity", "IntegratedLuminosityErr"], 'formats' : [np.float64, np.float64]})

LumiTuple["IntegratedLuminosity"][0] = 1.5
LumiTuple["IntegratedLuminosityErr"][0] = 0.5

print LumiTuple

root_numpy.array2root(LumiTuple, "LumiTuple.root", "LumiTuple", "RECREATE")
"""
from sys import argv

if len(argv)!=4:
    exit() 

from ROOT import TFile, TTree
from array import array
 
f = TFile( argv[1], 'RECREATE' )
t = TTree( 'LumiTuple', 'LumiTuple' )
 
IntegratedLuminosity    = array( 'd', [ float(argv[2]) ] )
IntegratedLuminosityErr = array( 'd', [ float(argv[3]) ] )
t.Branch( 'IntegratedLuminosity',    IntegratedLuminosity,    'IntegratedLuminosity/D' )
t.Branch( 'IntegratedLuminosityErr', IntegratedLuminosityErr, 'IntegratedLuminosityErr/D' )
 

t.Fill()
f.Write()
f.Close()

