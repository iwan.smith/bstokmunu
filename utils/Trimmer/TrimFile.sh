#!/bin/bash

if [ -z "$1" ]
then
    echo ""
    echo "Please Supply a file to trim"
    echo "    Base directory is assumed to be /eos/lhcb/wg/semileptonic/Bs2KmunuAna/Tuples/"
    echo "e.g. run as:"
    echo "    ./Trimmer Bs2DsMuNu_MCTUPLES_RAW_08Feb17/DTT_MC11_Bs2DsMuNu_13774000_Cocktail_SIGNAL_Up.root"
    echo ""
    
    exit
fi

FILE=$1

FILE2=$( sed -e 's@.root@@g' <<< $FILE )
FILE2=$( sed -e 's@RAW@TRIMMED@g' <<< $FILE2 )

rm -v /eos/lhcb/wg/semileptonic/Bs2KmunuAna/Tuples/${FILE2}_*.root


./Trimmer $FILE


hadd -f -O -k /eos/lhcb/wg/semileptonic/Bs2KmunuAna/Tuples/${FILE2}.root /eos/lhcb/wg/semileptonic/Bs2KmunuAna/Tuples/${FILE2}_*.root && \
rm -v /eos/lhcb/wg/semileptonic/Bs2KmunuAna/Tuples/${FILE2}_*.root
