#!/bin/bash

# Here's a hint. Plan for the future so that this sort of nonsence doesn't have to happen!

LINES=($(grep -n "Bs_PZ" dtt.h|sed -e 's/:.*//g'))

N=$(cat dtt.h|wc -l)

echo "Adding Ds variables to Tree Branch Manager"
echo $N

# Get Top Quarter
cat dtt.h | head -${LINES[0]} > dtt.h_0


# Get Second  Quarter
cat dtt.h | head -${LINES[1]} | tail -$(( ${LINES[1]} - ${LINES[0]} )) > dtt.h_1

# Get Third  Quarter
cat dtt.h | head -${LINES[2]} | tail -$(( ${LINES[2]} - ${LINES[1]} )) > dtt.h_2

# Get Lower Quarter
cat dtt.h | tail -$(( $N - ${LINES[2]} )) > dtt.h_3


# Now Add Lines To Sections of Code

echo '   Double_t        Ds_P;
   Double_t        Ds_PT;
   Double_t        Ds_PE;
   Double_t        Ds_PX;
   Double_t        Ds_PY;
   Double_t        Ds_PZ;
   Double_t        Ds_TRUEP_E;
   Double_t        Ds_TRUEP_X;
   Double_t        Ds_TRUEP_Y;
   Double_t        Ds_TRUEP_Z;' >> dtt.h_0
   

echo '   TBranch        *b_Ds_P;   //!
   TBranch        *b_Ds_PT;   //!
   TBranch        *b_Ds_PE;   //!
   TBranch        *b_Ds_PX;   //!
   TBranch        *b_Ds_PY;   //!
   TBranch        *b_Ds_PZ;   //!
   TBranch        *b_Ds_TRUEP_E; //!
   TBranch        *b_Ds_TRUEP_X; //!
   TBranch        *b_Ds_TRUEP_Y; //!
   TBranch        *b_Ds_TRUEP_Z; //!' >> dtt.h_1

echo '   fChain->SetBranchAddress("Ds_P",  &Ds_P,  &b_Ds_P);
   fChain->SetBranchAddress("Ds_PT", &Ds_PT, &b_Ds_PT);
   fChain->SetBranchAddress("Ds_PE", &Ds_PE, &b_Ds_PE);
   fChain->SetBranchAddress("Ds_PX", &Ds_PX, &b_Ds_PX);
   fChain->SetBranchAddress("Ds_PY", &Ds_PY, &b_Ds_PY);
   fChain->SetBranchAddress("Ds_PZ", &Ds_PZ, &b_Ds_PZ);
   fChain->SetBranchAddress("Ds_TRUEP_E", &Ds_TRUEP_E, &b_Ds_TRUEP_E);
   fChain->SetBranchAddress("Ds_TRUEP_X", &Ds_TRUEP_X, &b_Ds_TRUEP_X);
   fChain->SetBranchAddress("Ds_TRUEP_Y", &Ds_TRUEP_Y, &b_Ds_TRUEP_Y);
   fChain->SetBranchAddress("Ds_TRUEP_Z", &Ds_TRUEP_Z, &b_Ds_TRUEP_Z);' >> dtt.h_2


cat dtt.h_{0,1,2,3} > dtt.h

