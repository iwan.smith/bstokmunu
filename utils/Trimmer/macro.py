from ROOT import TString, TFile
from ROOT import gSystem
from ROOT import gInterpreter
import ROOT
VOS = ROOT.std.vector('std::string')
POS = ROOT.std.pair('std::string','std::string')


filename = "DTT_MC12_Bs2DsMuNu_13774002_Cocktail_SIGNAL_Down"
path     = "root://eoslhcb.cern.ch//eos/lhcb/wg/semileptonic/Bs2KmunuAna/Tuples/"

double_values = [ "Bs_MCORR", "Bs_MCORRERR", "Bs_P", "Bs_PT", "Bs_IPCHI2_OWNPV", "Bs_FDCHI2_OWNPV", "Bs_DIRA_OWNPV", "Bs_ENDVERTEX_CHI2", "Bs_Q2SOL1", "Bs_Q2SOL2", "Bs_iso_SumBDT_Long", "Bs_iso_MinBDT_Long", "Bs_iso_SumBDT_VELO", "Bs_iso_MinBDT_VELO", "Ds_P", "Ds_PT", "Ds_IPCHI2_OWNPV", "kaon_m_PIDK", "kaon_m_PIDp", "kaon_m_PIDmu", "kaon_m_P", "kaon_m_PZ", "kaon_m_PT", "kaon_m_IPCHI2_OWNPV", "kaon_m_TRACK_GhostProb", "kaon_p_PIDK", "kaon_p_PIDp", "kaon_p_PIDmu", "kaon_p_P", "kaon_p_PZ", "kaon_p_PT", "kaon_p_IPCHI2_OWNPV", "kaon_p_TRACK_GhostProb", "pi_p_PIDK", "pi_p_PIDp", "pi_p_PIDmu", "pi_p_P", "pi_p_PZ", "pi_p_PT", "pi_p_IPCHI2_OWNPV", "pi_p_TRACK_GhostProb", "muon_p_PIDK", "muon_p_PIDp", "muon_p_PIDmu", "muon_p_P", "muon_p_PZ", "muon_p_PT", "muon_p_IPCHI2_OWNPV", "muon_p_TRACK_GhostProb"] ## double variables to copy
bool_values         = [ "Bs_Hlt2TopoMu2BodyBBDTDecision_TOS" ] ## boolean variables to copy
int_values         = [ "Bs_BKGCAT", "nTracks" ] ## integer variables to copy



infile = path+filename+".root"
orgfile = TFile.Open(infile,"read")
orgtree = orgfile.Get("Bs2KmuNuTuple/DecayTree")
try:
   orgtree.GetBranch("Bs_Nu_TRUEID")
except:
   has_FF_info = False
else:
   has_FF_info = True
orgtree.MakeClass("dtt")
orgfile.Close()

if has_FF_info:
    flags = TString(gSystem.GetFlagsOpt())
    flags+=" -DUSE_FF_WEIGHTING"
    gSystem.SetFlagsOpt(flags.Data())
incpath = TString(gSystem.GetIncludePath())
incpath += " -ILaurentLib/lib"
gSystem.SetIncludePath(incpath.Data())
gInterpreter.LoadMacro("LaurentLib/lib/MassRegister.cpp+")
gInterpreter.LoadMacro("reducer.cc++")
gInterpreter.LoadMacro("dtt.C+")
gInterpreter.LoadMacro("FormFactorsRew.C+")




MCTruth = "Bs_BKGCAT == 50"
BCut    = "Bs_MCORR < 7000 && Bs_MCORR > 2500"
    ##std::string KmCut   = "(kaon_m_PIDK - kaon_m_PIDp) > 5 && kaon_m_PIDK > 5 && (kaon_m_PIDK - kaon_m_PIDmu) > 5"
    ##std::string KpCut   = "(kaon_p_PIDK - kaon_p_PIDp) > 5 && kaon_p_PIDK > 5 && (kaon_p_PIDK - kaon_p_PIDmu) > 5"
    ##std::string muCut   = "(muon_p_PIDmu - muon_p_PIDK) > 0 && (muon_p_PIDmu - muon_p_PIDp) > 0 && muon_p_IPCHI2_OWNPV > 12 && muon_p_TRACK_GhostProb < 0.35"
muCut   = "muon_p_IPCHI2_OWNPV > 12 && muon_p_TRACK_GhostProb < 0.35"

from ROOT import reducer
myReducer = reducer()
myReducer.m_treename       = "Bs2KmuNuTuple/DecayTree"
myReducer.m_filename       = path+filename+".root"
myReducer.m_outFilename    = "/tmp/removeme_trimmed_norm.root"
myReducer.m_convertToFloat = True ## converts doubles to float if true (default:false)
##/ TODO: re add ETA variables

double_value_vector = VOS()
for d in double_values:
    double_value_vector.push_back(d)
int_value_vector = VOS()
for i in int_values:
    int_value_vector.push_back(i)
bool_value_vector = VOS()
for b in bool_values:
    bool_value_vector.push_back(b)
myReducer.m_dVals = double_value_vector
myReducer.m_iVals = int_value_vector
##/ TODO: re add 3 and 4 body
myReducer.m_bVals = bool_value_vector
##myReducer.m_cuts           = { (MCTruth+" && "+BCut+" && "+KmCut+" && "+KpCut+" && "+muCut).c_str()}; ## cuts (optional)
from ROOT import TCut
myReducer.m_cuts           = TCut(MCTruth+" && "+BCut+" && "+muCut) ## cuts (optional)

## see also for setting myReducer.m_form
##/http://stackoverflow.com/questions/27669200/how-should-i-brace-initialize-an-stdarray-of-stdpairs
##myReducer.m_form           = { { "foobar" , "pi_p_PX*pi_p_PX+pi_p_PY*pi_p_PY+pi_p_PZ*pi_p_PZ-pi_p_P*pi_p_P" } , { "barbaz" , "TMath::Sqrt(pi_p_PX*pi_p_PX+pi_p_PY*pi_p_PY)" } } ; 

##/ sorry, the muon_p should be a muon_m
gInterpreter.LoadMacro("LaurentLib/lib/CutLibrary.h")
from ROOT.ForPaul import CutLibrary
from ROOT.ForPaul import ParticleDefinitions
m_cutlib = CutLibrary(ParticleDefinitions(("kaon_m"),("kaon_p"),("pi_p"),("muon_p")))
##myReducer.m_form.push_back({std::string("LcMassK2"),m_cutlib.getLcMassK2()})
##myReducer.m_form.push_back({std::string("DMassK2"),m_cutlib.getDMassK2()})
##myReducer.m_form.push_back({std::string("JPsiMassK2"),m_cutlib.getJPsiMassK2()})
##myReducer.m_form.push_back({std::string("JPsiMassPi"),m_cutlib.getJPsiMassPi()})

myReducer.m_form.push_back(POS("is_phi",m_cutlib.getDalitzCut(0)))
myReducer.m_form.push_back(POS("is_kstar",m_cutlib.getDalitzCut(1)))
myReducer.m_form.push_back(POS("is_nonresonant",m_cutlib.getDalitzCut(2)))
myReducer.m_form.push_back(POS("passJpsiCut","("+m_cutlib.getDalitzCut(0)+"&&"+m_cutlib.getJPsiCut(0)+")||(!("+m_cutlib.getDalitzCut(0)+")&&"+m_cutlib.getJPsiCut(1)+")"))
myReducer.m_form.push_back(POS("passDplusCut","("+m_cutlib.getDalitzCut(0)+")||(!("+m_cutlib.getDalitzCut(0)+")&&"+m_cutlib.getDplusCut()+")"))
myReducer.m_form.push_back(POS("passLambdaCut",m_cutlib.getLambdaCut()))


sys.exit(myReducer.reduceTree())


