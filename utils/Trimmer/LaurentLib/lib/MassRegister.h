/*
 * MassRegister.h
 *
 *  Created on: Oct 8, 2014
 *      Author: laurentd
 */
#ifndef MASSREGISTER_H_
#define MASSREGISTER_H_

class MassRegister {
public:
  static const float KPLUS;
  static const float PION;
  static const float MUON;
  static const float PROTON;
  static const float BS;

  static const float LAMBDAC;
  static const float DPLUS;
};

#endif /* MASSREGISTER_H_ */
