#include "MassRegister.h"

const float MassRegister::KPLUS = 493.677;
const float MassRegister::PION = 139.57;
const float MassRegister::MUON = 105.658;
const float MassRegister::PROTON = 938.27;
const float MassRegister::BS = 5366.77;

/**
 * These results come from sideband fits.
 */
const float MassRegister::LAMBDAC = 2287.93;
const float MassRegister::DPLUS = 1871;
