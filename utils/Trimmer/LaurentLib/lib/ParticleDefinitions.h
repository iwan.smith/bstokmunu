#ifndef PARTICLE_DEF_H
#define PARTICLE_DEF_H 1
#include <map>

namespace ForPaul {
class ParticleDefinitions {
public:
  enum class DsMuNuDecayParticle { SS_KAON, OS_KAON, PION, MUON, NEUTRINO };

private:
  std::map<DsMuNuDecayParticle, const std::string> particleNames;

public:
  ParticleDefinitions(const char *k1, const char *k2, const char *pi,
                      const char *mu) {
    particleNames.emplace(DsMuNuDecayParticle::OS_KAON, std::string(k1));
    particleNames.emplace(DsMuNuDecayParticle::PION, std::string(pi));
    particleNames.emplace(DsMuNuDecayParticle::SS_KAON, std::string(k2));
    particleNames.emplace(DsMuNuDecayParticle::MUON, std::string(mu));
  }

  const std::string getBranchPrefix(DsMuNuDecayParticle var) const {
    return particleNames.at(var);
  }
};
}

#endif
