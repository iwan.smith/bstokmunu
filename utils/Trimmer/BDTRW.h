#ifndef BDT_RW_H
#define BDT_RW_H

#include "BDT/src/reader_wrapper.h"
#include <iostream>
#include <regex>
#include "TFile.h"
#include "TSystem.h"
#include "TEntryList.h"
#include "TTree.h"
#include <iostream>
#include <algorithm>
#include <TTreeFormula.h>
#include <TLorentzVector.h>
#include <TVector3.h>
#include <cstddef>
#include <memory>
#include "TSystem.h"
#include "TMath.h"
#include "TMVA/Tools.h"
#include "TMVA/Reader.h"
#include "TH1.h"

using std::string;
using std::vector;
using std::size_t;


template<typename T>
void SmoothBins( T* h1, T* h2)
{

  const int nBinsX = h1->GetNbinsX();

  int SumBin_S = 0;
  int SumBin_B = 0;
  int Bin0 = 1;
  const int MinBin = 1000;
  for(int b = 0; b<= nBinsX+1; b++)
  {
      SumBin_S += h1->GetBinContent(b);
      SumBin_B += h2->GetBinContent(b);

      if ( SumBin_S + SumBin_B < MinBin and b != nBinsX) continue;

      if ( Bin0 != b)
      {
        for( int b2 = Bin0; b2 <= b; b2++)
        {
            h1->SetBinContent(b2, SumBin_S);
            h2->SetBinContent(b2, SumBin_B);
        }
      }
      SumBin_B = 0;
      SumBin_S = 0;

      Bin0 = b+1;


  }

}


// =====================================================
// -- Small Class for reWeighting data from a BDT
// =====================================================

class BDT_RW
{

public:

  BDT_RW() = delete;
  BDT_RW( string BDTFileName, reader_wrapper& BDT, TTree* T_Out, string BranchName );
  BDT_RW& operator=(BDT_RW&) = delete;


  const float& GetBDTVal(){return m_BDT.m_response;};
  const float& GetRWVal(){return *m_RW;};

  void Calculate();

private:

  std::shared_ptr<TH1> m_h_num;
  std::shared_ptr<TH1> m_h_den;

  reader_wrapper& m_BDT;

  std::shared_ptr<float_t> m_RW ;

};


#endif

