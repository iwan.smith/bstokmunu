//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Wed Oct 18 19:30:39 2017 by ROOT version 6.06/02
// from TTree DecayTree/DecayTree
// found on file: /eos/lhcb/wg/semileptonic/Bs2KmunuAna/Tuples/Bs2KMuNu_MCTUPLES_RAW_30May16/DTT_13512010_Bs_Kmunu_DecProdCut_Dn_Py8_MC12.root
//////////////////////////////////////////////////////////

#ifndef dtt_h
#define dtt_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.

class dtt {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

// Fixed size dimensions of array or collections stored in the TTree if any.
   const Int_t kMaxBs_ENDVERTEX_COV = 1;
   const Int_t kMaxBs_OWNPV_COV = 1;
   const Int_t kMaxkaon_m_OWNPV_COV = 1;
   const Int_t kMaxkaon_m_ORIVX_COV = 1;
   const Int_t kMaxmuon_p_OWNPV_COV = 1;
   const Int_t kMaxmuon_p_ORIVX_COV = 1;

   // Declaration of leaf types
   Double_t        Bs_ENDVERTEX_X;
   Double_t        Bs_ENDVERTEX_Y;
   Double_t        Bs_ENDVERTEX_Z;
   Double_t        Bs_OWNPV_X    ;
   Double_t        Bs_OWNPV_Y    ;
   Double_t        Bs_OWNPV_Z    ;
   Double_t        Bs_PE         ;
   Double_t        Bs_PX         ;
   Double_t        Bs_PY         ;
   Double_t        Bs_PZ         ;
   Double_t        Bs_TRUEP_E    ;
   Double_t        Bs_TRUEP_X    ;
   Double_t        Bs_TRUEP_Y    ;
   Double_t        Bs_TRUEP_Z    ;
   Double_t        Ds_PE         ;
   Double_t        Ds_PX         ;
   Double_t        Ds_PY         ;
   Double_t        Ds_PZ         ;
   Double_t        Ds_TRUEP_E    ;
   Double_t        Ds_TRUEP_X    ;
   Double_t        Ds_TRUEP_Y    ;
   Double_t        Ds_TRUEP_Z    ;
   Double_t        kaon_m_PE     ;
   Double_t        kaon_m_PX     ;
   Double_t        kaon_m_PY     ;
   Double_t        kaon_m_PZ     ;
   Double_t        kaon_m_TRUEP_E;
   Double_t        kaon_m_TRUEP_X;
   Double_t        kaon_m_TRUEP_Y;
   Double_t        kaon_m_TRUEP_Z;

   // List of branches
   TBranch        *b_Bs_ENDVERTEX_X;
   TBranch        *b_Bs_ENDVERTEX_Y;
   TBranch        *b_Bs_ENDVERTEX_Z;
   TBranch        *b_Bs_OWNPV_X    ;
   TBranch        *b_Bs_OWNPV_Y    ;
   TBranch        *b_Bs_OWNPV_Z    ;
   TBranch        *b_Bs_PE         ;
   TBranch        *b_Bs_PX         ;
   TBranch        *b_Bs_PY         ;
   TBranch        *b_Bs_PZ         ;
   TBranch        *b_Bs_TRUEP_E    ;
   TBranch        *b_Bs_TRUEP_X    ;
   TBranch        *b_Bs_TRUEP_Y    ;
   TBranch        *b_Bs_TRUEP_Z    ;

   TBranch        *b_Ds_TRUEP_E    ;
   TBranch        *b_Ds_TRUEP_X    ;
   TBranch        *b_Ds_TRUEP_Y    ;
   TBranch        *b_Ds_TRUEP_Z    ;
   TBranch        *b_Ds_PX         ;
   TBranch        *b_Ds_PY         ;
   TBranch        *b_Ds_PZ         ;
   TBranch        *b_Ds_PE         ;
   TBranch        *b_kaon_m_PE     ;
   TBranch        *b_kaon_m_PX     ;
   TBranch        *b_kaon_m_PY     ;
   TBranch        *b_kaon_m_PZ     ;
   TBranch        *b_kaon_m_TRUEP_E;
   TBranch        *b_kaon_m_TRUEP_X;
   TBranch        *b_kaon_m_TRUEP_Y;
   TBranch        *b_kaon_m_TRUEP_Z;

   dtt(TTree *tree=0);
   virtual ~dtt();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef dtt_cxx
dtt::dtt(TTree *tree) : fChain(0) 
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("/eos/lhcb/wg/semileptonic/Bs2KmunuAna/Tuples/Bs2KMuNu_MCTUPLES_RAW_30May16/DTT_13512010_Bs_Kmunu_DecProdCut_Dn_Py8_MC12.root");
      if (!f || !f->IsOpen()) {
         f = new TFile("/eos/lhcb/wg/semileptonic/Bs2KmunuAna/Tuples/Bs2KMuNu_MCTUPLES_RAW_30May16/DTT_13512010_Bs_Kmunu_DecProdCut_Dn_Py8_MC12.root");
      }
      TDirectory * dir = (TDirectory*)f->Get("/eos/lhcb/wg/semileptonic/Bs2KmunuAna/Tuples/Bs2KMuNu_MCTUPLES_RAW_30May16/DTT_13512010_Bs_Kmunu_DecProdCut_Dn_Py8_MC12.root:/Bs2KmuNuTuple");
      dir->GetObject("DecayTree",tree);

   }
   Init(tree);
}

dtt::~dtt()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t dtt::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t dtt::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void dtt::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("Bs_ENDVERTEX_X", &Bs_ENDVERTEX_X, &b_Bs_ENDVERTEX_X);
   fChain->SetBranchAddress("Bs_ENDVERTEX_Y", &Bs_ENDVERTEX_Y, &b_Bs_ENDVERTEX_Y);
   fChain->SetBranchAddress("Bs_ENDVERTEX_Z", &Bs_ENDVERTEX_Z, &b_Bs_ENDVERTEX_Z);
   fChain->SetBranchAddress("Bs_OWNPV_X", &Bs_OWNPV_X, &b_Bs_OWNPV_X);
   fChain->SetBranchAddress("Bs_OWNPV_Y", &Bs_OWNPV_Y, &b_Bs_OWNPV_Y);
   fChain->SetBranchAddress("Bs_OWNPV_Z", &Bs_OWNPV_Z, &b_Bs_OWNPV_Z);
   fChain->SetBranchAddress("Bs_PE", &Bs_PE, &b_Bs_PE);
   fChain->SetBranchAddress("Bs_PX", &Bs_PX, &b_Bs_PX);
   fChain->SetBranchAddress("Bs_PY", &Bs_PY, &b_Bs_PY);
   fChain->SetBranchAddress("Bs_PZ", &Bs_PZ, &b_Bs_PZ);
   fChain->SetBranchAddress("Ds_PE", &Ds_PE, &b_Ds_PE);
   fChain->SetBranchAddress("Ds_PX", &Ds_PX, &b_Ds_PX);
   fChain->SetBranchAddress("Ds_PY", &Ds_PY, &b_Ds_PY);
   fChain->SetBranchAddress("Ds_PZ", &Ds_PZ, &b_Ds_PZ);
   fChain->SetBranchAddress("Ds_TRUEP_E", &Ds_TRUEP_E, &b_Ds_TRUEP_E);
   fChain->SetBranchAddress("Ds_TRUEP_X", &Ds_TRUEP_X, &b_Ds_TRUEP_X);
   fChain->SetBranchAddress("Ds_TRUEP_Y", &Ds_TRUEP_Y, &b_Ds_TRUEP_Y);
   fChain->SetBranchAddress("Ds_TRUEP_Z", &Ds_TRUEP_Z, &b_Ds_TRUEP_Z);
   fChain->SetBranchAddress("Bs_TRUEP_E", &Bs_TRUEP_E, &b_Bs_TRUEP_E);
   fChain->SetBranchAddress("Bs_TRUEP_X", &Bs_TRUEP_X, &b_Bs_TRUEP_X);
   fChain->SetBranchAddress("Bs_TRUEP_Y", &Bs_TRUEP_Y, &b_Bs_TRUEP_Y);
   fChain->SetBranchAddress("Bs_TRUEP_Z", &Bs_TRUEP_Z, &b_Bs_TRUEP_Z);
   fChain->SetBranchAddress("kaon_m_PE", &kaon_m_PE, &b_kaon_m_PE);
   fChain->SetBranchAddress("kaon_m_PX", &kaon_m_PX, &b_kaon_m_PX);
   fChain->SetBranchAddress("kaon_m_PY", &kaon_m_PY, &b_kaon_m_PY);
   fChain->SetBranchAddress("kaon_m_PZ", &kaon_m_PZ, &b_kaon_m_PZ);
   fChain->SetBranchAddress("kaon_m_TRUEP_E", &kaon_m_TRUEP_E, &b_kaon_m_TRUEP_E);
   fChain->SetBranchAddress("kaon_m_TRUEP_X", &kaon_m_TRUEP_X, &b_kaon_m_TRUEP_X);
   fChain->SetBranchAddress("kaon_m_TRUEP_Y", &kaon_m_TRUEP_Y, &b_kaon_m_TRUEP_Y);
   fChain->SetBranchAddress("kaon_m_TRUEP_Z", &kaon_m_TRUEP_Z, &b_kaon_m_TRUEP_Z);

   Notify();
}

Bool_t dtt::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void dtt::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t dtt::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef dtt_cxx
