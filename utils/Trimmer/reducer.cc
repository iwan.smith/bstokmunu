#include "reducer.h"
#include "BDTRW.h"
#include "BDT/src/reader_wrapper.h"
#include <iostream>
#include <regex>
#include "TFile.h"
#include "TSystem.h"
#include "TEntryList.h"
#include "TTree.h"
#include <iostream>
#include <algorithm>
#include <TTreeFormula.h>
#include <TLorentzVector.h>
#include <TVector3.h>
#include <cstddef>
#include <memory>
#include "TSystem.h"
#include "TMath.h"
#include "TMVA/Tools.h"
#include "TMVA/Reader.h"
#include "Q2calculation.h"
#include "dtt.h"

using std::string;
using std::vector;
using std::size_t;



// =====================================================
// -- constructor
// =====================================================
reducer::reducer(string TreeName) {

  std::cout << "Welcome to reducer v 0.1" << std::endl;

  m_filename    = "";
  m_outFilename = "";
  m_treename    = "";
  m_convertToFloat = false;
  m_OutTreeName = TreeName;

}
// =====================================================
//-- destructor
// =====================================================
reducer::~reducer(){}

// =====================================================
//-- the main method: reduce a tree
// =====================================================
int reducer::reduceTree(string OpenHow){
  vector<reader_wrapper> bdts;





  if( m_filename  == "" || m_outFilename == "" || m_treename  == "" ){
    std::cout << "You need to define the input filename / treename / output filename" << std::endl;
    return 3;
  }

  if( m_dVals.empty() &&  m_iVals.empty() && m_bVals.empty()){
    std::cout << "You need to set variables for the new tree" << std::endl;
    return 3;
  }

  TFile* file = TFile::Open( m_filename.c_str() );
  if( !file ){
    std::cout << "file " << m_filename << " not found" << std::endl;
    return 2;
  }

  TTree* tree = (TTree*)file->Get( m_treename.c_str() );
  if( !tree ){
    std::cout << "tree " << m_treename << " not found" << std::endl;
    std::cout << "file contains:" << std::endl;
    file->ls();
    return 2;
  }


  {
    m_dRegs.push_back("Bs_TM_.*");
    TIter next(const_cast<TTree*>(tree)->GetListOfBranches());
    TBranch* br=nullptr;
    while ((br = (TBranch*)next())) {
      for (auto reg: m_dRegs) {
        std::regex mctruth(reg.c_str());
        if (std::regex_match(br->GetName(),mctruth)) {
          m_dVals.push_back(br->GetName());
        }
      }
      for (auto reg: m_iRegs) {
        std::regex mctruth(reg.c_str());
        if (std::regex_match(br->GetName(),mctruth)) {
          m_iVals.push_back(br->GetName());
        }
      }
    }
  }

  std::cout << " ============================= " << std::endl;
  std::cout << " Input filename:      " << m_filename << std::endl;
  std::cout << " treename:            " << m_treename << std::endl;
  std::cout << " Output filename:     " << m_outFilename << std::endl;
  std::cout << " Conversion to float: " << m_convertToFloat << std::endl;
  std::cout << std::endl;
  std::cout << " The variables to copy are: " << std::endl;
  for( string str : m_dVals ) std::cout << " " << str << ",";
  for( string str : m_iVals ) std::cout << " " << str << ",";
  for( string str : m_bVals ) std::cout << " " << str << ",";
  std::cout << std::endl;
  std::cout << " The cuts are: " << std::endl;
  std::cout << " " << m_cuts << std::endl;
  std::cout << " ============================= " << std::endl;


  /// https://root.cern.ch/phpBB3/viewtopic.php?f=3&t=21165&sid=003e047c399ecb5cb8a303266c0ede18
  Long64_t entries = tree->GetEntries();
  TEntryList *entryList = new TEntryList("mysel","mysel",tree);
  tree->Draw(">>mysel",m_cuts,"entrylist");
  tree->SetEntryList(entryList);


  tree->SetBranchStatus("*",1);

  UInt_t          runNumber;
  ULong64_t       eventNumber;
  vector<double>  doubleVars     (m_dVals.size());
  vector<float>   floatVars      (m_dVals.size());
  vector<int>     intVars        (m_iVals.size());
  std::deque <bool>    boolVars       (m_bVals.size()); // https://stackoverflow.com/questions/8324699/taking-address-of-temporary-while-accessing-address-of-a-element-in-vectorbool
  vector<double*> doubleVarsAddr (m_dVals.size());
  /// compiler was not happy with emplace_back for non-pointer TTreeFormula (http://stackoverflow.com/questions/10488348/add-to-vector-without-copying-struct-data/10488436#10488436)
  /// so i don't use vector<TTreeFormula> for now, and express my grudge at the end when cleaning up the heap
  vector<TTreeFormula*> m_formulas;
  vector<float> m_formVars(m_form.size(),0.f);
  for (auto &form: m_form )
  {
    m_formulas.emplace_back(new TTreeFormula(form.first.c_str(),form.second.c_str(),tree));
  }

  std::set<TBranch*> m_branches;
  /// https://root.cern.ch/phpBB3/viewtopic.php?f=3&t=21093&p=91702&sid=ee27773067273159ee449f213433b84b#p91702
  for (auto v : m_dVals)
  {
      if ( tree->GetBranch(v.c_str() ))
      {
        m_branches.insert(tree->GetBranch(v.c_str()));
      }
      else
      {
          std::cout << "Branch: " << v << " Not Found!" << std::endl;
      }
  }
  for (auto v : m_iVals)
  {
      std::cout << "Trying to open Branch: " << v << std::endl;
      m_branches.insert(tree->GetBranch(v.c_str()));
  }
  for (auto v : m_bVals)
  {
      std::cout << "Trying to open Branch: " << v << std::endl;
      m_branches.insert(tree->GetBranch(v.c_str()));
  }
  for (auto f : m_formulas) {
    for ( int v = 0 ; v < f->GetNcodes() ; ++v) {
      m_branches.insert(f->GetLeaf(v)->GetBranch());
    }
  }
  m_branches.insert(tree->GetBranch("runNumber"));
  m_branches.insert(tree->GetBranch("eventNumber"));

  for( unsigned int i = 0; i < m_dVals.size(); ++i ) tree->SetBranchAddress(m_dVals[i].c_str(), &doubleVars[i]);  // most of these will get overwritten by "dtt"
  for( unsigned int i = 0; i < m_iVals.size(); ++i ) tree->SetBranchAddress(m_iVals[i].c_str(), &intVars[i]);     // most of these will get overwritten by "dtt"
  for( unsigned int i = 0; i < m_bVals.size(); ++i ) tree->SetBranchAddress(m_bVals[i].c_str(), &boolVars[i]);    // most of these will get overwritten by "dtt"
  tree->SetBranchAddress("runNumber", &runNumber);
  tree->SetBranchAddress("eventNumber", &eventNumber);

  /// If you feel like knowing a better name for this variable,
  /// feel free to rename it.
  dtt treeBranchManager(tree);

  m_branches.insert(treeBranchManager.b_Bs_ENDVERTEX_X);
  m_branches.insert(treeBranchManager.b_Bs_ENDVERTEX_Y);
  m_branches.insert(treeBranchManager.b_Bs_ENDVERTEX_Z);
  m_branches.insert(treeBranchManager.b_Bs_OWNPV_X);
  m_branches.insert(treeBranchManager.b_Bs_OWNPV_Y);
  m_branches.insert(treeBranchManager.b_Bs_OWNPV_Z);

  m_branches.insert(treeBranchManager.b_Bs_PX);
  m_branches.insert(treeBranchManager.b_Bs_PY);
  m_branches.insert(treeBranchManager.b_Bs_PZ);
  m_branches.insert(treeBranchManager.b_Bs_PE);

  m_branches.insert(treeBranchManager.b_kaon_m_PX);
  m_branches.insert(treeBranchManager.b_kaon_m_PY);
  m_branches.insert(treeBranchManager.b_kaon_m_PZ);
  m_branches.insert(treeBranchManager.b_kaon_m_PE);

  m_branches.insert(treeBranchManager.b_Bs_TRUEP_X);
  m_branches.insert(treeBranchManager.b_Bs_TRUEP_Y);
  m_branches.insert(treeBranchManager.b_Bs_TRUEP_Z);
  m_branches.insert(treeBranchManager.b_Bs_TRUEP_E);
  m_branches.insert(treeBranchManager.b_kaon_m_TRUEP_X);
  m_branches.insert(treeBranchManager.b_kaon_m_TRUEP_Y);
  m_branches.insert(treeBranchManager.b_kaon_m_TRUEP_Z);
  m_branches.insert(treeBranchManager.b_kaon_m_TRUEP_E);


  for (size_t i = 0 ; i < m_dVals.size() ; ++i) doubleVarsAddr[i] = (double*)tree->GetBranch(m_dVals[i].c_str())->GetAddress();

  TFile* rFile = TFile::Open( m_outFilename.c_str(), OpenHow.c_str());
  if (nullptr == rFile) {
    std::cout << "could not open outputfile" << std::endl;
    return 1;
  }

  TTree* rTree2 = new TTree(m_OutTreeName.c_str(), m_OutTreeName.c_str());
  // FIXME: does this work w/o variable type declaration?
  rTree2->Branch("runNumber", &runNumber);
  rTree2->Branch("eventNumber", &eventNumber);

  // variables related to the Q2 determination
  Float_t Bs_Regression_Q2_TRUE = -999.;
  Float_t Bs_Regression_P_REGR,
    Bs_Regression_P_HI,
    Bs_Regression_P_LO,
    Bs_Regression_P_BEST,
    Bs_Regression_Q2_HI,
    Bs_Regression_Q2_LO,
    Bs_Regression_Q2_BEST,
    Bs_Regression_Q2_WORST,
    Bs_Regression_Q2_RANDOM,
    Bs_Regression_Q2_Discriminant,
    Bs_Reg_Best_PX,
    Bs_Reg_Best_PY,
    Bs_Reg_Best_PZ,
    Bs_Reg_Best_PE,
    Bs_Reg_Worst_PX,
    Bs_Reg_Worst_PY,
    Bs_Reg_Worst_PZ,
    Bs_Reg_Worst_PE;

  rTree2->Branch("Bs_Regression_Q2_RANDOM",&Bs_Regression_Q2_RANDOM,"Bs_Regression_Q2_RANDOM/F");
  rTree2->Branch("Bs_Regression_Q2_BEST",&Bs_Regression_Q2_BEST,"Bs_Regression_Q2_BEST/F");
  rTree2->Branch("Bs_Regression_Q2_WORST",&Bs_Regression_Q2_WORST,"Bs_Regression_Q2_WORST/F");
  rTree2->Branch("Bs_Regression_Q2_TRUE",&Bs_Regression_Q2_TRUE,"Bs_Regression_Q2_TRUE/F");
  const bool verboseRegression ( true );
  if ( verboseRegression ) {
    rTree2->Branch("Bs_Regression_P_REGR",&Bs_Regression_P_REGR,"Bs_Regression_P_REGR/F");
    rTree2->Branch("Bs_Regression_P_HI",&Bs_Regression_P_HI,"Bs_Regression_P_HI/F");
    rTree2->Branch("Bs_Regression_P_LO",&Bs_Regression_P_LO,"Bs_Regression_P_LO/F");
    rTree2->Branch("Bs_Regression_P_BEST",&Bs_Regression_P_BEST,"Bs_Regression_P_BEST/F");
    rTree2->Branch("Bs_Regression_Q2_HI",&Bs_Regression_Q2_HI,"Bs_Regression_Q2_HI/F");
    rTree2->Branch("Bs_Regression_Q2_LO",&Bs_Regression_Q2_LO,"Bs_Regression_Q2_LO/F");
    rTree2->Branch("Bs_Regression_Discriminant",&Bs_Regression_Q2_Discriminant,"Bs_Regression_Discriminant/F");

    rTree2->Branch("Bs_Reg_Best_PX",       &Bs_Reg_Best_PX ,      "Bs_Reg_Best_PX/F");
    rTree2->Branch("Bs_Reg_Best_PY",       &Bs_Reg_Best_PY ,      "Bs_Reg_Best_PY/F");
    rTree2->Branch("Bs_Reg_Best_PZ",       &Bs_Reg_Best_PZ ,      "Bs_Reg_Best_PZ/F");
    rTree2->Branch("Bs_Reg_Best_PE",       &Bs_Reg_Best_PE ,      "Bs_Reg_Best_PE/F");
    rTree2->Branch("Bs_Reg_Worst_PX",      &Bs_Reg_Worst_PX,      "Bs_Reg_Worst_PX/F");
    rTree2->Branch("Bs_Reg_Worst_PY",      &Bs_Reg_Worst_PY,      "Bs_Reg_Worst_PY/F");
    rTree2->Branch("Bs_Reg_Worst_PZ",      &Bs_Reg_Worst_PZ,      "Bs_Reg_Worst_PZ/F");
    rTree2->Branch("Bs_Reg_Worst_PE",      &Bs_Reg_Worst_PE,      "Bs_Reg_Worst_PE/F");



  }

  TTreeFormula * fo_FlightLength, *fo_FlightAngle;
  const string def_FlightLength("Bs_FD_OWNPV"),
    def_FlightAngle("1./TMath::Sin(TMath::ACos((Bs_ENDVERTEX_Z-Bs_OWNPV_Z)/Bs_FD_OWNPV))");
  fo_FlightLength = new TTreeFormula("FlightLength",def_FlightLength.c_str(),tree);
  fo_FlightAngle = new TTreeFormula("FlightAngle",def_FlightAngle.c_str(),tree);


  for ( int v = 0 ; v < fo_FlightLength->GetNcodes() ; ++v) {
    m_branches.insert(fo_FlightLength->GetLeaf(v)->GetBranch());
  }
  for ( int v = 0 ; v < fo_FlightAngle->GetNcodes() ; ++v) {
    m_branches.insert(fo_FlightAngle->GetLeaf(v)->GetBranch());
  }

  const string methodName("LD method"),
      weightfile(Form("%s/utils/BMomentumRegression/TMVARegression_LD.weights.xml",
                      gSystem->Getenv("BSTOKMUNUROOT")));
  Float_t f_FlightLength , f_FlightAngle;

  TMVA::Tools::Instance();
  TMVA::Reader *reader = new TMVA::Reader( "!Color:!Silent" );
  reader->AddVariable( def_FlightLength , &f_FlightLength );
  reader->AddVariable( def_FlightAngle  , &f_FlightAngle  );
  reader->BookMVA( methodName.c_str() , weightfile.c_str() );
  // end of setting up for the q2 determination

  vector<TString> bdt_files = {
    "TMVA_charge_BDT",
    "TMVA_neutral_BDT",
    "TMVA_charge_neutral_BDT",
    "TMVA_inclb_BDT",
    "TMVA_inclb_afc_BDT",
    "TMVA_SS_aftercut_BDT"
  };
  for (auto s: bdt_files) {
    std::cout << Form("/eos/lhcb/wg/semileptonic/Bs2KmunuAna/TMVAWeights/Alessio/Weights/Weight_%s.weights.xml",s.Data()) << std::endl;
    reader_wrapper temp;
    temp.SetTree(tree);
    temp.SetXMLFile(Form("/eos/lhcb/wg/semileptonic/Bs2KmunuAna/TMVAWeights/Alessio/Weights/Weight_%s.weights.xml",s.Data()));
    temp.SetTargetBranch(s.Data());
    bdts.push_back(temp);
  }



  // Iwan's BDTs

  std::string MagnetPolarity      = ( m_filename.find("Up") < m_filename.size() ) ? "Up" : "Dn";
  std::string WrongMagnetPolarity = ( m_filename.find("Up") < m_filename.size() ) ? "Dn" : "Up";
  {
    reader_wrapper temp;
    temp.SetTree(tree);
    temp.SetXMLFile("/eos/lhcb/wg/semileptonic/Bs2KmunuAna/TMVAWeights/Iwan/Weights/BDT_Charge_Training_" + WrongMagnetPolarity + "_BDT.weights.xml");
    temp.SetTargetBranch("TMVA_Charge_BDT_New");
    bdts.push_back(temp);


    reader_wrapper tempTrain;
    tempTrain.SetTree(tree);
    tempTrain.SetXMLFile("/eos/lhcb/wg/semileptonic/Bs2KmunuAna/TMVAWeights/Iwan/Weights/BDT_Charge_Training_" + MagnetPolarity + "_BDT.weights.xml");
    tempTrain.SetTargetBranch("TMVA_Charge_BDT_New_Training");
    bdts.push_back(tempTrain);


    reader_wrapper temp2;
    temp2.SetTree(tree);
    temp2.SetXMLFile("/eos/lhcb/wg/semileptonic/Bs2KmunuAna/TMVAWeights/Iwan/Weights/BDT_SS_Training_" + WrongMagnetPolarity + "_BDT.weights.xml");
    temp2.SetTargetBranch("TMVA_SS_Afc_BDT_New");
    bdts.push_back(temp2);


    reader_wrapper tempTrain2;
    tempTrain2.SetTree(tree);
    tempTrain2.SetXMLFile("/eos/lhcb/wg/semileptonic/Bs2KmunuAna/TMVAWeights/Iwan/Weights/BDT_SS_Training_" + MagnetPolarity + "_BDT.weights.xml");
    tempTrain2.SetTargetBranch("TMVA_SS_Afc_BDT_New_Training");
    bdts.push_back(tempTrain2);


  }



  vector<BDT_RW> BDTReWeighters;
  if( tree->GetBranch("B_s0_TRUEP_E"))// Only run the BDTReWeighter i
  {
      tree->SetAlias("Bs_PT",     "B_s0_TRUEPT");
      tree->SetAlias("Bs_ETA",    "0.5*log((B_s0_TRUEP_E+B_s0_TRUEP_Z)/(B_s0_TRUEP_E-B_s0_TRUEP_Z))");
      tree->SetAlias("kaon_m_PT", "Kminus_TRUEPT");
      tree->SetAlias("muon_p_PT", "muplus_TRUEPT");
  }

  if( tree->GetBranch("kaon_m_TRUEP_E") or tree->GetBranch("B_s0_TRUEP_E"))// Only run the BDTReWeighter i
  {

    // Add two branches, one for training, one for testing
    reader_wrapper BDTRW;
    BDTRW.SetTree(tree);
    BDTRW.SetXMLFile("/eos/lhcb/wg/semileptonic/Bs2KmunuAna/TMVAWeights/RW/weights/Weight_Training_MC_RW_" + WrongMagnetPolarity + "_BDT.weights.xml");
    BDTRW.SetTargetBranch("BDT_RW_Eval");
    bdts.push_back(BDTRW);


    BDTReWeighters.emplace_back(
        "/eos/lhcb/wg/semileptonic/Bs2KmunuAna/TMVAWeights/RW/Training_MC_RW_" + WrongMagnetPolarity + ".root",
        bdts.back(), rTree2, "RW_BDT");

    reader_wrapper BDTRWTrain;
    BDTRWTrain.SetTree(tree);
    BDTRWTrain.SetXMLFile("/eos/lhcb/wg/semileptonic/Bs2KmunuAna/TMVAWeights/RW/weights/Weight_Training_MC_RW_" + MagnetPolarity + "_BDT.weights.xml");
    BDTRWTrain.SetTargetBranch("BDT_RW_Eval_Training");
    bdts.push_back(BDTRWTrain);



    BDTReWeighters.emplace_back(
        "/eos/lhcb/wg/semileptonic/Bs2KmunuAna/TMVAWeights/RW/Training_MC_RW_" + MagnetPolarity + ".root",
        bdts.back(), rTree2, "RW_BDT_Training");


  }


  // -- do some string replace
  for( std::pair<string, string> replace : m_stringReplace ) std::cout << " Will replace *" << replace.first << "* with *" << replace.second << "*" << std::endl;
  std::cout << " ============================= " << std::endl;

  vector<string> m_iVals_replaced = m_iVals;
  vector<string> m_bVals_replaced = m_bVals;

  for( string& bla : m_dVals ) {
    for( std::pair<string, string> replace : m_stringReplace ){
      if( bla.find(replace.first) == string::npos ) continue;
      bla.replace(bla.find(replace.first), replace.first.length(), replace.second);
    }
  }
  for( string& bla : m_iVals_replaced ) {
    for( std::pair<string, string> replace : m_stringReplace ){
      if( bla.find(replace.first) == string::npos ) continue;
      bla.replace(bla.find(replace.first), replace.first.length(), replace.second);
    }
  }
  for( string& bla : m_bVals_replaced ) {
    for( std::pair<string, string> replace : m_stringReplace ){
      if( bla.find(replace.first) == string::npos ) continue;
      bla.replace(bla.find(replace.first), replace.first.length(), replace.second);
    }
  }

  // ---------------------------

  if( !m_convertToFloat )
  {
    for( unsigned int i = 0; i < m_dVals.size(); ++i )
        rTree2->Branch(m_dVals[i].c_str(), doubleVarsAddr[i]                              , (m_dVals[i]+"/D").c_str() );
  }
  else
  {
    for( unsigned int i = 0; i < m_dVals.size(); ++i )
        rTree2->Branch(m_dVals[i].c_str(), &floatVars[i]                                  , (m_dVals[i]+"/F").c_str() );
  }
  for( unsigned int i = 0; i < m_iVals.size(); ++i )
      rTree2->Branch(m_iVals[i].c_str(), tree->GetBranch(m_iVals[i].c_str())->GetAddress(), (m_iVals[i]+"/I").c_str() );

  for( unsigned int i = 0; i < m_bVals.size(); ++i )
      rTree2->Branch(m_bVals[i].c_str(), tree->GetBranch(m_bVals[i].c_str())->GetAddress(), (m_bVals[i]+"/O").c_str() );

  for (unsigned int i = 0 ; i < m_form.size() ; ++i)
  {
    rTree2->Branch(m_form[i].first.c_str(), &(m_formVars[i]), (m_form[i].first + "/F").c_str());
  }

  int bdt_errorcode = 0;
  for (auto& bdt: bdts) {
    bdt_errorcode |= bdt.getVariables();
    bdt_errorcode |= bdt.bookReader();
    bdt_errorcode |= bdt.SetOutTree(rTree2);
    bdt_errorcode |= bdt.initFormulas(true);
    for (auto branch: bdt.getBranches()) {
      m_branches.insert(branch);
    }
    if (bdt_errorcode) {
      std::cerr << "BDT complains" << std::endl;
      return bdt_errorcode;
    }
  }
  tree->SetBranchStatus("*",0);
  m_branches.erase((TBranch*)nullptr);
  for (auto b: m_branches) b->SetStatus(1);
  for (auto& bdt: bdts) {
    bdt_errorcode |= bdt.activateBranches();
    if (bdt_errorcode) {
      std::cerr << "BDT complains" << std::endl;
      return bdt_errorcode;
    }
  }


  // -- finally, loop through the tree and copy the entries
  std::cout << " Looping over events and copy the entries" << std::endl;
  int percentCounter = 1;

  Long64_t Passentries = tree->GetEntries();
  for (Long64_t i = 0 ; ; ++i) {
    Long64_t entryNumber = tree->GetEntryNumber(i);
    if (entryNumber < 0)
        {
            std::cout << entryNumber << std::endl;
            break;
        }
    Long64_t localEntry = tree->LoadTree(entryNumber);
    if (localEntry < 0) break;


    const int percent = (int)(entries/100.0);

    if( localEntry > percent*percentCounter ){
      std::cout << percentCounter << " % Total Events: " << entryNumber << " Good Events: " << i << " Total Events: " << entries << std::endl;
      percentCounter++;
    }
    for (auto b: m_branches) {
      try {
        b->GetEntry(localEntry);
      } catch(...) {
        std::cout << "problem with branch: " << b->GetName() << std::endl;
      }
    }

    for (auto& bdt: bdts) {
      bdt.Evaluate();
      //bdt.m_response; // Value obtained from BDT
    }
    for( auto& BDTReWeighter: BDTReWeighters)
      BDTReWeighter.Calculate();

    if ( m_convertToFloat ) {
      for ( unsigned int v = 0; v < m_dVals.size(); ++v ) {
        floatVars[v] = (float)( *doubleVarsAddr[v] );
      }
    }
    for ( size_t v = 0; v < m_formulas.size(); ++v ) {
      m_formVars[v] = m_formulas[v]->EvalInstance();
    }

    // evaluate the regression based b momentum estimate
    f_FlightLength       = fo_FlightLength->EvalInstance();
    f_FlightAngle        = fo_FlightAngle->EvalInstance();
    Bs_Regression_P_REGR = reader->EvaluateRegression( "LD method" )[0];

    // Compute the 2 momentum solutions

    // 3-vector along the flight direction of the b
    TVector3 Flight(
        treeBranchManager.Bs_ENDVERTEX_X - treeBranchManager.Bs_OWNPV_X,
        treeBranchManager.Bs_ENDVERTEX_Y - treeBranchManager.Bs_OWNPV_Y,
        treeBranchManager.Bs_ENDVERTEX_Z - treeBranchManager.Bs_OWNPV_Z);

    // the 4-momentum of the visible system
    TLorentzVector VIS(treeBranchManager.Bs_PX, treeBranchManager.Bs_PY,
                       treeBranchManager.Bs_PZ, treeBranchManager.Bs_PE);

    // the 4-momentum of the recoil system under the Kmunu hypothesis
    // where the kaon is the recoil.
    // this needs to be generalised to cover the Dsmunu case
    TLorentzVector RECOIL;

    if ( treeBranchManager.b_Ds_PX )
    {
        RECOIL = TLorentzVector(
          treeBranchManager.Ds_PX, treeBranchManager.Ds_PY,
          treeBranchManager.Ds_PZ, treeBranchManager.Ds_PE);
    }
    else
    {
        RECOIL = TLorentzVector(
          treeBranchManager.kaon_m_PX, treeBranchManager.kaon_m_PY,
          treeBranchManager.kaon_m_PZ, treeBranchManager.kaon_m_PE);
    }

    // the assumed mass of the parent B
    // would need to be generalised to cover something other than just Bs mesons
    const float mB(5366.8);

    // the quadratic equation takes as input
    // - the visible 4-momentum
    // - the flight vector
    // - the assumed mass
    float Psolutions[2];

    TLorentzVector BSol[2];
    Q2calculation::QuadraticMomentum(Psolutions,VIS,Flight,mB,Bs_Regression_Q2_Discriminant, BSol[0], BSol[1]);

    // which of the two solutions is closer to the regression based estimate?
    const int iBestSolution = fabs(Psolutions[0]-Bs_Regression_P_REGR) < fabs(Psolutions[1]-Bs_Regression_P_REGR) ? 0 : 1;

    // now compute the variables for the output tree
    Bs_Regression_P_HI      = Psolutions[0];                                          // larger momentum solution
    Bs_Regression_P_LO      = Psolutions[1];                                          // smaller momentum solution
    Bs_Regression_P_BEST    = Psolutions[iBestSolution];                              // the best momentum solution
    Bs_Regression_Q2_HI     = Q2calculation::Q2( RECOIL, Flight, mB, Psolutions[0] ); // Q2 for higher momentum
    Bs_Regression_Q2_LO     = Q2calculation::Q2( RECOIL, Flight, mB, Psolutions[1] ); // Q2 for lower momentum
    Bs_Regression_Q2_BEST   = Q2calculation::Q2( RECOIL, Flight, mB, Psolutions[iBestSolution] );            // best Q2
    Bs_Regression_Q2_WORST  = Q2calculation::Q2( RECOIL, Flight, mB, Psolutions[iBestSolution^1 ] ); // worst Q2
    Bs_Regression_Q2_RANDOM = Q2calculation::Q2( RECOIL, Flight, mB, Psolutions[i % 2] ); // random Q2

    if ( Bs_Regression_Q2_HI == -1000)
    {
        BSol[0].SetXYZM(0,0,0,0);
        BSol[1].SetXYZM(0,0,0,0);
    }


    Bs_Reg_Best_PX = BSol[iBestSolution].Px();
    Bs_Reg_Best_PY = BSol[iBestSolution].Py();
    Bs_Reg_Best_PZ = BSol[iBestSolution].Pz();
    Bs_Reg_Best_PE = BSol[iBestSolution].E();

    Bs_Reg_Worst_PX = BSol[1-iBestSolution].Px();
    Bs_Reg_Worst_PY = BSol[1-iBestSolution].Py();
    Bs_Reg_Worst_PZ = BSol[1-iBestSolution].Pz();
    Bs_Reg_Worst_PE = BSol[1-iBestSolution].E();



    if ( treeBranchManager.b_Bs_TRUEP_Z ) {
      TLorentzVector TRUE_Bs(
          treeBranchManager.Bs_TRUEP_X, treeBranchManager.Bs_TRUEP_Y,
          treeBranchManager.Bs_TRUEP_Z, treeBranchManager.Bs_TRUEP_E);

      TLorentzVector TRUE_RECOIL;

      if ( treeBranchManager.b_Ds_PX )
      {
          TRUE_RECOIL = TLorentzVector(
              treeBranchManager.Ds_TRUEP_X, treeBranchManager.Ds_TRUEP_Y,
              treeBranchManager.Ds_TRUEP_Z, treeBranchManager.Ds_TRUEP_E);
      }
      else
      {
          TRUE_RECOIL = TLorentzVector(
              treeBranchManager.kaon_m_TRUEP_X, treeBranchManager.kaon_m_TRUEP_Y,
              treeBranchManager.kaon_m_TRUEP_Z, treeBranchManager.kaon_m_TRUEP_E);
/*
          std::cout << treeBranchManager.kaon_m_TRUEP_X << "  ";
          std::cout << treeBranchManager.kaon_m_TRUEP_Y << "  ";
          std::cout << treeBranchManager.kaon_m_TRUEP_Z << "  ";
          std::cout << treeBranchManager.kaon_m_TRUEP_E << "  ";

          std::cout << treeBranchManager.Bs_TRUEP_X << "  ";
          std::cout << treeBranchManager.Bs_TRUEP_Y << "  ";
          std::cout << treeBranchManager.Bs_TRUEP_Z << "  ";
          std::cout << treeBranchManager.Bs_TRUEP_E << "\n";
*/
      }


//      std::cout << (TRUE_Bs - TRUE_RECOIL).M2() << "\n";


      Bs_Regression_Q2_TRUE = (TRUE_Bs - TRUE_RECOIL).M2();
    }

    rTree2->Fill();

  }

  rFile->WriteTObject(rTree2);
  rFile->Close();

  std::cout << "DONE" << std::endl;

  delete rFile;
  for (auto i_dont_like_to_clean_up : m_formulas) {
    delete i_dont_like_to_clean_up;
    i_dont_like_to_clean_up = nullptr;
  }
  std::cout << " ========= done ============== " << std::endl;
  return EXIT_SUCCESS;
}
// =====================================================
// -- copy everything with cuts
// =====================================================
int reducer::copyTree(){

  if( m_filename  == "" || m_outFilename == "" || m_treename  == "" ){
    std::cout << "You need to define the input filename / treename / output filename" << std::endl;
    return 3;
  }

  std::cout << " ============================= " << std::endl;
  std::cout << " Input filename:  " << m_filename << std::endl;
  std::cout << " treename:        " << m_treename << std::endl;
  std::cout << " Output filename: " << m_outFilename << std::endl;
  std::cout << " Will copy all variables " << std::endl;
  std::cout << " ============================= " << std::endl;
  std::cout << " Cuts: " << m_cuts << std::endl;
  std::cout << " ============================= " << std::endl;

  TFile* file = TFile::Open( m_filename.c_str() );
  if( !file ){
    std::cout << "file " << m_filename << " not found" << std::endl;
    return 2;
  }

  TTree* tree = (TTree*)file->Get( m_treename.c_str() );
  if( !tree ){
    std::cout << "tree " << m_treename << " not found" << std::endl;
    return 2;
  }


  TFile* outfile = new TFile(m_outFilename.c_str(),"RECREATE");
  if (nullptr == outfile) {
    return 1;
  }
  TTree* rTree1 = tree->CopyTree( m_cuts );
  rTree1->SetName( tree->GetName() );
  outfile->Write();
  outfile->Close();
  return 0;
}
