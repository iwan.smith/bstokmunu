# How to Trim:

There are two commands which will trim all data and Monte Carlo:

### Trimming Data:
```shell
bash TrimData.sh
```

### Trimming MC:
```shell
bash TrimMC.sh
```

### Trimming only one file:
```shell
bash TrimFile.sh PartialPath/FileName
```


# How the trimming works

* The trimmer is run on an individual file e.g. `./Trimmer Bs2KMuNu_DATATUPLES_RAW_19June16/161_DTT_2012_Reco14Strip21r0p1a_Down_SEMILEPTONIC.root`
* A new NTuple is created for every Tree within the file:
    * `Bs2KMuNu_DATATUPLES_TRIMMED_19June16/161_DTT_2012_Reco14Strip21r0p1a_Down_SEMILEPTONIC_LumiTuple.root`
    * `Bs2KMuNu_DATATUPLES_TRIMMED_19June16/161_DTT_2012_Reco14Strip21r0p1a_Down_SEMILEPTONIC_Bs2KmuNuTuple.root`
    * `Bs2KMuNu_DATATUPLES_TRIMMED_19June16/161_DTT_2012_Reco14Strip21r0p1a_Down_SEMILEPTONIC_Bs2KmuNuTuple_fakeKMu.root`
    * `Bs2KMuNu_DATATUPLES_TRIMMED_19June16/161_DTT_2012_Reco14Strip21r0p1a_Down_SEMILEPTONIC_Bs2DsMuNuSSTuple.root`
    * `Bs2KMuNu_DATATUPLES_TRIMMED_19June16/161_DTT_2012_Reco14Strip21r0p1a_Down_SEMILEPTONIC_Bs2DsMuNuTuple.root`
    * `Bs2KMuNu_DATATUPLES_TRIMMED_19June16/161_DTT_2012_Reco14Strip21r0p1a_Down_SEMILEPTONIC_Bs2KmuNuTupleSS_fakeMu.root`
    * `Bs2KMuNu_DATATUPLES_TRIMMED_19June16/161_DTT_2012_Reco14Strip21r0p1a_Down_SEMILEPTONIC_Bs2KmuNuSSTuple.root`
    * etc.
    * If a Tree is missing it is skipped
* The Trimmed files are all merged into one:
    * `hadd Bs2KMuNu_DATATUPLES_TRIMMED_19June16/161_DTT_2012_Reco14Strip21r0p1a_Down_SEMILEPTONIC.root Bs2KMuNu_DATATUPLES_TRIMMED_19June16/161_DTT_2012_Reco14Strip21r0p1a_Down_SEMILEPTONIC_*.root`
* And the temporary files removed:
    * `rm Bs2KMuNu_DATATUPLES_TRIMMED_19June16/161_DTT_2012_Reco14Strip21r0p1a_Down_SEMILEPTONIC_{LumiTuple,Bs2KmuNuTuple,Bs2KmuNuTuple_fakeKMu.etc.}.root`
    