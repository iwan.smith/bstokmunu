#include "CutLibrary.h"
#include <iostream>
#include <utility>
#include <vector>
#include <array>

int main(){


	ForPaul::CutLibrary m_cutlib(ForPaul::ParticleDefinitions(("kaon_m"),("kaon_p"),("pi_p"),("muon_p")));

	std::vector<std::array<std::string, 2>> cuts;



	cuts.push_back({std::string("is_phi"),m_cutlib.getDalitzCut(0)});
	cuts.push_back({std::string("is_kstar"),m_cutlib.getDalitzCut(1)});
	cuts.push_back({std::string("is_nonresonant"),m_cutlib.getDalitzCut(2)});
	cuts.push_back({std::string("DeltaMass"),m_cutlib.getDeltaMass()});
	cuts.push_back({std::string("DeltaMass1"),m_cutlib.getDeltaMass1()});
	cuts.push_back({std::string("DeltaMass2"),m_cutlib.getDeltaMass2()});

	cuts.push_back({std::string("passJpsiCut"),"("+m_cutlib.getDalitzCut(0)+"&&"+m_cutlib.getJPsiCut(0)+")||(!("+m_cutlib.getDalitzCut(0)+")&&"+m_cutlib.getJPsiCut(1)+")"});
	cuts.push_back({std::string("passDplusCut"),"("+m_cutlib.getDalitzCut(0)+")||(!("+m_cutlib.getDalitzCut(0)+")&&"+m_cutlib.getDplusCut()+")"});
	cuts.push_back({std::string("passLambdaCut"),"(("+m_cutlib.getDalitzCut(0)+")&&("+m_cutlib.getLambdaCut(0)+"))||(!("+m_cutlib.getDalitzCut(0)+")&&("+m_cutlib.getLambdaCut(1)+"))"});
	cuts.push_back({std::string("passKstarCut"),"("+m_cutlib.getDalitzCut(0)+")||(!("+m_cutlib.getDalitzCut(0)+")&&"+m_cutlib.getKstarCut()+")"});
	cuts.push_back({std::string("passDstarCut1"),"("+m_cutlib.getDalitzCut(0)+")||(!("+m_cutlib.getDalitzCut(0)+")&&"+m_cutlib.getDstarCut1()+")"});
	cuts.push_back({std::string("passDstarCut2"),"("+m_cutlib.getDalitzCut(0)+")||(!("+m_cutlib.getDalitzCut(0)+")&&"+m_cutlib.getDstarCut2()+")"});
	cuts.push_back({std::string("passDstarPlusCut"),"(!("+m_cutlib.getDalitzCut(2)+"))||(("+m_cutlib.getDalitzCut(2)+")&&"+m_cutlib.getDstarPlusCut()+")"});


	for (std::array<std::string, 2> cutval: cuts)
		std::cout << cutval[0] << "\t\t" << cutval[1] << std::endl;

	return 1;


}
