#ifndef Q2CALC_H
#define Q2CALC_H

#include <vector>
#include <iostream>
#include <TLorentzVector.h>

namespace Q2calculation {    

  void QuadraticMomentum( float * solutions, TLorentzVector & VIS, TVector3 & FLIGHT, const float mB, float& Discriminant, TLorentzVector& B1, TLorentzVector& B2);
  float Q2( TLorentzVector & D, TVector3 & FLIGHT, const float mB,const float pB);
}


#endif
