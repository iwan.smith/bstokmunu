#include "BDTRW.h"


// =====================================================
// -- Samll Class for reWeighting data from a BDT
// =====================================================

BDT_RW::BDT_RW( string BDTFileName, reader_wrapper& BDT, TTree* T_Out, string BranchName )
  :m_BDT(BDT)
  ,m_RW(new float(0))
{
  TFile* f_BDTRW = TFile::Open(BDTFileName.c_str());

  m_h_num = std::shared_ptr<TH1>( (TH1*)new TH1F("h_B", "h_B", 400, -0.3, 0.3) );
  m_h_den = std::shared_ptr<TH1>( (TH1*)new TH1F("h_S", "h_S", 400, -0.3, 0.3) );

  ((TTree*)f_BDTRW->Get("TrainTree")) ->Draw("BDT>>+h_S", "classID == 0");
  ((TTree*)f_BDTRW->Get("TrainTree")) ->Draw("BDT>>+h_B", "classID == 1");



  m_h_num->SetDirectory(0);
  m_h_den->SetDirectory(0);

  SmoothBins(m_h_num.get(), m_h_den.get());

  m_h_num->Scale( 1.0 / m_h_num->Integral() );
  m_h_den->Scale( 1.0 / m_h_den->Integral() );

  f_BDTRW->Close();

  /*
  TFile* f_out = TFile::Open(("OutHist"+ BranchName + ".root").c_str(), "RECREATE");
  m_h_num->Write();
  m_h_den->Write();
  f_out->Close();
  //std::abort();
  */



  T_Out->Branch(BranchName.c_str(), m_RW.get(), (BranchName+ "/F").c_str() );

};

void BDT_RW::Calculate()
{
  const int Bin = m_h_num->FindBin(m_BDT.m_response);
  *m_RW = m_h_num->GetBinContent(Bin) / m_h_den->GetBinContent(Bin);


  if ( TMath::IsNaN(*m_RW) )
    *m_RW = 1.0;
}

