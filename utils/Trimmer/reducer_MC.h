#ifndef REDUCER_MC
#define REDUCER_MC

#include <TCut.h>
#include <vector>
#include <algorithm>

class reducer_MC  {

 public:

  reducer_MC(std::string TreeName = "reducedTree");
  virtual ~reducer_MC();

  
 public:
  
  // -- the only essential method
  int reduceTree(std::string OpenHow = "RECREATE");
  // -- copy the tree with a selection
  int copyTree();
  
  
  // members
  TCut m_cuts;
  std::string m_filename;
  std::string m_outFilename;
  std::string m_treename;
  std::string m_OutTreeName;

  bool m_convertToFloat;
  // -- double values
  std::vector<std::string> m_dVals;
  // -- double regexps
  std::vector<std::string> m_dRegs;
  // -- integer values
  std::vector<std::string> m_iVals;
  // -- integer wildcards
  std::vector<std::string> m_iRegs;
  // -- boolean values
  std::vector<std::string> m_bVals;
  // -- do string replace
  std::vector<std::pair<std::string, std::string>> m_stringReplace;
  // -- compute TTreeFormulas
  std::vector<std::pair<std::string, std::string>> m_form;
  
};

#endif
