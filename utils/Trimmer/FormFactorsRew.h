#ifndef FormFactorsRew_h
#define FormFactorsRew_h

#include <string>
#include <TLorentzVector.h>

// https://indico.cern.ch/event/292998/contribution/3/attachments/548061/755401/SL_WG_20140122.pdf

class HTerms
{
public:
// http://stackoverflow.com/questions/3378560/how-to-disable-gcc-warnings-for-a-few-lines-of-code
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wshadow"
	HTerms(double hA1p, double hp, double hm, double hz) :
		_hA1p(hA1p), _hp(hp), _hm(hm), _hz(hz) {;}
#pragma GCC diagnostic pop

	double hA1p() { return _hA1p; }
	double hp() { return _hp; }
	double hm() { return _hm; }
	double hz() { return _hz; }

	double hA1p2() { return _hA1p*_hA1p; }
	double hp2() { return _hp*_hp; }
	double hm2() { return _hm*_hm; }
	double hz2() { return _hz*_hz; }

protected:
	double _hA1p, _hp, _hm, _hz;
};

class FormFactorsRew
{
public:
	FormFactorsRew() {;}

	void SetCurrentModel(double rho2, double hA1, double r1, double r2, std::string model="HQET2");
	void SetNewModel(double rho2, double hA1, double r1, double r2, std::string model="HQET2");

	double GetWeight(TLorentzVector BLab, TLorentzVector MuLab, TLorentzVector DStarLab, TLorentzVector DLab);

protected:
	HTerms CalculateHTerms(double w, double rho2, double hA1, double r1, double r2, unsigned int model);
	double IFF(double w, double ctd, double ctl, double chi, double rho2, double hA1, double r1, double r2, unsigned int model);
	double Rate(double rho2, double hA1, double r1, double r2, unsigned int model);
	double dGammadw(double *w, double *p);

	// model parameters
	double _rho2, _hA1, _r1, _r2;
	double _old_rho2, _old_hA1, _old_r1, _old_r2;
	unsigned int _model, _old_model;
	// holders for intermediate calculations
	double _Rate, _old_Rate, _r;
};

#endif





