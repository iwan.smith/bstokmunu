#!/bin/bash

cd /eos/lhcb/wg/semileptonic/Bs2KmunuAna/Tuples

FILES=`find -iname '*.root'|grep MC|grep RAW|grep -v B2JpsiX_MCTUPLES`

cd -

for FILE in $FILES
    do 
bsub -q 8nh -J $FILE << EOF
echo $FILE
cd /afs/cern.ch/user/i/ismith/ANA/BsToKMuNu/utils
source ./SetupEnv.sh
cd /afs/cern.ch/user/i/ismith/ANA/BsToKMuNu/utils/Trimmer
./Trimmer $FILE
EOF
done

NREMAIN=999
while [ $NREMAIN -gt 0 ]
do
  sleep 180
  NREMAIN=$(bjobs|grep root|wc -l)
  echo $NREMAIN "Jobs Remaining"
done

cd /eos/lhcb/wg/semileptonic/Bs2KmunuAna/Tuples

NSIM=0
for FILE in $FILES
do
  NSIM=`expr $NSIM + 1`
  
  SHORTFILE=`echo $FILE|sed -e 's/RAW/TRIMMED/g'|sed -e 's/.root//g'`
  echo $FILE
  echo $SHORTFILE 
  hadd -O -f -k ${SHORTFILE}.root ${SHORTFILE}_* && \
  rm  -v ${SHORTFILE}_* &
  

  if [ $NSIM -eq 16 ]
  then
    NSIM=0
    wait
  fi
done

wait


