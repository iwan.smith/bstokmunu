/* Author: Mika Vesterinen
 * Purpose: Train a TMVA regression algorithm for the B momentum...
 * ..Ciezarek, Lupato, Rotondo, Vesterinen,  J. High Energ. Phys. (2017) 2017: 21. 
 */
#include "TChain.h"
#include "TFile.h"
#include "TTree.h"
#include "TString.h"
#include "TObjString.h"
#include "TSystem.h"
#include "TROOT.h"
#include "TMVA/Tools.h"
#include "TMVA/Factory.h"
#include <iostream>
#include <map>
#include <string>

int main ( int argc , char** argv ) {

  std::cout << argv[0] << std::endl;

  std::string inputRootFilePath("root://eoslhcb.cern.ch/"\
			"/eos/lhcb/wg/semileptonic/Bs2KmunuAna/Tuples/"\
			"Bs2KMuNu_MCTUPLES_RAW_30May16/DTT_13512010_Bs_Kmunu_DecProdCut_Dn_Py8_MC12.root");
  
  std::string inputTreePath("Bs2KmuNuTuple/DecayTree");

  if ( argc == 3 ) {
    inputRootFilePath = std::string(argv[1]);
    inputTreePath = std::string(argv[2]);
  }

  TMVA::Tools::Instance();
  
  TString outfileName( "TMVAReg.root" );
  TFile* outputFile = TFile::Open( outfileName, "RECREATE" );
  
  TMVA::Factory *factory = new TMVA::Factory( "TMVARegression", outputFile,
					      "!V:!Silent:Color:DrawProgressBar" \
					      ":AnalysisType=Regression" );
  
  factory->AddVariable( "Bs_FD_OWNPV", "FlightLength", "mm", 'D' );
  factory->AddVariable( "1./TMath::Sin(TMath::ACos((Bs_ENDVERTEX_Z-Bs_OWNPV_Z)/Bs_FD_OWNPV))" \
			   , "FlightAngle", "", 'D' );

  factory->AddTarget( "TMath::Sqrt(Bs_TRUEP_X*Bs_TRUEP_X" \
		      "+Bs_TRUEP_Y*Bs_TRUEP_Y"		  \
		      "+Bs_TRUEP_Z*Bs_TRUEP_Z)");
  
  TFile * input = TFile::Open( inputRootFilePath.c_str() );

  TTree * regTree = ( TTree* ) input->Get ( inputTreePath.c_str() );
  
  
  // global event weights per tree (see below for setting event-wise weights)
  const Double_t regWeight  = 1.0;
  
  // You can add an arbitrary number of regression trees
  factory->AddRegressionTree( regTree, regWeight );
  
  // Apply additional cuts on the signal and background samples (can be different)
  TCut mycut = " 1 && Bs_MCORR < 7000 && Bs_MCORR > 2500 && (kaon_m_PIDK - kaon_m_PIDp) > 5 && kaon_m_PIDK > 5 && (kaon_m_PIDK - kaon_m_PIDmu) > 5 && kaon_m_P > 10000 && kaon_m_PT > 500 && (Bs_Hlt2SingleMuonDecision_TOS || Bs_Hlt2TopoMu2BodyBBDTDecision_TOS == 1) && Bs_Q2SOL1!=-1000 && Bs_M > 1900 && totCandidates<2 && !((kaon_m_MasshPi0>832 && kaon_m_MasshPi0<962) && (kaon_m_Pi0_M>112 && kaon_m_Pi0_M<158) || (kaon_m_MasshPi0>1340 && kaon_m_MasshPi0<1520) && (kaon_m_Pi0_M>112 && kaon_m_Pi0_M<158)) && !((sqrt(105.66*105.66+105.66*105.66+ 2.*sqrt(105.66*105.66 + kaon_m_PX*kaon_m_PX + kaon_m_PY*kaon_m_PY + kaon_m_PZ*kaon_m_PZ)*sqrt(105.66*105.66 + muon_p_PX*muon_p_PX + muon_p_PY*muon_p_PY + muon_p_PZ*muon_p_PZ)-2*(kaon_m_PX*muon_p_PX + kaon_m_PY*muon_p_PY + kaon_m_PZ*muon_p_PZ)) > 3072) && (sqrt(105.66*105.66+105.66*105.66+ 2.*sqrt(105.66*105.66 + kaon_m_PX*kaon_m_PX + kaon_m_PY*kaon_m_PY + kaon_m_PZ*kaon_m_PZ)*sqrt(105.66*105.66 + muon_p_PX*muon_p_PX + muon_p_PY*muon_p_PY + muon_p_PZ*muon_p_PZ)-2*(kaon_m_PX*muon_p_PX + kaon_m_PY*muon_p_PY + kaon_m_PZ*muon_p_PZ)) < 3130) && (kaon_m_isMuon==1))"; 
  
  // tell the Factory to use all remaining events in the trees after training for testing:
  factory->PrepareTrainingAndTestTree ( mycut,
					"nTrain_Regression=1000" \
					":nTest_Regression=0"	 \
					":SplitMode=Random"	 \
					":NormMode=NumEvents:!V" );
  
  
  factory->BookMethod( TMVA::Types::kLD, "LD",
		       "!H:!V:VarTransform=None" );
  
  // Train MVAs using the set of training events
  factory->TrainAllMethods();
  
  // Evaluate all MVAs using the set of test events
  factory->TestAllMethods();
  
  // Evaluate and compare performance of all configured MVAs
  factory->EvaluateAllMethods();
  
  // --------------------------------------------------------------
  
  // Save the output
  outputFile->Close();
    
  delete factory;

  return 0;

}
//end
