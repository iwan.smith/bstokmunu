#!/usr/bin/python
import ROOT

import os
ROOT.gROOT.ProcessLine(".x "+os.environ["BSTOKMUNUROOT"]+"/include/official.C")
ROOT.gStyle.SetPalette(57)
ROOT.gStyle.SetPaintTextFormat("4.1f");

input = ROOT.TFile.Open("root://eoslhcb.cern.ch//eos/lhcb/wg/semileptonic/Bs2KmunuAna/Tuples/Bs2KMuNu_MCTUPLES_TRIMMED_30May16/DTT_13512010_Bs_Kmunu_DecProdCut_Dn_Py8_MC12_trimmed.root");
input.ls()
tree = input.Get("reducedTree")
tree.Show(1)

c = ROOT.TCanvas()


CUTS = "TMath::Abs(Bs_TRUEID) == 531 && Bs_Regression_Q2_TRUE > 0.001" # && Bs_Regression_Q2_LO > 0 && Bs_Regression_P_HI > 0"
nBins2D = 200
hist2D = ROOT.TH2F("hist2D","",nBins2D,0,22.7,nBins2D,0,22.7)
hist2D.SetXTitle("True q^{2} [GeV^{2}]")
hist2D.SetYTitle("Rec. q^{2} [GeV^{2}]")

nBins1D = 10
histNumer = ROOT.TH1F("numer","",nBins1D,0,22.7)
histDenom = ROOT.TH1F("denom","",nBins1D,0,22.7)
histNumer.Sumw2()
histDenom.Sumw2()
for ax in [hist2D.GetXaxis(),hist2D.GetYaxis()]:ax.CenterTitle()
for var in ["BEST","RANDOM"]:
    histNumer.Reset("ICE")
    histDenom.Reset("ICE")
    tree.Draw("Bs_Regression_Q2_%s/1.e6:Bs_Regression_Q2_TRUE/1.e6>>hist2D"%var,CUTS,"COLZ")
    hist2D.SetContour(10000)
    hist2D.Draw("COLZ")
    hist2D.SetMarkerSize(4)
    hist2D.SetMarkerColor(ROOT.kRed)
    hist2D.Scale(100./hist2D.Integral())
    c.SetRightMargin(0.15)
    c.Print("MIGR_"+var+".png")
    c.SetRightMargin(0.05)
    #tree.Draw("Bs_Regression_Q2_TRUE/1.e6>>denom",CUTS)
    for opt in ["Stability","Purity"]:
        tree.Draw("Bs_Regression_Q2_%s/1.e6>>denom" %("TRUE" if opt == "Stability" else var),CUTS)
        for iBin in range(1,histNumer.GetNbinsX()+1):
            r = [histNumer.GetBinLowEdge(iBin),histNumer.GetBinLowEdge(iBin+1)]
            v = {"var":var,
                 "lo":r[0],
                 "hi":r[1]}
            histNumer.SetBinContent(iBin,tree.GetEntries("Bs_Regression_Q2_%(var)s/1.e6 > %(lo)s"\
                                                             "&& Bs_Regression_Q2_%(var)s/1.e6 < %(hi)s"\
                                                             "&& Bs_Regression_Q2_TRUE/1.e6 > %(lo)s"\
                                                             "&& Bs_Regression_Q2_TRUE/1.e6 < %(hi)s" %v))
        histNumer.Divide(histNumer,histDenom,1,1,"B")
        histNumer.Draw()
        histNumer.SetMarkerColor(ROOT.kBlue)
        histNumer.SetLineColor(ROOT.kBlue)
        histNumer.SetAxisRange(0,1,"Y")
        histNumer.SetXTitle("%s #[it]{q}^{2} [GeV^{2}/#[it]{c}^{2}]" %("Reconstructed" if opt == "Purity" else "True"))
        histNumer.SetYTitle("Bin purity")
        c.SetRightMargin(0.05)
        c.Print(opt+"_"+var+".png")
    
