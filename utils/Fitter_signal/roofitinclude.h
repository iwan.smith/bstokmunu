// C++
#include <fstream>
#include <iostream>
#include <ostream>
#include <sstream>
#include "Riostream.h"
#include <cmath>
#include <cstdlib>
#include <time.h>
#include <vector>
#include <iomanip>
// ROOT 
#include <TChain.h>
#include <TROOT.h>
#include "TSystem.h"
#include <TCanvas.h>
#include <TLegend.h>
#include <TLatex.h>
#include <TFile.h>
#include <TH2.h>
#include <TKey.h>
#include <TPad.h>
#include <TLegend.h>
#include <TPaveText.h>
#include <TH1.h>
#include <TF1.h>
#include <TSystem.h>
#include <TTree.h>
#include <TIterator.h>
#include <TProfile.h>
#include <TLorentzVector.h>
//#include "Math/Vector4D.h"
#include <TStopwatch.h>
#include "TMath.h"
#include "TMathText.h"
#include <TAttMarker.h>
#include <TStyle.h>
#include <TPaveStats.h>
#include <TArrow.h>
#include "TEventList.h"
#include "TEntryList.h"
#include "TList.h"
#include "TH3D.h"
#include "TPie.h"
#include "TGraphErrors.h"
#include "TGaxis.h"
#include "TMultiGraph.h"
#include "TFitResultPtr.h"
#include "TFitResult.h"
// ROOFIT
#include "RooMCStudy.h"
#include "RooKeysPdf.h"
#include <RooAbsReal.h>
#include "RooAddition.h"
#include "RooAbsData.h"
#include <RooAddModel.h>
#include <RooAddPdf.h>
#include <RooArgSet.h>
#include <RooBDecay.h>
#include <RooBCPGenDecay.h>
#include <RooCategory.h>
#include <RooCmdArg.h>
#include <RooArgusBG.h>
#include <RooDataSet.h>
#include <RooDataHist.h>
#include <RooDecay.h>
#include <RooEffProd.h>
#include <RooExponential.h>
#include <RooExtendPdf.h>
#include <RooFit.h>
#include <RooHist.h>
#include <RooFitResult.h>
#include <RooGExpModel.h>
#include <RooGaussian.h>
#include <RooGaussModel.h>
#include <RooGenericPdf.h>
#include <RooGlobalFunc.h>
#include <RooHistPdf.h>
#include <RooHistFunc.h>
#include <RooMinuit.h>
#include <RooNLLVar.h>
#include <RooChi2Var.h>
#include "RooNumConvPdf.h"
#include "RooNumIntConfig.h"
#include "RooNumConvolution.h"
#include <RooPlot.h>
#include <RooProdPdf.h>
#include "RooProduct.h"
#include "RooPolynomial.h"
#include "RooPolyVar.h"
#include <RooRandom.h>
#include <RooRealVar.h>
#include "RooRealSumPdf.h"
#include <RooRealConstant.h>
#include <RooResolutionModel.h>
#include <RooRealIntegral.h>
#include <RooSuperCategory.h>
#include <RooBinning.h>
#include <RooBMixDecay.h>
#include <RooTruthModel.h>
#include <RooThresholdCategory.h>
#include <RooSimultaneous.h>
#include <RooSimPdfBuilder.h>
#include <RooTable.h>
#include <Roo1DTable.h>
#include "RooFFTConvPdf.h"
#include "RooDstD0BG.h"
#include "RooLandau.h"
#include "RooVoigtian.h"
#include "RooCBShape.h"
#include "RooBreitWigner.h"
#include "RooBifurGauss.h"
#include "RooArgusBG.h"
#include "RooWorkspace.h"
#include "RooBernstein.h"
#include "RooMsgService.h"
#include "RooPoisson.h"
#include "RooUnblindPrecision.h"
#include "RooUnblindUniform.h"
// RooStats
#include "RooStats/SPlot.h"
#include "RooChebychev.h"
#include "TPaveLabel.h"
