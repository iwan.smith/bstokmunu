#include "/afs/cern.ch/user/b/bkhanji/roofitinclude.h"
#include <iostream>
#include <TROOT.h>
#include <TLatex.h>
#include <TFile.h>
#include <TTree.h>
#include <sstream>
#include <TChain.h>
#include <TCanvas.h>
#include <TObjArray.h>
#include <TBranch.h>
#include <TLorentzVector.h>
#include <iomanip>
#include <TH1F.h>
#include "RooStats/ModelConfig.h"
#include "RooStats/HistFactory/Measurement.h"
#include "RooStats/HistFactory/MakeModelAndMeasurementsFast.h"
#include "RooStats/HistFactory/HistFactorySimultaneous.h"
#include "RooStats/HistFactory/HistFactoryNavigation.h"
#include "assert.h"

using namespace RooFit;
using namespace RooStats;
using namespace HistFactory;
using namespace std;


void CheckNonZeroBin(TH1* h=0){
    Int_t binx = h->GetNbinsX();
    for(Int_t i = 0; i<= binx; i++){
    	if(h->GetBinContent(i)<=0) {
	  //cout<<"ZERO BIN FOUND at: "<<i<<", "<<endl;
	h->SetBinContent(i,1e-12);//set empty bins to 1e-12
	}
      }
  }

void DecorateHisto(TH1D* Data_CorrMass=0)
{
  //gPad->SetGrid();  
  //gPad->SetTickx();
  //gPad->SetTicky();
  //Data_CorrMass->SetTitle("");
  //gStyle->SetOptStats(1111);
  Data_CorrMass->SetStats(kTRUE);
  Data_CorrMass->GetYaxis()->SetTitle("Events / ( 48 MeV )");
  Data_CorrMass->GetXaxis()->SetTitle("Bs Corr. mass (MeV)");
  //Data_CorrMass->GetYaxis()->SetTitleOffset(0.9);
  //Data_CorrMass->SetStats(0);
  //Data_CorrMass->SetMinimum(0);
  //Data_CorrMass->GetYaxis()->SetNdivisions(7+100*5);
}

void DecorateHisto2(TH1D* Data_CorrMass=0)
{
  //gPad->SetGrid();  
  //gPad->SetTickx();
  //gPad->SetTicky();
  //Data_CorrMass->SetTitle("");
  //gStyle->SetOptStats(1111);
  Data_CorrMass->SetStats(kTRUE);
  //Data_CorrMass->GetYaxis()->SetTitle("Events / ( 48 MeV^{2} )");
  //Data_CorrMass->GetXaxis()->Setions(7+100*5);
  Data_CorrMass->GetXaxis()->SetTitle("Q^{2} BEST (MeV^{2})");
  //Data_CorrMass->GetYaxis()->SetTitleOffset(0.9);
  //Data_CorrMass->SetStats(0);
  //Data_CorrMass->SetMinimum(0);
  
  //Data_CorrMass->GetYaxis()->SetNdivisions(7+100*5);
}



TH1D* GetQ2(TString data_PATH="" , TString name = ""  , TString MyCuts= "" )
{
  
  TChain *fdata_chain = new TChain("data_OS","data_OS");
  fdata_chain->Add( data_PATH );
  fdata_chain->Print();
  TDirectory *where = gDirectory;
  where->cd();
  //TH1D *Data_CorrMass = new TH1D( name+ "_Bs_MCORR" , name+ "_Bs_MCORR" , 40 , 2500 , 5370  ) ;
  TH1D *Data_Q2_BEST = new TH1D( name , name , 100 , -5000000, 25000000  ) ;
  Data_Q2_BEST->Sumw2();
  fdata_chain->Project(Data_Q2_BEST->GetName() , "Bs_Regression_Q2_BEST" , MyCuts , "" , 100000000000, 0);//, "" ); 
  cout<< "Name of histo: "<< Data_Q2_BEST->GetName()<< endl;
  //Data_Q2_BEST->Scale(1./1000000);
  TCanvas *q2 = new TCanvas("q2_"+name , "q2_" + name );
  q2->cd();
  DecorateHisto2(Data_Q2_BEST);
  //Data_CorrMass->SetTitle("");
  Data_Q2_BEST->Draw();  
  return Data_Q2_BEST;
  }

TH1D* GetCorr_h(TString data_PATH="" , TString name = ""  , TString MyCuts= "" )
{
  
  TChain *fdata_chain = new TChain("data_OS","data_OS");
  fdata_chain->Add( data_PATH );
  fdata_chain->Print();
  TDirectory *where = gDirectory;
  where->cd();
  //TH1D *Data_CorrMass = new TH1D( name+ "_Bs_MCORR" , name+ "_Bs_MCORR" , 40 , 2500 , 5370  ) ;
  TH1D *Data_CorrMass = new TH1D( name+ "_Bs_MCORR" , name+ "_Bs_MCORR" , 60 , 2500, 5380  ) ;
  Data_CorrMass->Sumw2();
  fdata_chain->Project(Data_CorrMass->GetName() , "Bs_MCORR" , MyCuts , "" , 100000000000, 0);//, "" ); 
  cout<< "Name of histo: "<< Data_CorrMass->GetName()<< endl;
  //Data_CorrMass->Scale(1./Data_CorrMass->Integral());
  TCanvas *data_c = new TCanvas("c_"+name , "c_" + name );
  data_c->cd();
  DecorateHisto(Data_CorrMass);
  //Data_CorrMass->SetTitle("");
  Data_CorrMass->Draw();  
  return Data_CorrMass;
  }

int Plot()
{
  TChain *fsignal_chain = new TChain("signal","signal");
  fsignal_chain->Add("root://eoslhcb.cern.ch//eos/lhcb/wg/semileptonic/Bs2KmunuAna/Tuples/Bs2KMuNu_MCTUPLES_TRIMMED_30May16/obsolete/DTT_13512010_Bs_Kmunu_DecProdCut_Up_Py8_MC12_trimmed.root/reducedTree");
  fsignal_chain->Add("root://eoslhcb.cern.ch//eos/lhcb/wg/semileptonic/Bs2KmunuAna/Tuples/Bs2KMuNu_MCTUPLES_TRIMMED_30May16/obsolete/DTT_13512010_Bs_Kmunu_DecProdCut_Dn_Py8_MC12_trimmed.root/reducedTree");
  
  TChain *fdata_chain = new TChain("data","data");
  fdata_chain->Add("root://eoslhcb.cern.ch//eos/lhcb/wg/semileptonic/Bs2KmunuAna/Tuples/Bs2KMuNu_DATATUPLES_TRIMMED_19June16/*_DTT_2012_Reco14Strip21r0p1a_*_SEMILEPTONIC_trimmed.root/reducedTree");
  //fdata_chain->Add("root://eoslhcb.cern.ch//eos/lhcb/wg/semileptonic/Bs2KmunuAna/Tuples/Bs2KMuNu_MCTUPLES_TRIMMED_30May16/obsolete/DTT_13512010_Bs_Kmunu_DecProdCut_Dn_Py8_MC12_trimmed.root/reducedTree");

  TChain *fincl_chain = new TChain("incl","incl");
  fincl_chain->Add("root://eoslhcb.cern.ch//eos/lhcb/wg/semileptonic/Bs2KmunuAna/Tuples/Bs2KMuNu_MCTUPLES_TRIMMED_30May16/obsolete/DTT_MC12_inclb_OC2K*.root/reducedTree");


  //TTree* data=(TTree*)fsignal_chain->GetTree("reducedTree");
  /* TCanvas *c = new TCanvas("c_", "c_" );
  TH1D *Data_CorrMass = new TH1D(  "signal","signal", 60 , 2500, 5380  ) ;
  Data_CorrMass->SetLineColor(kGreen+2);
   DecorateHisto(Data_CorrMass);
    TH1D *Data_CorrMass2 = new TH1D(  "data","data", 60 , 2500, 5380  ) ;
  TH1D *Data_CorrMass3 = new TH1D( "incl","incl" , 60 , 2500, 5380  ) ;
   Data_CorrMass3->SetLineColor(kRed+2);
  //TH1D *Data_CorrMass = new TH1D( fsignal_chain->GetName()+"CorrectedMass" , "CorrectedMass" , 60 , 2500, 5380  ) ;
  c->cd();
  //fsignal_chain->Project(Data_CorrMass->GetName() , "Bs_MCORR" ,"TMVA_charge_BDT>0.107 && TMVA_SS_aftercut_BDT>0.1", "" , 100000000000, 0);
  fsignal_chain->Draw("Bs_MCORR>>signal","TMVA_charge_BDT>0.107 && TMVA_SS_aftercut_BDT>0.1 && Bs_Regression_Q2_BEST<7000000");
  Data_CorrMass->Scale(1./Data_CorrMass->Integral());
  //
  //Data_CorrMass->Draw();  
  //TCanvas *c1 = new TCanvas("c1_", "c1_" );
  fdata_chain->Draw("Bs_MCORR>>data","TMVA_charge_BDT>0.107 && TMVA_SS_aftercut_BDT>0.1 && Bs_Regression_Q2_BEST<7000000","same");
  Data_CorrMass2->Scale(1./Data_CorrMass2->Integral());
  //TCanvas *c2 = new TCanvas("c2_", "c2_" );
  fincl_chain->Draw("Bs_MCORR>>incl","TMVA_charge_BDT>0.107 && TMVA_SS_aftercut_BDT>0.1 && Bs_Regression_Q2_BEST<7000000","same");
  Data_CorrMass3->Scale(1./Data_CorrMass3->Integral());
  TLegend *leg = new TLegend(0.1,0.5,0.3,0.9);
   leg->AddEntry(Data_CorrMass,"BsKmunu","l");
   leg->AddEntry(Data_CorrMass3,"inclukmu","l");
   leg->AddEntry(Data_CorrMass2,"data","l");
   leg->Draw();
   c->Print("corr_mass_comp_BDTlessQ2cuts.pdf");

   TCanvas *c = new TCanvas("c_", "c_" );
  TH1D *Data_CorrMass = new TH1D(  "signal","signal", 60 , 0.994, 1  ) ;
  Data_CorrMass->SetLineColor(kGreen+2);
  //DecorateHisto(Data_CorrMass);
    TH1D *Data_CorrMass2 = new TH1D(  "data","data", 60 , 0.994, 1  ) ;
  TH1D *Data_CorrMass3 = new TH1D( "incl","incl" , 60 , 0.994, 1  ) ;
   Data_CorrMass3->SetLineColor(kRed+2);
  //TH1D *Data_CorrMass = new TH1D( fsignal_chain->GetName()+"CorrectedMass" , "CorrectedMass" , 60 , 2500, 5380  ) ;
  c->cd();
  //fsignal_chain->Project(Data_CorrMass->GetName() , "Bs_MCORR" ,"TMVA_charge_BDT>0.107 && TMVA_SS_aftercut_BDT>0.1", "" , 100000000000, 0);
  
  //
  //Data_CorrMass->Draw();  
  //TCanvas *c1 = new TCanvas("c1_", "c1_" );
  fdata_chain->Draw("Bs_DIRA_OWNPV>>data","TMVA_charge_BDT>0.107 && TMVA_SS_aftercut_BDT>0.1 && Bs_Regression_Q2_BEST<7000000");
  Data_CorrMass2->Scale(1./Data_CorrMass2->Integral());
  //TCanvas *c2 = new TCanvas("c2_", "c2_" );
  fincl_chain->Draw("Bs_DIRA_OWNPV>>incl","TMVA_charge_BDT>0.107 && TMVA_SS_aftercut_BDT>0.1 && Bs_Regression_Q2_BEST<7000000","same");
  Data_CorrMass3->Scale(1./Data_CorrMass3->Integral());
  fsignal_chain->Draw("Bs_DIRA_OWNPV>>signal","TMVA_charge_BDT>0.107 && TMVA_SS_aftercut_BDT>0.1 && Bs_Regression_Q2_BEST<7000000","same");
  Data_CorrMass->Scale(1./Data_CorrMass->Integral());
  TLegend *leg = new TLegend(0.1,0.5,0.3,0.9);
   leg->AddEntry(Data_CorrMass,"BsKmunu","l");
   leg->AddEntry(Data_CorrMass3,"inclukmu","l");
   leg->AddEntry(Data_CorrMass2,"data","l");
   leg->Draw();
   c->Print("Bs_DIRA_OWNPV_comp_BDTlessQ2cuts.pdf");

   TCanvas *c = new TCanvas("c_", "c_" );
  TH1D *Data_CorrMass = new TH1D(  "signal","signal", 100 , 0, 35000  ) ;
  Data_CorrMass->SetLineColor(kGreen+2);
  //DecorateHisto(Data_CorrMass);
    TH1D *Data_CorrMass2 = new TH1D(  "data","data", 100 , 0, 35000   ) ;
  TH1D *Data_CorrMass3 = new TH1D( "incl","incl" , 100 , 0, 35000  ) ;
   Data_CorrMass3->SetLineColor(kRed+2);
  //TH1D *Data_CorrMass = new TH1D( fsignal_chain->GetName()+"CorrectedMass" , "CorrectedMass" , 60 , 2500, 5380  ) ;
  c->cd();
  //fsignal_chain->Project(Data_CorrMass->GetName() , "Bs_MCORR" ,"TMVA_charge_BDT>0.107 && TMVA_SS_aftercut_BDT>0.1", "" , 100000000000, 0);
  
  //
  //Data_CorrMass->Draw();  
  //TCanvas *c1 = new TCanvas("c1_", "c1_" );
  
  //TCanvas *c2 = new TCanvas("c2_", "c2_" );
  fincl_chain->Draw("Bs_PT>>incl","TMVA_charge_BDT>0.107 && TMVA_SS_aftercut_BDT>0.1 && Bs_Regression_Q2_BEST<7000000");
  Data_CorrMass3->Scale(1./Data_CorrMass3->Integral());
  fdata_chain->Draw("Bs_PT>>data","TMVA_charge_BDT>0.107 && TMVA_SS_aftercut_BDT>0.1 && Bs_Regression_Q2_BEST<7000000","same");
  Data_CorrMass2->Scale(1./Data_CorrMass2->Integral());
  fsignal_chain->Draw("Bs_PT>>signal","TMVA_charge_BDT>0.107 && TMVA_SS_aftercut_BDT>0.1 && Bs_Regression_Q2_BEST<7000000","same");
  Data_CorrMass->Scale(1./Data_CorrMass->Integral());
  TLegend *leg = new TLegend(0.5,0.5,0.7,0.9);
   leg->AddEntry(Data_CorrMass,"BsKmunu","l");
   leg->AddEntry(Data_CorrMass3,"inclukmu","l");
   leg->AddEntry(Data_CorrMass2,"data","l");
   leg->Draw();
   c->Print("Bs_PT_comp_BDTlessQ2cuts.pdf");

   TCanvas *c = new TCanvas("c_", "c_" );
  TH1D *Data_CorrMass = new TH1D(  "signal","signal", 100 , 0, 15000  ) ;
  Data_CorrMass->SetLineColor(kGreen+2);
  //DecorateHisto(Data_CorrMass);
    TH1D *Data_CorrMass2 = new TH1D(  "data","data", 100 , 0, 15000   ) ;
  TH1D *Data_CorrMass3 = new TH1D( "incl","incl" , 100 , 0, 15000  ) ;
   Data_CorrMass3->SetLineColor(kRed+2);
  //TH1D *Data_CorrMass = new TH1D( fsignal_chain->GetName()+"CorrectedMass" , "CorrectedMass" , 60 , 2500, 5380  ) ;
  c->cd();
  //fsignal_chain->Project(Data_CorrMass->GetName() , "Bs_MCORR" ,"TMVA_charge_BDT>0.107 && TMVA_SS_aftercut_BDT>0.1", "" , 100000000000, 0);
  
  //
  //Data_CorrMass->Draw();  
  //TCanvas *c1 = new TCanvas("c1_", "c1_" );
  
  //TCanvas *c2 = new TCanvas("c2_", "c2_" );
  fincl_chain->Draw("Bs_IPCHI2_OWNPV>>incl","TMVA_charge_BDT>0.107 && TMVA_SS_aftercut_BDT>0.1 && Bs_Regression_Q2_BEST<7000000");
  Data_CorrMass3->Scale(1./Data_CorrMass3->Integral());
  fdata_chain->Draw("Bs_IPCHI2_OWNPV>>data","TMVA_charge_BDT>0.107 && TMVA_SS_aftercut_BDT>0.1 && Bs_Regression_Q2_BEST<7000000","same");
  Data_CorrMass2->Scale(1./Data_CorrMass2->Integral());
  fsignal_chain->Draw("Bs_IPCHI2_OWNPV>>signal","TMVA_charge_BDT>0.107 && TMVA_SS_aftercut_BDT>0.1 && Bs_Regression_Q2_BEST<7000000","same");
  Data_CorrMass->Scale(1./Data_CorrMass->Integral());
  TLegend *leg = new TLegend(0.5,0.5,0.7,0.9);
   leg->AddEntry(Data_CorrMass,"BsKmunu","l");
   leg->AddEntry(Data_CorrMass3,"inclukmu","l");
   leg->AddEntry(Data_CorrMass2,"data","l");
   leg->Draw();
   c->Print("Bs_IPCHI2_OWNPV_comp_BDTlessQ2cuts.pdf");

   TCanvas *c1 = new TCanvas("c1_", "c1_" );
   TH1D *Bs_VERTEX_CHI2 = new TH1D(  "signal1","signal1", 100 , 0,4  ) ;
  Bs_VERTEX_CHI2->SetLineColor(kGreen+2);
  //Bs_VERTEX_CHI2->SetFillColor(kGreen+2);
  Bs_VERTEX_CHI2->GetXaxis()->SetTitle("Bs_ENDVERTEX_CHI2");
  //DecorateHisto(Data_CorrMass);
    TH1D *Bs_VERTEX_CHI22 = new TH1D(  "data1","data1", 100 , 0,4 ) ;
    //Bs_VERTEX_CHI22->SetFillColor(kBlue);
  TH1D *Bs_VERTEX_CHI23 = new TH1D( "incl1","incl1" , 100 , 0,4 ) ;
   Bs_VERTEX_CHI23->SetLineColor(kRed+2);
   //Bs_VERTEX_CHI23->SetFillColor(kRed+2);
   fsignal_chain->Draw("Bs_ENDVERTEX_CHI2>>signal1","TMVA_charge_BDT>0.107 && TMVA_SS_aftercut_BDT>0.1 && Bs_Regression_Q2_BEST<7000000", "hist f");
  Bs_VERTEX_CHI2->Scale(1./Bs_VERTEX_CHI2->Integral());
  //fsignal_chain->Project(Data_CorrMass->GetName() , "Bs_MCORR" ,"TMVA_charge_BDT>0.107 && TMVA_SS_aftercut_BDT>0.1 && Bs_Regression_Q2_BEST>7000000", "" , 100000000000, 0);
  //Data_CorrMass->Draw();  
  //TCanvas *c1 = new TCanvas("c1_", "c1_" );
  fdata_chain->Draw("Bs_ENDVERTEX_CHI2>>data1","TMVA_charge_BDT>0.107 && TMVA_SS_aftercut_BDT>0.1 && Bs_Regression_Q2_BEST<7000000","hist same f");
  Bs_VERTEX_CHI22->Scale(1./Bs_VERTEX_CHI22->Integral());
  fincl_chain->Draw("Bs_ENDVERTEX_CHI2>>incl1","TMVA_charge_BDT>0.107 && TMVA_SS_aftercut_BDT>0.1 && Bs_Regression_Q2_BEST<7000000","hist same f");
  Bs_VERTEX_CHI23->Scale(1./Bs_VERTEX_CHI23->Integral());
  TLegend *leg1 = new TLegend(0.3,0.5,0.5,0.9);
   leg1->AddEntry(Bs_VERTEX_CHI2,"BsKmunu","l");
   leg1->AddEntry(Bs_VERTEX_CHI23,"inclukmu","l");
   leg1->AddEntry(Bs_VERTEX_CHI22,"data","l");
   leg1->Draw();
   c1->Print("Bs_VERTEX_comp_BDTlessQ2cuts.pdf");

   TCanvas *c1 = new TCanvas("c1_", "c1_" );
   TH1D *Bs_VERTEX_CHI2 = new TH1D(  "signal1","signal1", 100 , 0,1.1  ) ;
  Bs_VERTEX_CHI2->SetLineColor(kGreen+2);
  //Bs_VERTEX_CHI2->SetFillColor(kGreen+2);
  
  //DecorateHisto(Data_CorrMass);
    TH1D *Bs_VERTEX_CHI22 = new TH1D(  "data1","data1", 100 , 0,1.1 ) ;
    //Bs_VERTEX_CHI22->SetFillColor(kBlue);
  TH1D *Bs_VERTEX_CHI23 = new TH1D( "incl1","incl1" , 100 , 0,1.1 ) ;
   Bs_VERTEX_CHI23->SetLineColor(kRed+2);
   //Bs_VERTEX_CHI23->SetFillColor(kRed+2);
   Bs_VERTEX_CHI23->GetXaxis()->SetTitle("kaon_m_ConeIso");
  //fsignal_chain->Project(Data_CorrMass->GetName() , "Bs_MCORR" ,"TMVA_charge_BDT>0.107 && TMVA_SS_aftercut_BDT>0.1 && Bs_Regression_Q2_BEST>7000000", "" , 100000000000, 0);
  //Data_CorrMass->Draw();  
  //TCanvas *c1 = new TCanvas("c1_", "c1_" );
  
  fincl_chain->Draw("kaon_m_ConeIso>>incl1","TMVA_charge_BDT>0.107 && TMVA_SS_aftercut_BDT>0.1","hist f");
  Bs_VERTEX_CHI23->Scale(1./Bs_VERTEX_CHI23->Integral());
  fdata_chain->Draw("kaon_m_ConeIso>>data1","TMVA_charge_BDT>0.107 && TMVA_SS_aftercut_BDT>0.1","hist same f");
  Bs_VERTEX_CHI22->Scale(1./Bs_VERTEX_CHI22->Integral());
  
  fsignal_chain->Draw("kaon_m_ConeIso>>signal1","TMVA_charge_BDT>0.107 && TMVA_SS_aftercut_BDT>0.1", "hist same f");
  Bs_VERTEX_CHI2->Scale(1./Bs_VERTEX_CHI2->Integral());
  TLegend *leg1 = new TLegend(0.1,0.5,0.3,0.9);
   leg1->AddEntry(Bs_VERTEX_CHI2,"BsKmunu","l");
   leg1->AddEntry(Bs_VERTEX_CHI23,"inclukmu","l");
   leg1->AddEntry(Bs_VERTEX_CHI22,"data","l");
   leg1->Draw();
   c1->Print("kaon_m_ConeIso_comp_BDTcuts.pdf");*/

   TCanvas *c1 = new TCanvas("c1_", "c1_" );
   TH1D *Bs_VERTEX_CHI2 = new TH1D(  "signal1","signal1", 100 ,500,4000  ) ;
  Bs_VERTEX_CHI2->SetLineColor(kGreen+2);
  //Bs_VERTEX_CHI2->SetFillColor(kGreen+2);
  
  //DecorateHisto(Data_CorrMass);
    TH1D *Bs_VERTEX_CHI22 = new TH1D(  "data1","data1", 100 , 500,4000 ) ;
    //Bs_VERTEX_CHI22->SetFillColor(kBlue);
  TH1D *Bs_VERTEX_CHI23 = new TH1D( "incl1","incl1" , 100 , 500,4000 ) ;
   Bs_VERTEX_CHI23->SetLineColor(kRed+2);
   //Bs_VERTEX_CHI23->SetFillColor(kRed+2);
   Bs_VERTEX_CHI23->GetXaxis()->SetTitle("kaon_m_PAIR_M");
  //fsignal_chain->Project(Data_CorrMass->GetName() , "Bs_MCORR" ,"TMVA_charge_BDT>0.107 && TMVA_SS_aftercut_BDT>0.1 && Bs_Regression_Q2_BEST>7000000", "" , 100000000000, 0);
  //Data_CorrMass->Draw();  
  //TCanvas *c1 = new TCanvas("c1_", "c1_" );
  
  fincl_chain->Draw("kaon_m_PAIR_M>>incl1","TMVA_charge_BDT>0.107 && TMVA_SS_aftercut_BDT>0.1&& Bs_Regression_Q2_BEST<7000000","hist f");
  Bs_VERTEX_CHI23->Scale(1./Bs_VERTEX_CHI23->Integral());
  fdata_chain->Draw("kaon_m_PAIR_M>>data1","TMVA_charge_BDT>0.107 && TMVA_SS_aftercut_BDT>0.1&& Bs_Regression_Q2_BEST<7000000","hist same f");
  Bs_VERTEX_CHI22->Scale(1./Bs_VERTEX_CHI22->Integral());
  
  fsignal_chain->Draw("kaon_m_PAIR_M>>signal1","TMVA_charge_BDT>0.107 && TMVA_SS_aftercut_BDT>0.1&& Bs_Regression_Q2_BEST<7000000", "hist same f");
  Bs_VERTEX_CHI2->Scale(1./Bs_VERTEX_CHI2->Integral());
  TLegend *leg1 = new TLegend(0.3,0.5,0.5,0.9);
   leg1->AddEntry(Bs_VERTEX_CHI2,"BsKmunu","l");
   leg1->AddEntry(Bs_VERTEX_CHI23,"inclukmu","l");
   leg1->AddEntry(Bs_VERTEX_CHI22,"data","l");
   leg1->Draw();
   c1->Print("kaon_m_PAIR_M_comp_BDTlessQ2cuts.pdf");

   /*TCanvas *c2 = new TCanvas("c2_", "c2_" );
   TH1D *Bs_doca = new TH1D(  "signal2","signal2", 100 , 0,0.25  ) ;
  Bs_doca->SetLineColor(kGreen+2);
  Bs_doca->GetXaxis()->SetTitle("Bs_DOCA");
  //DecorateHisto(Data_CorrMass);
    TH1D *Bs_doca2 = new TH1D(  "data2","data2", 100 , 0,0.25 ) ;
  TH1D *Bs_doca3 = new TH1D( "incl2","incl2" , 100 , 0,0.25) ;
   Bs_doca3->SetLineColor(kRed+2);
  fsignal_chain->Draw("Bs_DOCA>>signal2","TMVA_charge_BDT>0.107 && TMVA_SS_aftercut_BDT>0.1 && Bs_Regression_Q2_BEST<7000000");
  Bs_doca->Scale(1./Bs_doca->Integral());
  //fsignal_chain->Project(Data_CorrMass->GetName() , "Bs_MCORR" ,"TMVA_charge_BDT>0.107 && TMVA_SS_aftercut_BDT>0.1 && Bs_Regression_Q2_BEST>7000000", "" , 100000000000, 0);
  //Data_CorrMass->Draw();  
  //TCanvas *c1 = new TCanvas("c1_", "c1_" );
  fdata_chain->Draw("Bs_DOCA>>data2","TMVA_charge_BDT>0.107 && TMVA_SS_aftercut_BDT>0.1 && Bs_Regression_Q2_BEST<7000000","same");
  Bs_doca2->Scale(1./Bs_doca2->Integral());
  fincl_chain->Draw("Bs_DOCA>>incl2","TMVA_charge_BDT>0.107 && TMVA_SS_aftercut_BDT>0.1 && Bs_Regression_Q2_BEST<7000000","same");
  Bs_doca3->Scale(1./Bs_doca3->Integral());
  TLegend *leg2 = new TLegend(0.3,0.5,0.5,0.9);
   leg2->AddEntry(Bs_doca,"BsKmunu","l");
   leg2->AddEntry(Bs_doca3,"inclukmu","l");
   leg2->AddEntry(Bs_doca2,"data","l");
   leg2->Draw();
   c2->Print("Bs_doca_comp_BDTlessQ2cuts.pdf");

    TCanvas *c3 = new TCanvas("c3_", "c3_" );
   TH1D *Bs_correrr = new TH1D(  "signal3","signal3", 100 , 0,1000  ) ;
  Bs_correrr->SetLineColor(kGreen+2);
  //DecorateHisto(Data_CorrMass);
    TH1D *Bs_correrr2 = new TH1D(  "data3","data3", 100 , 0,1000 ) ;
  TH1D *Bs_correrr3 = new TH1D( "incl3","incl3" , 100 , 0,1000) ;
   Bs_correrr3->SetLineColor(kRed+2);
  
  //fsignal_chain->Project(Data_CorrMass->GetName() , "Bs_MCORR" ,"TMVA_charge_BDT>0.107 && TMVA_SS_aftercut_BDT>0.1 && Bs_Regression_Q2_BEST>7000000", "" , 100000000000, 0);
  //Data_CorrMass->Draw();  
  //TCanvas *c1 = new TCanvas("c1_", "c1_" );
  Bs_correrr3->GetXaxis()->SetTitle("Bs_MCORRERR");
  fincl_chain->Draw("Bs_MCORRERR>>incl3","TMVA_charge_BDT>0.107 && TMVA_SS_aftercut_BDT>0.1 && Bs_Regression_Q2_BEST<7000000");
  Bs_correrr3->Scale(1./Bs_correrr3->Integral());
  fdata_chain->Draw("Bs_MCORRERR>>data3","TMVA_charge_BDT>0.107 && TMVA_SS_aftercut_BDT>0.1 && Bs_Regression_Q2_BEST<7000000","same");
  Bs_correrr2->Scale(1./Bs_correrr2->Integral());
  fsignal_chain->Draw("Bs_MCORRERR>>signal3","TMVA_charge_BDT>0.107 && TMVA_SS_aftercut_BDT>0.1 && Bs_Regression_Q2_BEST<7000000","same");
  Bs_correrr->Scale(1./Bs_correrr->Integral());
  TLegend *leg3 = new TLegend(0.3,0.5,0.5,0.9);
   leg3->AddEntry(Bs_correrr,"BsKmunu","l");
   leg3->AddEntry(Bs_correrr3,"inclukmu","l");
   leg3->AddEntry(Bs_correrr2,"data","l");
   leg3->Draw();
   c3->Print("Bs_correrr_comp_BDTlessQ2cuts.pdf");

 TCanvas *c3 = new TCanvas("c3_", "c3_" );
   TH1D *Bs_correrr = new TH1D(  "signal3","signal3", 100 , -5000000, 25000000 ) ;
  Bs_correrr->SetLineColor(kGreen+2);
  //DecorateHisto(Data_CorrMass);
    TH1D *Bs_correrr2 = new TH1D(  "data3","data3", 100 , -5000000, 25000000 ) ;
  TH1D *Bs_correrr3 = new TH1D( "incl3","incl3" , 100 , -5000000, 25000000 ) ;
   Bs_correrr3->SetLineColor(kRed+2);
  
  //fsignal_chain->Project(Data_CorrMass->GetName() , "Bs_MCORR" ,"TMVA_charge_BDT>0.107 && TMVA_SS_aftercut_BDT>0.1 && Bs_Regression_Q2_BEST>7000000", "" , 100000000000, 0);
  //Data_CorrMass->Draw();  
  //TCanvas *c1 = new TCanvas("c1_", "c1_" );
   Bs_correrr3->GetXaxis()->SetTitle("Bs_Regression_Q2_BEST");
  fincl_chain->Draw("Bs_Regression_Q2_BEST>>incl3","TMVA_charge_BDT>0.107 && TMVA_SS_aftercut_BDT>0.1 && Bs_Regression_Q2_BEST<7000000");
  Bs_correrr3->Scale(1./Bs_correrr3->Integral());
  fdata_chain->Draw("Bs_Regression_Q2_BEST>>data3","TMVA_charge_BDT>0.107 && TMVA_SS_aftercut_BDT>0.1 && Bs_Regression_Q2_BEST<7000000","same");
  Bs_correrr2->Scale(1./Bs_correrr2->Integral());
  fsignal_chain->Draw("Bs_Regression_Q2_BEST>>signal3","TMVA_charge_BDT>0.107 && TMVA_SS_aftercut_BDT>0.1 && Bs_Regression_Q2_BEST<7000000","same");
  Bs_correrr->Scale(1./Bs_correrr->Integral());
  TLegend *leg3 = new TLegend(0.1,0.5,0.3,0.9);
   leg3->AddEntry(Bs_correrr,"BsKmunu","l");
   leg3->AddEntry(Bs_correrr3,"inclukmu","l");
   leg3->AddEntry(Bs_correrr2,"data","l");
   leg3->Draw();
   c3->Print("Bs_Q2Best_comp_BDTlessQ2cuts.pdf");

   TCanvas *c3 = new TCanvas("c3_", "c3_" );
   TH1D *Bs_correrr = new TH1D(  "signal3","signal3", 100 , -0.6,0.3 ) ;
  Bs_correrr->SetLineColor(kGreen+2);
  //DecorateHisto(Data_CorrMass);
    TH1D *Bs_correrr2 = new TH1D(  "data3","data3", 100 , -0.6,0.3 ) ;
  TH1D *Bs_correrr3 = new TH1D( "incl3","incl3" , 100 , -0.6,0.3 ) ;
   Bs_correrr3->SetLineColor(kRed+2);
  
  //fsignal_chain->Project(Data_CorrMass->GetName() , "Bs_MCORR" ,"TMVA_charge_BDT>0.107 && TMVA_SS_aftercut_BDT>0.1 && Bs_Regression_Q2_BEST>7000000", "" , 100000000000, 0);
  //Data_CorrMass->Draw();  
  //TCanvas *c1 = new TCanvas("c1_", "c1_" );
   Bs_correrr3->GetXaxis()->SetTitle("TMVA_inclb_afc_BDT");
  
  
  fincl_chain->Draw("TMVA_inclb_afc_BDT>>incl3","TMVA_charge_BDT>0.107 && TMVA_SS_aftercut_BDT>0.1&& Bs_Regression_Q2_BEST<7000000");
  Bs_correrr3->Scale(1./Bs_correrr3->Integral());
  fdata_chain->Draw("TMVA_inclb_afc_BDT>>data3","TMVA_charge_BDT>0.107 && TMVA_SS_aftercut_BDT>0.1&& Bs_Regression_Q2_BEST<7000000","same");
  Bs_correrr2->Scale(1./Bs_correrr2->Integral());
  fsignal_chain->Draw("TMVA_inclb_afc_BDT>>signal3","TMVA_charge_BDT>0.107 && TMVA_SS_aftercut_BDT>0.1&& Bs_Regression_Q2_BEST<7000000","same");
  Bs_correrr->Scale(1./Bs_correrr->Integral());
  
  TLegend *leg3 = new TLegend(0.1,0.5,0.3,0.9);
   leg3->AddEntry(Bs_correrr,"BsKmunu","l");
   leg3->AddEntry(Bs_correrr3,"inclukmu","l");
   leg3->AddEntry(Bs_correrr2,"data","l");
   leg3->Draw();
   c3->Print("TMVA_inclb_afc_BDT_comp_BDTlessQ2cuts.pdf");*/
   
   
   
  // Cuts :
  TString MyCuts = "TMVA_charge_BDT>0.107 && TMVA_SS_aftercut_BDT>0.1";
  //TString MyCuts = "TMVA_charge_BDT>0.107 && TMVA_SS_aftercut_BDT>0.1 && Bs_Regression_Q2_BEST>7000000";
  // && muon_p_IsoMinBDT >-0.5";//"TMVA_charge_BDT>-0.2 &&TMVA_SS_aftercut_BDT>-0.2";
  //"TMVA_charge_BDT>0.107 &&TMVA_SS_aftercut_BDT>-0.0954 && (1820>kaon_m_PAIR_M || kaon_m_PAIR_M>1880)" ;
  // Get Corrected mass :

  
  return 0;
}



