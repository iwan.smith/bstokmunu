#include "/afs/cern.ch/user/b/bkhanji/roofitinclude.h"
#include <iostream>
#include <TROOT.h>
#include <TLatex.h>
#include <TFile.h>
#include <TTree.h>
#include <sstream>
#include <TChain.h>
#include <TCanvas.h>
#include <TObjArray.h>
#include <TBranch.h>
#include <TLorentzVector.h>
#include <iomanip>
#include <TH1F.h>
#include "RooStats/ModelConfig.h"
#include "RooStats/HistFactory/Measurement.h"
#include "RooStats/HistFactory/MakeModelAndMeasurementsFast.h"
#include "RooStats/HistFactory/HistFactorySimultaneous.h"
#include "RooStats/HistFactory/HistFactoryNavigation.h"

using namespace RooFit;
using namespace RooStats;
using namespace HistFactory;
using namespace std;


void CheckNonZeroBin(TH1F* h=0){
    Int_t binx = h->GetNbinsX();
    for(Int_t i = 0; i<= binx; i++){
    	if(h->GetBinContent(i)<=0) {
	cout<<"ZERO BIN FOUND at: "<<i<<", "<<endl;
	h->SetBinContent(i,1e-12);//set empty bins to 1e-12
	}
      }
  }

void DecorateHisto(TH1D* Data_CorrMass=0)
{
  //gPad->SetGrid();  
  //gPad->SetTickx();
  //gPad->SetTicky();
  //Data_CorrMass->SetTitle("");
  //gStyle->SetOptStats(1111);
  Data_CorrMass->SetStats(kTRUE);
  Data_CorrMass->GetYaxis()->SetTitle("Events / ( 48 MeV^{2} )");
  Data_CorrMass->GetXaxis()->SetTitle("B Corr. mass (MeV^{2})");
  //Data_CorrMass->GetYaxis()->SetTitleOffset(0.9);
  //Data_CorrMass->SetStats(0);
  //Data_CorrMass->SetMinimum(0);
  //Data_CorrMass->GetYaxis()->SetNdivisions(7+100*5);
}

void SetBinErrorhisto(TH1F* h=0){
    Int_t binx = h->GetNbinsX();
    for(Int_t i = 0; i<= binx; i++){
      // cout << "BinError: "<< h->GetName()<< " " << h->GetBinError(i)<<endl;
      h->SetBinError(i, 1e-12);
      }
  }


TH1D* GetCorr_h(TString data_PATH="" , TString name = ""  , TString MyCuts= "" )
{
  
  TChain *fdata_chain = new TChain("data_OS","data_OS");
  fdata_chain->Add( data_PATH );
  fdata_chain->Print();
  TDirectory *where = gDirectory;
  where->cd();
  //TH1D *Data_CorrMass = new TH1D( name+ "_Bs_MCORR" , name+ "_Bs_MCORR" , 40 , 2500 , 5370  ) ;
  TH1D *Data_CorrMass = new TH1D( name+ "_Bs_MCORR" , name+ "_Bs_MCORR" , 60 , 2500, 5380  ) ;
  Data_CorrMass->Sumw2();
  fdata_chain->Project(Data_CorrMass->GetName() , "Bs_MCORR" , MyCuts , "" , 100000000000, 0);//, "" ); 
  cout<< "Name of histo: "<< Data_CorrMass->GetName()<< endl;
  //Data_CorrMass->Scale(1./Data_CorrMass->Integral());
  TCanvas *data_c = new TCanvas("c_"+name , "c_" + name );
  data_c->cd();
  DecorateHisto(Data_CorrMass);
  //Data_CorrMass->SetTitle("");
  Data_CorrMass->Draw();  
  return Data_CorrMass;
  }

int Bs2Kmunu_Fit_newBDT()
{
  /*
  // Cuts :
  TString MyCuts = "TMVA_charge_BDT>0.107 && TMVA_SS_aftercut_BDT>0.1";
  // && muon_p_IsoMinBDT >-0.5";//"TMVA_charge_BDT>-0.2 &&TMVA_SS_aftercut_BDT>-0.2";
  //"TMVA_charge_BDT>0.107 &&TMVA_SS_aftercut_BDT>-0.0954 && (1820>kaon_m_PAIR_M || kaon_m_PAIR_M>1880)" ;
  // Get Corrected mass :

  TString eosMC    = "root://eoslhcb.cern.ch//eos/lhcb/wg/semileptonic/Bs2KmunuAna/Tuples/Bs2KMuNu_MCTUPLES_TRIMMED_BDT_30May16/";
  // old BDT TString eosMC    = "root://eoslhcb.cern.ch//eos/lhcb/wg/semileptonic/Bs2KmunuAna/Tuples/Bs2KMuNu_MCTUPLES_WITHBDT_30June16/";
  // old BDT TString eosdata  = "root://eoslhcb.cern.ch//eos/lhcb/wg/semileptonic/Bs2KmunuAna/Tuples/Bs2KMuNu_DATATUPLES_TRIMMED_WITHBDT_29June16/";
  TString eosdata  = "root://eoslhcb.cern.ch//eos/lhcb/wg/semileptonic/Bs2KmunuAna/Tuples/Bs2KMuNu_DATATUPLES_TRIMMED_BDT_19June16/";
  //
  TString SignalMC         = eosMC + "DTT_13512010_Bs_Kmunu_DecProdCut_*_Py8_MC12_*.root/reducedTree";
  TString Bu2JpsiKplusMC   = eosMC + "DTT_12143001_Bu_JpsiK_mm_DecProdCut_*_Py8_MC12_*.root/reducedTree";
  TString Bu2JpsiKstarMC   = eosMC + "DTT_12143401_Bu_JpsiKst_mm_Kpi0_DecProdCut_*_Py8_MC12_*.root/reducedTree";
  TString Bs2DsmunuMC      = eosMC + "DTT_13774000_Bs_Dsmunu_cocktail_hqet2_DsmuInAcc_*_Py8_MC12_*.root/reducedTree";
  TString inclb_OC2Kplusmu = eosMC + "DTT_MC12_inclb_OC2Kplusmu_gencut_BKG_10010037_*.root/reducedTree" ;
  TString inclb_OC2Kmu     = eosMC + "DTT_MC12_inclb_OC2Kmu_gencut_BKG_10010032_*.root/reducedTree"     ;
  TString inclb_OC2KmuSS   = eosMC + "DTT_MC12_inclb_OC2KmuSS_gencut_BKG_10010035_*.root/reducedTree"   ;
  TString inclb_ALL        = eosMC + "DTT_MC12_inclb_OC2K*.root/reducedTree"   ;
  TString Bd_pimunu = eosMC + "DTT_11512011_Bd_pimunu_DecProdCut_*_Py8_MC12_*.root/reducedTree"   ;
  TString Bd_JpsiKst = eosMC + "DTT_11144001_Bd_JpsiKst_mm_DecProdCut_*_Py8_MC12_*.root/reducedTree"   ;
  TString Bd_rhomunu = eosMC + "DTT_11512400_Bd_rhomunu_pipi0_DecProdCut_*_Py8_MC12_*.root/reducedTree"   ;
  TString Bd_Dstmunu = eosMC + "DTT_11874004_Bd_Dstmunu_Kpi_cocktail_hqet_D0muInAcc_BRCorr1_*_Py8_MC12_*.root/reducedTree"   ;
  //TString Bd_Dmunu = eosMC + "DTT_11874042_Bd_Dmunu_K-pi+pi+_cocktail_hqet_BRCorr1_DmuInAcc_*_Py8_MC12.root/reducedTree"   ;
  //TString Bd_Dmunu = eosMC + "DTT_11874042_Bd_Dmunu_K-pi+pi+_cocktail_hqet_BRCorr1_DmuInAcc_*_Py8_MC12.root/reducedTree"   ;
  TString Bu_D0munu  = eosMC + "DTT_12873002_Bu_D0munu_Kpi_cocktail_D0muInAcc_BRcorr1_*_Py8_MC12_*.root/reducedTree"   ;
  TString Bs_Jpsiphi = eosMC + "DTT_13144001_Bs_Jpsiphi_mm_CPV_update2012_DecProdCut_*_Py8_MC12_*.root/reducedTree"   ;
  TString Bs_Kstmunu = eosMC + "DTT_13512400_Bs_Kstmunu_Kpi0_DecProdCut_*_Py8_MC12_*.root/reducedTree"   ;
  TString Bs_K2stmunu = eosMC + "DTT_13512410_Bs_K2stmunu_Kpi0_DecProdCut_*_Py8_MC12_*.root/reducedTree"   ;
  TString Bs_K1430munu = eosMC + "DTT_13512420_Bs_K1430munu_Kpi0_DecProdCut_*_Py8_MC12_*.root/reducedTree"   ;
  TString Lb_pmunu = eosMC + "DTT_15512014_Lb_pmunu_DecProdCut_LQCD_*_Py8_MC12_*.root/reducedTree"   ;
  TString Bs_D0DsK = eosMC + "DTT_13796000_Bs_D0DsK_Kmunu_KKpi_DecProdCut_tightCut_*_Py8_MC12_*.root/reducedTree"   ;
  TString Bd_Dpi = eosMC + "DTT_11574051_Bd_Dpi_Kpimunu_DecProdCut_*_Py8_MC12_*.root/reducedTree"   ;
  TString Bu_rhomunu = eosMC + "DTT_12513001_Bu_rhomunu_DecProdCut_*_Py8_MC12_*.root/reducedTree"   ;
  TString Bu_D0pi = eosMC + "DTT_12573050_Bu_D0pi_Kmunu_DecProdCut_*_Py8_MC12_*.root/reducedTree"   ;
  TString Bd_Dst0pi_D0gamma = eosMC + "DTT_12573200_Bu_Dst0pi_D0gamma_Kmunu_DecProdCut_*_Py8_MC12_*.root/reducedTree"   ;
  TString Bd_Dst0pi_D0pi0 = eosMC + "DTT_12573400_Bu_Dst0pi_D0pi0_Kmunu_DecProdCut_*_Py8_MC12_*.root/reducedTree"   ;
  TString Bd_D0rho = eosMC + "DTT_12573401_Bu_D0rho_Kmunu_pipi0_DecProdCut_*_Py8_MC12_*.root/reducedTree"   ;
  //TString Bd_D0Kst0 = eosMC + "DTT_13574040_Bs_D0Kst0_Kmunu_Kpi_DecProdCut_*_Py8_MC12.root/reducedTree"   ;
  //
  //TString DataOS_2012      = "/afs/cern.ch/work/b/bkhanji/public/DTT_2012_trimmed.root/Bs2KmuNuTuple/reducedTree";
  //TString DataSS_2011      = "/afs/cern.ch/work/b/bkhanji/public/DTT_2012_trimmed_SS.root/Bs2KmuNuTuple/reducedTree" ;
  TString DataOS_2012 = eosdata + "*_DTT_2012_Reco14Strip21r0p1a_*_SEMILEPTONIC_trimmed.TMVA_SS_aftercut_BDT.root/reducedTree";
  // ================================================================
  // Corrected mass
  TH1D *Data_CorrMass_OS_2012         = GetCorr_h( DataOS_2012 , "OS_2012" , MyCuts );
  //TH1D *Data_CorrMass_SS_2012         = GetCorr_h( DataSS_2011 , "SS_2012" , MyCuts );
  TH1D *Signal_CorrMass_MC_2012       = GetCorr_h( SignalMC , "SignalMC" , MyCuts );
  TH1D *Bu2JpsiKplus_CorrMass_MC_2012 = GetCorr_h( Bu2JpsiKplusMC , "Bu2JpsiKplusMC" , MyCuts );
  TH1D *Bu2JpsiKstar_CorrMass_MC_2012 = GetCorr_h( Bu2JpsiKstarMC , "Bu2JpsiKstarMC" , MyCuts );
  TH1D *Bs2Dsmunu_CorrMass_MC_2012    = GetCorr_h( Bs2DsmunuMC    , "Bs2DsmunuMC"    , MyCuts );
  TH1D *Bd_pimunu_CorrMass_MC_2012       = GetCorr_h( Bd_pimunu , "Bd_pimunu" , MyCuts );
  TH1D *Bd_JpsiKst_CorrMass_MC_2012       = GetCorr_h( Bd_JpsiKst , "Bd_JpsiKst" , MyCuts );
  TH1D *Bd_rhomunu_CorrMass_MC_2012       = GetCorr_h( Bd_rhomunu , "Bd_rhomunu" , MyCuts );
  TH1D *Bd_Dstmunu_CorrMass_MC_2012       = GetCorr_h( Bd_Dstmunu , "Bd_Dstmunu" , MyCuts );
  //TH1D *Bd_Dmunu_CorrMass_MC_2012       = GetCorr_h( Bd_Dmunu , "Bd_Dmunu" , MyCuts );
  TH1D *Bu_D0munu_CorrMass_MC_2012       = GetCorr_h( Bu_D0munu , "Bu_D0munu" , MyCuts );
  TH1D *Bs_Jpsiphi_CorrMass_MC_2012       = GetCorr_h( Bs_Jpsiphi , "Bs_Jpsiphi" , MyCuts );
  TH1D *Bs_Kstmunu_CorrMass_MC_2012       = GetCorr_h( Bs_Kstmunu , "Bs_Kstmunu" , MyCuts );
  TH1D *Bs_K2stmunu_CorrMass_MC_2012       = GetCorr_h( Bs_K2stmunu , "Bs_K2stmunu" , MyCuts );
  TH1D *Bs_K1430munu_CorrMass_MC_2012       = GetCorr_h( Bs_K1430munu , "Bs_K1430munu" , MyCuts );
  TH1D *Lb_pmunu_CorrMass_MC_2012       = GetCorr_h( Lb_pmunu , "Lb_pmunu" , MyCuts );
  TH1D * Bs_D0DsK_CorrMass_MC_2012     = GetCorr_h( Bs_D0DsK , "Bs_D0DsK" , MyCuts );
  TH1D * Bd_Dpi_CorrMass_MC_2012     = GetCorr_h(Bd_Dpi  , "Bd_Dpi" , MyCuts );
  TH1D * Bu_rhomunu_CorrMass_MC_2012     = GetCorr_h(Bu_rhomunu  , "Bu_rhomunu" , MyCuts );
  TH1D * Bu_D0pi_CorrMass_MC_2012     = GetCorr_h( Bu_D0pi , "Bu_D0pi" , MyCuts );
  TH1D * Bd_Dst0pi_D0gamma_CorrMass_MC_2012     = GetCorr_h(Bd_Dst0pi_D0gamma  , "Bd_Dst0pi_D0gamma" , MyCuts );
  TH1D * Bd_Dst0pi_D0pi0_CorrMass_MC_2012     = GetCorr_h( Bd_Dst0pi_D0pi0 , "Bd_Dst0pi_D0pi0" , MyCuts );
  TH1D * Bd_D0rho_CorrMass_MC_2012     = GetCorr_h( Bd_D0rho , "Bd_D0rho" , MyCuts );
  //TH1D * Bd_D0Kst0_CorrMass_MC_2012     = GetCorr_h(Bd_D0Kst0  , "Bd_D0Kst0" , MyCuts );

  TH1D *inclb_OC2Kplusmu_CorrMass_MC_2012 = GetCorr_h( inclb_OC2Kplusmu , "inclb_OC2Kplusmu" , MyCuts );
  TH1D *inclb_OC2Kmu_CorrMass_MC_2012     = GetCorr_h( inclb_OC2Kmu     , "inclb_OC2Kmu"     , MyCuts );
  TH1D *inclb_OC2KmuSS_CorrMass_MC_2012   = GetCorr_h( inclb_OC2KmuSS   , "inclb_OC2KmuSS"   , MyCuts );
  TH1D *inclb_ALL_CorrMass_MC_2012        = GetCorr_h( inclb_ALL        , "inclb_ALL"        , MyCuts );

  cout<<"============================events=================================="<<endl;
  cout<< "Data_CorrMass_OS_2012 "<< Data_CorrMass_OS_2012->Integral()<< endl;
  cout<< "Signal_CorrMass_MC_2012 "<< Signal_CorrMass_MC_2012->GetEntries()<< endl;
  cout<< "Bu2JpsiKplus_CorrMass_MC_2012 "<< Bu2JpsiKplus_CorrMass_MC_2012->GetEntries()<< endl;
  cout<< "Bu2JpsiKstar_CorrMass_MC_2012 "<< Bu2JpsiKstar_CorrMass_MC_2012->GetEntries()<< endl;
  cout<< "Bs2Dsmunu_CorrMass_MC_2012 "<< Bs2Dsmunu_CorrMass_MC_2012->GetEntries()<< endl;
  cout<< "Bd_pimunu_CorrMass_MC_2012 "<< Bd_pimunu_CorrMass_MC_2012->GetEntries()<< endl;
  cout<< "Bd_JpsiKst_CorrMass_MC_2012 "<< Bd_JpsiKst_CorrMass_MC_2012->GetEntries()<< endl;
  cout<< "Bd_rhomunu_CorrMass_MC_2012 "<< Bd_rhomunu_CorrMass_MC_2012->GetEntries()<< endl;
  cout<< "Bd_Dstmunu_CorrMass_MC_2012 "<< Bd_Dstmunu_CorrMass_MC_2012->GetEntries()<< endl;
  //cout<< "Bd_Dmunu_CorrMass_MC_2012 "<< Bd_Dmunu_CorrMass_MC_2012->GetEntries()<< endl;
  cout<< "Bu_D0munu_CorrMass_MC_2012 "<< Bu_D0munu_CorrMass_MC_2012->GetEntries()<< endl;
  cout<< "Bs_Jpsiphi_CorrMass_MC_2012 "<< Bs_Jpsiphi_CorrMass_MC_2012->GetEntries()<< endl;
  cout<< "Bs_Kstmunu_CorrMass_MC_2012 "<< Bs_Kstmunu_CorrMass_MC_2012->GetEntries()<< endl;
  cout<< "Bs_K2stmunu_CorrMass_MC_2012 "<< Bs_K2stmunu_CorrMass_MC_2012->GetEntries()<< endl;
  cout<< "Bs_K1430munu_CorrMass_MC_2012 "<< Bs_K1430munu_CorrMass_MC_2012->GetEntries()<< endl;
  cout<< "Lb_pmunu_CorrMass_MC_2012 "<< Lb_pmunu_CorrMass_MC_2012->GetEntries()<< endl;
  cout<< "Bs_D0DsK_CorrMass_MC_2012 "<< Bs_D0DsK_CorrMass_MC_2012->GetEntries()<< endl;
  cout<< "Bd_Dpi_CorrMass_MC_2012 "<< Bd_Dpi_CorrMass_MC_2012->GetEntries()<< endl;
  cout<< "Bu_rhomunu_CorrMass_MC_2012 "<< Bu_rhomunu_CorrMass_MC_2012->GetEntries()<< endl;
  cout<< "Bu_D0pi_CorrMass_MC_2012 "<< Bu_D0pi_CorrMass_MC_2012->GetEntries()<< endl;
  cout<< "Bd_Dst0pi_D0gamma_CorrMass_MC_2012 "<< Bd_Dst0pi_D0gamma_CorrMass_MC_2012->GetEntries()<< endl;
  cout<< "Bd_Dst0pi_D0pi0_CorrMass_MC_2012 "<< Bd_Dst0pi_D0pi0_CorrMass_MC_2012->GetEntries()<< endl;
  cout<< "Bd_D0rho_CorrMass_MC_2012 "<< Bd_D0rho_CorrMass_MC_2012->GetEntries()<< endl;
  //cout<< "Bd_D0Kst0_CorrMass_MC_2012 "<< Bd_D0Kst0_CorrMass_MC_2012->GetEntries()<< endl;
  cout<< "inclb_OC2Kplusmu_CorrMass_MC_2012 "<< inclb_OC2Kplusmu_CorrMass_MC_2012->GetEntries()<< endl;
  cout<< "inclb_OC2Kmu_CorrMass_MC_2012 "<< inclb_OC2Kmu_CorrMass_MC_2012->GetEntries()<< endl;
  cout<< "inclb_OC2KmuSS_CorrMass_MC_2012 "<< inclb_OC2KmuSS_CorrMass_MC_2012->GetEntries()<< endl;
  cout<< "inclb_ALL_CorrMass_MC_2012 "<< inclb_ALL_CorrMass_MC_2012->GetEntries()<< endl;
  

  TCanvas *compare = new TCanvas("compare" , "compare");
  Data_CorrMass_OS_2012->DrawNormalized("ep");
  Signal_CorrMass_MC_2012->SetLineColor(kGreen+2)           ; Signal_CorrMass_MC_2012->DrawNormalized("hist");
  Bu2JpsiKplus_CorrMass_MC_2012->SetLineColor(kRed)         ; Bu2JpsiKplus_CorrMass_MC_2012->DrawNormalized("hist same");
  Bu2JpsiKstar_CorrMass_MC_2012->SetLineColor(kBlue)        ; Bu2JpsiKstar_CorrMass_MC_2012->DrawNormalized("hist same");
  //Bs2Dsmunu_CorrMass_MC_2012->SetLineColor(kOrange+2)       ; Bs2Dsmunu_CorrMass_MC_2012->DrawNormalized("same");
  //inclb_OC2Kplusmu_CorrMass_MC_2012->SetLineColor(kOrange+2); inclb_OC2Kplusmu_CorrMass_MC_2012->DrawNormalized("same");
  //inclb_OC2Kmu_CorrMass_MC_2012->SetLineColor(kOrange+2)    ; inclb_OC2Kmu_CorrMass_MC_2012->DrawNormalized("same");
  //inclb_OC2KmuSS_CorrMass_MC_2012->SetLineColor(kOrange+2)  ; inclb_OC2KmuSS_CorrMass_MC_2012 ->DrawNormalized("same");
  inclb_ALL_CorrMass_MC_2012->SetLineColor(kOrange+4)       ; inclb_ALL_CorrMass_MC_2012->DrawNormalized("hist same");
  Bs_Jpsiphi_CorrMass_MC_2012->SetLineColor(kViolet)  ;Bs_Jpsiphi_CorrMass_MC_2012->DrawNormalized("hist same");
  Bs_Kstmunu_CorrMass_MC_2012->SetLineColor(kPink);Bs_Kstmunu_CorrMass_MC_2012->DrawNormalized("hist same");
  Bs_K2stmunu_CorrMass_MC_2012->SetLineColor(kMagenta);Bs_K2stmunu_CorrMass_MC_2012->DrawNormalized("hist same");
  Bd_JpsiKst_CorrMass_MC_2012->SetLineColor(kBlack);Bd_JpsiKst_CorrMass_MC_2012->DrawNormalized("hist same");
  Bs_K1430munu_CorrMass_MC_2012->SetLineColor(kGreen);Bs_K1430munu_CorrMass_MC_2012->DrawNormalized("hist same");

  compare->BuildLegend(0,0,0.50,0.50);
  compare->Print("compare_MC_newBDToptimization.C");
  compare->Print("compare_MC_newBDToptimization.root");

  // TH1D *Data_CorrMass_SS_2012_scaled = (TH1D*)Data_CorrMass_SS_2012->Clone();
  // Data_CorrMass_SS_2012_scaled->Scale(10);
  //
  //  TCanvas *data_compare_c = new TCanvas("data_compare_c","data_compare_c");
  //  Data_CorrMass_OS_2012->SetTitle("");
  //  Data_CorrMass_OS_2012->SetLineColor(kBlack);
  //  Data_CorrMass_SS_2012->SetLineColor(kRed);
  //  Data_CorrMass_SS_2012_scaled->SetLineColor(kMagenta);
  //  Data_CorrMass_OS_2012->Draw();
  //  Data_CorrMass_SS_2012->Draw("same");
  //  Data_CorrMass_SS_2012_scaled->Draw("same");
  //  data_compare_c->BuildLegend();

  //TFile *Histos_f = new TFile("Histos_allMCfiles_newBDToptimization.root", "RECREATE");
  TFile *Histos_f = new TFile("Histos_datafile_newBDToptimization.root", "RECREATE");
  Data_CorrMass_OS_2012->Write();
  Signal_CorrMass_MC_2012->Write();
  Bu2JpsiKplus_CorrMass_MC_2012->Write();
  Bu2JpsiKstar_CorrMass_MC_2012->Write();
  Bs_Jpsiphi_CorrMass_MC_2012->Write();
  inclb_ALL_CorrMass_MC_2012->Write();
  Bs_Kstmunu_CorrMass_MC_2012->Write();
  Bs_K2stmunu_CorrMass_MC_2012->Write();
  Bd_JpsiKst_CorrMass_MC_2012->Write();
  Bs_K1430munu_CorrMass_MC_2012->Write();
  inclb_OC2KmuSS_CorrMass_MC_2012->Write();
  inclb_OC2Kplusmu_CorrMass_MC_2012->Write();
  inclb_OC2Kmu_CorrMass_MC_2012->Write();
  Bs2Dsmunu_CorrMass_MC_2012->Write();
  Bd_pimunu_CorrMass_MC_2012->Write();
  Bd_rhomunu_CorrMass_MC_2012->Write();
  Bd_Dstmunu_CorrMass_MC_2012->Write();
  Bu_D0munu_CorrMass_MC_2012->Write();
  Lb_pmunu_CorrMass_MC_2012->Write();
  Bs_D0DsK_CorrMass_MC_2012->Write();
  Bd_Dpi_CorrMass_MC_2012->Write();
  Bu_rhomunu_CorrMass_MC_2012->Write();
  Bu_D0pi_CorrMass_MC_2012->Write();    
  Bd_Dst0pi_D0gamma_CorrMass_MC_2012->Write();
  Bd_Dst0pi_D0pi0_CorrMass_MC_2012->Write();  
  Bd_D0rho_CorrMass_MC_2012->Write();  
   
  Histos_f->Close();
   */ 
  
  // ================================================================
  // double signal_MCnorm =  1./signal->Integral();
  
  TFile* f_hist = TFile::Open("root://eoslhcb.cern.ch///eos/lhcb/wg/semileptonic/Bs2KmunuAna/Tuples/SignalFit_input/Histos_datafile_newBDToptimization.root", "READ");
  TH1F* h_data = (TH1F*)f_hist->Get("OS_2012_Bs_MCORR")->Clone();
  double n_data = h_data->Integral();
  CheckNonZeroBin(h_data);

  TFile* f_MChist = TFile::Open("root://eoslhcb.cern.ch///eos/lhcb/wg/semileptonic/Bs2KmunuAna/Tuples/SignalFit_input/Histos_allMCfiles_newBDToptimization.root", "READ");
  TH1F* h_MCinclumuK = (TH1F*)f_MChist->Get("inclb_ALL_Bs_MCORR")->Clone();
  CheckNonZeroBin(h_MCinclumuK);
  h_MCinclumuK->Sumw2();
  //SetBinErrorhisto(h_MCinclumuK);
  TH1F* h_MCsignal = (TH1F*)f_MChist->Get("SignalMC_Bs_MCORR")->Clone();
  CheckNonZeroBin(h_MCsignal);
  h_MCsignal->Sumw2();
  //SetBinErrorhisto(h_MCsignal);
  TH1F* h_MCjpsiK = (TH1F*)f_MChist->Get("Bu2JpsiKplusMC_Bs_MCORR")->Clone();
  CheckNonZeroBin(h_MCjpsiK);
  h_MCjpsiK->Sumw2();
  //SetBinErrorhisto(h_MCjpsiK);
  TH1F* h_MCKstmunu = (TH1F*)f_MChist->Get("Bs_Kstmunu_Bs_MCORR")->Clone();
  CheckNonZeroBin(h_MCKstmunu);
  h_MCKstmunu->Sumw2();
  //SetBinErrorhisto(h_MCjpsiK);

  
  RooStats::HistFactory::Measurement Bs2Kmunu_measurement("Bs2Kmunu_measurement", "Bs2Kmunu measurement");
  // Naming 
  //Bs2Kmunu_measurement.SetOutputFilePrefix("Res/Bs2Kmunu_measurement");
  Bs2Kmunu_measurement.SetExportOnly(true);//Tells histfactory to not run the fit
  Bs2Kmunu_measurement.SetPOI("BsKmunu_Yield");
  // following line that all histos are already scaled to our luminosity
  Bs2Kmunu_measurement.SetLumi(1.0);
  Bs2Kmunu_measurement.SetLumiRelErr(0.000010);
  
  // Define the  data 
  RooStats::HistFactory::Channel chan("channel");
  chan.SetStatErrorConfig(1e-5,"Pois");//Configure the use of bin-by-bin statistical uncertainties, Tell histfactory how small of template uncertainties are worth considering (always set this small)
  chan.SetData("OS_2012_Bs_MCORR", "root://eoslhcb.cern.ch///eos/lhcb/wg/semileptonic/Bs2KmunuAna/Tuples/SignalFit_input/Histos_datafile_newBDToptimization.root"); // This file should contain a histogram not a branch !!!
  //chan.SetData(Data_CorrMass_OS_2012);
  // now create "pdf" :
  // Signal
  RooStats::HistFactory::Sample signal("signal","SignalMC_Bs_MCORR", "root://eoslhcb.cern.ch///eos/lhcb/wg/semileptonic/Bs2KmunuAna/Tuples/SignalFit_input/Histos_allMCfiles_newBDToptimization.root");
  signal.ActivateStatError();//to consider this template in statistical uncertainties
  signal.SetNormalizeByTheory(kFALSE);
  double signal_n = 30000;
  signal.AddNormFactor("BsKmunu_Yield", signal_n , 0. , n_data );
  //const BsKmunu_scalefactor = 1.08172;
  signal.AddNormFactor("BsKmunu_scalefactor", 1.08172, 1.08172, 1.08172);
  //signal.AddNormFactor(); 
  //signal.AddNormFactor("mcBsKmunu_Norm", 0.4 , 1e-9 , 1.);
  // signal.AddOverallSys("syst1",  0.95, 1.05);
  
  // BKG 1
  RooStats::HistFactory::Sample B2JpsiK("B2JpsiK","Bu2JpsiKplusMC_Bs_MCORR", "root://eoslhcb.cern.ch///eos/lhcb/wg/semileptonic/Bs2KmunuAna/Tuples/SignalFit_input/Histos_allMCfiles_newBDToptimization.root");
  //B2JpsiK.ActivateStatError();
  B2JpsiK.SetNormalizeByTheory(kFALSE);
  // double n_expected_B2JpsiK_Yield =  2 * bb_Xsec * f_Bu * BR_B2JpsiK * tot_eff_B2JpsiK ;
  // double n_expected_B2JpsiK_Yield = 6.377527853506962e-06 ;// 2 * (298+- 2+-36)* (0.4 +- ) * ( 1.026+- 0.031)* 1e-3 * (0.16714)*0.055 *( (9563.+9046)/(7076879.+6784500.) ) ;
  double Bu_n = 2400 ;
   B2JpsiK.AddNormFactor("B2JpsiK_Yield" , Bu_n , 2000. , 3500. );
   B2JpsiK.AddNormFactor("B2JpsiK_scalefactor", 3.51977,  3.51977, 3.51977);
   //const B2JpsiK_scalefactor=3.51977;
   //B2JpsiK.AddNormFactor(B2JpsiK_scalefactor);
  //
  //B2JpsiK.AddNormFactor("B2JpsiK_Yield" , Bu_n , 0. ,  n_data );
    // B2JpsiK.AddNormFactor("mcB2JpsiK_Norm", 0.5 , 1e-9 , 1.);
  //B2JpsiK.ActivateStatError("background1_statUncert", "data/example.root"); //  stat uncert. on BKG comes from MC  if the desired  errors are differnet form those stored in TH1 
  //B2JpsiK.AddOverallSys("syst2", 0.95, 1.05 );
  
  // BKG 
  RooStats::HistFactory::Sample Incl_Kmu("Incl_Kmu" , "inclb_ALL_Bs_MCORR" , "root://eoslhcb.cern.ch///eos/lhcb/wg/semileptonic/Bs2KmunuAna/Tuples/SignalFit_input/Histos_allMCfiles_newBDToptimization.root");
  //Incl_Kmu.SetHisto(inclb_ALL_CorrMass_MC_2012);
  //Incl_Kmu.SetNormalizeByTheory(kFALSE);
  Incl_Kmu.ActivateStatError();
  Incl_Kmu.AddNormFactor( "incl_kmu_Yield" , n_data - (Bu_n+signal_n) , 10. , n_data );
    //Incl_Kmu.AddNormFactor("mcincl_kmu_Norm", 0.7 , 1e-9 , 1.);
    //Incl_Kmu.AddOverallSys("syst3", 0.95, 1.05 );

  // BKG 2
  /*RooStats::HistFactory::Sample BsKstmunu("BsKstmunu","Bs_Kstmunu_Bs_MCORR", "Histos_allMCfiles.root");
  BsKstmunu.ActivateStatError();
  BsKstmunu.SetNormalizeByTheory(kFALSE);
  // double n_expected_B2JpsiK_Yield =  2 * bb_Xsec * f_Bu * BR_B2JpsiK * tot_eff_B2JpsiK ;
  // double n_expected_B2JpsiK_Yield = 6.377527853506962e-06 ;// 2 * (298+- 2+-36)* (0.4 +- ) * ( 1.026+- 0.031)* 1e-3 * (0.16714)*0.055 *( (9563.+9046)/(7076879.+6784500.) ) ;
  double BsKstmunu_n = 10000 ;
  BsKstmunu.AddNormFactor("BsKstmunu_Yield" , BsKstmunu_n , 6000. , n_data );*/

  // Add all that to an "NLL"-similar object
  chan.AddSample(B2JpsiK);     
  chan.AddSample(Incl_Kmu);
  chan.AddSample(signal);
  //chan.AddSample(BsKstmunu);

  // Append all that to the measure :
  Bs2Kmunu_measurement.AddChannel(chan);
  Bs2Kmunu_measurement.CollectHistograms();
  Bs2Kmunu_measurement.PrintTree();
  RooWorkspace* ws = RooStats::HistFactory::MakeModelAndMeasurementFast(Bs2Kmunu_measurement);
  ModelConfig* Mconfig = (ModelConfig*) ws->obj("ModelConfig");
  ((RooRealVar*)(Mconfig->GetNuisanceParameters()->find("Lumi")))->setConstant(true);//fix the Lumi to not have it also fitted
  ((RooRealVar*)(Mconfig->GetNuisanceParameters()->find("BsKmunu_scalefactor")))->setConstant(true);
  ((RooRealVar*)(Mconfig->GetNuisanceParameters()->find("B2JpsiK_scalefactor")))->setConstant(true);
  Bs2Kmunu_measurement.SaveAs("./myfile.root" ,"");
  
  //((RooRealVar*)(Mconfig->GetNuisanceParameters()->find("B2JpsiK_Yield")))->setConstant(true);
  //print pdfs before the fit
  cout<<"=============================================================="<<endl;
  cout<<"=============================================================="<<endl;
  RooSimultaneous* Total_pdf = (RooSimultaneous*) Mconfig->GetPdf();
  Total_pdf->Print();
  cout<<"=============================================================="<<endl;
  RooAbsData* data = (RooAbsData*) ws->data("obsData");
  data->Print();
  cout<<"=============================================================="<<endl;
  RooRealVar* poi = (RooRealVar*) Mconfig->GetParametersOfInterest()->createIterator()->Next();
  std::cout << "Param of Interest: " << poi->GetName() << std::endl;
  RooArgSet *obs = (RooArgSet*) Mconfig->GetObservables();
  RooCategory *idx = (RooCategory*) obs->find("channelCat");
  RooRealVar *Bs_corrMass  = (RooRealVar*) obs->find("obs_x_channel");
  Bs_corrMass->SetTitle("B Corr. mass");
  Bs_corrMass->setUnit("MeV^{2}");
  Bs_corrMass->Print("V");
  cout<<"=============================================================="<<endl;
  cout<<"=============================================================="<<endl;
  
  HistFactorySimultaneous* Total_pdf_sim = new HistFactorySimultaneous( *Total_pdf );//needed to use Beeston-Barlow
  RooAbsReal* nll =  Total_pdf_sim  ->createNLL( *data , Offset(kTRUE));//Add external constarints in the argument. 
  RooMinuit* minuit_nll = new RooMinuit( *nll ) ;
  minuit_nll->setStrategy(2);
  minuit_nll->setErrorLevel(0.5);
  minuit_nll->simplex();
  minuit_nll->hesse();
  minuit_nll->migrad();
  // minuit_nll->fit("mh");
  // minuit_nll->simplex();
  RooFitResult *res_save = minuit_nll->save();
  // Plotting RooFit style : 
  
  double Bu_yield    = ((RooRealVar*)(Mconfig->GetNuisanceParameters()->find("B2JpsiK_Yield")))->getVal();
  cout<<"Bu_yield = " << Bu_yield <<endl;
  double Sig_yield   = poi->getVal();
  cout<<"Sig_yield = " << Sig_yield <<endl; 
  double InclK_yield = ((RooRealVar*)(Mconfig->GetNuisanceParameters()->find("incl_kmu_Yield")))->getVal();
  cout<<"InclK_yield = " << InclK_yield <<endl; 
  // double BsKstmunu_yield = ((RooRealVar*)(Mconfig->GetNuisanceParameters()->find("BsKstmunu_Yield")))->getVal();
  //cout<<"BsKstmunu_yield = " << BsKstmunu_yield <<endl; 


  //double Scale_F = (n_data/(Sig_yield+Bu_yield+InclK_yield+BsKstmunu_yield ));
  double Scale_F = (n_data/(Sig_yield+Bu_yield+InclK_yield ));
     
  RooPlot *CorrMass_frame = Bs_corrMass->frame( Title(" B Corr. mass") );
  data->plotOn(CorrMass_frame , DataError(RooAbsData::Poisson) , 
               MarkerSize(0.7),DrawOption("ZP") , Cut("channelCat==0") ); //
  
  Total_pdf_sim->plotOn(CorrMass_frame  , Slice(*idx) , ProjWData(*idx,*data) , 
		    DrawOption("F") , FillColor( kBlue ) , LineColor( kBlue ) ,FillStyle(0)) ;   
  /* Total_pdf->plotOn(CorrMass_frame  , Slice(*idx) , ProjWData(*idx,*data) , Components("*BsKstmunu*") , 
		    LineColor( kViolet ) ,  DrawOption("F") , FillColor( kViolet+1 ) , FillStyle(0),
		    Normalization( (BsKstmunu_yield)*Scale_F  ,RooAbsReal::NumEvent)); //*/
  Total_pdf_sim->plotOn(CorrMass_frame  , Slice(*idx) , ProjWData(*idx,*data) , Components("*Incl_Kmu*") , 
                    LineColor( kRed ) , DrawOption("F") , FillColor( kRed+2 ) , FillStyle(0),
                    Normalization( InclK_yield*Scale_F  , RooAbsReal::NumEvent)); //
  Total_pdf_sim->plotOn(CorrMass_frame , Slice(*idx),ProjWData(*idx,*data) , Components("*signal*") , 
                    DrawOption("F") , FillColor( kGreen+2 ) , LineColor( kGreen+2 ) , FillStyle(0),
                    Normalization( Sig_yield*Scale_F   ,RooAbsReal::NumEvent) ); //
  Total_pdf_sim->plotOn(CorrMass_frame , Slice(*idx),ProjWData(*idx,*data) , Components("*B2JpsiK*") ,                                 
                    DrawOption("F") , FillColor( kOrange+1 ) , LineColor( kOrange+3 ) ,  FillStyle(0),                                                  
                    Normalization( Bu_yield*Scale_F  , RooAbsReal::NumEvent) );
  
  //CorrMass_frame->pullHist();
  data->plotOn(CorrMass_frame , DataError(RooAbsData::Poisson) , MarkerSize(0.7),DrawOption("ZP") ,Cut("channelCat==0"));
  
  CorrMass_frame->Draw();
  CorrMass_frame->Print("Bs_corrmass_templatefit_newBDToptimization.eps");
  CorrMass_frame->Print("Bs_corrmass_templatefit_newBDToptimization.C");
  
  cout<<" n_data      = " << n_data      << endl;
  cout<<" Sig_yield   = " << Sig_yield   << endl; 
  cout<<" Bu_yield    = " << Bu_yield    << endl;
  cout<<" InclK_yield = " << InclK_yield << endl;
  //cout<<" BsKstmunu_yield = " << BsKstmunu_yield<< endl;
  //cout<<" Sig_yield + Bu_yield + InclK_yield +BsKstmunu_yield = " << n_data - Sig_yield - Bu_yield -BsKstmunu_yield <<endl;
  cout<<" Sig_yield + Bu_yield + InclK_yield = " << n_data - Sig_yield - Bu_yield <<endl;


   
  // Get histos after fit :
  // TH1 *Incl_Kmu_afterfit_h = Incl_Kmu.GetHisto();
  // cout<<"HIIIIIIII"<<endl;
  // Incl_Kmu_afterfit_h->Print();
  RooStats::HistFactory::HistFactoryNavigation HF_Nav( Mconfig);
    
  TH1F* Signal_histoafterfit   =(TH1F*)HF_Nav.GetSampleHist("channel" , "signal" ,"Signal_histoafterfit" );
  Signal_histoafterfit->Print();
  
  TH1F* inclukmu_histoafterfit = (TH1F*)HF_Nav.GetSampleHist("channel" , "Incl_Kmu" , "inclukmu_histoafterfit");
  inclukmu_histoafterfit->Print();
  inclukmu_histoafterfit->Sumw2();
  Signal_histoafterfit->Sumw2();
  //inclukmu_histoafterfit->Scale(1./inclukmu_histoafterfit ->Integral());
  //Signal_histoafterfit->Scale(1./Signal_histoafterfit ->Integral());

  TH1F* B2JpsiK_histoafterfit = (TH1F*)HF_Nav.GetSampleHist("channel" , "B2JpsiK" , "B2JpsiK_histoafterfit");
  B2JpsiK_histoafterfit->Print();
  B2JpsiK_histoafterfit->Sumw2();
  //B2JpsiK_histoafterfit->Scale(1./B2JpsiK_histoafterfit ->Integral());

  /* TH1F* BsKstmunu_histoafterfit = (TH1F*)HF_Nav.GetSampleHist("channel" , "BsKstmunu" , "BsKstmunu_histoafterfit");
  BsKstmunu_histoafterfit->Print();
  BsKstmunu_histoafterfit->Sumw2();*/
  //SetBinErrorhisto(Signal_histoafterfit);
  //SetBinErrorhisto(inclukmu_histoafterfit);
  //SetBinErrorhisto(B2JpsiK_histoafterfit);

  TCanvas *c_Sigbeforeafter = new TCanvas("c_Sigbeforeafter", "c_Sigbeforeafter");
  Signal_histoafterfit->Draw("");
  h_MCsignal->SetLineColor(kBlack);
  h_MCsignal->Draw("samep");
  gPad->BuildLegend(0.75,0.75,0.95,0.95,"");
  
  //RooArgSet* model_comps = Total_pdf->getComponents() ;
  TCanvas *c_Inclkmubeforeafter = new TCanvas("c_Inclkmubbeforeafter", "c_Inclkmubbeforeafter");
  inclukmu_histoafterfit->Draw("");
  h_MCinclumuK->SetLineColor(kBlack);
  h_MCinclumuK->Draw("samep");
  gPad->BuildLegend(0.75,0.75,0.95,0.95,"");
  
  TCanvas *c_B2JpsiK_histoafterfit = new TCanvas("c_B2JpsiK_histoafterfit", "c_B2JpsiK_histoafterfit");
  B2JpsiK_histoafterfit->Draw("");
  h_MCjpsiK->Draw("sameep");
  gPad->BuildLegend(0.75,0.75,0.95,0.95,"");

  TCanvas *c_data = new TCanvas("c_data", "c_data");
  h_data->Draw("");
  
  /*TCanvas *c_BsKstmunu_histoafterfit = new TCanvas("c_BsKstmunu_histoafterfit", "c_BsKstmunu_histoafterfit");
  BsKstmunu_histoafterfit->Draw("hist");
  h_MCKstmunu->Draw("sameep");*/
  

  THStack *histostack = HF_Nav.GetChannelStack("channel","Stack_afterFit");
  TCanvas *c_Stack = new TCanvas("c_Stack", "c_Stack");
  //TH1F* h_stack = (TH1F*)h_data->Clone();
  //  h_data->Draw();
  histostack->Draw();
  h_data->Draw("SAME");
  c_Stack->Print("stack_newBDToptimization.C");
  c_Stack->Print("stack_newBDToptimization.root");

   TCanvas *bla= new TCanvas("bla", "bla", 1600, 1600);
   THStack* stack = new THStack("Stack", "");
   // TH1F* h_stack = (TH1F*)h_data->Clone();
   //DecorateHisto(B2JpsiK_histoafterfit);
   B2JpsiK_histoafterfit->SetFillColor(kOrange+1);
    B2JpsiK_histoafterfit->SetLineColor(kOrange+1);
    stack->Add(B2JpsiK_histoafterfit);
   //DecorateHisto(Signal_histoafterfit);
   Signal_histoafterfit->SetFillColor(kGreen+2);
   //Signal_histoafterfit->SetMarkerStyle(21);
   Signal_histoafterfit->SetLineColor(kGreen+2);
   stack->Add(Signal_histoafterfit);
   /*BsKstmunu_histoafterfit->SetFillColor(kBlue+2);
   BsKstmunu_histoafterfit->SetLineColor(kBlue+2);
   stack->Add(BsKstmunu_histoafterfit);*/
   //DecorateHisto(inclukmu_histoafterfit);
   inclukmu_histoafterfit->SetFillColor(kRed+2);
   inclukmu_histoafterfit->SetLineColor(kRed+2);
   stack->Add(inclukmu_histoafterfit);
   stack->Draw("hist");
   h_data->Draw("SAME");
   stack->GetYaxis()->SetTitle("Events / ( 48 MeV^{2} )");
   stack->GetXaxis()->SetTitle("Bs Corr. mass (MeV^{2})");
   gPad->BuildLegend(0.75,0.75,0.95,0.95,"");
   bla->Print("stack2_newBDToptimization.C");
   bla->Print("stack2_newBDToptimization.root");
   bla->Print("stack2_newBDToptimization.pdf");
   

  
  
  return 0;
}


