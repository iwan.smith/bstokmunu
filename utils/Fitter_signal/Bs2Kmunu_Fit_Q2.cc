#include "/afs/cern.ch/user/b/bkhanji/roofitinclude.h"
#include <iostream>
#include <TROOT.h>
#include <TLatex.h>
#include <TFile.h>
#include <TTree.h>
#include <sstream>
#include <TChain.h>
#include <TCanvas.h>
#include <TObjArray.h>
#include <TBranch.h>
#include <TLorentzVector.h>
#include <iomanip>
#include <TH1F.h>
#include "RooStats/ModelConfig.h"
#include "RooStats/HistFactory/Measurement.h"
#include "RooStats/HistFactory/MakeModelAndMeasurementsFast.h"
#include "RooStats/HistFactory/HistFactorySimultaneous.h"
#include "RooStats/HistFactory/HistFactoryNavigation.h"
#include "assert.h"

using namespace RooFit;
using namespace RooStats;
using namespace HistFactory;
using namespace std;


void CheckNonZeroBin(TH1* h=0){
    Int_t binx = h->GetNbinsX();
    for(Int_t i = 0; i<= binx; i++){
    	if(h->GetBinContent(i)<=0) {
	  //cout<<"ZERO BIN FOUND at: "<<i<<", "<<endl;
	h->SetBinContent(i,1e-12);//set empty bins to 1e-12
	}
      }
  }

void DecorateHisto(TH1D* Data_CorrMass=0)
{
  //gPad->SetGrid();  
  //gPad->SetTickx();
  //gPad->SetTicky();
  //Data_CorrMass->SetTitle("");
  //gStyle->SetOptStats(1111);
  Data_CorrMass->SetStats(kTRUE);
  Data_CorrMass->GetYaxis()->SetTitle("Events / ( 48 MeV )");
  Data_CorrMass->GetXaxis()->SetTitle("Bs Corr. mass (MeV)");
  //Data_CorrMass->GetYaxis()->SetTitleOffset(0.9);
  //Data_CorrMass->SetStats(0);
  //Data_CorrMass->SetMinimum(0);
  //Data_CorrMass->GetYaxis()->SetNdivisions(7+100*5);
}

void DecorateHisto2(TH1D* Data_CorrMass=0)
{
  //gPad->SetGrid();  
  //gPad->SetTickx();
  //gPad->SetTicky();
  //Data_CorrMass->SetTitle("");
  //gStyle->SetOptStats(1111);
  Data_CorrMass->SetStats(kTRUE);
  //Data_CorrMass->GetYaxis()->SetTitle("Events / ( 48 MeV^{2} )");
  //Data_CorrMass->GetXaxis()->Setions(7+100*5);
  Data_CorrMass->GetXaxis()->SetTitle("Q^{2} BEST (MeV^{2})");
  //Data_CorrMass->GetYaxis()->SetTitleOffset(0.9);
  //Data_CorrMass->SetStats(0);
  //Data_CorrMass->SetMinimum(0);
  
  //Data_CorrMass->GetYaxis()->SetNdivisions(7+100*5);
}


void SetBinErrorhisto(TH1F* h=0){
    Int_t binx = h->GetNbinsX();
    for(Int_t i = 0; i<= binx; i++){
      // cout << "BinError: "<< h->GetName()<< " " << h->GetBinError(i)<<endl;
      h->SetBinError(i, 1e-12);
      }
  }


TH1D* GetQ2(TString data_PATH="" , TString name = ""  , TString MyCuts= "" )
{
  
  TChain *fdata_chain = new TChain("data_OS","data_OS");
  fdata_chain->Add( data_PATH );
  fdata_chain->Print();
  TDirectory *where = gDirectory;
  where->cd();
  //TH1D *Data_CorrMass = new TH1D( name+ "_Bs_MCORR" , name+ "_Bs_MCORR" , 40 , 2500 , 5370  ) ;
  TH1D *Data_Q2_BEST = new TH1D( name , name , 100 , -5000000, 25000000  ) ;
  Data_Q2_BEST->Sumw2();
  fdata_chain->Project(Data_Q2_BEST->GetName() , "Bs_Regression_Q2_BEST" , MyCuts , "" , 100000000000, 0);//, "" ); 
  cout<< "Name of histo: "<< Data_Q2_BEST->GetName()<< endl;
  //Data_Q2_BEST->Scale(1./1000000);
  TCanvas *q2 = new TCanvas("q2_"+name , "q2_" + name );
  q2->cd();
  DecorateHisto2(Data_Q2_BEST);
  //Data_CorrMass->SetTitle("");
  Data_Q2_BEST->Draw();  
  return Data_Q2_BEST;
  }

TH1D* GetCorr_h(TString data_PATH="" , TString name = ""  , TString MyCuts= "" )
{
  
  TChain *fdata_chain = new TChain("data_OS","data_OS");
  fdata_chain->Add( data_PATH );
  fdata_chain->Print();
  TDirectory *where = gDirectory;
  where->cd();
  //TH1D *Data_CorrMass = new TH1D( name+ "_Bs_MCORR" , name+ "_Bs_MCORR" , 40 , 2500 , 5370  ) ;
  TH1D *Data_CorrMass = new TH1D( name+ "_Bs_MCORR" , name+ "_Bs_MCORR" , 60 , 2500, 5380  ) ;
  Data_CorrMass->Sumw2();
  fdata_chain->Project(Data_CorrMass->GetName() , "Bs_MCORR" , MyCuts , "" , 100000000000, 0);//, "" ); 
  cout<< "Name of histo: "<< Data_CorrMass->GetName()<< endl;
  //Data_CorrMass->Scale(1./Data_CorrMass->Integral());
  TCanvas *data_c = new TCanvas("c_"+name , "c_" + name );
  data_c->cd();
  DecorateHisto(Data_CorrMass);
  //Data_CorrMass->SetTitle("");
  Data_CorrMass->Draw();  
  return Data_CorrMass;
  }

int Bs2Kmunu_Fit_Q2()
{
  /*
  // Cuts :
  TString MyCuts = "TMVA_charge_BDT>0.107 && TMVA_SS_aftercut_BDT>0.1 && Bs_Regression_Q2_BEST>7000000";
  // && muon_p_IsoMinBDT >-0.5";//"TMVA_charge_BDT>-0.2 &&TMVA_SS_aftercut_BDT>-0.2";
  //"TMVA_charge_BDT>0.107 &&TMVA_SS_aftercut_BDT>-0.0954 && (1820>kaon_m_PAIR_M || kaon_m_PAIR_M>1880)" ;
  // Get Corrected mass :

  TString eosMC    = "root://eoslhcb.cern.ch//eos/lhcb/wg/semileptonic/Bs2KmunuAna/Tuples/Bs2KMuNu_MCTUPLES_TRIMMED_30May16/";
  //old TString eosMC    = "root://eoslhcb.cern.ch//eos/lhcb/wg/semileptonic/Bs2KmunuAna/Tuples/Bs2KMuNu_MCTUPLES_TRIMMED_BDT_30May16/";
  // old BDT TString eosMC    = "root://eoslhcb.cern.ch//eos/lhcb/wg/semileptonic/Bs2KmunuAna/Tuples/Bs2KMuNu_MCTUPLES_WITHBDT_30June16/";
  // old BDT TString eosdata  = "root://eoslhcb.cern.ch//eos/lhcb/wg/semileptonic/Bs2KmunuAna/Tuples/Bs2KMuNu_DATATUPLES_TRIMMED_WITHBDT_29June16/";
  TString eosdata  = "root://eoslhcb.cern.ch//eos/lhcb/wg/semileptonic/Bs2KmunuAna/Tuples/Bs2KMuNu_DATATUPLES_TRIMMED_19June16/";
  //
  TString SignalMC         = eosMC + "DTT_13512010_Bs_Kmunu_DecProdCut_*_Py8_MC12_*.root/reducedTree";
  TString Bu2JpsiKplusMC   = eosMC + "DTT_12143001_Bu_JpsiK_mm_DecProdCut_*_Py8_MC12_*.root/reducedTree";
  TString Bu2JpsiKstarMC   = eosMC + "DTT_12143401_Bu_JpsiKst_mm_Kpi0_DecProdCut_*_Py8_MC12_*.root/reducedTree";
  TString Bs2DsmunuMC      = eosMC + "DTT_13774000_Bs_Dsmunu_cocktail_hqet2_DsmuInAcc_*_Py8_MC12_*.root/reducedTree";
  TString inclb_OC2Kplusmu = eosMC + "DTT_MC12_inclb_OC2Kplusmu_gencut_BKG_10010037_*.root/reducedTree" ;
  TString inclb_OC2Kmu     = eosMC + "DTT_MC12_inclb_OC2Kmu_gencut_BKG_10010032_*.root/reducedTree"     ;
  TString inclb_OC2KmuSS   = eosMC + "DTT_MC12_inclb_OC2KmuSS_gencut_BKG_10010035_*.root/reducedTree"   ;
  TString inclb_ALL        = eosMC + "DTT_MC12_inclb_OC2K*.root/reducedTree"   ;
  TString Bd_pimunu = eosMC + "DTT_11512011_Bd_pimunu_DecProdCut_*_Py8_MC12_*.root/reducedTree"   ;
  TString Bd_JpsiKst = eosMC + "DTT_11144001_Bd_JpsiKst_mm_DecProdCut_*_Py8_MC12_*.root/reducedTree"   ;
  TString Bd_rhomunu = eosMC + "DTT_11512400_Bd_rhomunu_pipi0_DecProdCut_*_Py8_MC12_*.root/reducedTree"   ;
  TString Bd_Dstmunu = eosMC + "DTT_11874004_Bd_Dstmunu_Kpi_cocktail_hqet_D0muInAcc_BRCorr1_*_Py8_MC12_*.root/reducedTree"   ;
  TString Bd_Dmunu = eosMC + "DTT_11874042_Bd_Dmunu_K-pi+pi+_cocktail_hqet_BRCorr1_DmuInAcc_*_Py8_MC12.root/reducedTree"   ;
  //TString Bd_Dmunu = eosMC + "DTT_11874042_Bd_Dmunu_K-pi+pi+_cocktail_hqet_BRCorr1_DmuInAcc_*_Py8_MC12.root/reducedTree"   ;
  TString Bu_D0munu  = eosMC + "DTT_12873002_Bu_D0munu_Kpi_cocktail_D0muInAcc_BRcorr1_*_Py8_MC12_*.root/reducedTree"   ;
  TString Bs_Jpsiphi = eosMC + "DTT_13144001_Bs_Jpsiphi_mm_CPV_update2012_DecProdCut_*_Py8_MC12_*.root/reducedTree"   ;
  TString Bs_Kstmunu = eosMC + "DTT_13512400_Bs_Kstmunu_Kpi0_DecProdCut_*_Py8_MC12_*.root/reducedTree"   ;
  TString Bs_K2stmunu = eosMC + "DTT_13512410_Bs_K2stmunu_Kpi0_DecProdCut_*_Py8_MC12_*.root/reducedTree"   ;
  TString Bs_K1430munu = eosMC + "DTT_13512420_Bs_K1430munu_Kpi0_DecProdCut_*_Py8_MC12_*.root/reducedTree"   ;
  TString Lb_pmunu = eosMC + "DTT_15512014_Lb_pmunu_DecProdCut_LQCD_*_Py8_MC12_*.root/reducedTree"   ;
  TString Bs_D0DsK = eosMC + "DTT_13796000_Bs_D0DsK_Kmunu_KKpi_DecProdCut_tightCut_*_Py8_MC12_*.root/reducedTree"   ;
  TString Bd_Dpi = eosMC + "DTT_11574051_Bd_Dpi_Kpimunu_DecProdCut_*_Py8_MC12_*.root/reducedTree"   ;
  TString Bu_rhomunu = eosMC + "DTT_12513001_Bu_rhomunu_DecProdCut_*_Py8_MC12_*.root/reducedTree"   ;
  TString Bu_D0pi = eosMC + "DTT_12573050_Bu_D0pi_Kmunu_DecProdCut_*_Py8_MC12_*.root/reducedTree"   ;
  TString Bd_Dst0pi_D0gamma = eosMC + "DTT_12573200_Bu_Dst0pi_D0gamma_Kmunu_DecProdCut_*_Py8_MC12_*.root/reducedTree"   ;
  TString Bd_Dst0pi_D0pi0 = eosMC + "DTT_12573400_Bu_Dst0pi_D0pi0_Kmunu_DecProdCut_*_Py8_MC12_*.root/reducedTree"   ;
  TString Bd_D0rho = eosMC + "DTT_12573401_Bu_D0rho_Kmunu_pipi0_DecProdCut_*_Py8_MC12_*.root/reducedTree"   ;
  //TString Bd_D0Kst0 = eosMC + "DTT_13574040_Bs_D0Kst0_Kmunu_Kpi_DecProdCut_*_Py8_MC12.root/reducedTree"   ;
  //
  //TString DataOS_2012      = "/afs/cern.ch/work/b/bkhanji/public/DTT_2012_trimmed.root/Bs2KmuNuTuple/reducedTree";
  //TString DataSS_2011      = "/afs/cern.ch/work/b/bkhanji/public/DTT_2012_trimmed_SS.root/Bs2KmuNuTuple/reducedTree" ;
  TString DataOS_2012 = eosdata + "*_DTT_2012_Reco14Strip21r0p1a_*_SEMILEPTONIC_trimmed.root/reducedTree";//95_DTT_2012_Reco14Strip21r0p1a_Down_SEMILEPTONIC_trimmed.root
  // ================================================================
  // Corrected mass
  TH1D *Data_CorrMass_OS_2012         = GetCorr_h( DataOS_2012 , "OS_2012" , MyCuts );
  //TH1D *Data_CorrMass_SS_2012         = GetCorr_h( DataSS_2011 , "SS_2012" , MyCuts );
  TH1D *Signal_CorrMass_MC_2012       = GetCorr_h( SignalMC , "SignalMC" , MyCuts );
  TH1D *Bu2JpsiKplus_CorrMass_MC_2012 = GetCorr_h( Bu2JpsiKplusMC , "Bu2JpsiKplusMC" , MyCuts );
  TH1D *Bu2JpsiKstar_CorrMass_MC_2012 = GetCorr_h( Bu2JpsiKstarMC , "Bu2JpsiKstarMC" , MyCuts );
  TH1D *Bs2Dsmunu_CorrMass_MC_2012    = GetCorr_h( Bs2DsmunuMC    , "Bs2DsmunuMC"    , MyCuts );
  TH1D *Bd_pimunu_CorrMass_MC_2012       = GetCorr_h( Bd_pimunu , "Bd_pimunu" , MyCuts );
  TH1D *Bd_JpsiKst_CorrMass_MC_2012       = GetCorr_h( Bd_JpsiKst , "Bd_JpsiKst" , MyCuts );
  TH1D *Bd_rhomunu_CorrMass_MC_2012       = GetCorr_h( Bd_rhomunu , "Bd_rhomunu" , MyCuts );
  TH1D *Bd_Dstmunu_CorrMass_MC_2012       = GetCorr_h( Bd_Dstmunu , "Bd_Dstmunu" , MyCuts );
  TH1D *Bd_Dmunu_CorrMass_MC_2012       = GetCorr_h( Bd_Dmunu , "Bd_Dmunu" , MyCuts );
  TH1D *Bu_D0munu_CorrMass_MC_2012       = GetCorr_h( Bu_D0munu , "Bu_D0munu" , MyCuts );
  TH1D *Bs_Jpsiphi_CorrMass_MC_2012       = GetCorr_h( Bs_Jpsiphi , "Bs_Jpsiphi" , MyCuts );
  TH1D *Bs_Kstmunu_CorrMass_MC_2012       = GetCorr_h( Bs_Kstmunu , "Bs_Kstmunu" , MyCuts );
  TH1D *Bs_K2stmunu_CorrMass_MC_2012       = GetCorr_h( Bs_K2stmunu , "Bs_K2stmunu" , MyCuts );
  TH1D *Bs_K1430munu_CorrMass_MC_2012       = GetCorr_h( Bs_K1430munu , "Bs_K1430munu" , MyCuts );
  TH1D *Lb_pmunu_CorrMass_MC_2012       = GetCorr_h( Lb_pmunu , "Lb_pmunu" , MyCuts );
  TH1D * Bs_D0DsK_CorrMass_MC_2012     = GetCorr_h( Bs_D0DsK , "Bs_D0DsK" , MyCuts );
  TH1D * Bd_Dpi_CorrMass_MC_2012     = GetCorr_h(Bd_Dpi  , "Bd_Dpi" , MyCuts );
  TH1D * Bu_rhomunu_CorrMass_MC_2012     = GetCorr_h(Bu_rhomunu  , "Bu_rhomunu" , MyCuts );
  TH1D * Bu_D0pi_CorrMass_MC_2012     = GetCorr_h( Bu_D0pi , "Bu_D0pi" , MyCuts );
  TH1D * Bd_Dst0pi_D0gamma_CorrMass_MC_2012     = GetCorr_h(Bd_Dst0pi_D0gamma  , "Bd_Dst0pi_D0gamma" , MyCuts );
  TH1D * Bd_Dst0pi_D0pi0_CorrMass_MC_2012     = GetCorr_h( Bd_Dst0pi_D0pi0 , "Bd_Dst0pi_D0pi0" , MyCuts );
  TH1D * Bd_D0rho_CorrMass_MC_2012     = GetCorr_h( Bd_D0rho , "Bd_D0rho" , MyCuts );
  //TH1D * Bd_D0Kst0_CorrMass_MC_2012     = GetCorr_h(Bd_D0Kst0  , "Bd_D0Kst0" , MyCuts );

  TH1D *inclb_OC2Kplusmu_CorrMass_MC_2012 = GetCorr_h( inclb_OC2Kplusmu , "inclb_OC2Kplusmu" , MyCuts );
  TH1D *inclb_OC2Kmu_CorrMass_MC_2012     = GetCorr_h( inclb_OC2Kmu     , "inclb_OC2Kmu"     , MyCuts );
  TH1D *inclb_OC2KmuSS_CorrMass_MC_2012   = GetCorr_h( inclb_OC2KmuSS   , "inclb_OC2KmuSS"   , MyCuts );
  TH1D *inclb_ALL_CorrMass_MC_2012        = GetCorr_h( inclb_ALL        , "inclb_ALL"        , MyCuts );

  TH1D *Signal_Q2_MC_2012       = GetQ2( SignalMC , "SignalMC" , MyCuts );
  TH1D *Bu2JpsiKplus_Q2_MC_2012 = GetQ2( Bu2JpsiKplusMC , "Bu2JpsiKplusMC" , MyCuts );
  TH1D *Bu2JpsiKstar_Q2_MC_2012 = GetQ2( Bu2JpsiKstarMC , "Bu2JpsiKstarMC" , MyCuts );
  TH1D *Bs2Dsmunu_Q2_MC_2012    = GetQ2( Bs2DsmunuMC    , "Bs2DsmunuMC"    , MyCuts );
  TH1D *Bd_pimunu_Q2_MC_2012       = GetQ2( Bd_pimunu , "Bd_pimunu" , MyCuts );
  TH1D *Bd_JpsiKst_Q2_MC_2012       = GetQ2( Bd_JpsiKst , "Bd_JpsiKst" , MyCuts );
  TH1D *Bd_rhomunu_Q2_MC_2012       = GetQ2( Bd_rhomunu , "Bd_rhomunu" , MyCuts );
  TH1D *Bd_Dstmunu_Q2_MC_2012       = GetQ2( Bd_Dstmunu , "Bd_Dstmunu" , MyCuts );
  //TH1D *Bd_Dmunu_Q2_MC_2012       = GetCorr_h( Bd_Dmunu , "Bd_Dmunu" , MyCuts );
  TH1D *Bu_D0munu_Q2_MC_2012       = GetQ2( Bu_D0munu , "Bu_D0munu" , MyCuts );
  TH1D *Bs_Jpsiphi_Q2_MC_2012       = GetQ2( Bs_Jpsiphi , "Bs_Jpsiphi" , MyCuts );
  TH1D *Bs_Kstmunu_Q2_MC_2012       = GetQ2( Bs_Kstmunu , "Bs_Kstmunu" , MyCuts );
  TH1D *Bs_K2stmunu_Q2_MC_2012       = GetQ2( Bs_K2stmunu , "Bs_K2stmunu" , MyCuts );
  TH1D *Bs_K1430munu_Q2_MC_2012       = GetQ2( Bs_K1430munu , "Bs_K1430munu" , MyCuts );
  TH1D *Lb_pmunu_Q2_MC_2012       = GetQ2( Lb_pmunu , "Lb_pmunu" , MyCuts );
  TH1D * Bs_D0DsK_Q2_MC_2012     = GetQ2( Bs_D0DsK , "Bs_D0DsK" , MyCuts );
  TH1D * Bd_Dpi_Q2_MC_2012     = GetQ2(Bd_Dpi  , "Bd_Dpi" , MyCuts );
  TH1D * Bu_rhomunu_Q2_MC_2012     = GetQ2(Bu_rhomunu  , "Bu_rhomunu" , MyCuts );
  TH1D * Bu_D0pi_Q2_MC_2012     = GetQ2( Bu_D0pi , "Bu_D0pi" , MyCuts );
  TH1D * Bd_Dst0pi_D0gamma_Q2_MC_2012     = GetQ2(Bd_Dst0pi_D0gamma  , "Bd_Dst0pi_D0gamma" , MyCuts );
  TH1D * Bd_Dst0pi_D0pi0_Q2_MC_2012     = GetQ2( Bd_Dst0pi_D0pi0 , "Bd_Dst0pi_D0pi0" , MyCuts );
  TH1D * Bd_D0rho_Q2_MC_2012     = GetQ2( Bd_D0rho , "Bd_D0rho" , MyCuts );
  //TH1D * Bd_D0Kst0_CorrMass_MC_2012     = GetCorr_h(Bd_D0Kst0  , "Bd_D0Kst0" , MyCuts );
  TH1D *inclb_ALL_Q2_MC_2012        = GetQ2( inclb_ALL        , "inclb_ALL"        , MyCuts );
  TH1D *Data_Q2_OS_2012         = GetQ2( DataOS_2012 , "OS_2012" , MyCuts );

  cout<<"============================events=================================="<<endl;
  cout<< "Data_CorrMass_OS_2012 "<< Data_CorrMass_OS_2012->Integral()<< endl;
  cout<< "Signal_CorrMass_MC_2012 "<< Signal_CorrMass_MC_2012->GetEntries()<< endl;
  cout<< "Bu2JpsiKplus_CorrMass_MC_2012 "<< Bu2JpsiKplus_CorrMass_MC_2012->GetEntries()<< endl;
  cout<< "Bu2JpsiKstar_CorrMass_MC_2012 "<< Bu2JpsiKstar_CorrMass_MC_2012->GetEntries()<< endl;
  cout<< "Bs2Dsmunu_CorrMass_MC_2012 "<< Bs2Dsmunu_CorrMass_MC_2012->GetEntries()<< endl;
  cout<< "Bd_pimunu_CorrMass_MC_2012 "<< Bd_pimunu_CorrMass_MC_2012->GetEntries()<< endl;
  cout<< "Bd_JpsiKst_CorrMass_MC_2012 "<< Bd_JpsiKst_CorrMass_MC_2012->GetEntries()<< endl;
  cout<< "Bd_rhomunu_CorrMass_MC_2012 "<< Bd_rhomunu_CorrMass_MC_2012->GetEntries()<< endl;
  cout<< "Bd_Dstmunu_CorrMass_MC_2012 "<< Bd_Dstmunu_CorrMass_MC_2012->GetEntries()<< endl;
  cout<< "Bd_Dmunu_CorrMass_MC_2012 "<< Bd_Dmunu_CorrMass_MC_2012->GetEntries()<< endl;
  cout<< "Bu_D0munu_CorrMass_MC_2012 "<< Bu_D0munu_CorrMass_MC_2012->GetEntries()<< endl;
  cout<< "Bs_Jpsiphi_CorrMass_MC_2012 "<< Bs_Jpsiphi_CorrMass_MC_2012->GetEntries()<< endl;
  cout<< "Bs_Kstmunu_CorrMass_MC_2012 "<< Bs_Kstmunu_CorrMass_MC_2012->GetEntries()<< endl;
  cout<< "Bs_K2stmunu_CorrMass_MC_2012 "<< Bs_K2stmunu_CorrMass_MC_2012->GetEntries()<< endl;
  cout<< "Bs_K1430munu_CorrMass_MC_2012 "<< Bs_K1430munu_CorrMass_MC_2012->GetEntries()<< endl;
  cout<< "Lb_pmunu_CorrMass_MC_2012 "<< Lb_pmunu_CorrMass_MC_2012->GetEntries()<< endl;
  cout<< "Bs_D0DsK_CorrMass_MC_2012 "<< Bs_D0DsK_CorrMass_MC_2012->GetEntries()<< endl;
  cout<< "Bd_Dpi_CorrMass_MC_2012 "<< Bd_Dpi_CorrMass_MC_2012->GetEntries()<< endl;
  cout<< "Bu_rhomunu_CorrMass_MC_2012 "<< Bu_rhomunu_CorrMass_MC_2012->GetEntries()<< endl;
  cout<< "Bu_D0pi_CorrMass_MC_2012 "<< Bu_D0pi_CorrMass_MC_2012->GetEntries()<< endl;
  cout<< "Bd_Dst0pi_D0gamma_CorrMass_MC_2012 "<< Bd_Dst0pi_D0gamma_CorrMass_MC_2012->GetEntries()<< endl;
  cout<< "Bd_Dst0pi_D0pi0_CorrMass_MC_2012 "<< Bd_Dst0pi_D0pi0_CorrMass_MC_2012->GetEntries()<< endl;
  cout<< "Bd_D0rho_CorrMass_MC_2012 "<< Bd_D0rho_CorrMass_MC_2012->GetEntries()<< endl;
  //cout<< "Bd_D0Kst0_CorrMass_MC_2012 "<< Bd_D0Kst0_CorrMass_MC_2012->GetEntries()<< endl;
  cout<< "inclb_OC2Kplusmu_CorrMass_MC_2012 "<< inclb_OC2Kplusmu_CorrMass_MC_2012->GetEntries()<< endl;
  cout<< "inclb_OC2Kmu_CorrMass_MC_2012 "<< inclb_OC2Kmu_CorrMass_MC_2012->GetEntries()<< endl;
  cout<< "inclb_OC2KmuSS_CorrMass_MC_2012 "<< inclb_OC2KmuSS_CorrMass_MC_2012->GetEntries()<< endl;
  cout<< "inclb_ALL_CorrMass_MC_2012 "<< inclb_ALL_CorrMass_MC_2012->GetEntries()<< endl;
  */

  /*TCanvas *compare = new TCanvas("compare" , "compare");
  //Data_CorrMass_OS_2012->DrawNormalized("ep");
  
  Bu2JpsiKplus_Q2_MC_2012->SetLineColor(kRed)         ; Bu2JpsiKplus_Q2_MC_2012->DrawNormalized("hist");
  Signal_Q2_MC_2012->SetLineColor(kGreen+2)           ; Signal_Q2_MC_2012->DrawNormalized("hist same");
  Bs_Jpsiphi_Q2_MC_2012->SetLineColor(kViolet)  ;Bs_Jpsiphi_Q2_MC_2012->DrawNormalized("hist same");
  //Bs_Kstmunu_Q2_MC_2012->SetLineColor(kPink);Bs_Kstmunu_Q2_MC_2012->DrawNormalized("hist same");
  //Bs_K2stmunu_Q2_MC_2012->SetLineColor(kMagenta);Bs_K2stmunu_Q2_MC_2012->DrawNormalized("hist same");
  Bd_JpsiKst_Q2_MC_2012->SetLineColor(kOrange);Bd_JpsiKst_Q2_MC_2012->DrawNormalized("hist same");
  //Bs_K1430munu_Q2_MC_2012->SetLineColor(kGreen);Bs_K1430munu_Q2_MC_2012->DrawNormalized("hist same");
  //Bu_D0munu_Q2_MC_2012->SetLineColor(kGreen+4);Bu_D0munu_Q2_MC_2012->DrawNormalized("hist same");
  //Lb_pmunu_Q2_MC_2012->SetLineColor(kYellow);Lb_pmunu_Q2_MC_2012->DrawNormalized("hist same");
  //Bd_pimunu_Q2_MC_2012->SetLineColor(28);Bd_pimunu_Q2_MC_2012->DrawNormalized("hist same");
  //Bd_rhomunu_Q2_MC_2012->SetLineColor(kBlack); Bd_rhomunu_Q2_MC_2012->DrawNormalized("hist same");

  compare->BuildLegend(0.1,0.5,0.3,0.9);
  compare->Print("compare1_Q2_MC_newBDToptimization.pdf");

  TCanvas *compare2 = new TCanvas("compare2" , "compare2");
  //Data_CorrMass_OS_2012->DrawNormalized("ep");
  
   Signal_Q2_MC_2012->SetLineColor(kGreen+2)           ; Signal_Q2_MC_2012->DrawNormalized("hist");
  
  Bs_Kstmunu_Q2_MC_2012->SetLineColor(kPink);Bs_Kstmunu_Q2_MC_2012->DrawNormalized("hist same");
  Bs_K2stmunu_Q2_MC_2012->SetLineColor(kMagenta);Bs_K2stmunu_Q2_MC_2012->DrawNormalized("hist same");
  Bs_K1430munu_Q2_MC_2012->SetLineColor(kGreen);Bs_K1430munu_Q2_MC_2012->DrawNormalized("hist same");
  compare2->BuildLegend(0.1,0.5,0.3,0.9);
  compare2->Print("compare2_Q2_MC_newBDToptimization.pdf");

  TCanvas *compare3 = new TCanvas("compare3" , "compare3");
  Bs2Dsmunu_Q2_MC_2012->SetLineColor(kBlue);Bs2Dsmunu_Q2_MC_2012->DrawNormalized("hist");
  inclb_ALL_Q2_MC_2012->SetLineColor(kOrange+4)       ; inclb_ALL_Q2_MC_2012->DrawNormalized("hist same");
  //Bu_D0munu_Q2_MC_2012->SetLineColor(kGreen+4);Bu_D0munu_Q2_MC_2012->DrawNormalized("hist");
  //Lb_pmunu_Q2_MC_2012->SetLineColor(kYellow);Lb_pmunu_Q2_MC_2012->DrawNormalized("hist same");
  //Bd_pimunu_Q2_MC_2012->SetLineColor(28);Bd_pimunu_Q2_MC_2012->DrawNormalized("hist same");
  //Bd_rhomunu_Q2_MC_2012->SetLineColor(kBlack); Bd_rhomunu_Q2_MC_2012->DrawNormalized("hist same");
  //Bd_Dstmunu_Q2_MC_2012->SetLineColor(kGreen);Bd_Dstmunu_Q2_MC_2012->DrawNormalized("hist same");
  
  Signal_Q2_MC_2012->SetLineColor(kGreen+2)           ; Signal_Q2_MC_2012->DrawNormalized("hist same");

  compare3->BuildLegend(0.1,0.5,0.3,0.9);
  compare3->Print("compare3_Q2_MC_newBDToptimization.pdf");


  TCanvas *compare4 = new TCanvas("compare4" , "compare4");
  //Lb_pmunu_Q2_MC_2012->SetLineColor(kYellow);Lb_pmunu_Q2_MC_2012->DrawNormalized("hist");
  Bd_Dstmunu_Q2_MC_2012->SetLineColor(kGreen);Bd_Dstmunu_Q2_MC_2012->DrawNormalized("hist");
  Bu_D0munu_Q2_MC_2012->SetLineColor(kBlue);Bu_D0munu_Q2_MC_2012->DrawNormalized("hist same");
  Bd_pimunu_Q2_MC_2012->SetLineColor(kRed);Bd_pimunu_Q2_MC_2012->DrawNormalized("hist same");
  Bd_rhomunu_Q2_MC_2012->SetLineColor(kBlack); Bd_rhomunu_Q2_MC_2012->DrawNormalized("hist same");
  
  
  Signal_Q2_MC_2012->SetLineColor(kGreen+2)           ; Signal_Q2_MC_2012->DrawNormalized("hist same");

  compare4->BuildLegend(0.1,0.5,0.3,0.9);
  compare4->Print("compare4_Q2_MC_newBDToptimization.pdf");*/
  

  // TH1D *Data_CorrMass_SS_2012_scaled = (TH1D*)Data_CorrMass_SS_2012->Clone();
  // Data_CorrMass_SS_2012_scaled->Scale(10);
  //
  //  TCanvas *data_compare_c = new TCanvas("data_compare_c","data_compare_c");
  //  Data_CorrMass_OS_2012->SetTitle("");
  //  Data_CorrMass_OS_2012->SetLineColor(kBlack);
  //  Data_CorrMass_SS_2012->SetLineColor(kRed);
  //  Data_CorrMass_SS_2012_scaled->SetLineColor(kMagenta);
  //  Data_CorrMass_OS_2012->Draw();
  //  Data_CorrMass_SS_2012->Draw("same");
  //  Data_CorrMass_SS_2012_scaled->Draw("same");
  //  data_compare_c->BuildLegend();

  //TFile *Histos_f = new TFile("Histos_allMCfiles_newBDToptimization_lessQ2cut.root", "RECREATE");
  //TFile *Histos_f = new TFile("Histos_datafile_newBDToptimization_Q2.root", "RECREATE");
  //Data_CorrMass_OS_2012->Write();
  /*Signal_CorrMass_MC_2012->Write();
  Bu2JpsiKplus_CorrMass_MC_2012->Write();
  Bu2JpsiKstar_CorrMass_MC_2012->Write();
  Bs_Jpsiphi_CorrMass_MC_2012->Write();
  inclb_ALL_CorrMass_MC_2012->Write();
  Bs_Kstmunu_CorrMass_MC_2012->Write();
  Bs_K2stmunu_CorrMass_MC_2012->Write();
  Bd_JpsiKst_CorrMass_MC_2012->Write();
  Bs_K1430munu_CorrMass_MC_2012->Write();
  inclb_OC2KmuSS_CorrMass_MC_2012->Write();
  inclb_OC2Kplusmu_CorrMass_MC_2012->Write();
  inclb_OC2Kmu_CorrMass_MC_2012->Write();
  Bs2Dsmunu_CorrMass_MC_2012->Write();
  Bd_pimunu_CorrMass_MC_2012->Write();
  Bd_rhomunu_CorrMass_MC_2012->Write();
  Bd_Dstmunu_CorrMass_MC_2012->Write();
  Bu_D0munu_CorrMass_MC_2012->Write();
  Lb_pmunu_CorrMass_MC_2012->Write();
  Bs_D0DsK_CorrMass_MC_2012->Write();
  Bd_Dpi_CorrMass_MC_2012->Write();
  Bu_rhomunu_CorrMass_MC_2012->Write();
  Bu_D0pi_CorrMass_MC_2012->Write();    
  Bd_Dst0pi_D0gamma_CorrMass_MC_2012->Write();
  Bd_Dst0pi_D0pi0_CorrMass_MC_2012->Write();  
  Bd_D0rho_CorrMass_MC_2012->Write();*/
  //Histos_f->Close();
  
  //TFile *Histos_f2 = new TFile("HistosQ2_allMCfiles_newBDToptimization.root", "RECREATE");
  // TFile *Histos_f2 = new TFile("HistosQ2_datafile_newBDToptimization.root", "RECREATE");
  //Data_Q2_OS_2012->Write();
  /*Signal_Q2_MC_2012->Write();
  Bu2JpsiKplus_Q2_MC_2012->Write();
  Bu2JpsiKstar_Q2_MC_2012->Write();
  Bs_Jpsiphi_Q2_MC_2012->Write();
  inclb_ALL_Q2_MC_2012->Write();
  Bs_Kstmunu_Q2_MC_2012->Write();
  Bs_K2stmunu_Q2_MC_2012->Write();
  Bd_JpsiKst_Q2_MC_2012->Write();
  Bs_K1430munu_Q2_MC_2012->Write();
  //inclb_OC2KmuSS_Q2_MC_2012->Write();
  //inclb_OC2Kplusmu_Q2_MC_2012->Write();
  //inclb_OC2Kmu_Q2_MC_2012->Write();
  Bs2Dsmunu_Q2_MC_2012->Write();
  Bd_pimunu_Q2_MC_2012->Write();
  Bd_rhomunu_Q2_MC_2012->Write();
  Bd_Dstmunu_Q2_MC_2012->Write();
  Bu_D0munu_Q2_MC_2012->Write();
  Lb_pmunu_Q2_MC_2012->Write();
  Bs_D0DsK_Q2_MC_2012->Write();
  Bd_Dpi_Q2_MC_2012->Write();
  Bu_rhomunu_Q2_MC_2012->Write();
  Bu_D0pi_Q2_MC_2012->Write();    
  Bd_Dst0pi_D0gamma_Q2_MC_2012->Write();
  Bd_Dst0pi_D0pi0_Q2_MC_2012->Write();  
  Bd_D0rho_Q2_MC_2012->Write();  */
  

  //Histos_f2->Close();
   
  
  // ================================================================
  // double signal_MCnorm =  1./signal->Integral();
  
  TFile* f_hist = TFile::Open("Histos_datafile_newBDToptimization_Q2.root", "READ");
  TH1F* h_data = (TH1F*)f_hist->Get("OS_2012_Bs_MCORR")->Clone();
  double n_data = h_data->Integral();
  CheckNonZeroBin(h_data);

  // TFile q("root://eoslhcb.cern.ch///eos/lhcb/wg/semileptonic/Bs2KmunuAna/Tuples/SignalFit_input/Histos_allMCfiles_newBDToptimization.root");
  TFile q("Histos_allMCfiles_newBDToptimization_Q2cut.root");//Histos_allMCfiles_newBDToptimization.root");
  TH1 *htemp;
  TString mchistos[15]={"inclb_ALL","SignalMC","Bu2JpsiKplusMC", "Bs_Kstmunu","Bu2JpsiKstarMC", "Bs2DsmunuMC", "Bd_pimunu", "Bd_JpsiKst", "Bd_rhomunu", "Bd_Dstmunu", "Bu_D0munu", "Bs_Jpsiphi", "Bs_K2stmunu", "Bs_K1430munu","Lb_pmunu"};//, "Bd_Dmunu"};
  double mcN_inclb_ALL,mcN_SignalMC,mcN_Bu2JpsiKplusMC, mcN_Bs_Kstmunu, mcN_Bu2JpsiKstarMC, mcN_Bs2DsmunuMC, mcN_Bd_pimunu, mcN_Bd_JpsiKst, mcN_Bd_rhomunu, mcN_Bd_Dstmunu, mcN_Bu_D0munu, mcN_Bs_Jpsiphi, mcN_Bs_K2stmunu, mcN_Bs_K1430munu, mcN_Lb_pmunu, mcN_Bd_Dmunu;
  double *mcnorms[15]={&mcN_inclb_ALL, &mcN_SignalMC, &mcN_Bu2JpsiKplusMC, &mcN_Bs_Kstmunu, &mcN_Bu2JpsiKstarMC, &mcN_Bs2DsmunuMC, &mcN_Bd_pimunu, &mcN_Bd_JpsiKst, &mcN_Bd_rhomunu, &mcN_Bd_Dstmunu, &mcN_Bu_D0munu, &mcN_Bs_Jpsiphi, &mcN_Bs_K2stmunu, &mcN_Bs_K1430munu, &mcN_Lb_pmunu};//, &mcN_Bd_Dmunu};
  for(int i =0; i < 15; i++){
    cout<<"Getting:"<< mchistos[i]+"_Bs_MCORR"<<endl;
    q.GetObject(mchistos[i]+"_Bs_MCORR",htemp);
    assert(htemp!=NULL);
    //CheckNonZeroBin(htemp);
    //htemp->Sumw2();
    *(mcnorms[i])=1./htemp->Integral();
    cout << "mcN_"+mchistos[i]+" = " << 1./ *(mcnorms[i]) << endl;
    
  }

  TFile* f_MChist = TFile::Open("Histos_allMCfiles_newBDToptimization_Q2cut.root", "READ");
  TH1F* h_MCinclumuK = (TH1F*)f_MChist->Get("inclb_ALL_Bs_MCORR")->Clone();
  CheckNonZeroBin(h_MCinclumuK);
  h_MCinclumuK->Sumw2();
  //SetBinErrorhisto(h_MCinclumuK);
  TH1F* h_MCsignal = (TH1F*)f_MChist->Get("SignalMC_Bs_MCORR")->Clone();
  CheckNonZeroBin(h_MCsignal);
  h_MCsignal->Sumw2();
  //SetBinErrorhisto(h_MCsignal);
  TH1F* h_MCjpsiK = (TH1F*)f_MChist->Get("Bu2JpsiKplusMC_Bs_MCORR")->Clone();
  CheckNonZeroBin(h_MCjpsiK);
  h_MCjpsiK->Sumw2();
  //SetBinErrorhisto(h_MCjpsiK);
  TH1F* h_MCKstmunu = (TH1F*)f_MChist->Get("Bs_Kstmunu_Bs_MCORR")->Clone();
  CheckNonZeroBin(h_MCKstmunu);
  h_MCKstmunu->Sumw2();
  //SetBinErrorhisto(h_MCjpsiK);
  TH1F* h_MCK2stmunu = (TH1F*)f_MChist->Get("Bs_K2stmunu_Bs_MCORR")->Clone();
  CheckNonZeroBin(h_MCK2stmunu);
  h_MCK2stmunu->Sumw2();
  TH1F* h_MCK1430munu = (TH1F*)f_MChist->Get("Bs_K1430munu_Bs_MCORR")->Clone();
  CheckNonZeroBin(h_MCK1430munu);
  h_MCK1430munu->Sumw2();
  TH1F* h_MCJpsiphi = (TH1F*)f_MChist->Get("Bs_Jpsiphi_Bs_MCORR")->Clone();
  CheckNonZeroBin(h_MCJpsiphi);
  h_MCJpsiphi->Sumw2();
  TH1F* h_MCD0munu = (TH1F*)f_MChist->Get( "Bu_D0munu_Bs_MCORR")->Clone();
  CheckNonZeroBin(h_MCD0munu);
  h_MCD0munu->Sumw2();
  TH1F* h_MCDsmunu = (TH1F*)f_MChist->Get( "Bs2DsmunuMC_Bs_MCORR")->Clone();
  CheckNonZeroBin(h_MCDsmunu);
  h_MCDsmunu->Sumw2();
  //TH1F* h_MCBdDmunu = (TH1F*)f_MChist->Get( "Bd_Dmunu_Bs_MCORR")->Clone();
  //CheckNonZeroBin(h_MCBdDmunu);
  //h_MCBdDmunu->Sumw2();
  TH1F* h_MCBdJpsiKst = (TH1F*)f_MChist->Get( "Bd_JpsiKst_Bs_MCORR")->Clone();
  CheckNonZeroBin( h_MCBdJpsiKst);
   h_MCBdJpsiKst->Sumw2();
  TH1F* h_MCLbpmunu = (TH1F*)f_MChist->Get( "Lb_pmunu_Bs_MCORR")->Clone();
  CheckNonZeroBin(h_MCLbpmunu);
  h_MCLbpmunu->Sumw2();
  TH1F* h_MCBdrhomunu = (TH1F*)f_MChist->Get( "Bd_rhomunu_Bs_MCORR")->Clone();
  CheckNonZeroBin(h_MCBdrhomunu);
  h_MCBdrhomunu->Sumw2();
  TH1F* h_MCBdpimunu = (TH1F*)f_MChist->Get( "Bd_pimunu_Bs_MCORR")->Clone();
  CheckNonZeroBin(h_MCBdpimunu);
  h_MCBdpimunu->Sumw2();
  
  RooStats::HistFactory::Measurement Bs2Kmunu_measurement("Bs2Kmunu_measurement", "Bs2Kmunu measurement");
  // Naming 
  //Bs2Kmunu_measurement.SetOutputFilePrefix("Res/Bs2Kmunu_measurement");
  Bs2Kmunu_measurement.SetExportOnly(true);//Tells histfactory to not run the fit
  Bs2Kmunu_measurement.SetPOI("BsKmunu_Yield");
  // following line that all histos are already scaled to our luminosity
  Bs2Kmunu_measurement.SetLumi(1.0);
  Bs2Kmunu_measurement.SetLumiRelErr(0.000010);
  
  // Define the  data 
  RooStats::HistFactory::Channel chan("channel");
  chan.SetStatErrorConfig(1e-5,"Pois");//Configure the use of bin-by-bin statistical uncertainties, Tell histfactory how small of template uncertainties are worth considering (always set this small)
  chan.SetData("OS_2012_Bs_MCORR", "Histos_datafile_newBDToptimization_Q2.root"); // This file should contain a histogram not a branch !!!
  //chan.SetData(Data_CorrMass_OS_2012);
  // now create "pdf" :
  // Signal
  RooStats::HistFactory::Sample signal("signal","SignalMC_Bs_MCORR", "Histos_allMCfiles_newBDToptimization_Q2cut.root");
  signal.ActivateStatError();//to consider this template in statistical uncertainties
  signal.SetNormalizeByTheory(kFALSE);
  double signal_n = 23000;
  //double mcN_signal = 1./SignalMC_Bs_MCORR->Integral();
  signal.AddNormFactor("mcNorm_SignalMC", mcN_SignalMC, 1e-9, 1.);
  signal.AddNormFactor("BsKmunu_Yield",23000 , 0 , n_data );
  //const BsKmunu_scalefactor = 1.08172;
  signal.AddNormFactor("BsKmunu_scalefactor", 0.924455, 0.924455, 0.924455);
  //signal.AddNormFactor(); 
  //signal.AddNormFactor("mcBsKmunu_Norm", 0.4 , 1e-9 , 1.);
  // signal.AddOverallSys("syst1",  0.95, 1.05);
  
  // BKG 1
  RooStats::HistFactory::Sample B2JpsiK("B2JpsiK","Bu2JpsiKplusMC_Bs_MCORR", "Histos_allMCfiles_newBDToptimization_Q2cut.root");
  //B2JpsiK.ActivateStatError();
  B2JpsiK.SetNormalizeByTheory(kFALSE);
  // double n_expected_B2JpsiK_Yield =  2 * bb_Xsec * f_Bu * BR_B2JpsiK * tot_eff_B2JpsiK ;
  // double n_expected_B2JpsiK_Yield = 6.377527853506962e-06 ;// 2 * (298+- 2+-36)* (0.4 +- ) * ( 1.026+- 0.031)* 1e-3 * (0.16714)*0.055 *( (9563.+9046)/(7076879.+6784500.) ) ;
  double Bu_n = 3400 ;
  B2JpsiK.AddNormFactor("mcNorm_Bu2JpsiKplusMC" , mcN_Bu2JpsiKplusMC , 1e-9, 1.);
   B2JpsiK.AddNormFactor("B2JpsiK_Yield" , 3400. , 3400. , 3400. );
   B2JpsiK.AddNormFactor("B2JpsiK_scalefactor", 0.28411,  0.28411, 0.28411);
   //const B2JpsiK_scalefactor=3.51977;
   //B2JpsiK.AddNormFactor(B2JpsiK_scalefactor);
  //
  //B2JpsiK.AddNormFactor("B2JpsiK_Yield" , Bu_n , 0. ,  n_data );
    // B2JpsiK.AddNormFactor("mcB2JpsiK_Norm", 0.5 , 1e-9 , 1.);
  //B2JpsiK.ActivateStatError("background1_statUncert", "data/example.root"); //  stat uncert. on BKG comes from MC  if the desired  errors are differnet form those stored in TH1 
  //B2JpsiK.AddOverallSys("syst2", 0.95, 1.05 );
  
  // BKG 
  RooStats::HistFactory::Sample Incl_Kmu("Incl_Kmu" , "inclb_ALL_Bs_MCORR" , "Histos_allMCfiles_newBDToptimization_Q2cut.root");
  //Incl_Kmu.SetHisto(inclb_ALL_CorrMass_MC_2012);
  //Incl_Kmu.SetNormalizeByTheory(kFALSE);
  Incl_Kmu.ActivateStatError();
  Incl_Kmu.AddNormFactor( "mcNorm_inclb_ALL", mcN_inclb_ALL, 1e-9, 1.);
  Incl_Kmu.AddNormFactor( "incl_kmu_Yield" , n_data - (Bu_n+signal_n) , 10. , n_data );
    //Incl_Kmu.AddNormFactor("mcincl_kmu_Norm", 0.7 , 1e-9 , 1.);
    //Incl_Kmu.AddOverallSys("syst3", 0.95, 1.05 );

  // BKG 2
  RooStats::HistFactory::Sample BsKstmunu("BsKstmunu","Bs_Kstmunu_Bs_MCORR", "Histos_allMCfiles_newBDToptimization_Q2cut.root");
  //BsKstmunu.ActivateStatError();
  BsKstmunu.SetNormalizeByTheory(kFALSE);
  double BsKstmunu_n = 10400;//11800;
  BsKstmunu.AddNormFactor("mcNorm_Bs_Kstmunu" ,mcN_Bs_Kstmunu, 1e-9, 1.);
  BsKstmunu.AddNormFactor("BsKstmunu_Yield" , BsKstmunu_n , BsKstmunu_n , BsKstmunu_n );
  BsKstmunu.AddNormFactor("BsKstmunu_scalefactor", 0.41085,  0.41085,  0.41085);

// BKG 3
  RooStats::HistFactory::Sample BsK2stmunu("BsK2stmunu","Bs_K2stmunu_Bs_MCORR", "Histos_allMCfiles_newBDToptimization_Q2cut.root");
  //BsKstmunu.ActivateStatError();
  BsK2stmunu.SetNormalizeByTheory(kFALSE);
  double BsK2stmunu_n = 1700;//2700;
  BsK2stmunu.AddNormFactor("mcNorm_Bs_K2stmunu", mcN_Bs_K2stmunu , 1e-9, 1.);
  BsK2stmunu.AddNormFactor("BsK2stmunu_Yield" , BsK2stmunu_n , BsK2stmunu_n , BsK2stmunu_n );	
  BsK2stmunu.AddNormFactor("BsK2stmunu_scalefactor", 0.09835,  0.09835, 0.09835);

// BKG 3
  RooStats::HistFactory::Sample BsK1430munu("BsK1430munu","Bs_K1430munu_Bs_MCORR", "Histos_allMCfiles_newBDToptimization_Q2cut.root");
  //BsKstmunu.ActivateStatError();
  BsK1430munu.SetNormalizeByTheory(kFALSE);
  double BsK1430munu_n = 3400;
  BsK1430munu.AddNormFactor("mcNorm_Bs_K1430munu" , mcN_Bs_K1430munu , 1e-9, 1.);
  BsK1430munu.AddNormFactor("BsK1430munu_Yield" , BsK1430munu_n , BsK1430munu_n , BsK1430munu_n );
  BsK1430munu.AddNormFactor("BsK1430munu_scalefactor", 0.19249,  0.19249, 0.19249);	

// BKG 2
  RooStats::HistFactory::Sample BsDsmunu("BsDsmunu","Bs2DsmunuMC_Bs_MCORR", "Histos_allMCfiles_newBDToptimization_Q2cut.root");
  //BsKstmunu.ActivateStatError();
  BsDsmunu.SetNormalizeByTheory(kFALSE);
  double BsDsmunu_n = 58600;
  BsDsmunu.AddNormFactor("mcNorm_Bs2DsmunuMC" , mcN_Bs2DsmunuMC, 1e-9, 1.);
  BsDsmunu.AddNormFactor("BsDsmunu_Yield" , BsDsmunu_n , BsDsmunu_n , BsDsmunu_n );
  BsDsmunu.AddNormFactor("BsDsmunu_scalefactor", 223.96758, 223.96758, 223.96758);

// BKG 2
  RooStats::HistFactory::Sample BuD0munu("BuD0munu","Bu_D0munu_Bs_MCORR", "Histos_allMCfiles_newBDToptimization_Q2cut.root");
  //BsKstmunu.ActivateStatError();
  BuD0munu.SetNormalizeByTheory(kFALSE);
  double BuD0munu_n = 104400;
  BuD0munu.AddNormFactor("mcNorm_Bu_D0munu" , mcN_Bu_D0munu, 1e-9, 1.);
  BuD0munu.AddNormFactor("BuD0munu_Yield" , BuD0munu_n , BuD0munu_n , BuD0munu_n );
  BuD0munu.AddNormFactor("BuD0munu_scalefactor", 13.11647, 13.11647, 13.11647);

// BKG 2
  RooStats::HistFactory::Sample BsJpsiphi("BsJpsiphi","Bs_Jpsiphi_Bs_MCORR", "Histos_allMCfiles_newBDToptimization_Q2cut.root");
  //BsKstmunu.ActivateStatError();
  BsJpsiphi.SetNormalizeByTheory(kFALSE);
  double BsJpsiphi_n = 12;
  BsJpsiphi.AddNormFactor("mcNorm_Bs_Jpsiphi" , mcN_Bs_Jpsiphi, 1e-9, 1.);
  BsJpsiphi.AddNormFactor("BsJpsiphi_Yield" , BsJpsiphi_n , BsJpsiphi_n , BsJpsiphi_n );
  BsJpsiphi.AddNormFactor("BsJpsiphi_scalefactor", 4.22806e-3,  4.22806e-3, 4.22806e-3);

  // BKG 2
  RooStats::HistFactory::Sample Lbpmunu("Lbpmunu","Lb_pmunu_Bs_MCORR", "Histos_allMCfiles_newBDToptimization_Q2cut.root");
  //BsKstmunu.ActivateStatError();
  Lbpmunu.SetNormalizeByTheory(kFALSE);
  double Lb_pmunu_n = 370;//495;
  Lbpmunu.AddNormFactor("mcNorm_Lb_pmunu" , mcN_Lb_pmunu, 1e-9, 1.);
  Lbpmunu.AddNormFactor("Lbpmunu_Yield" , Lb_pmunu_n , Lb_pmunu_n , Lb_pmunu_n );
  Lbpmunu.AddNormFactor("Lbpmunu_scalefactor", 10.52521,  10.52521, 10.52521);

  // BKG 2
  RooStats::HistFactory::Sample BdJpsiKst("BdJpsiKst","Bd_JpsiKst_Bs_MCORR", "Histos_allMCfiles_newBDToptimization_Q2cut.root");
  //BsKstmunu.ActivateStatError();
  BdJpsiKst.SetNormalizeByTheory(kFALSE);
  double Bd_JpsiKst_n = 2200;
  BdJpsiKst.AddNormFactor("mcNorm_Bd_JpsiKst" , mcN_Bd_JpsiKst, 1e-9, 1.);
  BdJpsiKst.AddNormFactor("BdJpsiKst_Yield" , Bd_JpsiKst_n , Bd_JpsiKst_n , Bd_JpsiKst_n );
  BdJpsiKst.AddNormFactor("BdJpsiKst_scalefactor", 2.44487,   2.44487,  2.44487);

  // BKG 2
  /*RooStats::HistFactory::Sample BdDmunu("BdDmunu","Bd_Dmunu_Bs_MCORR", "root://eoslhcb.cern.ch///eos/lhcb/wg/semileptonic/Bs2KmunuAna/Tuples/SignalFit_input/Histos_allMCfiles_newBDToptimization.root");
  //BsKstmunu.ActivateStatError();
  BdDmunu.SetNormalizeByTheory(kFALSE);
  double Bd_Dmunu_n = 5050;
  BdDmunu.AddNormFactor("mcNorm_Bd_Dmunu" , mcN_Bd_Dmunu, 1e-9, 1.);
  BdDmunu.AddNormFactor("Bd_Dmunu_Yield" , Bd_Dmunu_n , Bd_Dmunu_n , Bd_Dmunu_n );
  BdDmunu.AddNormFactor("Bd_Dmunu_scalefactor", 13.6110, 13.6110, 13.6110 );*/
  
  // BKG 2
  RooStats::HistFactory::Sample Bdrhomunu("Bdrhomunu","Bd_rhomunu_Bs_MCORR", "Histos_allMCfiles_newBDToptimization_Q2cut.root");
  //BsKstmunu.ActivateStatError();
  Bdrhomunu.SetNormalizeByTheory(kFALSE);
  double Bd_rhomunu_n = 440;//538;
  Bdrhomunu.AddNormFactor("mcNorm_Bd_rhomunu" , mcN_Bd_rhomunu, 1e-9, 1.);
  Bdrhomunu.AddNormFactor("Bd_rhomunu_Yield" , Bd_rhomunu_n , Bd_rhomunu_n , Bd_rhomunu_n );
  Bdrhomunu.AddNormFactor("Bd_rhomunu_scalefactor", 4.65322, 4.65322, 4.65322 );

  // BKG 2
  RooStats::HistFactory::Sample Bdpimunu("Bdpimunu","Bd_pimunu_Bs_MCORR", "Histos_allMCfiles_newBDToptimization_Q2cut.root");
  //BsKstmunu.ActivateStatError();
  Bdpimunu.SetNormalizeByTheory(kFALSE);
  double Bd_pimunu_n = 480;//1120;
  Bdpimunu.AddNormFactor("mcNorm_Bd_pimunu" , mcN_Bd_pimunu, 1e-9, 1.);
  Bdpimunu.AddNormFactor("Bd_pimunu_Yield" , Bd_pimunu_n , Bd_pimunu_n , Bd_pimunu_n );
  Bdpimunu.AddNormFactor("Bd_pimunu_scalefactor", 2.6857, 2.6857, 2.6857 );


  // Add all that to an "NLL"-similar object
  chan.AddSample(B2JpsiK);     
  chan.AddSample(Incl_Kmu);
  chan.AddSample(signal);
  //chan.AddSample(BuD0munu);
  chan.AddSample(BsKstmunu);
  chan.AddSample(BsK2stmunu);
  chan.AddSample(BsK1430munu);
  //chan.AddSample(BsDsmunu);
  //chan.AddSample(BuD0munu);
  chan.AddSample(BsJpsiphi);
  //chan.AddSample(Lbpmunu);
  chan.AddSample(BdJpsiKst);
  //chan.AddSample(BdDmunu);
  //chan.AddSample(Bdrhomunu);
  //chan.AddSample(Bdpimunu);
  
  
  

  // Append all that to the measure :
  Bs2Kmunu_measurement.AddChannel(chan);
  Bs2Kmunu_measurement.CollectHistograms();
  Bs2Kmunu_measurement.PrintTree();
  
  RooWorkspace* ws = RooStats::HistFactory::MakeModelAndMeasurementFast(Bs2Kmunu_measurement);
  ModelConfig* Mconfig = (ModelConfig*) ws->obj("ModelConfig");
  ((RooRealVar*)(Mconfig->GetNuisanceParameters()->find("Lumi")))->setConstant(true);//fix the Lumi to not have it also fitted
  //((RooRealVar*)(Mconfig->GetNuisanceParameters()->find("BsKmunu_scalefactor")))->setConstant(true);
  //((RooRealVar*)(Mconfig->GetNuisanceParameters()->find("B2JpsiK_scalefactor")))->setConstant(true);
  for(int i =0; i < 15; i++){
    if (((RooRealVar*)(Mconfig->GetNuisanceParameters()->find("mcNorm_"+mchistos[i])))!=NULL)
      {
	((RooRealVar*)(Mconfig->GetNuisanceParameters()->find("mcNorm_"+mchistos[i])))->setConstant(kTRUE);
	cout << "mcN_"+mchistos[i] + " = " << ((RooRealVar*)(Mconfig->GetNuisanceParameters()->find("mcNorm_"+mchistos[i])))->getVal() << endl;
      }
  }
  
  Bs2Kmunu_measurement.SaveAs("./myfile.root" ,"");
  
  //((RooRealVar*)(Mconfig->GetNuisanceParameters()->find("B2JpsiK_Yield")))->setConstant(true);
  //print pdfs before the fit
  cout<<"=============================================================="<<endl;
  cout<<"=============================================================="<<endl;
  RooSimultaneous* Total_pdf = (RooSimultaneous*) Mconfig->GetPdf();
  Total_pdf->Print();
  cout<<"=============================================================="<<endl;
  RooAbsData* data = (RooAbsData*) ws->data("obsData");
  data->Print();
  cout<<"=============================================================="<<endl;
  RooRealVar* poi = (RooRealVar*) Mconfig->GetParametersOfInterest()->createIterator()->Next();
  std::cout << "Param of Interest: " << poi->GetName() << std::endl;
  RooArgSet *obs = (RooArgSet*) Mconfig->GetObservables();
  RooCategory *idx = (RooCategory*) obs->find("channelCat");
  RooRealVar *Bs_corrMass  = (RooRealVar*) obs->find("obs_x_channel");
  Bs_corrMass->SetTitle("Bs Corr. mass");
  Bs_corrMass->setUnit("MeV^{2}");
  Bs_corrMass->Print("V");
  cout<<"=============================================================="<<endl;
  cout<<"=============================================================="<<endl;
  
  HistFactorySimultaneous* Total_pdf_sim = new HistFactorySimultaneous( *Total_pdf );//needed to use Beeston-Barlow
  RooAbsReal* nll =  Total_pdf_sim  ->createNLL( *data , Offset(kTRUE));//Add external constarints in the argument. 
  RooMinuit* minuit_nll = new RooMinuit( *nll ) ;
  minuit_nll->setStrategy(2);
  minuit_nll->setErrorLevel(0.5);
  minuit_nll->simplex();
  minuit_nll->hesse();
  minuit_nll->migrad();
  //minuit_nll->fit("smh");
  // minuit_nll->fit("mh");
  // minuit_nll->simplex();
  RooFitResult *res_save = minuit_nll->save();
  // Plotting RooFit style : 
  
  double B2JpsiK_yield    = ((RooRealVar*)(Mconfig->GetNuisanceParameters()->find("B2JpsiK_Yield")))->getVal();
  double B2JpsiK_scalefactor    = ((RooRealVar*)(Mconfig->GetNuisanceParameters()->find("B2JpsiK_scalefactor")))->getVal();
  cout<<"B2JpsiK_yield = " << B2JpsiK_yield <<endl;
  double Sig_yield   = poi->getVal();
  cout<<"Sig_yield = " << Sig_yield <<endl; 
  double BsKmunu_scalefactor = ((RooRealVar*)(Mconfig->GetNuisanceParameters()->find("BsKmunu_scalefactor")))->getVal();
  cout<<"BsKmunu_scalefactor = " << BsKmunu_scalefactor <<endl;
  double InclK_yield = ((RooRealVar*)(Mconfig->GetNuisanceParameters()->find("incl_kmu_Yield")))->getVal();
  //double InclK_scalefactor= ((RooRealVar*)(Mconfig->GetNuisanceParameters()->find("incl_kmu_scalefactor")))->getVal();
  cout<<"InclK_yield = " << InclK_yield <<endl; 
  /*double BsKstmunu_yield = ((RooRealVar*)(Mconfig->GetNuisanceParameters()->find("BsKstmunu_Yield")))->getVal();
  double BsKstmunu_scalefactor = ((RooRealVar*)(Mconfig->GetNuisanceParameters()->find("BsKstmunu_scalefactor")))->getVal();
  cout<<"BsKstmunu_yield = " << BsKstmunu_yield <<endl; 
  double BsK2stmunu_yield = ((RooRealVar*)(Mconfig->GetNuisanceParameters()->find("BsK2stmunu_Yield")))->getVal();
  double BsK2stmunu_scalefactor = ((RooRealVar*)(Mconfig->GetNuisanceParameters()->find("BsK2stmunu_scalefactor")))->getVal();
  cout<<"BsK2stmunu_yield = " << BsK2stmunu_yield <<endl; 
  double BsK1430munu_yield = ((RooRealVar*)(Mconfig->GetNuisanceParameters()->find("BsK1430munu_Yield")))->getVal();
  double BsK1430munu_scalefactor = ((RooRealVar*)(Mconfig->GetNuisanceParameters()->find("BsK1430munu_scalefactor")))->getVal();
  cout<<"BsK1430munu_yield = " << BsK1430munu_yield <<endl; 
  double BsDsmunu_yield = ((RooRealVar*)(Mconfig->GetNuisanceParameters()->find("BsDsmunu_Yield")))->getVal();
  double BsDsmunu_scalefactor = ((RooRealVar*)(Mconfig->GetNuisanceParameters()->find("BsDsmunu_scalefactor")))->getVal();
  cout<<"BsDsmunu_yield = " << BsDsmunu_yield <<endl; 
   double BuD0munu_yield = ((RooRealVar*)(Mconfig->GetNuisanceParameters()->find("BuD0munu_Yield")))->getVal();
  double BuD0munu_scalefactor = ((RooRealVar*)(Mconfig->GetNuisanceParameters()->find("BuD0munu_scalefactor")))->getVal();
  cout<<"BuD0munu_yield = " << BuD0munu_yield <<endl; */
  
  /*double BsJpsiphi_yield = ((RooRealVar*)(Mconfig->GetNuisanceParameters()->find("BsJpsiphi_Yield")))->getVal();
  double BsJpsiphi_scalefactor = ((RooRealVar*)(Mconfig->GetNuisanceParameters()->find("BsJpsiphi_scalefactor")))->getVal();
  cout<<"BsJpsiphi_yield = " << BsJpsiphi_yield <<endl;
  double BdJpsiKst_yield = ((RooRealVar*)(Mconfig->GetNuisanceParameters()->find("BdJpsiKst_Yield")))->getVal();
  double BdJpsiKst_scalefactor = ((RooRealVar*)(Mconfig->GetNuisanceParameters()->find("BdJpsiKst_scalefactor")))->getVal();
  cout<<"BdJpsiKst_yield = " << BdJpsiKst_yield <<endl;
  double BdDmunu_yield = ((RooRealVar*)(Mconfig->GetNuisanceParameters()->find("BdDmunu_Yield")))->getVal();
  double BdDmunu_scalefactor = ((RooRealVar*)(Mconfig->GetNuisanceParameters()->find("BdDmunu_scalefactor")))->getVal();
  cout<<"BdDmunu_yield = " << BdDmunu_yield <<endl;
  double Lbpmunu_yield = ((RooRealVar*)(Mconfig->GetNuisanceParameters()->find("Lbpmunu_Yield")))->getVal();
  double Lbpmunu_scalefactor = ((RooRealVar*)(Mconfig->GetNuisanceParameters()->find("Lbpmunu_scalefactor")))->getVal();
  cout<<"Lbpmunu_yield = " << Lbpmunu_yield <<endl;*/
 

  //double Scale_F = (n_data/(Sig_yield*BsKmunu_scalefactor+B2JpsiK_yield*B2JpsiK_scalefactor+InclK_yield+BsKstmunu_yield*BsKstmunu_scalefactor+BsK2stmunu_yield*BsK2stmunu_scalefactor+BsK1430munu_yield*BsK1430munu_scalefactor+BsJpsiphi_yield*BsJpsiphi_scalefactor ));
  //double Scale_F = (n_data/(Sig_yield*BsKmunu_scalefactor+B2JpsiK_yield*B2JpsiK_scalefactor+InclK_yield+BsKstmunu_yield*BsKstmunu_scalefactor+BsK2stmunu_yield*BsK2stmunu_scalefactor+BsK1430munu_yield*BsK1430munu_scalefactor+BsJpsiphi_yield*BsJpsiphi_scalefactor+ BsJpsiphi_yield*BsJpsiphi_scalefactor+BdJpsiKst_yield*BdJpsiKst_scalefactor ));
  //double Scale_F = (n_data/(Sig_yield*BsKmunu_scalefactor+B2JpsiK_yield*B2JpsiK_scalefactor+InclK_yield ));
  //cout<<"Scale_F "<< Scale_F<<endl;
     
  RooPlot *CorrMass_frame = Bs_corrMass->frame( Title(" ") );
  data->plotOn(CorrMass_frame , DataError(RooAbsData::Poisson) , MarkerSize(0.7),DrawOption("ZP") , Cut("channelCat==0") ); //
  
  /*Total_pdf_sim->plotOn(CorrMass_frame  , Slice(*idx) , ProjWData(*idx,*data) , Components("*B2JpsiK*,*signal*,*BsKstmunu*,*BsK2stmunu*,*BsK1430munu*,*BsJpsiphi*,*Incl_Kmu*") , LineColor( kRed ) , DrawOption("F") , FillColor( kRed+2 ) , FillStyle(0),Normalization( Scale_F*(InclK_yield+BsJpsiphi_yield*BsJpsiphi_scalefactor+BsK1430munu_yield*BsK1430munu_scalefactor+BsK2stmunu_yield*BsK2stmunu_scalefactor+BsKstmunu_yield*BsKstmunu_scalefactor+Sig_yield*BsKmunu_scalefactor+B2JpsiK_yield * B2JpsiK_scalefactor)  , RooAbsReal::NumEvent)); // 
  Total_pdf_sim->plotOn(CorrMass_frame  , Slice(*idx) , ProjWData(*idx,*data) , Components("*B2JpsiK*,*signal*,*BsKstmunu*,*BsK2stmunu*,*BsK1430munu*,*BsJpsiphi*") , LineColor( kRed-4 ) ,  DrawOption("F") , FillColor( kRed-4 ) , FillStyle(0),Normalization(Scale_F* (BsJpsiphi_yield*BsJpsiphi_scalefactor+BsK1430munu_yield*BsK1430munu_scalefactor+BsK2stmunu_yield*BsK2stmunu_scalefactor+BsKstmunu_yield*BsKstmunu_scalefactor+Sig_yield*BsKmunu_scalefactor+B2JpsiK_yield * B2JpsiK_scalefactor) ,RooAbsReal::NumEvent)); //
  Total_pdf_sim->plotOn(CorrMass_frame  , Slice(*idx) , ProjWData(*idx,*data) , Components("*B2JpsiK*,*signal*,*BsKstmunu*,*BsK2stmunu*,*BsK1430munu*") ,  LineColor( kYellow ) ,  DrawOption("F") , FillColor( kYellow+1 ) , FillStyle(0),Normalization(Scale_F*  (BsK1430munu_yield*BsK1430munu_scalefactor+BsK2stmunu_yield*BsK2stmunu_scalefactor+BsKstmunu_yield*BsKstmunu_scalefactor+Sig_yield*BsKmunu_scalefactor+B2JpsiK_yield * B2JpsiK_scalefactor) ,RooAbsReal::NumEvent)); //
   Total_pdf_sim->plotOn(CorrMass_frame  , Slice(*idx) , ProjWData(*idx,*data) , Components("*B2JpsiK*,*signal*,*BsKstmunu*,*BsK2stmunu*") , LineColor( kMagenta ) ,  DrawOption("F") , FillColor( kMagenta+1 ) , FillStyle(0), Normalization( Scale_F* (BsK2stmunu_yield*BsK2stmunu_scalefactor+BsKstmunu_yield*BsKstmunu_scalefactor+Sig_yield*BsKmunu_scalefactor+B2JpsiK_yield * B2JpsiK_scalefactor) ,RooAbsReal::NumEvent)); //
   Total_pdf_sim->plotOn(CorrMass_frame  , Slice(*idx) , ProjWData(*idx,*data) , Components("*B2JpsiK*,*signal*,*BsKstmunu*") , LineColor( kViolet ) ,  DrawOption("F") , FillColor( kViolet+1 ) , FillStyle(0) ,Normalization( Scale_F* (BsKstmunu_yield*BsKstmunu_scalefactor+Sig_yield*BsKmunu_scalefactor+B2JpsiK_yield * B2JpsiK_scalefactor ) ,RooAbsReal::NumEvent)); //
   Total_pdf_sim->plotOn(CorrMass_frame , Slice(*idx),ProjWData(*idx,*data) , Components("*B2JpsiK*,*signal*") , DrawOption("F") , FillColor( kGreen+2 ) , LineColor( kGreen+2 ) , FillStyle(0),Normalization( Scale_F* (Sig_yield*BsKmunu_scalefactor+B2JpsiK_yield * B2JpsiK_scalefactor)  ,RooAbsReal::NumEvent) ); *///
  //Total_pdf_sim->plotOn(CorrMass_frame  , Slice(*idx) , ProjWData(*idx,*data) , Components("*Incl_Kmu*") , LineColor( kRed ) , DrawOption("F") , FillColor( kRed+2 ) , FillStyle(0), Normalization( Scale_F*(InclK_yield)  , RooAbsReal::NumEvent)); // 
  Total_pdf_sim->plotOn(CorrMass_frame  , Slice(*idx) , ProjWData(*idx,*data) , Components("*BsJpsiphi*") , LineColor( kRed-4 ) ,  DrawOption("F") , FillColor( kRed-4 ) , FillStyle(0),Range(2500, 5380));//Normalization((BsJpsiphi_yield*BsJpsiphi_scalefactor) ,RooAbsReal::NumEvent)); //
 Total_pdf_sim->plotOn(CorrMass_frame  , Slice(*idx) , ProjWData(*idx,*data) , Components("*BsK1430munu*") ,  LineColor( kYellow ) ,  DrawOption("F") , FillColor( kYellow+1 ) , FillStyle(0),Range(2500, 5380));//Normalization((BsK1430munu_yield*BsK1430munu_scalefactor) ,RooAbsReal::NumEvent)); //
 Total_pdf_sim->plotOn(CorrMass_frame  , Slice(*idx) , ProjWData(*idx,*data) , Components("*BsK2stmunu*") , LineColor( kMagenta ) ,  DrawOption("F") , FillColor( kMagenta+1 ) , FillStyle(0), Range(2500, 5380));//Normalization( (BsK2stmunu_yield*BsK2stmunu_scalefactor) ,RooAbsReal::NumEvent)); //
 Total_pdf_sim->plotOn(CorrMass_frame  , Slice(*idx) , ProjWData(*idx,*data) , Components("*BsKstmunu*") , LineColor( kViolet ) ,  DrawOption("F") , FillColor( kViolet+1 ) , FillStyle(0) ,Range(2500, 5380));//Normalization( Scale_F*(BsKstmunu_yield*BsKstmunu_scalefactor) ,RooAbsReal::NumEvent)); 
  Total_pdf_sim->plotOn(CorrMass_frame  , Slice(*idx) , ProjWData(*idx,*data) , Components("L_x_Incl_Kmu*") , LineColor( kRed ) , DrawOption("F") , FillColor( kRed+2 ) , FillStyle(0), Range(2500, 5380));
   Total_pdf_sim->plotOn(CorrMass_frame , Slice(*idx),ProjWData(*idx,*data) , Components("L_x_signal*") , DrawOption("F") , FillColor( kGreen+2 ) , LineColor( kGreen+2 ) , FillStyle(0), Range(2500, 5380));//,Normalization( Scale_F*(Sig_yield*BsKmunu_scalefactor)  ,RooAbsReal::NumEvent) );
  Total_pdf_sim->plotOn(CorrMass_frame , Slice(*idx),ProjWData(*idx,*data) , Components("L_x_B2JpsiK*") , DrawOption("F") , FillColor( kOrange+1 ) , LineColor( kOrange+3 ) ,  FillStyle(0), Range(2500, 5380));
  //Total_pdf_sim->plotOn(CorrMass_frame , Slice(*idx),ProjWData(*idx,*data) , Components("*signal*") , DrawOption("F") , FillColor( kGreen+2 ) , LineColor( kGreen+2 ) , FillStyle(0),Normalization( Scale_F*(Sig_yield*BsKmunu_scalefactor)  ,RooAbsReal::NumEvent) );
  //Total_pdf_sim->plotOn(CorrMass_frame , Slice(*idx),ProjWData(*idx,*data) , Components("*B2JpsiK*") , DrawOption("F") , FillColor( kOrange+1 ) , LineColor( kOrange+3 ) ,  FillStyle(0), Normalization( Scale_F*(B2JpsiK_yield * B2JpsiK_scalefactor)  , RooAbsReal::NumEvent) );
   //Total_pdf->plotOn(CorrMass_frame  , Slice(*idx) , ProjWData(*idx,*data) , Components("*BsDsmunu*") , LineColor( kCyan ) ,  DrawOption("F") , FillColor( kCyan+1 ) , FillStyle(0), Normalization( (BsDsmunu_yield*BsDsmunu_scalefactor)  ,RooAbsReal::NumEvent)); //
   //Total_pdf->plotOn(CorrMass_frame  , Slice(*idx) , ProjWData(*idx,*data) , Components("*BuD0munu*") , LineColor( kGreen ) ,  DrawOption("F") , FillColor( kGreen ) , FillStyle(0), Normalization(Scale_F* (BuD0munu_yield*BuD0munu_scalefactor)  ,RooAbsReal::NumEvent)); //  
  //CorrMass_frame->pullHist();
  //Total_pdf->plotOn(CorrMass_frame  , Slice(*idx) , ProjWData(*idx,*data) , Components("*Lbpmunu*") , LineColor( kBlue-3 ) ,  DrawOption("F") , FillColor( kBlue-3 ) , FillStyle(0), Normalization(Scale_F* (Lbpmunu_yield*Lbpmunu_scalefactor)  ,RooAbsReal::NumEvent)); //  
  Total_pdf->plotOn(CorrMass_frame  , Slice(*idx) , ProjWData(*idx,*data) , Components("*BdJpsiKst*") , LineColor( kGreen-3 ) ,  DrawOption("F") , FillColor( kGreen-3 ) , FillStyle(0), Range(2500, 5380));//Normalization(Scale_F* (BdJpsiKst_yield*BdJpsiKst_scalefactor)  ,RooAbsReal::NumEvent)); //  
   //Total_pdf->plotOn(CorrMass_frame  , Slice(*idx) , ProjWData(*idx,*data) , Components("*BdDmunu*") , LineColor( kYellow+3 ) ,  DrawOption("F") , FillColor( kYellow+3 ) , FillStyle(0), Normalization(Scale_F* (BdDmunu_yield*BdDmunu_scalefactor)  ,RooAbsReal::NumEvent)); //  
  //Total_pdf->plotOn(CorrMass_frame  , Slice(*idx) , ProjWData(*idx,*data) , Components("*Bdrhomunu*") , LineColor( kYellow+3 ) ,  DrawOption("F") , FillColor( kYellow+3 ) , FillStyle(0),  Range(2500, 5380));//Normalization(Scale_F* (BdDmunu_yield*BdDmunu_scalefactor)  ,RooAbsReal::NumEvent)); //  
  //Total_pdf->plotOn(CorrMass_frame  , Slice(*idx) , ProjWData(*idx,*data) , Components("*Bdpimunu*") , LineColor( kYellow+3 ) ,  DrawOption("F") , FillColor( kYellow+3 ) , FillStyle(0), Range(2500, 5380));//, Normalization(Scale_F* (BdDmunu_yield*BdDmunu_scalefactor)  ,RooAbsReal::NumEvent)); //  

  Total_pdf_sim->plotOn(CorrMass_frame  , Slice(*idx) , ProjWData(*idx,*data) , DrawOption("F") , FillColor( kBlue ) , LineColor( kBlue ) ,FillStyle(0));//,Normalization( Scale_F*(InclK_yield+Sig_yield*BsKmunu_scalefactor+B2JpsiK_yield * B2JpsiK_scalefactor) , RooAbsReal::NumEvent)) ;   
  //Total_pdf_sim->plotOn(CorrMass_frame  , Slice(*idx) , ProjWData(*idx,*data) , DrawOption("F") , FillColor( kBlue ) , LineColor( kBlue ) ,FillStyle(0),Normalization( (InclK_yield+BsJpsiphi_yield*BsJpsiphi_scalefactor+BsK1430munu_yield*BsK1430munu_scalefactor+BsK2stmunu_yield*BsK2stmunu_scalefactor+BsKstmunu_yield*BsKstmunu_scalefactor+Sig_yield*BsKmunu_scalefactor+B2JpsiK_yield * B2JpsiK_scalefactor+ BsDsmunu_yield*BsDsmunu_scalefactor+BuD0munu_yield*BuD0munu_scalefactor) , RooAbsReal::NumEvent)) ;   
  data->plotOn(CorrMass_frame , DataError(RooAbsData::Poisson) , MarkerSize(0.7),DrawOption("ZP") ,Cut("channelCat==0"));
   TCanvas *c11 = new TCanvas("RooFit","RooFit");
  CorrMass_frame->Draw();
 
  //c11->Print("Bs_corrmass_Roofit.pdf");
  //c11->Print("Bs_corrmass_Roofit.C");
  
  cout<<" n_data      = " << n_data      << endl;
  cout<<" Sig_yield   = " << Sig_yield   << endl; 
  cout<<" B2JpsiK_yield    = " << B2JpsiK_yield    << endl;
  cout<<" InclK_yield = " << InclK_yield << endl;
  //cout<<" BsKstmunu_yield = " << BsKstmunu_yield<< endl;
  //cout<<" Sig_yield + Bu_yield + InclK_yield +BsKstmunu_yield = " << n_data - Sig_yield - Bu_yield -BsKstmunu_yield <<endl;
  cout<<" Sig_yield + B2JpsiK_yield + InclK_yield = " << n_data - Sig_yield - B2JpsiK_yield <<endl;


   
  // Get histos after fit :
  // TH1 *Incl_Kmu_afterfit_h = Incl_Kmu.GetHisto();
  // cout<<"HIIIIIIII"<<endl;
  // Incl_Kmu_afterfit_h->Print();
  RooStats::HistFactory::HistFactoryNavigation HF_Nav( Mconfig);
    
  TH1F* Signal_histoafterfit   =(TH1F*)HF_Nav.GetSampleHist("channel" , "signal" ,"Signal_histoafterfit" );
  Signal_histoafterfit->Print();
  Signal_histoafterfit->Sumw2();
  
  TH1F* inclukmu_histoafterfit = (TH1F*)HF_Nav.GetSampleHist("channel" , "Incl_Kmu" , "inclukmu_histoafterfit");
  inclukmu_histoafterfit->Print();
  inclukmu_histoafterfit->Sumw2();
 
  //inclukmu_histoafterfit->Scale(1./inclukmu_histoafterfit ->Integral());
  //Signal_histoafterfit->Scale(1./Signal_histoafterfit ->Integral());

  TH1F* B2JpsiK_histoafterfit = (TH1F*)HF_Nav.GetSampleHist("channel" , "B2JpsiK" , "B2JpsiK_histoafterfit");
  B2JpsiK_histoafterfit->Print();
  B2JpsiK_histoafterfit->Sumw2();
  //B2JpsiK_histoafterfit->Scale(1./B2JpsiK_histoafterfit ->Integral());

  TH1F* BsKstmunu_histoafterfit = (TH1F*)HF_Nav.GetSampleHist("channel" , "BsKstmunu" , "BsKstmunu_histoafterfit");
  BsKstmunu_histoafterfit->Print();
  //BsKstmunu_histoafterfit->Sumw2();

  //SetBinErrorhisto(Signal_histoafterfit);
  //SetBinErrorhisto(inclukmu_histoafterfit);
  //SetBinErrorhisto(B2JpsiK_histoafterfit);
  TH1F* BsK2stmunu_histoafterfit = (TH1F*)HF_Nav.GetSampleHist("channel" , "BsK2stmunu" , "BsK2stmunu_histoafterfit");
  BsK2stmunu_histoafterfit->Print();
  //BsK2stmunu_histoafterfit->Sumw2();

  TH1F* BsK1430munu_histoafterfit = (TH1F*)HF_Nav.GetSampleHist("channel" , "BsK1430munu" , "BsK1430munu_histoafterfit");
  BsK1430munu_histoafterfit->Print();
  //BsK1430munu_histoafterfit->Sumw2();
  
  //TH1F* BsDsmunu_histoafterfit = (TH1F*)HF_Nav.GetSampleHist("channel" , "BsDsmunu" , "BsDsmunu_histoafterfit");
  //BsDsmunu_histoafterfit->Print();
//BsDsmunu_histoafterfit->Sumw2();
  
  

  TH1F* BsJpsiphi_histoafterfit= (TH1F*)HF_Nav.GetSampleHist("channel" , "BsJpsiphi" , "BsJpsiphi_histoafterfit");
  BsJpsiphi_histoafterfit->Print();
  // BsJpsiphi_histoafterfit->Sumw2();
  //TH1F* Lbpmunu_histoafterfit= (TH1F*)HF_Nav.GetSampleHist("channel" , "Lbpmunu" , "Lbpmunu_histoafterfit");
  //Lbpmunu_histoafterfit->Print();
  // Lbpmunu_histoafterfit->Sumw2();
  TH1F* BdJpsiKst_histoafterfit= (TH1F*)HF_Nav.GetSampleHist("channel" , "BdJpsiKst" , "BdJpsiKst_histoafterfit");
  BdJpsiKst_histoafterfit->Print();
  // BdJpsiKst_histoafterfit->Sumw2();
  //TH1F* BdDmunu_histoafterfit= (TH1F*)HF_Nav.GetSampleHist("channel" , "BdDmunu" , "BdDmunu_histoafterfit");
  //BdDmunu_histoafterfit->Print();
  // BdDmunu_histoafterfit->Sumw2();

  /*TH1F* Bdrhomunu_histoafterfit= (TH1F*)HF_Nav.GetSampleHist("channel" , "Bdrhomunu" , "Bdrhomunu_histoafterfit");
  Bdrhomunu_histoafterfit->Print();

  TH1F* Bdpimunu_histoafterfit= (TH1F*)HF_Nav.GetSampleHist("channel" , "Bdpimunu" , "Bdpimunu_histoafterfit");
  Bdpimunu_histoafterfit->Print();*/


  // TH1F* BuD0munu_histoafterfit = (TH1F*)HF_Nav.GetSampleHist("channel" , "BuD0munu" , "BuD0munu_histoafterfit");
  //BuD0munu_histoafterfit->Print();
  //BuD0munu_histoafterfit->Sumw2();

  TCanvas *c_Sigbeforeafter = new TCanvas("c_Sigbeforeafter", "c_Sigbeforeafter");
  h_MCsignal->SetLineColor(kBlack);
  h_MCsignal->Draw("");
  Signal_histoafterfit->Draw("samep");
 
  gPad->BuildLegend(0.75,0.75,0.95,0.95,"");
  
  //RooArgSet* model_comps = Total_pdf->getComponents() ;
  TCanvas *c_Inclkmubeforeafter = new TCanvas("c_Inclkmubbeforeafter", "c_Inclkmubbeforeafter");
  inclukmu_histoafterfit->Draw("");
  h_MCinclumuK->SetLineColor(kBlack);
  h_MCinclumuK->Draw("samep");
  gPad->BuildLegend(0.75,0.75,0.95,0.95,"");
  
  TCanvas *c_B2JpsiK_histoafterfit = new TCanvas("c_B2JpsiK_histoafterfit", "c_B2JpsiK_histoafterfit");
  h_MCjpsiK->Draw("");
  B2JpsiK_histoafterfit->Draw("sameep");
  gPad->BuildLegend(0.75,0.75,0.95,0.95,"");

  TCanvas *c_data = new TCanvas("c_data", "c_data");
  h_data->Draw("");
  
  TCanvas *c_BsKstmunu_histoafterfit = new TCanvas("c_BsKstmunu_histoafterfit", "c_BsKstmunu_histoafterfit");
  h_MCKstmunu->Draw("");
  BsKstmunu_histoafterfit->Draw("sameep");
  gPad->BuildLegend(0.75,0.75,0.95,0.95,"");

  TCanvas *c_BK2stmunu_histoafterfit = new TCanvas("c_BK2stmunu_histoafterfit", "c_BsK2stmunu_histoafterfit");
  h_MCK2stmunu->Draw("");
  BsK2stmunu_histoafterfit->SetLineColor(kMagenta+3);
  BsK2stmunu_histoafterfit->Draw("sameep");
  gPad->BuildLegend(0.75,0.75,0.95,0.95,"");

  TCanvas *c_BsK1430munu_histoafterfit = new TCanvas("c_BsK1430munu_histoafterfit", "c_BsK1430munu_histoafterfit");
  h_MCK1430munu->Draw("");
  BsK1430munu_histoafterfit->Draw("sameep");
 
  gPad->BuildLegend(0.75,0.75,0.95,0.95,"");

  /*TCanvas *c_BsDsmunu_histoafterfit = new TCanvas("c_BsDsmunu_histoafterfit","c_BsDsmunu_histoafterfit");
  BsDsmunu_histoafterfit->Draw("");
  h_MCDsmunu->Draw("sameep");
  
  gPad->BuildLegend(0.75,0.75,0.95,0.95,"");*/
  

  TCanvas *c_BsJpsiphi_histoafterfit = new TCanvas("c_BsJpsiphi_histoafterfit","c_BsJpsiphi_histoafterfit");
  h_MCJpsiphi->Draw("");
  BsJpsiphi_histoafterfit->Draw("sameep");
  gPad->BuildLegend(0.75,0.75,0.95,0.95,"");


  TCanvas *c_BdJpsiKst_histoafterfit = new TCanvas("c_BdJpsiKst_histoafterfit","c_BdJpsiKst_histoafterfit");
  BdJpsiKst_histoafterfit->Draw("");
  h_MCBdJpsiKst->Draw("sameep");
  
  gPad->BuildLegend(0.75,0.75,0.95,0.95,"");

  /* TCanvas *c_BdDmunu_histoafterfit = new TCanvas("c_BdDmunu_histoafterfit","c_BdDmunu_histoafterfit");
  h_MCBdDmunu->Draw("");
  BdDmunu_histoafterfit->Draw("sameep");
  gPad->BuildLegend(0.75,0.75,0.95,0.95,""); 

  TCanvas *c_Lbpmunu_histoafterfit = new TCanvas("c_Lbpmunu_histoafterfit","c_Lbpmunu_histoafterfit");
  Lbpmunu_histoafterfit->Draw("");
  h_MCLbpmunu->Draw("sameep");
  
  gPad->BuildLegend(0.75,0.75,0.95,0.95,"");

   TCanvas *c_BuD0munu_histoafterfit = new TCanvas("c_BuD0munu_histoafterfit","c_BuD0munu_histoafterfit");
  BuD0munu_histoafterfit->Draw("");
  h_MCD0munu->Draw("sameep");
  gPad->BuildLegend(0.75,0.75,0.95,0.95,"");*/

  /*TCanvas *c_Bdrhomunu_histoafterfit = new TCanvas("c_Bdrhomunu_histoafterfit","c_Bdrhomunu_histoafterfit");
  Bdrhomunu_histoafterfit->SetLineColor(kBlack);
  Bdrhomunu_histoafterfit->Draw("");
  h_MCBdrhomunu->Draw("sameep");
  gPad->BuildLegend(0.75,0.75,0.95,0.95,"");
  
  TCanvas *c_Bdpimunu_histoafterfit = new TCanvas("c_Bdpimunu_histoafterfit","c_Bdpimunu_histoafterfit");
  Bdpimunu_histoafterfit->Draw("");
  h_MCBdpimunu->Draw("sameep");
  gPad->BuildLegend(0.75,0.75,0.95,0.95,"");*/
  
  

  THStack *histostack = HF_Nav.GetChannelStack("channel","Stack_afterFit");
  TCanvas *c_Stack = new TCanvas("c_Stack", "c_Stack");
  //TH1F* h_stack = (TH1F*)h_data->Clone();
  //  h_data->Draw();
  histostack->Draw();
  h_data->Draw("SAME");
  histostack->GetYaxis()->SetTitle("Events / ( 48 MeV )");
  histostack->GetXaxis()->SetTitle("Bs Corr. mass (MeV)");
  gPad->BuildLegend(0.75,0.75,0.95,0.95,"");
  //c_Stack->Print("stack_newBDToptimization.C");
  //c_Stack->Print("stack_newBDToptimization.root");

   TCanvas *bla= new TCanvas("bla", "bla");
   THStack* stack = new THStack("Stack", "");
   // TH1F* h_stack = (TH1F*)h_data->Clone();
   //DecorateHisto(B2JpsiK_histoafterfit);
   B2JpsiK_histoafterfit->SetFillColor(kOrange+1);
    B2JpsiK_histoafterfit->SetLineColor(kOrange+1);
    stack->Add(B2JpsiK_histoafterfit);
    BsJpsiphi_histoafterfit->SetFillColor(kRed-4);
   BsJpsiphi_histoafterfit->SetLineColor(kRed-4);
   stack->Add(BsJpsiphi_histoafterfit);
   BdJpsiKst_histoafterfit->SetFillColor(kOrange);
   BdJpsiKst_histoafterfit->SetLineColor(kOrange);
   stack->Add(BdJpsiKst_histoafterfit);
   //DecorateHisto(Signal_histoafterfit);
   
   BsKstmunu_histoafterfit->SetFillColor(kViolet);
   BsKstmunu_histoafterfit->SetLineColor(kViolet);
   stack->Add(BsKstmunu_histoafterfit);
   BsK1430munu_histoafterfit->SetFillColor(kMagenta+2);
   BsK1430munu_histoafterfit->SetLineColor(kMagenta+2);
   stack->Add(BsK1430munu_histoafterfit); 
   BsK2stmunu_histoafterfit->SetFillColor(kMagenta);
   BsK2stmunu_histoafterfit->SetLineColor(kMagenta);
   stack->Add(BsK2stmunu_histoafterfit); 
   /*BsDsmunu_histoafterfit->SetFillColor(kCyan);
   BsDsmunu_histoafterfit->SetLineColor(kCyan);
   stack->Add(BsDsmunu_histoafterfit);*/
   
   
   //Lbpmunu_histoafterfit->SetFillColor(kBlue-3);
   //Lbpmunu_histoafterfit->SetLineColor(kBlue-3);
   //stack->Add(Lbpmunu_histoafterfit);
   
   /*BdDmunu_histoafterfit->SetFillColor(kYellow+3);
   BdDmunu_histoafterfit->SetLineColor(kYellow+3);
   stack->Add(BdDmunu_histoafterfit);*/
   //DecorateHisto(inclukmu_histoafterfit);
   /*Bdpimunu_histoafterfit->SetFillColor(28);
   Bdpimunu_histoafterfit->SetLineColor(28);
   stack->Add(Bdpimunu_histoafterfit);
   Bdrhomunu_histoafterfit->SetFillColor(kBlack);
   Bdrhomunu_histoafterfit->SetLineColor(kBlack);
   stack->Add(Bdrhomunu_histoafterfit);*/
   inclukmu_histoafterfit->SetFillColor(kRed+2);
   inclukmu_histoafterfit->SetLineColor(kRed+2);
   stack->Add(inclukmu_histoafterfit);
   Signal_histoafterfit->SetFillColor(kGreen+2);
   //Signal_histoafterfit->SetMarkerStyle(21);
   Signal_histoafterfit->SetLineColor(kGreen+2);
   stack->Add(Signal_histoafterfit);
   /*BuD0munu_histoafterfit->SetFillColor(kGreen);
   BuD0munu_histoafterfit->SetLineColor(kGreen);
   stack->Add(BuD0munu_histoafterfit);*/
   
   
   
   stack->Draw("hist");
   h_data->Draw("SAME");
   stack->GetYaxis()->SetTitle("Events / ( 48 MeV )");
   stack->GetXaxis()->SetTitle("Bs Corr. mass (MeV)");
   TLegend *leg = new TLegend(0.1,0.5,0.3,0.9);
   leg->AddEntry(Signal_histoafterfit,"BsKmunu","f");
   leg->AddEntry(inclukmu_histoafterfit,"inclukmu","f");
   //leg->AddEntry(Bdrhomunu_histoafterfit,"Bdrhomunu","f");
   //leg->AddEntry(Bdpimunu_histoafterfit,"Bdpimunu","f");
    leg->AddEntry(BsK2stmunu_histoafterfit,"BsK2stmunu","f");
    leg->AddEntry( BsK1430munu_histoafterfit,"BsK1430munu","f");
    leg->AddEntry(BsKstmunu_histoafterfit,"BsKstmunu","f");
    leg->AddEntry(BdJpsiKst_histoafterfit,"BdJpsiKst","f");
    leg->AddEntry(BsJpsiphi_histoafterfit,"BsJpsiphi","f");
   leg->AddEntry(B2JpsiK_histoafterfit,"B2JpsiK","f");
   leg->Draw();
   //gPad->BuildLegend(0.75,0.75,0.95,0.95,"");
   //bla->Print("stack2_.C");
   //bla->Print("stack2_newBDToptimization_test5.root");
   bla->Print("new_Q2cut_part.pdf");
   

  
  
  return 0;
}



