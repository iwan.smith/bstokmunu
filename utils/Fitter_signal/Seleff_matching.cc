#include <fstream>
#include <iostream>
#include <TROOT.h>
#include <TLatex.h>
#include <TFile.h>
#include <TTree.h>
#include <sstream>
#include <TChain.h>
#include <TCanvas.h>
#include <TObjArray.h>
#include <TBranch.h>
#include <TLorentzVector.h>
#include <iomanip>
#include <TH1F.h>
#include "/afs/cern.ch/user/b/bkhanji/roofitinclude.h" 

//nominator are BDT & trimming cuts applied
TString eosMC_dir = "root://eoslhcb.cern.ch//eos/lhcb/wg/semileptonic/Bs2KmunuAna/Tuples/Bs2KMuNu_MCTUPLES_TRIMMED_BDT_30May16/";

TString MC = "DTT_15512013_Lb_pmunu_DecProdCut_LCSR_*.root";
//insert here sample you want to look at
TString MC_BuJpsiK  = eosMC_dir + MC+ "/reducedTree";


//denominator are # of events passing the stripping
TString MC_BuJpsiK_EvtTuple  = "root://eoslhcb.cern.ch//eos/lhcb/wg/semileptonic/Bs2KmunuAna/Tuples/Bs2KMuNu_MCTUPLES_TRIMMED_BDT_30May16/"+ MC +"/reducedTree";

//TString MyCuts    = "TMVA_charge_BDT>0.107 && TMVA_SS_aftercut_BDT>-0.0954";
//TString MyCuts_MC = "TMVA_charge_BDT>0.107 && TMVA_SS_aftercut_BDT>-0.0954 && abs(Bs_TRUEID)==531 && abs(kaon_m_TRUEID)==321 && abs(muon_p_TRUEID)==13 && abs(muon_p_MC_MOTHER_ID)==531  && abs(kaon_m_MC_MOTHER_ID)==431 && abs(kaon_m_MC_GD_MOTHER_ID)==531 &&(kaon_m_MC_GD_MOTHER_KEY==muon_p_MC_MOTHER_KEY)";

TString MyCuts    = "TMVA_charge_BDT>0.107 && TMVA_SS_aftercut_BDT>0.1";
 TString MyCuts_MC = "TMVA_charge_BDT>0.107 && TMVA_SS_aftercut_BDT>0.1 && abs(Bs_TRUEID)==5122 && abs(kaon_m_TRUEID)==2212 && abs(muon_p_TRUEID)==13 && abs(muon_p_MC_MOTHER_ID)==5122 && abs(kaon_m_MC_MOTHER_ID)==5122  && (kaon_m_MC_MOTHER_KEY==muon_p_MC_MOTHER_KEY)";


//13774000 TString MyCuts_MC = "TMVA_charge_BDT>0.107 && TMVA_SS_aftercut_BDT>0.1 && abs(Bs_TRUEID)==531 && abs(kaon_m_TRUEID)==321 && abs(muon_p_TRUEID)==13 && abs(muon_p_MC_MOTHER_ID)==531  && abs(kaon_m_MC_MOTHER_ID)==431 && abs(kaon_m_MC_GD_MOTHER_ID)==531 &&(kaon_m_MC_GD_MOTHER_KEY==muon_p_MC_MOTHER_KEY)";

//13144001 TString MyCuts_MC = "TMVA_charge_BDT>0.107 && TMVA_SS_aftercut_BDT>0.1 && abs(Bs_TRUEID)==531 && abs(kaon_m_TRUEID)==321 && abs(muon_p_TRUEID)==13 && abs(muon_p_MC_MOTHER_ID)==443 && abs(muon_p_MC_GD_MOTHER_ID)==531  && abs(kaon_m_MC_MOTHER_ID)==333 && abs(kaon_m_MC_GD_MOTHER_ID)==531 &&(kaon_m_MC_GD_MOTHER_KEY==muon_p_MC_GD_MOTHER_KEY)";

//12143401 TString MyCuts_MC = "TMVA_charge_BDT>0.107 && TMVA_SS_aftercut_BDT>-0.0954 && abs(Bs_TRUEID)==521 && abs(kaon_m_TRUEID)==321 && abs(muon_p_TRUEID)==13 && abs(muon_p_MC_MOTHER_ID)==443 && abs(muon_p_MC_GD_MOTHER_ID)==521  && abs(kaon_m_MC_MOTHER_ID)==323 && abs(kaon_m_MC_GD_MOTHER_ID)==521 &&(kaon_m_MC_GD_MOTHER_KEY==muon_p_MC_GD_MOTHER_KEY)";

//12143001 TString MyCuts_MC = "TMVA_charge_BDT>0.107 && TMVA_SS_aftercut_BDT>-0.0954 && abs(Bs_TRUEID)==521 && abs(kaon_m_TRUEID)==321 && abs(muon_p_TRUEID)==13 && abs(muon_p_MC_MOTHER_ID)==443 && abs(muon_p_MC_GD_MOTHER_ID)==521  && abs(kaon_m_MC_MOTHER_ID)==521 && (kaon_m_MC_MOTHER_KEY==muon_p_MC_GD_MOTHER_KEY)";

//11874004 TString MyCuts_MC = "TMVA_charge_BDT>0.107 && TMVA_SS_aftercut_BDT>-0.0954 && abs(Bs_TRUEID)==511 && abs(kaon_m_TRUEID)==321 && abs(muon_p_TRUEID)==13 && abs(muon_p_MC_MOTHER_ID)==511 && abs(kaon_m_MC_MOTHER_ID)==421 && abs(kaon_m_MC_GD_MOTHER_ID)==413 &&  abs(kaon_m_MC_GD_GD_MOTHER_ID)==511 && (kaon_m_MC_GD_GD_MOTHER_KEY==muon_p_MC_MOTHER_KEY)";

//15512013 TString MyCuts_MC = "TMVA_charge_BDT>0.107 && TMVA_SS_aftercut_BDT>-0.0954 && abs(Bs_TRUEID)==5122 && abs(kaon_m_TRUEID)==2212 && abs(muon_p_TRUEID)==13 && abs(muon_p_MC_MOTHER_ID)==5122 && abs(kaon_m_MC_MOTHER_ID)==5122  && (kaon_m_MC_MOTHER_KEY==muon_p_MC_MOTHER_KEY)";

//12873002 TString MyCuts_MC = "TMVA_charge_BDT>0.107 && TMVA_SS_aftercut_BDT>-0.0954 && abs(Bs_TRUEID)==521 && abs(kaon_m_TRUEID)==321 && abs(muon_p_TRUEID)==13 && abs(muon_p_MC_MOTHER_ID)==521 && abs(kaon_m_MC_MOTHER_ID)==421";

//12513001 TString MyCuts_MC = "TMVA_charge_BDT>0.107 && TMVA_SS_aftercut_BDT>-0.0954 && abs(Bs_TRUEID)==521 && abs(kaon_m_TRUEID)==211 && abs(muon_p_TRUEID)==13 && abs(muon_p_MC_MOTHER_ID)==521 && abs(kaon_m_MC_MOTHER_ID)==113 &&  abs(kaon_m_MC_GD_MOTHER_ID)==521 && (kaon_m_MC_GD_MOTHER_KEY==muon_p_MC_MOTHER_KEY)";

//Signal TString MyCuts_MC = "TMVA_charge_BDT>0.107 && TMVA_SS_aftercut_BDT>-0.0954 && abs(Bs_TRUEID)==531 && abs(kaon_m_TRUEID)==321 && abs(muon_p_TRUEID)==13 && abs(muon_p_MC_MOTHER_ID)==531 && abs(kaon_m_MC_MOTHER_ID)==531 && (kaon_m_MC_MOTHER_KEY==muon_p_MC_MOTHER_KEY)";

//13512400 TString MyCuts_MC = "TMVA_charge_BDT>0.107 && TMVA_SS_aftercut_BDT>-0.0954 && abs(Bs_TRUEID)==531 && abs(kaon_m_TRUEID)==321 && abs(muon_p_TRUEID)==13 && abs(muon_p_MC_MOTHER_ID)==531 && abs(kaon_m_MC_MOTHER_ID)==323 && abs(kaon_m_MC_GD_MOTHER_ID)==531 && (kaon_m_MC_GD_MOTHER_KEY==muon_p_MC_MOTHER_KEY)";

//13512420 TString MyCuts_MC = "TMVA_charge_BDT>0.107 && TMVA_SS_aftercut_BDT>-0.0954 && abs(Bs_TRUEID)==531 && abs(kaon_m_TRUEID)==321 && abs(muon_p_TRUEID)==13 && abs(muon_p_MC_MOTHER_ID)==531 && abs(kaon_m_MC_MOTHER_ID)==10321 && abs(kaon_m_MC_GD_MOTHER_ID)==531 && (kaon_m_MC_GD_MOTHER_KEY==muon_p_MC_MOTHER_KEY)";

//11512400 TString MyCuts_MC = "TMVA_charge_BDT>0.107 && TMVA_SS_aftercut_BDT>-0.0954 && abs(Bs_TRUEID)==511 && abs(kaon_m_TRUEID)==211 && abs(muon_p_TRUEID)==13 && abs(muon_p_MC_MOTHER_ID)==511 && abs(kaon_m_MC_MOTHER_ID)==213 && abs(kaon_m_MC_GD_MOTHER_ID)==511 && (kaon_m_MC_GD_MOTHER_KEY==muon_p_MC_MOTHER_KEY)";

//13512410 TString MyCuts_MC = "TMVA_charge_BDT>0.107 && TMVA_SS_aftercut_BDT>-0.0954 && abs(Bs_TRUEID)==531 && abs(kaon_m_TRUEID)==321 && abs(muon_p_TRUEID)==13 && abs(muon_p_MC_MOTHER_ID)==531 && abs(kaon_m_MC_MOTHER_ID)==325 && abs(kaon_m_MC_GD_MOTHER_ID)==531 && (kaon_m_MC_GD_MOTHER_KEY==muon_p_MC_MOTHER_KEY)";

//11512011 TString MyCuts_MC = "TMVA_charge_BDT>0.107 && TMVA_SS_aftercut_BDT>-0.0954 && abs(Bs_TRUEID)==511 && abs(kaon_m_TRUEID)==211 && abs(muon_p_TRUEID)==13 && abs(muon_p_MC_MOTHER_ID)==511 && abs(kaon_m_MC_MOTHER_ID)==511 && (kaon_m_MC_MOTHER_KEY==muon_p_MC_MOTHER_KEY)";

//11144001 TString MyCuts_MC = "TMVA_charge_BDT>0.107 && TMVA_SS_aftercut_BDT>-0.0954 && abs(Bs_TRUEID)==511 && abs(kaon_m_TRUEID)==321 && abs(muon_p_TRUEID)==13 && abs(muon_p_MC_MOTHER_ID)==443 && abs(kaon_m_MC_MOTHER_ID)==313 && abs(muon_p_MC_GD_MOTHER_ID)==511 && abs(kaon_m_MC_GD_MOTHER_ID)==511 && (kaon_m_MC_GD_MOTHER_KEY==muon_p_MC_GD_MOTHER_KEY)";

//&& abs(Bs_TRUEID)==521 && abs(kaon_m_TRUEID)==321 && abs(muon_p_TRUEID)==13 && muon_p_MC_MOTHER_ID==443 && abs(kaon_m_MC_MOTHER_ID)==521 && abs(muon_p_MC_GD_MOTHER_ID)==521";//&& muon_p_NIsoTr_MC_MOTHER_ID==443 && abs(muon_p_NIsoTr_MC_GD_MOTHER_ID)==521";

/*int FromB(int i){ 
   int MID = i;
   if(i<0) MID = -i; 
   if(i%100==0) i=i+1;

   int n2= int(MID/1000.);
   MID=MID-n2*1000;
   int n1= int(MID/100.);
   if(n1==5) return 1;

   int BID=i;
   if(i<0) BID = -i; 
   n2= int(BID/10000.);
   BID=BID-n2*10000;
   n1= int(BID/1000.);
   if(n1==5) return 1;

   return 0;
   }*/

void DefineImportantVariables(TTree* mytree = 0 , bool MC= false )
{
  mytree->SetBranchStatus("*",0);
  mytree->SetBranchStatus("Bs_M",1);
  mytree->SetBranchStatus("Bs_MCORR",1);
  mytree->SetBranchStatus("Bs_P*",1);
  //mytree->SetBranchStatus("kaon_m_IsoMinBDT",1);
  //mytree->SetBranchStatus("kaon_m_*",1);
  //mytree->SetBranchStatus("kaon_m_PAIR_M",1);
  // mytree->SetBranchStatus("kaon_m_TRACK_Eta",1);
  //mytree->SetBranchStatus("muon_p_IsoMinBDT",1);
  //  mytree->SetBranchStatus("muon_p_*",1);
  //mytree->SetBranchStatus("muon_p_PAIR_M",1);
  //mytree->SetBranchStatus("muon_p_PIDK",1);
  // mytree->SetBranchStatus("kaon_m_PIDK",1);
  //mytree->SetBranchStatus("muon_p_PT",1);
  
  mytree->SetBranchStatus("kaon_m_P*",1); 
  mytree->SetBranchStatus("muon_p_NIsoTr_P*",1); 
  //mytree->SetBranchStatus("TMVA_*");
  mytree->SetBranchStatus("TMVA_*",1);
  mytree->SetBranchStatus("runNumber",1);
  mytree->SetBranchStatus("eventNumber",1);
  if (MC)
  {
    mytree->SetBranchStatus("*_MC_*_ID",1);
    mytree->SetBranchStatus("*_MC_*_KEY",1);
    mytree->SetBranchStatus("*_TRUEID",1);
  };
  
}

void arrangehistos( TH1D*h1 , TH1D*h2 , TString var_name = "" ,TCanvas *c = 0)
{
  //h1->Scale(1./h1->Integral());
  //h2->Scale(1./h2->Integral());
  double highest_reachof_h_1 = h1->GetMaximum() ;
  double highest_reachof_h_2 = h2->GetMaximum() ;
  
  double bin_h1_max = h1->GetBinCenter(h1->GetMaximumBin());
  double bin_h2_max = h2->GetBinCenter(h2->GetMaximumBin());
  
  highest_reachof_h_1  =+ h1->GetBinError(h1->GetMaximumBin());
  highest_reachof_h_2  =+ h2->GetBinError(h2->GetMaximumBin());
  double max_value     = max( highest_reachof_h_1 , highest_reachof_h_2 );
  h1->SetLineWidth(2) ; h2->SetLineWidth(2) ; 
  if( max_value == highest_reachof_h_1 )
  {
    h1->SetStats(0);
    h1->GetXaxis()->SetTitle(var_name);
    h1->Draw();
    h2->Draw("same");
    //c->BuildLegend( xlow_cor , ylow_cor , xhigh_cor , yhigh_cor  ) ;
  }
  else
  {
    h2->SetStats(0);
    h2->GetXaxis()->SetTitle(var_name);
    h2->Draw();
    h1->Draw("same");
    //c->BuildLegend( xlow_cor , ylow_cor , xhigh_cor , yhigh_cor  ) ;
  }
  gPad->Update();
  //c->SaveAs((TString)c->GetName() +".pdf");
}

TTree* PrepareTrees(TString OS_data = "", bool MC = false )
{
  
  TChain *OSdata_2012_tree = new TChain("data_OS","data_OS");
  OSdata_2012_tree->Add( OS_data);
  TString Cuts_t = MyCuts;
  
  if (MC) {
    DefineImportantVariables(OSdata_2012_tree , true); 
    Cuts_t = MyCuts_MC;
  }
  
  else DefineImportantVariables(OSdata_2012_tree );
  
  TTree *newOSdata_2012_tree = OSdata_2012_tree->CopyTree(Cuts_t,"", 1000000000 , 0);  
  
  return newOSdata_2012_tree;
}

void CompareVars( TTree *newOSdata_2012_tree=0  , TTree *newSSdata_2012_tree=0  ) {
  
  cout <<"Same Sign Entries: "<< newSSdata_2012_tree->GetEntries() << endl;
  cout <<"Opposite Sign Entries: "<< newOSdata_2012_tree->GetEntries() << endl;
  cout<<" 2012/2011 ~ " <<setprecision(2)<<(double) newSSdata_2012_tree->GetEntries()/newOSdata_2012_tree->GetEntries() << endl;
  
  // -- Get list of branches for the tree
  TObjArray *o_2012 = newSSdata_2012_tree->GetListOfBranches(); 
  TObjArray *o_2011 = newOSdata_2012_tree->GetListOfBranches(); 
  
  int m = o_2012->GetEntries(); 
  cout << "Number of branches: " << m << endl;
  
  TCanvas **c      = new TCanvas*[m];
  TCanvas **c_div  = new TCanvas*[m];
  TH1D** array_SS  = new TH1D*[m];
  TH1D** array_OS  = new TH1D*[m];
  TH1D** array_div = new TH1D*[m];
  
  for (int ttt=0;ttt<m;ttt++) {
    cout<<"Name of the Branch is : " <<((TBranch*)(*o_2012)[ttt])->GetName() <<endl;
    //cout<<"Minimum is : " << SSdata_2012_tree->GetMinimum(((TBranch*)(*o_2012)[ttt])->GetName())<<endl;
    //cout<<"Maximum is : " << SSdata_2012_tree->GetMaximum(((TBranch*)(*o_2012)[ttt])->GetName())<<endl;
    array_SS[ttt] = new TH1D(Form("SS_2012_%d",ttt),((TBranch*)(*o_2012)[ttt])->GetName(),100 ,
                             newSSdata_2012_tree->GetMinimum(((TBranch*)(*o_2012)[ttt])->GetName()) ,
                             newSSdata_2012_tree->GetMaximum(((TBranch*)(*o_2012)[ttt])->GetName()) );
    
    array_OS[ttt] = new TH1D(Form("OS_2012_%d",ttt),((TBranch*)(*o_2012)[ttt])->GetName(),100 ,
                             newSSdata_2012_tree->GetMinimum(((TBranch*)(*o_2012)[ttt])->GetName()) ,
                             newSSdata_2012_tree->GetMaximum(((TBranch*)(*o_2012)[ttt])->GetName()) );

  }
  // -- Loop over all, and draw their variables into TCanvas c1
  int cnt(0); 
  
  for (int i = 0; i < m; ++i) {
    newSSdata_2012_tree->Project(array_SS[i]->GetName(), ((TBranch*)(*o_2012)[i])->GetName(),  
                              MyCuts
                              ,"" , 10000000000 , 0 ) ; 
    //array_SS[i]->Scale(10);
    array_OS[i]->SetLineColor(kRed);
    newOSdata_2012_tree->Project(array_OS[i]->GetName(), ((TBranch*)(*o_2012)[i])->GetName(), 
                              MyCuts
                              ,"" , 100000000 , 0   ) ; 
    c[i]= new TCanvas( "c_"+ (TString)((TBranch*)(*o_2012)[i])->GetName() , 
                       "c_"+ (TString)((TBranch*)(*o_2012)[i])->GetName() ); //
    c[i]->cd() ;
    arrangehistos( array_OS[i]  , array_SS[i]  ,  (TString)((TBranch*)(*o_2012)[i])->GetName()  ,  c[i]  );
    c[i]->SaveAs((TString)c[i]->GetName()+".pdf");
    // array_OS[i]->DrawNormalized();
    // array_SS[i]->DrawNormalized("same");
    //c[i]->Update(); 

    c_div[i] = new TCanvas( Form("c_div%d", i ) , Form("c_div%d", i ) );
    array_OS[i]->Divide(array_SS[i] );
    c_div[i]->cd() ;
    array_OS[i]->Draw();
    //c_div[i]->Update();
  }
  return ;   

}

void GetSSCrossOS( TTree *newOSdata_2012_tree=0  , TTree *newSSdata_2012_tree =0  )
{
  
  UInt_t runN_OS ,runN_SS;
  ULong64_t EvtN_OS,EvtN_SS;
  
  newOSdata_2012_tree->SetBranchAddress("eventNumber"  , &EvtN_OS  );
  newOSdata_2012_tree->SetBranchAddress("runNumber"  , &runN_OS  );
  newSSdata_2012_tree->SetBranchAddress("eventNumber"  , &EvtN_SS  );
  newSSdata_2012_tree->SetBranchAddress("runNumber"  , &runN_SS  );
  
  TFile *Crossfile = new TFile("CrossOSandSS.root","recreate");
  TTree *Crosstree = newSSdata_2012_tree->CloneTree(0);
  
  Long64_t nentries_OS = newOSdata_2012_tree->GetEntries();
  Long64_t nentries_SS = newSSdata_2012_tree->GetEntries();
  int offset = 0;
  
  for (Long64_t i=0;i< nentries_OS;i++) 
  {
    newOSdata_2012_tree->GetEntry(i);
    for (Long64_t i_SS =0 ; i_SS< newSSdata_2012_tree->GetEntries() ; i_SS++) 
    {
      newSSdata_2012_tree->GetEntry(i_SS + offset);
      if (runN_OS == runN_SS && EvtN_OS == EvtN_SS)
      {
        cout<<"==============================="<<endl;
        cout<<" OS index : " << i <<endl;
        cout<<" SS index : " << i_SS <<endl;
        cout<<"  Cross event found ! "  << endl;
        cout<< " runN_OS = " << runN_OS << endl ;
        cout<< " runN_SS = " << runN_SS << endl ;
        cout<< " EvtN_OS = " << EvtN_OS << endl ;
        cout<< " EvtN_SS = " << EvtN_SS << endl ;
        cout<<"------------------------------"<<endl;
        Crosstree->Fill();
        offset = i_SS;
        
        break;
      }
    }
  }
  
  Crosstree->AutoSave();
  Crosstree->Print();
  Crosstree->Write();
  Crossfile->Close();
  
  cout<<"............................................................"<<endl;
  cout<<"............................................................"<<endl;
  cout<<" OS nentries    : "<< newOSdata_2012_tree->GetEntries() << endl;
  cout<<" SS nentries    : "<< newSSdata_2012_tree->GetEntries() << endl;
  cout<<" Cross nentries : "<< Crosstree          ->GetEntries() << endl;
  cout<<"------------------------------------------------------------"<<endl;
  cout<<"------------------------------------------------------------"<<endl;
    
  return ;
}

TH1D *GetInvMOfBplusBKG( TTree *newOSdata_2012_tree  )
{
  
  double pion_mass = 139.57;
  double kaon_mass = 493.67;
  
  /*
  TLorentzVector Mu_p(    muon_p_PX    ,     muon_p_PY    ,    muon_p_PZ     ,  muon_p_PE    );
  TLorentzVector Mu_m(muon_p_NIsoTr_PX , muon_p_NIsoTr_PY , muon_p_NIsoTr_PZ , muon_p_NIsoTr_PE );
  TLorentzVector K_m(kaon_m_NIsoTr_PX , kaon_m_NIsoTr_PY , kaon_m_NIsoTr_PZ , kaon_m_NIsoTr_PE );
  TLorentzVector Jpsi = Mu_p + Mu_m  ;
  TLorentzVector Bplus = Jpsi + K_m  ;
  double Bplus_mass = Bplus.M();
  */
  // calculate the invariant mass of the system Bs + best isolated track
  // InvM = sqrt( M_Bs^2 + M_NIso^2 + 2*(Pe_B*Pe_Niso - ( px_B*px_NIso + py_B*py_NIso + pz_B*pz_NIso ) ) )
  TString InvMass_str = "sqrt( Bs_M*Bs_M + 105.658*105.658 + 2*( Bs_PE*muon_p_NIsoTr_PE - ( Bs_PX*muon_p_NIsoTr_PX  + Bs_PY*muon_p_NIsoTr_PY  +  Bs_PZ*muon_p_NIsoTr_PZ  ) ) )";
  
  TH1D *InvMofBplus_h = new TH1D("InvMofBplus_h","InvMofBplus_h",100 , 5100 , 5450);
  
  /*
    // Make a data set to enable likelihood fit
    TFile *dataset_bplusmass = new TFile("dataset_bplusmass.root","RECREATE");
    RooDataSet *data = new RooDataSet("data","data",
    RooArgSet(*Dplusmass,*t ,*tag , *B_M ),
    Import(newOSdata_2012_tree)
    );
  */

  
  newOSdata_2012_tree->Project( InvMofBplus_h->GetName(), InvMass_str,
                                MyCuts
                                ,"" , 10000000000 , 0 );
  
  return InvMofBplus_h;
  
}

void Seleff_matching()
{
  
  // Analyze MC B+ - >JpsiK+ sample : 
  TTree *MC2012_tree      = PrepareTrees( MC_BuJpsiK , true);
  TH1D* bplusMass_MC_h    = GetInvMOfBplusBKG(MC2012_tree);
  TCanvas *c_BplusMass_MC = new TCanvas("c_BplusMass_MC","c_BplusMass_MC");
  c_BplusMass_MC->cd();
  bplusMass_MC_h->Draw();
  
  TTree *EvtTpl_tree      = PrepareTrees( MC_BuJpsiK_EvtTuple);
  //TChain *EvtTpl_tree = new TChain("EvtTpl_tree","EvtTpl_tree");
  //EvtTpl_tree->Add( MC_BuJpsiK_EvtTuple );
  double N_evtAnalyzed = EvtTpl_tree->GetEntries();
  cout<<"N_evtAnalyzed = "<< N_evtAnalyzed <<endl;
  double N_passed = MC2012_tree->GetEntries();
  cout<<"N_passed = "<< N_passed <<endl;
  double eff_strip = N_passed/N_evtAnalyzed;
  cout<<"Eff. of stripping on B+ bkg = "<< eff_strip  <<endl;

  // Analyze data sample :  
  /*TTree *newOSdata_2012_tree = PrepareTrees( OS_data );
  TH1D* bplusMass_data_h     = GetInvMOfBplusBKG(newOSdata_2012_tree);
  TCanvas *c_BplusMass_data = new TCanvas("c_BplusMass_data ","c_BplusMass_data");
  c_BplusMass_data->cd();
  bplusMass_data_h->Draw();
  
  // Save results to .root file
  TFile *Bplus_indata_f = new TFile("Bplus_indataBDTCut_f.root" , "RECREATE");//  TFile *Histos_f = new TFile("Histos_allMCfiles.root", "RECREATE");
  bplusMass_MC_h->Write();
  bplusMass_data_h->Write();
  Bplus_indata_f->Close();*/
  
  // recycables 
  // Compare distributions
  // CompareVars( newOSdata_2012_tree , newSSdata_2012_tree  );
  // GetSSCrossOS( newOSdata_2012_tree , newSSdata_2012_tree );
  
  
  return ;
}
