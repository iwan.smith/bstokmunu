#include <fstream>
#include <iostream>
#include <TROOT.h>
#include <TLatex.h>
#include <TFile.h>
#include <TTree.h>
#include <sstream>
#include <TChain.h>
#include <TCanvas.h>
#include <TObjArray.h>
#include <TBranch.h>
#include <TLorentzVector.h>
#include <iomanip>
#include <TH1F.h>
#include "/afs/cern.ch/user/b/bkhanji/roofitinclude.h" 
#include <TString.h>
//std::vector<TString> vector{“blabla”};
//using namespace std;

//std::vector<TString> test_vect;


 
std::vector<TString> MyCuts_MC;

std::vector<TString> MyCuts;
  


void DefineImportantVariables(TTree* mytree = 0 , bool MC= false )
{
  mytree->SetBranchStatus("*",0);
  mytree->SetBranchStatus("Bs_M",1);
  mytree->SetBranchStatus("Bs_MCORR",1);
  mytree->SetBranchStatus("Bs_P*",1);
  //mytree->SetBranchStatus("kaon_m_IsoMinBDT",1);
  //mytree->SetBranchStatus("kaon_m_*",1);
  //mytree->SetBranchStatus("kaon_m_PAIR_M",1);
  // mytree->SetBranchStatus("kaon_m_TRACK_Eta",1);
  //mytree->SetBranchStatus("muon_p_IsoMinBDT",1);
  //  mytree->SetBranchStatus("muon_p_*",1);
  //mytree->SetBranchStatus("muon_p_PAIR_M",1);
  //mytree->SetBranchStatus("muon_p_PIDK",1);
  // mytree->SetBranchStatus("kaon_m_PIDK",1);
  //mytree->SetBranchStatus("muon_p_PT",1);
  
  mytree->SetBranchStatus("kaon_m_P*",1); 
  mytree->SetBranchStatus("muon_p_NIsoTr_P*",1); 
  //mytree->SetBranchStatus("TMVA_*");
  mytree->SetBranchStatus("TMVA_*",1);
  mytree->SetBranchStatus("runNumber",1);
  mytree->SetBranchStatus("eventNumber",1);
  if (MC)
  {
    mytree->SetBranchStatus("*_MC_*_ID",1);
    mytree->SetBranchStatus("*_MC_*_KEY",1);
    mytree->SetBranchStatus("*_TRUEID",1);
  };
  
}

TTree* PrepareTrees(TString OS_data = "", bool MC = false, TString Cuts = "")
{
  
  //MyCuts_MC.clear();
  //MyCuts.clear();
  
  TChain *OSdata_2012_tree = new TChain("data_OS","data_OS");
  OSdata_2012_tree->Add( OS_data);
  // for(int i=0; i<MyCuts_MC.size(); ++i ){
  //int i=0;
  //Cuts = MyCuts.at(i);
  // }
  
  if (MC) {
    DefineImportantVariables(OSdata_2012_tree , true); 
    
    //for(int i=0; i<MyCuts_MC.size(); ++i ){
    //cout<<MyCuts_MC.at(i)<<endl;
    //Cuts_t.at(i) = MyCuts_MC.at(i);
    //Cuts = MyCuts_MC.at(i);
    //cout<<Cuts<<endl;
    // }
  }
  
  else DefineImportantVariables(OSdata_2012_tree );
  //for(int i=0; i<MyCuts_MC.size(); ++i ){
    TTree *newOSdata_2012_tree = OSdata_2012_tree->CopyTree(Cuts,"", 1000000000 , 0);  
    return newOSdata_2012_tree;
  //}
  
  
}


void Seleff_matching_2()
{
   std::vector<TString> MC2;
  MC2.clear();
  MC2.push_back("DTT_13774000_Bs_Dsmunu_cocktail_hqet2_DsmuInAcc_*.root");
  MC2.push_back("DTT_13144001_Bs_Jpsiphi_mm_CPV_update2012_DecProdCut_*.root");
  MC2.push_back("DTT_12143401_Bu_JpsiKst_mm_Kpi0_DecProdCut_*.root");
  MC2.push_back("DTT_12513001_Bu_rhomunu_DecProdCut_*.root");
  MC2.push_back("DTT_11874004_Bd_Dstmunu_Kpi_cocktail_hqet_D0muInAcc_BRCorr1_*.root");
  MC2.push_back("DTT_15512013_Lb_pmunu_DecProdCut_LCSR_*.root");
  MC2.push_back("DTT_12873002_Bu_D0munu_Kpi_cocktail_D0muInAcc_BRcorr1_*.root");
  MC2.push_back("DTT_12143001_Bu_JpsiK_mm_DecProdCut_*.root");
  MC2.push_back("DTT_13512010_Bs_Kmunu_DecProdCut_*.root");
  MC2.push_back("DTT_13512400_Bs_Kstmunu_Kpi0_DecProdCut_*.root");
  MC2.push_back("DTT_13512420_Bs_K1430munu_Kpi0_DecProdCut_*.root");
  MC2.push_back("DTT_11512400_Bd_rhomunu_pipi0_DecProdCut_*.root");
  MC2.push_back("DTT_13512410_Bs_K2stmunu_Kpi0_DecProdCut_*.root");
  MC2.push_back("DTT_11512011_Bd_pimunu_DecProdCut_*.root");
  MC2.push_back("DTT_11144001_Bd_JpsiKst_mm_DecProdCut_*.root");
    

  //insert here sample you want to look at
  //TString MC_BuJpsiK  = eosMC_dir + MC+ "/DecayTree";
  std::vector<TString> MC_BuJpsiK2;
  MC_BuJpsiK2.clear();
  for(int i=0; i<MC2.size(); ++i ){
    //cout<<MC2.at(i)<<endl;
  MC_BuJpsiK2.push_back("root://eoslhcb.cern.ch//eos/lhcb/wg/semileptonic/Bs2KmunuAna/Tuples/Bs2KMuNu_MCTUPLES_TRIMMED_BDT_30May16/" + MC2.at(i)+"/reducedTree");
  }


  //denominator are # of events passing the stripping
  std::vector<TString> MC_BuJpsiK_EvtTuple2;
  MC_BuJpsiK_EvtTuple2.clear();
  for(int i=0; i<MC2.size(); ++i ){
    //cout<<MC2.at(i)<<endl;
   MC_BuJpsiK_EvtTuple2.push_back("root://eoslhcb.cern.ch//eos/lhcb/wg/semileptonic/Bs2KmunuAna/Tuples/Bs2KMuNu_MCTUPLES_TRIMMED_BDT_30May16/" + MC2.at(i)+"/reducedTree");
  }

    MyCuts_MC.clear();
//13774000
MyCuts_MC.push_back("TMVA_charge_BDT>0.107 && TMVA_SS_aftercut_BDT>0.1 && abs(Bs_TRUEID)==531 && abs(kaon_m_TRUEID)==321 && abs(muon_p_TRUEID)==13 && abs(muon_p_MC_MOTHER_ID)==531  && abs(kaon_m_MC_MOTHER_ID)==431 && abs(kaon_m_MC_GD_MOTHER_ID)==531 &&(kaon_m_MC_GD_MOTHER_KEY==muon_p_MC_MOTHER_KEY)");
//13144001
MyCuts_MC.push_back("TMVA_charge_BDT>0.107 && TMVA_SS_aftercut_BDT>0.1 && abs(Bs_TRUEID)==531 && abs(kaon_m_TRUEID)==321 && abs(muon_p_TRUEID)==13 && abs(muon_p_MC_MOTHER_ID)==443 && abs(muon_p_MC_GD_MOTHER_ID)==531  && abs(kaon_m_MC_MOTHER_ID)==333 && abs(kaon_m_MC_GD_MOTHER_ID)==531 &&(kaon_m_MC_GD_MOTHER_KEY==muon_p_MC_GD_MOTHER_KEY)");
//12143401
MyCuts_MC.push_back("TMVA_charge_BDT>0.107 && TMVA_SS_aftercut_BDT>0.1 && abs(Bs_TRUEID)==521 && abs(kaon_m_TRUEID)==321 && abs(muon_p_TRUEID)==13 && abs(muon_p_MC_MOTHER_ID)==443 && abs(muon_p_MC_GD_MOTHER_ID)==521  && abs(kaon_m_MC_MOTHER_ID)==323 && abs(kaon_m_MC_GD_MOTHER_ID)==521 &&(kaon_m_MC_GD_MOTHER_KEY==muon_p_MC_GD_MOTHER_KEY)");
//12513001
MyCuts_MC.push_back("TMVA_charge_BDT>0.107 && TMVA_SS_aftercut_BDT>0.1 && abs(Bs_TRUEID)==521 && abs(kaon_m_TRUEID)==211 && abs(muon_p_TRUEID)==13 && abs(muon_p_MC_MOTHER_ID)==521 && abs(kaon_m_MC_MOTHER_ID)==113 &&  abs(kaon_m_MC_GD_MOTHER_ID)==521 && (kaon_m_MC_GD_MOTHER_KEY==muon_p_MC_MOTHER_KEY)");
//11874004
MyCuts_MC.push_back("TMVA_charge_BDT>0.107 && TMVA_SS_aftercut_BDT>0.1 && abs(Bs_TRUEID)==511 && abs(kaon_m_TRUEID)==321 && abs(muon_p_TRUEID)==13 && abs(muon_p_MC_MOTHER_ID)==511 && abs(kaon_m_MC_MOTHER_ID)==421 && abs(kaon_m_MC_GD_MOTHER_ID)==413 &&  abs(kaon_m_MC_GD_GD_MOTHER_ID)==511 && (kaon_m_MC_GD_GD_MOTHER_KEY==muon_p_MC_MOTHER_KEY)");
//15512013
MyCuts_MC.push_back("TMVA_charge_BDT>0.107 && TMVA_SS_aftercut_BDT>0.1 && abs(Bs_TRUEID)==5122 && abs(kaon_m_TRUEID)==2212 && abs(muon_p_TRUEID)==13 && abs(muon_p_MC_MOTHER_ID)==5122 && abs(kaon_m_MC_MOTHER_ID)==5122  && (kaon_m_MC_MOTHER_KEY==muon_p_MC_MOTHER_KEY)");
//12873002
MyCuts_MC.push_back("TMVA_charge_BDT>0.107 && TMVA_SS_aftercut_BDT>0.1 && abs(Bs_TRUEID)==521 && abs(kaon_m_TRUEID)==321 && abs(muon_p_TRUEID)==13 && abs(muon_p_MC_MOTHER_ID)==521 && abs(kaon_m_MC_MOTHER_ID)==421");
//12143001
MyCuts_MC.push_back("TMVA_charge_BDT>0.107 && TMVA_SS_aftercut_BDT>0.1 && abs(Bs_TRUEID)==521 && abs(kaon_m_TRUEID)==321 && abs(muon_p_TRUEID)==13 && abs(muon_p_MC_MOTHER_ID)==443 && abs(muon_p_MC_GD_MOTHER_ID)==521  && abs(kaon_m_MC_MOTHER_ID)==521 && (kaon_m_MC_MOTHER_KEY==muon_p_MC_GD_MOTHER_KEY)");
//Signal
MyCuts_MC.push_back("TMVA_charge_BDT>0.107 && TMVA_SS_aftercut_BDT>0.1 && abs(Bs_TRUEID)==531 && abs(kaon_m_TRUEID)==321 && abs(muon_p_TRUEID)==13 && abs(muon_p_MC_MOTHER_ID)==531 && abs(kaon_m_MC_MOTHER_ID)==531 && (kaon_m_MC_MOTHER_KEY==muon_p_MC_MOTHER_KEY)");
//13512400
MyCuts_MC.push_back("TMVA_charge_BDT>0.107 && TMVA_SS_aftercut_BDT>0.1 && abs(Bs_TRUEID)==531 && abs(kaon_m_TRUEID)==321 && abs(muon_p_TRUEID)==13 && abs(muon_p_MC_MOTHER_ID)==531 && abs(kaon_m_MC_MOTHER_ID)==323 && abs(kaon_m_MC_GD_MOTHER_ID)==531 && (kaon_m_MC_GD_MOTHER_KEY==muon_p_MC_MOTHER_KEY)");
//13512420
MyCuts_MC.push_back("TMVA_charge_BDT>0.107 && TMVA_SS_aftercut_BDT>0.1 && abs(Bs_TRUEID)==531 && abs(kaon_m_TRUEID)==321 && abs(muon_p_TRUEID)==13 && abs(muon_p_MC_MOTHER_ID)==531 && abs(kaon_m_MC_MOTHER_ID)==10321 && abs(kaon_m_MC_GD_MOTHER_ID)==531 && (kaon_m_MC_GD_MOTHER_KEY==muon_p_MC_MOTHER_KEY)");
//11512400
MyCuts_MC.push_back("TMVA_charge_BDT>0.107 && TMVA_SS_aftercut_BDT>0.1 && abs(Bs_TRUEID)==511 && abs(kaon_m_TRUEID)==211 && abs(muon_p_TRUEID)==13 && abs(muon_p_MC_MOTHER_ID)==511 && abs(kaon_m_MC_MOTHER_ID)==213 && abs(kaon_m_MC_GD_MOTHER_ID)==511 && (kaon_m_MC_GD_MOTHER_KEY==muon_p_MC_MOTHER_KEY)");
//13512410
MyCuts_MC.push_back("TMVA_charge_BDT>0.107 && TMVA_SS_aftercut_BDT>0.1 && abs(Bs_TRUEID)==531 && abs(kaon_m_TRUEID)==321 && abs(muon_p_TRUEID)==13 && abs(muon_p_MC_MOTHER_ID)==531 && abs(kaon_m_MC_MOTHER_ID)==325 && abs(kaon_m_MC_GD_MOTHER_ID)==531 && (kaon_m_MC_GD_MOTHER_KEY==muon_p_MC_MOTHER_KEY)");
//11512011
MyCuts_MC.push_back("TMVA_charge_BDT>0.107 && TMVA_SS_aftercut_BDT>0.1 && abs(Bs_TRUEID)==511 && abs(kaon_m_TRUEID)==211 && abs(muon_p_TRUEID)==13 && abs(muon_p_MC_MOTHER_ID)==511 && abs(kaon_m_MC_MOTHER_ID)==511 && (kaon_m_MC_MOTHER_KEY==muon_p_MC_MOTHER_KEY)");
//11144001
MyCuts_MC.push_back("TMVA_charge_BDT>0.107 && TMVA_SS_aftercut_BDT>0.1 && abs(Bs_TRUEID)==511 && abs(kaon_m_TRUEID)==321 && abs(muon_p_TRUEID)==13 && abs(muon_p_MC_MOTHER_ID)==443 && abs(kaon_m_MC_MOTHER_ID)==313 && abs(muon_p_MC_GD_MOTHER_ID)==511 && abs(kaon_m_MC_GD_MOTHER_ID)==511 && (kaon_m_MC_GD_MOTHER_KEY==muon_p_MC_GD_MOTHER_KEY)");

//&& abs(Bs_TRUEID)==521 && abs(kaon_m_TRUEID)==321 && abs(muon_p_TRUEID)==13 && muon_p_MC_MOTHER_ID==443 && abs(kaon_m_MC_MOTHER_ID)==521 && abs(muon_p_MC_GD_MOTHER_ID)==521";//&& muon_p_NIsoTr_MC_MOTHER_ID==443 && abs(muon_p_NIsoTr_MC_GD_MOTHER_ID)==521";
 MyCuts.clear();
 for(int i=0; i<MyCuts_MC.size(); ++i ){
   MyCuts.push_back("TMVA_charge_BDT>0.107 && TMVA_SS_aftercut_BDT>0.1");
 }
   
  // calculate efficicencies
  for(int i=0; i<MC_BuJpsiK2.size(); ++i ){
    TTree *MC2012_tree      = PrepareTrees( MC_BuJpsiK2.at(i), true, MyCuts_MC.at(i) );
    TTree *EvtTpl_tree      = PrepareTrees( MC_BuJpsiK_EvtTuple2.at(i), false, MyCuts.at(i) );
     cout<<MC_BuJpsiK2.at(i)<< endl;
     cout<<MC_BuJpsiK_EvtTuple2.at(i)<< endl;
     cout<<MyCuts_MC.at(i)<< endl;
     cout<<MyCuts.at(i)<< endl;
    double N_evtAnalyzed = EvtTpl_tree->GetEntries();
    cout<<"N_evtAnalyzed = "<< N_evtAnalyzed <<endl;
    double N_passed = MC2012_tree->GetEntries();
    cout<<"N_passed = "<< N_passed <<endl;
    double eff_strip = N_passed/N_evtAnalyzed;
    //cout<<"Efficiency of  = " << eff_strip  <<endl;
    cout<<"Efficiency of "<< MC_BuJpsiK2.at(i) <<  " = " << eff_strip  <<endl;
  }
   
  
  return ;
}
