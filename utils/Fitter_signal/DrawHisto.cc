#include "/afs/cern.ch/user/b/bkhanji/roofitinclude.h"
#include <iostream>
#include <TROOT.h>
#include <TLatex.h>
#include <TFile.h>
#include <TTree.h>
#include <sstream>
#include <TChain.h>
#include <TCanvas.h>
#include <TObjArray.h>
#include <TBranch.h>
#include <TLorentzVector.h>
#include <TLegend.h>
#include <iomanip>
#include <TH1F.h>
#include <TStyle.h>
#include "RooStats/ModelConfig.h"
#include "RooStats/HistFactory/Measurement.h"
#include "RooStats/HistFactory/MakeModelAndMeasurementsFast.h"
#include "RooStats/HistFactory/HistFactorySimultaneous.h"
#include "RooStats/HistFactory/HistFactoryNavigation.h"
//#include "TROOT.h"
#include "TSystem.h"
#include <TROOT.h>

using namespace RooFit;
using namespace RooStats;
using namespace HistFactory;

//gROOT->ForceStyle(); 
//gROOT->SetBatch(kFALSE);
//gROOT->Reset();
//gROOT->Clear();


int DrawHisto()
{
  gStyle->SetOptStat(0);
  TFile* f_hist = TFile::Open("Histos_allMCfiles.root", "READ");
  std::vector<TString> name;
  name.clear();
  name.push_back("SignalMC_Bs_MCORR");
  name.push_back("inclb_ALL_Bs_MCORR");
  name.push_back("Bu2JpsiKplusMC_Bs_MCORR");
  name.push_back("Bs_Kstmunu_Bs_MCORR");
  name.push_back("Bu2JpsiKstarMC_Bs_MCORR");
  name.push_back("Bs2DsmunuMC_Bs_MCORR");
  name.push_back("Bs_Jpsiphi_Bs_MCORR");
  name.push_back("Bs_K2stmunu_Bs_MCORR");
  name.push_back("Bd_JpsiKst_Bs_MCORR");
  name.push_back("Bs_K1430munu_Bs_MCORR");
  name.push_back("Bd_pimunu_Bs_MCORR");
  name.push_back("Bd_rhomunu_Bs_MCORR");
  name.push_back("Bd_Dstmunu_Bs_MCORR");
  name.push_back("Bu_D0munu_Bs_MCORR");
  name.push_back("Lb_pmunu_Bs_MCORR");
  


  TFile* f_hist_newBDT = TFile::Open("Histos_allMCfiles_newBDToptimization.root", "READ");
  std::vector<TString> name_newBDT;
  name_newBDT.clear();
  name_newBDT.push_back("SignalMC_Bs_MCORR");
  name_newBDT.push_back("inclb_ALL_Bs_MCORR");
  name_newBDT.push_back("Bu2JpsiKplusMC_Bs_MCORR");
  name_newBDT.push_back("Bs_Kstmunu_Bs_MCORR");
  name_newBDT.push_back("Bu2JpsiKstarMC_Bs_MCORR");
  name_newBDT.push_back("Bs2DsmunuMC_Bs_MCORR");
  name_newBDT.push_back("Bs_Jpsiphi_Bs_MCORR");
  name_newBDT.push_back("Bs_K2stmunu_Bs_MCORR");
  name_newBDT.push_back("Bd_JpsiKst_Bs_MCORR");
  name_newBDT.push_back("Bs_K1430munu_Bs_MCORR");
  name_newBDT.push_back("Bd_pimunu_Bs_MCORR");
  name_newBDT.push_back("Bd_rhomunu_Bs_MCORR");
  name_newBDT.push_back("Bd_Dstmunu_Bs_MCORR");
  name_newBDT.push_back("Bu_D0munu_Bs_MCORR");
  name_newBDT.push_back("Lb_pmunu_Bs_MCORR");
  //name.push_back();
  //name.push_back();

  TFile* f_q2 = TFile::Open("HistosQ2_allMCfiles_newBDToptimization.root", "READ");
  std::vector<TString> name1;
  name1.clear();
  name1.push_back("SignalMC_Bs_Regression_Q2_BEST");
  name1.push_back("inclb_ALL_Bs_Regression_Q2_BEST");
  name1.push_back("Bu2JpsiKplusMC_Bs_Regression_Q2_BEST");
  name1.push_back("Bs_Kstmunu_Bs_Regression_Q2_BEST");
  name1.push_back("Bu2JpsiKstarMC_Bs_Regression_Q2_BEST");
  name1.push_back("Bs2DsmunuMC_Bs_Regression_Q2_BEST");
  name1.push_back("Bs_Jpsiphi_Bs_Regression_Q2_BEST");
  name1.push_back("Bs_K2stmunu_Bs_Regression_Q2_BEST");
  name1.push_back("Bd_JpsiKst_Bs_Regression_Q2_BEST");
  name1.push_back("Bs_K1430munu_Bs_Regression_Q2_BEST");
  name1.push_back("Bd_pimunu_Bs_Regression_Q2_BEST");
  name1.push_back("Bd_rhomunu_Bs_Regression_Q2_BEST");
  name1.push_back("Bd_Dstmunu_Bs_Regression_Q2_BEST");
  name1.push_back("Bu_D0munu_Bs_Regression_Q2_BEST");
  name1.push_back("Lb_pmunu_Bs_Regression_Q2_BEST");
  
    
   for(int i=0; i<name.size(); ++i ){
    f_hist_newBDT->cd();
    cout<<name_newBDT.at(i)<<endl;;
    TCanvas *data_c = new TCanvas("c_"+name_newBDT.at(i) , "c_" + name_newBDT.at(i) );
    TH1F* h_data_newBDT = (TH1F*)f_hist_newBDT->Get(name_newBDT.at(i))->Clone(); 
    h_data_newBDT->SetLineColor(kBlack);
    h_data_newBDT->Draw("hist");
    f_hist->cd();
    cout<<name.at(i)<<endl;
    TH1F* h_data = (TH1F*)f_hist->Get(name.at(i))->Clone();
    h_data->Draw("hist same");
    TLegend *leg = new TLegend(0.1,0.7,0.3,0.9);
   leg->AddEntry(h_data,"Alessio's BDT","l");
   leg->AddEntry(h_data_newBDT,"new BDT optimization","l");
   //leg->AddEntry("gr","Graph with error bars","lep");
   leg->Draw();
   //data_c->Print("newBDTcomparison_"+name.at(i)+".eps");
   //gPad->BuildLegend(0.75,0.75,0.95,0.95,"");
  }
   //f_hist->Close();
  for(int i=0; i<name1.size(); ++i ){
    f_q2->cd();
    cout<<name1.at(i)<<endl;;
    TCanvas *data_c = new TCanvas("c_"+name1.at(i) , "c_" + name1.at(i) );
    TH1F* h_q2 = (TH1F*)f_q2->Get(name1.at(i))->Clone(); 
    h_q2->SetLineColor(kBlack);
    h_q2->Draw("hist");//reducedTree->Draw("Bs_Regression_Q2_BEST","Bs_Regression_Q2_BEST>10000000","")
    //f_hist->cd();
    //cout<<name.at(i)<<endl;
    //TH1F* h_data = (TH1F*)f_hist->Get(name.at(i))->Clone();
    //h_data->Draw("hist same");
    
   data_c->Print("Q2_"+name1.at(i)+".eps");
   //gPad->BuildLegend(0.75,0.75,0.95,0.95,"");
  }
   
  
  
   /*for(int j=0; j<name_newBDT.size(); ++j ){
    //TH1F* h_data = (TH1F*)f_hist->Get(name.at(i))->Clone();
    cout<<name_newBDT.at(j)<<endl;;
     f_hist_newBDT->cd();
     TH1F* h_data_newBDT = (TH1F*)f_hist_newBDT->Get(name_newBDT.at(j))->Clone();
    TCanvas *data_d = new TCanvas("d_"+name_newBDT.at(j) , "d_" + name_newBDT.at(j) );
    //h_data->Draw();
    h_data_newBDT->Draw();
    
    }*/

  
  //content->FindObject("*Bs_MCORR");
   TCanvas *c = new TCanvas("c_", "c_" );
  TString hname="SignalMC_Bs_MCORR";
   TString hname2="Bs2DsmunuMC_Bs_MCORR";
  TH1F* h_data = (TH1F*)f_hist->Get(hname)->Clone();
    h_data->Draw("hist");
    TH1F* h_data2 = (TH1F*)f_hist->Get(hname2)->Clone();
    h_data2->Draw("hist same");
    //}
  /*for(key=0, key<25;++key ){
    TH1F* h_data = (TH1F*)f_hist->Get(hname)->Clone();
  h_data->Draw();
  }*/
  //TH1F* h_data = (TH1F*)f_hist->Get("SignalMC_Bs_MCORR")->Clone();
  //h_data->Draw();

  /*TFile* f_MChist = TFile::Open("Histos_allMCfiles.root", "READ");
  TH1F* h_MCinclumuK = (TH1F*)f_MChist->Get("inclb_ALL_Bs_MCORR")->Clone();

  h_MCinclumuK->Draw();
  TH1F* h_MCsignal = (TH1F*)f_MChist->Get("SignalMC_Bs_MCORR")->Clone();
  TH1F* h_MCjpsiK = (TH1F*)f_MChist->Get("Bu2JpsiKplusMC_Bs_MCORR")->Clone();
  TH1F* h_MCKstmunu = (TH1F*)f_MChist->Get("Bs_Kstmunu_Bs_MCORR")->Clone();*/
 

  
  
  return 0;
}


