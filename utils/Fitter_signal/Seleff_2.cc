#include <fstream>
#include <iostream>
#include <TROOT.h>
#include <TLatex.h>
#include <TFile.h>
#include <TTree.h>
#include <sstream>
#include <TChain.h>
#include <TCanvas.h>
#include <TObjArray.h>
#include <TBranch.h>
#include <TLorentzVector.h>
#include <iomanip>
#include <TH1F.h>
#include "/afs/cern.ch/user/b/bkhanji/roofitinclude.h" 
#include "TSystem.h"

//nominator are BDT & trimming cuts applied
TString eosMC_dir = "root://eoslhcb.cern.ch//eos/lhcb/wg/semileptonic/Bs2KmunuAna/Tuples/Bs2KMuNu_MCTUPLES_TRIMMED_BDT_30May16/";
//TString eosMC_dir  = "root://eoslhcb.cern.ch//eos/lhcb/wg/semileptonic/Bs2KmunuAna/Tuples/obsolete/Bs2KMuNu_MCTUPLES_WITHBDT_30June16/";



TString MyCuts = "";
TString MyCuts_MC = "TMVA_SS_aftercut_BDT>0.1 && TMVA_charge_BDT>0.107";//"TMVA_SS_aftercut_BDT>-0.0954 "; //"TMVA_charge_BDT>0.107"; //&& TMVA_SS_aftercut_BDT>-0.0954 ";

  
void DefineImportantVariables(TTree* mytree = 0 , bool MC= false )
{
  mytree->SetBranchStatus("*",0);
  mytree->SetBranchStatus("Bs_M",1);
  mytree->SetBranchStatus("Bs_MCORR",1);
  mytree->SetBranchStatus("Bs_P*",1);
  mytree->SetBranchStatus("Bs_Hlt*",1);
  mytree->SetBranchStatus("Bs_Q2SOL1",1);
  
  //mytree->SetBranchStatus("kaon_m_IsoMinBDT",1);
  //mytree->SetBranchStatus("kaon_m_*",1);
  //mytree->SetBranchStatus("kaon_m_PAIR_M",1);
  // mytree->SetBranchStatus("kaon_m_TRACK_Eta",1);
  //mytree->SetBranchStatus("muon_p_IsoMinBDT",1);
  //  mytree->SetBranchStatus("muon_p_*",1);
  //mytree->SetBranchStatus("muon_p_PAIR_M",1);
  //mytree->SetBranchStatus("muon_p_PIDK",1);
  // mytree->SetBranchStatus("kaon_m_PIDK",1);
  //mytree->SetBranchStatus("muon_p_PT",1);
   mytree->SetBranchStatus("*_MC_*_ID",1);
    mytree->SetBranchStatus("*_TRUEID",1);
    mytree->SetBranchStatus("*_MC_*_KEY",1);
 
  mytree->SetBranchStatus("kaon_m_M*",1); 
  mytree->SetBranchStatus("muon_p_NIsoTr_P*",1); 
  //mytree->SetBranchStatus("TMVA_*");
  mytree->SetBranchStatus("TMVA_*",1);
  mytree->SetBranchStatus("runNumber",1);
  mytree->SetBranchStatus("eventNumber",1);
  if (MC)
  {
    //mytree->SetBranchStatus("totCandidates",1);
     mytree->SetBranchStatus("muon_p_*",1);
     mytree->SetBranchStatus("kaon_m_*",1); 
     //mytree->SetBranchStatus("Bs_BKGCAT",1); 
   
  };
  
}

TTree* PrepareTrees( TString OS_data = "", bool MC = false )
{
  
  TChain *OSdata_2012_tree = new TChain("data_OS","data_OS");
  //for(int i=0; i<OS_data.size(); ++i ){
     OSdata_2012_tree->Add( OS_data);
  TString Cuts_t = MyCuts;
  
  if (MC) {
    DefineImportantVariables(OSdata_2012_tree , true); 
    Cuts_t = MyCuts_MC;
  }
  
  else DefineImportantVariables(OSdata_2012_tree );
  
  TTree *newOSdata_2012_tree = OSdata_2012_tree->CopyTree(Cuts_t,"", 1000000000 , 0);  
   
  
  return newOSdata_2012_tree;
   
}




void Seleff_2()
{

  std::vector<TString> MC2;
  MC2.clear();
  MC2.push_back("DTT_15512014_Lb_pmunu_DecProdCut_LQCD_*.root");
  MC2.push_back("DTT_13512010_Bs_Kmunu_DecProdCut_*.root");
  MC2.push_back("DTT_11144001_Bd_JpsiKst_mm_DecProdCut_*.root");
  MC2.push_back("DTT_11512011_Bd_pimunu_DecProdCut_*.root");
  MC2.push_back("DTT_11512400_Bd_rhomunu_pipi0_DecProdCut_*.root");
  MC2.push_back("DTT_11574050_Bd_Dstpi_D0pi_Kmunu_DecProdCut_*.root");
  MC2.push_back("DTT_11574051_Bd_Dpi_Kpimunu_DecProdCut_*.root");
  MC2.push_back("DTT_11874004_Bd_Dstmunu_Kpi_cocktail_hqet_D0muInAcc_BRCorr1_*.root");
  //MC2.push_back("DTT_11874042_Bd_Dmunu_K-pi+pi+_cocktail_hqet_BRCorr1_DmuInAcc_*.root");
  MC2.push_back("DTT_12143001_Bu_JpsiK_mm_DecProdCut_*.root");
  MC2.push_back("DTT_12143401_Bu_JpsiKst_mm_Kpi0_DecProdCut_*.root");
  MC2.push_back("DTT_12513001_Bu_rhomunu_DecProdCut_*.root");
  MC2.push_back("DTT_12573050_Bu_D0pi_Kmunu_DecProdCut_*.root");
  MC2.push_back("DTT_12573200_Bu_Dst0pi_D0gamma_Kmunu_DecProdCut_*.root");
  MC2.push_back("DTT_12573400_Bu_Dst0pi_D0pi0_Kmunu_DecProdCut_*.root");
  MC2.push_back("DTT_12573401_Bu_D0rho_Kmunu_pipi0_DecProdCut_Dn_*.root");
  MC2.push_back("DTT_12873002_Bu_D0munu_Kpi_cocktail_D0muInAcc_BRcorr1_*.root");
  MC2.push_back("DTT_13144001_Bs_Jpsiphi_mm_CPV_update2012_DecProdCut_*.root");
  MC2.push_back("DTT_13512400_Bs_Kstmunu_Kpi0_DecProdCut_*.root");
  MC2.push_back("DTT_13512410_Bs_K2stmunu_Kpi0_DecProdCut_*.root");
  MC2.push_back("DTT_13512420_Bs_K1430munu_Kpi0_DecProdCut_*.root");
  MC2.push_back("DTT_13574040_Bs_D0Kst0_Kmunu_Kpi_DecProdCut_*.root");
  MC2.push_back("DTT_13774000_Bs_Dsmunu_cocktail_hqet2_DsmuInAcc_*.root");
  MC2.push_back("DTT_13796000_Bs_D0DsK_Kmunu_KKpi_DecProdCut_tightCut_*.root");
  MC2.push_back("DTT_15512013_Lb_pmunu_DecProdCut_LCSR_*.root");
  MC2.push_back("DTT_MC12_inclb_OC2Kmu_gencut_BKG_10010032_*.root");
  MC2.push_back("DTT_MC12_inclb_OC2KmuSS_gencut_BKG_10010035_*.root");
  MC2.push_back("DTT_MC12_inclb_OC2Kplusmu_gencut_BKG_10010037_*.root");
    

  //insert here sample you want to look at
  //TString MC_BuJpsiK  = eosMC_dir + MC+ "/DecayTree";
  std::vector<TString> MC_BuJpsiK2;
  MC_BuJpsiK2.clear();
  for(int i=0; i<MC2.size(); ++i ){
    //cout<<MC2.at(i)<<endl;
  MC_BuJpsiK2.push_back("root://eoslhcb.cern.ch//eos/lhcb/wg/semileptonic/Bs2KmunuAna/Tuples/Bs2KMuNu_MCTUPLES_TRIMMED_BDT_30May16/" + MC2.at(i)+"/reducedTree");
  }


  //denominator are # of events passing the stripping
  std::vector<TString> MC_BuJpsiK_EvtTuple2;
  MC_BuJpsiK_EvtTuple2.clear();
  for(int i=0; i<MC2.size(); ++i ){
    //cout<<MC2.at(i)<<endl;
   MC_BuJpsiK_EvtTuple2.push_back("root://eoslhcb.cern.ch//eos/lhcb/wg/semileptonic/Bs2KmunuAna/Tuples/Bs2KMuNu_MCTUPLES_RAW_BDT_30May16/"+ MC2.at(i) +"/DecayTree");
  }
  
  // calculate efficicencies
  for(int i=0; i<MC_BuJpsiK2.size(); ++i ){
     TTree *MC2012_tree      = PrepareTrees( MC_BuJpsiK2.at(i), true);
     TTree *EvtTpl_tree      = PrepareTrees( MC_BuJpsiK_EvtTuple2.at(i));
     cout<<MC_BuJpsiK2.at(i)<< endl;
    double N_evtAnalyzed = EvtTpl_tree->GetEntries();
    cout<<"N_evtAnalyzed = "<< N_evtAnalyzed <<endl;
    double N_passed = MC2012_tree->GetEntries();
    cout<<"N_passed = "<< N_passed <<endl;
    double eff_strip = N_passed/N_evtAnalyzed;
    //cout<<"Efficiency of  = " << eff_strip  <<endl;
    cout<<"Efficiency of "<< MC_BuJpsiK2.at(i) <<  " = " << eff_strip  <<endl;
  }
   
  
  
  return ;
}
