#include "TString.h"

namespace as_signal {
  TString KMuNu() {
    return "abs(Bs_TRUEID)==531 && abs(kaon_m_TRUEID)==321 && abs(muon_p_TRUEID)==13 && abs(muon_p_MC_MOTHER_ID)==531 && abs(kaon_m_MC_MOTHER_ID)==531 && kaon_m_MC_MOTHER_KEY==muon_p_MC_MOTHER_KEY";
  }
  TString JpsiK() {
    return "abs(kaon_m_TRUEID)==321 && abs(muon_p_TRUEID)==13 && abs(muon_p_MC_MOTHER_ID)==443 && abs(muon_p_MC_GD_MOTHER_ID)==521 && abs(kaon_m_MC_MOTHER_ID)==521 && kaon_m_MC_MOTHER_KEY==muon_p_MC_GD_MOTHER_KEY";
  }
  TString JpsiPhi() {
    return "abs(Bs_TRUEID)==531 && abs(kaon_m_TRUEID)==321 && abs(muon_p_TRUEID)==13 && abs(muon_p_MC_MOTHER_ID)==443 && abs(muon_p_MC_GD_MOTHER_ID)==531 && abs(kaon_m_MC_GD_MOTHER_ID)==531 && kaon_m_MC_GD_MOTHER_KEY==muon_p_MC_GD_MOTHER_KEY && abs(kaon_m_MC_MOTHER_ID)==333";
  }
}
namespace with_isolation {
  TString JpsiK() {
    return as_signal::JpsiK() + " && abs(muon_p_NIsoTr_TRUEID) == 13 && muon_p_NIsoTr_MC_MOTHER_ID==443 && abs(muon_p_NIsoTr_MC_GD_MOTHER_ID)== 521 &&  muon_p_NIsoTr_MC_MOTHER_KEY==muon_p_MC_MOTHER_KEY"; 
  }
}
namespace as_calibration {
  TString JpsiK() {
    return as_signal::JpsiK() + " && abs(muon_m_TRUEID) == 13 && muon_m_MC_MOTHER_KEY == muon_p_MC_MOTHER_KEY";
  }
  TString JpsiPhi() {
    return as_signal::JpsiPhi() + " && abs(muon_m_TRUEID) == 13 && muon_m_MC_MOTHER_KEY == muon_p_MC_MOTHER_KEY && abs(kaon_p_TRUEID) == 321 && kaon_m_MC_MOTHER_KEY == kaon_p_MC_MOTHER_KEY";
  }
}

