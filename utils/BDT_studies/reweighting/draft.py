# requires a bit of setup:
# source /cvmfs/sft.cern.ch/lcg/views/LCG_87/$CMTCONFIG/setup.sh
# pip install --user root_numpy
# pip install --user progressbar
# pip install --user hep_ml
# (requires ~360MB space in $HOME/.local)

from root_numpy import tree2array, array2tree
import numpy
import ROOT
from ROOT import TFile, TTree
from hep_ml.reweight import BinsReweighter
import os

try:
    ROOT.gInterpreter.LoadMacro(os.path.join(os.environ["BSTOKMUNUROOT"],"include","TruthMatch.h"))
except KeyError:
    # use heuristic instead
    thisdir = os.path.dirname(os.path.abspath(__file__))  # BsToKMuNu/utils/BDT_studies/reweighting
    tmpdir = os.path.dirname(thisdir)  # BsToKMuNu/utils/BDT_studies
    tmpdir = os.path.dirname(tmpdir)  # BsToKMuNu/utils/
    bstokmunuroot = os.path.dirname(tmpdir)  # BsToKMuNu/
    ROOT.gInterpreter.LoadMacro(os.path.join(bstokmunuroot,"include","TruthMatch.h"))
sig_cutstring = ROOT.as_signal.KMuNu()
control_cutstring = "muon_p_L0MuonDecision_TOS&&(muon_p_Hlt2SingleMuonDecision_TOS||Bu_Hlt2TopoMu2BodyBBDTDecision_TOS)&&kaon_m_PT>800";
control_cutstring += "&&" + ROOT.as_calibration.JpsiK().Data();



kmunufile_up = TFile.Open("root://eoslhcb.cern.ch//eos/lhcb/wg/semileptonic/Bs2KmunuAna/Tuples/Bs2KMuNu_MCTUPLES_TRIMMED_BDT_30May16/DTT_13512010_Bs_Kmunu_DecProdCut_Up_Py8_MC12_trimmed.TMVA_SS_aftercut_BDT.root","READ")
kmunutree_up = kmunufile_up.Get("reducedTree")
kmunuarray_up = tree2array(kmunutree_up,branches=["Bs_PT","kaon_m_PT"],selection=sig_cutstring.Data())
jpsikfile_up = TFile.Open("root://eoslhcb.cern.ch//eos/lhcb/wg/semileptonic/Bs2KmunuAna/Tuples/Bu2JpsiK_MCTUPLES_RAW_BDT_12November16/DTT_12143001_Bu_JpsiK_mm_DecProdCut_Up_Py8_MC12_jpsik.TMVA_SS_aftercut_BDT.root","read")
jpsiktree_up = jpsikfile_up.Get("DecayTree")
jpsikarray_up = tree2array(jpsiktree_up,branches=["Bs_PT","kaon_m_PT"],selection=control_cutstring)



# This assumes all variables have the same datatype. I use the following hack sometimes to convert integers to floats, but there must be a better way: branches=["Bs_PT","kaon_m_PT","nTracks*1.0"]
kmunuarray_up_prime = kmunuarray_up.view(( kmunuarray_up.dtype[0], 2 )) 
jpsikarray_up_prime = jpsikarray_up.view(( jpsikarray_up.dtype[0], 2 ))


original_weights_up = numpy.ones(len(jpsikarray_up))
reweighter_up = BinsReweighter(n_bins=130)
reweighter_up.fit(original=jpsikarray_up_prime,target=kmunuarray_up_prime,original_weight=original_weights_up)





kmunufile_dn = TFile.Open("root://eoslhcb.cern.ch//eos/lhcb/wg/semileptonic/Bs2KmunuAna/Tuples/Bs2KMuNu_MCTUPLES_TRIMMED_BDT_30May16/DTT_13512010_Bs_Kmunu_DecProdCut_Dn_Py8_MC12_trimmed.TMVA_SS_aftercut_BDT.root","READ")
kmunutree_dn = kmunufile_dn.Get("reducedTree")
kmunuarray_dn = tree2array(kmunutree_dn,branches=["Bs_PT","kaon_m_PT"],selection=sig_cutstring.Data())
jpsikfile_dn = TFile.Open("root://eoslhcb.cern.ch//eos/lhcb/wg/semileptonic/Bs2KmunuAna/Tuples/Bu2JpsiK_MCTUPLES_RAW_BDT_12November16/DTT_12143001_Bu_JpsiK_mm_DecProdCut_Dn_Py8_MC12_jpsik.TMVA_SS_aftercut_BDT.root","read")
jpsiktree_dn = jpsikfile_dn.Get("DecayTree")
jpsikarray_dn = tree2array(jpsiktree_dn,branches=["Bs_PT","kaon_m_PT"],selection=control_cutstring)

kmunuarray_dn_prime = kmunuarray_dn.view(( kmunuarray_dn.dtype[0], 2 )) 
jpsikarray_dn_prime = jpsikarray_dn.view(( jpsikarray_dn.dtype[0], 2 ))

original_weights_dn = numpy.ones(len(jpsikarray_dn))
reweighter_dn = BinsReweighter(n_bins=130)
reweighter_dn.fit(original=jpsikarray_dn_prime,target=kmunuarray_dn_prime,original_weight=original_weights_dn)

# Now start writing the trees
#Source: https://rootpy.github.io/root_numpy/reference/generated/root_numpy.array2tree.html#root_numpy.array2tree

print "I don't know why I'm getting the following warnings. \nThe numbers look fine."

jpsikfile_up_new = TFile.Open("root://eoslhcb.cern.ch//eos/lhcb/wg/semileptonic/Bs2KmunuAna/Tuples/Bu2JpsiK_MCTUPLES_RAW_BDT_12November16/DTT_12143001_Bu_JpsiK_mm_DecProdCut_Up_Py8_MC12_jpsik.TMVA_SS_aftercut_BDT_rew.root","recreate")
jpsiktree_up_new = jpsiktree_up.CloneTree(0)

jpsiktree_up_new_rw_Vars = tree2array(jpsiktree_up,branches=["Bs_PT","kaon_m_PT"])
jpsiktree_up_new_rw_Vars = jpsiktree_up_new_rw_Vars.view(( jpsiktree_up_new_rw_Vars.dtype[0], 2 )) 


myvar = reweighter_up.predict_weights( jpsiktree_up_new_rw_Vars ) 
myvar.dtype = [ ( 'momentum_weight', numpy.float64 ) ]
array2tree(myvar, tree=jpsiktree_up_new)


jpsikfile_up_new.cd()
jpsiktree_up_new.Write()
jpsikfile_up_new.Close()


jpsikfile_dn_new = TFile.Open("root://eoslhcb.cern.ch//eos/lhcb/wg/semileptonic/Bs2KmunuAna/Tuples/Bu2JpsiK_MCTUPLES_RAW_BDT_12November16/DTT_12143001_Bu_JpsiK_mm_DecProdCut_Dn_Py8_MC12_jpsik.TMVA_SS_aftercut_BDT_rew.root","recreate")
jpsiktree_dn_new = jpsiktree_dn.CloneTree(0)

jpsiktree_dn_new_rw_Vars = tree2array(jpsiktree_dn,branches=["Bs_PT","kaon_m_PT"])
jpsiktree_dn_new_rw_Vars = jpsiktree_dn_new_rw_Vars.view(( jpsiktree_dn_new_rw_Vars.dtype[0], 2 )) 


myvar = reweighter_dn.predict_weights( jpsiktree_dn_new_rw_Vars ) 
myvar.dtype = [ ( 'momentum_weight', numpy.float64 ) ]
array2tree(myvar, tree=jpsiktree_dn_new)



jpsikfile_dn_new.cd()
jpsiktree_dn_new.Write()
jpsikfile_dn_new.Close()


### datafiles

jpsikfile_up = TFile.Open("root://eoslhcb.cern.ch//eos/lhcb/wg/semileptonic/Bs2KmunuAna/Tuples/Bu2JpsiK_DATATUPLES_RAW_BDT_15November16/DTT_2011_Reco14Strip21r1_Up_DIMUON_jpsik.TMVA_SS_aftercut_BDT.root","read")
jpsiktree_up = jpsikfile_up.Get("DecayTree")
jpsikfile_up_new = TFile.Open("root://eoslhcb.cern.ch//eos/lhcb/wg/semileptonic/Bs2KmunuAna/Tuples/Bu2JpsiK_DATATUPLES_RAW_BDT_15November16/DTT_2011_Reco14Strip21r1_Up_DIMUON_jpsik.TMVA_SS_aftercut_BDT_rew.root","recreate")
jpsiktree_up_new = jpsiktree_up.CloneTree(0)

jpsiktree_up_new_rw_Vars = tree2array(jpsiktree_up,branches=["Bs_PT","kaon_m_PT"])
jpsiktree_up_new_rw_Vars = jpsiktree_up_new_rw_Vars.view(( jpsiktree_up_new_rw_Vars.dtype[0], 2 )) 


myvar = reweighter_up.predict_weights( jpsiktree_up_new_rw_Vars ) 
myvar.dtype = [ ( 'momentum_weight', numpy.float64 ) ]
array2tree(myvar, tree=jpsiktree_up_new)

jpsikfile_up_new.cd()
jpsiktree_up_new.Write()
jpsikfile_up_new.Close()




jpsikfile_up = TFile.Open("root://eoslhcb.cern.ch//eos/lhcb/wg/semileptonic/Bs2KmunuAna/Tuples/Bu2JpsiK_DATATUPLES_RAW_BDT_15November16/DTT_2012_Reco14Strip21_Up_DIMUON_jpsik.TMVA_SS_aftercut_BDT.root","read")
jpsiktree_up = jpsikfile_up.Get("DecayTree")
jpsikfile_up_new = TFile.Open("root://eoslhcb.cern.ch//eos/lhcb/wg/semileptonic/Bs2KmunuAna/Tuples/Bu2JpsiK_DATATUPLES_RAW_BDT_15November16/DTT_2012_Reco14Strip21_Up_DIMUON_jpsik.TMVA_SS_aftercut_BDT_rew.root","recreate")
jpsiktree_up_new = jpsiktree_up.CloneTree(0)

jpsiktree_up_new_rw_Vars = tree2array(jpsiktree_up,branches=["Bs_PT","kaon_m_PT"])
jpsiktree_up_new_rw_Vars = jpsiktree_up_new_rw_Vars.view(( jpsiktree_up_new_rw_Vars.dtype[0], 2 )) 


myvar = reweighter_up.predict_weights( jpsiktree_up_new_rw_Vars ) 
myvar.dtype = [ ( 'momentum_weight', numpy.float64 ) ]
array2tree(myvar, tree=jpsiktree_up_new)


jpsikfile_up_new.cd()
jpsiktree_up_new.Write()
jpsikfile_up_new.Close()




jpsikfile_dn = TFile.Open("root://eoslhcb.cern.ch//eos/lhcb/wg/semileptonic/Bs2KmunuAna/Tuples/Bu2JpsiK_DATATUPLES_RAW_BDT_15November16/DTT_2012_Reco14Strip21_Down_DIMUON_jpsik.TMVA_SS_aftercut_BDT.root","read")
jpsiktree_dn = jpsikfile_dn.Get("DecayTree")
jpsikfile_dn_new = TFile.Open("root://eoslhcb.cern.ch//eos/lhcb/wg/semileptonic/Bs2KmunuAna/Tuples/Bu2JpsiK_DATATUPLES_RAW_BDT_15November16/DTT_2012_Reco14Strip21_Down_DIMUON_jpsik.TMVA_SS_aftercut_BDT_rew.root","recreate")
jpsiktree_dn_new = jpsiktree_dn.CloneTree(0)

jpsiktree_dn_new_rw_Vars = tree2array(jpsiktree_dn,branches=["Bs_PT","kaon_m_PT"])
jpsiktree_dn_new_rw_Vars = jpsiktree_dn_new_rw_Vars.view(( jpsiktree_dn_new_rw_Vars.dtype[0], 2 )) 


myvar = reweighter_dn.predict_weights( jpsiktree_dn_new_rw_Vars ) 
myvar.dtype = [ ( 'momentum_weight', numpy.float64 ) ]
array2tree(myvar, tree=jpsiktree_dn_new)


jpsikfile_dn_new.cd()
jpsiktree_dn_new.Write()
jpsikfile_dn_new.Close()



jpsikfile_dn = TFile.Open("root://eoslhcb.cern.ch//eos/lhcb/wg/semileptonic/Bs2KmunuAna/Tuples/Bu2JpsiK_DATATUPLES_RAW_BDT_15November16/DTT_2011_Reco14Strip21r1_Down_DIMUON_jpsik.TMVA_SS_aftercut_BDT.root","read")
jpsiktree_dn = jpsikfile_dn.Get("DecayTree")
jpsikfile_dn_new = TFile.Open("root://eoslhcb.cern.ch//eos/lhcb/wg/semileptonic/Bs2KmunuAna/Tuples/Bu2JpsiK_DATATUPLES_RAW_BDT_15November16/DTT_2011_Reco14Strip21r1_Down_DIMUON_jpsik.TMVA_SS_aftercut_BDT_rew.root","recreate")
jpsiktree_dn_new = jpsiktree_dn.CloneTree(0)


jpsiktree_dn_new_rw_Vars = tree2array(jpsiktree_dn,branches=["Bs_PT","kaon_m_PT"])
jpsiktree_dn_new_rw_Vars = jpsiktree_dn_new_rw_Vars.view(( jpsiktree_dn_new_rw_Vars.dtype[0], 2 )) 


myvar = reweighter_dn.predict_weights( jpsiktree_dn_new_rw_Vars ) 
myvar.dtype = [ ( 'momentum_weight', numpy.float64 ) ]
array2tree(myvar, tree=jpsiktree_dn_new)

jpsikfile_dn_new.cd()
jpsiktree_dn_new.Write()
jpsikfile_dn_new.Close()


