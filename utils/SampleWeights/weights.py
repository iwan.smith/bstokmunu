
weights = {
    "DTT_11144001_Bd_JpsiKst_mm_Dn_Py8_MC12":11.55569,
    "DTT_11512011_Bd_pimunu_Dn_Py8_MC12":0.54392,
    "DTT_11512400_Bd_rhomunu_pipi0_Dn_Py8_MC12":0.86274,
    "DTT_12873002_Bu_D0munu_Kpi_Dn_Py8_MC12":2.16513,
    "DTT_13144001_Bs_Jpsiphi_mm_Dn_Py8_MC12":3.69155,
    "DTT_13512010_Bs_Kmunu_Dn_Py8_MC12":10.80326,
    "DTT_13512400_Bs_Kstmunu_Kpi0_Dn_Py8_MC12":5.81353,
    "DTT_13512410_Bs_K2stmunu_Kpi0_Dn_Py8_MC12":1.15870,
    "DTT_13512420_Bs_K1430munu_Kpi0_Dn_Py8_MC12":2.27035,
    "DTT_15512013_Lb_pmunu_Dn_Py8_MC12":1.37311,
    "DTT_15512014_Lb_pmunu_Dn_Py8_MC12":1.29626,
    "DTT_12143001_Bu_JpsiK_mm_Dn_Py8_MC12":21.48377
}
