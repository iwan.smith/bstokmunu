#!/bin/bash


for J in `seq -w $@`
do
echo "
cd /afs/cern.ch/user/i/ismith/ANA/BsToKMuNu/utils
. SetupEnv.sh
cd `pwd`
bash Execute.sh $J
" | tee /dev/tty | bsub -q 8nh -J Vub_MC_$J
done
