# file /build/jenkins-tests/workspace/nightly-builds/checkout/tmp/checkout/DBASE/Gen/DecFiles/v30r1/options/13512010.py generated: Wed, 16 Aug 2017 12:20:58
#
# Event Type: 13512010
#
# ASCII decay Descriptor: {[[B_s0] => K- nu_mu mu+]cc, [B_s~0] => K+ anti-nu_mu mu+]cc
#
from Configurables import Generation
Generation().EventType = 13512010
Generation().SampleGenerationTool = "SignalRepeatedHadronization"
from Configurables import SignalRepeatedHadronization
Generation().addTool( SignalRepeatedHadronization )
Generation().SignalRepeatedHadronization.ProductionTool = "PythiaProduction"
from Configurables import ToolSvc
from Configurables import EvtGenDecay
ToolSvc().addTool( EvtGenDecay )
ToolSvc().EvtGenDecay.UserDecayFile = "../DEC_NoCuts/Bs_Kmunu=DecProdCut.dec"
Generation().SignalRepeatedHadronization.CutTool = ""
Generation().SignalRepeatedHadronization.SignalPIDList = [ 531,-531 ]

# Ad-hoc particle gun code This is redundant since ParticleGun isn't called. We use Pythia instead.
"""
from Configurables import ParticleGun
pgun = ParticleGun("ParticleGun")
pgun.SignalPdgCode = 531
pgun.DecayTool = "EvtGenDecay"
#pgun.GenCutTool = "DaughtersInLHCb"

from Configurables import FlatNParticles
pgun.NumberOfParticlesTool = "FlatNParticles"
pgun.addTool( FlatNParticles , name = "FlatNParticles" )

from GaudiKernel.SystemOfUnits import *


from Configurables import MomentumRange
pgun.addTool(MomentumRange, name="MomentumRange")
pgun.ParticleGunTool = "MomentumRange"
pgun.MomentumRange.PdgCodes = [531, -531]
pgun.MomentumRange.ThetaMin = 0*rad
pgun.MomentumRange.ThetaMax = 3.14*rad



from Configurables import MomentumSpectrum
pgun.ParticleGunTool = "MomentumSpectrum"
pgun.addTool( MomentumSpectrum , name = "MomentumSpectrum" )
pgun.MomentumSpectrum.PdgCodes = [ 531,-531 ]
pgun.MomentumSpectrum.InputFile = "$PGUNSDATAROOT/data/Ebeam4000GeV/MomentumSpectrum_531.root"
pgun.MomentumSpectrum.BinningVariables = "pteta"
pgun.MomentumSpectrum.HistogramPath = "h_pteta"


from Configurables import BeamSpotSmearVertex
pgun.addTool(BeamSpotSmearVertex, name="BeamSpotSmearVertex")
pgun.VertexSmearingTool = "BeamSpotSmearVertex"
pgun.EventType = 13512010
"""
