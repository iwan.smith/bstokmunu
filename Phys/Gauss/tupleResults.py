from Configurables import (
    DaVinci,
    EventSelector,
    PrintMCTree,
    MCDecayTreeTuple
    )
from DecayTreeTuple.Configuration import *

"""Configure the variables below with:
    decay: Decay you want to inspect, using 'newer' LoKi decay descriptor syntax,
    decay_heads: Particles you'd like to see the decay tree of,
    datafile: Where the file created by the Gauss generation phase is, and
    year: What year the MC is simulating.
"""

# https://twiki.cern.ch/twiki/bin/view/LHCb/FAQ/LoKiNewDecayFinders
datafile = "xgenfile.xgen"

# For a quick and dirty check, you don't need to edit anything below here.
##########################################################################

# Create an MC DTT containing any candidates matching the decay descriptor


mctuple_DsMuNu = MCDecayTreeTuple("MCDecayTreeTuple_Ds")
mctuple_DsMuNu.Decay = "( [ [B_s0]nos -> ^( D_s- => ^K+ ^K- ^pi- ) ^mu+ ^nu_mu ... ]CC ) || ([ [B_s~0]os -> ^( D_s- => ^K+ ^K- ^pi- ) ^mu+ ^nu_mu ... ]CC )"

mctuple_KMuNu = MCDecayTreeTuple("MCDecayTreeTuple_K")
mctuple_KMuNu.Decay = "'([ [B_s0]nos -> ^K- ^mu+ ^nu_mu ... ]CC || [ [B_s~0]os -> ^K- ^mu+ ^nu_mu ... ]CC)'"

mctuple_K892MuNu = MCDecayTreeTuple("MCDecayTreeTuple_K_892")
mctuple_K892MuNu.Decay = "'([ [B_s0]nos -> ^( K*(892)- => ^K- ^pi0 ) ^mu+ ^nu_mu ... ]CC || [ [B_s~0]os -> ^( K*(892)- => ^K- ^pi0 ) ^mu+ ^nu_mu ... ]CC)'"

mctuple_K1430MuNu = MCDecayTreeTuple("MCDecayTreeTuple_K_1430")
mctuple_K1430MuNu.Decay = "'([ [B_s0]nos -> ^( K*_0(1430)- => ^K- ^pi0 ) ^mu+ ^nu_mu ... ]CC || [ [B_s~0]os -> ^( K*_0(1430)- => ^K- ^pi0 ) ^mu+ ^nu_mu ... ]CC)'"

mctuples = [mctuple_DsMuNu, mctuple_KMuNu, mctuple_K892MuNu, mctuple_K1430MuNu]

for mctuple in mctuples:
    
    mctuple.ToolList = [
      "MCTupleToolHierarchy"
      ,"LoKi::Hybrid::MCTupleTool/LoKi_Photos"

       ]
    # Add a 'number of photons' branch
    mctuple.addTupleTool("MCTupleToolKinematic").Verbose = True
    mctuple.addTupleTool("LoKi::Hybrid::TupleTool/LoKi_Photos").Variables = {
      "nPhotos": "MCNINTREE(('gamma' == MCABSID))"
    }
    
    

# Print the decay tree for any particle in decay_heads
printMC = PrintMCTree()
printMC.ParticleNames = ["B_s0", "B_s~0"]

# Name of the .xgen file produced by Gauss
EventSelector().Input = ["DATAFILE='{0}' TYP='POOL_ROOTTREE' Opt='READ'".format(datafile)]

# Configure DaVinci
DaVinci().TupleFile = "DVntuple.root"
DaVinci().Simulation = True
DaVinci().Lumi = False
DaVinci().DataType = "2012"
DaVinci().UserAlgorithms = [printMC] + mctuples

