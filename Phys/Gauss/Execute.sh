#!/usr/bin/bash

export BASEDIR=`pwd`
export NEWDIR=BatchDir_$1
export RUNNUMBER=$1;

export NEV=500

mkdir $NEWDIR 
cd $NEWDIR

pwd



# see here https://its.cern.ch/jira/browse/LHCBGAUSS-726
# https://its.cern.ch/jira/browse/LHCBGAUSS-1036

export EVENTTYPE="13512400"
bash ../Gen_13512400.sh

mv NewCatalog.xml NewCatalog_$EVENTTYPE.xml
mv GeneratorLog.xml GeneratorLog_$EVENTTYPE.xml


FILE=Gauss-$EVENTTYPE*xgen

ln -s Gauss-${EVENTTYPE}-${NEV}ev-*.xgen xgenfile.xgen
lb-run DaVinci/v42r2 bash -c 'gaudirun.py ../tupleResults.py' # > /dev/null
rm xgenfile.xgen
rm $FILE
mv DVntuple.root DVntuple_$EVENTTYPE.root



export EVENTTYPE="13512420"
bash ../Gen_13512420.sh

mv NewCatalog.xml NewCatalog_$EVENTTYPE.xml
mv GeneratorLog.xml GeneratorLog_$EVENTTYPE.xml


FILE=Gauss-$EVENTTYPE*xgen

ln -s Gauss-${EVENTTYPE}-${NEV}ev-*.xgen xgenfile.xgen
lb-run DaVinci/v42r2 bash -c 'gaudirun.py ../tupleResults.py' # > /dev/null
rm xgenfile.xgen
rm $FILE
mv DVntuple.root DVntuple_$EVENTTYPE.root
