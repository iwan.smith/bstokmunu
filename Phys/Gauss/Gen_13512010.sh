export EVENTTYPE="13512010"

# Setup Gauss Version for sim08a
. /cvmfs/lhcb.cern.ch/lib/lhcb/LBSCRIPTS/LBSCRIPTS_v9r1p7/InstallArea/scripts/LbLogin.sh -c x86_64-slc5-gcc46-opt
. /cvmfs/lhcb.cern.ch/lib/lhcb/LBSCRIPTS/LBSCRIPTS_v9r1p7/InstallArea/scripts/SetupProject.sh GAUSS v45r3

export APPCONFIGOPTS2="/cvmfs/lhcb.cern.ch/lib/lhcb/DBASE/AppConfig/v3r171/options"

gaudirun.py \
$BASEDIR/Gauss-Job.py \
$GAUSSOPTS/GenStandAlone.py \
$APPCONFIGOPTS2/Gauss/Sim08-Beam4000GeV-md100-2012-nu2.5.py \
$APPCONFIGOPTS/Gauss/DataType-2012.py \
$APPCONFIGOPTS/Gauss/NoPacking.py \
$BASEDIR/DEC_NoCuts/$EVENTTYPE.py \
$LBPYTHIA8ROOT/options/Pythia8.py \
$APPCONFIGOPTS/Gauss/G4PL_FTFP_BERT_EmNoCuts_noLHCbphys.py \
$APPCONFIGOPTS/Persistency/Compression-ZLIB-1.py

# http://lhcb-release-area.web.cern.ch/LHCb-release-area/DOC/STATISTICS/SIM08STAT/SemiLep-WG/Generation_Sim08-Beam4000GeV-mu100-2012-nu2.5-Pythia8.html#13774000
