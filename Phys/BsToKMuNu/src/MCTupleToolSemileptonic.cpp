// Include files
// from Gaudi
#include "GaudiKernel/ToolFactory.h"
// local
#include "MCTupleToolSemileptonic.h"

#include "TLorentzVector.h"
#include "TVector3.h"

using namespace LHCb;

//-----------------------------------------------------------------------------
// Implementation file for class : MCTupleToolSemileptonic
//
// 2015-10-28 : Angelo Di Canto
//-----------------------------------------------------------------------------

// Declaration of the Tool Factory
// actually acts as a using namespace TupleTool
DECLARE_TOOL_FACTORY( MCTupleToolSemileptonic )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
  MCTupleToolSemileptonic::MCTupleToolSemileptonic( const std::string& type,
                                              const std::string& name,
                                              const IInterface* parent )
    : TupleToolBase ( type, name , parent )
{
  declareInterface<IMCParticleTupleTool>(this);
}

//=============================================================================
StatusCode MCTupleToolSemileptonic::fill( const LHCb::MCParticle*
                                       , const LHCb::MCParticle* mcp
                                       , const std::string& head
                                       , Tuples::Tuple& tuple )
{
  const std::string prefix=fullName(head);
  bool test=true;

  Gaudi::LorentzVector nu_trueP(0,0,0,0);
  int nu_trueId(0);

  if ( mcp && !isStable(mcp) )
  {
    // find decay vertex
    const LHCb::MCVertex *mcv(0);
    SmartRefVector<LHCb::MCVertex>::const_iterator iv;
    for ( iv = mcp->endVertices().begin(); iv != mcp->endVertices().end(); iv++ )
    {
      if( (*iv)->type() == LHCb::MCVertex::DecayVertex ||
          (*iv)->type() == LHCb::MCVertex::OscillatedAndDecay ||
          (*iv)->type() == LHCb::MCVertex::HadronicInteraction )
      {
        mcv = *iv;
        break;
      }
    }
    if ( mcv ) {
	   // find neutrino
	   SmartRefVector<LHCb::MCParticle>::const_iterator idau;
	   for ( idau = mcv->products().begin(); idau != mcv->products().end(); idau++ )
      {
        int pid = abs( (*idau)->particleID().pid() );
        if ( pid==12 || pid==14 || pid==16 )
        {
          nu_trueId = (*idau)->particleID().pid();
          nu_trueP  = (*idau)->momentum();
          break;
        }
      }
    }
  }
  std::vector<TLorentzVector> nu_solns;

  test &= tuple->column( prefix+"_Nu_TRUEID", nu_trueId );
  test &= tuple->column( prefix+"_Nu_TRUEP_", nu_trueP );
  
  if ( nu_trueId != 0 ){

	  Gaudi::LorentzVector LV_B = mcp->momentum();
	  
	  TLorentzVector B4(LV_B.px(), LV_B.py(), LV_B.pz(), LV_B.E() );
	  TLorentzVector nu4(nu_trueP.px(), nu_trueP.py(), nu_trueP.pz(), nu_trueP.E() );
	  
	  TLorentzVector Y4 = B4 - nu4;
	  TVector3 M_dirn = B4.Vect().Unit();
	  
	  nu_solns = recoNu(Y4, M_dirn, 5279);
  }
  if (nu_solns.size() == 2){
    //std::cout << nu_solns[0].Px() << "  " << nu_solns[1].Px() << "  " << nu_trueP.px() << "  ";
    test &= tuple->column( prefix+"_Nu_SOL1_TRUEP_PX", nu_solns[0].Px() );
    test &= tuple->column( prefix+"_Nu_SOL1_TRUEP_PY", nu_solns[0].Py() );
    test &= tuple->column( prefix+"_Nu_SOL1_TRUEP_PZ", nu_solns[0].Pz() );
    test &= tuple->column( prefix+"_Nu_SOL1_TRUEP_PE", nu_solns[0].E() );
    test &= tuple->column( prefix+"_Nu_SOL1_TRUEP_ETA",nu_solns[0].Eta() );
    
    test &= tuple->column( prefix+"_Nu_SOL2_TRUEP_PX", nu_solns[1].Px() );
    test &= tuple->column( prefix+"_Nu_SOL2_TRUEP_PY", nu_solns[1].Py() );
    test &= tuple->column( prefix+"_Nu_SOL2_TRUEP_PZ", nu_solns[1].Pz() );
    test &= tuple->column( prefix+"_Nu_SOL2_TRUEP_PE", nu_solns[1].E() );
    test &= tuple->column( prefix+"_Nu_SOL2_TRUEP_ETA",nu_solns[1].Eta() );
   
    //Now find the correct solution
    
    double factor1 = pow(nu_solns[0].Px() - nu_trueP.px(), 2) + pow(nu_solns[0].Py() - nu_trueP.py(), 2) + pow(nu_solns[0].Pz() - nu_trueP.pz(), 2);
    double factor2 = pow(nu_solns[1].Px() - nu_trueP.px(), 2) + pow(nu_solns[1].Py() - nu_trueP.py(), 2) + pow(nu_solns[1].Pz() - nu_trueP.pz(), 2);
    if ( factor1 < factor2 )
      test &= tuple->column( prefix+"_Nu_TRUE_SOL", 1);
    else
      test &= tuple->column( prefix+"_Nu_TRUE_SOL", 2);
    // std::cout << factor1 << "  " << factor2 << std::endl;

  }
  else{
    test &= tuple->column( prefix+"_Nu_SOL1_TRUEP_PX", 0. );
    test &= tuple->column( prefix+"_Nu_SOL1_TRUEP_PY", 0. );
    test &= tuple->column( prefix+"_Nu_SOL1_TRUEP_PZ", 0. );
    test &= tuple->column( prefix+"_Nu_SOL1_TRUEP_PE", 0. );
    test &= tuple->column( prefix+"_Nu_SOL1_TRUEP_ETA",0. );
    test &= tuple->column( prefix+"_Nu_SOL2_TRUEP_PX", 0. );
    test &= tuple->column( prefix+"_Nu_SOL2_TRUEP_PY", 0. );
    test &= tuple->column( prefix+"_Nu_SOL2_TRUEP_PZ", 0. );
    test &= tuple->column( prefix+"_Nu_SOL2_TRUEP_PE", 0. );
    test &= tuple->column( prefix+"_Nu_SOL2_TRUEP_ETA",0. );
    test &= tuple->column( prefix+"_Nu_TRUE_SOL", 0);
  }  
  
  
  
  return StatusCode(test);
}

std::vector<TLorentzVector> MCTupleToolSemileptonic::recoNu(TLorentzVector Y, TVector3 M_dirn, double mass)
{
	std::vector<TLorentzVector> p_nu;
	// Get 3 Vector from Y
	TVector3 Y3  = (Y).Vect(); 
	// Construct combined pmu/Kmu 4 vector in new rotated frame
	TLorentzVector Ppmu(Y3.Dot(M_dirn),(Y).Perp(M_dirn),0.0,(Y).E());
	// Get component of Y3 perpendicular to *M_dirn
	TVector3 Perp_dirn  = ( Y3  - (M_dirn) * Y3.Dot((M_dirn))).Unit();  
	// Calculate neutrino energy in mother rest frame
	double E_nurest = ( mass  * mass  - (Y).M2())/ (2 * mass ); 
	// Calculate pmu " "
	double E_pmurest = sqrt(E_nurest * E_nurest + (Y).M2());
	double px_rest;
  
	// Find magnitude of momentum along mother dirn in rest frame

	if( E_nurest * E_nurest  - Ppmu.Py() * Ppmu.Py() >= 0)
	{
	    px_rest = sqrt( E_nurest * E_nurest  - Ppmu.Py() * Ppmu.Py() ); 
	}
	else
	{  
	    return p_nu;
	}

	// px_rest = sqrt( E_nurest * E_nurest  - Ppmu.Py() * Ppmu.Py() ); 
	// quadratic coefficients
	double A = (Y).E() * (Y).E()  - Ppmu.Px() * Ppmu.Px();
	double B = - 2 * Ppmu.Px()  * (E_nurest * E_pmurest + px_rest * px_rest );
	double C =  - (E_nurest * E_pmurest + px_rest * px_rest )*
	    (E_nurest * E_pmurest + px_rest * px_rest ) +  (Y).E() * (Y).E() * Ppmu.Py() * Ppmu.Py();

	if (B*B < 4*A*C) { 
	    return  p_nu;
	}
	// Two neutrino E/p solutions in Lab Frame
	double p1nu = (- B + sqrt(B*B - 4 * A * C) )/(2*A);
	double p2nu = (- B - sqrt(B*B - 4 * A * C) )/(2*A);

	// reconstruct neutrino 3 vectors and 4 vectors
	TVector3 P_nu_recon_3V1 = (M_dirn) * p1nu  + Perp_dirn * -Ppmu.Py(); 
	TVector3 P_nu_recon_3V2 = (M_dirn) * p2nu  + Perp_dirn * -Ppmu.Py(); 
	p_nu.push_back(TLorentzVector(P_nu_recon_3V1, sqrt(p1nu*p1nu + Ppmu.Py() * Ppmu.Py())));
	p_nu.push_back(TLorentzVector(P_nu_recon_3V2, sqrt(p2nu*p2nu + Ppmu.Py() * Ppmu.Py())));
	return p_nu;
    }


