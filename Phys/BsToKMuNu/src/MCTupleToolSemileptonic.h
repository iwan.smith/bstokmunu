#ifndef MCTupleToolSemileptonic_H
#define MCTupleToolSemileptonic_H 1

// Include files
#include "DecayTreeTupleBase/TupleToolBase.h"
#include "Kernel/IMCParticleTupleTool.h"
#include "Kernel/Particle2MCLinker.h"
#include "Kernel/IDaVinciAssociatorsWrapper.h"
#include "TLorentzVector.h"
#include "TVector3.h"

/** @class MCTupleToolSemileptonic
 *
 *  @author Angelo Di Canto
 *  @date   2015-10-28
 */
class MCTupleToolSemileptonic : public TupleToolBase, virtual public IMCParticleTupleTool
{
public:
  /// Standard constructor
  MCTupleToolSemileptonic( const std::string& type,
                        const std::string& name,
                        const IInterface* parent);

  virtual ~MCTupleToolSemileptonic(){}; ///< Destructor

  virtual StatusCode fill( const LHCb::MCParticle*
                           , const LHCb::MCParticle*
                           , const std::string&
                           , Tuples::Tuple& );
                           
  std::vector<TLorentzVector> recoNu(TLorentzVector, TVector3, double);


#include "DecayTreeTupleBase/isStable.h"
};

#endif // MCTupleToolSemileptonic_H
