#include "GaudiKernel/ToolFactory.h"
#include "GaudiAlg/Tuple.h"
#include "GaudiAlg/TupleObj.h"
#include "Event/Particle.h"
#include "Kernel/IPVReFitter.h"
#include "Kernel/IDVAlgorithm.h"
#include <Kernel/GetIDVAlgorithm.h>
#include <Kernel/IDistanceCalculator.h>
#include "TMath.h"
#include "IsoBDT_Bs.h"
#include "TMVAClassification_Bs2KmuNu.class.h"
#include "TupleToolBs2Dsmunu.h"
#include <utility>
#include <iostream>     
#include <algorithm>    

#if (defined __GNUC__)
#define UNUSED __attribute__((unused))
#else
#define UNUSED
#endif

//-----------------------------------------------------------------------------
// Implementation file for class : TupleToolBs2Dsmunu
//
//-----------------------------------------------------------------------------
using namespace LHCb ;
using namespace Gaudi::Units;
using namespace ROOT::Math;
//Declaration of Factory
DECLARE_TOOL_FACTORY( TupleToolBs2Dsmunu )  //commented to adapt to DecayTreeTuple
//============================================================================
TupleToolBs2Dsmunu::TupleToolBs2Dsmunu(const std::string& type,
                                         const std::string& name,
                                         const IInterface* parent )
  : TupleToolBase ( type, name , parent ),
  m_dva(0), 
  m_dist(0),
  m_descend(0),
  m_util(0),
  //m_pvReFitter(0),
  m_pvReFitterName( "LoKi::PVReFitter:PUBLIC" ),
  m_vtxfitter(0),
  m_read_BDT_muontrack(0),
  m_read_BDT_kaontrack(0),
  m_read_BDT_muontrack_New(0),
  m_read_BDT_kaontrack_New(0)
  
{
  declareInterface<IParticleTupleTool>(this);
  declareProperty("ParticlePath",
                  m_ParticlePath="/Event/Phys/StdAllNoPIDsPions/Particles"
                  );
  declareProperty("VarSuffix",
                  aux_name  = "_Long"
                  );
  
}
// =============================================================================
StatusCode TupleToolBs2Dsmunu::initialize() {

  const StatusCode sc = TupleToolBase::initialize();
  if ( sc.isFailure() ) return sc;
  
  std::vector<std::string> BDT_Var_names;

  m_dva = Gaudi::Utils::getIDVAlgorithm ( contextSvc(), this ) ;
  if (!m_dva) return Error("Couldn't get parent DVAlgorithm");
  
  m_dist = m_dva->distanceCalculator();
  
  m_descend = tool<IParticleDescendants> ( "ParticleDescendants", this );
  if( ! m_descend ) {fatal() << "Unable to retrieve ParticleDescendants tool "<< endreq;
    return StatusCode::FAILURE; }
  
  m_util = tool<ITaggingUtilsChecker> ( "TaggingUtilsChecker", this );
  if( ! m_util ) {fatal() << "Unable to retrieve TaggingUtilsChecker tool "<< endreq;    
    return StatusCode::FAILURE;}
  
  m_pvReFitter = tool<IPVReFitter>( m_pvReFitterName, this );
  if ( !m_pvReFitter )
  {
    return Error( "Unable to retrieve IPVReFitter instance" );
  }
    
  m_vtxfitter = tool<IVertexFit>("LoKi::VertexFitter");
  if(!m_vtxfitter ) 
  {
    fatal() << "Unable to retrieve Loki VertexFitter" << endreq;
    return StatusCode::FAILURE;
  }
  
  BDTvariableNames_bb_bkg_training(m_inNames_muon );
  BDTvariableNames_bb_bkg_training(m_inNames_kaon );
  BDTvariableNames_Bplus_training(m_inNames_muon_New );
  BDTvariableNames_Bplus_training(m_inNames_kaon_New );
  
  m_read_BDT_muontrack = new ReadBDT_bS(m_inNames_muon);
  m_read_BDT_kaontrack = new ReadBDT_bS(m_inNames_kaon);
  
  m_read_BDT_muontrack_New = new ReadBDTG_isoBDT_Bplus(m_inNames_muon_New);
  m_read_BDT_kaontrack_New = new ReadBDTG_isoBDT_Bplus(m_inNames_kaon_New);
  
  m_input_muon = new std::vector<double>;
  m_input_kaon = new std::vector<double>;
  
  m_input_muon_New = new std::vector<double>;
  m_input_kaon_New = new std::vector<double>;
  
  return sc;
}
//=============================================================================
StatusCode TupleToolBs2Dsmunu::fill(const LHCb::Particle* mother 
                                     , const LHCb::Particle* P
                                     , const std::string& head
                                     , Tuples::Tuple& tuple)
{
  Assert( P && m_dva
          , "No mother or particle, or tools misconfigured." );
  
  const std::string prefix=fullName(head);
  bool test = true;
  if( !P ) {return StatusCode::FAILURE ; std::cout<<"I did not find the particle ----- "<<std::endl;};
  if( !( P->particleID().hasBottom() )) return StatusCode::SUCCESS;
  
  int iso_muon = 0;     double minSumBDT = -5 ;        
  int iso_ds = 0;     double minMinBDT_New  = 5.5 ;  
  
  double minMinBDT_New_muon  = 5.5 ;  
  double minMinBDT_New_ds  = 5.5 ;  
  //double MinBDT_New    = -2.5;
  //double SumBDT        = -2.5;
  int    best_track_charge = 0 ;
  UNUSED double MuMu_M_best        = -1 ;
  double MuMu_M_best_fromiso= -1 ;
  
  UNUSED double Best_MuMu_VtxChi2  = -1 ;
  UNUSED double Best_MuMu_distance = -1 ;
  UNUSED double Best_MuMu_distance_chi2  = -1 ;
  double Sum_of_best_MuMu_d_chi2s = -1;
    
  UNUSED double Kh_M_best          = -1 ;
  double Kh_M_best_fromiso  = -1 ;
  
  UNUSED double Best_Kh_VtxChi2  = -1 ;
  UNUSED double Best_Kh_distance = -1 ;
  UNUSED double Best_Kh_distance_chi2   = -1 ;
  double Sum_of_best_Kh_d_chi2s   = -1;
    
  const VertexBase* aPV = NULL;
  aPV = m_dva->bestVertex(mother);//bestVertex
    
  Gaudi::XYZPoint PosPV(0,0,200000) ;
  if(!aPV){ return StatusCode::SUCCESS; }
  if(aPV){PosPV = aPV->position(); }
  
  const Vertex* endv = mother->endVertex();
  double Bd_mass = mother->momentum().M();
  
  Particle::ConstVector parts_cand = m_descend->descendants(mother);
  
  const Particle* muon;
  const Particle* ds;

  if (mother->daughters()[0]->particleID().isLepton()) {
    muon   = mother->daughters()[0];
    ds   = mother->daughters()[1];
  } else {
    muon   = mother->daughters()[1];
    ds   = mother->daughters()[0];
  }
  if(!muon)   return StatusCode::SUCCESS;
  if(!ds)   return StatusCode::SUCCESS;
  
  Gaudi::XYZPoint PosSV(0,0,200000);
  if( !endv ) { return StatusCode::FAILURE;  
    fatal() << "Can't retrieve the end vertex for TupleToolGeometry cannot act on such a particle============anch" << endmsg ;}
  if(  endv ) { PosSV= mother->endVertex()->position(); } 
  
  // Get info from B to estimate the function used in the isolation BDT:
  Gaudi::XYZVector A = mother->momentum().Vect();
  Gaudi::XYZVector B = endv->position() - aPV->position();
  double cosPFD = A.Dot( B ) / std::sqrt( A.Mag2()*B.Mag2() );
  double Bd_eta = mother->momentum().Eta();
  double Bd_phi = mother->momentum().Phi();
  double Bd_pT  = mother->pt();
  double Sum_of_trackpt = 0;
  double Bd_PointingAngle  = std::acos(cosPFD);
  
  // Estimate corrected mass
  double pNuT        = sin(Bd_PointingAngle)*std::sqrt( A.Mag2());
  UNUSED double corrMass    = sqrt(Bd_mass*Bd_mass +pNuT*pNuT) + pNuT;
  
  XYZTVector Muon4vect = muon->momentum();
  XYZTVector Ds4vect = ds->momentum();
  
  double minchi2_MuMu = 99999;
  double minchi2_Kh   = 99999;
  
  double MuMu_VtxChi2 = -1;
  double Kh_VtxChi2   = -1;
  
  LHCb::Particle::Range allparts ;
  if (exist<LHCb::Particle::Range>(m_ParticlePath))
  { allparts = get<LHCb::Particle::Range>(m_ParticlePath); }
  else return Error("Nothing found at "+ m_ParticlePath , StatusCode::SUCCESS,1);
  
  LHCb::Particle::Range::const_iterator im;
  for(im = allparts.begin() ; im != allparts.end() ; ++im)
  {
    const Particle * axp  = (*im);
    const Track* track = axp->proto()->track();
        
    bool isInDecay = false;
    Particle::ConstVector::const_iterator it_p;
    // Get the decendants
    //Particle::ConstVector parts_cand = m_descend->descendants(mother);
    
    for( it_p=parts_cand.begin(); it_p!=parts_cand.end(); it_p++){
      if(!(*it_p)->isBasicParticle()) continue;
      const LHCb::Track* Track_decay = (*it_p)->proto()->track();
      //if(!track || !Track_decay) continue;
      if(Track_decay && track){
        //debug()<<"track of decay particle exist!"<<endreq;
        if(Track_decay == track){
          //debug()<<"This track is in decay .. remove it"<<endreq;
          isInDecay = true;
        }  
      } 
    }
    
    
    if(isInDecay) continue;
        
    const double track_minIPchi2 = get_MINIPCHI2(axp);
    double track_pt  = axp->pt();
    double track_eta = track->momentum().eta();
    double track_phi = track->momentum().phi();
    double track_Matchchi2 = track->info(LHCb::Track::FitMatchChi2,-1);
    
    XYZTVector track4vect = axp->momentum();
    int track_charge = (std::abs(axp->particleID().pid()))/(axp->particleID().pid());
    //std::cout<<"TRACK CHARGE IS " << track_charge <<std::endl;
    
    XYZTVector MuMu_mom = Muon4vect + track4vect;
    XYZTVector Kh_mom   = Ds4vect  + track4vect;
    double Kh_mass   = std::abs(std::sqrt(Kh_mom.M2())   );
    double MuMu_mass = std::abs(std::sqrt(MuMu_mom.M2()) );
    
    Gaudi::XYZPoint pos_track(track->position());
    Gaudi::XYZVector mom_track(track->momentum());
    Gaudi::XYZPoint pos_muon(muon->proto()->track()->position() );
    Gaudi::XYZVector mom_muon(muon->proto()->track()->momentum());
    Gaudi::XYZPoint pos_ds(ds->endVertex()->position() );
    Gaudi::XYZVector mom_ds(ds->momentum());
    
    // Calculate the input of ISO variable :
    Gaudi::XYZPoint vtx_muon_track(0.,0.,0.);
    Gaudi::XYZPoint vtx_ds_track(0.,0.,0.);
        
    double doca_muon(-1.) , angle_muon(-1.) ; 
    double doca_ds(-1.) , angle_ds(-1.) ; 
    
    InCone(pos_muon   , mom_muon , pos_track , mom_track , vtx_muon_track  , doca_muon, angle_muon);
    InCone(pos_ds   , mom_ds , pos_track , mom_track , vtx_ds_track  , doca_ds, angle_ds);
    
    double PVdis_muon_track   = ( vtx_muon_track.z() -PosPV.z() ) / fabs(vtx_muon_track.z()-PosPV.z())*(vtx_muon_track-PosPV).R() ;
    double SVdis_muon_track   = ( vtx_muon_track.z() -PosSV.z() ) / fabs(vtx_muon_track.z()-PosSV.z())*(vtx_muon_track-PosSV).R() ;
    double fc_mu = pointer(vtx_muon_track -PosPV ,mom_track , mom_muon );
    
    double PVdis_ds_track   = ( vtx_ds_track.z() -PosPV.z() ) / fabs(vtx_ds_track.z()-PosPV.z())*(vtx_ds_track-PosPV).R() ;
    double SVdis_ds_track   = ( vtx_ds_track.z() -PosSV.z() ) / fabs(vtx_ds_track.z()-PosSV.z())*(vtx_ds_track-PosSV).R() ;
    double fc_k = pointer(vtx_ds_track -PosPV ,mom_track , mom_ds );
    
    m_input_muon->clear();
    m_input_muon->reserve(6);
    m_input_muon->push_back(track_minIPchi2);
    m_input_muon->push_back(PVdis_muon_track);
    m_input_muon->push_back(SVdis_muon_track);
    m_input_muon->push_back(doca_muon);
    m_input_muon->push_back(angle_muon);
    m_input_muon->push_back(fc_mu);

    m_input_kaon->clear();
    m_input_kaon->reserve(6);
    m_input_kaon->push_back(track_minIPchi2);
    m_input_kaon->push_back(PVdis_ds_track);
    m_input_kaon->push_back(SVdis_ds_track);
    m_input_kaon->push_back(doca_ds);
    m_input_kaon->push_back(angle_ds);
    m_input_kaon->push_back(fc_k);
    
    double BDT_val_muon =0;
    double BDT_val_ds =0;
    BDT_val_muon =  m_read_BDT_muontrack->GetMvaValue( *m_input_muon );
    BDT_val_ds =  m_read_BDT_kaontrack->GetMvaValue( *m_input_kaon );
        
    // New BDT tunning:
    // Sisters for slpion and muon
    m_input_muon_New->clear();
    m_input_muon_New->reserve(10);
    m_input_muon_New->push_back(track_minIPchi2);
    m_input_muon_New->push_back(track_eta);
    m_input_muon_New->push_back(track_phi);
    m_input_muon_New->push_back(track_pt);
    m_input_muon_New->push_back(track_Matchchi2);
    m_input_muon_New->push_back(PVdis_muon_track);
    m_input_muon_New->push_back(SVdis_muon_track);
    m_input_muon_New->push_back(doca_muon);
    m_input_muon_New->push_back(angle_muon);
    m_input_muon_New->push_back(fc_mu);
    
    m_input_kaon_New->clear();
    m_input_kaon_New->reserve(9);
    m_input_kaon_New->push_back(track_minIPchi2);
    m_input_kaon_New->push_back(track_eta);
    m_input_kaon_New->push_back(track_phi);
    m_input_kaon_New->push_back(track_pt);
    m_input_kaon_New->push_back(track_Matchchi2);
    m_input_kaon_New->push_back(PVdis_ds_track);
    m_input_kaon_New->push_back(SVdis_ds_track);
    m_input_kaon_New->push_back(doca_ds);
    m_input_kaon_New->push_back(angle_ds);
    m_input_kaon_New->push_back(fc_k);
    
    double BDT_val_muon_New =0;
    double BDT_val_ds_New =0;
    BDT_val_muon_New =  m_read_BDT_muontrack_New->GetMvaValue( *m_input_muon_New );
    BDT_val_ds_New =  m_read_BDT_kaontrack_New->GetMvaValue( *m_input_kaon_New );
    // Get the mumu mass corresponding to best non-isolating track
    if( minMinBDT_New_muon > BDT_val_muon_New  ) 
    { minMinBDT_New_muon = BDT_val_muon_New ; MuMu_M_best_fromiso = MuMu_mass;  }
    if( minMinBDT_New_ds > BDT_val_ds_New  ) 
    { minMinBDT_New_ds = BDT_val_ds_New ; Kh_M_best_fromiso = Kh_mass;  }
    
    double MinBDT_New = std::min(BDT_val_muon_New , BDT_val_ds_New);
    
    if(  minMinBDT_New > MinBDT_New  ) {
      minMinBDT_New = MinBDT_New;
      //std::cout<<"CHARGE OF WORST ISOLATED TRACK W.R.T B VERTEX IS " << track_charge <<std::endl;
      best_track_charge = track_charge;
    }
    
    double SumBDT = BDT_val_muon + BDT_val_ds ;
    
    if( minSumBDT<SumBDT ) {
      minSumBDT = SumBDT;
    }
    
        
    // Apply giampie's iso cut:
    if ( angle_muon < 0.27 && fc_mu<0.60 && doca_muon<0.13 && track_minIPchi2 > 3.0 &&
         SVdis_muon_track >-0.15 && SVdis_muon_track<30. && PVdis_muon_track>0.5 && PVdis_muon_track<40.
      ) {
      iso_muon += 1;
      
    }
    
    if ( angle_ds < 0.27 && fc_k<0.60 && doca_ds<0.13 && track_minIPchi2 > 3.0 &&
         SVdis_ds_track >-0.15 && SVdis_ds_track<30. && PVdis_ds_track>0.5 && PVdis_ds_track<40.
         ) {
      iso_ds += 1;
      
    }
    
    // calculate the Jpsi mass :
        
    LHCb::Vertex vtx_MuMu; LHCb::Vertex vtx_Kh;
    double track_MM_sign = muon->charge()/( axp->charge());
    double track_Kh_sign = ds->charge()/( axp->charge());
    //std::cout<<"track_MM_sign = " << track_MM_sign << std::endl;
    //std::cout<<"track_Kh_sign = " << track_Kh_sign << std::endl;
    double MuMu_distance =  0 ; double MuMu_chi2 =  0 ;
    double Kh_distance   =  0 ; double Kh_chi2 =  0;
    
    StatusCode ok_mumu =  m_dist->distance( muon , axp , MuMu_distance, MuMu_chi2 );
    StatusCode ok_kk   =  m_dist->distance( ds , axp , Kh_distance  , Kh_chi2   );
        
    StatusCode MuMu_Vtx_fit = m_vtxfitter ->fit( vtx_MuMu, *muon, *axp );
    if ( !MuMu_Vtx_fit ) { MuMu_VtxChi2 = -1; }
    else {
      MuMu_VtxChi2 = vtx_MuMu.chi2()/(double)vtx_MuMu.nDoF();
      if ( minchi2_MuMu>0 && minchi2_MuMu> MuMu_VtxChi2 && track_MM_sign < 0)
      {
        minchi2_MuMu       = MuMu_VtxChi2   ;
        Best_MuMu_VtxChi2  = MuMu_VtxChi2   ;
        Best_MuMu_distance =  MuMu_distance ;
        Best_MuMu_distance_chi2 =  MuMu_chi2 ;
        Sum_of_best_MuMu_d_chi2s +=  MuMu_chi2 ;
        MuMu_M_best = MuMu_mass;
        
      }
    }
    
    
    StatusCode Kh_Vtx_fit   = m_vtxfitter ->fit( vtx_Kh, *ds, *axp );
    if ( !Kh_Vtx_fit ) { Kh_VtxChi2 = -1; }
    else {
      Kh_VtxChi2 = vtx_Kh.chi2()/(double)vtx_Kh.nDoF();
      if ( minchi2_Kh > Kh_VtxChi2 && track_Kh_sign < 0)
      {
        minchi2_Kh = Kh_VtxChi2   ;
        Best_Kh_VtxChi2 = Kh_VtxChi2;
        Best_Kh_distance =  Kh_distance;
        Best_Kh_distance_chi2   = Kh_chi2;
        Sum_of_best_Kh_d_chi2s += Kh_chi2;
        Kh_M_best = Kh_mass;
      }
    }
    
    double cone_radius = std::sqrt(std::pow((Bd_eta - track_eta) , 2)  + 
                                   std::pow((Bd_phi - track_phi) , 2)  ) ;
    
    if ( cone_radius <1){
      Sum_of_trackpt += axp->pt() ;
    }
  }
  
  double Bd_CDF3 = Bd_pT/(Bd_pT+Sum_of_trackpt);
  /*
    if (track_type == 1 ) aux_name = "_VELO" ;
    if (track_type == 3 ) aux_name = "_Long" ;
    if (track_type == 4 ) aux_name = "_Upstream" ;
  */
  
  test &= tuple->column(prefix+"_IsoTrack_Charge" +aux_name, best_track_charge );
  //test &= tuple->column(prefix+"_CorrMass" , corrMass);
  test &= tuple->column(prefix+"_iso_muon"+aux_name    , iso_muon);
  test &= tuple->column(prefix+"_iso_Ds"+aux_name     , iso_ds);
  test &= tuple->column(prefix+"_iso_SumBDT"+aux_name   , minSumBDT);
  test &= tuple->column(prefix+"_iso_MinBDT"+aux_name   , minMinBDT_New);
  test &= tuple->column(prefix+"_ConeIso"+aux_name      , Bd_CDF3);
  //test &= tuple->column(prefix+"_MuMu_M_bestchi2"+aux_name  , MuMu_M_best);
  test &= tuple->column(prefix+"_MuMu_M_iso"+aux_name  , MuMu_M_best_fromiso);
  //test &= tuple->column(prefix+"_Best_MuMu_VtxChi2"+aux_name   , Best_MuMu_VtxChi2  );
  //test &= tuple->column(prefix+"_Best_MuMu_distance"+aux_name  , Best_MuMu_distance );
  //test &= tuple->column(prefix+"_Best_MuMu_distance_chi2"+aux_name    , Best_MuMu_distance_chi2 );
  //test &= tuple->column(prefix+"_Sum_of_best_MuMu_d_chi2s"+aux_name  , Sum_of_best_MuMu_d_chi2s);
  //test &= tuple->column(prefix+"_Kh_M_bestchi2"+aux_name           , Kh_M_best);
  test &= tuple->column(prefix+"_Kh_M_iso"+aux_name           , Kh_M_best_fromiso);
  //test &= tuple->column(prefix+"_Best_Kh_VtxChi2"+aux_name    , Best_Kh_VtxChi2 );
  //test &= tuple->column(prefix+"_Best_Kh_distance"+aux_name    , Best_Kh_distance );
  //test &= tuple->column(prefix+"_Best_Kh_distance_chi2"+aux_name    , Best_Kh_distance_chi2 );
  //test &= tuple->column(prefix+"_Sum_of_best_Kh_d_chi2s"+aux_name  , Sum_of_best_Kh_d_chi2s);
  
  return StatusCode(test);
}

const Particle* TupleToolBs2Dsmunu::findID(unsigned int id,
                                     Particle::ConstVector& v,
                                     std::string opts )
{
  const Particle* p=0;
  for( Particle::ConstVector::const_iterator ip=v.begin(); ip!=v.end(); ip++)
  {
    if((*ip)->particleID().abspid() == id) 
    {
      if(id==211 && opts=="slow")
      {
        const Particle* mater = m_util->motherof(*ip, v);
        if(mater->particleID().abspid()!=413) continue;
      }
      if(id==211 && opts=="fast")
      {
        const Particle* mater = m_util->motherof(*ip, v);
        if(mater->particleID().abspid()==413) continue;
      }
      p = (*ip);
      break;
    }
  }
 if(!p) 
 {
   return NULL;
 }
 return p;
}

//=============================================================================
double TupleToolBs2Dsmunu::get_MINIPCHI2(const Particle* p){  
  double minchi2 = -1 ;
  
  const RecVertex::Range PV = m_dva->primaryVertices();
  if ( !PV.empty() ){
    for (RecVertex::Range::const_iterator pv = PV.begin(); pv!=PV.end(); ++pv){
      double ip, chi2;
      
      RecVertex newPV(**pv);
      StatusCode scfit = m_pvReFitter->remove(p, &newPV);
      if(!scfit) { err()<<"ReFitter fails!"<<endreq; continue; }
      
      LHCb::VertexBase* newPVPtr = (LHCb::VertexBase*)&newPV; 
      bool test2  =  m_dist->distance ( p, newPVPtr, ip, chi2 );
      if( test2 ) {
        if ((chi2<minchi2) || (minchi2<0.)) minchi2 = chi2 ;        
      }
    }
  }
  return minchi2;
}

//========================================================================
double TupleToolBs2Dsmunu::get_IPCHI2wrtDcyVtx(const Particle* B,
                                                const Particle* P){
  
  const VertexBase *decayVtxB = B->endVertex();
  double chi2 = 0 ;
  
  if ( 0==decayVtxB )
  { chi2 = -999. ;}
  else 
  {
    double dist = 0;
    bool ok =  m_dist->distance(P , decayVtxB , dist , chi2 );
    if (!ok) return -999.;
  }
  return chi2;
}
//=============================================================================
void TupleToolBs2Dsmunu::InCone(Gaudi::XYZPoint o1,
                                   Gaudi::XYZVector p1,Gaudi::XYZPoint o2,
                                   Gaudi::XYZVector p2,
                                   Gaudi::XYZPoint& vtx, double&
                                   doca, double& angle) const
{

  Gaudi::XYZPoint rv;
  Gaudi::XYZPoint close;
  Gaudi::XYZPoint close_mu;
  bool fail(false);
  closest_point(o1,p1,o2,p2,close,close_mu,vtx, fail);
  if (fail) {
    doca =-1.;
    angle=-1.;
  }
  else {
    doca = (close-close_mu).R();
    angle = arcosine(p1,p2);
  }
}

//============================================================================
double TupleToolBs2Dsmunu::pointer (Gaudi::XYZVector vertex,
                                  Gaudi::XYZVector p, Gaudi::XYZVector p_mu) const
{
  double pt=p.Rho()+p_mu.Rho();
  Gaudi::XYZVector ptot(p+p_mu);
  double  num=ptot.R()*sin(arcosine(vertex,ptot));
  double  den=(num+pt);
  double fc = num/den;
  return fc;
}
//============================================================================
void TupleToolBs2Dsmunu::closest_point(Gaudi::XYZPoint o,Gaudi::XYZVector p,
                                     Gaudi::XYZPoint o_mu,Gaudi::XYZVector
                                     p_mu, Gaudi::XYZPoint& close1,
                                     Gaudi::XYZPoint& close2,
                                     Gaudi::XYZPoint& vertex, bool&
                                     fail) const
{
  Gaudi::XYZVector v0(o - o_mu);
  Gaudi::XYZVector v1(p.unit());
  Gaudi::XYZVector v2(p_mu.unit());
  Gaudi::XYZPoint temp1(0.,0.,0.);
  Gaudi::XYZPoint temp2(0.,0.,0.);
  fail = false;

  double  d02 = v0.Dot(v2);
  double  d21 = v2.Dot(v1);
  double  d01 = v0.Dot(v1);
  double  d22 = v2.Dot(v2);
  double  d11 = v1.Dot(v1);
  double  denom = d11 * d22 - d21 * d21;
  if (fabs(denom) <= 0.) {
    close1 = temp1;
    close2 = temp2;
    fail = true;
  }
  else {
    double numer = d02 * d21 - d01 * d22;
    double mu1 = numer / denom;
    double mu2 = (d02 + d21 * mu1) / d22;
    close1 = o+v1*mu1;
    close2 = o_mu+v2*mu2;
  }
  vertex = (close1+(close2-close1)*0.5);
}
//============================================================================
double TupleToolBs2Dsmunu::arcosine(Gaudi::XYZVector p1,
                                 Gaudi::XYZVector p2) const
{

  double num=(p1.Cross(p2)).R();
  double den=p1.R()*p2.R();
  double seno = num/den;
  double coseno = p1.Dot(p2)/den;
  double alpha = asin(fabs(seno));
  if (coseno < 0 ) {
    alpha = ROOT::Math::Pi() - alpha;
  }
  return alpha; 
}
//============================================================================
void TupleToolBs2Dsmunu::BDTvariableNames_bb_bkg_training(std::vector<std::string>& inNames) const {
  inNames.clear();
  inNames.push_back("track_minIPchi2");
  inNames.push_back("track_pvdis_mu");
  inNames.push_back("tracksvdis_mu" );
  inNames.push_back("track_doca_mu" );
  inNames.push_back("track_angle_mu");
  inNames.push_back("track_fc_mu"   );
  return; 
}
//============================================================================
//============================================================================
void TupleToolBs2Dsmunu::BDTvariableNames_Bplus_training(std::vector<std::string>& inNames) const {
  inNames.clear();
  inNames.push_back("track_minIPchi2" );
  inNames.push_back("track_eta" );
  inNames.push_back("track_phi" );
  inNames.push_back("track_pt" );
  inNames.push_back("track_MatchChi2" );
  inNames.push_back("track_pvdis_mu");
  inNames.push_back("tracksvdis_mu" );
  inNames.push_back("track_doca_mu" );
  inNames.push_back("track_angle_mu");
  inNames.push_back("track_fc_mu");
  return; 
}
//====================================================================================

