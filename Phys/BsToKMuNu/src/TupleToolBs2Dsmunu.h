#ifndef TUPLETOOLBS2DSMUNU_H 
#define TUPLETOOLBS2DSMUNU_H 1

// from Gaudi
#include "DecayTreeTupleBase/TupleToolBase.h"
#include "Kernel/IParticleTupleTool.h" 

#include "Kernel/IParticleDescendants.h"
#include <Kernel/IDVAlgorithm.h>
#include <Kernel/GetIDVAlgorithm.h>

#include "ITaggingUtilsChecker.h"
#include "Kernel/IVertexFit.h"



class IDVAlgorithm;
class IDistanceCalculator;
class IPVReFitter;
class IBTaggingTool;
class IVertexFit;
// from local

/** @todo write doxygen comments
 */

class TupleToolBs2Dsmunu : public TupleToolBase, virtual public IParticleTupleTool {

public:
  
  /// Standard constructor
  TupleToolBs2Dsmunu( const std::string& type, 
                       const std::string& name,
                       const IInterface* parent);
  
  virtual ~TupleToolBs2Dsmunu( ){}; ///< Destructor
  
  virtual StatusCode initialize();
  
  virtual StatusCode fill(   const LHCb::Particle*
                             , const LHCb::Particle*
                             , const std::string&
                             , Tuples::Tuple& );
  
  
  
private:
  
  std::string m_ParticlePath;
  std::string aux_name ;
  //const IDistanceCalculator* m_dist;
  IDVAlgorithm* m_dva;
  const IDistanceCalculator* m_dist;
  IParticleDescendants* m_descend;
  ITaggingUtilsChecker* m_util;
  std::string  m_pvReFitterName;
  IPVReFitter* m_pvReFitter;
  IVertexFit *m_vtxfitter;
  
  
  const LHCb::Particle* findID(unsigned int id, LHCb::Particle::ConstVector& v, std::string s="");
  
  double get_MINIPCHI2(const LHCb::Particle* p);
  double get_IPCHI2wrtDcyVtx(const LHCb::Particle* B, const LHCb::Particle* P);
  double get_MIPDV(const LHCb::Particle* p);
  double get_D_wrt_DcyVtx(const LHCb::Particle* B, const LHCb::Particle* P);
  
  std::vector<std::string>  m_inNames_muon ;
  std::vector<std::string>  m_inNames_kaon ;
  // New BDT tunning for B+
  std::vector<std::string>  m_inNames_muon_New ;
  std::vector<std::string>  m_inNames_kaon_New ;
  //
  std::string m_inputLocation;
  // New BDT tunning for B+ 
  std::vector<double>* m_input_muon;
  std::vector<double>* m_input_kaon;
  std::vector<double>* m_input_muon_New;
  std::vector<double>* m_input_kaon_New;
  //
  IClassifierReader* m_read_BDT_muontrack;
  IClassifierReader* m_read_BDT_kaontrack;
  // New BDT tunning for B+
  IClassifierReader* m_read_BDT_muontrack_New;
  IClassifierReader* m_read_BDT_kaontrack_New;
  
  //
  void BDTvariableNames_bb_bkg_training(std::vector<std::string>& inNames ) const;
  void BDTvariableNames_Bplus_training(std::vector<std::string>& inNames ) const;
  
  void InCone(Gaudi::XYZPoint o1,
              Gaudi::XYZVector p1,
              Gaudi::XYZPoint o2,
              Gaudi::XYZVector p2,
              Gaudi::XYZPoint& vtx, double&
              doca, double& angle) const;

  double pointer (Gaudi::XYZVector vtx,
                  Gaudi::XYZVector P_tr,
                  Gaudi::XYZVector P_mu) const;

  double arcosine(Gaudi::XYZVector p1,
                  Gaudi::XYZVector p2) const;

  void closest_point(Gaudi::XYZPoint o,
                     Gaudi::XYZVector p,
                     Gaudi::XYZPoint o_mu,
                     Gaudi::XYZVector p_mu, 
                     Gaudi::XYZPoint& close1,
                     Gaudi::XYZPoint& close2,
                     Gaudi::XYZPoint& vertex, 
                     bool& fail) const ;
    
};
//===========================================================================//
#endif // USER_ISOBDT_SMALL_H
