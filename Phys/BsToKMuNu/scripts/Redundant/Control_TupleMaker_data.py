### __author__ : Basem Khanji ,  basem.khanji@cern.ch 
Year = "2012"
#######################################################################
from Gaudi.Configuration import *
#######################################################################
#
from Gaudi.Configuration import *
from DecayTreeTuple.Configuration import *
#######################################################################
from PhysSelPython.Wrappers import AutomaticData, Selection, SelectionSequence
from PhysSelPython.Wrappers import (DataOnDemand,
                                    Selection,
                                    MergedSelection,
                                    SelectionSequence)
import GaudiKernel.SystemOfUnits as Units

from Configurables import ( DaVinci, CheckPV, DecayTreeTuple, TupleToolTrigger,
                            LoKi__Hybrid__TupleTool, TupleToolTagging, TupleToolTISTOS,
                            TupleToolDecay, TupleToolNeutrinoReco , LoKi__Hybrid__EvtTupleTool ,
                            L0TriggerTisTos , TriggerTisTos , TupleToolIsoGeneric )

from Configurables import FilterDesktop , CombineParticles, TupleToolDocas
from Configurables import TupleToolEventInfo

# StdNoPIDsVeloPions are not by default in COMMONPARTICLES , you need to make them before running the tool !
# These lines stolen from here : https://lhcb-release-area.web.cern.ch/LHCb-release-area/DOC/urania/latest_doxygen/py/d0/d15/config_iso_8py_source.html otherwise I need to change the design of the TupleToolBs2Kmunu_velotrac tool ...
# MAKE loose selection :
def createVeloTracks():
    from Configurables import ChargedProtoParticleMaker, DaVinci
    veloprotos = ChargedProtoParticleMaker("ProtoPMaker")
    veloprotos.Inputs = ["Rec/Track/Best"]
    veloprotos.Output = "Rec/ProtoP/myProtoPMaker/ProtoParticles"
    DaVinci().appendToMainSequence( [ veloprotos ])
    from Configurables       import ProtoParticleCALOFilter, CombinedParticleMaker,NoPIDsParticleMaker
    from CommonParticles.Utils import trackSelector, updateDoD
    algorithm = NoPIDsParticleMaker('StdNoPIDsVeloPions',  Particle = 'pion',  )
    algorithm.Input = "Rec/ProtoP/myProtoPMaker/ProtoParticles"
    selector = trackSelector ( algorithm , trackTypes = ['Velo'] )
    locations = updateDoD ( algorithm )
    DaVinci().appendToMainSequence( [ algorithm ])

createVeloTracks()

def configIso():
    from Configurables import ChargedProtoParticleMaker, DaVinci
    Alltracksprotos = ChargedProtoParticleMaker("ProtoPMaker")
    Alltracksprotos.Inputs = ["Rec/Track/Best"]
    Alltracksprotos.Output = "Rec/ProtoP/myProtoPMaker/ProtoParticles"
    DaVinci().appendToMainSequence( [ Alltracksprotos ])
    from Configurables       import ProtoParticleCALOFilter, CombinedParticleMaker,NoPIDsParticleMaker
    from CommonParticles.Utils import trackSelector, updateDoD
    algorithm = NoPIDsParticleMaker('StdNoPIDsAlltracksPions',  Particle = 'pion' ,  )
    algorithm.Input = "Rec/ProtoP/myProtoPMaker/ProtoParticles"
    selector = trackSelector ( algorithm , trackTypes = ['Velo', 'Long' , 'Upstream'] )
    locations = updateDoD ( algorithm )
    DaVinci().appendToMainSequence( [ algorithm ])
configIso()

LongPions      = DataOnDemand( Location ='Phys/StdAllNoPIDsPions/Particles' )
UpStreamPions  = DataOnDemand( Location ='Phys/StdNoPIDsUpPions/Particles'  )
VeloPions      = DataOnDemand( Location ='Phys/StdNoPIDsVeloPions/Particles')
AllPions       = MergedSelection( "AllPions", RequiredSelections =[ LongPions , UpStreamPions , VeloPions  ] )
AllPions_seq   = SelectionSequence('AllPions_seq', TopSelection = AllPions)
DaVinci().appendToMainSequence( [ AllPions_seq ])

#==================================================================================================================
# B+ selection
# cuts
Kaon_cuts = "(MINTREE(ABSID=='K+',P)> 10000.0 *MeV ) & (MINTREE(ABSID=='K+',PT)> 800.0 *MeV)& (MINTREE(ABSID=='K+',TRGHOSTPROB)< 0.5) & (MINTREE(ABSID=='K+',PIDK-PIDpi)> 5.0 )& (MINTREE(ABSID=='K+',PIDK-PIDp)> 5.0 )& (MINTREE(ABSID=='K+',PIDK-PIDmu)> 5.0 ) & (MINTREE(ABSID=='K+',MIPCHI2DV(PRIMARY))> 16)"
Muon_cuts = "(MINTREE(ABSID=='mu+',P)> 6000.0 *MeV) & (MINTREE(ABSID=='mu+',PT)> 1500.0* MeV)& (MINTREE(ABSID=='mu+',TRGHOSTPROB) < 0.35)& (MINTREE(ABSID=='mu+',PIDmu-PIDpi)> 3.0 )& (MINTREE(ABSID=='mu+',PIDmu-PIDp)> 0.0 )& (MINTREE(ABSID=='mu+',PIDmu-PIDK)> 0.0 )& (MINTREE(ABSID=='mu+',MIPCHI2DV(PRIMARY))> 12)"
Jpsi_cuts ="( ( (BPVDLS>3) | (BPVDLS<-3) ) ) &  ( (MM > 2996.916) & (MM < 3196.916) ) & (VFASPF(VCHI2PDOF)< 20.0)"
Bu_Mothercuts  = "(VFASPF(VCHI2/VDOF)< 20.0) & (BPVDIRA> 0.994) & (BPVVDCHI2 >120.0) & (M>5100) & (M<6000)"
# call particles
Kaon_particles  = DataOnDemand( Location ='Phys/StdLooseKaons/Particles')
Jpsi_particles  = DataOnDemand( Location ='/Event/Dimuon/Phys/FullDSTDiMuonJpsi2MuMuDetachedLine/Particles')
# Filter
Kaon_Fltr = FilterDesktop("Phis_Fltr", Code = Kaon_cuts                                                          )
Kaon_sel  = Selection("Kaon_sel"     , Algorithm     = Kaon_Fltr          , RequiredSelections = [Kaon_particles])
Jpsi_Fltr = FilterDesktop("Jpsi_Fltr", Code = Muon_cuts +" & " + Jpsi_cuts                                       )
Jpsi_sel  = Selection("Jpsi_sel"     , Algorithm     = Jpsi_Fltr          , RequiredSelections = [Jpsi_particles])
# make the B
Bu2JpsiK    = CombineParticles("Bu2JpsiK",
                               DecayDescriptors = [ '[B+ -> J/psi(1S) K+]cc' ] ,
                               CombinationCut   = "(AM> 500.0*MeV) & (AM<6000.0*MeV)" ,
                               MotherCut        = Bu_Mothercuts
                               )
Bu2JpsiK.MotherCut      = "(VFASPF(VCHI2/VDOF)< 25) & (BPVDIRA> 0.95)"
Bu2JpsiK_Sel            = Selection("Bu2JpsiK_Sel",
                                    Algorithm = Bu2JpsiK ,
                                    RequiredSelections = [ Jpsi_sel, Kaon_sel ] )
SeqBu2JpsiK = SelectionSequence('SeqBu2JpsiK', TopSelection = Bu2JpsiK_Sel)
SeqBu2JpsiK.outputLevel = 6
#==================================================================================================================

#############################################################
# DecayTreeTuple
#
checkPV = CheckPV("CheckForOnePV")
checkPV.MinPVs = 1
print checkPV

##############################################################################
seqBsKmunu_gaudi           = GaudiSequencer('seqBsKmunu_gaudi')
seqBsKmunu_gaudi.Members       +=  [SeqBu2JpsiK.sequence()]
seqBsKmunu_gaudi.ModeOR         =  True
seqBsKmunu_gaudi.ShortCircuit   =  False

TupleSeq        =    GaudiSequencer('TupleSeq')
JpsiK           =    DecayTreeTuple("Bu2JpsiKTuple")
JpsiK.Inputs    =  [ SeqBu2JpsiK.outputLocation() ]
JpsiK.ToolList +=  [
    "TupleToolGeometry"
    ,"TupleToolKinematic"
    , "TupleToolPrimaries"
    , "TupleToolTrackInfo"
    , "LoKi::Hybrid::TupleTool/LoKiTool"
    ]
JpsiK.ReFitPVs = True # causing a problem ?
JpsiK.OutputLevel = 6
JpsiK.Decay    = '[B- -> ^(J/psi(1S)-> ^mu+ ^mu-) ^K- ]CC'
JpsiK.Branches = {
    "muon_p"   : '[B- ->  (J/psi(1S)-> ^mu+  mu-)  K- ]CC'
    ,"muon_m"  : '[B- ->  (J/psi(1S)->  mu+ ^mu-)  K- ]CC'
    ,"kaon_m"  : '[B- ->  (J/psi(1S)->  mu+  mu-) ^K- ]CC'
    ,"Jpsi"    : '[B- -> ^(J/psi(1S)->  mu+  mu-)  K- ]CC'
    ,"Bu"      : '[B- ->  (J/psi(1S)->  mu+  mu-)  K- ]CC'
    }
JpsiK.addTool(TupleToolDecay, name="Bu"  )
JpsiK.addTool(TupleToolDecay, name="muon_p")
JpsiK.addTool(TupleToolDecay, name="muon_m")
JpsiK.addTool(TupleToolDecay, name="kaon_m")
JpsiK.addTool(TupleToolDecay, name="Jpsi")

TupleToolIsoGeneric              =  JpsiK.addTupleTool("TupleToolIsoGeneric")
TupleToolIsoGeneric.ParticlePath =  AllPions_seq.outputLocation()
TupleToolIsoGeneric.VerboseMode = True
LoKiTool_noniso = LoKi__Hybrid__TupleTool("LoKiTool_noniso")
LoKiTool_noniso.Variables    = { "ETA" : "ETA" , "PHI" : "PHI" , "TRTYPE" : "TRTYPE"}
TupleToolIsoGeneric.ToolList += ["LoKi::Hybrid::TupleTool/LoKiTool_noniso"]
TupleToolIsoGeneric.addTool(LoKiTool_noniso)
TupleToolIsoGeneric.OutputLevel = 6

budocas = JpsiK.Bu.addTupleTool('TupleToolDocas')
budocas.Name       = [ "Kmu_OS", "Kmu_SS", "mumu" ]
budocas.Location1  = [ "[B- ->  (J/psi(1S)-> mu+  mu-)  ^K-]CC",
                       "[B- ->  (J/psi(1S)-> mu+  mu-)  ^K-]CC",
                       "[B- ->  (J/psi(1S)-> mu+  ^mu-)  K-]CC"]
budocas.Location2  = [ "[B- ->  (J/psi(1S)-> ^mu+  mu-)  K-]CC",
                       "[B- ->  (J/psi(1S)-> mu+  ^mu-)  K-]CC",
                       "[B- ->  (J/psi(1S)-> ^mu+  mu-)  K-]CC"]

LoKiVariables_Bu = JpsiK.Bu.addTupleTool("LoKi::Hybrid::TupleTool/LoKiVariables_Bu")
LoKiVariables_Bu.Variables = {
    "ETA"              : "ETA",
    "PHI"              : "PHI",
    "DOCA"             : "DOCA(1,2)",
    "DTF_TAU"          : "DTF_CTAU( 0, True )/0.299792458",
    "DTF_CTAUS"        : "DTF_CTAUSIGNIFICANCE( 0, True )",
    "DTF_CHI2NDOF"     : "DTF_CHI2NDOF( True )",
    "M_JpsiConstr"     : "DTF_FUN ( M , True , 'J/psi(1S)' )" ,
    "DTF_VCHI2NDOF"    : "DTF_FUN ( VFASPF(VCHI2/VDOF) , True  )",
    "FD_CHI2_LOKI"     : "BPVVDCHI2",
    "VCHI2_LOKI"       : "VFASPF(VCHI2/VDOF)",
    "FD_S"             : "BPVDLS",
    "cosTheta1_star"   : "LV01",
    "cosTheta2_star"   : "LV02",
    }

LoKiVariables_K = JpsiK.kaon_m.addTupleTool("LoKi::Hybrid::TupleTool/LoKiVariables_K")
LoKiVariables_K.Variables = { "MIPCHI2DV_PV" : "MIPCHI2DV(PRIMARY)" }
LoKiVariables_Mu = JpsiK.muon_p.addTupleTool("LoKi::Hybrid::TupleTool/LoKiVariables_Mu")
LoKiVariables_Mu.Variables = { "MIPCHI2DV_PV" : "MIPCHI2DV(PRIMARY)" }
############################################################################
TupleToolTISTOS = TupleToolTISTOS('TupleToolTISTOS')
TupleToolTISTOS.addTool(L0TriggerTisTos())
TupleToolTISTOS.addTool(TriggerTisTos())
TupleToolTISTOS.TriggerList=[
     "L0MuonDecision",
     "L0DiMuonDecision",
     "Hlt2TopoMu2BodyBBDTDecision",
     "Hlt2SingleMuonDecision",
     "Hlt2SingleMuonLowPTDecision"
     ]
TupleToolTISTOS.VerboseL0   = True
TupleToolTISTOS.VerboseHlt1 = True
TupleToolTISTOS.VerboseHlt2 = True
TupleToolTISTOS.TUS         = True
TupleToolTISTOS.TPS         = True
JpsiK.Bu.addTool(TupleToolTISTOS , name = 'TupleToolTISTOS' )
JpsiK.Bu.ToolList   += [ "TupleToolTISTOS"]
JpsiK.kaon_m.addTool(TupleToolTISTOS , name = 'TupleToolTISTOS' )
JpsiK.kaon_m.ToolList   += [ "TupleToolTISTOS"]
JpsiK.muon_p.addTool(TupleToolTISTOS , name = 'TupleToolTISTOS' )
JpsiK.muon_p.ToolList   += [ "TupleToolTISTOS"]

from Configurables import TupleToolConeIsolation
JpsiK.kaon_m.addTupleTool("TupleToolConeIsolation")
JpsiK.kaon_m.TupleToolConeIsolation.FillAsymmetry     = True
JpsiK.kaon_m.TupleToolConeIsolation.FillDeltas        = True
JpsiK.kaon_m.TupleToolConeIsolation.FillComponents    = True
JpsiK.kaon_m.TupleToolConeIsolation.MinConeSize       = 0.5
JpsiK.kaon_m.TupleToolConeIsolation.SizeStep          = 0.5
JpsiK.kaon_m.TupleToolConeIsolation.MaxConeSize       = 2.0
JpsiK.kaon_m.TupleToolConeIsolation.FillPi0Info       = True
JpsiK.kaon_m.TupleToolConeIsolation.FillMergedPi0Info = True

JpsiK.muon_p.addTupleTool("TupleToolConeIsolation")
JpsiK.muon_p.TupleToolConeIsolation.FillAsymmetry     = True
JpsiK.muon_p.TupleToolConeIsolation.FillDeltas        = True
JpsiK.muon_p.TupleToolConeIsolation.FillComponents    = True
JpsiK.muon_p.TupleToolConeIsolation.MinConeSize       = 0.5
JpsiK.muon_p.TupleToolConeIsolation.SizeStep          = 0.5
JpsiK.muon_p.TupleToolConeIsolation.MaxConeSize       = 2.0

JpsiK.ToolList +=  [
    "TupleToolPropertime"
    , "TupleToolRecoStats"
    , "TupleToolSLTools"
    , "TupleToolTrackPosition"
    ]

#===========================================================================================================
#==================================================================================================================
Kaon_cuts = "(MINTREE(ABSID=='K+',P)> 10000.0 *MeV ) & (MINTREE(ABSID=='K+',PT)> 800.0 *MeV) & (MINTREE(ABSID=='K+',TRGHOSTPROB)< 0.5) & (MINTREE(ABSID=='K+',PIDK-PIDpi)> 5.0 )&  (MINTREE(ABSID=='K+',PIDK-PIDp)> 5.0 )& (MINTREE(ABSID=='K+',PIDK-PIDmu)> 5.0 ) & (MINTREE(ABSID=='K+',MIPCHI2DV(PRIMARY))> 16)"
phi_cuts  = "(VFASPF(VCHI2) < 20.0) & (M> 980.0*MeV) & (M<1040.0*MeV)"
Muon_cuts = "(MINTREE(ABSID=='mu+',P)> 6000.0 *MeV) & (MINTREE(ABSID=='mu+',PT)> 1500.0* MeV)& (MINTREE(ABSID=='mu+',TRGHOSTPROB) < 0.35)& (MINTREE(ABSID=='mu+',PIDmu-PIDpi)> 3.0 )&  (MINTREE(ABSID=='mu+',PIDmu-PIDp)> 0.0 )& (MINTREE(ABSID=='mu+',PIDmu-PIDK)> 0.0 )& (MINTREE(ABSID=='mu+',MIPCHI2DV(PRIMARY))> 12)"
Jpsi_cuts = "( ( (BPVDLS>3) | (BPVDLS<-3) ) ) &  ( (MM > 2996.916) & (MM < 3196.916) ) & (VFASPF(VCHI2PDOF)< 20.0)"
Bs_Mothercuts  = "(VFASPF(VCHI2/VDOF)< 20.0) & (BPVDIRA> 0.994) & (BPVVDCHI2 >120.0)"# & (M>4900) & (M<5800)"
#
phi_particles   = DataOnDemand( Location ='Phys/StdLooseDetachedPhi2KK/Particles')
Jpsi_particles  = DataOnDemand( Location ='/Event/Dimuon/Phys/FullDSTDiMuonJpsi2MuMuDetachedLine/Particles')
#
Phi_Fltr  = FilterDesktop("Phis_Fltr", Code = Kaon_cuts +" & " + phi_cuts                                        )
Phi_sel   = Selection("Phi_sel"      , Algorithm     = Phi_Fltr           , RequiredSelections = [phi_particles] )
#
Bs2JpsiPhi    = CombineParticles("Bs2JpsiPhi",
                                 DecayDescriptors = [ ' B_s0 -> J/psi(1S) phi(1020)' ] ,
                                 CombinationCut   = "(AM> 500.0*MeV) & (AM<6000.0*MeV)" ,
                                 MotherCut        = Bs_Mothercuts
                                 )
Bs2JpsiPhi_Sel     = Selection("Bs2JpsiPhi_Sel",
                               Algorithm = Bs2JpsiPhi ,
                               RequiredSelections = [ Jpsi_particles , Phi_sel ]
                               )
SeqBs2JpsiPhi = SelectionSequence('SeqBs2JpsiPhi', TopSelection = Bs2JpsiPhi_Sel)
SeqBs2JpsiPhi.outputLevel = 6
#==================================================================================================================
seqBsKmunu_gaudi.Members       +=  [SeqBs2JpsiPhi.sequence()]
seqBsKmunu_gaudi.ModeOR         =  True
seqBsKmunu_gaudi.ShortCircuit   =  False
#===========================================================================================================
JpsiPhi          =  JpsiK.clone("Bs2JpsiPhiTuple")
JpsiPhi.Inputs   =  [ SeqBs2JpsiPhi.outputLocation() ]

JpsiPhi.Decay    = '[B_s0 -> ^(J/psi(1S)-> ^mu+ ^mu-) ^(phi(1020) -> ^K+ ^K-)]CC'
JpsiPhi.Branches = {
    "muon_p"   : "[B_s0 ->  (J/psi(1S)-> ^mu+  mu-)  (phi(1020) ->  K+  K-)]CC"
    ,"muon_m"  : "[B_s0 ->  (J/psi(1S)->  mu+ ^mu-)  (phi(1020) ->  K+  K-)]CC"
    ,"kaon_p"  : "[B_s0 ->  (J/psi(1S)->  mu+  mu-)  (phi(1020) -> ^K+  K-)]CC"
    ,"kaon_m"  : "[B_s0 ->  (J/psi(1S)->  mu+  mu-)  (phi(1020) ->  K+ ^K-)]CC"
    ,"Jpsi"    : "[B_s0 -> ^(J/psi(1S)->  mu+  mu-)  (phi(1020) ->  K+  K-)]CC"
    ,"phi"     : "[B_s0 ->  (J/psi(1S)->  mu+  mu-) ^(phi(1020) ->  K+  K-)]CC"
    ,"Bs"      : "[B_s0 ->  (J/psi(1S)->  mu+  mu-)  (phi(1020) ->  K+  K-)]CC"
    }
JpsiPhi.addTool(TupleToolDecay, name="Bs"  )
JpsiPhi.addTool(TupleToolDecay, name="kaon_m")
JpsiPhi.addTool(TupleToolDecay, name="muon_p")
JpsiPhi.addTool(TupleToolDecay, name="muon_m")
JpsiPhi.addTool(TupleToolDecay, name="kaon_p")
JpsiPhi.addTool(TupleToolDecay, name="Jpsi")
JpsiPhi.addTool(TupleToolDecay, name="phi" )

bsdocas = JpsiPhi.Bs.addTupleTool('TupleToolDocas')
bsdocas.Name       = [ "Kmu_pm", "Kmu_mp", "mumu", "KK" ]
bsdocas.Location1  = [ "[B_s0 ->  (J/psi(1S)->  mu+  mu-)  (phi(1020) -> ^K+  K-)]CC",
                       "[B_s0 ->  (J/psi(1S)->  mu+  mu-)  (phi(1020) ->  K+ ^K-)]CC",
                       "[B_s0 ->  (J/psi(1S)-> ^mu+  mu-)  (phi(1020) ->  K+  K-)]CC",
                       "[B_s0 ->  (J/psi(1S)->  mu+  mu-)  (phi(1020) -> ^K+  K-)]CC"]
bsdocas.Location2  = [ "[B_s0 ->  (J/psi(1S)->  mu+ ^mu-)  (phi(1020) ->  K+  K-)]CC",
                       "[B_s0 ->  (J/psi(1S)-> ^mu+  mu-)  (phi(1020) ->  K+  K-)]CC",
                       "[B_s0 ->  (J/psi(1S)->  mu+ ^mu-)  (phi(1020) ->  K+  K-)]CC",
                       "[B_s0 ->  (J/psi(1S)->  mu+  mu-)  (phi(1020) ->  K+ ^K-)]CC"]

LoKiVariables_Bs = JpsiPhi.Bs.addTupleTool("LoKi::Hybrid::TupleTool/LoKiVariables_Bs")
LoKiVariables_Bs.Variables = LoKiVariables_Bu.Variables
JpsiPhi.Bs.addTool(TupleToolTISTOS , name = 'TupleToolTISTOS' )
JpsiPhi.Bs.ToolList   += [ "TupleToolTISTOS"]
#===========================================================================================================
## Fill the Tupleseq
TupleSeq.Members        += [ JpsiK ]
TupleSeq.ModeOR          = True
TupleSeq.ShortCircuit    = False

TupleSeq.Members        += [ JpsiPhi ]
TupleSeq.ModeOR          = True
TupleSeq.ShortCircuit    = False
######################################################################
seqBsKmunu_gaudi.Members       +=  [TupleSeq]
seqBsKmunu_gaudi.ModeOR         =  True
seqBsKmunu_gaudi.ShortCircuit   =  False
######################################################################
#
# Event Tuple
#
from Configurables import EventTuple , TupleToolEventInfo
etuple = EventTuple()
########################################################################
#
# DaVinci settings
#
configIso()
from Configurables import DaVinci
#DaVinci().UserAlgorithms += [etuple, mysel , tuple]
DaVinci().UserAlgorithms += [ checkPV , seqBsKmunu_gaudi ]
#DaVinci().UserAlgorithms += [ etuple , SeqBs2KmuNu.sequence() , LooseTuple]
DaVinci().MainOptions  = ""
DaVinci().EvtMax       = 1000
DaVinci().PrintFreq    = 25000
#DaVinci().SkipEvents   = 25000
DaVinci().DataType     = Year
# data:
from Configurables import CondDB
CondDB().UseLatestTags = [Year]
DaVinci().Lumi=True
# MC
DaVinci().Simulation = False
#######################################################################
#
DaVinci().TupleFile = "DTT_data.root"

#importOptions("/afs/cern.ch/user/b/bkhanji/private/DVana/Bs2KMuNu/Dimuon_12a.py")
#importOptions("/afs/cern.ch/user/b/bkhanji/private/DVana/Bs2KMuNu/Bs_KmuNu_data_2012.py")
#importOptions("/afs/cern.ch/user/a/acontu/public/S21Incremental_dsts/PFNSemileptonic.dst.py")
