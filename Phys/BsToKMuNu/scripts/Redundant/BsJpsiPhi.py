#-- GAUDI jobOptions generated on Mon Oct  2 15:12:21 2017
#-- Contains event types : 
#--   13144001 - 3 files - 55705 events - 11.49 GBytes


#--  Extra information about the data processing phases:


#--  Processing Pass Step-124629 

#--  StepId : 124629 
#--  StepName : Merge14 for Sim08 
#--  ApplicationName : LHCb 
#--  ApplicationVersion : v35r4 
#--  OptionFiles : $APPCONFIGOPTS/Merging/CopyDST.py 
#--  DDDB : None 
#--  CONDDB : None 
#--  ExtraPackages : AppConfig.v3r164 
#--  Visible : N 

from Gaudi.Configuration import * 
from GaudiConf import IOHelper
IOHelper('ROOT').inputFiles(['LFN:/lhcb/MC/2012/ALLSTREAMS.DST/00036952/0000/00036952_00000001_1.allstreams.dst',
'LFN:/lhcb/MC/2012/ALLSTREAMS.DST/00036952/0000/00036952_00000002_1.allstreams.dst',
'LFN:/lhcb/MC/2012/ALLSTREAMS.DST/00036952/0000/00036952_00000003_1.allstreams.dst'
], clear=True)
FileCatalog().Catalogs += [ 'xmlcatalog_file:BsJpsiPhi.xml' ]
