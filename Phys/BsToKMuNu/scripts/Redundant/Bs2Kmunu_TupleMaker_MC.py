### __author__ : Basem Khanji ,  basem.khanji@cern.ch 
#######################################################################
import os , sys
os.getcwd()
sys.path.append(os.getcwd())
from JobName import *

from Gaudi.Configuration import *
#######################################################################
from DecayTreeTuple.Configuration import *
#######################################################################
from PhysSelPython.Wrappers import AutomaticData, DataOnDemand, Selection, SelectionSequence, MergedSelection
import GaudiKernel.SystemOfUnits as Units

from Configurables import DaVinci, DecayTreeTuple, TupleToolTrigger, LoKi__Hybrid__TupleTool, TupleToolTagging, TupleToolTISTOS, TupleToolDecay, TupleToolNeutrinoReco , TupleToolBs2Kmunu , TupleToolBs2Kmunu_UpStream , TupleToolBs2Kmunu_velotracks , LoKi__Hybrid__EvtTupleTool, MCTupleToolInteractions, L0TriggerTisTos , TriggerTisTos

from Configurables import FilterDesktop , CombineParticles
from Configurables import TupleToolEventInfo
# StdNoPIDsVeloPions are not by default in COMMONPARTICLES , you need to make them before running the tool !
# These lines are stolen from here : https://lhcb-release-area.web.cern.ch/LHCb-release-area/DOC/urania/latest_doxygen/py/d0/d15/config_iso_8py_source.html otherwise I need to change the design of the TupleToolBs2Kmunu_velotrac tool ...
################################################################################################################################                        
# Re-Run the Stripping :
# Define here the pion cuts :
from StrippingConf.Configuration import StrippingConf, StrippingStream
from StrippingSettings.Utils import strippingConfiguration
from StrippingArchive.Utils import buildStreams
from StrippingArchive import strippingArchive
from Configurables import EventNodeKiller
stripping             = 'stripping24'
config                = strippingConfiguration(stripping)
archive               = strippingArchive(stripping)
streams               = buildStreams(stripping=config, archive=archive)
eventNodeKiller       = EventNodeKiller('Stripkiller')
eventNodeKiller.Nodes = [ '/Event/AllStreams', '/Event/Strip' ]
DaVinci().appendToMainSequence( [ eventNodeKiller ] )
MyStream = StrippingStream("MyStream")
#MyLines = [ 'StrippingB2XuMuNuBs2KLine']

MyLines = [ 'StrippingB2XuMuNuBs2K_FakeKMuLine' , 'StrippingB2XuMuNuBs2KLine']
Lines_to_run = []
for stream in streams:
    for line in stream.lines:
        if line.name() in MyLines:
            print line.name()
            if (line.name() in 'StrippingB2XuMuNuBs2K_FakeKMuLine' ) :
                print '============================================================='
                print 'Found the line : ' , line
                line._prescale = 0.5
                if ('Jpsi' in job_name ):
                    line._prescale = 0.1
            Lines_to_run += [line]

MyStream.appendLines( Lines_to_run)

from Configurables import ProcStatusCheck
filterBadEvents = ProcStatusCheck()
sc = StrippingConf( Streams = [ MyStream ],
                    MaxCandidates = 2000,
                    AcceptBadEvents = False,
                    BadEventSelection = ProcStatusCheck(),
                    TESPrefix = 'StripTest' )
#            
# ====================================================================== 
def createVeloTracks():
    # still to add to thia function: MC associators
    from Configurables import ChargedProtoParticleMaker, DaVinci
    veloprotos = ChargedProtoParticleMaker("ProtoPMaker")
    veloprotos.Inputs = ["Rec/Track/Best"]
    veloprotos.Output = "Rec/ProtoP/myProtoPMaker/ProtoParticles"
    DaVinci().appendToMainSequence( [ veloprotos ])
    from Configurables       import ProtoParticleCALOFilter, CombinedParticleMaker,NoPIDsParticleMaker
    from CommonParticles.Utils import trackSelector, updateDoD
    algorithm = NoPIDsParticleMaker('StdNoPIDsVeloPions',  Particle = 'pion',  )
    algorithm.Input = "Rec/ProtoP/myProtoPMaker/ProtoParticles"
    selector = trackSelector ( algorithm , trackTypes = ['Velo'] )
    locations = updateDoD ( algorithm )
    DaVinci().appendToMainSequence( [ algorithm ])
    
createVeloTracks()        
        
def configIso():
    from Configurables import ChargedProtoParticleMaker, DaVinci
    Alltracksprotos = ChargedProtoParticleMaker("ProtoPMaker")
    Alltracksprotos.Inputs = ["Rec/Track/Best"]
    Alltracksprotos.Output = "Rec/ProtoP/myProtoPMaker/ProtoParticles"
    DaVinci().appendToMainSequence( [ Alltracksprotos ])
    from Configurables       import ProtoParticleCALOFilter, CombinedParticleMaker,NoPIDsParticleMaker
    from CommonParticles.Utils import trackSelector, updateDoD
    algorithm = NoPIDsParticleMaker('StdNoPIDsAlltracksPions',  Particle = 'pion' ,  )
    algorithm.Input = "Rec/ProtoP/myProtoPMaker/ProtoParticles"
    selector = trackSelector ( algorithm , trackTypes = ['Velo', 'Long' , 'Upstream'] )
    locations = updateDoD ( algorithm )
    DaVinci().appendToMainSequence( [ algorithm ]) 
configIso()

LongPions      = DataOnDemand( Location ='Phys/StdAllNoPIDsPions/Particles' )
UpStreamPions  = DataOnDemand( Location ='Phys/StdNoPIDsUpPions/Particles'  )
VeloPions      = DataOnDemand( Location ='Phys/StdNoPIDsVeloPions/Particles')
AllPions       = MergedSelection( "AllPions", RequiredSelections =[ LongPions , UpStreamPions , VeloPions  ] )
AllPions_seq   = SelectionSequence('AllPions_seq', TopSelection = AllPions)
DaVinci().appendToMainSequence( [ AllPions_seq ])
#############################################################


#############################################################
# DecayTreeTuple 
TupleSeq        = GaudiSequencer('TupleSeq')
tuple           = DecayTreeTuple("Bs2KmuNuTuple")
tuple.Inputs    = ['Phys/B2XuMuNuBs2KLine/Particles']
tuple.ToolList += [
    "TupleToolGeometry"
    ,"TupleToolKinematic"
    , "TupleToolPrimaries"
    , "TupleToolMCBackgroundInfo"
    , "TupleToolTrackInfo"
    , "LoKi::Hybrid::TupleTool/LoKiTool"
    ]

tuple.ReFitPVs = True
tuple.OutputLevel = 6

tuple.Decay    = "[B_s0 -> ^K- ^mu+]CC"
tuple.Branches = {
    "muon_p"   : "[B_s0 ->  K- ^mu+]CC"
    ,"kaon_m"  : "[B_s0 -> ^K-  mu+]CC" 
    ,"Bs"      : "[B_s0 ->  K-  mu+]CC" 
     }

tuple.addTool(TupleToolDecay, name="Bs")
tuple.addTool(TupleToolDecay, name="kaon_m")
tuple.addTool(TupleToolDecay, name="muon_p")

tuple.addTupleTool("TupleToolBs2Kmunu")
tuple.TupleToolBs2Kmunu.ParticlePath =  AllPions_seq.outputLocation() 
#'/Event/Phys/StdNoPIDsAlltracksPions/Particles'
#tuple.TupleToolBs2Kmunu.MCflag = False
tuple.TupleToolBs2Kmunu.OutputLevel = 6
MCTruth_noniso = TupleToolMCTruth('MCTruth_noniso')
#MCTruth_noniso.ToolList += ["MCTupleToolHierarchy" ]
#MCTruth_noniso.addTupleTool("MCTupleToolReconstructed")
#MCTruth_noniso.addTupleTool("MCTupleToolHierarchy")
#LoKiTool_noniso = LoKi__Hybrid__TupleTool("LoKiTool_noniso")
#LoKiTool_noniso.Variables = {
#    "ETA"              : "ETA",
#    "PHI"              : "PHI"
#    }
tuple.TupleToolBs2Kmunu.ToolList += ["TupleToolMCTruth" , "TupleToolTrackInfo" ]

LoKiVariables = tuple.Bs.addTupleTool("LoKi::Hybrid::TupleTool/LoKiVariables")
LoKiVariables.Preambulo = [
     "from LoKiPhysMC.decorators import *",
     "from LoKiPhysMC.functions import mcMatch"
    ]

LoKiVariables.Variables = {
    "ETA"              : "ETA",
    "PHI"              : "PHI",
    "DOCA"             : "DOCA(1,2)",
    "DTF_TAU"          : "DTF_CTAU( 0, True )/0.299792458",
    "DTF_CTAUS"        : "DTF_CTAUSIGNIFICANCE( 0, True )",
    "DTF_CHI2NDOF"     : "DTF_CHI2NDOF( True )",
    "FD_CHI2_LOKI"     : "BPVVDCHI2",
    "VCHI2_LOKI"       : "VFASPF(VCHI2/VDOF)",
    "FD_S"             : "BPVDLS",
    "cosTheta1_star"   : "LV01",
    "cosTheta2_star"   : "LV02",
    #"COSPOL"          : "COSPOL",
    #"SINCHI"          :  "SINCHI" ,
    #"COSCHI"  :  "COSCHI" ,
    #"COSTHETATR"  :  "COSTHETATR" ,
    #"SINPHITR"  :  "SINPHITR" ,
    #"COSPHITR"  :  "COSPHITR" ,
    #"MINANGLE"  :  "MINANGLE()" ,
    "TruthMatched"     : "switch( mcMatch ('[ [B_s~0]cc ==> K+ mu- Neutrino ]CC', 1 )  , 1 , 0 )"
    }

LoKiVariables_K = tuple.kaon_m.addTupleTool("LoKi::Hybrid::TupleTool/LoKiVariables_K")
LoKiVariables_K.Variables = { "MIPCHI2DV_PV" : "MIPCHI2DV(PRIMARY)" }
LoKiVariables_Mu = tuple.muon_p.addTupleTool("LoKi::Hybrid::TupleTool/LoKiVariables_Mu")
LoKiVariables_Mu.Variables = { "MIPCHI2DV_PV" : "MIPCHI2DV(PRIMARY)" }
############################################################################
TupleToolTISTOS = TupleToolTISTOS('TupleToolTISTOS')
TupleToolTISTOS.addTool(L0TriggerTisTos())
TupleToolTISTOS.addTool(TriggerTisTos())
TupleToolTISTOS.TriggerList=[
    "L0MuonDecision",
    "L0DiMuonDecision",        
    "Hlt2TopoMu2BodyBBDTDecision",
    "Hlt2SingleMuonDecision",
    "Hlt2SingleMuonLowPTDecision"
    #"L0HadronDecision",
    #"Hlt2SingleMuonHighPTDecision",
    #"Hlt2Topo2BodySimpleDecision",
    #"Hlt2Topo2BodyBBDTDecision"
    ]

TupleToolTISTOS.VerboseL0   = True
TupleToolTISTOS.VerboseHlt1 = True
TupleToolTISTOS.VerboseHlt2 = True
TupleToolTISTOS.TUS         = True
TupleToolTISTOS.TPS         = True

tuple.Bs.addTool(TupleToolTISTOS , name = 'TupleToolTISTOS' )
tuple.Bs.ToolList   += [ "TupleToolTISTOS"]

tuple.kaon_m.addTool(TupleToolTISTOS , name = 'TupleToolTISTOS' )
tuple.kaon_m.ToolList   += [ "TupleToolTISTOS"]

tuple.muon_p.addTool(TupleToolTISTOS , name = 'TupleToolTISTOS' )
tuple.muon_p.ToolList   += [ "TupleToolTISTOS"]

MCTruth = tuple.addTupleTool("TupleToolMCTruth")
MCTruth.addTupleTool("MCTupleToolKinematic")

MCTruth_k = tuple.kaon_m.addTupleTool("TupleToolMCTruth")
MCTruth_k.addTupleTool("MCTupleToolReconstructed")
MCTruth_k.addTupleTool("MCTupleToolKinematic")

MCTruth_mu = tuple.muon_p.addTupleTool("TupleToolMCTruth")
MCTruth_mu.addTupleTool("MCTupleToolReconstructed")
MCTruth_mu.addTupleTool("MCTupleToolKinematic")

from Configurables import TupleToolConeIsolation
tuple.kaon_m.addTupleTool("TupleToolConeIsolation")
tuple.kaon_m.TupleToolConeIsolation.FillAsymmetry     = True
tuple.kaon_m.TupleToolConeIsolation.FillDeltas        = True
tuple.kaon_m.TupleToolConeIsolation.FillComponents    = True
tuple.kaon_m.TupleToolConeIsolation.MinConeSize       = 0.5
tuple.kaon_m.TupleToolConeIsolation.SizeStep          = 0.5
tuple.kaon_m.TupleToolConeIsolation.MaxConeSize       = 2.0
tuple.kaon_m.TupleToolConeIsolation.FillPi0Info       = True
tuple.kaon_m.TupleToolConeIsolation.FillMergedPi0Info = True

tuple.muon_p.addTupleTool("TupleToolConeIsolation")
tuple.muon_p.TupleToolConeIsolation.FillAsymmetry     = True
tuple.muon_p.TupleToolConeIsolation.FillDeltas        = True
tuple.muon_p.TupleToolConeIsolation.FillComponents    = True
tuple.muon_p.TupleToolConeIsolation.MinConeSize       = 0.5
tuple.muon_p.TupleToolConeIsolation.SizeStep          = 0.5
tuple.muon_p.TupleToolConeIsolation.MaxConeSize       = 2.0

tuple.ToolList +=  [
    "TupleToolPropertime"
    , "TupleToolRecoStats"
    , "TupleToolSLTools"
    , "TupleToolTrackPosition"
    , "TupleToolGeneration"
    ]

NOPIDTuple = tuple.clone("NoPIDTuple")
NOPIDTuple.Inputs    = ['Phys/B2XuMuNuBs2K_FakeKMuLine/Particles']

TupleSeq.Members        += [ tuple ]
TupleSeq.ModeOR          = True
TupleSeq.ShortCircuit    = False

TupleSeq.Members        += [ NOPIDTuple ]
TupleSeq.ModeOR          = True
TupleSeq.ShortCircuit    = False
######################################################################
#seqBsKmunu_gaudi.Members       +=  [TupleSeq]
#seqBsKmunu_gaudi.ModeOR         =  True
#seqBsKmunu_gaudi.ShortCircuit   =  False
#######################################################################
# Event Tuple
from Configurables import EventTuple , TupleToolEventInfo
etuple = EventTuple()
########################################################################
#
# DaVinci settings
from Configurables import DaVinci

DaVinci().appendToMainSequence( [ sc.sequence() ] )
DaVinci().UserAlgorithms += [ etuple , TupleSeq ]
DaVinci().MainOptions   = "" 
DaVinci().EvtMax        = -1
DaVinci().PrintFreq     = 1000
DaVinci().DataType      = '2012'
# data 2012:
DaVinci().DDDBtag       =  "dddb-20120831" # MC12
DaVinci().CondDBtag     =  "sim-20121025-vc-mu100" # MC12
#from Configurables import CondDB
#CondDB().UseLatestTags = ["2012"]
# MC
DaVinci().Simulation = True
DaVinci().Lumi       = False        
#######################################################################
#
DaVinci().TupleFile = "DTT_Bs2K2stplusMuNu_kpi0.root"

# Signal
#importOptions("/afs/cern.ch/user/b/bkhanji/private/DVana/Bs2KMuNu/Bs_KmuNu_MC.py")
# Control channel
#importOptions("/afs/cern.ch/user/b/bkhanji/private/DVana/Bs2KMuNu/Bs_DsMuNu_MC12.py")
# dangerous bkg B+ -> Jpisi K+ 
#importOptions("/afs/cern.ch/user/b/bkhanji/private/DVana/Bs2KMuNu/Bu_JpsiK_MC12.py")
# Dangerous BKG Bs-> Jpsi Phi
#importOptions("/afs/cern.ch/user/b/bkhanji/private/DVana/Bs2KMuNu/Bs_JpsiPji_MC12.py")
# Dangerous BKG Bs-> K K
#importOptions("/afs/cern.ch/user/b/bkhanji/private/DVana/Bs2KMuNu/Bs_KK_MC12.py")
# bb->mumu inclusive
#importOptions("/afs/cern.ch/user/b/bkhanji/private/DVana/Bs2KMuNu/bb_mumuMC12.py")

# kpi0:
#importOptions("/afs/cern.ch/user/b/bkhanji/private/DVana/Bs2KMuNu/Bs2KstplusMuNu_kpi0.py")
importOptions("/afs/cern.ch/user/b/bkhanji/private/DVana/Bs2KMuNu/K2star.py")
