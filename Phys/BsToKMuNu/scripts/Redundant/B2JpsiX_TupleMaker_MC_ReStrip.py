### __author__ : Basem Khanji ,  basem.khanji@cern.ch 
#######################################################################
import os , sys
os.getcwd()
sys.path.append(os.getcwd())
from JobName import *

from Gaudi.Configuration import *
#######################################################################
#
from DecayTreeTuple.Configuration import *
#######################################################################
from PhysSelPython.Wrappers import AutomaticData, DataOnDemand, Selection, SelectionSequence , MergedSelection
import GaudiKernel.SystemOfUnits as Units

from Configurables import DaVinci, DecayTreeTuple, TupleToolTrigger, LoKi__Hybrid__TupleTool, TupleToolTagging, TupleToolTISTOS, TupleToolDecay, TupleToolNeutrinoReco, LoKi__Hybrid__EvtTupleTool,  L0TriggerTisTos , TriggerTisTos , TupleToolIsoGeneric

from Configurables import FilterDesktop , CombineParticles, TupleToolDocas
from Configurables import TupleToolEventInfo
# StdNoPIDsVeloPions are not by default in COMMONPARTICLES , you need to make them before running the tool !
# These lines are stolen from here : https://lhcb-release-area.web.cern.ch/LHCb-release-area/DOC/urania/latest_doxygen/py/d0/d15/config_iso_8py_source.html otherwise I need to change the design of the TupleToolBs2Kmunu_velotrac tool ...
# re-run the stripping to get  FullDSTDiMuonJpsi2MuMuDetachedLine 
################################################################################################################################
simulationFLAG == True

if simulationFLAG == True:
    from StrippingConf.Configuration import StrippingConf, StrippingStream
    from StrippingSettings.Utils import strippingConfiguration
    from StrippingArchive.Utils import buildStreams
    from StrippingArchive import strippingArchive
    
    stripping= "stripping24"
    MyStream = StrippingStream("Dimuon")
    MyLines  = [ 'StrippingFullDSTDiMuonJpsi2MuMuDetachedLine' ]
    config  = strippingConfiguration(stripping)
    archive = strippingArchive(stripping)
    streams = buildStreams(stripping=config, archive=archive) 
    eventNodeKiller       = EventNodeKiller('Stripkiller')
    eventNodeKiller.Nodes = [ '/Event/AllStreams', '/Event/Strip' ]
    DaVinci().appendToMainSequence( [ eventNodeKiller ] )
    
    for stream in streams:
        for line in stream.lines:
            if line.name() in MyLines:
                MyStream.appendLines( [ line ] )

    from Configurables import ProcStatusCheck
    filterBadEvents = ProcStatusCheck()
    sc = StrippingConf( Streams = [ MyStream ],
                        MaxCandidates = 2000,
                        AcceptBadEvents = False,
                        BadEventSelection = filterBadEvents )
    DaVinci().UserAlgorithms += [ sc.sequence() ]
################################################################################################################################

################################################################################################################################
def createVeloTracks():
    from Configurables import ChargedProtoParticleMaker, DaVinci
    veloprotos = ChargedProtoParticleMaker("ProtoPMaker")
    veloprotos.Inputs = ["Rec/Track/Best"]
    veloprotos.Output = "Rec/ProtoP/myProtoPMaker/ProtoParticles"
    DaVinci().appendToMainSequence( [ veloprotos ])
    from Configurables       import ProtoParticleCALOFilter, CombinedParticleMaker,NoPIDsParticleMaker
    from CommonParticles.Utils import trackSelector, updateDoD
    algorithm = NoPIDsParticleMaker('StdNoPIDsVeloPions',  Particle = 'pion',  )
    algorithm.Input = "Rec/ProtoP/myProtoPMaker/ProtoParticles"
    selector = trackSelector ( algorithm , trackTypes = ['Velo'] )
    locations = updateDoD ( algorithm )
    DaVinci().appendToMainSequence( [ algorithm ])

createVeloTracks()

def configIso():
    from Configurables import ChargedProtoParticleMaker, DaVinci
    Alltracksprotos = ChargedProtoParticleMaker("ProtoPMaker")
    Alltracksprotos.Inputs = ["Rec/Track/Best"]
    Alltracksprotos.Output = "Rec/ProtoP/myProtoPMaker/ProtoParticles"
    DaVinci().appendToMainSequence( [ Alltracksprotos ])
    from Configurables       import ProtoParticleCALOFilter, CombinedParticleMaker,NoPIDsParticleMaker
    from CommonParticles.Utils import trackSelector, updateDoD
    algorithm = NoPIDsParticleMaker('StdNoPIDsAlltracksPions',  Particle = 'pion' ,  )
    algorithm.Input = "Rec/ProtoP/myProtoPMaker/ProtoParticles"
    selector = trackSelector ( algorithm , trackTypes = ['Velo', 'Long' , 'Upstream'] )
    locations = updateDoD ( algorithm )
    DaVinci().appendToMainSequence( [ algorithm ])
configIso()

LongPions      = DataOnDemand( Location ='Phys/StdAllNoPIDsPions/Particles' )
UpStreamPions  = DataOnDemand( Location ='Phys/StdNoPIDsUpPions/Particles'  )
VeloPions      = DataOnDemand( Location ='Phys/StdNoPIDsVeloPions/Particles')
AllPions       = MergedSelection( "AllPions", RequiredSelections =[ LongPions , UpStreamPions , VeloPions  ] )
AllPions_seq   = SelectionSequence('AllPions_seq', TopSelection = AllPions)
DaVinci().appendToMainSequence( [ AllPions_seq ])
#############################################################
Unified_Kaon_cuts = "(MINTREE(ABSID=='K+',P)> 10000.0 *MeV ) & (MINTREE(ABSID=='K+',PT)> 800.0 *MeV)& (MINTREE(ABSID=='K+',TRGHOSTPROB)< 0.5) & (MINTREE(ABSID=='K+',PIDK-PIDpi)> 5.0 )& (MINTREE(ABSID=='K+',PIDK-PIDp)> 5.0 )& (MINTREE(ABSID=='K+',PIDK-PIDmu)> 5.0 ) & (MINTREE(ABSID=='K+',MIPCHI2DV(PRIMARY))> 16)"
Unified_Muon_cuts = "(MINTREE(ABSID=='mu+',P)> 6000.0 *MeV) & (MINTREE(ABSID=='mu+',PT)> 1500.0* MeV)& (MINTREE(ABSID=='mu+',TRGHOSTPROB) < 0.35) & (MINTREE(ABSID=='mu+',PIDmu-PIDpi)> 3.0 )& (MINTREE(ABSID=='mu+',PIDmu-PIDp)> 0.0 )& (MINTREE(ABSID=='mu+',PIDmu-PIDK)> 0.0 )& (MINTREE(ABSID=='mu+',MIPCHI2DV(PRIMARY))> 12)"
Unified_Jpsi_cuts = "(((BPVDLS>3) | (BPVDLS<-3))) & (MINTREE('mu+'==ABSID,PIDmu) > 0.0) & ((MM > 2996.916) & (MM < 3196.916)) & (VFASPF(VCHI2PDOF)< 20.0) & (MINTREE('mu+'==ABSID,PT) > 500.0 *MeV)"
Unified_MotherCut = "(VFASPF(VCHI2/VDOF)< 25.0) & (BPVDIRA> 0.994) & (BPVVDCHI2 >120.0) & (M>5100) & (M<6000)"
Unified_CombinationCut  = "(AM> 500.0*MeV) & (AM<6000.0*MeV)"

Kaon_particles  = DataOnDemand( Location ='Phys/StdLooseKaons/Particles')
Muon_particles  = DataOnDemand( Location ='Phys/StdAllLooseMuons/Particles')
#Jpsi_particles  = DataOnDemand( Location ='Phys/FullDSTDiMuonJpsi2MuMuDetachedLine/Particles')
Jpsi_particles  = DataOnDemand( Location ='Phys/StdLooseJpsi2MuMu/Particles')
phi_particles   = DataOnDemand( Location ='Phys/StdLooseDetachedPhi2KK/Particles')

Kaon_Fltr  = FilterDesktop("Phis_Fltr", Code = Unified_Kaon_cuts)
Kaon_sel   = Selection("Kaon_sel", Algorithm = Kaon_Fltr, RequiredSelections = [Kaon_particles])
Jpsi_Fltr  = FilterDesktop("Jpsi_Fltr", Code = Unified_Muon_cuts +" & " + Unified_Jpsi_cuts)
#Jpsi_Fltr  = FilterDesktop("Jpsi_Fltr", Code = Unified_Muon_cuts )
Unified_Jpsi_sel   = Selection("Unified_Jpsi_sel"     , Algorithm     = Jpsi_Fltr  , RequiredSelections = [Jpsi_particles])

Bu2JpsiK = CombineParticles("Bu2JpsiK",
                            DecayDescriptors = [ '[B+ -> J/psi(1S) K+]cc' ] ,
                            CombinationCut   = Unified_CombinationCut       , 
                            MotherCut        = Unified_MotherCut            )
if simulationFLAG == True:
    Bu2JpsiK.Preambulo      = ["from LoKiPhysMC.decorators import *","from LoKiPhysMC.functions import mcMatch"]
    Bu_Mothercuts           = "(mcMatch ('[ B+ => J/psi(1S) K+ ]CC'))"
Bu2JpsiK.MotherCut      = Bu_Mothercuts
Bu2JpsiK_Sel            = Selection("Bu2JpsiK_Sel",
                                    Algorithm = Bu2JpsiK,
                                    RequiredSelections = [ Unified_Jpsi_sel, Kaon_sel ]
                                    )
SeqBu2JpsiK = SelectionSequence('SeqBu2JpsiK', TopSelection = Bu2JpsiK_Sel)
SeqBu2JpsiK.outputLevel = 6
# Bs->JpsiPhi
phi_cuts        = "(VFASPF(VCHI2) < 16.0) & (M> 980.0*MeV) & (M<1040.0*MeV)"
Phi_Fltr        = FilterDesktop("Phis_Fltr", Code = Unified_Kaon_cuts +" & " + phi_cuts)
Phi_sel         = Selection("Phi_sel", Algorithm = Phi_Fltr, RequiredSelections = [phi_particles])

Bs2JpsiPhi      = CombineParticles("Bs2JpsiPhi",
                                 DecayDescriptors = [ ' B_s0 -> J/psi(1S) phi(1020)' ] ,
                                 CombinationCut   = Unified_CombinationCut,
                                 MotherCut        = Unified_MotherCut
                                 )
if simulationFLAG == True:
    Bs2JpsiPhi.Preambulo      = ["from LoKiPhysMC.decorators import *","from LoKiPhysMC.functions import mcMatch"]
    Bs_Mothercuts             = "(mcMatch ('[ [B_s~0]cc => J/psi(1S) phi(1020) ]CC'))"
Bs2JpsiPhi.MotherCut      = Bs_Mothercuts
Bs2JpsiPhi_Sel            = Selection("Bs2JpsiPhi_Sel",
                                      Algorithm = Bs2JpsiPhi ,
                                      RequiredSelections = [ Unified_Jpsi_sel,Phi_sel ]
                                      )
SeqBs2JpsiPhi = SelectionSequence('SeqBs2JpsiPhi', TopSelection = Bs2JpsiPhi_Sel)
SeqBs2JpsiPhi.outputLevel = 6
#==================================================================================================================
B2JpsiXSelection_seq = GaudiSequencer('B2JpsiXSelection_seq')
B2JpsiXSelection_seq.Members       +=  [ SeqBs2JpsiPhi.sequence() ]
B2JpsiXSelection_seq.ModeOR         =  True
B2JpsiXSelection_seq.ShortCircuit   =  False
B2JpsiXSelection_seq.Members       +=  [ SeqBu2JpsiK.sequence()  ]
B2JpsiXSelection_seq.ModeOR         =  True
B2JpsiXSelection_seq.ShortCircuit   =  False
#######################################################################

#############################################################
# DecayTreeTuple
TupleSeq         = GaudiSequencer('TupleSeq')
Bu2JpsiKTuple    = DecayTreeTuple("Bu2JpsiKTuple")
#Bu2JpsiKTuple.Inputs   = ['Phys/B2XuMuNuBs2KLine/Particles']
Bu2JpsiKTuple.Inputs    = [SeqBu2JpsiK.outputLocation()]
Bu2JpsiKTuple.ToolList += [
    "TupleToolGeometry"
    ,"TupleToolKinematic"
    , "TupleToolPrimaries"
    , "TupleToolTrackInfo"
    , "LoKi::Hybrid::TupleTool/LoKiTool"
    ]

if simulationFLAG == True:
    Bu2JpsiKTuple.ToolList += [
        "TupleToolMCBackgroundInfo"
        ]
Bu2JpsiKTuple.ReFitPVs = True
Bu2JpsiKTuple.OutputLevel = 6

Bu2JpsiKTuple.Decay    = '[B- -> ^(J/psi(1S)-> ^mu+ ^mu-) ^K- ]CC'
Bu2JpsiKTuple.Branches = {
    "muon_p"   : '[B- ->  (J/psi(1S)-> ^mu+  mu-)  K- ]CC'
    ,"muon_m"  : '[B- ->  (J/psi(1S)->  mu+ ^mu-)  K- ]CC'
    ,"kaon_m"  : '[B- ->  (J/psi(1S)->  mu+  mu-) ^K- ]CC'
    ,"Jpsi"    : '[B- -> ^(J/psi(1S)->  mu+  mu-)  K- ]CC'
    ,"Bu"      : '[B- ->  (J/psi(1S)->  mu+  mu-)  K- ]CC'
    }
Bu2JpsiKTuple.addTool(TupleToolDecay, name="Bu"  )
Bu2JpsiKTuple.addTool(TupleToolDecay, name="muon_p")
Bu2JpsiKTuple.addTool(TupleToolDecay, name="muon_m")
Bu2JpsiKTuple.addTool(TupleToolDecay, name="kaon_m")
Bu2JpsiKTuple.addTool(TupleToolDecay, name="Jpsi")

TupleToolIsoGeneric              =  Bu2JpsiKTuple.addTupleTool("TupleToolIsoGeneric")
TupleToolIsoGeneric.ParticlePath =  AllPions_seq.outputLocation()
TupleToolIsoGeneric.VerboseMode = True


LoKiTool_noniso = LoKi__Hybrid__TupleTool("LoKiTool_noniso")
LoKiTool_noniso.Variables     = { "ETA" : "ETA" , "PHI" : "PHI" , "TRTYPE" : "TRTYPE"}
TupleToolIsoGeneric.ToolList += ["LoKi::Hybrid::TupleTool/LoKiTool_noniso"]
TupleToolIsoGeneric.addTool(LoKiTool_noniso)
TupleToolIsoGeneric.OutputLevel = 6
if simulationFLAG == True:
    MCTruth_noniso          = TupleToolMCTruth('MCTruth_noniso')
    MCTruth_noniso.ToolList = ["MCTupleToolHierarchy"]
    TupleToolIsoGeneric.ToolList += ["TupleToolMCTruth/MCTruth_noniso"]
    TupleToolIsoGeneric.addTool(MCTruth_noniso)

budocas = Bu2JpsiKTuple.Bu.addTupleTool('TupleToolDocas')
budocas.Name       = [ "Kmu_OS", "Kmu_SS", "mumu" ]
budocas.Location1  = [ "[B- ->  (J/psi(1S)-> mu+  mu-)  ^K-]CC",
                       "[B- ->  (J/psi(1S)-> mu+  mu-)  ^K-]CC",
                       "[B- ->  (J/psi(1S)-> mu+  ^mu-)  K-]CC"]
budocas.Location2  = [ "[B- ->  (J/psi(1S)-> ^mu+  mu-)  K-]CC",
                       "[B- ->  (J/psi(1S)-> mu+  ^mu-)  K-]CC",
                       "[B- ->  (J/psi(1S)-> ^mu+  mu-)  K-]CC"]

LoKiVariables = Bu2JpsiKTuple.Bu.addTupleTool("LoKi::Hybrid::TupleTool/LoKiVariables")
if simulationFLAG == True:
    LoKiVariables.Preambulo = [
        "from LoKiPhysMC.decorators import *",
        "from LoKiPhysMC.functions import mcMatch"
        ]

LoKiVariables.Variables = {
    "M_JpsiConstr"     : "DTF_FUN ( M , True , 'J/psi(1S)' )" ,
    "DTF_VCHI2NDOF"    : "DTF_FUN ( VFASPF(VCHI2/VDOF) , True  )",
    "ETA"              : "ETA",
    "PHI"              : "PHI",
    "DOCA"             : "DOCA(1,2)",
    "DTF_TAU"          : "DTF_CTAU( 0, True )/0.299792458",
    "DTF_CTAUS"        : "DTF_CTAUSIGNIFICANCE( 0, True )",
    "DTF_CHI2NDOF"     : "DTF_CHI2NDOF( True )",
    "FD_CHI2_LOKI"     : "BPVVDCHI2",
    "VCHI2_LOKI"       : "VFASPF(VCHI2/VDOF)",
    "FD_S"             : "BPVDLS",
    "cosTheta1_star"   : "LV01",
    "cosTheta2_star"   : "LV02"
    }
if simulationFLAG == True:
    LoKiVariables.Variables += {
        "TruthMatched"     : "switch( mcMatch ('[ B- ==> (J/psi(1S)-> mu+ mu-) K- ]CC', 1 )  , 1 , 0 )"
        }
LoKiVariables_K = Bu2JpsiKTuple.kaon_m.addTupleTool("LoKi::Hybrid::TupleTool/LoKiVariables_K")
LoKiVariables_K.Variables = { "MIPCHI2DV_PV" : "MIPCHI2DV(PRIMARY)" }
LoKiVariables_Mu = Bu2JpsiKTuple.muon_p.addTupleTool("LoKi::Hybrid::TupleTool/LoKiVariables_Mu")
LoKiVariables_Mu.Variables = { "MIPCHI2DV_PV" : "MIPCHI2DV(PRIMARY)" }
############################################################################
TupleToolTISTOS = TupleToolTISTOS('TupleToolTISTOS')
TupleToolTISTOS.addTool(L0TriggerTisTos())
TupleToolTISTOS.addTool(TriggerTisTos())
TupleToolTISTOS.TriggerList=[
    "L0MuonDecision",
    "L0DiMuonDecision",
    "Hlt2TopoMu2BodyBBDTDecision",
    "Hlt2SingleMuonDecision",
    "Hlt2SingleMuonLowPTDecision"
    #"L0HadronDecision",
    #"Hlt2SingleMuonHighPTDecision",
    #"Hlt2Topo2BodySimpleDecision",
    #"Hlt2Topo2BodyBBDTDecision"
    ]

TupleToolTISTOS.VerboseL0   = True
TupleToolTISTOS.VerboseHlt1 = True
TupleToolTISTOS.VerboseHlt2 = True
TupleToolTISTOS.TUS         = True
TupleToolTISTOS.TPS         = True

Bu2JpsiKTuple.Bu.addTool(TupleToolTISTOS , name = 'TupleToolTISTOS' )
Bu2JpsiKTuple.Bu.ToolList   += [ "TupleToolTISTOS"]

Bu2JpsiKTuple.kaon_m.addTool(TupleToolTISTOS , name = 'TupleToolTISTOS' )
Bu2JpsiKTuple.kaon_m.ToolList   += [ "TupleToolTISTOS"]

Bu2JpsiKTuple.muon_p.addTool(TupleToolTISTOS , name = 'TupleToolTISTOS' )
Bu2JpsiKTuple.muon_p.ToolList   += [ "TupleToolTISTOS"]

if simulationFLAG == True:
    MCTruth = Bu2JpsiKTuple.addTupleTool("TupleToolMCTruth")
    MCTruth.addTupleTool("MCTupleToolHierarchy")

    MCTruth_k = Bu2JpsiKTuple.kaon_m.addTupleTool("TupleToolMCTruth")
    MCTruth_k.addTupleTool("MCTupleToolReconstructed")
    MCTruth_k.addTupleTool("MCTupleToolHierarchy")

    #MCTruth.muon_p.addTupleTool("MCTupleToolReconstructed")
    MCTruth_mu = Bu2JpsiKTuple.muon_p.addTupleTool("TupleToolMCTruth")
    MCTruth_mu.addTupleTool("MCTupleToolReconstructed")
    MCTruth_mu.addTupleTool("MCTupleToolHierarchy")


from Configurables import TupleToolConeIsolation
Bu2JpsiKTuple.kaon_m.addTupleTool("TupleToolConeIsolation")
Bu2JpsiKTuple.kaon_m.TupleToolConeIsolation.FillAsymmetry     = True
Bu2JpsiKTuple.kaon_m.TupleToolConeIsolation.FillDeltas        = True
Bu2JpsiKTuple.kaon_m.TupleToolConeIsolation.FillComponents    = True
Bu2JpsiKTuple.kaon_m.TupleToolConeIsolation.MinConeSize       = 0.5
Bu2JpsiKTuple.kaon_m.TupleToolConeIsolation.SizeStep          = 0.5
Bu2JpsiKTuple.kaon_m.TupleToolConeIsolation.MaxConeSize       = 2.0
Bu2JpsiKTuple.kaon_m.TupleToolConeIsolation.FillPi0Info       = True
Bu2JpsiKTuple.kaon_m.TupleToolConeIsolation.FillMergedPi0Info = True

Bu2JpsiKTuple.muon_p.addTupleTool("TupleToolConeIsolation")
Bu2JpsiKTuple.muon_p.TupleToolConeIsolation.FillAsymmetry     = True
Bu2JpsiKTuple.muon_p.TupleToolConeIsolation.FillDeltas        = True
Bu2JpsiKTuple.muon_p.TupleToolConeIsolation.FillComponents    = True
Bu2JpsiKTuple.muon_p.TupleToolConeIsolation.MinConeSize       = 0.5
Bu2JpsiKTuple.muon_p.TupleToolConeIsolation.SizeStep          = 0.5
Bu2JpsiKTuple.muon_p.TupleToolConeIsolation.MaxConeSize       = 2.0

Bu2JpsiKTuple.ToolList +=  [
    "TupleToolPropertime"
    , "TupleToolRecoStats"
    , "TupleToolSLTools"
    , "TupleToolTrackPosition"
    #, "TupleToolRICHPid"
    #, "TupleToolMuonPid"
    , "TupleToolGeneration"
    ]


Bs2JpsiPhiTuple          = Bu2JpsiKTuple.clone("Bs2JpsiPhiTuple")
Bs2JpsiPhiTuple.Inputs   = [ SeqBs2JpsiPhi.outputLocation() ]
Bs2JpsiPhiTuple.Decay    = '[B_s0 -> ^(J/psi(1S)-> ^mu+ ^mu-) ^(phi(1020) -> ^K+ ^K-)]CC'
Bs2JpsiPhiTuple.Branches = {
    "muon_p"   : "[B_s0 ->  (J/psi(1S)-> ^mu+  mu-)  (phi(1020) ->  K+  K-)]CC"
    ,"muon_m"  : "[B_s0 ->  (J/psi(1S)->  mu+ ^mu-)  (phi(1020) ->  K+  K-)]CC"
    ,"kaon_p"  : "[B_s0 ->  (J/psi(1S)->  mu+  mu-)  (phi(1020) -> ^K+  K-)]CC"
    ,"kaon_m"  : "[B_s0 ->  (J/psi(1S)->  mu+  mu-)  (phi(1020) ->  K+ ^K-)]CC"
    ,"Jpsi"    : "[B_s0 -> ^(J/psi(1S)->  mu+  mu-)  (phi(1020) ->  K+  K-)]CC"
    ,"phi"     : "[B_s0 ->  (J/psi(1S)->  mu+  mu-) ^(phi(1020) ->  K+  K-)]CC"
    ,"Bs"      : "[B_s0 ->  (J/psi(1S)->  mu+  mu-)  (phi(1020) ->  K+  K-)]CC"
    }
Bs2JpsiPhiTuple.addTool(TupleToolDecay, name="Bs"  )
Bs2JpsiPhiTuple.addTool(TupleToolDecay, name="kaon_m")
Bs2JpsiPhiTuple.addTool(TupleToolDecay, name="muon_p")
Bs2JpsiPhiTuple.addTool(TupleToolDecay, name="muon_m")
Bs2JpsiPhiTuple.addTool(TupleToolDecay, name="kaon_p")
Bs2JpsiPhiTuple.addTool(TupleToolDecay, name="Jpsi")
Bs2JpsiPhiTuple.addTool(TupleToolDecay, name="phi" )

bsdocas = Bs2JpsiPhiTuple.Bs.addTupleTool('TupleToolDocas')
bsdocas.Name       = [ "Kmu_pm", "Kmu_mp", "mumu", "KK" ]
bsdocas.Location1  = [ "[B_s0 ->  (J/psi(1S)->  mu+  mu-)  (phi(1020) -> ^K+  K-)]CC",
                       "[B_s0 ->  (J/psi(1S)->  mu+  mu-)  (phi(1020) ->  K+ ^K-)]CC",
                       "[B_s0 ->  (J/psi(1S)-> ^mu+  mu-)  (phi(1020) ->  K+  K-)]CC",
                       "[B_s0 ->  (J/psi(1S)->  mu+  mu-)  (phi(1020) -> ^K+  K-)]CC"]
bsdocas.Location2  = [ "[B_s0 ->  (J/psi(1S)->  mu+ ^mu-)  (phi(1020) ->  K+  K-)]CC",
                       "[B_s0 ->  (J/psi(1S)-> ^mu+  mu-)  (phi(1020) ->  K+  K-)]CC",
                       "[B_s0 ->  (J/psi(1S)->  mu+ ^mu-)  (phi(1020) ->  K+  K-)]CC",
                       "[B_s0 ->  (J/psi(1S)->  mu+  mu-)  (phi(1020) ->  K+ ^K-)]CC"]

LoKiVariables = Bs2JpsiPhiTuple.Bs.addTupleTool("LoKi::Hybrid::TupleTool/LoKiVariables")
if simulationFLAG == True:
    LoKiVariables.Preambulo = ["from LoKiPhysMC.decorators import *",
                               "from LoKiPhysMC.functions import mcMatch" ]
LoKiVariables.Variables = {
    "M_JpsiConstr"     : "DTF_FUN ( M , True , 'J/psi(1S)' )" ,
    "DTF_VCHI2NDOF"    : "DTF_FUN ( VFASPF(VCHI2/VDOF) , True  )",
    "ETA"              : "ETA",
    "PHI"              : "PHI",
    "DOCA"             : "DOCA(1,2)",
    "DTF_TAU"          : "DTF_CTAU( 0, True )/0.299792458",
    "DTF_CTAUS"        : "DTF_CTAUSIGNIFICANCE( 0, True )",
    "DTF_CHI2NDOF"     : "DTF_CHI2NDOF( True )",
    "FD_CHI2_LOKI"     : "BPVVDCHI2",
    "VCHI2_LOKI"       : "VFASPF(VCHI2/VDOF)",
    "FD_S"             : "BPVDLS",
    "cosTheta1_star"   : "LV01",
    "cosTheta2_star"   : "LV02"
    }
if simulationFLAG == True:
    LoKiVariables.Variables += {
        "TruthMatched"     : "switch( mcMatch ('[ [B_s~0]cc ==> (J/psi(1S)==> mu+ mu-) (phi(1020) ==> K+ K-) ]CC', 1 )  , 1 , 0 )"
        }
    
Bs2JpsiPhiTuple.Bs.addTool(TupleToolTISTOS , name = 'TupleToolTISTOS' )
Bs2JpsiPhiTuple.Bs.ToolList   += [ "TupleToolTISTOS"]

TupleSeq.Members        += [ Bu2JpsiKTuple ]
TupleSeq.ModeOR          = True
TupleSeq.ShortCircuit    = False

TupleSeq.Members        += [ Bs2JpsiPhiTuple ]
TupleSeq.ModeOR          = True
TupleSeq.ShortCircuit    = False
######################################################################
#
# Event Tuple
from Configurables import EventTuple , TupleToolEventInfo
etuple = EventTuple()
########################################################################
#
# DaVinci settings
#

from Configurables import DaVinci

#DaVinci().appendToMainSequence( [ sc.sequence() ] )
DaVinci().appendToMainSequence( [ sc.sequence() ] )
DaVinci().UserAlgorithms += [ etuple , B2JpsiXSelection_seq , TupleSeq ]
DaVinci().MainOptions   = ""
DaVinci().EvtMax        = 2500
DaVinci().PrintFreq     = 1000
DaVinci().DataType      = '2012'
# data 2012:

#from Configurables import CondDB
#CondDB().UseLatestTags = ["2012"]
# MC
DaVinci().Simulation = False
DaVinci().Lumi       = True
if simulationFLAG == True:
    DaVinci().DDDBtag       =  "dddb-20120831" # MC12
    DaVinci().CondDBtag     =  "sim-20121025-vc-mu100" # MC12
    DaVinci().Simulation = True
    DaVinci().Lumi       = False
#######################################################################
#
DaVinci().TupleFile = "DTT_Sig.root"

# Signal
#importOptions("/afs/cern.ch/user/b/bkhanji/private/DVana/Bs2KMuNu/Bs_KmuNu_MC.py")
# Control channel
#importOptions("/afs/cern.ch/user/b/bkhanji/private/DVana/Bs2KMuNu/Bs_DsMuNu_MC12.py")
## dangerous bkg B+ -> Jpisi K+

#importOptions("./BuJpsiK.py")

# Dangerous BKG Bs-> Jpsi Phi

#importOptions("./BsJpsiPhi.py")
# Dangerous BKG Bs-> K K
#importOptions("/afs/cern.ch/user/b/bkhanji/private/DVana/Bs2KMuNu/Bs_KK_MC12.py")
# bb->mumu inclusive
#importOptions("/afs/cern.ch/user/b/bkhanji/private/DVana/Bs2KMuNu/bb_mumuMC12.py")

# kpi0:
#importOptions("/afs/cern.ch/user/b/bkhanji/private/DVana/Bs2KMuNu/Bs2KstplusMuNu_kpi0.py")
#importOptions("/afs/cern.ch/user/b/bkhanji/private/DVana/Bs2KMuNu/K2star.py")
