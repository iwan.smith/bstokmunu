#######################################################################
from Gaudi.Configuration import *
#######################################################################
from DecayTreeTuple.Configuration import *
#######################################################################
from PhysSelPython.Wrappers import AutomaticData, DataOnDemand, Selection, SelectionSequence, MergedSelection
import GaudiKernel.SystemOfUnits as Units

from Configurables import DaVinci, DecayTreeTuple, TupleToolTrigger, LoKi__Hybrid__TupleTool, TupleToolTagging, TupleToolTISTOS, TupleToolDecay, TupleToolNeutrinoReco, LoKi__Hybrid__EvtTupleTool, MCTupleToolInteractions, L0TriggerTisTos , TriggerTisTos
from Configurables import GaudiSequencer, TupleToolTrackInfo, TupleToolPropertime, ReadHltReport, TrackMasterFitter
from Configurables import TupleToolStripping, TupleToolRecoStats, TupleToolGeometry, TupleToolPid, TupleToolMuonPid
from Configurables import TupleToolAngles
from Configurables import LoKi__HDRFilter as StripFilter
from Configurables import LoKi__VoidFilter as VoidFilter
from StandardParticles import StdLooseKaons, StdLooseMuons
from Configurables import TisTosParticleTagger


from Configurables import FilterDesktop , CombineParticles
from Configurables import TupleToolEventInfo
from StrippingConf.Configuration import StrippingConf, StrippingStream
import re

# Select my lines
from Configurables import ProcStatusCheck

MessageSvc().Format = "% F%60W%S%7W%R%T %0W%M"

#Raw event juggler to split Other/RawEvent into Velo/RawEvent and Tracker/RawEvent
#
from Configurables import RawEventJuggler
juggler = RawEventJuggler( DataOnDemand=True, Input=2.0, Output=4.0 )





def makeNewStripSel_Bs2KMuNu(isSS):
    # we think that these cuts here are not so important, so we decided to do not apply them and forget about the problem with the GEC and VoidFilter
    # GEC1_filter = VoidFilter( "GEC1_filter_cut",
    #                           Code = "ALG_EXECUTED('StrippingStreamSemileptonicBadEvent') & ~ALG_PASSED('StrippingStreamSemileptonicBadEvent')",
    #                           Preambulo = [ 'from LoKiHlt.algorithms import *' ]
    #                           )
    # GEC1_sel = Selection("GEC1_sel", Algorithm = ??, RequiredSelections = [??])

    # GEC2_filter = VoidFilter( "GEC2_filter_cut",
    #                              Code = "( recSummaryTrack(LHCb.RecSummary.nLongTracks, TrLONG) < 250.0 )",
    #                              Preambulo = [ 'from LoKiTracks.decorators import *' ]
    #                              )
    # GEC2_sel = Selection("GEC2_sel", Algorithm = GEC2_filter)


    # CUT FOR KAON
    kaons_particles_filter = FilterDesktop("kaons_particles_cut",
                                           Code = "(TRCHI2DOF < 6.0 )& (P> 10000.0 *MeV) & (PT> 500.0 *MeV)& (TRGHOSTPROB < 0.5)& (MIPCHI2DV(PRIMARY)> 16 )"
                                           # & (PIDK-PIDpi> 5.0 )& (PIDK-PIDp> 5.0 )& (PIDK-PIDmu> 5.0 ) # PID cut removed because of PID correction will be purely data driven
                                           )
    kaons_particles_sel = Selection("kaons_particles_cut_sel",
                                    Algorithm = kaons_particles_filter,
                                    RequiredSelections = [ StdLooseKaons ])

    # CUT FOR MUON
    Muons_particles_filter= FilterDesktop("Muons_particles_cut",
                                          Code = "(TRCHI2DOF < 4.0 ) & (P> 6000.0 *MeV) & (PT> 1500.0* MeV)& (TRGHOSTPROB < 0.35)& (MIPCHI2DV(PRIMARY)> 12)" # & (TOS('L0.*Muon.*Decision', 'TriggerTisTos'))"
                                          # & (PIDmu-PIDpi> 3.0 )& (PIDmu-PIDp> 0.0 )& (PIDmu-PIDK> 0.0 ) # PID cut removed because of PID correction will be purely data driven
                                          )
    Muons_particles_sel = Selection("Muons_particles_cut_sel",
                                    Algorithm = Muons_particles_filter,
                                    RequiredSelections = [ StdLooseMuons ]
                                    )
    #muon trigger request
    myMuTagger = TisTosParticleTagger("MyMuTagger")
    myMuTagger.TisTosSpecs = { 'L0.*Muon.*Decision%TOS' : 0 }
    selTrigger = Selection("SelTriggL0Muon", Algorithm = myMuTagger, RequiredSelections = [ Muons_particles_sel ])

    # CUT FOR Bs
    decaydesc = '[B_s~0 -> K+ mu-]cc'
    if(isSS): decaydesc = '[B_s~0 -> K+ mu+]cc'
    Bs2KMu    = CombineParticles("Bs2KMu",
                                 DecayDescriptors = [ decaydesc ] ,
                                 CombinationCut   = "ATRUE"
                                 )
    Bs2KMu.Preambulo        = ["from LoKiPhysMC.decorators import *","from LoKiPhysMC.functions import mcMatch"]
    Bs2KMu.MotherCut        = "(VFASPF(VCHI2/VDOF)< 4.0) & (BPVDIRA> 0.994)& (BPVVDCHI2 >120.0) & (BPVCORRM > 2500.0 *MeV) & (BPVCORRM < 7000.0 *MeV)"
    Bs2KMu.DaughtersCuts    = { '' : 'ALL' , 'K+' : 'ALL' , 'K-' : 'ALL' , 'mu+' : 'ALL' , 'mu-' : 'ALL' }
    Bs2KMu_Sel = Selection("Bs2KMu_Sel",
                           Algorithm = Bs2KMu ,
                           RequiredSelections = [ selTrigger ,  kaons_particles_sel ]
                           )
    # Bs trigger request
    myBsTagger = TisTosParticleTagger("MyBsTagger")
    myBsTagger.TisTosSpecs = { 'Hlt2.*SingleMuon.*Decision%TOS' : 0 , 'Hlt2.*TopoMu2Body.*Decision%TOS' : 0 }
    selTrigger = Selection("SelTriggerTopoMu2", Algorithm = myBsTagger, RequiredSelections = [ Bs2KMu_Sel ])

    SeqBs2KmuNu = SelectionSequence('SeqBs2KmuNu', TopSelection = selTrigger)
    SeqBs2KmuNu.outputLevel = 6
    return SeqBs2KmuNu



def makeLooseTuple_Bs2KMuNu():
    # CUT FOR KAON
    kaons_particles_filter = FilterDesktop("kaons_particles_cut",
                                           Code = "('K-'==ABSID) & (P> 0.0 *MeV)"
                                           # & (PIDK-PIDpi> 5.0 )& (PIDK-PIDp> 5.0 )& (PIDK-PIDmu> 5.0 ) # PID cut removed because of PID correction will be purely data driven
                                           )
    kaons_particles_sel = Selection("kaons_particles_cut_sel",
                                    Algorithm = kaons_particles_filter,
                                    RequiredSelections = [ StdLooseKaons ])

    # CUT FOR MUON
    Muons_particles_filter= FilterDesktop("Muons_particles_cut",
                                          Code = "('mu+'==ABSID) & (P> 0.0 *MeV)" # & (TOS('L0.*Muon.*Decision', 'TriggerTisTos'))"
                                          # & (PIDmu-PIDpi> 3.0 )& (PIDmu-PIDp> 0.0 )& (PIDmu-PIDK> 0.0 ) # PID cut removed because of PID correction will be purely data driven
                                          )
    Muons_particles_sel = Selection("Muons_particles_cut_sel",
                                    Algorithm = Muons_particles_filter,
                                    RequiredSelections = [ kaons_particles_sel, StdLooseMuons ]
                                    )
    # CUT FOR Bs
    decaydesc = '[B_s~0 -> K+ mu-]cc'
    Bs2KMu    = CombineParticles("Bs2KMu",
                                 DecayDescriptors = [ decaydesc ] ,
                                 CombinationCut   = "ATRUE"
                                 )
    Bs2KMu.Preambulo        = ["from LoKiPhysMC.decorators import *","from LoKiPhysMC.functions import mcMatch"]
    Bs2KMu.MotherCut        = "(BPVCORRM > 0.0 *MeV)"
    Bs2KMu.DaughtersCuts    = { '' : 'ALL' , 'K+' : 'ALL' , 'K-' : 'ALL' , 'mu+' : 'ALL' , 'mu-' : 'ALL' }
    Bs2KMu_Sel = Selection("Bs2KMu_Sel",
                           Algorithm = Bs2KMu ,
                           RequiredSelections = [ Muons_particles_sel ,  kaons_particles_sel ]
                           )

    SeqBs2KmuNu = SelectionSequence('SeqBs2KmuNu', TopSelection = Bs2KMu_Sel)
    SeqBs2KmuNu.outputLevel = 6
    return SeqBs2KmuNu




def makeBu2JpsiKSel(isdata):
    Kaon_cuts = "(MINTREE(ABSID=='K+',P)> 10000.0 *MeV ) & (MINTREE(ABSID=='K+',PT)> 800.0 *MeV)& (MINTREE(ABSID=='K+',TRGHOSTPROB)< 0.5) & (MINTREE(ABSID=='K+',PIDK-PIDpi)> 5.0 )& (MINTREE(ABSID=='K+',PIDK-PIDp)> 5.0 )& (MINTREE(ABSID=='K+',PIDK-PIDmu)> 5.0 ) & (MINTREE(ABSID=='K+',MIPCHI2DV(PRIMARY))> 16)"
    Muon_cuts = "(MINTREE(ABSID=='mu+',P)> 6000.0 *MeV) & (MINTREE(ABSID=='mu+',PT)> 1500.0* MeV)& (MINTREE(ABSID=='mu+',TRGHOSTPROB) < 0.35)& (MINTREE(ABSID=='mu+',PIDmu-PIDpi)> 3.0 )& (MINTREE(ABSID=='mu+',PIDmu-PIDp)> 0.0 )& (MINTREE(ABSID=='mu+',PIDmu-PIDK)> 0.0 )& (MINTREE(ABSID=='mu+',MIPCHI2DV(PRIMARY))> 12)"
    Jpsi_cuts = "( ( (BPVDLS>3) | (BPVDLS<-3) ) ) &  ( (MM > 2996.916) & (MM < 3196.916) ) & (VFASPF(VCHI2PDOF)< 20.0)"
    Bu_Mothercuts  = "(VFASPF(VCHI2/VDOF)< 4.0) & (BPVDIRA> 0.994) & (BPVVDCHI2 >120.0)"
    if(isdata):
        Bu_Mothercuts  = "(VFASPF(VCHI2/VDOF)< 20.0) & (BPVDIRA> 0.994) & (BPVVDCHI2 >120.0) & (M>5100) & (M<6000)"
    Kaon_particles  = DataOnDemand( Location ='Phys/StdLooseKaons/Particles')
    Jpsi_particles  = DataOnDemand( Location ='Phys/StdMassConstrainedJpsi2MuMu/Particles')
    if(isdata):
        Jpsi_particles  = DataOnDemand( Location ='/Event/Dimuon/Phys/FullDSTDiMuonJpsi2MuMuDetachedLine/Particles')
    Kaon_Fltr = FilterDesktop("Phis_Fltr", Code = Kaon_cuts)
    Kaon_sel  = Selection("Kaon_sel", Algorithm = Kaon_Fltr, RequiredSelections = [Kaon_particles])
    Jpsi_Fltr = FilterDesktop("Jpsi_Fltr", Code = Muon_cuts +" & " + Jpsi_cuts)
    Jpsi_sel  = Selection("Jpsi_sel"     , Algorithm     = Jpsi_Fltr          , RequiredSelections = [Jpsi_particles])

    forMotherCut = "(VFASPF(VCHI2/VDOF)< 25) & (BPVDIRA> 0.95) & (mcMatch ('[ B+ => J/psi(1S) K+ ]CC'))"
    if(isdata):
        forMotherCut = "(VFASPF(VCHI2/VDOF)< 25) & (BPVDIRA> 0.95)"
    Bu2JpsiK = CombineParticles("Bu2JpsiK",
                                DecayDescriptors = [ '[B+ -> J/psi(1S) K+]cc' ] ,
                                CombinationCut   = "(AM> 500.0*MeV) & (AM<6000.0*MeV)" ,
                                MotherCut        = Bu_Mothercuts
                                )
    Bu2JpsiK.Preambulo      = ["from LoKiPhysMC.decorators import *","from LoKiPhysMC.functions import mcMatch"]
    Bu2JpsiK.MotherCut      = forMotherCut
    Bu2JpsiK_Sel = Selection("Bu2JpsiK_Sel",
                             Algorithm = Bu2JpsiK,
                             RequiredSelections = [ Jpsi_sel, Kaon_sel ]
                             )
    SeqBu2JpsiK = SelectionSequence('SeqBu2JpsiK', TopSelection = Bu2JpsiK_Sel)
    SeqBu2JpsiK.outputLevel = 6
    return SeqBu2JpsiK


# def makeBd2JpsiKstSel(isdata): Bd2JpsiKst or Bu2JpsiKst ???? K*(892)0 or  K*(892)+ is the difference (I think the first but then need to understand the selection better)
#     Kaon_cuts = "(MINTREE(ABSID=='K*(892)0',P)> 10000.0 *MeV ) & (MINTREE(ABSID=='K*(892)0',PT)> 800.0 *MeV)& (MINTREE(ABSID=='K*(892)0',TRGHOSTPROB)< 0.5) & (MINTREE(ABSID=='K*(892)0',PIDK-PIDpi)> 5.0 )& (MINTREE(ABSID=='K*(892)0',PIDK-PIDp)> 5.0 )& (MINTREE(ABSID=='K*(892)0',PIDK-PIDmu)> 5.0 ) & (MINTREE(ABSID=='K*(892)0',MIPCHI2DV(PRIMARY))> 16)"
#     Muon_cuts = "(MINTREE(ABSID=='mu+',P)> 6000.0 *MeV) & (MINTREE(ABSID=='mu+',PT)> 1500.0* MeV)& (MINTREE(ABSID=='mu+',TRGHOSTPROB) < 0.35)& (MINTREE(ABSID=='mu+',PIDmu-PIDpi)> 3.0 )& (MINTREE(ABSID=='mu+',PIDmu-PIDp)> 0.0 )& (MINTREE(ABSID=='mu+',PIDmu-PIDK)> 0.0 )& (MINTREE(ABSID=='mu+',MIPCHI2DV(PRIMARY))> 12)"
#     Jpsi_cuts = "( ( (BPVDLS>3) | (BPVDLS<-3) ) ) &  ( (MM > 2996.916) & (MM < 3196.916) ) & (VFASPF(VCHI2PDOF)< 20.0)"
#     Bd_Mothercuts  = "(VFASPF(VCHI2/VDOF)< 4.0) & (BPVDIRA> 0.994) & (BPVVDCHI2 >120.0)"
#     if(isdata):
#         Bd_Mothercuts  = "(VFASPF(VCHI2/VDOF)< 20.0) & (BPVDIRA> 0.994) & (BPVVDCHI2 >120.0) & (M>5100) & (M<6000)"
#     Kaon_particles  = DataOnDemand( Location ='Phys/StdLooseKaons/Particles')
#     Jpsi_particles  = DataOnDemand( Location ='Phys/StdMassConstrainedJpsi2MuMu/Particles')
#     if(isdata):
#         Jpsi_particles  = DataOnDemand( Location ='/Event/Dimuon/Phys/FullDSTDiMuonJpsi2MuMuDetachedLine/Particles')
#     Kaon_Fltr = FilterDesktop("Phis_Fltr", Code = Kaon_cuts)
#     Kaon_sel  = Selection("Kaon_sel", Algorithm = Kaon_Fltr, RequiredSelections = [Kaon_particles])
#     Jpsi_Fltr = FilterDesktop("Jpsi_Fltr", Code = Muon_cuts +" & " + Jpsi_cuts)
#     Jpsi_sel  = Selection("Jpsi_sel"     , Algorithm     = Jpsi_Fltr          , RequiredSelections = [Jpsi_particles])

#     forMotherCut = "(VFASPF(VCHI2/VDOF)< 25) & (BPVDIRA> 0.95) & (mcMatch ('[ B0 => J/psi(1S) K*(892)0 ]CC'))"
#     if(isdata):
#         forMotherCut = "(VFASPF(VCHI2/VDOF)< 25) & (BPVDIRA> 0.95)"
#     Bd2JpsiK = CombineParticles("Bd2JpsiK",
#                                 DecayDescriptors = [ '[B0 -> J/psi(1S) K*(892)0]cc' ] ,
#                                 CombinationCut   = "(AM> 500.0*MeV) & (AM<6000.0*MeV)" ,
#                                 MotherCut        = Bd_Mothercuts
#                                 )
#     Bd2JpsiK.Preambulo      = ["from LoKiPhysMC.decorators import *","from LoKiPhysMC.functions import mcMatch"]
#     Bd2JpsiK.MotherCut      = forMotherCut
#     Bd2JpsiK_Sel = Selection("Bd2JpsiK_Sel",
#                              Algorithm = Bd2JpsiK,
#                              RequiredSelections = [ Jpsi_sel, Kaon_sel ]
#                              )
#     SeqBd2JpsiK = SelectionSequence('SeqBd2JpsiK', TopSelection = Bd2JpsiK_Sel)
#     SeqBd2JpsiK.outputLevel = 6
#     return SeqBd2JpsiK


def makeBs2JpsiPhiSel(isdata):
    Kaon_cuts = "(MINTREE(ABSID=='K+',P)> 10000.0 *MeV ) & (MINTREE(ABSID=='K+',PT)> 800.0 *MeV)& (MINTREE(ABSID=='K+',TRGHOSTPROB)< 0.5) & (MINTREE(ABSID=='K+',PIDK-PIDpi)> 5.0 )& (MINTREE(ABSID=='K+',PIDK-PIDp)> 5.0 )& (MINTREE(ABSID=='K+',PIDK-PIDmu)> 5.0 ) & (MINTREE(ABSID=='K+',MIPCHI2DV(PRIMARY))> 16)"
    phi_cuts  = "(VFASPF(VCHI2) < 16.0) & (M> 980.0*MeV) & (M<1040.0*MeV)"
    Muon_cuts = "(MINTREE(ABSID=='mu+',P)> 6000.0 *MeV) & (MINTREE(ABSID=='mu+',PT)> 1500.0* MeV)& (MINTREE(ABSID=='mu+',TRGHOSTPROB) < 0.35)& (MINTREE(ABSID=='mu+',PIDmu-PIDpi)> 3.0 )& (MINTREE(ABSID=='mu+',PIDmu-PIDp)> 0.0 )& (MINTREE(ABSID=='mu+',PIDmu-PIDK)> 0.0 )& (MINTREE(ABSID=='mu+',MIPCHI2DV(PRIMARY))> 12)"
    Jpsi_cuts = "( ( (BPVDLS>3) | (BPVDLS<-3) ) ) &  ( (MM > 2996.916) & (MM < 3196.916) ) & (VFASPF(VCHI2PDOF)< 20.0)"
    Bs_Mothercuts  = "(VFASPF(VCHI2/VDOF)< 4.0) & (BPVDIRA> 0.994) & (BPVVDCHI2 >120.0)"
    phi_particles   = DataOnDemand( Location ='Phys/StdLooseDetachedPhi2KK/Particles')
    Jpsi_particles  = DataOnDemand( Location ='Phys/StdMassConstrainedJpsi2MuMu/Particles')
    if(isdata):
        Jpsi_particles  = DataOnDemand( Location ='/Event/Dimuon/Phys/FullDSTDiMuonJpsi2MuMuDetachedLine/Particles')

    Phi_Fltr = FilterDesktop("Phis_Fltr", Code = Kaon_cuts +" & " + phi_cuts)
    Phi_sel  = Selection("Phi_sel", Algorithm = Phi_Fltr, RequiredSelections = [phi_particles])
    Jpsi_Fltr = FilterDesktop("Jpsi_Fltr", Code = Muon_cuts +" & " + Jpsi_cuts)
    Jpsi_sel  = Selection("Jpsi_sel", Algorithm = Jpsi_Fltr, RequiredSelections = [Jpsi_particles])

    Bs2JpsiPhi    = CombineParticles("Bs2JpsiPhi",
                                     DecayDescriptors = [ ' B_s0 -> J/psi(1S) phi(1020)' ] ,
                                     CombinationCut   = "(AM> 500.0*MeV) & (AM<6000.0*MeV)" ,
                                     MotherCut        = Bs_Mothercuts
                                     )
    if(not isdata):
        Bs2JpsiPhi.Preambulo      = ["from LoKiPhysMC.decorators import *","from LoKiPhysMC.functions import mcMatch"]
    Bs2JpsiPhi.MotherCut      = "(VFASPF(VCHI2/VDOF)< 25) & (BPVDIRA> 0.95) & (mcMatch ('[ [B_s~0]cc => J/psi(1S) phi(1020) ]CC'))"
    if(isdata):
        Bs2JpsiPhi.MotherCut      = "(VFASPF(VCHI2/VDOF)< 25) & (BPVDIRA> 0.95)"
    Bs2JpsiPhi_Sel            = Selection("Bs2JpsiPhi_Sel",
                                          Algorithm = Bs2JpsiPhi ,
                                          RequiredSelections = [ Jpsi_sel,Phi_sel ]
                                          )
    SeqBs2JpsiPhi = SelectionSequence('SeqBs2JpsiPhi', TopSelection = Bs2JpsiPhi_Sel)
    SeqBs2JpsiPhi.outputLevel = 6
    return SeqBs2JpsiPhi



def makeTuple(InputLocation, mode,isopath='Phys/StdNoPIDsVeloPions/Particles', DTTAppend = ""):
    print InputLocation
    
    
    
    tuples = []

    SS = "SS" in InputLocation
    
    if( RecoAs == "JpsiPhi" ):
        tuple = DecayTreeTuple("Bu2JpsiPhiTuple")
        tuple.Decay    = '[B_s0 -> ^(J/psi(1S)-> ^mu+ ^mu-) ^(phi(1020) -> ^K+ ^K-)]CC'
        tuple.addBranches( {
            "muon_p"   : "[B_s0 ->  (J/psi(1S)-> ^mu+  mu-)  (phi(1020) ->  K+  K-)]CC"
            ,"muon_m"  : "[B_s0 ->  (J/psi(1S)->  mu+ ^mu-)  (phi(1020) ->  K+  K-)]CC"
            ,"kaon_p"  : "[B_s0 ->  (J/psi(1S)->  mu+  mu-)  (phi(1020) -> ^K+  K-)]CC"
            ,"kaon_m"  : "[B_s0 ->  (J/psi(1S)->  mu+  mu-)  (phi(1020) ->  K+ ^K-)]CC"
            ,"Jpsi"    : "[B_s0 -> ^(J/psi(1S)->  mu+  mu-)  (phi(1020) ->  K+  K-)]CC"
            ,"phi"     : "[B_s0 ->  (J/psi(1S)->  mu+  mu-) ^(phi(1020) ->  K+  K-)]CC"
            ,"Bs"      : "[B_s0 ->  (J/psi(1S)->  mu+  mu-)  (phi(1020) ->  K+  K-)]CC"
        })
        tuples += [ tuple ]
        
    elif( RecoAs == "JpsiK"):
        tuple = DecayTreeTuple("Bu2JpsiKTuple")
        tuple.Decay    = '[B- -> ^(J/psi(1S)-> ^mu+ ^mu-) ^K- ]CC'
        tuple.addBranches( {
            "muon_p"   : '[B- ->  (J/psi(1S)-> ^mu+  mu-)  K- ]CC'
            ,"muon_m"  : '[B- ->  (J/psi(1S)->  mu+ ^mu-)  K- ]CC'
            ,"kaon_m"  : '[B- ->  (J/psi(1S)->  mu+  mu-) ^K- ]CC'
            ,"Jpsi"    : '[B- -> ^(J/psi(1S)->  mu+  mu-)  K- ]CC'
            ,"Bs"      : '[B- ->  (J/psi(1S)->  mu+  mu-)  K- ]CC' # it shold be Bu but with Bs the code below its the same
        })
        tuples += [ tuple ]

        
    elif ( RecoAs == "DsMuNu" ):
        tupleOS = DecayTreeTuple("Bs2DsMuNuTuple"+DTTAppend)
        tupleOS.Decay    = "[ [B0]cc -> ^(D- -> ^K- ^K+ ^pi- ) ^mu+]CC"
        tupleOS.addBranches( {
            "muon_p"   : "[ [B0]cc ->  (D- ->  K-  K+  pi- ) ^mu+]CC"
            ,"kaon_m"  : "[ [B0]cc ->  (D- -> ^K-  K+  pi- )  mu+]CC"
            ,"pi_p"    : "[ [B0]cc ->  (D- ->  K-  K+ ^pi- )  mu+]CC"
            ,"kaon_p"  : "[ [B0]cc ->  (D- ->  K- ^K+  pi- )  mu+]CC"
            ,"Ds"      : "[ [B0]cc -> ^(D- ->  K-  K+  pi- )  mu+]CC"
            ,"Bs"      : "[ [B0]cc ->  (D- ->  K-  K+  pi- )  mu+]CC"
        })
        
        tupleSS = DecayTreeTuple("Bs2DsMuNuSSTuple"+DTTAppend)
        tupleSS.Decay    = "[ [B0]cc -> ^(D+ -> ^K- ^K+ ^pi+ ) ^mu+]CC"
        tupleSS.addBranches( {
             "muon_p"  : "[ [B0]cc ->  (D+ ->  K-  K+  pi+ ) ^mu+]CC"
            ,"kaon_m"  : "[ [B0]cc ->  (D+ -> ^K-  K+  pi+ )  mu+]CC"
            ,"pi_p"    : "[ [B0]cc ->  (D+ ->  K-  K+ ^pi+ )  mu+]CC"
            ,"kaon_p"  : "[ [B0]cc ->  (D+ ->  K- ^K+  pi+ )  mu+]CC"
            ,"Ds"      : "[ [B0]cc -> ^(D+ ->  K-  K+  pi+ )  mu+]CC"
            ,"Bs"      : "[ [B0]cc ->  (D+ ->  K-  K+  pi+ )  mu+]CC"
        })
        tuples += [ tupleOS, tupleSS ]
        
    elif ( RecoAs == "KMuNu" ):
        if ( SS ):
            tupleSS = DecayTreeTuple("Bs2KmuNuSSTuple"+DTTAppend)
            tupleSS.Decay    = "[B_s0 -> ^K+ ^mu+]CC"
            tupleSS.addBranches( {
                "muon_p"  : "[B_s0 ->  K+ ^mu+]CC"
                ,"kaon_m"  : "[B_s0 -> ^K+  mu+]CC"
                ,"Bs"      : "[B_s0 ->  K+  mu+]CC"
            })
            tuples += [ tupleSS ]
        else:
            tupleOS = DecayTreeTuple("Bs2KmuNuTuple"+DTTAppend)
            tupleOS.Decay    = "[B_s0 -> ^K- ^mu+]CC"
            tupleOS.addBranches( {
                "muon_p"   : "[B_s0 ->  K- ^mu+]CC"
                ,"kaon_m"  : "[B_s0 -> ^K-  mu+]CC"
                ,"Bs"      : "[B_s0 ->  K-  mu+]CC"
            })
            tuples += [ tupleOS ]


    for tuple in tuples:
        tuple.Inputs = [InputLocation]
        tuple.ToolList +=  [
            "TupleToolGeometry"
            ,"TupleToolKinematic"
            , "TupleToolPrimaries"
            , "TupleToolTrackInfo"
            # ,"TupleToolTISTOS"
            # ,"TupleToolPid"
            , "LoKi::Hybrid::TupleTool/LoKiTool"
            ,"TupleToolEventInfo"
        ]
        # tts = tuple.addTupleTool("TupleToolStripping") #Flag if the stripping line below select the event
        # tts.StrippingList = ["StrippingKS02MuMuLineDecision", "etc..."]
        tuple.ReFitPVs = True
        tuple.OutputLevel = 6
    
        
        TupleToolIsoGeneric              =  tuple.addTupleTool("TupleToolIsoGeneric")
        TupleToolIsoGeneric.ParticlePath =  isopath
        TupleToolIsoGeneric.VerboseMode = True
        if( DaVinci().Simulation ):
            MCTruth_noniso          = TupleToolMCTruth('MCTruth_noniso')
            MCTruth_noniso.ToolList = ["MCTupleToolHierarchy"]
            TupleToolIsoGeneric.addTool(MCTruth_noniso)
            TupleToolIsoGeneric.ToolList += ["TupleToolMCTruth/MCTruth_noniso"]
        LoKiTool_noniso = LoKi__Hybrid__TupleTool("LoKiTool_noniso")
        LoKiTool_noniso.Variables     = { "ETA" : "ETA" , "PHI" : "PHI" , "TRTYPE" : "TRTYPE"}
        TupleToolIsoGeneric.ToolList += ["LoKi::Hybrid::TupleTool/LoKiTool_noniso"]
        TupleToolIsoGeneric.addTool(LoKiTool_noniso)
        TupleToolIsoGeneric.OutputLevel = 6
        
        LoKiVariables = tuple.Bs.addTupleTool("LoKi::Hybrid::TupleTool/LoKiVariables")
        LoKiVariables.Variables = {
            "ETA"              : "ETA",
            "PHI"              : "PHI",
            "DOCA"             : "DOCA(1,2)",
            "DTF_TAU"          : "DTF_CTAU( 0, True )/0.299792458",
            "DTF_CTAUS"        : "DTF_CTAUSIGNIFICANCE( 0, True )",
            "DTF_CHI2NDOF"     : "DTF_CHI2NDOF( True )",
            "FD_CHI2_LOKI"     : "BPVVDCHI2",
            "VCHI2_LOKI"       : "VFASPF(VCHI2/VDOF)",
            "FD_S"             : "BPVDLS",
            "cosTheta1_star"   : "LV01",
            "cosTheta2_star"   : "LV02",
        }
        if( DaVinci().Simulation ):
            tuple.ToolList +=  ["TupleToolMCBackgroundInfo"]
            LoKiVariables.Preambulo = [
                "from LoKiPhysMC.decorators import *",
                "from LoKiPhysMC.functions import mcMatch"
            ]
            #LoKiVariables = LoKi__Hybrid__TupleTool('LoKiVariables')
            LoKiVariables.Variables["TruthMatched"] = "switch( mcMatch ('[ [B_s~0]cc => K+ mu- Neutrino ]CC',1)   , 1 , 0 )"
    
        if( RecoAs == "JpsiPhi" ):
            LoKiVariables.Variables["M_JpsiConstr"] = "DTF_FUN ( M , True , 'J/psi(1S)' )"
            LoKiVariables.Variables["DTF_VCHI2NDOF"]= "DTF_FUN ( VFASPF(VCHI2/VDOF) , True  )"
            if( DaVinci().Simulation ):
                LoKiVariables.Variables["TM_Bs2JpsiPhi"] = "switch( mcMatch ('[ [B_s~0]cc => J/psi(1S) phi(1020) ]CC', 1 )  , 1 , 0 )"
        elif( RecoAs == "JpsiPhi" ):
            LoKiVariables.Variables["M_JpsiConstr"] = "DTF_FUN ( M , True , 'J/psi(1S)' )"
            LoKiVariables.Variables["DTF_VCHI2NDOF"] = "DTF_FUN ( VFASPF(VCHI2/VDOF) , True  )"
            if( DaVinci().Simulation ):
                LoKiVariables.Variables["TM_Bu2JpsiK"] = "switch( mcMatch ('[ B+ => J/psi(1S) K+ ]CC', 1 )  , 1 , 0 )"
        elif( DaVinci().Simulation and RecoAs == "DsMuNu"):
            LoKiVariables.Variables["TM_DsMu"] = "switch( mcMatch ('[ [B_s~0]cc => D_s+ mu- Neutrino ]CC',1)   , 1 , 0 )"
            LoKiVariables.Variables["TM_DsTau"] = "switch( mcMatch ('[ [B_s~0]cc => D_s+ tau- Neutrino ]CC',1)   , 1 , 0 )"
    
            LoKiVariables.Variables["TM_DsstarMu_Dsg"] = "switch( mcMatch ('[ [B_s~0]cc => (D*_s+ => D_s+  gamma) mu- Neutrino ]CC',1)   , 1 , 0 )"
            LoKiVariables.Variables["TM_DsstarMu_Dspi0"] = "switch( mcMatch ('[ [B_s~0]cc => (D*_s+ => D_s+  pi0) mu- Neutrino ]CC',1)   , 1 , 0 )"
            LoKiVariables.Variables["TM_DsstarTau_Dsg"] = "switch( mcMatch ('[ [B_s~0]cc => (D*_s+ => D_s+  gamma) tau- Neutrino ]CC',1)   , 1 , 0 )"
            LoKiVariables.Variables["TM_DsstarTau_Dspi0"] = "switch( mcMatch ('[ [B_s~0]cc => (D*_s+ => D_s+  pi0) tau- Neutrino ]CC',1)   , 1 , 0 )"
    
            LoKiVariables.Variables["TM_Dsstar0Mu_Dspi0"] = "switch( mcMatch ('[ [B_s~0]cc => (D*_s0+ => D_s+   pi0) mu- Neutrino ]CC',1)   , 1 , 0 )"
            LoKiVariables.Variables["TM_Dsstar0Mu_Dsstarg_Dsg"] = "switch( mcMatch ('[ [B_s~0]cc => (D*_s0+ => (D*_s+ => D_s+  gamma)  gamma) mu- Neutrino ]CC',1)   , 1 , 0 )"
            LoKiVariables.Variables["TM_Dsstar0Mu_Dsstarg_Dspi0"] = "switch( mcMatch ('[ [B_s~0]cc => (D*_s0+ => (D*_s+ => D_s+  pi0)  gamma) mu- Neutrino ]CC',1)   , 1 , 0 )"
            LoKiVariables.Variables["TM_Dsstar0Mu_Dspipi"] = "switch( mcMatch ('[ [B_s~0]cc => (D*_s0+ => D_s+   pi+ pi-) mu- Neutrino ]CC',1)   , 1 , 0 )"
            LoKiVariables.Variables["TM_Dsstar0Mu_Dspi0pi0"] = "switch( mcMatch ('[ [B_s~0]cc => (D*_s0+ => D_s+   pi0 pi0) mu- Neutrino ]CC',1)   , 1 , 0 )"
            LoKiVariables.Variables["TM_Dsstar0Tau_Dspi0"] = "switch( mcMatch ('[ [B_s~0]cc => (D*_s0+ => D_s+   pi0) tau- Neutrino ]CC',1)   , 1 , 0 )"
            LoKiVariables.Variables["TM_Dsstar0Tau_Dsstarg_Dsg"] = "switch( mcMatch ('[ [B_s~0]cc => (D*_s0+ => (D*_s+ => D_s+  gamma)  gamma) tau- Neutrino ]CC',1)   , 1 , 0 )"
            LoKiVariables.Variables["TM_Dsstar0Tau_Dsstarg_Dspi0"] = "switch( mcMatch ('[ [B_s~0]cc => (D*_s0+ => (D*_s+ => D_s+  pi0)  gamma) tau- Neutrino ]CC',1)   , 1 , 0 )"
            LoKiVariables.Variables["TM_Dsstar0Tau_Dspipi"] = "switch( mcMatch ('[ [B_s~0]cc => (D*_s0+ => D_s+   pi+ pi-) tau- Neutrino ]CC',1)   , 1 , 0 )"
            LoKiVariables.Variables["TM_Dsstar0Tau_Dspi0pi0"] = "switch( mcMatch ('[ [B_s~0]cc => (D*_s0+ => D_s+   pi0 pi0) tau- Neutrino ]CC',1)   , 1 , 0 )"
    
            LoKiVariables.Variables["TM_Ds12460_Dsstarpi0_Dsg"] = "switch( mcMatch ('[ [B_s~0]cc => (D_s1(2460)+ => (D*_s+ => D_s+  gamma) pi0) mu- Neutrino ]CC',1)   , 1 , 0 )"
            LoKiVariables.Variables["TM_Ds12460_Dsstarpi0_Dspi0"] = "switch( mcMatch ('[ [B_s~0]cc => (D_s1(2460)+ => (D*_s+ => D_s+  pi0) pi0) mu- Neutrino ]CC',1)   , 1 , 0 )"
            LoKiVariables.Variables["TM_Ds12460_Dspipi"] = "switch( mcMatch ('[ [B_s~0]cc => (D_s1(2460)+ => D_s+  pi+ pi-) mu- Neutrino ]CC',1)   , 1 , 0 )"
            LoKiVariables.Variables["TM_Ds12460_Dspi0pi0"] = "switch( mcMatch ('[ [B_s~0]cc => (D_s1(2460)+ => D_s+  pi0 pi0) mu- Neutrino ]CC',1)   , 1 , 0 )"
    
            LoKiVariables.Variables["TM_Ds12536Mu_Dsstarpi0_Dspi0"] = "switch( mcMatch ('[ [B_s~0]cc => (D_s1(2536)+ => (D*_s0+ => D_s+   pi0) pi0) mu- Neutrino ]CC',1)   , 1 , 0 )"
            LoKiVariables.Variables["TM_Ds12536Mu_Dsstarg_Dsg"] = "switch( mcMatch ('[ [B_s~0]cc => (D_s1(2536)+ => (D*_s0+ => (D*_s+ => D_s+  gamma)  gamma) pi0) mu- Neutrino ]CC',1)   , 1 , 0 )"
            LoKiVariables.Variables["TM_Ds12536Mu_Dsstarg_Dspi0"] = "switch( mcMatch ('[ [B_s~0]cc => (D_s1(2536)+ => (D*_s0+ => (D*_s+ => D_s+  pi0)  gamma) pi0) mu- Neutrino ]CC',1)   , 1 , 0 )"
            LoKiVariables.Variables["TM_Ds12536Mu_Dsstarpi0_Dspipi"] = "switch( mcMatch ('[ [B_s~0]cc => (D_s1(2536)+ => (D*_s0+ => D_s+   pi+ pi-) pi0) mu- Neutrino ]CC',1)   , 1 , 0 )"
            LoKiVariables.Variables["TM_Ds12536Mu_Dsstarpi0_Dspi0pi0"] = "switch( mcMatch ('[ [B_s~0]cc => (D_s1(2536)+ => (D*_s0+ => D_s+   pi0 pi0) pi0) mu- Neutrino ]CC',1)   , 1 , 0 )"
            LoKiVariables.Variables["TM_Ds12536Mu_Dsg"] = "switch( mcMatch ('[ [B_s~0]cc => (D_s1(2536)+ => D_s+   gamma) mu- Neutrino ]CC',1)   , 1 , 0 )"
            LoKiVariables.Variables["TM_Ds12536Mu_Dspipi"] = "switch( mcMatch ('[ [B_s~0]cc => (D_s1(2536)+ => D_s+   pi+ pi-) mu- Neutrino ]CC',1)   , 1 , 0 )"
            LoKiVariables.Variables["TM_Ds12536Mu_Dspi0pi0"] = "switch( mcMatch ('[ [B_s~0]cc => (D_s1(2536)+ => D_s+   pi0 pi0) mu- Neutrino ]CC',1)   , 1 , 0 )"
            LoKiVariables.Variables["TM_Ds12536Tau_Dsstarpi0_Dspi0"] = "switch( mcMatch ('[ [B_s~0]cc => (D_s1(2536)+ => (D*_s0+ => D_s+   pi0) pi0) tau- Neutrino ]CC',1)   , 1 , 0 )"
            LoKiVariables.Variables["TM_Ds12536Tau_Dsstarg_Dsg"] = "switch( mcMatch ('[ [B_s~0]cc => (D_s1(2536)+ => (D*_s0+ => (D*_s+ => D_s+  gamma)  gamma) pi0) tau- Neutrino ]CC',1)   , 1 , 0 )"
            LoKiVariables.Variables["TM_Ds12536Tau_Dsstarg_Dspi0"] = "switch( mcMatch ('[ [B_s~0]cc => (D_s1(2536)+ => (D*_s0+ => (D*_s+ => D_s+  pi0)  gamma) pi0) tau- Neutrino ]CC',1)   , 1 , 0 )"
            LoKiVariables.Variables["TM_Ds12536Tau_Dsstarpi0_Dspipi"] = "switch( mcMatch ('[ [B_s~0]cc => (D_s1(2536)+ => (D*_s0+ => D_s+   pi+ pi-) pi0) tau- Neutrino ]CC',1)   , 1 , 0 )"
            LoKiVariables.Variables["TM_Ds12536Tau_Dsstarpi0_Dspi0pi0"] = "switch( mcMatch ('[ [B_s~0]cc => (D_s1(2536)+ => (D*_s0+ => D_s+   pi0 pi0) pi0) tau- Neutrino ]CC',1)   , 1 , 0 )"
            LoKiVariables.Variables["TM_Ds12536Tau_Dsg"] = "switch( mcMatch ('[ [B_s~0]cc => (D_s1(2536)+ => D_s+   gamma) tau- Neutrino ]CC',1)   , 1 , 0 )"
            LoKiVariables.Variables["TM_Ds12536Tau_Dspipi"] = "switch( mcMatch ('[ [B_s~0]cc => (D_s1(2536)+ => D_s+   pi+ pi-) tau- Neutrino ]CC',1)   , 1 , 0 )"
            LoKiVariables.Variables["TM_Ds12536Tau_Dspi0pi0"] = "switch( mcMatch ('[ [B_s~0]cc => (D_s1(2536)+ => D_s+   pi0 pi0) tau- Neutrino ]CC',1)   , 1 , 0 )"
    
    
        for branch in tuple.Branches: #add ETA and MIPCHI2DV_PV to all final state particles (all with a "_" in the branch name)
            if re.match(".*_.*",branch):
                LoKiVariables = getattr(tuple,branch).addTupleTool("LoKi::Hybrid::TupleTool/LoKiVariables_"+branch)
                LoKiVariables.Variables = {
                        "MIPCHI2DV_PV" : "MIPCHI2DV(PRIMARY)",
                        "ETA"          : "ETA"
                        }
    
        myTupleToolTISTOS = TupleToolTISTOS('TupleToolTISTOS')
        myTupleToolTISTOS.addTool(L0TriggerTisTos())
        myTupleToolTISTOS.addTool(TriggerTisTos())
        myTupleToolTISTOS.TriggerList=[
            "L0MuonDecision",
            "L0DiMuonDecision",
            "Hlt2TopoMu2BodyBBDTDecision",
            "Hlt2SingleMuonDecision",
            "Hlt2SingleMuonLowPTDecision"
            ]
        if RecoAs == "DsMuNu":
            myTupleToolTISTOS.TriggerList+=[
                "Hlt2Topo2BodyBBDTDecision",
                "Hlt2Topo3BodyBBDTDecision",
                "Hlt2Topo4BodyBBDTDecision",
                "Hlt2TopoMu3BodyBBDTDecision",
                "Hlt2TopoMu4BodyBBDTDecision",
            ]
        myTupleToolTISTOS.VerboseL0   = True
        myTupleToolTISTOS.VerboseHlt1 = True
        myTupleToolTISTOS.VerboseHlt2 = True
        myTupleToolTISTOS.TUS         = True
        myTupleToolTISTOS.TPS         = True
    
        tuple.Bs.addTool(myTupleToolTISTOS , name = 'TupleToolTISTOS' )
        tuple.Bs.ToolList   += [ "TupleToolTISTOS"]
    
        tuple.muon_p.addTool(myTupleToolTISTOS , name = 'TupleToolTISTOS' )
        tuple.muon_p.ToolList   += [ "TupleToolTISTOS"]
    
        #if RecoAs == "DsMuNu":
        #    tuple.Ds.addTool(myTupleToolTISTOS , name = 'TupleToolTISTOS' )
        #    tuple.Ds.ToolList   += [ "TupleToolTISTOS"]
        #else:
        tuple.kaon_m.addTool(myTupleToolTISTOS , name = 'TupleToolTISTOS' )
        tuple.kaon_m.ToolList   += [ "TupleToolTISTOS"]
    
    
        from Configurables import TupleToolConeIsolation
        tuple.kaon_m.addTupleTool("TupleToolConeIsolation")
        tuple.kaon_m.TupleToolConeIsolation.FillAsymmetry     = True
        tuple.kaon_m.TupleToolConeIsolation.FillDeltas        = True
        tuple.kaon_m.TupleToolConeIsolation.FillComponents    = True
        tuple.kaon_m.TupleToolConeIsolation.MinConeSize       = 0.5
        tuple.kaon_m.TupleToolConeIsolation.SizeStep          = 0.5
        tuple.kaon_m.TupleToolConeIsolation.MaxConeSize       = 2.0
        tuple.kaon_m.TupleToolConeIsolation.FillPi0Info       = True
        tuple.kaon_m.TupleToolConeIsolation.FillMergedPi0Info = True
    
        tuple.muon_p.addTupleTool("TupleToolConeIsolation")
        tuple.muon_p.TupleToolConeIsolation.FillAsymmetry     = True
        tuple.muon_p.TupleToolConeIsolation.FillDeltas        = True
        tuple.muon_p.TupleToolConeIsolation.FillComponents    = True
        tuple.muon_p.TupleToolConeIsolation.MinConeSize       = 0.5
        tuple.muon_p.TupleToolConeIsolation.SizeStep          = 0.5
        tuple.muon_p.TupleToolConeIsolation.MaxConeSize       = 2.0
        if RecoAs == "DsMuNu":
            tuple.kaon_p.addTupleTool("TupleToolConeIsolation")
            tuple.kaon_p.TupleToolConeIsolation.FillAsymmetry     = True
            tuple.kaon_p.TupleToolConeIsolation.FillDeltas        = True
            tuple.kaon_p.TupleToolConeIsolation.FillComponents    = True
            tuple.kaon_p.TupleToolConeIsolation.MinConeSize       = 0.5
            tuple.kaon_p.TupleToolConeIsolation.SizeStep          = 0.5
            tuple.kaon_p.TupleToolConeIsolation.MaxConeSize       = 2.0
            tuple.kaon_p.TupleToolConeIsolation.FillPi0Info       = True
            tuple.kaon_p.TupleToolConeIsolation.FillMergedPi0Info = True
        
            tuple.pi_p.addTupleTool("TupleToolConeIsolation")
            tuple.pi_p.TupleToolConeIsolation.FillAsymmetry     = True
            tuple.pi_p.TupleToolConeIsolation.FillDeltas        = True
            tuple.pi_p.TupleToolConeIsolation.FillComponents    = True
            tuple.pi_p.TupleToolConeIsolation.MinConeSize       = 0.5
            tuple.pi_p.TupleToolConeIsolation.SizeStep          = 0.5
            tuple.pi_p.TupleToolConeIsolation.MaxConeSize       = 2.0
    
            tuple.Ds.addTupleTool("TupleToolConeIsolation")
            tuple.Ds.TupleToolConeIsolation.FillAsymmetry     = True
            tuple.Ds.TupleToolConeIsolation.FillDeltas        = True
            tuple.Ds.TupleToolConeIsolation.FillComponents    = True
            tuple.Ds.TupleToolConeIsolation.MinConeSize       = 0.5
            tuple.Ds.TupleToolConeIsolation.SizeStep          = 0.5
            tuple.Ds.TupleToolConeIsolation.MaxConeSize       = 2.0
    
        if( DaVinci().Simulation ):
            MCTruth=tuple.addTupleTool("TupleToolMCTruth")
            try:
                MCTruth.addTupleTool("MCTupleToolHierarchy")
            except AttributeError:
                pass
            try:
                MCTruth.addTupleTool("MCTupleToolKinematic")
            except AttributeError:
                pass
            #MCTruth.addTupleTool("MCTupleToolInteractions")
            MCTruth.addTupleTool("MCTupleToolReconstructed")
            if( (  DaVinci().Simulation and RecoAs == "KMuNu" ) or(  DaVinci().Simulation and RecoAs == "DsMuNu" )):
                MCTruth.addTupleTool("MCTupleToolSemileptonic")
                
            tuple.ToolList +=  ["TupleToolGeneration"]
    
        tuple.ToolList +=  [
            "TupleToolPropertime"
            , "TupleToolRecoStats"
            , "TupleToolSLTools"
            , "TupleToolTrackPosition"
            # , "TupleToolRICHPid"
            , "TupleToolRecoStats"
            , "TupleToolMuonPid"
            , "TupleToolGeneration"
            ]
        #When we have the inclusive bkg of Adlene we will not know which kind of specific bkg of the event
        #Maybe it will become useful to add the Tool TupleToolHierarcy to know the specific channel (need to check if this is true)
        #Maybe also the Ancestor (the upper one) can be useful to know (need to understand better)

    return tuples


def makeEventTuple():
    from Configurables import EventTuple , TupleToolEventInfo
    etuple = EventTuple()
    return etuple


def makeMCDecayTreeTuple():
    from Configurables import MCDecayTreeTuple, MCTupleToolKinematic, MCTupleToolHierarchy, LoKi__Hybrid__MCTupleTool, LoKi__Hybrid__EvtTupleTool, TupleToolEventInfo


    Trees = []

    #Ds Decays
    DecayTree = MCDecayTreeTuple('TupleBs2DsMuNu')
    DecayTree.Decay = '( [ [B_s0]nos -> ^( D_s- => ^K+ ^K- ^pi- ) ^mu+ ... ]CC ) || ([ [B_s~0]os -> ^( D_s- => ^K+ ^K- ^pi- ) ^mu+ ... ]CC )'
    Trees.append( DecayTree )

    #DsStar Decays
    DecayTree = MCDecayTreeTuple('TupleBs2DsStarMuNu_pi0')
    DecayTree.Decay = '( [ [B_s0]nos -> ^(  D*_s- => ^( D_s- => ^K+ ^K- ^pi- ) ^pi0 ) ^mu+ ... ]CC ) || ([ [B_s~0]os -> ^(  D*_s- => ^( D_s- => ^K+ ^K- ^pi- ) ^pi0 ) ^mu+ ... ]CC )'
    Trees.append( DecayTree )

    DecayTree = MCDecayTreeTuple('TupleBs2DsStarMuNu_g')
    DecayTree.Decay = '( [ [B_s0]nos -> ^(  D*_s- => ^( D_s- => ^K+ ^K- ^pi- ) ^gamma ) ^mu+ ... ]CC ) || ([ [B_s~0]os -> ^(  D*_s- => ^( D_s- => ^K+ ^K- ^pi- ) ^gamma ) ^mu+ ... ]CC )'
    Trees.append( DecayTree )

    #DsStar0 Decays
    DecayTree = MCDecayTreeTuple('TupleBs2DsStar0MuNu_pi0')
    DecayTree.Decay = '( [ [B_s0]nos -> ^(  D*_s0- => ^( D_s- => ^K+ ^K- ^pi- ) ^pi0 ) ^mu+ ... ]CC ) || ([ [B_s~0]os -> ^(  D*_s0- => ^( D_s- => ^K+ ^K- ^pi- ) ^pi0 ) ^mu+ ... ]CC )'
    Trees.append( DecayTree )


    # These Two Decay Descriptors may be buggy. No events in the Analysis NTuple. But they may not be in the MC anyway?
    DecayTree = MCDecayTreeTuple('TupleBs2DsStar0MuNu_g')
    DecayTree.Decay = '( [ [B_s0]nos -> ^( D*_s0- => ^(  D*_s- => ^( D_s- => ^K+ ^K- ^pi- ) ^gamma ) ^gamma ) ^mu+ ... ]CC ) || ([ [B_s~0]os -> ^( D*_s0- => ^(  D*_s- => ^( D_s- => ^K+ ^K- ^pi- ) ^gamma ) ^gamma ) ^mu+ ... ]CC )'
    Trees.append( DecayTree )

    DecayTree = MCDecayTreeTuple('TupleBs2DsStar0MuNu_pi0pi0')
    DecayTree.Decay = '( [ [B_s0]nos -> ^(  D*_s0- => ^( D_s- => ^K+ ^K- ^pi- ) ^pi0 ^pi0 ) ^mu+ ... ]CC ) || ([ [B_s~0]os -> ^(  D*_s0- => ^( D_s- => ^K+ ^K- ^pi- ) ^pi0 ^pi0 ) ^mu+ ... ]CC )'
    Trees.append( DecayTree )

    DecayTree = MCDecayTreeTuple('TupleBs2DsStar0MuNu_pippim')
    DecayTree.Decay = '( [ [B_s0]nos -> ^(  D*_s0- => ^( D_s- => ^K+ ^K- ^pi- ) ^pi+ ^pi- ) ^mu+ ... ]CC ) || ([ [B_s~0]os -> ^(  D*_s0- => ^( D_s- => ^K+ ^K- ^pi- ) ^pi+ ^pi- ) ^mu+ ... ]CC )'
    Trees.append( DecayTree )


    #DsStar1(2460) Decays
    DecayTree = MCDecayTreeTuple('TupleBs2DsStar12460MuNu_pi0')
    DecayTree.Decay = '( [ [B_s0]nos -> ^( D_s1(2460)- => ^(  D*_s- => ^( D_s- => ^K+ ^K- ^pi- ) ^gamma ) ^pi0 ) ^mu+ ... ]CC ) || ([ [B_s~0]os -> ^( D_s1(2460)- => ^(  D*_s- => ^( D_s- => ^K+ ^K- ^pi- ) ^gamma ) ^pi0 ) ^mu+ ... ]CC )'
    Trees.append( DecayTree )

    DecayTree = MCDecayTreeTuple('TupleBs2DsStar12460MuNu_pi0pi0')
    DecayTree.Decay = '( [ [B_s0]nos -> ^( D_s1(2460)- => ^( D_s- => ^K+ ^K- ^pi- ) ^pi0 ^pi0 ) ^mu+ ... ]CC ) || ([ [B_s~0]os -> ^(  D_s1(2460)- => ^( D_s- => ^K+ ^K- ^pi- ) ^pi0 ^pi0 ) ^mu+ ... ]CC )'
    Trees.append( DecayTree )

    DecayTree = MCDecayTreeTuple('TupleBs2DsStar12460MuNu_pippim')
    DecayTree.Decay = '( [ [B_s0]nos -> ^( D_s1(2460)- => ^( D_s- => ^K+ ^K- ^pi- ) ^pi+ ^pi- ) ^mu+ ... ]CC ) || ([ [B_s~0]os -> ^(  D_s1(2460)- => ^( D_s- => ^K+ ^K- ^pi- ) ^pi+ ^pi- ) ^mu+ ... ]CC )'
    Trees.append( DecayTree )


    # Tauonic Decays
    DecayTree = MCDecayTreeTuple('TupleBs2DsTauNu')
    DecayTree.Decay = '( [ [B_s0]nos -> ^( D_s- => ^K+ ^K- ^pi- ) ^( tau+ => ^mu+ ^nu_tau~ ^nu_mu )  ... ]CC ) || ([ [B_s~0]os -> ^( D_s- => ^K+ ^K- ^pi- ) ^( tau+ => ^mu+ ^nu_tau~ ^nu_mu ) ... ]CC )'
    Trees.append( DecayTree )

    DecayTree = MCDecayTreeTuple('TupleBs2DsStarTauNu_g')
    DecayTree.Decay = '( [ [B_s0]nos -> ^(  D*_s- => ^( D_s- => ^K+ ^K- ^pi- ) ^gamma ) ^( tau+ => ^mu+ ^nu_tau~ ^nu_mu ) ... ]CC ) || ([ [B_s~0]os -> ^(  D*_s- => ^( D_s- => ^K+ ^K- ^pi- ) ^gamma ) ^( tau+ => ^mu+ ^nu_tau~ ^nu_mu ) ... ]CC )'
    Trees.append( DecayTree )


    for tree in Trees:
        tree.addTool(MCTupleToolKinematic())
        tree.MCTupleToolKinematic.Verbose=True
    
    
        tree.ToolList += [
                 "MCTupleToolHierarchy"
                ,"MCTupleToolSemileptonic"
                ,"TupleToolRecoStats"
                ,"MCTupleToolKinematic"
                ]


    return Trees





def makeSeq(mode,isopath='Phys/StdNoPIDsVeloPions/Particles'):
    TupleSeq   = GaudiSequencer('TupSeq'+mode)
    #stripping signalline = ""

    print InputLocation, RecoAs


    isdata = not DaVinci().Simulation

    # TO MAKE LOOSETUPLE TO STUDY THE STRIPPING EFF
    # CandidateMaker = makeLooseTuple_Bs2KMuNu()
    # myTuple = makeTuple(CandidateMaker.outputLocation(), mode, isopath=isopath)
    # TupleSeq.Members += [CandidateMaker.sequence(), myTuple]

    if( RecoAs == "JpsiPhi" ):
        CandidateMaker = makeBs2JpsiPhiSel(isdata)
        myTuples = makeTuple(CandidateMaker.outputLocation(), mode, isopath=isopath)
        myTuples = makeTuple(CandidateMaker.outputLocation(), mode, isopath=isopath)
        TupleSeq.Members += [CandidateMaker.sequence()]
        TupleSeq.Members += myTuples
    elif( RecoAs == "JpsiK" ):
        CandidateMaker = makeBu2JpsiKSel(isdata)
        myTuples = makeTuple(CandidateMaker.outputLocation(), mode, isopath=isopath)
        TupleSeq.Members += [CandidateMaker.sequence()]
        TupleSeq.Members += myTuples

    # elif( ("MC" in mode) and (("BKG" in mode) or ("Bs2KMuNu_SIGNAL" in mode)) ):
    #     isSS = False
    #     if "SS" in mode: isSS = True
    #     CandidateMaker = makeNewStripSel_Bs2KMuNu(isSS)
    #     myTuple = makeTuple(CandidateMaker.outputLocation(), mode, isopath=isopath)
    #     TupleSeq.Members += [CandidateMaker.sequence(), myTuple]
    
    elif ( RecoAs == "DsMuNu" ):
        if isRestrip:
            confname='B2DMuNuX'
            # NOTE: this will work only if you inserted correctly the 
            # default_config dictionary in the code where your LineBuilder 
            # is defined.
            from StrippingSelections import buildersConf
            confs = buildersConf(["SL"])
            from StrippingSelections.Utils import lineBuilder, buildStreamsFromBuilder
            streams = buildStreamsFromBuilder(confs,confname)
            MyLines = [
                #'StrippingB2XuMuNuBs2KLine',
                #'Strippingb2DsMuXB2DMuNuXLine'
                'StrippingB2DMuNuX_Ds',
                'StrippingB2DMuNuX_Ds_FakeMuon'
            
                #'StrippingB2XuMuNuBs2K_FakeKMuLine'
                ]
            
            MyStream = StrippingStream("b2KMu.newstrip")
            for stream in streams:
              for line in stream.lines:
                # Set prescales to 1.0 if necessary
                line._prescale = 1.0
                if line.name() in MyLines :
                    MyStream.appendLines( [ line ] )
            
            from Configurables import ProcStatusCheck
            
            sc = StrippingConf( Streams = [ MyStream ],
                                MaxCandidates = 2000,
                                Verbose = True,
                                )
            
                        
            MyStream.sequence().IgnoreFilterPassed = False # so that we do not get all events written out

            from Configurables import EventNodeKiller
            event_node_killer = EventNodeKiller('StripKiller')
            event_node_killer.Nodes = ['/Event/AllStreams', '/Event/Strip']
            DaVinci().appendToMainSequence( [ event_node_killer, sc.sequence() ] )
            # myTuple = makeTuple("Phys/B2XuMuNuBs2KLine/Particles", mode, isopath=isopath)

        for Fake in ["", "_FakeMuon"]:
            print InputLocation, RecoAs
            TupleSeq.Members += makeTuple(AutomaticData(InputLocation.format(Fake=Fake)).outputLocation(), mode, isopath=isopath, DTTAppend=Fake) # old stripping for MC, it's ok for the data too


        TupleSeq.ModeOR          = True
        TupleSeq.ShortCircuit    = False

    elif ( RecoAs == "KMuNu" ):
        
        for Fake in ["", "_FakeK", "_FakeKMu", "_FakeMu"]:
            for SS in ["", "SS"]:
                print InputLocation, RecoAs
                TupleSeq.Members += makeTuple(AutomaticData(InputLocation.format(Fake=Fake, SS=SS)).outputLocation(), mode, isopath=isopath, DTTAppend=Fake) # old stripping for MC, it's ok for the data too


        TupleSeq.ModeOR          = True
        TupleSeq.ShortCircuit    = False


    return TupleSeq


#            
# ====================================================================== 
def createVeloTracks():
    # still to add to thia function: MC associators
    from Configurables import ChargedProtoParticleMaker, DaVinci
    veloprotos = ChargedProtoParticleMaker("VeloProtoPMaker")
    veloprotos.Inputs = ["Rec/Track/Best"]
    veloprotos.Output = "Rec/ProtoP/myVeloProtoPMaker/ProtoParticles"
    DaVinci().appendToMainSequence( [ veloprotos ])
    from Configurables       import ProtoParticleCALOFilter, CombinedParticleMaker,NoPIDsParticleMaker
    from CommonParticles.Utils import trackSelector, updateDoD
    algorithm = NoPIDsParticleMaker('StdNoPIDsVeloPions',  Particle = 'pion',  )
    algorithm.Input = veloprotos.Output
    selector = trackSelector ( algorithm , trackTypes = ['Velo'] )
    locations = updateDoD ( algorithm )
    return algorithm
    
def configIso():
    from Configurables import ChargedProtoParticleMaker, DaVinci
    Alltracksprotos = ChargedProtoParticleMaker("AllProtoPMaker")
    Alltracksprotos.Inputs = ["Rec/Track/Best"]
    Alltracksprotos.Output = "Rec/ProtoP/myAllProtoPMaker/ProtoParticles"
    DaVinci().appendToMainSequence( [ Alltracksprotos ])
    from Configurables       import ProtoParticleCALOFilter, CombinedParticleMaker,NoPIDsParticleMaker
    from CommonParticles.Utils import trackSelector, updateDoD
    algorithm = NoPIDsParticleMaker('StdNoPIDsAlltracksPions',  Particle = 'pion' ,  )
    algorithm.Input = Alltracksprotos.Output
    selector = trackSelector ( algorithm , trackTypes = ['Velo', 'Long' , 'Upstream'] )
    locations = updateDoD ( algorithm )
    return algorithm

def mergeTracks():
    LongPions      = DataOnDemand( Location ='Phys/StdAllNoPIDsPions/Particles' )
    UpStreamPions  = DataOnDemand( Location ='Phys/StdNoPIDsUpPions/Particles'  )
    VeloPions      = DataOnDemand( Location ='Phys/StdNoPIDsVeloPions/Particles')
    AllPions       = MergedSelection( "AllPions", RequiredSelections =[ LongPions , UpStreamPions , VeloPions  ] )
    AllPions_seq   = SelectionSequence('AllPions_seq', TopSelection = AllPions)
    return AllPions_seq
#############################################################


#This is the only function to be used in gangajob.py and testBsKMuNu.py
AllPions_seq=None

def SetupBsKMuNuAlgos():
    DaVinci().UserAlgorithms += [createVeloTracks()]
    DaVinci().UserAlgorithms += [configIso()]
    global AllPions_seq
    AllPions_seq = mergeTracks()
    DaVinci().UserAlgorithms += [AllPions_seq]

def setupBsKMuNu(mode):
    if( DaVinci().Simulation ):
        DaVinci().UserAlgorithms += [makeEventTuple(), makeSeq(mode,isopath=AllPions_seq.outputLocation())]
        DaVinci().UserAlgorithms += makeMCDecayTreeTuple()
    else:
        DaVinci().UserAlgorithms += [makeSeq(mode,isopath=AllPions_seq.outputLocation())]







