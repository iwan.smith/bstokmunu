
# first copy the options file to a temp location:
from shutil import copyfile
import getpass
uname = getpass.getuser()
from random import random


tempoptionsfile = "/tmp/" + uname + "/Bs_TupleMaker_" + str(random())[2:] + ".py"

print tempoptionsfile

copyfile('Phys/BsToKMuNu/scripts/Bs_DsMuNu_TupleMaker/Bs_TupleMaker.py', tempoptionsfile)



# Load the database stored in the local file 

RunDB = {}

with open("Phys/BsToKMuNu/scripts/Bs_DsMuNu_TupleMaker/Database", "r") as DB_local:

    Columns = DB_local.readline().split()
    
    
    for line in DB_local:
        Columns = line.split()

        # Skip any empty lines
        if len(Columns) == 0:
            continue
        
        Key     = Columns[0]
        Columns = Columns[1:]
           
        RunDB[Key] = Columns


RunMultiple = type(mode) == type([])
if RunMultiple:
    modelist = mode
    mode = modelist[0]
else:
    modelist = [mode]

try:
    RecoAs, isRestrip, Path, Input, tuplename, dddbTag, condDBTag = RunDB[mode]
except:
    # Some annoyances with data. The path contains a space, and there are no tags
    RecoAs, isRestrip, Path1, Path2, Input, tuplename = RunDB[mode]
    Path = Path1 + " " + Path2
    dddbTag, condDBTag = "", ""

isRestrip = eval(isRestrip)
Path      = Path     .format(Polarity=Polarity)
tuplename = tuplename.format(Polarity=Polarity)
condDBTag = condDBTag.format(Polarity= "mu" if (Polarity == "Up") else "md")
isMC      = "/MC/" in Path

Year = ""
if ( "/MC/2011/" in Path or "Collision11" in Path):
    Year = "2011"
if ( "/MC/2012/" in Path or "Collision12" in Path):
    Year = "2012"



import os

test = True


DaVinciVersion = 'v39r1p1'
nEvt = -1
nFreq = 25000

def getTheDTS(mode):
    bk_query = BKQuery(path=Path)
    ds = bk_query.getDataset()
    print "getting the data sample for : ", ds
    return ds.files


#myApp = prepareGaudiExec('DaVinci','v41r2', myPath='.')	
#job.application = myApp

import os
pwd = os.getcwd()


myApplication = GaudiExec()
myApplication.directory = pwd
myApplication.platform = 'x86_64-slc6-gcc49-opt'

job = Job(name = mode+"_"+Polarity )# ,backend=Dirac() )
job.application = myApplication
job.application.options = [tempoptionsfile]



data = LHCbDataset()

if(not test):
    print "Calls splitter"
    job.splitter = SplitByFiles(filesPerJob = 10, maxFiles = -1, ignoremissing = True)
    job.backend = Dirac()
    job.outputfiles = [DiracFile(tuplename)]
    data.files += getTheDTS(mode)

else:
    job.backend = Local()
    job.outputfiles = [LocalFile(tuplename)]
    if ( isMC ):
        data.files.append(LocalFile('/eos/lhcb/grid/prod/lhcb/MC/2012/ALLSTREAMS.DST/00021467/0000/00021467_00000002_1.allstreams.dst') ) # for Bs2DsMuNu mode
    else:
        data.files.append(LocalFile('/eos/lhcb/grid/prod/lhcb/LHCb/Collision12/SEMILEPTONIC.DST/00050929/0000/00050929_00001833_1.semileptonic.dst') ) # for Bs2DsMuNu mode
        
    nEvt = 10000
    nFreq = 3000


extraopts = ""
extraopts += "from Configurables import DaVinci\n"
extraopts += "DaVinci().TupleFile = \""+tuplename+"\"\n"
extraopts += "DaVinci().DataType = \""+Year+"\"\n" 
extraopts += "DaVinci().EvtMax = "+str(nEvt)+"\n" 
extraopts += "DaVinci().PrintFreq = "+str(nFreq)+"\n" 
extraopts += "DaVinci().InputType = \"DST\"\n" 
extraopts += "DaVinci().Simulation = "+str(isMC)+"\n" 
extraopts += "DaVinci().MainOptions = \"\"\n" 
extraopts += "DaVinci().SkipEvents = 0\n" 
extraopts += "DaVinci().Lumi = "+str(not isMC)+"\n" 

if(isMC):
    extraopts += "from Configurables import CondDB\n"
    extraopts += "DaVinci().DDDBtag = \"" + dddbTag + "\"\n"
    extraopts += "DaVinci().CondDBtag =\"" + condDBTag + "\"\n"
else:
    extraopts += "from Configurables import CondDB\n"
    extraopts += "CondDB().UseLatestTags = [\""+Year+"\"]\n"


extraopts += "isRestrip ="+str(isRestrip)+"\n"
extraopts += "UseStrippingSel = True\n\n"

extraopts += "SetupBsKMuNuAlgos()\n"

for m in modelist:

    try:
        RecoAs, none, none, Input, none, none, none = RunDB[m]
    except:
        # Some annoyances with data. The path contains a space, and there are no tags
        RecoAs, none, none, none, Input, none = RunDB[m]
    
    extraopts += "InputLocation = \"" + Input + "\"\n"
    extraopts += "RecoAs = \"" + RecoAs + "\"\n"
    extraopts +="setupBsKMuNu(\""+m+"\")\n"



# In the past the extraoptions were glued onto the file in question, now we need to explicitly append then to the file
#job.application.extraOpts = extraopts
with open(tempoptionsfile, "a") as tempoptions:
    tempoptions.write(extraopts)

job.inputdata  = data


# for LSF :
print "Finally submit the job:", job.name
job.submit()
print "job submitted:", job.name
