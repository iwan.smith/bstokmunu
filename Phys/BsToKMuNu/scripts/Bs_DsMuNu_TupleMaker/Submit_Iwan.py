
RunDB = {}

with open("Database", "r") as DB_local:

    Columns = DB_local.readline().split()
            
    for line in DB_local:
        Columns = line.split()
        
        Key     = Columns[0]
        Columns = Columns[1:]
           
        RunDB[Key] = Columns

Polarity = "Up"

isMC, isRestrip, Path, tuplename, dddbTag, condDBTag, Year = RunDB["MC12_Bs2DsMuNu_Cocktail_SIGNAL"]


isMC      = eval(isMC)
isRestrip = eval(isRestrip)
Path      = Path     .format(Polarity=Polarity)
tuplename = tuplename.format(Polarity=Polarity)
condDBTag = condDBTag.format(Polarity= "mu" if (Polarity == "Up") else "md")
