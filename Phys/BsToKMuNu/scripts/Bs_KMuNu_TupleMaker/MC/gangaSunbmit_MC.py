# author : Basem Khanji bkhanji@lxplus.cern.ch 
# Script to submit MC jobs for Bs2Kmunu based on
# its EventType(s) (gauss).
# To run you need to:
#   SetupProject Ganga 
#   lb-run LHCbDirac ganga -i gangaSunbmit_MC.py
# Jobs will be separated by polarity/year/pythia version
# their name will contain these infos (eventtype, name of decay, etc, ...) 
# Optionfile will be assigned the correct DataBase tags 
# This is wrapped around Vanya's script:
# /afs/cern.ch/user/i/ibelyaev/public/scripts/dirac-bookkeeping-get-prodinfo-eventtype.py, Twiki :
# https://twiki.cern.ch/twiki/bin/view/LHCb/DownloadAndBuild#Look_for_available_files_in_the
# please run SetupProject LHCbDirac ...
import os , re , subprocess ,ast , time
#os.system('SetupProject.sh LHCbDirac')
#os.system('SetupProject.csh LHCbDirac')
os.system('SetupProject.sh Gauss')
# make sure you setup needed projects
# Verbosity level : 
Isverbose = True
#================================================================================================================
#================================================================================================================
# Some helper functions
# add a function that makes a report how many jobs with no tags , how many jobs submitted how many jobs per event
# what are the sim versions found , which event number was nerve submitted at all 
#----------------------------------------------------------------------------------------------------------------
# 0- colored print
def Colorprint(mytext , color):
    if (color in 'Red' and Isverbose)   :print("\033[2;31;107m " + mytext + " \033[0m" )
    if (color in 'Blue' and Isverbose)  :print("\033[2;34;107m " + mytext + " \033[0m" )
    if (color in 'Green' and Isverbose) :print("\033[2;32;107m " + mytext + " \033[0m" )
#----------------------------------------------------------------------------------------------------------------
# 1- PreparMyJob: collect ganga syntax in a function  
def PreparMyJob(job , JobName , tags , DataSet ):
    job.name = JobName
    file_j = open('JobName.py','wa')
    file_j.write('job_name ='+ ' "'+ JobName  +'" ' )
    file_j.close()
    #job.application.optsfile   = ['/afs/cern.ch/user/b/bkhanji/private/DVana/Bs2KMuNu/Bs2Kmunu_TupleMaker_MC_filtered.py']
    #job.application.optsfile   = ['/afs/cern.ch/user/b/bkhanji/private/DVana/Bs2KMuNu/Bs2Kmunu_TupleMaker_MC_NEW.py']
    job.application.optsfile   = ['/afs/cern.ch/user/i/ismith/cmtuser/DaVinci_v39r1p1/Phys/BsToKMuNu/scripts/Bs_KMuNu_TupleMaker/MC/Bs2Kmunu_TupleMaker_MC_NEW.py']
    if bool(tags):
        job.application.extraopts  =     (
            'DaVinci().TupleFile     = "DTT_'+ JobName + '.root"      ; ' +  
            'DaVinci().DDDBtag       = "' + tags_list[0] +         '" ; ' +
            'DaVinci().EvtMax        =              -1                ; ' +  #Change This
            'DaVinci().CondDBtag     = "' + tags_list[1] + '" ; '         +
            'DaVinci().DataType      = '  + bool('MC12' in JobName)*' "2012" '  +  bool('MC11' in JobName)*' "2011" '    
            )
    else :
        Colorprint( 'did not find the tags ==> using the default ones !!' , 'Red')
        job.application.extraopts  =   (
            'DaVinci().TupleFile     = "DTT_'+ JobName + '.root"       ; ' +
            'DaVinci().DataType      = '  + bool('-2012-' in JobName)*' "2012" '  +  bool('-2011-' in JobName)*' "2011" '    
            )
    job.inputdata               =  DataSet
    job.outputfiles             =  [LocalFile(namePattern='*.root')] 
    job.splitter                =  SplitByFiles(filesPerJob = 10 , maxFiles = -1 , ignoremissing = True)   
    job.backend                 =  Dirac()
#----------------------------------------------------------------------------------------------------------------
# 3- Check for submitted jobs : check if the list contains eventypes which were already submitted jobs
# The function remove those eventtypes by modifying the list in the input
def CheckMylist(Event_Type_MC):
    for eventype in list(Event_Type_MC):
        print '======================'
        i=  0
        i_problematic = 0
        jobs_problematic = []
        """
        print 'Checking now eventtype : ', eventype 
        for j in jobs:
            if eventype in j.name:
                if j.status in 'submitted, completed , running':
                    i += 1
                else: 
                    i_problematic += 1
                    jobs_problematic.append(j.id)
        #print i
        """
        
        if (i!= 0): ## 
            Colorprint( str(i) + ' jobs are submitted for Eventype: ' + eventype + ' => removing it from the list' , 'Blue')
            Event_Type_MC.remove(eventype)
            print '======================'
        if i==0:
            Colorprint( 'Eventype: ' + eventype +' jobs were not submitted at all => append' , 'Green')
            print '======================'
        if i_problematic > 0:
            Colorprint( 'jobs ' + str(jobs_problematic) + 'related to eventype ' + eventype , 'Red')
            Colorprint( 'are problematic, check your Ganga session'   , 'Red')
            print '======================'
         
    print '========================================================'
    Colorprint('List of all eventtypes which were not submitted (' + str(len(Event_Type_MC)) +' elements)', 'Blue')
    Colorprint( str(Event_Type_MC)                           , 'Red')
    return Event_Type_MC
    print '========================================================'
#----------------------------------------------------------------------------------------------------------------
# 4- get the decay name using eventtype:
def DecayName(EventType):
    decay_name=''
    filename_et = '/afs/cern.ch/lhcb/software/releases/DBASE/Gen/DecFiles/v28r4/options/'  + EventType  +'.py'
    #filename_et = '$DECFILESROOT/options/'
    if os.path.isfile(filename_et):
        et_f = open( filename_et )
        for line in et_f:
            if 'UserDecayFile' in line:
                v1,v2,decay_name,v3 = re.split('/|.dec',line)
                #Colorprint( 'The decay name is : ' + str(decay_name) ,  'Green')
                # nicer name:
                decay_name = decay_name.replace(',' , '_').replace('=' , '_')
                break
    return decay_name       
#----------------------------------------------------------------------------------------------------------------
# 5- Print message :
def PrintNewJob(evt_type, dcyname):
    print '-----------------------------------------------------------------------------'
    print '--------- N     N EEEEE W     WWW     W     JJJJ    OOO    BBBBB    ---------'
    print '--------- N N   N E     W     W W     W        J  OO   OO  BB   BB  ---------'
    print '--------- N  N  N EEEEE W    W   W    W        J O       O BBBBB    ---------'
    print '--------- N   N N E      W  W     W  W     JJ  J  OO   OO  BB   BB  ---------'
    print '--------- N     N EEEEE   WW       WW       JJJJ    OOO    BBBBB    ---------'
    print '-----------------------------------------------------------------------------'
    Colorprint( 'Filling the inputdata for job corresponding to EventType: ' + evt_type , 'Blue')
    Colorprint( 'This event type corresponds to the decay :' + str(dcyname) , 'Blue' )
    print '-----------------------------------------------------------------------------'
#----------------------------------------------------------------------------------------------------------------
# 6- Save list of Paths for those eventypes which did not get submitted (end of the job), this function
# could be time consuming :
def PathsOfUnsubmittedEvenTypes(eventype_list):
    filename = 'Paths_List_For_Unsumbitted_Jobs.txt'
    Colorprint('I will print out full paths and tags in the following file:' , 'Red')
    Colorprint('  ' + filename , 'Red')
    f = open( filename , "wa")
    f.write("=========================================== \n")
    f.write("In this file we list All possible paths     \n")
    f.write("For the EvenTypes Which were not submitted  \n")
    f.write("=========================================== \n")
    f.write("  \n")
    f.flush()
    print 'No correct path was found for the following event types:'
    print eventype_list
    print 'Engage DIRAC to get All possible paths (using Vanya\' script : dirac-bookkeeping-get-prodinfo-eventtype.py)'
    for eventpe in Event_Type_MC:
        print "---------------------------------------------------------- "    
        f.write("---------------------------------------------------------- \n")
        f.write('Fetch path name and tags for :')
        f.write('EventType  : ' + str(eventpe) + ' \n' )
        f.write('Decay name : ' + str(DecayName(eventpe) ) + ' \n')
        f.write("  \n")
        Mycommand   = 'python dirac-bookkeeping-get-prodinfo-eventtype.py '
        p = subprocess.Popen(Mycommand + eventpe , stdout= subprocess.PIPE , shell = True )
        output  = p.stdout.read()
        f.write(output)
        f.write("  \n")
        f.write("---------------------------------------------------------- \n")
        print "---------------------------------------------------------- "    
    f.close()
#----------------------------------------------------------------------------------------------------------------
# - get pathes for list of eventtypes  
def GetPathsAndTags(Event_Type_MC ):

    print ' \n'
    print '==============================================================='
    print 'Collecting Job inputs: PATH, Decay names, DBBB & SimCond tags :'
    print '==============================================================='
    print ' \n'
    List_of_Job_inputs = []
    NamePattern = ' "Beam4000GeV-2012-Mag.*-Nu2.5-Pythia8.*Sim08.*Reco14.*ALLSTREAMS.DST" '
    #NamePattern = ' "B2KMU.TRIGSTRIP.DST" '
    Mycommand   = 'python dirac-bookkeeping-get-prodinfo-eventtype.py '
    for eventpe in Event_Type_MC:
        print '--------------------------------------------------------------------------'
        Colorprint ('Filling a new tuple containing job inputs for eventtype '+ eventpe , 'Green' )
        p = subprocess.Popen(Mycommand + eventpe + ' | grep -i ' + NamePattern + ' ', stdout= subprocess.PIPE , shell = True )
        #output  = p.stdout.read()
        #output  = p.stdout.readlines()
        idx = 0
        for Lins_tuple in p.stdout:
            if(Lins_tuple == ''):
                Colorprint( 'No path was found for event type: ' +eventpe , 'Red')
                Colorprint( 'I will keep it in the list of problematic eventtypes' +eventpe , 'Red')
            else :
                idx +=1
                mytuple =  ast.literal_eval( Lins_tuple )
                PathTagTuple = str(eventpe), str(DecayName(eventpe)) , str(mytuple[0]) , str(mytuple[1]) , str(mytuple[2]) 
                Colorprint ('Appending tuple "'+str(idx) +'" containing job inputs to a list ', 'Blue')
                List_of_Job_inputs.append(PathTagTuple)
                #print List_of_Job_inputs
        print 'Finished filling the tuples for eventype: ',  eventpe    
        print '--------------------------------------------------------------------------'
        print ' '
            
    return List_of_Job_inputs           
#================================================================================================================
#================================================================================================================


#================================================================================================================
# first part: define the eventtypes, simulation, polarity, reconstruction, pythia version wanted.
# Default values below are for known to be interesting for physics analysis, feel free to change as you see fit.  
Event_Type_MC = ['13512010']#, '13512010' ]
"""
[ '10010015' , '10010017' , '11144001' , '11512011' , '11512400' , '12143001' ,
                  '12513001' , '12873002' , '13144001' , '13512010' , '13574040' , '13512400' ,
                  '13512410' , '13512420' , '13774002' , '15512013' , '15512014' , '13873000' ,
                  '13796000' , '11876010' , '11874050' , '11874042' , '11874004' , '15574001' ,
                  '13596000' , '13796000' , '12573050' , '12143401' , '12573200' , '12573400' ,
                  '12573401' , '11574050' , '11574051' , '12573050' , '13774000' ]
"""
#, '11995200' , '11995201' , '11995202' , '12995600' , '15574000' , '11696610' ,
#'12995601' , '12995602' , '13996202' , '13996201' , '13996200' ]
#10011001 , '15574010' , '12873440' ,
#Event_Type_MC = [ '10010035' ] 
# to remove any repeated eventtype :
Event_Type_MC = list(set(Event_Type_MC))
print Event_Type_MC
#================================================================================================================



#================================================================================================================
# check that eventtyoe is not already among the submitted jobs
Effective_eventtypelist = CheckMylist(Event_Type_MC)
#================================================================================================================

Tuple_For_PathsAndTags = ''
t0_GetPathsAndTags = time.time()
Tuple_For_PathsAndTags = GetPathsAndTags( Effective_eventtypelist )
t_GetPathsAndTags = time.time() - t0_GetPathsAndTags

#================================================================================================================
#================================================================================================================
# Third part is to submit the jobs:
print '==============================================================================='
print ' '
print 'Hello! this script will submit ', len(Tuple_For_PathsAndTags) , ' jobs'
print ' '
print '==============================================================================='
print ' '

    
print Tuple_For_PathsAndTags

t0_jobs = time.time()
n_submitted_jobs = 0 
for tuple_entry in Tuple_For_PathsAndTags:
    print '==============================================================================='
    Colorprint('Looking at the list of jobs inputs, entry : '+ str(Tuple_For_PathsAndTags.index(tuple_entry)) , 'Green')
    Colorprint('in the list of job inputs', 'Green')
    EvtType_e, DecyName_e         = tuple_entry[0] , tuple_entry[1]
    Path_e , DDBtag_e , SimCond_e = tuple_entry[2] , tuple_entry[3] , tuple_entry[4]
    Colorprint('EventType is  : ' + EvtType_e   ,'Blue')
    Colorprint('Decay Name is : ' + DecyName_e  ,'Blue')
    Colorprint('Data path is  : ' + Path_e      ,'Blue')
    Colorprint('Data tags are : ' + DDBtag_e + ' , ' + SimCond_e  ,'Blue')
    print '==============================================================================='
    tags_list  = [ DDBtag_e , SimCond_e ]
    submitted_jobs = False
    #===============================================
    bk_query = BKQuery( path  = Path_e )
    dataset = bk_query.getDataset()
    if len(dataset)>0:
        print ' '
        print '<><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><>><><><><><><><><>'
        print ' '
        Colorprint( 'Found the data set (' + str(len(dataset)) +' files found) corresponding to :','Green')
        Colorprint(  EvtType_e + '(' + str(DecyName_e) +') : ' + Path_e , 'Green')
        jobname = EvtType_e                        +       "_"                          + \
                  DecyName_e                       +       '_'                          + \
                  bool('Up' in Path_e)*'Up_'       + bool('Down' in Path_e)*'Dn_'       + \
                  bool('Pythia8' in Path_e)*'Py8_' + bool('Pythia6' in Path_e )*'Py6_'  + \
                  bool('-2012-' in Path_e)*'MC12'  + bool('-2011-' in Path_e )*'MC11'  
        jobs =Job(application = DaVinci(version='v39r1p1'))
        Colorprint('job number : '+ str(jobs.id) , 'Blue')
        Colorprint(jobname + ' prepares for submission' , 'Blue')
        print ' '          
        PrintNewJob
        PreparMyJob(jobs, jobname , tags_list , dataset)
        #queues.add(jobs.submit)
        submitted_jobs = True
        n_submitted_jobs += 1
        Colorprint( 'job number: ' + str(jobs.id) + ' with the name: ' + jobs.name  +' is submitted' , 'Green')
        print '<><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><>><><><><><><><><>'
        print ' '
    else:
        Colorprint('PATH of eventtype : '+ EvtType_e + ' was not found', 'Red'  )
        Colorprint('This incident can be further investigated through : PathsOfUnsubmittedEvenTypes() '      , 'Blue' ) 
    if not submitted_jobs:
        Colorprint('Something is wrong with eventtype : '+ EvtType_e, 'Red'  )
        Colorprint('This incident can be further investigated through : PathsOfUnsubmittedEvenTypes() '      , 'Blue' ) 
    Colorprint('Loop for the job : '+ jobname +' is terminated' , 'Blue' )
    print '==============================================================================='
t_jobs = time.time() - t0_jobs
print ' \n'
print '--------------------------------------------------------------------------------'
Colorprint('Script just finished submitting jobs ...' , 'Green')
Colorprint('Checking for irregularities (Unsbmitted jobs, incorrect jobs status etc ...)' , 'Blue')
print '--------------------------------------------------------------------------------'
print ' \n'
# check that all eventtypes in the list are all submitted as jobs:

'''

list_problematic = CheckMylist(Event_Type_MC)

print 'These are the eventtypes which did not get submitted'
print Event_Type_MC  


t0_prob = time.time()
if (list_problematic != []):
    Colorprint( 'Following is the list with problematic eventtypes :' ,'Red')
    Colorprint( str(list_problematic) ,'Red')
    PathsOfUnsubmittedEvenTypes(list_problematic)

t_prob = time.time() - t0_prob

Colorprint('Basic time-profiling in Report_time.txt','Blue')
f = open('Report_time.txt' , 'wa')
f.write('==================================================================================')
f.write(' \n')
f.write('Time taken to collect the PATHS & Tags in a list of tuples : '+str(t_GetPathsAndTags/3600.)+' hours ')
f.write(' \n')
f.write('Time taken to Submit all th jobs ('+ str(n_submitted_jobs)+' jobs) is : '+ str(t_jobs/3600.)+' hours')
f.write(' \n')
f.write('Time taken to collect info of problematic eventtypes : '+str(t_prob/3600.)+' hours')
f.write(' \n')
f.write('Number of problematic eventypes is: '+ str(len(list_problematic))  +' hours')
f.write(' \n')
f.write('==================================================================================')
f.close()

Colorprint("End of script execution" , 'Green')
print ' \n'


# if you want to retrive directly after submitting (probably during the night)
# Start retrive right away so tuples wiould be ready around noon
process = subprocess.Popen('ganga', stdout=subprocess.PIPE)
# keep cleaning the gangadir outputs each hour for 8 hours:
i_cleandir = 0
# get gangadir location :
gangarc_f = open( os.path.expanduser('~') +'/.gangarc' )
gangadir_vstr = [line for line in gangarc_f if  'gangadir = ' in line ]
gangadir_str  = (gangadir_vstr[0].rstrip('\n')).split('=')[1] + '/workspace/bkhanji/LocalXML/'
while (i_cleandir < 7):
    Colorprint('Cleaning ... campagin number : ' + str(i_cleandir) , 'Red')
    os.system('rm -rf '+ gangadir_str  +' */*/output/Step1_Ganga_DaVinci_v39r1.log', stdout=subprocess.PIPE)
    os.system('rm -rf '+ gangadir_str  +' */*/output/_postprocesslocations__', stdout=subprocess.PIPE)
    os.system('rm -rf '+ gangadir_str  +' */*/output/std.out', stdout=subprocess.PIPE)
    os.system('rm -rf '+ gangadir_str  +' */*/output/stdout', stdout=subprocess.PIPE) 
    i_cleandir +=1
    Colorprint('Cleaned '+ gangadir_str  + ' directory'  , 'Red')
    print '-------------------------------------------------------'
print "Good bye!"    

'''
