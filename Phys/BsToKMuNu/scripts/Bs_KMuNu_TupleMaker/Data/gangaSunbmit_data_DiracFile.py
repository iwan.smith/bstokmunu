import os , re
#
year         = [   '11'  ,    '12'    ]#,  '15'     ]
energy       = [  '3500' ,    '4000'  ]#,  '6500'   ]  
strip_v      = ['21r1p1a',  '21r0p1a' ]#,   '24'    ]
Reco_v       = [   '14'  ,     '14'   ]#,  '15a'    ]
polarity     = [   'Up'  ,    'Down'  ]#            ]
streams      = [   'SEMILEPTONIC'     ]#            ]

#################################################
# make the paths list
job_setting   = { }

for stm in streams:
    for pol in polarity:
        for i in range(len(year)):
            PATH_name ='/LHCb/Collision'+year[i]+'/Beam'+energy[i]+'GeV-VeloClosed-Mag'+pol+'/Real Data/Reco'+Reco_v[i]+'/Stripping'+strip_v[i]+'/90000000/'+stm+'.DST'
            print PATH_name
            job_name =  '20' +year[i] + '_Reco' + Reco_v[i] + 'Strip' + strip_v[i] + '_' + pol + '_' + stm 
            job_setting[job_name] = PATH_name 
print '========================================'
print 'Filled the list of PATHS for ganga jobs'
print '========================================'
for f in job_setting.items(): print f
#################################################


#################################################
# make the job templates 
for job_name , path_dict in job_setting.items():
    print '======================================'   
    print 'Sumbitting a new job ...'
    print path_dict , ','  , job_name
    bk_query = BKQuery( path  =  path_dict )
    dataset = bk_query.getDataset()
    job=Job(application=DaVinci(version='v39r1p1') ,
            name = job_name )
    Year = bool('2011' in job_name)*' "2011" '+bool('2012' in job_name)*' "2012" '+ bool('2015' in job_name)*' "2015" '
    job.application.optsfile = [
        './Bs_TupleMaker_data.py'
        ]
    job.application.extraopts  =     (
        'DaVinci().TupleFile     = "DTT_'+ job_name + '.root"  ; ' +
        'DaVinci().EvtMax        =              -1             ; ' +
        'CondDB().UseLatestTags  =  ['+ Year +']             ; ' +
        'DaVinci().DataType      =   '+ Year +'              ; '
        )
    print "Create job for the jobs: ", job.name
    job.inputdata  = dataset 
    #job.outputfiles= [DiracFile(namePattern='*.root' , locations=["CERN-USER"]) ] # to save the output on user's grid-space
    job.outputfiles= [DiracFile(namePattern='*.root',locations=['CERN-USER']) ] # It is not doing the trick we need to replicate 
    print "Calls splitter"
    job.splitter = SplitByFiles(filesPerJob = 25 , maxFiles =-1 , ignoremissing = True)
    print "Finally submit the job"
    job.backend    = Dirac()
    job.submit()
    print '======================================'   
    print "job: ", job.name + " submitted" 
    print '======================================'   
print " Jobs submitted .... bye "
#################################################

