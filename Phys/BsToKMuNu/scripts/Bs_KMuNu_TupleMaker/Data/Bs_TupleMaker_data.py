### __author__ : Basem Khanji ,  basem.khanji@cern.ch 
Year = "2012"
#######################################################################
from Gaudi.Configuration import *
#######################################################################
#
from Gaudi.Configuration import *
from DecayTreeTuple.Configuration import *
#######################################################################
from PhysSelPython.Wrappers import AutomaticData, Selection, SelectionSequence
from PhysSelPython.Wrappers import (DataOnDemand,
                                    Selection,
                                    MergedSelection,
                                    SelectionSequence)
import GaudiKernel.SystemOfUnits as Units

from Configurables import DaVinci, CheckPV, DecayTreeTuple, TupleToolTrigger, LoKi__Hybrid__TupleTool, TupleToolTagging, TupleToolTISTOS, TupleToolDecay, TupleToolNeutrinoReco , LoKi__Hybrid__EvtTupleTool , L0TriggerTisTos , TriggerTisTos , TupleToolIsoGeneric

from Configurables import FilterDesktop , CombineParticles
from Configurables import TupleToolEventInfo 

# StdNoPIDsVeloPions are not by default in COMMONPARTICLES , you need to make them before running the tool !
# These lines stolen from here : https://lhcb-release-area.web.cern.ch/LHCb-release-area/DOC/urania/latest_doxygen/py/d0/d15/config_iso_8py_source.html otherwise I need to change the design of the TupleToolBs2Kmunu_velotrac tool ... 
# MAKE loose selection :
def createVeloTracks():
    from Configurables import ChargedProtoParticleMaker, DaVinci
    veloprotos = ChargedProtoParticleMaker("ProtoPMaker")
    veloprotos.Inputs = ["Rec/Track/Best"]
    veloprotos.Output = "Rec/ProtoP/myProtoPMaker/ProtoParticles"
    DaVinci().appendToMainSequence( [ veloprotos ])
    from Configurables       import ProtoParticleCALOFilter, CombinedParticleMaker,NoPIDsParticleMaker
    from CommonParticles.Utils import trackSelector, updateDoD
    algorithm = NoPIDsParticleMaker('StdNoPIDsVeloPions',  Particle = 'pion',  )
    algorithm.Input = "Rec/ProtoP/myProtoPMaker/ProtoParticles"
    selector = trackSelector ( algorithm , trackTypes = ['Velo'] )
    locations = updateDoD ( algorithm )
    DaVinci().appendToMainSequence( [ algorithm ])
    
createVeloTracks()

def configIso():
    from Configurables import ChargedProtoParticleMaker, DaVinci
    Alltracksprotos = ChargedProtoParticleMaker("ProtoPMaker")
    Alltracksprotos.Inputs = ["Rec/Track/Best"]
    Alltracksprotos.Output = "Rec/ProtoP/myProtoPMaker/ProtoParticles"
    DaVinci().appendToMainSequence( [ Alltracksprotos ])
    from Configurables       import ProtoParticleCALOFilter, CombinedParticleMaker,NoPIDsParticleMaker
    from CommonParticles.Utils import trackSelector, updateDoD
    algorithm = NoPIDsParticleMaker('StdNoPIDsAlltracksPions',  Particle = 'pion' ,  )
    algorithm.Input = "Rec/ProtoP/myProtoPMaker/ProtoParticles"
    selector = trackSelector ( algorithm , trackTypes = ['Velo', 'Long' , 'Upstream'] )
    locations = updateDoD ( algorithm )
    DaVinci().appendToMainSequence( [ algorithm ])
configIso()

LongPions      = DataOnDemand( Location ='Phys/StdAllNoPIDsPions/Particles' )
UpStreamPions  = DataOnDemand( Location ='Phys/StdNoPIDsUpPions/Particles'  )
VeloPions      = DataOnDemand( Location ='Phys/StdNoPIDsVeloPions/Particles')
AllPions       = MergedSelection( "AllPions", RequiredSelections =[ LongPions , UpStreamPions , VeloPions  ] )
AllPions_seq   = SelectionSequence('AllPions_seq', TopSelection = AllPions)
DaVinci().appendToMainSequence( [ AllPions_seq ])

Bs2KMuNuInputLocation   =  "/Event/Semileptonic/Phys/B2XuMuNuBs2KLine/Particles"
Bs2KMuNuSSInputLocation =  "/Event/Semileptonic/Phys/B2XuMuNuBs2KSSLine/Particles"

Bs2KMuNuSSInputLocation_fakeKMu  =  "/Event/Semileptonic/Phys/B2XuMuNuBs2KSS_FakeKMuLine/Particles"
Bs2KMuNuSSInputLocation_fakeK    =  "/Event/Semileptonic/Phys/B2XuMuNuBs2KSS_FakeKLine/Particles"
Bs2KMuNuSSInputLocation_fakeMu   =  "/Event/Semileptonic/Phys/B2XuMuNuBs2KSS_FakeMuLine/Particles"

Bs2KMuNuInputLocation_fakeK      =  "/Event/Semileptonic/Phys/B2XuMuNuBs2K_FakeKLine/Particles"
Bs2KMuNuInputLocation_fakeMu     =  "/Event/Semileptonic/Phys/B2XuMuNuBs2K_FakeMuLine/Particles"
Bs2KMuNuInputLocation_fakeKMu    =  "/Event/Semileptonic/Phys/B2XuMuNuBs2K_FakeKMuLine/Particles"

Bs2DsMuNuInputLocation           =  '/Event/Semileptonic/Phys/B2DMuNuX_Ds/Particles'
Bs2DsMuNuInputLocation_fakeMu    =  '/Event/Semileptonic/Phys/B2DMuNuX_Ds_FakeMuon/Particles'
#############################################################
# DecayTreeTuple 
#
checkPV = CheckPV("CheckForOnePV")
checkPV.MinPVs = 1
print checkPV

##############################################################################
seqBsKmunu_gaudi           = GaudiSequencer('seqBsKmunu_gaudi')

TupleSeq   = GaudiSequencer('TupleSeq')
tuple = DecayTreeTuple("Bs2KmuNuTuple")
tuple.Inputs = [Bs2KMuNuInputLocation]
tuple.ToolList +=  [
    "TupleToolGeometry"
    ,"TupleToolKinematic"
    , "TupleToolPrimaries"
    , "TupleToolTrackInfo"
    , "LoKi::Hybrid::TupleTool/LoKiTool"
    ]
tuple.ReFitPVs = True # causing a problem ?
tuple.OutputLevel = 6

tuple.Decay    = "[B_s0 -> ^K- ^mu+]CC"
tuple.Branches = {
    "muon_p"   : "[B_s0 ->  K- ^mu+]CC"
    ,"kaon_m"  : "[B_s0 -> ^K-  mu+]CC" 
    ,"Bs"      : "[B_s0 ->  K-  mu+]CC" 
     }
tuple.addTool(TupleToolDecay, name="Bs")
tuple.addTool(TupleToolDecay, name="kaon_m")
tuple.addTool(TupleToolDecay, name="muon_p")

TupleToolIsoGeneric              =  tuple.addTupleTool("TupleToolIsoGeneric")
TupleToolIsoGeneric.ParticlePath =  AllPions_seq.outputLocation()
TupleToolIsoGeneric.VerboseMode = True
LoKiTool_noniso = LoKi__Hybrid__TupleTool("LoKiTool_noniso")
LoKiTool_noniso.Variables    = { "ETA" : "ETA" , "PHI" : "PHI" , "TRTYPE" : "TRTYPE"}
TupleToolIsoGeneric.ToolList += ["LoKi::Hybrid::TupleTool/LoKiTool_noniso"]
TupleToolIsoGeneric.addTool(LoKiTool_noniso)
TupleToolIsoGeneric.OutputLevel = 6

LoKiVariables = tuple.Bs.addTupleTool("LoKi::Hybrid::TupleTool/LoKiVariables")
LoKiVariables.Variables = {
    "ETA"              : "ETA",
    "PHI"              : "PHI",
    "DOCA"             : "DOCA(1,2)",
    "DTF_TAU"          : "DTF_CTAU( 0, True )/0.299792458",
    "DTF_CTAUS"        : "DTF_CTAUSIGNIFICANCE( 0, True )",
    "DTF_CHI2NDOF"     : "DTF_CHI2NDOF( True )",
    "FD_CHI2_LOKI"     : "BPVVDCHI2",
    "VCHI2_LOKI"       : "VFASPF(VCHI2/VDOF)",
    "FD_S"             : "BPVDLS",
    "cosTheta1_star"   : "LV01",
    "cosTheta2_star"   : "LV02",
    }

LoKiVariables_K = tuple.kaon_m.addTupleTool("LoKi::Hybrid::TupleTool/LoKiVariables_K")
LoKiVariables_K.Variables = { "MIPCHI2DV_PV" : "MIPCHI2DV(PRIMARY)" }
LoKiVariables_Mu = tuple.muon_p.addTupleTool("LoKi::Hybrid::TupleTool/LoKiVariables_Mu")
LoKiVariables_Mu.Variables = { "MIPCHI2DV_PV" : "MIPCHI2DV(PRIMARY)" }
############################################################################
TupleToolTISTOS = TupleToolTISTOS('TupleToolTISTOS')
TupleToolTISTOS.addTool(L0TriggerTisTos())
TupleToolTISTOS.addTool(TriggerTisTos())
TupleToolTISTOS.TriggerList=[
     "L0MuonDecision",
     "L0DiMuonDecision",
     "Hlt2TopoMu2BodyBBDTDecision",
     "Hlt2SingleMuonDecision",
     "Hlt2SingleMuonLowPTDecision"
     ]
TupleToolTISTOS.VerboseL0   = True
TupleToolTISTOS.VerboseHlt1 = True
TupleToolTISTOS.VerboseHlt2 = True
TupleToolTISTOS.TUS         = True
TupleToolTISTOS.TPS         = True
tuple.Bs.addTool(TupleToolTISTOS , name = 'TupleToolTISTOS' )
tuple.Bs.ToolList   += [ "TupleToolTISTOS"]
tuple.kaon_m.addTool(TupleToolTISTOS , name = 'TupleToolTISTOS' )
tuple.kaon_m.ToolList   += [ "TupleToolTISTOS"]
tuple.muon_p.addTool(TupleToolTISTOS , name = 'TupleToolTISTOS' )
tuple.muon_p.ToolList   += [ "TupleToolTISTOS"]


from Configurables import TupleToolConeIsolation
tuple.kaon_m.addTupleTool("TupleToolConeIsolation")
tuple.kaon_m.TupleToolConeIsolation.FillAsymmetry     = True
tuple.kaon_m.TupleToolConeIsolation.FillDeltas        = True
tuple.kaon_m.TupleToolConeIsolation.FillComponents    = True
tuple.kaon_m.TupleToolConeIsolation.MinConeSize       = 0.5
tuple.kaon_m.TupleToolConeIsolation.SizeStep          = 0.5
tuple.kaon_m.TupleToolConeIsolation.MaxConeSize       = 2.0
tuple.kaon_m.TupleToolConeIsolation.FillPi0Info       = True
tuple.kaon_m.TupleToolConeIsolation.FillMergedPi0Info = True

tuple.muon_p.addTupleTool("TupleToolConeIsolation")
tuple.muon_p.TupleToolConeIsolation.FillAsymmetry     = True
tuple.muon_p.TupleToolConeIsolation.FillDeltas        = True
tuple.muon_p.TupleToolConeIsolation.FillComponents    = True
tuple.muon_p.TupleToolConeIsolation.MinConeSize       = 0.5
tuple.muon_p.TupleToolConeIsolation.SizeStep          = 0.5
tuple.muon_p.TupleToolConeIsolation.MaxConeSize       = 2.0

tuple.ToolList +=  [
    "TupleToolPropertime"
    , "TupleToolRecoStats"
    , "TupleToolSLTools"
    , "TupleToolTrackPosition"
    ]


KmuNuTuple_fakeK = tuple.clone("Bs2KmuNuTuple_fakeK")
KmuNuTuple_fakeK.Inputs = [ Bs2KMuNuInputLocation_fakeK  ]
TupleSeq.Members +=  [ KmuNuTuple_fakeK  ]
TupleSeq.ModeOR          = True
TupleSeq.ShortCircuit    = False

KmuNuTuple_fakeMu = tuple.clone("Bs2KmuNuTuple_fakeMu")
KmuNuTuple_fakeMu.Inputs =  [ Bs2KMuNuInputLocation_fakeMu  ]
TupleSeq.Members   +=  [ KmuNuTuple_fakeMu  ]
TupleSeq.ModeOR        = True
TupleSeq.ShortCircuit  = False

KmuNuTuple_fakeKMu = tuple.clone("Bs2KmuNuTuple_fakeKMu")
KmuNuTuple_fakeKMu.Inputs =  [ Bs2KMuNuInputLocation_fakeKMu  ]

KmuNuSSTuple = tuple.clone("Bs2KmuNuSSTuple")
KmuNuSSTuple.Inputs = [Bs2KMuNuSSInputLocation]
KmuNuSSTuple.Decay    = "[B_s0 -> ^K+ ^mu+]CC"
KmuNuSSTuple.Branches = {
     "muon_p"  : "[B_s0 ->  K+ ^mu+]CC"
    ,"kaon_m"  : "[B_s0 -> ^K+  mu+]CC" 
    ,"Bs"      : "[B_s0 ->  K+  mu+]CC" 
     }

KmuNuTupleSS_fakeK = KmuNuSSTuple.clone("Bs2KmuNuTupleSS_fakeK")
KmuNuTupleSS_fakeK.Inputs = [ Bs2KMuNuSSInputLocation_fakeK  ]

KmuNuTupleSS_fakeMu = KmuNuSSTuple.clone("Bs2KmuNuTupleSS_fakeMu")
KmuNuTupleSS_fakeMu.Inputs =  [ Bs2KMuNuSSInputLocation_fakeMu  ]

KmuNuTupleSS_fakeKMu       =  KmuNuSSTuple.clone("Bs2KmuNuTupleSS_fakeKMu")
KmuNuTupleSS_fakeKMu.Inputs =  [ Bs2KMuNuSSInputLocation_fakeKMu  ]
#===========================================================================================================
# DsMuNu tuple is quite large we apply a mass cut around the Ds to reduce it :
Bs_particles = DataOnDemand( Location = Bs2DsMuNuInputLocation )
Bs_particles_filter = FilterDesktop("Bs_particles_filter",
                                    Code = "(MAXTREE('D+'==ABSID,M)>1920) & (MAXTREE('D+'==ABSID,M)<2030)" )
Bs_particles_sel  = Selection("Bs_particles_sel",
                              Algorithm = Bs_particles_filter,
                              RequiredSelections = [ Bs_particles ] )
SeqBs2DsMuNu      = SelectionSequence('SeqBs2DsMuNu', TopSelection = Bs_particles_sel)
seqBsKmunu_gaudi.Members       +=  [SeqBs2DsMuNu.sequence()]
seqBsKmunu_gaudi.ModeOR         =  True
seqBsKmunu_gaudi.ShortCircuit   =  False
# fake muon Dsmu line
Bs_particles_fakeMu = DataOnDemand( Location = Bs2DsMuNuInputLocation_fakeMu )
Bs_particles_filter_fakeMu = FilterDesktop("Bs_particles_filter_fakeMu",
                                           Code = "(MAXTREE('D+'==ABSID,M)>1920) & (MAXTREE('D+'==ABSID,M)<2030)")
Bs_particles_sel_fakeMu  = Selection("Bs_particles_sel_fakeMu",
                                     Algorithm = Bs_particles_filter_fakeMu,
                                     RequiredSelections = [ Bs_particles_fakeMu ])
SeqBs2DsMuNu_fakeMu             = SelectionSequence('SeqBs2DsMuNu_fakeMu', TopSelection = Bs_particles_sel_fakeMu)
seqBsKmunu_gaudi.Members       +=  [SeqBs2DsMuNu_fakeMu.sequence()]
seqBsKmunu_gaudi.ModeOR         =  True
seqBsKmunu_gaudi.ShortCircuit   =  False
#===========================================================================================================
DsMuNuTuple          =  tuple.clone("Bs2DsMuNuTuple")
DsMuNuTuple.Inputs   =  [ SeqBs2DsMuNu.outputLocation() ] 
#DsMuNuTuple.Inputs   =  [ '/Event/Semileptonic/Phys/b2DsMuXB2DMuNuXLine/Particles' ] 
DsMuNuTuple.Decay    = "[ [B~0]cc -> ^(D+ -> ^K- ^K+ ^pi+ ) ^mu-]CC"
DsMuNuTuple.Branches = {
    "muon_p"   : "[ [B~0]cc ->  (D+ ->  K-  K+  pi+ ) ^mu-]CC"
    ,"kaon_m"  : "[ [B~0]cc ->  (D+ -> ^K-  K+  pi+ )  mu-]CC"
    ,"pi_p"    : "[ [B~0]cc ->  (D+ ->  K-  K+ ^pi+ )  mu-]CC"
    ,"kaon_p"  : "[ [B~0]cc ->  (D+ ->  K- ^K+  pi+ )  mu-]CC"
    ,"Ds"      : "[ [B~0]cc -> ^(D+ ->  K-  K+  pi+ )  mu-]CC"
    ,"Bs"      : "[ [B~0]cc ->  (D+ ->  K-  K+  pi+ )  mu-]CC"
    }


DsMuNuTuple_fakeMu        = DsMuNuTuple.clone("Bs2DsMuNuTuple_fakeMu")
DsMuNuTuple_fakeMu.Inputs = [ SeqBs2DsMuNu_fakeMu.outputLocation() ] 

DsMuNuSSTuple          =  tuple.clone("Bs2DsMuNuSSTuple")
DsMuNuSSTuple.Inputs   =  [ SeqBs2DsMuNu.outputLocation() ] 
DsMuNuSSTuple.Decay    = "[ [B0]cc -> ^(D+ -> ^K- ^K+ ^pi+ ) ^mu+]CC"
DsMuNuSSTuple.Branches = {
    "muon_p"   : "[ [B0]cc ->  (D+ ->  K-  K+  pi+ ) ^mu+]CC"
    ,"kaon_m"  : "[ [B0]cc ->  (D+ -> ^K-  K+  pi+ )  mu+]CC"
    ,"pi_p"    : "[ [B0]cc ->  (D+ ->  K-  K+ ^pi+ )  mu+]CC"
    ,"K_p"     : "[ [B0]cc ->  (D+ ->  K- ^K+  pi+ )  mu+]CC"
    ,"Ds"      : "[ [B0]cc -> ^(D+ ->  K-  K+  pi+ )  mu+]CC"
    ,"Bs"      : "[ [B0]cc ->  (D+ ->  K-  K+  pi+ )  mu+]CC"
    }

DsMuNuSSTuple_fakeMu        = DsMuNuSSTuple.clone("Bs2DsMuNuSSTuple_fakeMu")
DsMuNuSSTuple_fakeMu.Inputs = [ SeqBs2DsMuNu_fakeMu.outputLocation() ] 
#===========================================================================================================









#===========================================================================================================
## Fill the Tupleseq
TupleSeq.Members        += [ tuple ]
TupleSeq.ModeOR          = True
TupleSeq.ShortCircuit    = False
TupleSeq.Members        +=  [ KmuNuTuple_fakeKMu  ]
TupleSeq.ModeOR          = True
TupleSeq.ShortCircuit    = False
TupleSeq.Members        += [KmuNuSSTuple]
TupleSeq.ModeOR          = True
TupleSeq.ShortCircuit    = False
TupleSeq.Members        +=  [ KmuNuTupleSS_fakeK  ]
TupleSeq.ModeOR          = True
TupleSeq.ShortCircuit    = False
TupleSeq.Members        +=  [ KmuNuTupleSS_fakeMu  ]
TupleSeq.ModeOR          = True
TupleSeq.ShortCircuit    = False
TupleSeq.Members        +=  [ KmuNuTupleSS_fakeKMu  ]
TupleSeq.ModeOR          =  True
TupleSeq.ShortCircuit    =  False
TupleSeq.Members        += [ DsMuNuTuple ]
TupleSeq.ModeOR          = True
TupleSeq.ShortCircuit    = False
TupleSeq.Members        += [ DsMuNuTuple_fakeMu ]
TupleSeq.ModeOR          = True
TupleSeq.ShortCircuit    = False
TupleSeq.Members        += [ DsMuNuSSTuple ]
TupleSeq.ModeOR          = True
TupleSeq.ShortCircuit    = False
TupleSeq.Members        += [ DsMuNuTuple_fakeMu ]
TupleSeq.ModeOR          = True
TupleSeq.ShortCircuit    = False
######################################################################
seqBsKmunu_gaudi.Members       +=  [TupleSeq]
seqBsKmunu_gaudi.ModeOR         =  True
seqBsKmunu_gaudi.ShortCircuit   =  False
######################################################################
#
# Event Tuple
#
from Configurables import EventTuple , TupleToolEventInfo
etuple = EventTuple()
########################################################################
#
# DaVinci settings
#
configIso()
from Configurables import DaVinci
#DaVinci().UserAlgorithms += [etuple, mysel , tuple]
DaVinci().UserAlgorithms += [ checkPV , seqBsKmunu_gaudi ]
#DaVinci().UserAlgorithms += [ etuple , SeqBs2KmuNu.sequence() , LooseTuple]
DaVinci().MainOptions  = "" 
DaVinci().EvtMax       = 1000
DaVinci().PrintFreq    = 25000
#DaVinci().SkipEvents   = 25000
DaVinci().DataType     = Year
# data:
from Configurables import CondDB
CondDB().UseLatestTags = [Year]
DaVinci().Lumi=True
# MC
DaVinci().Simulation = False
#######################################################################
#
DaVinci().TupleFile = "DTT_data.root"

#importOptions("/afs/cern.ch/user/b/bkhanji/private/DVana/Bs2KMuNu/Bs_KmuNu_data_2012.py")
#importOptions("/afs/cern.ch/user/a/acontu/public/S21Incremental_dsts/PFNSemileptonic.dst.py")
