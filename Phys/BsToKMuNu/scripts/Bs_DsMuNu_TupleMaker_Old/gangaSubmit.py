import os

test = False
output2EOS = False

"""
if test:
    Polarity = "Up"
    modes = ["data15_Bs2KMuNu", "data15_Bs2KMuNuSS", "data12_Bs2KMuNu", "data12_Bs2KMuNuSS", "data12_Bs2DsMuNu", "data12_Bs2DsMuNuSS", "data12_Bu2JpsiK", "data12_Bs2JpsiPhi",
             "data12_Bd2JpsiKst", "data11_Bs2KMuNu", "data11_Bs2KMuNuSS", "data11_Bs2DsMuNu", "data11_Bs2DsMuNuSS", "data11_Bu2JpsiK", "data11_Bs2JpsiPhi", "data11_Bd2JpsiKst",
             "MC12_Bs2DsMuNu_Cocktail_SIGNAL", "MC12_Bs2DsMuNu_Cocktail_BKG", "MC12_Bs2KMuNu_SIGNAL",
             "MC12_Bs2KstMuNu_BKG", "MC12_Bs2JpsiPhi_BKG", "MC12_Bd2JpsiKst_BKG", "MC12_Bd2PiMuNu_NoMCut_BKG", "MC12_Bd2PiMuNu_Mcut_BKG",
             "MC12_Bu2JpsiK_BKG", "MC12_Bu2RhoMuNu_BKG", "MC12_Bu2D0MuNu_BKG", "MC12_Bc2D0MuNu_BKG", "MC12_Lb2PMuNu_LCSR_BKG",
             "MC12_Bd2Rhopi0piMuNu_BKG", "MC12_Bs2K2stkpi0MuNu_BKG", "MC12_Bs2Kst1430kpi0MuNu_BKG", "MC12_inclb_OC2Kmu_BKG", "MC12_inclb_OC2KmuSS_BKG",
             "MC12_inclb_OC2Kmu_gencut_BKG", "MC12_inclb_OC2KmuSS_gencut_BKG", "MC12_inclb_OC2Kplusmu_gencut_BKG", "MC12_Bu2JpsiK_SIGNAL", "MC12_Bs2JpsiPhi_SIGNAL",
             "MC12_Bd2DsDst_BKG", "MC12_Bs2DsstDsst_BKG", "MC12_Bu2DsstD0st_BKG",

             "MC11_Bs2DsMuNu_Cocktail_SIGNAL", "MC11_Bs2DsMuNu_Cocktail_BKG", "MC11_Bs2KMuNu_SIGNAL",
             "MC11_Bs2KstMuNu_BKG", "MC11_Bs2JpsiPhi_BKG", "MC11_Bd2JpsiKst_BKG", "MC11_Bd2PiMuNu_NoMCut_BKG", "MC11_Bd2PiMuNu_Mcut_BKG",
             "MC11_Bu2JpsiK_BKG", "MC11_Bu2RhoMuNu_BKG", "MC11_Bu2D0MuNu_BKG", "MC11_Bc2D0MuNu_BKG", "MC11_Lb2PMuNu_LCSR_BKG",
             "MC11_Bd2Rhopi0piMuNu_BKG", "MC11_Bs2K2stkpi0MuNu_BKG", "MC11_Bs2Kst1430kpi0MuNu_BKG", "MC11_inclb_OC2Kmu_BKG", "MC11_inclb_OC2KmuSS_BKG"
             ]
    mode = modes[-1]
    print "Run in test mode for the channel", mode
"""
isMC = ("MC" in mode)

Year = "2012"
if("data11" in mode):
    Year = "2011"
if("data15" in mode):
    Year = "2015"
if isMC:
    if("MC12" in mode):
        Year = "2012"
    elif("MC11" in mode):
        Year = "2011"
DaVinciVersion = 'v39r1p1'
filesperjob     = 10
maxfiles        = -1
nEvt = -1
nFreq = 25000
if test:
    nEvt = 10000
    nFreq = 3000

def getTheDTS(mode):
    dtspath = {
        "data15_Bs2KMuNu":["/LHCb/Collision15/Beam6500GeV-VeloClosed-MagDown/Real Data/Reco15a/Stripping24/90000000/SEMILEPTONIC.DST"],
        "data15_Bs2KMuNuSS":["/LHCb/Collision15/Beam6500GeV-VeloClosed-MagDown/Real Data/Reco15a/Stripping24/90000000/SEMILEPTONIC.DST"],

        "data12_Bs2KMuNu":["/LHCb/Collision12/Beam4000GeV-VeloClosed-MagDown/Real Data/Reco14/Stripping21r0p1a/90000000/SEMILEPTONIC.DST"],
        "data12_Bs2KMuNuSS":["/LHCb/Collision12/Beam4000GeV-VeloClosed-MagDown/Real Data/Reco14/Stripping21r0p1a/90000000/SEMILEPTONIC.DST"],
        #"data12_Bs2DsMuNu":["/LHCb/Collision12/Beam4000GeV-VeloClosed-MagDown/Real Data/Reco14/Stripping21/90000000/SEMILEPTONIC.DST"],
        #"data12_Bs2DsMuNuSS":["/LHCb/Collision12/Beam4000GeV-VeloClosed-MagDown/Real Data/Reco14/Stripping21/90000000/SEMILEPTONIC.DST"],
        "data12_Bs2DsMuNu":["/LHCb/Collision12/Beam4000GeV-VeloClosed-MagDown/Real Data/Reco14/Stripping21r0p1a/90000000/SEMILEPTONIC.DST"],
        "data12_Bs2DsMuNuSS":["/LHCb/Collision12/Beam4000GeV-VeloClosed-MagDown/Real Data/Reco14/Stripping21r0p1a/90000000/SEMILEPTONIC.DST"],
        "data12_Bu2JpsiK":["/LHCb/Collision12/Beam4000GeV-VeloClosed-MagDown/Real Data/Reco14/Stripping21/90000000/DIMUON.DST"],
        "data12_Bs2JpsiPhi":["/LHCb/Collision12/Beam4000GeV-VeloClosed-MagDown/Real Data/Reco14/Stripping21/90000000/DIMUON.DST"],
        "data12_Bd2JpsiKst":["/LHCb/Collision12/Beam4000GeV-VeloClosed-MagDown/Real Data/Reco14/Stripping21/90000000/DIMUON.DST"],

        "data11_Bs2KMuNu":["/LHCb/Collision11/Beam3500GeV-VeloClosed-MagDown/Real Data/Reco14/Stripping21r1p1a/90000000/SEMILEPTONIC.DST"],
        "data11_Bs2KMuNuSS":["/LHCb/Collision11/Beam3500GeV-VeloClosed-MagDown/Real Data/Reco14/Stripping21r1p1a/90000000/SEMILEPTONIC.DST"],
        "data11_Bs2DsMuNu":["/LHCb/Collision11/Beam3500GeV-VeloClosed-MagDown/Real Data/Reco14/Stripping21r1/90000000/SEMILEPTONIC.DST"],
        "data11_Bs2DsMuNuSS":["/LHCb/Collision11/Beam3500GeV-VeloClosed-MagDown/Real Data/Reco14/Stripping21r1/90000000/SEMILEPTONIC.DST"],
        "data11_Bu2JpsiK":["/LHCb/Collision11/Beam3500GeV-VeloClosed-MagDown/Real Data/Reco14/Stripping21r1/90000000/DIMUON.DST"],
        "data11_Bs2JpsiPhi":["/LHCb/Collision11/Beam3500GeV-VeloClosed-MagDown/Real Data/Reco14/Stripping21r1/90000000/DIMUON.DST"],
        "data11_Bd2JpsiKst":["/LHCb/Collision11/Beam3500GeV-VeloClosed-MagDown/Real Data/Reco14/Stripping21r1/90000000/DIMUON.DST"],

        "MC12_Bs2DsMuNu_Cocktail_SIGNAL":[#"/MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-Pythia8/Sim08a/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/13774002/ALLSTREAMS.DST",
                                          #"/MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-Pythia8/Sim08h/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/13774002/ALLSTREAMS.DST"
                                          "/MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-Pythia8/Sim08i/Digi13/Trig0x409f0045/Reco14c/Stripping21NoPrescalingFlagged/13774000/ALLSTREAMS.DST"
                                          # ,"/MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-Pythia6/Sim08a/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/13774002/ALLSTREAMS.DST",
                                          # "/MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-Pythia6/Sim08h/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/13774002/ALLSTREAMS.DST"
                                          ],
        "MC12_Bs2DsMuNu_Cocktail_BKG":["/MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-Pythia8/Sim08a/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/13774002/ALLSTREAMS.DST"
                                       #"/MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-Pythia8/Sim08h/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/13774002/ALLSTREAMS.DST"
                                       # ,"/MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-Pythia6/Sim08a/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/13774002/ALLSTREAMS.DST",
                                       # /MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-Pythia6/Sim08h/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/13774002/ALLSTREAMS.DST"
                                       ],
        "MC12_Bs2KMuNu_SIGNAL":["/MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-Pythia8/Sim08a/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/13512010/ALLSTREAMS.DST",
                                # "/MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-Pythia6/Sim08a/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/13512010/ALLSTREAMS.DST"
                                ],
        "MC12_Bs2KstMuNu_BKG":["/MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-Pythia8/Sim08h/Digi13/Trig0x409f0045/Reco14c/Stripping20NoPrescalingFlagged/13512400/ALLSTREAMS.DST"],
        "MC12_Bs2JpsiPhi_BKG":["/MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-Pythia8/Sim08a/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/13144001/ALLSTREAMS.DST",
                               "/MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-Pythia8/Sim08e/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/13144001/ALLSTREAMS.DST",
                               # "/MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-Pythia6/Sim08a/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/13144001/ALLSTREAMS.DST",
                               # "/MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-Pythia6/Sim08e/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/13144001/ALLSTREAMS.DST"
                               ],
        "MC12_Bd2JpsiKst_BKG":["/MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-Pythia8/Sim08a/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/11144001/ALLSTREAMS.DST",
                               "/MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-Pythia8/Sim08c/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/11144001/ALLSTREAMS.DST",
                               "/MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-Pythia8/Sim08f/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/11144001/ALLSTREAMS.DST",
                               # "/MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-Pythia6/Sim08a/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/11144001/ALLSTREAMS.DST",
                               # "/MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-Pythia6/Sim08c/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/11144001/ALLSTREAMS.DST"
                               ],
        "MC12_Bd2PiMuNu_NoMCut_BKG":["/MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-Pythia8/Sim08h/Digi13/Trig0x409f0045/Reco14c/Stripping21NoPrescalingFlagged/11512011/ALLSTREAMS.DST"],
        "MC12_Bd2PiMuNu_Mcut_BKG":["/MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-Pythia8/Sim08e/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/11512012/ALLSTREAMS.DST"],
        "MC12_Bu2JpsiK_BKG":["/MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-Pythia8/Sim08a/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/12143001/ALLSTREAMS.DST",
                             "/MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-Pythia8/Sim08e/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/12143001/ALLSTREAMS.DST",
                             # "/MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-Pythia6/Sim08a/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/12143001/ALLSTREAMS.DST",
                             # "/MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-Pythia6/Sim08e/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/12143001/ALLSTREAMS.DST"
                             ],
        "MC12_Bu2RhoMuNu_BKG":["/MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-Pythia8/Sim08g/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/12513001/ALLSTREAMS.DST",
                               # "/MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-Pythia6/Sim08g/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/12513001/ALLSTREAMS.DST"
                               ],
        "MC12_Bu2D0MuNu_BKG":["/MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-Pythia8/Sim08a/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/12873002/ALLSTREAMS.DST",
                              # "/MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-Pythia6/Sim08a/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/12873002/ALLSTREAMS.DST"
                              ],
        "MC12_Bc2D0MuNu_BKG":["/MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-BcVegPy/Sim08e/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/14573002/ALLSTREAMS.DST"],
        "MC12_Lb2PMuNu_LCSR_BKG":["/MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-Pythia8/Sim08b/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/15512013/ALLSTREAMS.DST",
                                  # "/MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-Pythia6/Sim08b/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/15512013/ALLSTREAMS.DST"
                                  ],
        "MC12_Bd2Rhopi0piMuNu_BKG":["/MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-Pythia8/Sim08h/Digi13/Trig0x409f0045/Reco14c/Stripping20NoPrescalingFlagged/11512400/ALLSTREAMS.DST"],
        "MC12_Bs2K2stkpi0MuNu_BKG":["/MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-Pythia8/Sim08h/Digi13/Trig0x409f0045/Reco14c/Stripping20NoPrescalingFlagged/13512410/ALLSTREAMS.DST"],
        "MC12_Bs2Kst1430kpi0MuNu_BKG":["/MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-Pythia8/Sim08h/Digi13/Trig0x409f0045/Reco14c/Stripping20NoPrescalingFlagged/13512420/ALLSTREAMS.DST"],
        "MC12_inclb_OC2Kmu_BKG":["/MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-Pythia8/Sim08h/Digi13/Trig0x40760037/Reco14c/Stripping21NoPrescalingFlagged/10010002/ALLSTREAMS.DST"],
        "MC12_inclb_OC2KmuSS_BKG":["/MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-Pythia8/Sim08h/Digi13/Trig0x409f0045/Reco14c/Stripping21NoPrescalingFlagged/10010015/ALLSTREAMS.DST"],
        "MC12_inclb_OC2Kplusmu_BKG":["/MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-Pythia8/Sim08h/Digi13/Trig0x409f0045/Reco14c/Stripping21NoPrescalingFlagged/10010017/ALLSTREAMS.DST"],
        "MC12_inclb_OC2Kmu_gencut_BKG":[],
        "MC12_inclb_OC2KmuSS_gencut_BKG":[],
        "MC12_inclb_OC2Kplusmu_gencut_BKG":[],
        "MC12_Bu2JpsiK_SIGNAL":["/MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-Pythia8/Sim08a/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/12143001/ALLSTREAMS.DST",
                             "/MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-Pythia8/Sim08e/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/12143001/ALLSTREAMS.DST",
                             # "/MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-Pythia6/Sim08a/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/12143001/ALLSTREAMS.DST",
                             # "/MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-Pythia6/Sim08e/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/12143001/ALLSTREAMS.DST"
                                ],
        "MC12_Bs2JpsiPhi_SIGNAL":["/MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-Pythia8/Sim08a/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/13144001/ALLSTREAMS.DST",
                               "/MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-Pythia8/Sim08e/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/13144001/ALLSTREAMS.DST",
                               # "/MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-Pythia6/Sim08a/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/13144001/ALLSTREAMS.DST",
                               # "/MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-Pythia6/Sim08e/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/13144001/ALLSTREAMS.DST"
                                  ],

        "MC12_Bd2DsDst_SSBKG"   : ["/MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-Pythia8/Sim08a/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/11876001/ALLSTREAMS.DST",
                                 "/MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-Pythia6/Sim08a/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/11876001/ALLSTREAMS.DST"],
        "MC12_Bs2DsstDsst_SSBKG": ["/MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-Pythia8/Sim08a/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/13873201/ALLSTREAMS.DST",
                                 "/MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-Pythia6/Sim08a/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/13873201/ALLSTREAMS.DST"],
        "MC12_Bu2DsstD0st_SSBKG": ["/MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-Pythia8/Sim08a/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/12875601/ALLSTREAMS.DST",
                                 "/MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-Pythia6/Sim08a/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/12875601/ALLSTREAMS.DST"],
        "MC12_Bd2DsDst_BKG"   : ["/MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-Pythia8/Sim08a/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/11876001/ALLSTREAMS.DST",
                                 "/MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-Pythia6/Sim08a/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/11876001/ALLSTREAMS.DST"],
        "MC12_Bs2DsstDsst_BKG": ["/MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-Pythia8/Sim08a/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/13873201/ALLSTREAMS.DST",
                                 "/MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-Pythia6/Sim08a/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/13873201/ALLSTREAMS.DST"],
        "MC12_Bu2DsstD0st_BKG": ["/MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-Pythia8/Sim08a/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/12875601/ALLSTREAMS.DST",
                                 "/MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-Pythia6/Sim08a/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/12875601/ALLSTREAMS.DST"],
        "MC11_Bs2DsMuNu_Cocktail_SIGNAL":[#"/MC/2011/Beam3500GeV-2011-MagDown-Nu2-Pythia8/Sim08h/Digi13/Trig0x40760037/Reco14c/Stripping20r1p3Filtered/13774002/B2DMUX.TRIGSTRIP.DST",
                                          "/MC/2011/Beam3500GeV-2011-MagDown-Nu2-Pythia8/Sim08i/Digi13/Trig0x40760037/Reco14c/Stripping21r1NoPrescalingFlagged/13774000/ALLSTREAMS.DST"
                                          # "/MC/2011/Beam3500GeV-2011-MagDown-Nu2-Pythia6/Sim08h/Digi13/Trig0x40760037/Reco14c/Stripping20r1p3Filtered/13774002/B2DMUX.TRIGSTRIP.DST"
                                          ], #OK
        "MC11_Bs2DsMuNu_Cocktail_BKG":["/MC/2011/Beam3500GeV-2011-MagDown-Nu2-Pythia8/Sim08h/Digi13/Trig0x40760037/Reco14c/Stripping20r1p3Filtered/13774002/B2DMUX.TRIGSTRIP.DST"
                                       # ,"/MC/2011/Beam3500GeV-2011-MagDown-Nu2-Pythia6/Sim08h/Digi13/Trig0x40760037/Reco14c/Stripping20r1p3Filtered/13774002/B2DMUX.TRIGSTRIP.DST"
                                       ], #OK
        "MC11_Bs2KMuNu_SIGNAL":["13512010"], # STRANGE STRIPPING
        # "MC11_Bs2KstMuNu_BKG":["/MC/2011/Beam3500GeV-2011-MagDown-Nu2-Pythia8/Sim08h/Digi13/Trig0x409f0045/Reco14c/Stripping20NoPrescalingFlagged/13512400/ALLSTREAMS.DST"], # NOT FOUND
        "MC11_Bs2JpsiPhi_BKG":["/MC/2011/Beam3500GeV-2011-MagDown-Nu2-Pythia8/Sim08a/Digi13/Trig0x40760037/Reco14a/Stripping20r1NoPrescalingFlagged/13144001/ALLSTREAMS.DST",
                               "/MC/2011/Beam3500GeV-2011-MagDown-Nu2-Pythia8/Sim08e/Digi13/Trig0x40760037/Reco14a/Stripping20r1NoPrescalingFlagged/13144001/ALLSTREAMS.DST",
                               # "/MC/2011/Beam3500GeV-2011-MagDown-Nu2-Pythia6/Sim08a/Digi13/Trig0x40760037/Reco14a/Stripping20r1NoPrescalingFlagged/13144001/ALLSTREAMS.DST",
                               # "/MC/2011/Beam3500GeV-2011-MagDown-Nu2-Pythia6/Sim08e/Digi13/Trig0x40760037/Reco14a/Stripping20r1NoPrescalingFlagged/13144001/ALLSTREAMS.DST"
                               ], #OK
        "MC11_Bd2JpsiKst_BKG":["/MC/2011/Beam3500GeV-2011-MagDown-Nu2-Pythia8/Sim08b/Digi13/Trig0x40760037/Reco14a/Stripping20r1NoPrescalingFlagged/11144001/ALLSTREAMS.DST",
                               "/MC/2011/Beam3500GeV-2011-MagDown-Nu2-Pythia8/Sim08f/Digi13/Trig0x40760037/Reco14a/Stripping20r1NoPrescalingFlagged/11144001/ALLSTREAMS.DST",
                               # "/MC/2011/Beam3500GeV-2011-MagDown-Nu2-Pythia6/Sim08b/Digi13/Trig0x40760037/Reco14a/Stripping20r1NoPrescalingFlagged/11144001/ALLSTREAMS.DST"
                               ], #OK
        "MC11_Bd2PiMuNu_NoMCut_BKG":["/MC/2011/Beam3500GeV-2011-MagDown-Nu2-Pythia8/Sim08h/Digi13/Trig0x40760037/Reco14c/Stripping21r1NoPrescalingFlagged/11512011/ALLSTREAMS.DST"], #OK
        "MC11_Bd2PiMuNu_Mcut_BKG":["11512012"], # NO ACCEPTANCE CUT
        "MC11_Bu2JpsiK_BKG":["/MC/2011/Beam3500GeV-2011-MagDown-Nu2-Pythia8/Sim08a/Digi13/Trig0x40760037/Reco14a/Stripping20r1NoPrescalingFlagged/12143001/ALLSTREAMS.DST",
                             "/MC/2011/Beam3500GeV-2011-MagDown-Nu2-Pythia8/Sim08c/Digi13/Trig0x40760037/Reco14a/Stripping20r1NoPrescalingFlagged/12143001/ALLSTREAMS.DST",
                             # "/MC/2011/Beam3500GeV-2011-MagDown-Nu2-Pythia6/Sim08a/Digi13/Trig0x40760037/Reco14a/Stripping20r1NoPrescalingFlagged/12143001/ALLSTREAMS.DST",
                             # "/MC/2011/Beam3500GeV-2011-MagDown-Nu2-Pythia6/Sim08c/Digi13/Trig0x40760037/Reco14a/Stripping20r1NoPrescalingFlagged/12143001/ALLSTREAMS.DST"
                             ], #OK
        "MC11_Bu2RhoMuNu_BKG":["12513001/ALLSTREAMS.DST"], # NO ACCEPTANCE CUT
        "MC11_Bu2D0MuNu_BKG":["/MC/2011/Beam3500GeV-2011-MagDown-Nu2-Pythia8/Sim08a/Digi13/Trig0x40760037/Reco14a/Stripping20r1NoPrescalingFlagged/12873002/ALLSTREAMS.DST",
                              # "/MC/2011/Beam3500GeV-2011-MagDown-Nu2-Pythia6/Sim08a/Digi13/Trig0x40760037/Reco14a/Stripping20r1NoPrescalingFlagged/12873002/ALLSTREAMS.DST"
                              ], #OK
        "MC11_Bc2D0MuNu_BKG":["14573002/ALLSTREAMS.DST"], # NOT FOUND
        "MC11_Lb2PMuNu_LCSR_BKG":["15512013/ALLSTREAMS.DST"], # NOT FOUND
        "MC11_Bd2Rhopi0piMuNu_BKG":["/MC/2011/Beam3500GeV-2011-MagDown-Nu2-Pythia8/Sim08h/Digi13/Trig0x40760037/Reco14c/Stripping20NoPrescalingFlagged/11512400/ALLSTREAMS.DST"], #OK
        "MC11_Bs2K2stkpi0MuNu_BKG":["/MC/2011/Beam3500GeV-2011-MagDown-Nu2-Pythia8/Sim08h/Digi13/Trig0x40760037/Reco14c/Stripping21r1NoPrescalingFlagged/13512410/ALLSTREAMS.DST"], #OK
        "MC11_Bs2Kst1430kpi0MuNu_BKG":["/MC/2011/Beam3500GeV-2011-MagDown-Nu2-Pythia8/Sim08h/Digi13/Trig0x40760037/Reco14c/Stripping21r1NoPrescalingFlagged/13512420/ALLSTREAMS.DST"], #OK
        "MC11_inclb_OC2Kmu_BKG":["/MC/2011/Beam3500GeV-2011-MagDown-Nu2-Pythia8/Sim08h/Digi13/Trig0x40760037/Reco14c/Stripping21NoPrescalingFlagged/10010002/ALLSTREAMS.DST"], #OK
        "MC11_inclb_OC2KmuSS_BKG":["/MC/2011/Beam3500GeV-2011-MagDown-Nu2-Pythia8/Sim08h/Digi13/Trig0x40760037/Reco14c/Stripping21NoPrescalingFlagged/10010015/ALLSTREAMS.DST"], #OK
        "MC11_inclb_OC2Kplusmu_BKG":["/MC/2011/Beam3500GeV-2011-MagDown-Nu2-Pythia8/Sim08h/Digi13/Trig0x40760037/Reco14c/Stripping21NoPrescalingFlagged/10010017/ALLSTREAMS.DST"], #OK


"MC11_11995200":["/MC/2011/Beam3500GeV-2011-MagDown-Nu2-Pythia8/Sim08i/Digi13/Trig0x40760037/Reco14c/Stripping20r1NoPrescalingFlagged/11995200/ALLSTREAMS.DST"],
"MC11_11995202":["/MC/2011/Beam3500GeV-2011-MagDown-Nu2-Pythia8/Sim08i/Digi13/Trig0x40760037/Reco14c/Stripping20r1NoPrescalingFlagged/11995202/ALLSTREAMS.DST"],
"MC11_12995600":["/MC/2011/Beam3500GeV-2011-MagDown-Nu2-Pythia8/Sim08i/Digi13/Trig0x40760037/Reco14c/Stripping20r1NoPrescalingFlagged/12995600/ALLSTREAMS.DST"],
"MC11_12995602":["/MC/2011/Beam3500GeV-2011-MagDown-Nu2-Pythia8/Sim08i/Digi13/Trig0x40760037/Reco14c/Stripping20r1NoPrescalingFlagged/12995602/ALLSTREAMS.DST"],
"MC11_13996200":["/MC/2011/Beam3500GeV-2011-MagDown-Nu2-Pythia8/Sim08i/Digi13/Trig0x40760037/Reco14c/Stripping20r1NoPrescalingFlagged/13996200/ALLSTREAMS.DST"],
"MC11_13996202":["/MC/2011/Beam3500GeV-2011-MagDown-Nu2-Pythia8/Sim08i/Digi13/Trig0x40760037/Reco14c/Stripping20r1NoPrescalingFlagged/13996202/ALLSTREAMS.DST"],
"MC11_15998000":["/MC/2011/Beam3500GeV-2011-MagDown-Nu2-Pythia8/Sim08i/Digi13/Trig0x40760037/Reco14c/Stripping20r1NoPrescalingFlagged/15998000/ALLSTREAMS.DST"],
"MC11_15998002":["/MC/2011/Beam3500GeV-2011-MagDown-Nu2-Pythia8/Sim08i/Digi13/Trig0x40760037/Reco14c/Stripping20r1NoPrescalingFlagged/15998002/ALLSTREAMS.DST"],
"MC11_11995201":["/MC/2011/Beam3500GeV-2011-MagDown-Nu2-Pythia8/Sim08i/Digi13/Trig0x40760037/Reco14c/Stripping20r1NoPrescalingFlagged/11995201/ALLSTREAMS.DST"],
"MC11_12995601":["/MC/2011/Beam3500GeV-2011-MagDown-Nu2-Pythia8/Sim08i/Digi13/Trig0x40760037/Reco14c/Stripping20r1NoPrescalingFlagged/12995601/ALLSTREAMS.DST"],
"MC11_13996201":["/MC/2011/Beam3500GeV-2011-MagDown-Nu2-Pythia8/Sim08i/Digi13/Trig0x40760037/Reco14c/Stripping20r1NoPrescalingFlagged/13996201/ALLSTREAMS.DST"],
"MC12_10010037":["/MC/2012/Beam4000GeV-2012-MagDown-Nu2.5-Pythia8/Sim08i/Digi13/Trig0x409f0045/Reco14c/Stripping21r0p1Filtered/10010037/B2KMU.TRIGSTRIP.DST"],


        }
    # isPythia6 = ( ("Bs2DsMuNu_Cocktail_BKG" in mode) | ("Bs2DsMuNu_Cocktail_SIGNAL" in mode) | ("MC12_Lb2PMuNu_LCSR_BKG" in mode) | ("MC12_Bu2D0MuNu_BKG" in mode) | ("MC12_Bu2RhoMuNu_BKG" in mode) | ("MC12_Bu2JpsiK_BKG" in mode) | ("MC12_Bd2JpsiKst_BKG" in mode) | ("MC12_Bs2KMuNu_SIGNAL" in mode) )
    isPythia6 = False
    # ("MC12_Bs2JpsiPhi_BKG" in mode) # checked, only Pythia 8 is included
    for i,p in enumerate(dtspath[mode]):
        if(not isPythia6):
            if("Pythia6" in p):
                continue
        print "Is this where I break?"
        print i
        print p
        if(i == 0):
            if(Polarity == "Down"):
                bk_query = BKQuery(path= p)
                # ds = bk_query_Down.getDataset()
            else:
                bk_query = BKQuery(path= p.replace("Down","Up") )
                # ds.extend(bk_query_Up.getDataset())
            ds = bk_query.getDataset()
        else:
            if(Polarity == "Down"):
                bk_qy_Dw = BKQuery(path= p) 
                ds.extend(bk_qy_Dw.getDataset())
            else:
                bk_qy_Up =BKQuery(path= p.replace("Down","Up"))
                ds.extend(bk_qy_Up.getDataset())
    print "getting the data sample for : ", ds
    return ds.files

tuplefilename = { 
        "data15_Bs2KMuNu":"data15_Bs2KMuNu_"+Polarity+".root",
        "data15_Bs2KMuNuSS":"data15_Bs2KMuNuSS_"+Polarity+".root",

        "data12_Bs2KMuNu":"data12_Bs2KMuNu_"+Polarity+".root",
        "data12_Bs2KMuNuSS":"data12_Bs2KMuNuSS_"+Polarity+".root",
        "data12_Bs2DsMuNu":"data12_Bs2DsMuNu_"+Polarity+".root",
        "data12_Bs2DsMuNuSS":"data12_Bs2DsMuNuSS_"+Polarity+".root",
        "data12_Bu2JpsiK": "data12_Bu2JpsiK_"+Polarity+".root",
        "data12_Bs2JpsiPhi":"data12_Bs2JpsiPhi_"+Polarity+".root",
        "data12_Bd2JpsiKst":"data12_Bd2JpsiKst_"+Polarity+".root",

        "data11_Bs2KMuNu":"data11_Bs2KMuNu_"+Polarity+".root",
        "data11_Bs2KMuNuSS":"data11_Bs2KMuNuSS_"+Polarity+".root",
        "data11_Bs2DsMuNu":"data11_Bs2DsMuNu_"+Polarity+".root",
        "data11_Bs2DsMuNuSS":"data11_Bs2DsMuNuSS_"+Polarity+".root",
        "data11_Bu2JpsiK": "data11_Bu2JpsiK_"+Polarity+".root",
        "data11_Bs2JpsiPhi":"data11_Bs2JpsiPhi_"+Polarity+".root",
        "data11_Bd2JpsiKst":"data11_Bd2JpsiKst_"+Polarity+".root",

        "MC12_Bs2DsMuNu_Cocktail_SIGNAL":"DTT_MC12_Bs2DsMuNu_13774000_Cocktail_SIGNAL_"+Polarity+".root", #Pythia 6 and 8 both Sim08a, Sim08h
        "MC12_Bs2DsMuNu_Cocktail_BKG":"DTT_MC12_Bs2DsMuNu_13774002_Cocktail_BKG_"+Polarity+".root", #Pythia 6 and 8 both Sim08a, Sim08h
        "MC12_Bs2KMuNu_SIGNAL":"DTT_MC12_Bs2KMuNu_13512010_SIGNAL_"+Polarity+".root", #Pythia 6 and 8 both Sim08a
        "MC12_Bs2KstMuNu_BKG":"DTT_MC12_Bs2KstMuNu_13512400_BKG_"+Polarity+".root", #Pytia 8 Sim08h
        "MC12_Bs2JpsiPhi_BKG":"DTT_MC12_Bs2JpsiPhi_13144001_BKG_"+Polarity+".root", #Pythia 6 and 8 both Sim08a, Sim08e
        "MC12_Bd2JpsiKst_BKG":"DTT_MC12_Bd2JpsiKst_11144001_BKG_"+Polarity+".root", #Pythia 6 (Sim08a, Sim08c) and 8 (Sim08a, Sim08c, Sim08f)
        "MC12_Bd2PiMuNu_NoMCut_BKG":"DTT_MC12_Bd2PiMuNu_NoMCut_11512011_BKG_"+Polarity+".root", #Pythia 8 Sim08h
        "MC12_Bd2PiMuNu_Mcut_BKG":"DTT_MC12_Bd2PiMuNu_Mcut_11512012_BKG_"+Polarity+".root", #Pythia 8 Sim08e
        "MC12_Bu2JpsiK_BKG":"DTT_MC12_Bu2JpsiK_12143001_BKG_"+Polarity+".root", #Pythia 6 and 8 both Sim08a, Sim08e
        "MC12_Bu2RhoMuNu_BKG":"DTT_MC12_Bu2RhoMuNu_12513001_BKG_"+Polarity+".root", ##Pythia 6 and 8 both Sim08g
        "MC12_Bu2D0MuNu_BKG":"DTT_MC12_Bu2D0MuNu_12873002_BKG_"+Polarity+".root", #Pythia 6 and 8 both Sim08a
        "MC12_Bc2D0MuNu_BKG":"DTT_MC12_Bc2D0MuNu_14573002_BKG_"+Polarity+".root",  #BcVegPy Sim08e
        "MC12_Lb2PMuNu_LCSR_BKG":"DTT_MC12_Lb2PMuNu_LCSR_15512013_BKG_"+Polarity+".root", #Pythia 6 and 8 both Sim08b
        "MC12_Bd2Rhopi0piMuNu_BKG":"DTT_MC12_Bd2Rhopi0piMuNu_BKG_11512400_"+Polarity+".root", #Pythia 8 Sim08h
        "MC12_Bs2K2stkpi0MuNu_BKG":"DTT_MC12_Bs2K2stkpi0MuNu_BKG_13512410_"+Polarity+".root", #Pythia 8 Sim08h
        "MC12_Bs2Kst1430kpi0MuNu_BKG":"DTT_MC12_Bs2Kst1430kpi0MuNu_BKG_13512420_"+Polarity+".root", #Pythia 8 Sim08h
        "MC12_inclb_OC2Kmu_BKG":"DTT_MC12_inclb_OC2Kmu_BKG_10010002_"+Polarity+".root", #Pythia 8 Sim08h
        "MC12_inclb_OC2KmuSS_BKG":"DTT_MC12_inclb_OC2KmuSS_BKG_10010015_"+Polarity+".root", #Pythia 8 Sim08h
        "MC12_inclb_OC2Kplusmu_BKG":"DTT_MC12_inclb_OC2Kplusmu_BKG_10010017_"+Polarity+".root", #Pythia 8 sim08h
        "MC12_inclb_OC2Kmu_gencut_BKG":"DTT_MC12_inclb_OC2Kmu_gencut_BKG_10010032_"+Polarity+".root", #TODO
        "MC12_inclb_OC2KmuSS_gencut_BKG":"DTT_MC12_inclb_OC2KmuSS_gencut_BKG_10010035_"+Polarity+".root", #TODO
        "MC12_inclb_OC2Kplusmu_gencut_BKG":"DTT_MC12_inclb_OC2Kplusmu_gencut_BKG_10010037_"+Polarity+".root", #TODO
        "MC12_Bu2JpsiK_SIGNAL":"DTT_MC12_Bu2JpsiK_12143001_SIGNAL_"+Polarity+".root", #Pythia 6 and 8 both Sim08a, Sim08e
        "MC12_Bs2JpsiPhi_SIGNAL":"DTT_MC12_Bs2JpsiPhi_13144001_SIGNAL_"+Polarity+".root", #Pythia 6 and 8 both Sim08a, Sim08e,
        "MC12_Bd2DsDst_SSBKG"   :  "DTT_MC12_Bd2DsDst_"    + "11876001" + "_BKG_" +Polarity+".root", #Pythia 6 and 8 Sim08a,
        "MC12_Bs2DsstDsst_SSBKG":  "DTT_MC12_Bs2DsstDsst_" + "13873201" + "_BKG_" +Polarity+".root", #Pythia 6 and 8 Sim08a,
        "MC12_Bu2DsstD0st_SSBKG":  "DTT_MC12_Bu2DsstD0st_" + "12875601" + "_BKG_" +Polarity+".root", #Pythia 6 and 8 Sim08a,
        "MC12_Bd2DsDst_BKG"   :  "DTT_MC12_Bd2DsDst_"    + "11876001" + "_BKG_" +Polarity+".root", #Pythia 6 and 8 Sim08a,
        "MC12_Bs2DsstDsst_BKG":  "DTT_MC12_Bs2DsstDsst_" + "13873201" + "_BKG_" +Polarity+".root", #Pythia 6 and 8 Sim08a,
        "MC12_Bu2DsstD0st_BKG":  "DTT_MC12_Bu2DsstD0st_" + "12875601" + "_BKG_" +Polarity+".root", #Pythia 6 and 8 Sim08a,

                                
        "MC11_Bs2DsMuNu_Cocktail_SIGNAL":"DTT_MC11_Bs2DsMuNu_13774000_Cocktail_SIGNAL_"+Polarity+".root", #Pythia 6 and 8 both Sim08a, Sim08h
        "MC11_Bs2DsMuNu_Cocktail_BKG":"DTT_MC11_Bs2DsMuNu_13774002_Cocktail_BKG_"+Polarity+".root", #Pythia 6 and 8 both Sim08a, Sim08h
        "MC11_Bs2KMuNu_SIGNAL":"DTT_MC11_Bs2KMuNu_13512010_SIGNAL_"+Polarity+".root", #Pythia 6 and 8 both Sim08a
        "MC11_Bs2KstMuNu_BKG":"DTT_MC11_Bs2KstMuNu_13512400_BKG_"+Polarity+".root", #Pytia 8 Sim08h
        "MC11_Bs2JpsiPhi_BKG":"DTT_MC11_Bs2JpsiPhi_13144001_BKG_"+Polarity+".root", #Pythia 6 and 8 both Sim08a, Sim08e
        "MC11_Bd2JpsiKst_BKG":"DTT_MC11_Bd2JpsiKst_11144001_BKG_"+Polarity+".root", #Pythia 6 (Sim08a, Sim08c) and 8 (Sim08a, Sim08c, Sim08f)
        "MC11_Bd2PiMuNu_NoMCut_BKG":"DTT_MC11_Bd2PiMuNu_NoMCut_11512011_BKG_"+Polarity+".root", #Pythia 8 Sim08h
        "MC11_Bd2PiMuNu_Mcut_BKG":"DTT_MC11_Bd2PiMuNu_Mcut_11512012_BKG_"+Polarity+".root", #Pythia 8 Sim08e
        "MC11_Bu2JpsiK_BKG":"DTT_MC11_Bu2JpsiK_12143001_BKG_"+Polarity+".root", #Pythia 6 and 8 both Sim08a, Sim08e
        "MC11_Bu2RhoMuNu_BKG":"DTT_MC11_Bu2RhoMuNu_12513001_BKG_"+Polarity+".root", ##Pythia 6 and 8 both Sim08g
        "MC11_Bu2D0MuNu_BKG":"DTT_MC11_Bu2D0MuNu_12873002_BKG_"+Polarity+".root", #Pythia 6 and 8 both Sim08a
        "MC11_Bc2D0MuNu_BKG":"DTT_MC11_Bc2D0MuNu_14573002_BKG_"+Polarity+".root",  #BcVegPy Sim08e
        "MC11_Lb2PMuNu_LCSR_BKG":"DTT_MC11_Lb2PMuNu_LCSR_15512013_BKG_"+Polarity+".root", #Pythia 6 and 8 both Sim08b
        "MC11_Bd2Rhopi0piMuNu_BKG":"DTT_MC11_Bd2Rhopi0piMuNu_BKG_11512400_"+Polarity+".root", #Pythia 8 Sim08h
        "MC11_Bs2K2stkpi0MuNu_BKG":"DTT_MC11_Bs2K2stkpi0MuNu_BKG_13512410_"+Polarity+".root", #Pythia 8 Sim08h
        "MC11_Bs2Kst1430kpi0MuNu_BKG":"DTT_MC11_Bs2Kst1430kpi0MuNu_BKG_13512420_"+Polarity+".root", #Pythia 8 Sim08h
        "MC11_inclb_OC2Kmu_BKG":"DTT_MC11_inclb_OC2Kmu_BKG_10010002_"+Polarity+".root", #Pythia 8 Sim08h
        "MC11_inclb_OC2KmuSS_BKG":"DTT_MC11_inclb_OC2KmuSS_BKG_10010015_"+Polarity+".root", #Pythia 8 Sim08h
        "MC11_inclb_OC2Kplusmu_BKG":"DTT_MC11_inclb_OC2Kplusmu_BKG_10010017_"+Polarity+".root", #Pythia 8 sim08h

"MC11_11995200":"DTT_MC11_11995200_"+Polarity+".root",
"MC11_11995202":"DTT_MC11_11995202_"+Polarity+".root",
"MC11_12995600":"DTT_MC11_12995600_"+Polarity+".root",
"MC11_12995602":"DTT_MC11_12995602_"+Polarity+".root",
"MC11_13996200":"DTT_MC11_13996200_"+Polarity+".root",
"MC11_13996202":"DTT_MC11_13996202_"+Polarity+".root",
"MC11_15998000":"DTT_MC11_15998000_"+Polarity+".root",
"MC11_15998002":"DTT_MC11_15998002_"+Polarity+".root",
"MC11_11995201":"DTT_MC11_11995201_"+Polarity+".root",
"MC11_12995601":"DTT_MC11_12995601_"+Polarity+".root",
"MC11_13996201":"DTT_MC11_13996201_"+Polarity+".root",
"MC12_10010037":"DTT_MC11_10010037_"+Polarity+".root",




        }

dddbTag = { 
        "data15_Bs2KMuNu":"",
        "data15_Bs2KMuNuSS":"",

        "data12_Bs2KMuNu":"",
        "data12_Bs2KMuNuSS":"",
        "data12_Bs2DsMuNu":"",
        "data12_Bs2DsMuNuSS":"",
        "data12_Bu2JpsiK":"",
        "data12_Bs2JpsiPhi":"",
        "data12_Bd2JpsiKst":"",

        "data11_Bs2KMuNu":"",
        "data11_Bs2KMuNuSS":"",
        "data11_Bs2DsMuNu":"",
        "data11_Bs2DsMuNuSS":"",
        "data11_Bu2JpsiK":"",
        "data11_Bs2JpsiPhi":"",
        "data11_Bd2JpsiKst":"",

        "MC12_Bs2DsMuNu_Cocktail_SIGNAL":"dddb-20150522-2",
        "MC12_Bs2DsMuNu_Cocktail_BKG":"dddb-20150522-2",
        "MC12_Bs2KMuNu_SIGNAL":"dddb-20150522-2",
        "MC12_Bs2KstMuNu_BKG":"dddb-20150522-2",
        "MC12_Bs2JpsiPhi_BKG":"dddb-20150522-2",
        "MC12_Bd2JpsiKst_BKG":"dddb-20150522-2",
        "MC12_Bd2PiMuNu_NoMCut_BKG":"dddb-20150522-2",
        "MC12_Bd2PiMuNu_Mcut_BKG":"dddb-20150522-2",
        "MC12_Bu2JpsiK_BKG":"dddb-20150522-2",
        "MC12_Bu2RhoMuNu_BKG":"dddb-20150522-2",
        "MC12_Bu2D0MuNu_BKG":"dddb-20150522-2",
        "MC12_Bc2D0MuNu_BKG":"dddb-20150522-2",
        "MC12_Lb2PMuNu_LCSR_BKG":"dddb-20150522-2",
        "MC12_Bd2Rhopi0piMuNu_BKG":"dddb-20150522-2", 
        "MC12_Bs2K2stkpi0MuNu_BKG":"dddb-20150522-2", 
        "MC12_Bs2Kst1430kpi0MuNu_BKG":"dddb-20150522-2", 
        "MC12_inclb_OC2Kmu_BKG":"dddb-20150522-2", 
        "MC12_inclb_OC2KmuSS_BKG":"dddb-20150522-2", 
        "MC12_inclb_OC2Kplusmu_BKG":"dddb-20150522-2",
        "MC12_inclb_OC2Kmu_gencut_BKG":"dddb-20150522-2",
        "MC12_inclb_OC2KmuSS_gencut_BKG":"dddb-20150522-2",
        "MC12_inclb_OC2Kplusmu_gencut_BKG":"dddb-20150522-2",
        "MC12_Bu2JpsiK_SIGNAL":"dddb-20150522-2",
        "MC12_Bs2JpsiPhi_SIGNAL":"dddb-20150522-2",
        "MC12_Bd2DsDst_SSBKG"   :  "Sim08-20130503-1",  # from db-tags in Bender
        "MC12_Bs2DsstDsst_SSBKG":  "Sim08-20130503-1",  # from db-tags in Bender
        "MC12_Bu2DsstD0st_SSBKG":  "Sim08-20130503-1",  # from db-tags in Bender
        "MC12_Bd2DsDst_BKG"   :  "Sim08-20130503-1",  # from db-tags in Bender
        "MC12_Bs2DsstDsst_BKG":  "Sim08-20130503-1",  # from db-tags in Bender
        "MC12_Bu2DsstD0st_BKG":  "Sim08-20130503-1",  # from db-tags in Bender
# old one dddb-201208310

        "MC11_Bs2DsMuNu_Cocktail_SIGNAL":"dddb-20150522-2",
        "MC11_Bs2DsMuNu_Cocktail_BKG":"dddb-20150522-2",
        "MC11_Bs2KMuNu_SIGNAL":"dddb-20150522-2",
        "MC11_Bs2KstMuNu_BKG":"dddb-20150522-2",
        "MC11_Bs2JpsiPhi_BKG":"dddb-20150522-2",
        "MC11_Bd2JpsiKst_BKG":"dddb-20150522-2",
        "MC11_Bd2PiMuNu_NoMCut_BKG":"dddb-20150522-2",
        "MC11_Bd2PiMuNu_Mcut_BKG":"dddb-20150522-2",
        "MC11_Bu2JpsiK_BKG":"dddb-20150522-2",
        "MC11_Bu2RhoMuNu_BKG":"dddb-20150522-2",
        "MC11_Bu2D0MuNu_BKG":"dddb-20150522-2",
        "MC11_Bc2D0MuNu_BKG":"dddb-20150522-2",
        "MC11_Lb2PMuNu_LCSR_BKG":"dddb-20150522-2",
        "MC11_Bd2Rhopi0piMuNu_BKG":"dddb-20150522-2", 
        "MC11_Bs2K2stkpi0MuNu_BKG":"dddb-20150522-2", 
        "MC11_Bs2Kst1430kpi0MuNu_BKG":"dddb-20150522-2", 
        "MC11_inclb_OC2Kmu_BKG":"dddb-20150522-2", 
        "MC11_inclb_OC2KmuSS_BKG":"dddb-20150522-2", 
        "MC11_inclb_OC2Kplusmu_BKG":"dddb-20150522-2" ,

"MC11_11995200":"dddb-20130929",
"MC11_11995202":"dddb-20130929",
"MC11_12995600":"dddb-20130929",
"MC11_12995602":"dddb-20130929",
"MC11_13996200":"dddb-20130929",
"MC11_13996202":"dddb-20130929",
"MC11_15998000":"dddb-20130929",
"MC11_15998002":"dddb-20130929",
"MC11_11995201":"dddb-20130929",
"MC11_12995601":"dddb-20130929",
"MC11_13996201":"dddb-20130929",
"MC12_10010037":"dddb-20130929",


        }

condDBTag = {
        "data15_Bs2KMuNu":"",
        "data15_Bs2KMuNuSS":"",

        "data12_Bs2KMuNu":"",
        "data12_Bs2KMuNuSS":"",
        "data12_Bs2DsMuNu":"",
        "data12_Bs2DsMuNuSS":"",
        "data12_Bu2JpsiK":"",
        "data12_Bs2JpsiPhi":"",
        "data12_Bd2JpsiKst":"",

        "data11_Bs2KMuNu":"",
        "data11_Bs2KMuNuSS":"",
        "data11_Bs2DsMuNu":"",
        "data11_Bs2DsMuNuSS":"",
        "data11_Bu2JpsiK":"",
        "data11_Bs2JpsiPhi":"",
        "data11_Bd2JpsiKst":"",

        "MC12_Bs2DsMuNu_Cocktail_SIGNAL":"sim-20121025-vc-mu100",
        "MC12_Bs2DsMuNu_Cocktail_BKG":"sim-20121025-vc-mu100",
        "MC12_Bs2KMuNu_SIGNAL":"sim-20121025-vc-mu100",
        "MC12_Bs2KstMuNu_BKG":"sim-20121025-vc-mu100",
        "MC12_Bs2JpsiPhi_BKG":"sim-20121025-vc-mu100",
        "MC12_Bd2JpsiKst_BKG":"sim-20121025-vc-mu100",
        "MC12_Bd2PiMuNu_NoMCut_BKG":"sim-20121025-vc-mu100",
        "MC12_Bd2PiMuNu_Mcut_BKG":"sim-20121025-vc-mu100",
        "MC12_Bu2JpsiK_BKG":"sim-20121025-vc-mu100",
        "MC12_Bu2RhoMuNu_BKG":"sim-20121025-vc-mu100",
        "MC12_Bu2D0MuNu_BKG":"sim-20121025-vc-mu100",
        "MC12_Bc2D0MuNu_BKG":"sim-20121025-vc-mu100",
        "MC12_Lb2PMuNu_LCSR_BKG":"sim-20121025-vc-mu100",
        "MC12_Bd2Rhopi0piMuNu_BKG":"sim-20121025-vc-mu100", 
        "MC12_Bs2K2stkpi0MuNu_BKG":"sim-20121025-vc-mu100", 
        "MC12_Bs2Kst1430kpi0MuNu_BKG":"sim-20121025-vc-mu100", 
        "MC12_inclb_OC2Kmu_BKG":"sim-20121025-vc-mu100", 
        "MC12_inclb_OC2KmuSS_BKG":"sim-20121025-vc-mu100",
        "MC12_inclb_OC2Kplusmu_BKG":"sim-20121025-vc-mu100",
        "MC12_inclb_OC2Kmu_gencut_BKG":"sim-20121025-vc-mu100",
        "MC12_inclb_OC2KmuSS_gencut_BKG":"sim-20121025-vc-mu100",
        "MC12_inclb_OC2Kplusmu_gencut_BKG":"sim-20121025-vc-mu100",
        "MC12_Bu2JpsiK_SIGNAL":"sim-20121025-vc-mu100",
        "MC12_Bs2JpsiPhi_SIGNAL":"sim-20121025-vc-mu100",
        "MC12_Bd2DsDst_SSBKG"   :  "Sim08-20130503-1-vc-mu100",   # from db-tags in Bender
        "MC12_Bs2DsstDsst_SSBKG":  "Sim08-20130503-1-vc-mu100",   # from db-tags in Bender
        "MC12_Bu2DsstD0st_SSBKG":  "Sim08-20130503-1-vc-mu100",   # from db-tags in Bender
        "MC12_Bd2DsDst_BKG"   :  "Sim08-20130503-1-vc-mu100",   # from db-tags in Bender
        "MC12_Bs2DsstDsst_BKG":  "Sim08-20130503-1-vc-mu100",   # from db-tags in Bender
        "MC12_Bu2DsstD0st_BKG":  "Sim08-20130503-1-vc-mu100",   # from db-tags in Bender

        "MC11_Bs2DsMuNu_Cocktail_SIGNAL":"sim-20121025-vc-mu100",
        "MC11_Bs2DsMuNu_Cocktail_BKG":"sim-20121025-vc-mu100",
        "MC11_Bs2KMuNu_SIGNAL":"sim-20121025-vc-mu100",
        "MC11_Bs2KstMuNu_BKG":"sim-20121025-vc-mu100",
        "MC11_Bs2JpsiPhi_BKG":"sim-20121025-vc-mu100",
        "MC11_Bd2JpsiKst_BKG":"sim-20121025-vc-mu100",
        "MC11_Bd2PiMuNu_NoMCut_BKG":"sim-20121025-vc-mu100",
        "MC11_Bd2PiMuNu_Mcut_BKG":"sim-20121025-vc-mu100",
        "MC11_Bu2JpsiK_BKG":"sim-20121025-vc-mu100",
        "MC11_Bu2RhoMuNu_BKG":"sim-20121025-vc-mu100",
        "MC11_Bu2D0MuNu_BKG":"sim-20121025-vc-mu100",
        "MC11_Bc2D0MuNu_BKG":"sim-20121025-vc-mu100",
        "MC11_Lb2PMuNu_LCSR_BKG":"sim-20121025-vc-mu100",
        "MC11_Bd2Rhopi0piMuNu_BKG":"sim-20121025-vc-mu100", 
        "MC11_Bs2K2stkpi0MuNu_BKG":"sim-20121025-vc-mu100", 
        "MC11_Bs2Kst1430kpi0MuNu_BKG":"sim-20121025-vc-mu100", 
        "MC11_inclb_OC2Kmu_BKG":"sim-20121025-vc-mu100", 
        "MC11_inclb_OC2KmuSS_BKG":"sim-20121025-vc-mu100",
        "MC11_inclb_OC2Kplusmu_BKG":"sim-20121025-vc-mu100" ,

"MC11_11995200":"sim-20130522-vc-mu100",
"MC11_11995202":"sim-20130522-vc-mu100",
"MC11_12995600":"sim-20130522-vc-mu100",
"MC11_12995602":"sim-20130522-vc-mu100",
"MC11_13996200":"sim-20130522-vc-mu100",
"MC11_13996202":"sim-20130522-vc-mu100",
"MC11_15998000":"sim-20130522-vc-mu100",
"MC11_15998002":"sim-20130522-vc-mu100",
"MC11_11995201":"sim-20130522-vc-mu100",
"MC11_12995601":"sim-20130522-vc-mu100",
"MC11_13996201":"sim-20130522-vc-mu100",

"MC12_10010037":"sim-20130522-vc-mu100",

    }

extraopts = ""
extraopts +="\nDaVinci().TupleFile = \""+tuplefilename[mode]+"\"\n"
extraopts += "isMC ="+str(isMC)+"\n"
extraopts += "UseStrippingSel = True\n"
extraopts += "DaVinci().DataType = \""+Year+"\"\n" 
extraopts += "DaVinci().EvtMax = "+str(nEvt)+"\n" 
extraopts += "DaVinci().PrintFreq = "+str(nFreq)+"\n" 
extraopts += "DaVinci().InputType = \"DST\"\n" 
extraopts += "DaVinci().Simulation = "+str(isMC)+"\n" 
extraopts += "DaVinci().MainOptions = \"\"\n" 
extraopts += "DaVinci().SkipEvents = 0\n" 
extraopts += "DaVinci().Lumi = "+str(not isMC)+"\n" 

data = LHCbDataset()
if(test):
    # data.files.append(DiracFile('/lhcb/MC/2012/ALLSTREAMS.DST/00025691/0000/00025691_00000003_1.allstreams.dst') )
    data.files.append(DiracFile('/lhcb/MC/2012/ALLSTREAMS.DST/00021467/0000/00021467_00000002_1.allstreams.dst') ) # for Bs2DsMuNu mode
else:
    data.files += getTheDTS(mode)

if(isMC):
    extraopts += "from Configurables import CondDB\n"
    extraopts += "DaVinci().DDDBtag = \"" + dddbTag[mode] + "\"\n"
    if(Polarity == "Down"):
        extraopts += "DaVinci().CondDBtag =\"" + condDBTag[mode].replace("mu100","md100") + "\"\n"
    else:
        extraopts += "DaVinci().CondDBtag =\"" + condDBTag[mode] + "\"\n"
else:
    extraopts += "from Configurables import CondDB\n"
    extraopts += "CondDB().UseLatestTags = [\""+Year+"\"]\n"

extraopts +="setupBsKMuNu(\""+mode+"\")\n"


job = Job(application = DaVinci(version=DaVinciVersion,platform="x86_64-slc6-gcc49-opt",masterpackage="Phys/BsToKMuNu"), name = mode+"_"+Polarity )# ,backend=Dirac() )
job.application.optsfile = ['/afs/cern.ch/user/'+username+'/cmtuser/DaVinci_'+DaVinciVersion+'/Phys/BsToKMuNu/scripts/Bs_DsMuNu_TupleMaker/Bs_TupleMaker.py']
job.application.extraopts = extraopts

print "Created job for the jobs: ", job.name
job.inputdata  = data

if output2EOS:
    outf=MassStorageFile(tuplefilename[mode])
    if(test == False):
        outf.outputfilenameformat = '/'+job.name+'/{fname}{sjid}.root'
    else:
        outf.outputfilenameformat = '/'+job.name+'/{jid}_{fname}'
        job.outputfiles = [outf]
else:
    job.outputfiles = [DiracFile(tuplefilename[mode])]
# f = MassStorageFile(mode+'.root')
# f.outputfilenameformat = '/'+mode+'/{jid}_{fname}'
# job.outputfiles = [f]
# job.inputfiles = ["possible weight file"]
#job.outputfiles = [LocalFile(namePattern=tuplefilename[mode])] #['DTT_B02DstD_data.root']
#template.outputfiles                = [DiracFile(outputFileName)] # from Ksmumu


if(not test):
    print "Calls splitter"
    job.splitter = SplitByFiles(filesPerJob = filesperjob, maxFiles = maxfiles, ignoremissing = True)

# for LSF :
print "Finally submit the job"
job.backend = Dirac()
job.submit()
print "job: ", job.name + " submitted"
