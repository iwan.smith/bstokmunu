#    "data12_Bs2KMuNu"
#    "data12_Bs2KMuNuSS"
#    "data12_Bs2DsMuNu"
#    "data12_Bs2DsMuNuSS"
#    "data12_Bu2JpsiK"
#    "data12_Bs2JpsiPhi"
#
#    "data11_Bs2KMuNu"
#    "data11_Bs2KMuNuSS"
#    "data11_Bs2DsMuNu"
#    "data11_Bs2DsMuNuSS"
#    "data11_Bu2JpsiK"
#    "data11_Bs2JpsiPhi"

#   "MC12_Bs2DsMuNu_Cocktail_SIGNAL"
#    "MC12_Bs2DsMuNu_Cocktail_BKG"
#    "MC12_Bs2KMuNu_SIGNAL"
#    "MC12_Bs2KstMuNu_BKG"
#    "MC12_Bs2JpsiPhi_BKG"
#    "MC12_Bd2JpsiKst_BKG"
#    "MC12_Bd2PiMuNu_NoMCut_BKG"
#    "MC12_Bd2PiMuNu_Mcut_BKG"
#    "MC12_Bu2JpsiK_BKG"
#    "MC12_Bu2RhoMuNu_BKG"
#    "MC12_Bu2D0MuNu_BKG"
#    "MC12_Bc2D0MuNu_BKG"
#    "MC12_Lb2PMuNu_LCSR_BKG"
#    "MC12_Bd2Rhopi0piMuNu_BKG"
#    "MC12_Bs2K2stkpi0MuNu_BKG"
#    "MC12_Bs2Kst1430kpi0MuNu_BKG"
#    "MC12_inclb_OC2Kmu_BKG"
#    "MC12_inclb_OC2KmuSS_BKG"
#    "MC12_inclb_OC2Kplusmu_BKG"
#    "MC12_inclb_OC2Kmu_gencut_BKG"
#    "MC12_inclb_OC2KmuSS_gencut_BKG"
#    "MC12_inclb_OC2Kplusmu_gencut_BKG"
#    "MC12_Bu2JpsiK_SIGNAL"
#    "MC12_Bs2JpsiPhi_SIGNAL"
#    "MC11_Bs2DsMuNu_Cocktail_SIGNAL"
#    "MC11_Bs2DsMuNu_Cocktail_BKG"
#    "MC11_Bs2KMuNu_SIGNAL"
#    "MC11_Bs2KstMuNu_BKG"
#    "MC11_Bs2JpsiPhi_BKG"
#    "MC11_Bd2JpsiKst_BKG"
#    "MC11_Bd2PiMuNu_NoMCut_BKG"
#    "MC11_Bd2PiMuNu_Mcut_BKG"
#    "MC11_Bu2JpsiK_BKG"
#    "MC11_Bu2RhoMuNu_BKG"
#    "MC11_Bu2D0MuNu_BKG"
#    "MC11_Bc2D0MuNu_BKG"
#    "MC11_Lb2PMuNu_LCSR_BKG"
#    "MC11_Bd2Rhopi0piMuNu_BKG"
#    "MC11_Bs2K2stkpi0MuNu_BKG"
#    "MC11_Bs2Kst1430kpi0MuNu_BKG"
#    "MC11_inclb_OC2Kmu_BKG"
#    "MC11_inclb_OC2KmuSS_BKG"
#    "MC11_inclb_OC2Kplusmu_BKG"
# "MC11_11995200"
# "MC11_11995202"
# "MC11_12995600"
# "MC11_12995602"
# "MC11_13996200"
# "MC11_13996202"
# "MC11_15998000"
# "MC11_15998002"
# "MC11_11995201"
# "MC11_12995601"
# "MC11_13996201"

#    "MC12_Bd2DsDst_BKG"   
#    "MC12_Bs2DsstDsst_BKG"
#    "MC12_Bu2DsstD0st_BKG"

#    "data15_Bs2KMuNu"
#    "data15_Bs2KMuNuSS"
#"MC12_10010037"

processmodes = [
#   "MC12_Bs2DsMuNu_Cocktail_BKG"
#   "MC11_Bs2DsMuNu_Cocktail_BKG",
#   "MC12_Bd2DsDst_BKG"   ,
#   "MC12_Bs2DsstDsst_BKG",
#   "MC12_Bu2DsstD0st_BKG",
#
#    "data11_Bs2DsMuNu",
    "data12_Bs2DsMuNu"
#    "data11_Bs2KMuNu",
#    "data11_Bs2KMuNuSS",
#    "data12_Bs2KMuNu",
    #"data12_Bs2KMuNuSS"

#"data12_Bs2DsMuNu"
#"data12_Bs2DsMuNuSS"
# "MC11_11995200",
# "MC11_11995202",
# "MC11_12995600",
# "MC11_12995602",
# "MC11_13996202",

# "MC11_12995601",
# "MC11_11995201",
# "MC11_15998002",

# "13996200",
# "15998000",
# "13996201",
#"MC12_10010037"
#"MC11_Bs2KMuNu_SIGNAL",
#"MC12_Bs2KMuNu_SIGNAL"

]
pol = ["Up","Down"]
username="i/ismith"
for mode in processmodes:
    for Polarity in pol:
        print "calling gangajob for mode " + mode + " and Polarity " + Polarity
        execfile("gangaSubmit.py")
        #execfile("newGanga.py")

