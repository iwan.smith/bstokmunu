#######################################################################
from Gaudi.Configuration import *
#######################################################################
from DecayTreeTuple.Configuration import *
#######################################################################
from PhysSelPython.Wrappers import AutomaticData, DataOnDemand, Selection, SelectionSequence, MergedSelection
import GaudiKernel.SystemOfUnits as Units

from Configurables import DaVinci, DecayTreeTuple, TupleToolTrigger, LoKi__Hybrid__TupleTool, TupleToolTagging, TupleToolTISTOS, TupleToolDecay, TupleToolNeutrinoReco, LoKi__Hybrid__EvtTupleTool, MCTupleToolInteractions, L0TriggerTisTos , TriggerTisTos
from Configurables import GaudiSequencer, TupleToolTrackInfo, TupleToolPropertime, ReadHltReport, TrackMasterFitter
from Configurables import TupleToolStripping, TupleToolRecoStats, TupleToolGeometry, TupleToolPid, TupleToolMuonPid
from Configurables import TupleToolAngles
from Configurables import LoKi__HDRFilter as StripFilter
from Configurables import LoKi__VoidFilter as VoidFilter
from StandardParticles import StdLooseKaons, StdLooseMuons
from Configurables import TisTosParticleTagger


from Configurables import FilterDesktop , CombineParticles
from Configurables import TupleToolEventInfo
from StrippingConf.Configuration import StrippingConf, StrippingStream
from StrippingSettings.Utils import strippingConfiguration
from StrippingArchive.Utils import buildStream
from StrippingArchive import strippingArchive
import re
stripping             = 'stripping21r1p1'
#get the configuration dictionary from the database
config                = strippingConfiguration(stripping)
#get the line builders from the archive
archive               = strippingArchive(stripping)

stream = buildStream(stripping = config, streamName='Semileptonic', archive = archive)
MyStream = StrippingStream("b2KMu.newstrip")
# Select my lines
MyLines = [
    #'StrippingB2XuMuNuBs2KLine',
    #'Strippingb2DsMuXB2DMuNuXLine'
    'StrippingB2DMuNuX_Ds'
    #'StrippingB2XuMuNuBs2K_FakeKMuLine'
    ]

for line in stream.lines:
    # Set prescales to 1.0 if necessary
    line._prescale = 1.0
    if line.name() in MyLines :
        MyStream.appendLines( [ line ] )

from Configurables import ProcStatusCheck

sc = StrippingConf( Streams = [ MyStream ],
                    MaxCandidates = 2000,
                    #AcceptBadEvents = False,
                    #BadEventSelection = ProcStatusCheck(),
                    #TESPrefix = 'StripTest',
                    #HDRLocation="Phys/ReportsSuck",
                    #ActiveMDSTStream = True,
                    Verbose = True,
                    )
MessageSvc().Format = "% F%60W%S%7W%R%T %0W%M"


#add trigger requirement
# from PhysConf.Filters import LoKi_Filters
# trigfltrs = LoKi_Filters (
#     L0DU_Code = "L0_CHANNEL_RE('Muon|DiMuon')",
#     HLT_Code = "(HLT_PASS_RE('Hlt1Track.*Decision') | HLT_PASS_RE('Hlt1.*Muon.*Decision')) & (HLT_PASS_RE('Hlt2.*Topo.*Decision') | HLT_PASS_RE('Hlt2.*SingleMuon.*Decision'))"
# from Configurables import DaVinci
# DaVinci().EventPreFilters = trigfltrs.filters('TrigFilters')

MyStream.sequence().IgnoreFilterPassed = False # so that we do not get all events written out



def makeNewStripSel_Bs2KMuNu(isSS):
    # we think that these cuts here are not so important, so we decided to do not apply them and forget about the problem with the GEC and VoidFilter
    # GEC1_filter = VoidFilter( "GEC1_filter_cut",
    #                           Code = "ALG_EXECUTED('StrippingStreamSemileptonicBadEvent') & ~ALG_PASSED('StrippingStreamSemileptonicBadEvent')",
    #                           Preambulo = [ 'from LoKiHlt.algorithms import *' ]
    #                           )
    # GEC1_sel = Selection("GEC1_sel", Algorithm = ??, RequiredSelections = [??])

    # GEC2_filter = VoidFilter( "GEC2_filter_cut",
    #                              Code = "( recSummaryTrack(LHCb.RecSummary.nLongTracks, TrLONG) < 250.0 )",
    #                              Preambulo = [ 'from LoKiTracks.decorators import *' ]
    #                              )
    # GEC2_sel = Selection("GEC2_sel", Algorithm = GEC2_filter)


    # CUT FOR KAON
    kaons_particles_filter = FilterDesktop("kaons_particles_cut",
                                           Code = "(TRCHI2DOF < 6.0 )& (P> 10000.0 *MeV) & (PT> 500.0 *MeV)& (TRGHOSTPROB < 0.5)& (MIPCHI2DV(PRIMARY)> 16 )"
                                           # & (PIDK-PIDpi> 5.0 )& (PIDK-PIDp> 5.0 )& (PIDK-PIDmu> 5.0 ) # PID cut removed because of PID correction will be purely data driven
                                           )
    kaons_particles_sel = Selection("kaons_particles_cut_sel",
                                    Algorithm = kaons_particles_filter,
                                    RequiredSelections = [ StdLooseKaons ])

    # CUT FOR MUON
    Muons_particles_filter= FilterDesktop("Muons_particles_cut",
                                          Code = "(TRCHI2DOF < 4.0 ) & (P> 6000.0 *MeV) & (PT> 1500.0* MeV)& (TRGHOSTPROB < 0.35)& (MIPCHI2DV(PRIMARY)> 12)" # & (TOS('L0.*Muon.*Decision', 'TriggerTisTos'))"
                                          # & (PIDmu-PIDpi> 3.0 )& (PIDmu-PIDp> 0.0 )& (PIDmu-PIDK> 0.0 ) # PID cut removed because of PID correction will be purely data driven
                                          )
    Muons_particles_sel = Selection("Muons_particles_cut_sel",
                                    Algorithm = Muons_particles_filter,
                                    RequiredSelections = [ StdLooseMuons ]
                                    )
    #muon trigger request
    myMuTagger = TisTosParticleTagger("MyMuTagger")
    myMuTagger.TisTosSpecs = { 'L0.*Muon.*Decision%TOS' : 0 }
    selTrigger = Selection("SelTriggL0Muon", Algorithm = myMuTagger, RequiredSelections = [ Muons_particles_sel ])

    # CUT FOR Bs
    decaydesc = '[B_s~0 -> K+ mu-]cc'
    if(isSS): decaydesc = '[B_s~0 -> K+ mu+]cc'
    Bs2KMu    = CombineParticles("Bs2KMu",
                                 DecayDescriptors = [ decaydesc ] ,
                                 CombinationCut   = "ATRUE"
                                 )
    Bs2KMu.Preambulo        = ["from LoKiPhysMC.decorators import *","from LoKiPhysMC.functions import mcMatch"]
    Bs2KMu.MotherCut        = "(VFASPF(VCHI2/VDOF)< 4.0) & (BPVDIRA> 0.994)& (BPVVDCHI2 >120.0) & (BPVCORRM > 2500.0 *MeV) & (BPVCORRM < 7000.0 *MeV)"
    Bs2KMu.DaughtersCuts    = { '' : 'ALL' , 'K+' : 'ALL' , 'K-' : 'ALL' , 'mu+' : 'ALL' , 'mu-' : 'ALL' }
    Bs2KMu_Sel = Selection("Bs2KMu_Sel",
                           Algorithm = Bs2KMu ,
                           RequiredSelections = [ selTrigger ,  kaons_particles_sel ]
                           )
    # Bs trigger request
    myBsTagger = TisTosParticleTagger("MyBsTagger")
    myBsTagger.TisTosSpecs = { 'Hlt2.*SingleMuon.*Decision%TOS' : 0 , 'Hlt2.*TopoMu2Body.*Decision%TOS' : 0 }
    selTrigger = Selection("SelTriggerTopoMu2", Algorithm = myBsTagger, RequiredSelections = [ Bs2KMu_Sel ])

    SeqBs2KmuNu = SelectionSequence('SeqBs2KmuNu', TopSelection = selTrigger)
    SeqBs2KmuNu.outputLevel = 6
    return SeqBs2KmuNu



def makeLooseTuple_Bs2KMuNu():
    # CUT FOR KAON
    kaons_particles_filter = FilterDesktop("kaons_particles_cut",
                                           Code = "('K-'==ABSID) & (P> 0.0 *MeV)"
                                           # & (PIDK-PIDpi> 5.0 )& (PIDK-PIDp> 5.0 )& (PIDK-PIDmu> 5.0 ) # PID cut removed because of PID correction will be purely data driven
                                           )
    kaons_particles_sel = Selection("kaons_particles_cut_sel",
                                    Algorithm = kaons_particles_filter,
                                    RequiredSelections = [ StdLooseKaons ])

    # CUT FOR MUON
    Muons_particles_filter= FilterDesktop("Muons_particles_cut",
                                          Code = "('mu+'==ABSID) & (P> 0.0 *MeV)" # & (TOS('L0.*Muon.*Decision', 'TriggerTisTos'))"
                                          # & (PIDmu-PIDpi> 3.0 )& (PIDmu-PIDp> 0.0 )& (PIDmu-PIDK> 0.0 ) # PID cut removed because of PID correction will be purely data driven
                                          )
    Muons_particles_sel = Selection("Muons_particles_cut_sel",
                                    Algorithm = Muons_particles_filter,
                                    RequiredSelections = [ kaons_particles_sel, StdLooseMuons ]
                                    )
    # CUT FOR Bs
    decaydesc = '[B_s~0 -> K+ mu-]cc'
    Bs2KMu    = CombineParticles("Bs2KMu",
                                 DecayDescriptors = [ decaydesc ] ,
                                 CombinationCut   = "ATRUE"
                                 )
    Bs2KMu.Preambulo        = ["from LoKiPhysMC.decorators import *","from LoKiPhysMC.functions import mcMatch"]
    Bs2KMu.MotherCut        = "(BPVCORRM > 0.0 *MeV)"
    Bs2KMu.DaughtersCuts    = { '' : 'ALL' , 'K+' : 'ALL' , 'K-' : 'ALL' , 'mu+' : 'ALL' , 'mu-' : 'ALL' }
    Bs2KMu_Sel = Selection("Bs2KMu_Sel",
                           Algorithm = Bs2KMu ,
                           RequiredSelections = [ Muons_particles_sel ,  kaons_particles_sel ]
                           )

    SeqBs2KmuNu = SelectionSequence('SeqBs2KmuNu', TopSelection = Bs2KMu_Sel)
    SeqBs2KmuNu.outputLevel = 6
    return SeqBs2KmuNu




def makeBu2JpsiKSel(isdata):
    Kaon_cuts = "(MINTREE(ABSID=='K+',P)> 10000.0 *MeV ) & (MINTREE(ABSID=='K+',PT)> 800.0 *MeV)& (MINTREE(ABSID=='K+',TRGHOSTPROB)< 0.5) & (MINTREE(ABSID=='K+',PIDK-PIDpi)> 5.0 )& (MINTREE(ABSID=='K+',PIDK-PIDp)> 5.0 )& (MINTREE(ABSID=='K+',PIDK-PIDmu)> 5.0 ) & (MINTREE(ABSID=='K+',MIPCHI2DV(PRIMARY))> 16)"
    Muon_cuts = "(MINTREE(ABSID=='mu+',P)> 6000.0 *MeV) & (MINTREE(ABSID=='mu+',PT)> 1500.0* MeV)& (MINTREE(ABSID=='mu+',TRGHOSTPROB) < 0.35)& (MINTREE(ABSID=='mu+',PIDmu-PIDpi)> 3.0 )& (MINTREE(ABSID=='mu+',PIDmu-PIDp)> 0.0 )& (MINTREE(ABSID=='mu+',PIDmu-PIDK)> 0.0 )& (MINTREE(ABSID=='mu+',MIPCHI2DV(PRIMARY))> 12)"
    Jpsi_cuts = "( ( (BPVDLS>3) | (BPVDLS<-3) ) ) &  ( (MM > 2996.916) & (MM < 3196.916) ) & (VFASPF(VCHI2PDOF)< 20.0)"
    Bu_Mothercuts  = "(VFASPF(VCHI2/VDOF)< 4.0) & (BPVDIRA> 0.994) & (BPVVDCHI2 >120.0)"
    if(isdata):
        Bu_Mothercuts  = "(VFASPF(VCHI2/VDOF)< 20.0) & (BPVDIRA> 0.994) & (BPVVDCHI2 >120.0) & (M>5100) & (M<6000)"
    Kaon_particles  = DataOnDemand( Location ='Phys/StdLooseKaons/Particles')
    Jpsi_particles  = DataOnDemand( Location ='Phys/StdMassConstrainedJpsi2MuMu/Particles')
    if(isdata):
        Jpsi_particles  = DataOnDemand( Location ='/Event/Dimuon/Phys/FullDSTDiMuonJpsi2MuMuDetachedLine/Particles')
    Kaon_Fltr = FilterDesktop("Phis_Fltr", Code = Kaon_cuts)
    Kaon_sel  = Selection("Kaon_sel", Algorithm = Kaon_Fltr, RequiredSelections = [Kaon_particles])
    Jpsi_Fltr = FilterDesktop("Jpsi_Fltr", Code = Muon_cuts +" & " + Jpsi_cuts)
    Jpsi_sel  = Selection("Jpsi_sel"     , Algorithm     = Jpsi_Fltr          , RequiredSelections = [Jpsi_particles])

    forMotherCut = "(VFASPF(VCHI2/VDOF)< 25) & (BPVDIRA> 0.95) & (mcMatch ('[ B+ => J/psi(1S) K+ ]CC'))"
    if(isdata):
        forMotherCut = "(VFASPF(VCHI2/VDOF)< 25) & (BPVDIRA> 0.95)"
    Bu2JpsiK = CombineParticles("Bu2JpsiK",
                                DecayDescriptors = [ '[B+ -> J/psi(1S) K+]cc' ] ,
                                CombinationCut   = "(AM> 500.0*MeV) & (AM<6000.0*MeV)" ,
                                MotherCut        = Bu_Mothercuts
                                )
    Bu2JpsiK.Preambulo      = ["from LoKiPhysMC.decorators import *","from LoKiPhysMC.functions import mcMatch"]
    Bu2JpsiK.MotherCut      = forMotherCut
    Bu2JpsiK_Sel = Selection("Bu2JpsiK_Sel",
                             Algorithm = Bu2JpsiK,
                             RequiredSelections = [ Jpsi_sel, Kaon_sel ]
                             )
    SeqBu2JpsiK = SelectionSequence('SeqBu2JpsiK', TopSelection = Bu2JpsiK_Sel)
    SeqBu2JpsiK.outputLevel = 6
    return SeqBu2JpsiK


# def makeBd2JpsiKstSel(isdata): Bd2JpsiKst or Bu2JpsiKst ???? K*(892)0 or  K*(892)+ is the difference (I think the first but then need to understand the selection better)
#     Kaon_cuts = "(MINTREE(ABSID=='K*(892)0',P)> 10000.0 *MeV ) & (MINTREE(ABSID=='K*(892)0',PT)> 800.0 *MeV)& (MINTREE(ABSID=='K*(892)0',TRGHOSTPROB)< 0.5) & (MINTREE(ABSID=='K*(892)0',PIDK-PIDpi)> 5.0 )& (MINTREE(ABSID=='K*(892)0',PIDK-PIDp)> 5.0 )& (MINTREE(ABSID=='K*(892)0',PIDK-PIDmu)> 5.0 ) & (MINTREE(ABSID=='K*(892)0',MIPCHI2DV(PRIMARY))> 16)"
#     Muon_cuts = "(MINTREE(ABSID=='mu+',P)> 6000.0 *MeV) & (MINTREE(ABSID=='mu+',PT)> 1500.0* MeV)& (MINTREE(ABSID=='mu+',TRGHOSTPROB) < 0.35)& (MINTREE(ABSID=='mu+',PIDmu-PIDpi)> 3.0 )& (MINTREE(ABSID=='mu+',PIDmu-PIDp)> 0.0 )& (MINTREE(ABSID=='mu+',PIDmu-PIDK)> 0.0 )& (MINTREE(ABSID=='mu+',MIPCHI2DV(PRIMARY))> 12)"
#     Jpsi_cuts = "( ( (BPVDLS>3) | (BPVDLS<-3) ) ) &  ( (MM > 2996.916) & (MM < 3196.916) ) & (VFASPF(VCHI2PDOF)< 20.0)"
#     Bd_Mothercuts  = "(VFASPF(VCHI2/VDOF)< 4.0) & (BPVDIRA> 0.994) & (BPVVDCHI2 >120.0)"
#     if(isdata):
#         Bd_Mothercuts  = "(VFASPF(VCHI2/VDOF)< 20.0) & (BPVDIRA> 0.994) & (BPVVDCHI2 >120.0) & (M>5100) & (M<6000)"
#     Kaon_particles  = DataOnDemand( Location ='Phys/StdLooseKaons/Particles')
#     Jpsi_particles  = DataOnDemand( Location ='Phys/StdMassConstrainedJpsi2MuMu/Particles')
#     if(isdata):
#         Jpsi_particles  = DataOnDemand( Location ='/Event/Dimuon/Phys/FullDSTDiMuonJpsi2MuMuDetachedLine/Particles')
#     Kaon_Fltr = FilterDesktop("Phis_Fltr", Code = Kaon_cuts)
#     Kaon_sel  = Selection("Kaon_sel", Algorithm = Kaon_Fltr, RequiredSelections = [Kaon_particles])
#     Jpsi_Fltr = FilterDesktop("Jpsi_Fltr", Code = Muon_cuts +" & " + Jpsi_cuts)
#     Jpsi_sel  = Selection("Jpsi_sel"     , Algorithm     = Jpsi_Fltr          , RequiredSelections = [Jpsi_particles])

#     forMotherCut = "(VFASPF(VCHI2/VDOF)< 25) & (BPVDIRA> 0.95) & (mcMatch ('[ B0 => J/psi(1S) K*(892)0 ]CC'))"
#     if(isdata):
#         forMotherCut = "(VFASPF(VCHI2/VDOF)< 25) & (BPVDIRA> 0.95)"
#     Bd2JpsiK = CombineParticles("Bd2JpsiK",
#                                 DecayDescriptors = [ '[B0 -> J/psi(1S) K*(892)0]cc' ] ,
#                                 CombinationCut   = "(AM> 500.0*MeV) & (AM<6000.0*MeV)" ,
#                                 MotherCut        = Bd_Mothercuts
#                                 )
#     Bd2JpsiK.Preambulo      = ["from LoKiPhysMC.decorators import *","from LoKiPhysMC.functions import mcMatch"]
#     Bd2JpsiK.MotherCut      = forMotherCut
#     Bd2JpsiK_Sel = Selection("Bd2JpsiK_Sel",
#                              Algorithm = Bd2JpsiK,
#                              RequiredSelections = [ Jpsi_sel, Kaon_sel ]
#                              )
#     SeqBd2JpsiK = SelectionSequence('SeqBd2JpsiK', TopSelection = Bd2JpsiK_Sel)
#     SeqBd2JpsiK.outputLevel = 6
#     return SeqBd2JpsiK


def makeBs2JpsiPhiSel(isdata):
    Kaon_cuts = "(MINTREE(ABSID=='K+',P)> 10000.0 *MeV ) & (MINTREE(ABSID=='K+',PT)> 800.0 *MeV)& (MINTREE(ABSID=='K+',TRGHOSTPROB)< 0.5) & (MINTREE(ABSID=='K+',PIDK-PIDpi)> 5.0 )& (MINTREE(ABSID=='K+',PIDK-PIDp)> 5.0 )& (MINTREE(ABSID=='K+',PIDK-PIDmu)> 5.0 ) & (MINTREE(ABSID=='K+',MIPCHI2DV(PRIMARY))> 16)"
    phi_cuts  = "(VFASPF(VCHI2) < 16.0) & (M> 980.0*MeV) & (M<1040.0*MeV)"
    Muon_cuts = "(MINTREE(ABSID=='mu+',P)> 6000.0 *MeV) & (MINTREE(ABSID=='mu+',PT)> 1500.0* MeV)& (MINTREE(ABSID=='mu+',TRGHOSTPROB) < 0.35)& (MINTREE(ABSID=='mu+',PIDmu-PIDpi)> 3.0 )& (MINTREE(ABSID=='mu+',PIDmu-PIDp)> 0.0 )& (MINTREE(ABSID=='mu+',PIDmu-PIDK)> 0.0 )& (MINTREE(ABSID=='mu+',MIPCHI2DV(PRIMARY))> 12)"
    Jpsi_cuts = "( ( (BPVDLS>3) | (BPVDLS<-3) ) ) &  ( (MM > 2996.916) & (MM < 3196.916) ) & (VFASPF(VCHI2PDOF)< 20.0)"
    Bs_Mothercuts  = "(VFASPF(VCHI2/VDOF)< 4.0) & (BPVDIRA> 0.994) & (BPVVDCHI2 >120.0)"
    phi_particles   = DataOnDemand( Location ='Phys/StdLooseDetachedPhi2KK/Particles')
    Jpsi_particles  = DataOnDemand( Location ='Phys/StdMassConstrainedJpsi2MuMu/Particles')
    if(isdata):
        Jpsi_particles  = DataOnDemand( Location ='/Event/Dimuon/Phys/FullDSTDiMuonJpsi2MuMuDetachedLine/Particles')

    Phi_Fltr = FilterDesktop("Phis_Fltr", Code = Kaon_cuts +" & " + phi_cuts)
    Phi_sel  = Selection("Phi_sel", Algorithm = Phi_Fltr, RequiredSelections = [phi_particles])
    Jpsi_Fltr = FilterDesktop("Jpsi_Fltr", Code = Muon_cuts +" & " + Jpsi_cuts)
    Jpsi_sel  = Selection("Jpsi_sel", Algorithm = Jpsi_Fltr, RequiredSelections = [Jpsi_particles])

    Bs2JpsiPhi    = CombineParticles("Bs2JpsiPhi",
                                     DecayDescriptors = [ ' B_s0 -> J/psi(1S) phi(1020)' ] ,
                                     CombinationCut   = "(AM> 500.0*MeV) & (AM<6000.0*MeV)" ,
                                     MotherCut        = Bs_Mothercuts
                                     )
    if(not isdata):
        Bs2JpsiPhi.Preambulo      = ["from LoKiPhysMC.decorators import *","from LoKiPhysMC.functions import mcMatch"]
    Bs2JpsiPhi.MotherCut      = "(VFASPF(VCHI2/VDOF)< 25) & (BPVDIRA> 0.95) & (mcMatch ('[ [B_s~0]cc => J/psi(1S) phi(1020) ]CC'))"
    if(isdata):
        Bs2JpsiPhi.MotherCut      = "(VFASPF(VCHI2/VDOF)< 25) & (BPVDIRA> 0.95)"
    Bs2JpsiPhi_Sel            = Selection("Bs2JpsiPhi_Sel",
                                          Algorithm = Bs2JpsiPhi ,
                                          RequiredSelections = [ Jpsi_sel,Phi_sel ]
                                          )
    SeqBs2JpsiPhi = SelectionSequence('SeqBs2JpsiPhi', TopSelection = Bs2JpsiPhi_Sel)
    SeqBs2JpsiPhi.outputLevel = 6
    return SeqBs2JpsiPhi



def makeTuple(InputLocation, mode,isopath='Phys/StdNoPIDsVeloPions/Particles'):
    isMC = ("MC" in mode)
    isNormalisation = (("DsMuNu_Cocktail_SIGNAL" in mode) or ("data12_Bs2DsMuNu" in mode) or ("data11_Bs2DsMuNu" in mode) or ("Bd2DsDst" in mode) or ("Bs2DsstDsst" in mode) or ("Bu2DsstD0st" in mode) or ("MC11_1" in mode))
    if isNormalisation and ("SS" in mode):
        isNormalisation = False
        isSSNormalisation = True
    if isNormalisation:
        tuple = DecayTreeTuple("Bs2DsMuNuTuple")
    elif isSSNormalisation:
        tuple = DecayTreeTuple("Bs2DsMuNuSSTuple")
    else:
        tuple = DecayTreeTuple("Bs2KmuNuTuple")
    if(("MC12_Bs2JpsiPhi_SIGNAL" in mode) or ("data12_Bs2JpsiPhi" in mode) or ("data11_Bs2JpsiPhi" in mode)):
        tuple.Decay    = '[B_s0 -> ^(J/psi(1S)-> ^mu+ ^mu-) ^(phi(1020) -> ^K+ ^K-)]CC'
        tuple.addBranches( {
            "muon_p"   : "[B_s0 ->  (J/psi(1S)-> ^mu+  mu-)  (phi(1020) ->  K+  K-)]CC"
            ,"muon_m"  : "[B_s0 ->  (J/psi(1S)->  mu+ ^mu-)  (phi(1020) ->  K+  K-)]CC"
            ,"kaon_p"  : "[B_s0 ->  (J/psi(1S)->  mu+  mu-)  (phi(1020) -> ^K+  K-)]CC"
            ,"kaon_m"  : "[B_s0 ->  (J/psi(1S)->  mu+  mu-)  (phi(1020) ->  K+ ^K-)]CC"
            ,"Jpsi"    : "[B_s0 -> ^(J/psi(1S)->  mu+  mu-)  (phi(1020) ->  K+  K-)]CC"
            ,"phi"     : "[B_s0 ->  (J/psi(1S)->  mu+  mu-) ^(phi(1020) ->  K+  K-)]CC"
            ,"Bs"      : "[B_s0 ->  (J/psi(1S)->  mu+  mu-)  (phi(1020) ->  K+  K-)]CC"
        })
    elif(("Bu2JpsiK_SIGNAL" in mode) or ("data12_Bu2JpsiK" in mode) or ("data11_Bu2JpsiK" in mode)):
        tuple.Decay    = '[B- -> ^(J/psi(1S)-> ^mu+ ^mu-) ^K- ]CC'
        tuple.addBranches( {
            "muon_p"   : '[B- ->  (J/psi(1S)-> ^mu+  mu-)  K- ]CC'
            ,"muon_m"  : '[B- ->  (J/psi(1S)->  mu+ ^mu-)  K- ]CC'
            ,"kaon_m"  : '[B- ->  (J/psi(1S)->  mu+  mu-) ^K- ]CC'
            ,"Jpsi"    : '[B- -> ^(J/psi(1S)->  mu+  mu-)  K- ]CC'
            ,"Bs"      : '[B- ->  (J/psi(1S)->  mu+  mu-)  K- ]CC' # it shold be Bu but with Bs the code below its the same
        })
    elif("KMuNuSS" in mode):
        tuple.Decay    = "[B_s0 -> ^K+ ^mu+]CC"
        tuple.addBranches( {
            "muon_p"  : "[B_s0 ->  K+ ^mu+]CC"
            ,"kaon_m"  : "[B_s0 -> ^K+  mu+]CC"
            ,"Bs"      : "[B_s0 ->  K+  mu+]CC"
        })
    elif isNormalisation:
        tuple.Decay    = "[ [B~0]cc -> ^(D+ -> ^K- ^K+ ^pi+ ) ^mu-]CC"
        tuple.addBranches( {
            "muon_p"   : "[ [B~0]cc ->  (D+ ->  K-  K+  pi+ ) ^mu-]CC"
            ,"kaon_m"  : "[ [B~0]cc ->  (D+ -> ^K-  K+  pi+ )  mu-]CC"
            ,"pi_p"    : "[ [B~0]cc ->  (D+ ->  K-  K+ ^pi+ )  mu-]CC"
            ,"kaon_p"  : "[ [B~0]cc ->  (D+ ->  K- ^K+  pi+ )  mu-]CC"
            ,"Ds"      : "[ [B~0]cc -> ^(D+ ->  K-  K+  pi+ )  mu-]CC"
            ,"Bs"      : "[ [B~0]cc ->  (D+ ->  K-  K+  pi+ )  mu-]CC"
        })
    elif isSSNormalisation:
        tuple.Decay    = "[ [B0]cc -> ^(D+ -> ^K- ^K+ ^pi+ ) ^mu+]CC"
        tuple.addBranches( {
            "muon_p"   : "[ [B0]cc ->  (D+ ->  K-  K+  pi+ ) ^mu+]CC"
            ,"kaon_m"  : "[ [B0]cc ->  (D+ -> ^K-  K+  pi+ )  mu+]CC"
            ,"pi_p"    : "[ [B0]cc ->  (D+ ->  K-  K+ ^pi+ )  mu+]CC"
            ,"kaon_p"  : "[ [B0]cc ->  (D+ ->  K- ^K+  pi+ )  mu+]CC"
            ,"Ds"      : "[ [B0]cc -> ^(D+ ->  K-  K+  pi+ )  mu+]CC"
            ,"Bs"      : "[ [B0]cc ->  (D+ ->  K-  K+  pi+ )  mu+]CC"
        })
    elif("Bs2DsMuNuSS" in mode):
        tuple.Decay = "[ [B0]cc -> ^(D+ -> ^K- ^K+ ^pi+ ) ^mu+]CC"
        tuple.addBranches( {
            "muon_p"   : "[ [B0]cc ->  (D+ ->  K-  K+  pi+ ) ^mu+]CC"
            ,"kaon_m"  : "[ [B0]cc ->  (D+ -> ^K-  K+  pi+ )  mu+]CC"
            ,"pi_p"    : "[ [B0]cc ->  (D+ ->  K-  K+ ^pi+ )  mu+]CC"
            ,"K_p"     : "[ [B0]cc ->  (D+ ->  K- ^K+  pi+ )  mu+]CC"
            ,"Ds"      : "[ [B0]cc -> ^(D+ ->  K-  K+  pi+ )  mu+]CC"
            ,"Bs"      : "[ [B0]cc ->  (D+ ->  K-  K+  pi+ )  mu+]CC"
        })
    else:
        tuple.Decay    = "[B_s0 -> ^K- ^mu+]CC"
        tuple.addBranches( {
            "muon_p"   : "[B_s0 ->  K- ^mu+]CC"
            ,"kaon_m"  : "[B_s0 -> ^K-  mu+]CC"
            ,"Bs"      : "[B_s0 ->  K-  mu+]CC"
        })

    tuple.Inputs = [InputLocation]
    tuple.ToolList +=  [
        "TupleToolGeometry"
        ,"TupleToolKinematic"
        , "TupleToolPrimaries"
        , "TupleToolTrackInfo"
        # ,"TupleToolTISTOS"
        # ,"TupleToolPid"
        , "LoKi::Hybrid::TupleTool/LoKiTool"
        ,"TupleToolEventInfo"
    ]
    # tts = tuple.addTupleTool("TupleToolStripping") #Flag if the stripping line below select the event
    # tts.StrippingList = ["StrippingKS02MuMuLineDecision", "etc..."]
    tuple.ReFitPVs = True
    tuple.OutputLevel = 6


    TupleToolIsoGeneric              =  tuple.addTupleTool("TupleToolIsoGeneric")
    TupleToolIsoGeneric.ParticlePath =  isopath
    TupleToolIsoGeneric.VerboseMode = True
    if(isMC):
        MCTruth_noniso          = TupleToolMCTruth('MCTruth_noniso')
        MCTruth_noniso.ToolList = ["MCTupleToolHierarchy"]
        TupleToolIsoGeneric.addTool(MCTruth_noniso)
        TupleToolIsoGeneric.ToolList += ["TupleToolMCTruth/MCTruth_noniso"]
    LoKiTool_noniso = LoKi__Hybrid__TupleTool("LoKiTool_noniso")
    LoKiTool_noniso.Variables     = { "ETA" : "ETA" , "PHI" : "PHI" , "TRTYPE" : "TRTYPE"}
    TupleToolIsoGeneric.ToolList += ["LoKi::Hybrid::TupleTool/LoKiTool_noniso"]
    TupleToolIsoGeneric.addTool(LoKiTool_noniso)
    TupleToolIsoGeneric.OutputLevel = 6

    LoKiVariables = tuple.Bs.addTupleTool("LoKi::Hybrid::TupleTool/LoKiVariables")
    LoKiVariables.Variables = {
        "ETA"              : "ETA",
        "PHI"              : "PHI",
        "DOCA"             : "DOCA(1,2)",
        "DTF_TAU"          : "DTF_CTAU( 0, True )/0.299792458",
        "DTF_CTAUS"        : "DTF_CTAUSIGNIFICANCE( 0, True )",
        "DTF_CHI2NDOF"     : "DTF_CHI2NDOF( True )",
        "FD_CHI2_LOKI"     : "BPVVDCHI2",
        "VCHI2_LOKI"       : "VFASPF(VCHI2/VDOF)",
        "FD_S"             : "BPVDLS",
        "cosTheta1_star"   : "LV01",
        "cosTheta2_star"   : "LV02",
    }
    if(isMC):
        tuple.ToolList +=  ["TupleToolMCBackgroundInfo"]
        LoKiVariables.Preambulo = [
            "from LoKiPhysMC.decorators import *",
            "from LoKiPhysMC.functions import mcMatch"
        ]
        #LoKiVariables = LoKi__Hybrid__TupleTool('LoKiVariables')
        LoKiVariables.Variables["TruthMatched"] = "switch( mcMatch ('[ [B_s~0]cc => K+ mu- Neutrino ]CC',1)   , 1 , 0 )"

    if(("MC12_Bs2JpsiPhi_SIGNAL" in mode) or ("data12_Bs2JpsiPhi" in mode) or ("data11_Bs2JpsiPhi" in mode)):
        LoKiVariables.Variables["M_JpsiConstr"] = "DTF_FUN ( M , True , 'J/psi(1S)' )"
        LoKiVariables.Variables["DTF_VCHI2NDOF"]= "DTF_FUN ( VFASPF(VCHI2/VDOF) , True  )"
        if(isMC):
            LoKiVariables.Variables["TM_Bs2JpsiPhi"] = "switch( mcMatch ('[ [B_s~0]cc => J/psi(1S) phi(1020) ]CC', 1 )  , 1 , 0 )"
    elif(("MC12_Bu2JpsiK_SIGNAL" in mode) or ("data12_Bu2JpsiK" in mode) or ("data11_Bu2JpsiK" in mode)):
        LoKiVariables.Variables["M_JpsiConstr"] = "DTF_FUN ( M , True , 'J/psi(1S)' )"
        LoKiVariables.Variables["DTF_VCHI2NDOF"] = "DTF_FUN ( VFASPF(VCHI2/VDOF) , True  )"
        if(isMC):
            LoKiVariables.Variables["TM_Bu2JpsiK"] = "switch( mcMatch ('[ B+ => J/psi(1S) K+ ]CC', 1 )  , 1 , 0 )"
    elif(isMC and isNormalisation):
        LoKiVariables.Variables["TM_DsMu"] = "switch( mcMatch ('[ [B_s~0]cc => D_s+ mu- Neutrino ]CC',1)   , 1 , 0 )"
        LoKiVariables.Variables["TM_DsTau"] = "switch( mcMatch ('[ [B_s~0]cc => D_s+ tau- Neutrino ]CC',1)   , 1 , 0 )"

        LoKiVariables.Variables["TM_DsstarMu_Dsg"] = "switch( mcMatch ('[ [B_s~0]cc => (D*_s+ => D_s+  gamma) mu- Neutrino ]CC',1)   , 1 , 0 )"
        LoKiVariables.Variables["TM_DsstarMu_Dspi0"] = "switch( mcMatch ('[ [B_s~0]cc => (D*_s+ => D_s+  pi0) mu- Neutrino ]CC',1)   , 1 , 0 )"
        LoKiVariables.Variables["TM_DsstarTau_Dsg"] = "switch( mcMatch ('[ [B_s~0]cc => (D*_s+ => D_s+  gamma) tau- Neutrino ]CC',1)   , 1 , 0 )"
        LoKiVariables.Variables["TM_DsstarTau_Dspi0"] = "switch( mcMatch ('[ [B_s~0]cc => (D*_s+ => D_s+  pi0) tau- Neutrino ]CC',1)   , 1 , 0 )"

        LoKiVariables.Variables["TM_Dsstar0Mu_Dspi0"] = "switch( mcMatch ('[ [B_s~0]cc => (D*_s0+ => D_s+   pi0) mu- Neutrino ]CC',1)   , 1 , 0 )"
        LoKiVariables.Variables["TM_Dsstar0Mu_Dsstarg_Dsg"] = "switch( mcMatch ('[ [B_s~0]cc => (D*_s0+ => (D*_s+ => D_s+  gamma)  gamma) mu- Neutrino ]CC',1)   , 1 , 0 )"
        LoKiVariables.Variables["TM_Dsstar0Mu_Dsstarg_Dspi0"] = "switch( mcMatch ('[ [B_s~0]cc => (D*_s0+ => (D*_s+ => D_s+  pi0)  gamma) mu- Neutrino ]CC',1)   , 1 , 0 )"
        LoKiVariables.Variables["TM_Dsstar0Mu_Dspipi"] = "switch( mcMatch ('[ [B_s~0]cc => (D*_s0+ => D_s+   pi+ pi-) mu- Neutrino ]CC',1)   , 1 , 0 )"
        LoKiVariables.Variables["TM_Dsstar0Mu_Dspi0pi0"] = "switch( mcMatch ('[ [B_s~0]cc => (D*_s0+ => D_s+   pi0 pi0) mu- Neutrino ]CC',1)   , 1 , 0 )"
        LoKiVariables.Variables["TM_Dsstar0Tau_Dspi0"] = "switch( mcMatch ('[ [B_s~0]cc => (D*_s0+ => D_s+   pi0) tau- Neutrino ]CC',1)   , 1 , 0 )"
        LoKiVariables.Variables["TM_Dsstar0Tau_Dsstarg_Dsg"] = "switch( mcMatch ('[ [B_s~0]cc => (D*_s0+ => (D*_s+ => D_s+  gamma)  gamma) tau- Neutrino ]CC',1)   , 1 , 0 )"
        LoKiVariables.Variables["TM_Dsstar0Tau_Dsstarg_Dspi0"] = "switch( mcMatch ('[ [B_s~0]cc => (D*_s0+ => (D*_s+ => D_s+  pi0)  gamma) tau- Neutrino ]CC',1)   , 1 , 0 )"
        LoKiVariables.Variables["TM_Dsstar0Tau_Dspipi"] = "switch( mcMatch ('[ [B_s~0]cc => (D*_s0+ => D_s+   pi+ pi-) tau- Neutrino ]CC',1)   , 1 , 0 )"
        LoKiVariables.Variables["TM_Dsstar0Tau_Dspi0pi0"] = "switch( mcMatch ('[ [B_s~0]cc => (D*_s0+ => D_s+   pi0 pi0) tau- Neutrino ]CC',1)   , 1 , 0 )"

        LoKiVariables.Variables["TM_Ds12460_Dsstarpi0_Dsg"] = "switch( mcMatch ('[ [B_s~0]cc => (D_s1(2460)+ => (D*_s+ => D_s+  gamma) pi0) mu- Neutrino ]CC',1)   , 1 , 0 )"
        LoKiVariables.Variables["TM_Ds12460_Dsstarpi0_Dspi0"] = "switch( mcMatch ('[ [B_s~0]cc => (D_s1(2460)+ => (D*_s+ => D_s+  pi0) pi0) mu- Neutrino ]CC',1)   , 1 , 0 )"
        LoKiVariables.Variables["TM_Ds12460_Dspipi"] = "switch( mcMatch ('[ [B_s~0]cc => (D_s1(2460)+ => D_s+  pi+ pi-) mu- Neutrino ]CC',1)   , 1 , 0 )"
        LoKiVariables.Variables["TM_Ds12460_Dspi0pi0"] = "switch( mcMatch ('[ [B_s~0]cc => (D_s1(2460)+ => D_s+  pi0 pi0) mu- Neutrino ]CC',1)   , 1 , 0 )"

        LoKiVariables.Variables["TM_Ds12536Mu_Dsstarpi0_Dspi0"] = "switch( mcMatch ('[ [B_s~0]cc => (D_s1(2536)+ => (D*_s0+ => D_s+   pi0) pi0) mu- Neutrino ]CC',1)   , 1 , 0 )"
        LoKiVariables.Variables["TM_Ds12536Mu_Dsstarg_Dsg"] = "switch( mcMatch ('[ [B_s~0]cc => (D_s1(2536)+ => (D*_s0+ => (D*_s+ => D_s+  gamma)  gamma) pi0) mu- Neutrino ]CC',1)   , 1 , 0 )"
        LoKiVariables.Variables["TM_Ds12536Mu_Dsstarg_Dspi0"] = "switch( mcMatch ('[ [B_s~0]cc => (D_s1(2536)+ => (D*_s0+ => (D*_s+ => D_s+  pi0)  gamma) pi0) mu- Neutrino ]CC',1)   , 1 , 0 )"
        LoKiVariables.Variables["TM_Ds12536Mu_Dsstarpi0_Dspipi"] = "switch( mcMatch ('[ [B_s~0]cc => (D_s1(2536)+ => (D*_s0+ => D_s+   pi+ pi-) pi0) mu- Neutrino ]CC',1)   , 1 , 0 )"
        LoKiVariables.Variables["TM_Ds12536Mu_Dsstarpi0_Dspi0pi0"] = "switch( mcMatch ('[ [B_s~0]cc => (D_s1(2536)+ => (D*_s0+ => D_s+   pi0 pi0) pi0) mu- Neutrino ]CC',1)   , 1 , 0 )"
        LoKiVariables.Variables["TM_Ds12536Mu_Dsg"] = "switch( mcMatch ('[ [B_s~0]cc => (D_s1(2536)+ => D_s+   gamma) mu- Neutrino ]CC',1)   , 1 , 0 )"
        LoKiVariables.Variables["TM_Ds12536Mu_Dspipi"] = "switch( mcMatch ('[ [B_s~0]cc => (D_s1(2536)+ => D_s+   pi+ pi-) mu- Neutrino ]CC',1)   , 1 , 0 )"
        LoKiVariables.Variables["TM_Ds12536Mu_Dspi0pi0"] = "switch( mcMatch ('[ [B_s~0]cc => (D_s1(2536)+ => D_s+   pi0 pi0) mu- Neutrino ]CC',1)   , 1 , 0 )"
        LoKiVariables.Variables["TM_Ds12536Tau_Dsstarpi0_Dspi0"] = "switch( mcMatch ('[ [B_s~0]cc => (D_s1(2536)+ => (D*_s0+ => D_s+   pi0) pi0) tau- Neutrino ]CC',1)   , 1 , 0 )"
        LoKiVariables.Variables["TM_Ds12536Tau_Dsstarg_Dsg"] = "switch( mcMatch ('[ [B_s~0]cc => (D_s1(2536)+ => (D*_s0+ => (D*_s+ => D_s+  gamma)  gamma) pi0) tau- Neutrino ]CC',1)   , 1 , 0 )"
        LoKiVariables.Variables["TM_Ds12536Tau_Dsstarg_Dspi0"] = "switch( mcMatch ('[ [B_s~0]cc => (D_s1(2536)+ => (D*_s0+ => (D*_s+ => D_s+  pi0)  gamma) pi0) tau- Neutrino ]CC',1)   , 1 , 0 )"
        LoKiVariables.Variables["TM_Ds12536Tau_Dsstarpi0_Dspipi"] = "switch( mcMatch ('[ [B_s~0]cc => (D_s1(2536)+ => (D*_s0+ => D_s+   pi+ pi-) pi0) tau- Neutrino ]CC',1)   , 1 , 0 )"
        LoKiVariables.Variables["TM_Ds12536Tau_Dsstarpi0_Dspi0pi0"] = "switch( mcMatch ('[ [B_s~0]cc => (D_s1(2536)+ => (D*_s0+ => D_s+   pi0 pi0) pi0) tau- Neutrino ]CC',1)   , 1 , 0 )"
        LoKiVariables.Variables["TM_Ds12536Tau_Dsg"] = "switch( mcMatch ('[ [B_s~0]cc => (D_s1(2536)+ => D_s+   gamma) tau- Neutrino ]CC',1)   , 1 , 0 )"
        LoKiVariables.Variables["TM_Ds12536Tau_Dspipi"] = "switch( mcMatch ('[ [B_s~0]cc => (D_s1(2536)+ => D_s+   pi+ pi-) tau- Neutrino ]CC',1)   , 1 , 0 )"
        LoKiVariables.Variables["TM_Ds12536Tau_Dspi0pi0"] = "switch( mcMatch ('[ [B_s~0]cc => (D_s1(2536)+ => D_s+   pi0 pi0) tau- Neutrino ]CC',1)   , 1 , 0 )"


    for branch in tuple.Branches: #add ETA and MIPCHI2DV_PV to all final state particles (all with a "_" in the branch name)
        if re.match(".*_.*",branch):
            LoKiVariables = getattr(tuple,branch).addTupleTool("LoKi::Hybrid::TupleTool/LoKiVariables_"+branch)
            LoKiVariables.Variables = {
                    "MIPCHI2DV_PV" : "MIPCHI2DV(PRIMARY)",
                    "ETA"          : "ETA"
                    }

    myTupleToolTISTOS = TupleToolTISTOS('TupleToolTISTOS')
    myTupleToolTISTOS.addTool(L0TriggerTisTos())
    myTupleToolTISTOS.addTool(TriggerTisTos())
    myTupleToolTISTOS.TriggerList=[
        "L0MuonDecision",
        "L0DiMuonDecision",
        "Hlt2TopoMu2BodyBBDTDecision",
        "Hlt2SingleMuonDecision",
        "Hlt2SingleMuonLowPTDecision"
        ]
    if isNormalisation or isSSNormalisation:
        myTupleToolTISTOS.TriggerList+=[
            "Hlt2Topo2BodyBBDTDecision",
            "Hlt2Topo3BodyBBDTDecision",
            "Hlt2Topo4BodyBBDTDecision",
            "Hlt2TopoMu3BodyBBDTDecision",
            "Hlt2TopoMu4BodyBBDTDecision",
        ]
    myTupleToolTISTOS.VerboseL0   = True
    myTupleToolTISTOS.VerboseHlt1 = True
    myTupleToolTISTOS.VerboseHlt2 = True
    myTupleToolTISTOS.TUS         = True
    myTupleToolTISTOS.TPS         = True

    tuple.Bs.addTool(myTupleToolTISTOS , name = 'TupleToolTISTOS' )
    tuple.Bs.ToolList   += [ "TupleToolTISTOS"]

    tuple.muon_p.addTool(myTupleToolTISTOS , name = 'TupleToolTISTOS' )
    tuple.muon_p.ToolList   += [ "TupleToolTISTOS"]

    if isNormalisation or isSSNormalisation:
        tuple.Ds.addTool(myTupleToolTISTOS , name = 'TupleToolTISTOS' )
        tuple.Ds.ToolList   += [ "TupleToolTISTOS"]
    else:
        tuple.kaon_m.addTool(myTupleToolTISTOS , name = 'TupleToolTISTOS' )
        tuple.kaon_m.ToolList   += [ "TupleToolTISTOS"]


    from Configurables import TupleToolConeIsolation
    tuple.kaon_m.addTupleTool("TupleToolConeIsolation")
    tuple.kaon_m.TupleToolConeIsolation.FillAsymmetry     = True
    tuple.kaon_m.TupleToolConeIsolation.FillDeltas        = True
    tuple.kaon_m.TupleToolConeIsolation.FillComponents    = True
    tuple.kaon_m.TupleToolConeIsolation.MinConeSize       = 0.5
    tuple.kaon_m.TupleToolConeIsolation.SizeStep          = 0.5
    tuple.kaon_m.TupleToolConeIsolation.MaxConeSize       = 2.0
    tuple.kaon_m.TupleToolConeIsolation.FillPi0Info       = True
    tuple.kaon_m.TupleToolConeIsolation.FillMergedPi0Info = True

    tuple.muon_p.addTupleTool("TupleToolConeIsolation")
    tuple.muon_p.TupleToolConeIsolation.FillAsymmetry     = True
    tuple.muon_p.TupleToolConeIsolation.FillDeltas        = True
    tuple.muon_p.TupleToolConeIsolation.FillComponents    = True
    tuple.muon_p.TupleToolConeIsolation.MinConeSize       = 0.5
    tuple.muon_p.TupleToolConeIsolation.SizeStep          = 0.5
    tuple.muon_p.TupleToolConeIsolation.MaxConeSize       = 2.0

    if(isMC):
        MCTruth=tuple.addTupleTool("TupleToolMCTruth")
        try:
            MCTruth.addTupleTool("MCTupleToolHierarchy")
        except AttributeError:
            pass
        try:
            MCTruth.addTupleTool("MCTupleToolKinematic")
        except AttributeError:
            pass
        #MCTruth.addTupleTool("MCTupleToolInteractions")
        MCTruth.addTupleTool("MCTupleToolReconstructed")
        if( ("MC12_Bs2KMuNu_SIGNAL" in mode) or( isMC and isNormalisation ) or( isMC and isSSNormalisation )):
            MCTruth.addTupleTool("MCTupleToolSemileptonic")
        tuple.ToolList +=  ["TupleToolGeneration"]

    tuple.ToolList +=  [
        "TupleToolPropertime"
        , "TupleToolRecoStats"
        , "TupleToolSLTools"
        , "TupleToolTrackPosition"
        # , "TupleToolRICHPid"
        , "TupleToolRecoStats"
        , "TupleToolMuonPid"
        , "TupleToolGeneration"
        ]
    #When we have the inclusive bkg of Adlene we will not know which kind of specific bkg of the event
    #Maybe it will become useful to add the Tool TupleToolHierarcy to know the specific channel (need to check if this is true)
    #Maybe also the Ancestor (the upper one) can be useful to know (need to understand better)
    return tuple


def makeEventTuple():
    from Configurables import EventTuple , TupleToolEventInfo
    etuple = EventTuple()
    return etuple


def makeSeq(mode,isopath='Phys/StdNoPIDsVeloPions/Particles'):
    TupleSeq   = GaudiSequencer('TupSeq'+mode)
    #stripping signalline = ""
    InputLocation_name = {
        "data15_Bs2KMuNu":"/Event/Semileptonic/Phys/B2XuMuNuBs2KLine/Particles",
        "data15_Bs2KMuNuSS":"/Event/Semileptonic/Phys/B2XuMuNuBs2KSSLine/Particles",

        "data12_Bs2KMuNu":"/Event/Semileptonic/Phys/B2XuMuNuBs2KLine/Particles",
        "data12_Bs2KMuNuSS":"/Event/Semileptonic/Phys/B2XuMuNuBs2KSSLine/Particles",
        "data12_Bs2DsMuNu":"/Event/Semileptonic/Phys/B2DMuNuX_Ds/Particles",
        "data12_Bs2DsMuNuSS":"/Event/Semileptonic/Phys/B2DMuNuX_Ds/Particles", #what here?? Does the stripping of not SS ask for differnt signs?

        "data11_Bs2KMuNu":"/Event/Semileptonic/Phys/B2XuMuNuBs2KLine/Particles",
        "data11_Bs2KMuNuSS":"/Event/Semileptonic/Phys/B2XuMuNuBs2KSSLine/Particles",
        "data11_Bs2DsMuNu":"/Event/Semileptonic/Phys/B2DMuNuX_Ds/Particles",
        "data11_Bs2DsMuNuSS":"/Event/Semileptonic/Phys/B2DMuNuX_Ds/Particles", #same here

        #"MC12_Bs2DsMuNu_Cocktail_SIGNAL":"/Event/AllStreams/Phys/b2DsMuXB2DMuNuXLine/Particles",
        "MC12_Bs2DsMuNu_Cocktail_SIGNAL":"Phys/B2DMuNuX_Ds/Particles",
        "MC12_Bs2DsMuNu_Cocktail_BKG":"/Event/AllStreams/Phys/B2XuMuNuBs2KLine/Particles",
        "MC12_Bs2KMuNu_SIGNAL":"/Event/AllStreams/Phys/B2XuMuNuBs2KLine/Particles",
        "MC12_Bs2KstMuNu_BKG":"/Event/AllStreams/Phys/B2XuMuNuBs2KLine/Particles",
        "MC12_Bs2JpsiPhi_BKG":"/Event/AllStreams/Phys/B2XuMuNuBs2KLine/Particles",
        "MC12_Bd2JpsiKst_BKG":"/Event/AllStreams/Phys/B2XuMuNuBs2KLine/Particles",
        "MC12_Bd2PiMuNu_NoMCut_BKG":"/Event/AllStreams/Phys/B2XuMuNuBs2KLine/Particles",
        "MC12_Bd2PiMuNu_Mcut_BKG":"/Event/AllStreams/Phys/B2XuMuNuBs2KLine/Particles",
        "MC12_Bu2JpsiK_BKG":"/Event/AllStreams/Phys/B2XuMuNuBs2KLine/Particles",
        "MC12_Bu2RhoMuNu_BKG":"/Event/AllStreams/Phys/B2XuMuNuBs2KLine/Particles",
        "MC12_Bu2D0MuNu_BKG":"/Event/AllStreams/Phys/B2XuMuNuBs2KLine/Particles",
        "MC12_Bc2D0MuNu_BKG":"/Event/AllStreams/Phys/B2XuMuNuBs2KLine/Particles",
        "MC12_Lb2PMuNu_LCSR_BKG":"/Event/AllStreams/Phys/B2XuMuNuBs2KLine/Particles",
        "MC12_Bd2Rhopi0piMuNu_BKG":"/Event/AllStreams/Phys/B2XuMuNuBs2KLine/Particles",
        "MC12_Bs2K2stkpi0MuNu_BKG":"/Event/AllStreams/Phys/B2XuMuNuBs2KLine/Particles",
        "MC12_Bs2Kst1430kpi0MuNu_BKG":"/Event/AllStreams/Phys/B2XuMuNuBs2KLine/Particles",
        "MC12_inclb_OC2Kmu_BKG":"/Event/AllStreams/Phys/B2XuMuNuBs2KLine/Particles",
        "MC12_inclb_OC2KmuSS_BKG":"/Event/AllStreams/Phys/B2XuMuNuBs2KLine/Particles",
        "MC12_inclb_OC2Kplusmu_BKG":"/Event/AllStreams/Phys/B2XuMuNuBs2KLine/Particles",
        "MC12_inclb_OC2Kmu_gencut_BKG":"/Event/AllStreams/Phys/B2XuMuNuBs2KLine/Particles",
        "MC12_inclb_OC2KmuSS_gencut_BKG":"/Event/AllStreams/Phys/B2XuMuNuBs2KLine/Particles",
        "MC12_inclb_OC2Kplusmu_gencut_BKG":"/Event/AllStreams/Phys/B2XuMuNuBs2KLine/Particles",
        #"MC12_Bd2DsDst_BKG":"/Event/AllStreams/Phys/b2DsMuXB2DMuNuXLine/Particles",
        #"MC12_Bs2DsstDsst_BKG":"/Event/AllStreams/Phys/b2DsMuXB2DMuNuXLine/Particles",
        #"MC12_Bu2DsstD0st_BKG":"/Event/AllStreams/Phys/b2DsMuXB2DMuNuXLine/Particles",
        "MC12_Bd2DsDst_SSBKG":     "Phys/B2DMuNuX_Ds/Particles",
        "MC12_Bs2DsstDsst_SSBKG":  "Phys/B2DMuNuX_Ds/Particles",
        "MC12_Bu2DsstD0st_SSBKG":  "Phys/B2DMuNuX_Ds/Particles",
        "MC12_Bd2DsDst_BKG":     "Phys/B2DMuNuX_Ds/Particles",
        "MC12_Bs2DsstDsst_BKG":  "Phys/B2DMuNuX_Ds/Particles",
        "MC12_Bu2DsstD0st_BKG":  "Phys/B2DMuNuX_Ds/Particles",

        #"MC11_Bs2DsMuNu_Cocktail_SIGNAL":"/Event/AllStreams/Phys/b2DsMuXB2DMuNuXLine/Particles",
        "MC11_Bs2DsMuNu_Cocktail_SIGNAL":"Phys/B2DMuNuX_Ds/Particles",
        "MC11_Bs2DsMuNu_Cocktail_BKG":"/Event/AllStreams/Phys/B2XuMuNuBs2KLine/Particles",
        "MC11_Bs2KMuNu_SIGNAL":"/Event/AllStreams/Phys/B2XuMuNuBs2KLine/Particles",
        "MC11_Bs2KstMuNu_BKG":"/Event/AllStreams/Phys/B2XuMuNuBs2KLine/Particles",
        "MC11_Bs2JpsiPhi_BKG":"/Event/AllStreams/Phys/B2XuMuNuBs2KLine/Particles",
        "MC11_Bd2JpsiKst_BKG":"/Event/AllStreams/Phys/B2XuMuNuBs2KLine/Particles",
        "MC11_Bd2PiMuNu_NoMCut_BKG":"/Event/AllStreams/Phys/B2XuMuNuBs2KLine/Particles",
        "MC11_Bd2PiMuNu_Mcut_BKG":"/Event/AllStreams/Phys/B2XuMuNuBs2KLine/Particles",
        "MC11_Bu2JpsiK_BKG":"/Event/AllStreams/Phys/B2XuMuNuBs2KLine/Particles",
        "MC11_Bu2RhoMuNu_BKG":"/Event/AllStreams/Phys/B2XuMuNuBs2KLine/Particles",
        "MC11_Bu2D0MuNu_BKG":"/Event/AllStreams/Phys/B2XuMuNuBs2KLine/Particles",
        "MC11_Bc2D0MuNu_BKG":"/Event/AllStreams/Phys/B2XuMuNuBs2KLine/Particles",
        "MC11_Lb2PMuNu_LCSR_BKG":"/Event/AllStreams/Phys/B2XuMuNuBs2KLine/Particles",
        "MC11_Bd2Rhopi0piMuNu_BKG":"/Event/AllStreams/Phys/B2XuMuNuBs2KLine/Particles",
        "MC11_Bs2K2stkpi0MuNu_BKG":"/Event/AllStreams/Phys/B2XuMuNuBs2KLine/Particles",
        "MC11_Bs2Kst1430kpi0MuNu_BKG":"/Event/AllStreams/Phys/B2XuMuNuBs2KLine/Particles",
        "MC11_inclb_OC2Kmu_BKG":"/Event/AllStreams/Phys/B2XuMuNuBs2KLine/Particles",
        "MC11_inclb_OC2KmuSS_BKG":"/Event/AllStreams/Phys/B2XuMuNuBs2KLine/Particles",
        "MC11_inclb_OC2Kplusmu_BKG":"/Event/AllStreams/Phys/B2XuMuNuBs2KLine/Particles",



         "MC11_11995200":"Phys/B2DMuNuX_Ds/Particles",
         "MC11_11995202":"Phys/B2DMuNuX_Ds/Particles",
         "MC11_12995600":"Phys/B2DMuNuX_Ds/Particles",
         "MC11_12995602":"Phys/B2DMuNuX_Ds/Particles",
         "MC11_13996200":"Phys/B2DMuNuX_Ds/Particles",
         "MC11_13996202":"Phys/B2DMuNuX_Ds/Particles",
         "MC11_15998000":"Phys/B2DMuNuX_Ds/Particles",
         "MC11_15998002":"Phys/B2DMuNuX_Ds/Particles",
         "MC11_11995201":"Phys/B2DMuNuX_Ds/Particles",
         "MC11_12995601":"Phys/B2DMuNuX_Ds/Particles",
         "MC11_13996201":"Phys/B2DMuNuX_Ds/Particles",


         "MC12_10010037":"b2KMu.TrigStrip/Phys/B2XuMuNuBs2K_FakeKMuLine/Particles"






        }
    isdata = False
    if("data" in mode):
        isdata = True

    # TO MAKE LOOSETUPLE TO STUDY THE STRIPPING EFF
    # CandidateMaker = makeLooseTuple_Bs2KMuNu()
    # myTuple = makeTuple(CandidateMaker.outputLocation(), mode, isopath=isopath)
    # TupleSeq.Members += [CandidateMaker.sequence(), myTuple]

    if( ("MC12_Bs2JpsiPhi_SIGNAL" in mode) or ("data12_Bs2JpsiPhi" in mode) or ("data11_Bs2JpsiPhi" in mode) ):
        CandidateMaker = makeBs2JpsiPhiSel(isdata)
        myTuple = makeTuple(CandidateMaker.outputLocation(), mode, isopath=isopath)
        TupleSeq.Members += [CandidateMaker.sequence(), myTuple]
    elif(("MC12_Bu2JpsiK_SIGNAL" in mode) or ("data12_Bu2JpsiK" in mode) or ("data11_Bu2JpsiK" in mode)):
        CandidateMaker = makeBu2JpsiKSel(isdata)
        myTuple = makeTuple(CandidateMaker.outputLocation(), mode, isopath=isopath)
        TupleSeq.Members += [CandidateMaker.sequence(), myTuple]
    # elif( ("MC" in mode) and (("BKG" in mode) or ("Bs2KMuNu_SIGNAL" in mode)) ):
    #     isSS = False
    #     if "SS" in mode: isSS = True
    #     CandidateMaker = makeNewStripSel_Bs2KMuNu(isSS)
    #     myTuple = makeTuple(CandidateMaker.outputLocation(), mode, isopath=isopath)
    #     TupleSeq.Members += [CandidateMaker.sequence(), myTuple]
    else:
        # for MC_Ds when reco as SIGNAL and maybe others (and data I think)
        myTuple = makeTuple(AutomaticData(InputLocation_name[mode]).outputLocation(), mode, isopath=isopath) # old stripping for MC, it's ok for the data too
        # for new stripping
        if InputLocation_name[mode] == "Phys/B2DMuNuX_Ds/Particles":
            from Configurables import EventNodeKiller
            event_node_killer = EventNodeKiller('StripKiller')
            event_node_killer.Nodes = ['/Event/AllStreams', '/Event/Strip']
            DaVinci().appendToMainSequence( [ event_node_killer, sc.sequence() ] )
        # myTuple = makeTuple("Phys/B2XuMuNuBs2KLine/Particles", mode, isopath=isopath)

        TupleSeq.Members += [myTuple]
        TupleSeq.ModeOR          = True
        TupleSeq.ShortCircuit    = False
        # for new stripping NoPID
        # NOPIDTuple = myTuple.clone("NoPIDTuple")
        # NOPIDTuple.Inputs    = ['Phys/B2XuMuNuBs2K_FakeKMuLine/Particles']
        # TupleSeq.Members += [NOPIDTuple]
        # TupleSeq.ModeOR          = True
        # TupleSeq.ShortCircuit    = False

    return TupleSeq


#            
# ====================================================================== 
def createVeloTracks():
    # still to add to thia function: MC associators
    from Configurables import ChargedProtoParticleMaker, DaVinci
    veloprotos = ChargedProtoParticleMaker("VeloProtoPMaker")
    veloprotos.Inputs = ["Rec/Track/Best"]
    veloprotos.Output = "Rec/ProtoP/myVeloProtoPMaker/ProtoParticles"
    DaVinci().appendToMainSequence( [ veloprotos ])
    from Configurables       import ProtoParticleCALOFilter, CombinedParticleMaker,NoPIDsParticleMaker
    from CommonParticles.Utils import trackSelector, updateDoD
    algorithm = NoPIDsParticleMaker('StdNoPIDsVeloPions',  Particle = 'pion',  )
    algorithm.Input = veloprotos.Output
    selector = trackSelector ( algorithm , trackTypes = ['Velo'] )
    locations = updateDoD ( algorithm )
    return algorithm
    
def configIso():
    from Configurables import ChargedProtoParticleMaker, DaVinci
    Alltracksprotos = ChargedProtoParticleMaker("AllProtoPMaker")
    Alltracksprotos.Inputs = ["Rec/Track/Best"]
    Alltracksprotos.Output = "Rec/ProtoP/myAllProtoPMaker/ProtoParticles"
    DaVinci().appendToMainSequence( [ Alltracksprotos ])
    from Configurables       import ProtoParticleCALOFilter, CombinedParticleMaker,NoPIDsParticleMaker
    from CommonParticles.Utils import trackSelector, updateDoD
    algorithm = NoPIDsParticleMaker('StdNoPIDsAlltracksPions',  Particle = 'pion' ,  )
    algorithm.Input = Alltracksprotos.Output
    selector = trackSelector ( algorithm , trackTypes = ['Velo', 'Long' , 'Upstream'] )
    locations = updateDoD ( algorithm )
    return algorithm

def mergeTracks():
    LongPions      = DataOnDemand( Location ='Phys/StdAllNoPIDsPions/Particles' )
    UpStreamPions  = DataOnDemand( Location ='Phys/StdNoPIDsUpPions/Particles'  )
    VeloPions      = DataOnDemand( Location ='Phys/StdNoPIDsVeloPions/Particles')
    AllPions       = MergedSelection( "AllPions", RequiredSelections =[ LongPions , UpStreamPions , VeloPions  ] )
    AllPions_seq   = SelectionSequence('AllPions_seq', TopSelection = AllPions)
    return AllPions_seq
#############################################################



#This is the only function to be used in gangajob.py and testBsKMuNu.py
def setupBsKMuNu(mode):
    DaVinci().UserAlgorithms += [createVeloTracks()]
    DaVinci().UserAlgorithms += [configIso()]
    AllPions_seq = mergeTracks()
    DaVinci().UserAlgorithms += [AllPions_seq]
    isMC = ("MC" in mode)
    if(isMC):
        DaVinci().UserAlgorithms += [makeEventTuple(), makeSeq(mode,isopath=AllPions_seq.outputLocation())]
    else:
        DaVinci().UserAlgorithms += [makeSeq(mode,isopath=AllPions_seq.outputLocation())]







DaVinci().TupleFile = "DTT_MC12_Bs2DsMuNu_13774002_Cocktail_SIGNAL_Up.root"
isMC =True
UseStrippingSel = True
DaVinci().DataType = "2012"
DaVinci().EvtMax = -1
DaVinci().PrintFreq = 25000
DaVinci().InputType = "DST"
DaVinci().Simulation = True
DaVinci().MainOptions = ""
DaVinci().SkipEvents = 0
DaVinci().Lumi = False
from Configurables import CondDB
DaVinci().DDDBtag = "dddb-20150522-2"
DaVinci().CondDBtag ="sim-20121025-vc-mu100"
setupBsKMuNu("MC12_Bs2DsMuNu_Cocktail_SIGNAL")
