# README

this is the README file for the Bs→Kµν analysis.
It mostly has to be written :)

# how to get the code

### through ssh

```shell
git clone --recursive ssh://git@gitlab.cern.ch:7999/lhcb-slb/BsToKMuNu.git
```

### through kerberos

```shell
git clone --recursive https://:@gitlab.cern.ch:8443/lhcb-slb/BsToKMuNu.git
```

### with a "normal" clone command

```shell
git clone https://:@gitlab.cern.ch:8443/lhcb-slb/BsToKMuNu.git
cd ./BsToKMuNu
git submodule update --init --recursive
```

### through an ssh tunnel and then ssh (if you're behind some badass firewall)

(assumes access to git*hub* is still fine)

```shell
ssh lxplus.cern.ch -fCNL 2000:gitlab.cern.ch:7999
git clone --recursive ssh://git@localhost:2000/lhcb-slb/BsToKMuNu.git
```

# how to work

All is happening in the `utils` subdirectory:

```shell
cd ./utils
source ./SetupEnv.sh
make -j8 -k
```

# workflow

## create ntuples

Switched to `lb-dev` and no longer using `SetupProject`

```shell
lb-dev DaVinci/v39r1p1
cd DaVinciDev_v39r1p1
git remote add Bs2KMuNu ssh://git@gitlab.cern.ch:7999/lhcb-slb/BsToKMuNu.git
git fetch Bs2KMuNu
git checkout Bs2KMuNu/master Phys
make -j 8
ganga -i Phys/BsToKMuNu/scripts/Bs_DsMuNu_TupleMaker/RunOnGanga.py
```




## trim ntuples (not for J/psi X modes)


The trimmer should not be run directly. Instead shell scripts are provided for convenience.
To trim one file:
```shell
cd utils/Trimmer
bash TrimFile.sh ShortPath.root
``` 
where `ShortPath.root` is path originating from `/eos/lhcb/wg/semileptonic/Bs2KmunuAna/Tuples`. e.g. `Bs2KMuNu_MCTUPLES_RAW_08Feb17/DTT_13512010_Bs_Kmunu_DecProdCut_Dn_Py8_MC12.root`

To trim all the data:
```shell
cd utils/Trimmer
bash TrimData.sh
```

To trim all the Monte Carlo:
```shell
cd utils/Trimmer
bash TrimMC.sh
```

When trimming all data/MC the batch system is used.



## add branch aliases (only for J/psi X modes)

check the `setting_aliases` root macro in `utils/BDT` (and read the usage
message in the comments)

## add BDT

check for batch submission scripts in `utils/BDT`

## Histogrammer

Generate a root file containing histograms.

How to build:
```shell
cd utils
. SetupEnv.sh
cd Histogrammer
make
```
How to run a minimal histogrammer. Source is at src/app/MinimalExample.cxx and provides a simple example.
```shell
./bin/MinimalExample
```

The main trimming code:
```shell
# Uses boost program options

$ ./bin/RunHistogrammer
Usage: ./bin/RunHistogrmmer [options]
Allowed options:
  -h [ --help ]                          produce help message
  --KMuNu                                Run the Bs -> K Mu Nu Histogrammer
  --DsMuNu                               Run the Bs -> Ds Mu Nu Histogrammer
  --JpsiK                                Run the B+ -> JPsi K Histogrammer
  --JpsiPhi                              Run the Bs -> JPsi Phi Histogrammer
  --AddOns                               After execution run the AddOns (if 
                                         any)
  --SaveTree                             Save Tree of values. Effectively a 
                                         trimmer
  -P [ --PrintFreq ] arg (=100)          Print information for every n events
  --MaxFiles arg (=-1)                   Maximum number of files to run over 
                                         for each sample
  --MaxEv arg (=-1)                      Maximum number of entries to run over 
                                         in each file
  -o [ --Output ] arg (=Histograms.root) Specify output file name
$ 
```

Typical set of commands to run histogrammer:

```shell

./bin/RunHistogrammer --KMuNu   --PrintFreq=10000 -o Histograms_KMuNu.root &
./bin/RunHistogrammer --DsMuNu  --PrintFreq=10000 --AddOns -o Histograms_DsMuNu.root &
./bin/RunHistogrammer --JpsiK   --PrintFreq=10000 -o Histograms_JpsiK.root &
./bin/RunHistogrammer --JpsiPhi --PrintFreq=10000 -o Histograms_JpsiPhi.root &


```


## make plots

use `utils/BDT_studies`. LHCb plot style should be picked up more or less
automatically.

## signal fit

WRITE ME

## normalisation fit

To setup the environment, compile and show the run instructions

```shell
cd utils
. SetupEnv.sh
cd Fitter_Control
make
./Fitter/bin/PerformFullFit
```

To generate the histograms from scratch:
```shell
./Fitter/bin/PerformFullFit Tools/Default_Config.conf 1
```
To perform the fit:
```shell
./Fitter/bin/PerformFullFit Tools/Default_Config.conf 2
```

To perform the toy studies:
```shell
./Fitter/bin/PerformFullFit Tools/Default_Config.conf 3
```

Replace the configuration file with one of your choice, or write your own.

## efficiencies

WRITE ME

## note writing

happens in the branch `mvesteri_...`

# directories

The directory structure is mainly a relict from the times when the ntuple
reading and ntuple writing code were still sharing the same repository.

## utils

Code to do something (fitting, trimming, BDT'ing)

## old_studies

Scripts to produce plots we looked at, at some point. Not necessary to run the
analysis, but maybe interesting to look at, at some point, or to understand the
reasoning for some conclusions.

# anaNote

The analysis note is drafted in a separate branch.
The latest build (from CI) is here: <a href="https://gitlab.cern.ch/lhcb-slb/BsToKMuNu/builds/artifacts/master/file/anaNote/BsToKMuNu-ANA-v0.pdf?job=BuildNote" target="_blank">latest ANA note</a>

# VIM goodies

A configuration for [YouCompleteMe](https://github.com/Valloric/YouCompleteMe)
is provided as proof of principle for the trimmer. See the [VIM](vim.md)
instructions.
